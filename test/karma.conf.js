// Karma configuration
// Generated on 2017-06-28

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      'jasmine'
    ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'bower_components/jquery/dist/jquery.js',
      'bower_components/angular/angular.js',
      'bower_components/bootstrap/dist/js/bootstrap.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-cookies/angular-cookies.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-loading-bar/build/loading-bar.js',
      'bower_components/chart.js/dist/Chart.js',
      'bower_components/angular-chart.js/dist/angular-chart.js',
      'bower_components/angularMultipleSelect/build/multiple-select.min.js',
      'bower_components/DataTables/media/js/jquery.dataTables.js',
      'bower_components/ngstorage/ngStorage.js',
      'bower_components/ngmap/build/scripts/ng-map.js',
      'bower_components/angular-messages/angular-messages.js',
      'bower_components/ng-file-upload/ng-file-upload.js',
      'bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js',
      'bower_components/lightbox2/dist/js/lightbox.js',
      'bower_components/lodash/lodash.js',
      'bower_components/jquery-ui/jquery-ui.js',
      'bower_components/angular-ui-sortable/sortable.js',
      'bower_components/moment/moment.js',
      'bower_components/angular-flash-alert/dist/angular-flash.js',
      'bower_components/ui-cropper/compile/minified/ui-cropper.js',
      'bower_components/select2/select2.js',
      'bower_components/angular-select2/dist/angular-select2.js',
      'bower_components/angular-ui-select2/src/select2.js',
      'bower_components/angular-ui-select/dist/select.js',
      'bower_components/pdfmake/build/pdfmake.js',
      'bower_components/pdfmake/build/vfs_fonts.js',
      'bower_components/dom-to-image/src/dom-to-image.js',
      'bower_components/es6-promise/es6-promise.js',
      'bower_components/ng-dom-to-pdf/dist/ng-dom-to-pdf.min.js',
      'bower_components/pdfjs-dist/build/pdf.js',
      'bower_components/pdfjs-dist/build/pdf.worker.js',
      'bower_components/angular-pdf/dist/angular-pdf.js',
      'bower_components/angular-smart-table/dist/smart-table.js',
      'bower_components/angular-utils-pagination/dirPagination.js',
      'bower_components/bootstrap-ui-datetime-picker/dist/datetime-picker.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
      // endbower
      'app/scripts/**/*.js',
      'test/mock/**/*.js',
      'test/spec/**/*.js'
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine'
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
