(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationMealPlansDetailController', OrganizationMealPlansDetailController);

        OrganizationMealPlansDetailController.$inject = ['$scope', '$location', '$state', 'records', 'GeneralMealPlansService', '$localStorage', 'URLS','organization'];

    function OrganizationMealPlansDetailController($scope, $location, $state, records, GeneralMealPlansService, $localStorage, URLS,organization) {
        var vm = this;
        vm.organization = organization;
        console.log("records");
        console.log(vm.records);
        vm.account = $localStorage.current_organisation;
        console.log($localStorage.current_organisation);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        console.log(vm.profile_image_url);
        vm.isAuthenticated = null;


    }
})();
