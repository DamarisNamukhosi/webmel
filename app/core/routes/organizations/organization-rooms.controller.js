(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationRoomsController', OrganizationRoomsController);

    OrganizationRoomsController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'organization', 'OrganizationsService'];

    function OrganizationRoomsController($scope, $rootScope, $stateParams, previousState, organization, OrganizationsService) {
        var vm = this;

        vm.organizationId = $stateParams.id;
        vm.catId = $stateParams.catId;
        vm.rooms = organization;
        console.log(vm.rooms);
        vm.previousState = previousState.name;
    }
})();