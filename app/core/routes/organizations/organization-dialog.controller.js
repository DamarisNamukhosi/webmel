(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationDialogController', OrganizationDialogController);

    OrganizationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$localStorage','entity', 'OrganizationsService', 'OrganizationTypesService'];

    function OrganizationDialogController($timeout, $scope, $stateParams, $uibModalInstance, $localStorage, entity, OrganizationsService, OrganizationTypesService) {
        var vm = this;

        vm.organization = entity;
        vm.clear = clear;
        vm.save = save;
        console.log('editing an org...');
        console.log(vm.organization);
        vm.organizationTypes = OrganizationTypesService.query();
        vm.organizations = OrganizationsService.getOrganizations();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.organization.id !== null) {
                console.log('on saving...');
                console.log(vm.organization);
                OrganizationsService.update(vm.organization, onSaveSuccess, onSaveError);
            } else {
                OrganizationsService.register(vm.organization, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);           
            console.log(result);
            $localStorage.current_organisation = result;
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
