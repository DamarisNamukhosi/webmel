(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "OrganizationRoomCategoriesController",
      OrganizationRoomCategoriesController
    );

  OrganizationRoomCategoriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "organization",
    "entity",
    "OrganizationsService",
    "objectLabelsByObjectType",
    "RoomsService"

  ];

  function OrganizationRoomCategoriesController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    organization,
    entity,
    OrganizationsService,
    objectLabelsByObjectType,
    RoomsService
  ) {
    var vm = this;

    vm.organizationId = $stateParams.id;
    vm.organization = organization;
    vm.entity = entity;
    vm.objectLabels = objectLabelsByObjectType;
    console.log(vm.organization);
    console.log(vm.objectLabels);
    vm.previousState = previousState.name;
    vm.displayOrganizations = true;
   RoomsService.getRoomsByCategory({ id: organization[0].id }).$promise.then(function (response){
      vm.rooms=response;
      console.log('ROOMS');
      console.log(vm.rooms[0] );
    
    } );
   //roomsFound
    if (vm.organization.length === 0) {
      vm.displayOrganizations = false;
    }
  }
})();
