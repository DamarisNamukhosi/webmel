(function() {
  'use strict';

  angular
      .module('webApp')
      .controller('OrganizationAlbumDetailController', OrganizationAlbumDetailController);

    OrganizationAlbumDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$localStorage', 'previousState', 'entity', 'URLS', 'OrganizationsService', 'album'];

    function OrganizationAlbumDetailController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, OrganizationsService, album) {
      var vm = this;


      vm.organization = entity;
      vm.album = album;
      vm.previousState = previousState.name;

      var permissions = $localStorage.current_user_permissions;

      /**
       * This hack checks whether the logged in user is a MASTER_USER
       * or not then allocates the appropriate sref for the
       * organizations-view
       */

      vm.previousState = permissions.indexOf("MASTER_USER") > -1 ? "organizations" : "organizations-own";

  }
})();
