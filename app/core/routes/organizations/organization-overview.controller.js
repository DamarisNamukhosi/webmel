(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OverviewController', OverviewController);

  OverviewController.$inject = ['$scope', '$state', '$rootScope', '$stateParams', '$localStorage', 'previousState', 'organization', 'OrganizationsService', 'URLS', 'LocationsService', 'MenusService', 'MealPlansService', 'RoomCategoryService'];

  function OverviewController($scope, $state, $rootScope, $stateParams, $localStorage, previousState, organization, OrganizationsService, URLS, LocationsService, MenusService, MealPlansService, RoomCategoryService) {
    var vm = this;

    vm.organization = organization;
    console.log("vm.organization");
    console.log(vm.organization);
    vm.previousState = previousState.name;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.organization.uuid;

    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;

    vm.locations = [];

    vm.mealPlans = [];

    vm.menus = [];

    vm.roomCategories = [];

    vm.currentState = $state.current.name;

    vm.getMealPlans = getMealPlans;

    vm.currentDiv = null;

    vm.getLocations = getLocations;

    vm.getRoomCategories = getRoomCategories;

    // locations for the map
    getLocations();

    // rest will be called when they're needed
    // // menus
    // getMenus();

    // // mealPlans
    // getMealPlans();

    // // contacts
    // getLocations();

    // // operating hours
    // getLocations();

    // // gallery
    // getLocations();

    vm.toggleContentStatus = function (organization) {

      organization.contentStatus = organization.contentStatus == 'DRAFT' ? 'PUBLISHED' : 'DRAFT';

      OrganizationsService.update(organization, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(response) {
      $state.reload();
    }

    function onSaveError(error) {
      console.log(error);
      alert(error);
    }

    function getLocations() {
      if (vm.locations == undefined || vm.locations.length == 0) {
        LocationsService.getLocationsByOrganizationId({
          id: vm.organization.id
        }).
        $promise.then(function (response) {
          vm.locations = response;
          vm.zoomLevel = 11;
          if (vm.locations[0].coordinates != "undefined") {
            vm.locationsCoordinates = vm.locations[0].coordinates;

            $localStorage.current_location_coordinates = vm.locationsCoordinates;
          }

        });
      }

      vm.currentDiv = 'overview';
    }

    function getMenus() {
      // only do query if there exists no data
      if (vm.menus == undefined || vm.menus.length == 0) {
        MenusService.getMenusByOrganizationId({
          id: vm.organization.id
        }).
        $promise.then(function (response) {
          vm.menus = response;
        });
      }
    }

    function getMealPlans() {
      // only do query if there exists no data
      if (vm.mealPlans == undefined || vm.mealPlans.length == 0) {
        MealPlansService.getMealPlans({
          id: vm.organization.id
        }).
        $promise.then(function (response) {
          vm.mealPlans = response;
        });
      }
      vm.currentDiv = 'meals';
      getMenus();
    }


    function getRoomCategories() {
      if (vm.roomCategories == undefined || vm.roomCategories.length == 0) {
        RoomCategoryService.filterByOrganisation({
          id: vm.organization.id
        }).
        $promise.then(function (response) {
          vm.roomCategories = response;
          vm.zoomLevel = 11;
        });
      }

      vm.currentDiv = 'rooms';
    }


  }
})();
