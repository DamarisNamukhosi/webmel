(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "OrganizationContactsController",
      OrganizationContactsController
    );

  OrganizationContactsController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "OrganizationsService",
    "contact"
  ];

  function OrganizationContactsController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    OrganizationsService,
    contact
  ) {
    var vm = this;

    console.log($stateParams);
    vm.organizationId = $stateParams.id;
    vm.organization = entity;
    console.log(vm.organization);
    vm.previousState = previousState.name;
  }
})();
