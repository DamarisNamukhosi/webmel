(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OrganizationEditDialogController', OrganizationEditDialogController);

  OrganizationEditDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$state', '$uibModalInstance', '$localStorage', 'entity', 'OrganizationsService', 'OrganizationTypesService', 'URLS', 'LocationsService', 'CoordinatesService', 'NgMap'];

  function OrganizationEditDialogController($timeout, $scope, $stateParams, $state, $uibModalInstance, $localStorage, entity, OrganizationsService, OrganizationTypesService, URLS, LocationsService, CoordinatesService, NgMap) {

    var vm = this;

    vm.organization = entity;

    vm.latlng = [0.8349313860427184, 37.4853515625];

    vm.locations = null;

    vm.setOrganizationTypeVisible = true;

    vm.tempParentLocations = null;
    vm.clear = clear;
    vm.save = save;

    vm.parentLocationChange = parentLocationChange;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.placeChanged = placeChanged;
    vm.handleClick = handleClick;
    vm.child = null;
    vm.zoomLevel = 7;
    // by Jeremy
    vm.allowedParentLocations = [3, 4, 5, 6, 7, 8, 12, 13, 17];

    vm.organizationWithFullLocation = {
      id: vm.organization.id,
      letterhead: vm.organization.letterhead,
      abbreviation: vm.organization.abbreviation,
      brief: vm.organization.brief,
      contentStatus: vm.organization.contentStatus,
      description: vm.organization.description,
      location: {},
      logo: vm.organization.logo,
      name: vm.organization.name,
      organisationTypeId: vm.organization.organisationTypeId,
      organisationTypeName: vm.organization.organisationTypeName,
      profileStatus: vm.organization.profileStatus,
      statusReason: vm.organization.statusReason,
      uuid: vm.organization.uuid
    };
    console.log(vm.organization);
    console.log("vm.organizationWithFullLocation");
    console.log(vm.organizationWithFullLocation);
    initFullLocation();


    vm.getpos = function (event) {
      console.log("event");
      console.log(event);
      console.log("vm.map");
      console.log(vm.map);
      //
      vm.latlng = [event.latLng.lat(), event.latLng.lng()];
      vm.lat = event.latLng.lat();
      vm.long = event.latLng.lng();
      vm.zoomLevel = 10;
    };


    function clear() {
      $uibModalInstance.dismiss('cancel');
    }



    function save() {
      console.log("ready to save");
      console.log(vm.organizationWithFullLocation);
      console.log("vm.latlng");
      console.log(vm.latlng);
      vm.organizationWithFullLocation.abbreviation = vm.organization.abbreviation;
      vm.organizationWithFullLocation.brief = vm.organization.brief;
      vm.organizationWithFullLocation.name = vm.organization.name;
      vm.organizationWithFullLocation.description = vm.organization.description;
      vm.organizationWithFullLocation.location.coordinates[0].latitude =
        vm.latlng[0];
      vm.organizationWithFullLocation.location.coordinates[0].longitude =
        vm.latlng[1];
      vm.organizationWithFullLocation.location.brief = vm.organization.brief;
      vm.organizationWithFullLocation.location.parentId = vm.organization.location.parentId;
      vm.organizationWithFullLocation.location.parentName = vm.organization.location.parentName;
      vm.organizationWithFullLocation.location.description = vm.organization.description;
      vm.organizationWithFullLocation.location.name = vm.organization.location.name;
      vm.organizationWithFullLocation.location.abbreviation = vm.organization.abbreviation;

      // vm.organizationWithFullLocation.location.coordinates[0].longitude= vm.latlng[1];

      console.log("vm.organizationWithFullLocation");
      console.log(vm.organizationWithFullLocation);
      OrganizationsService.editWithFullLocation(vm.organizationWithFullLocation, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
      console.log("success");
      console.log(result);
    }

    function onSaveError() {
      vm.isSaving = false;
      console.log("failure");
    }
    function initFullLocation() {
      NgMap.getMap().then(function (map) {
        vm.map = map;
      });

      vm.organizationTypes = OrganizationTypesService.query();

      LocationsService.getLocationsByOrganizationId({ id: vm.organization.id }).$promise.then(
        function (result) {
          vm.locations = result;

          console.log("vm.locations");
          console.log(vm.locations);

          vm.organization.location = vm.locations[0];
          vm.organizationWithFullLocation.location = vm.organization.location;
          console.log("vm.organization");
          console.log(vm.organization);

          vm.organization.parentLocationId = vm.locations[0].parentId;
          vm.latlng = [vm.locations[0].coordinates[0].latitude, vm.locations[0].coordinates[0].longitude];

          // organizationTypeChange(vm.organization.typeId);
        }
      )
      initParentLocation();
    }


    function initParentLocation() {
      vm.tempParentLocations = [];
      console.log('changed location Type id');

      LocationsService.query({ name: '', types: vm.allowedParentLocations }).$promise.then(function (result) {
        vm.parentLocations = result;
        console.log("vm.parentLocations");
        console.log(vm.parentLocations);
        angular.forEach(vm.parentLocations, function (location) {
          vm.maxParent = 9;
          if ((location.typeId <= vm.maxParent || location.typeId == 12 || location.typeId == 13)) {
            vm.tempParentLocations.push(location);
          }
        })
      })
      if (vm.organization.id !== null && vm.organization.id !== undefined) {
        console.log("vm.organization.id ");
        console.log(vm.organization.id);
        console.log(vm.organization.parentLocationId === "undefined");
        console.log(vm.organization.parentLocationId !== null);
        if (vm.organization.parentLocationId !== null && vm.organization.parentLocationId !== undefined) {
          console.log("caling parent location changes");
          parentLocationChange(vm.organization.parentLocationId);
        } else {
          vm.latlng = [0.8349313860427184, 37.4853515625];
          console.log(vm.organizationWithFullLocation);
          console.log("vm.latlng");
          console.log(vm.latlng);
        }
      }

    }

    function parentLocationChange(id) {
      //vm.ParentLocations= [];
      console.log('changed parent location id');
      console.log(id);
      vm.locationCoordinates = [];

      LocationsService.getFullLocation({
        id: id
      }).$promise.then(function (response) {
        console.log("response");
        console.log(response);
        vm.latlng = [response.coordinates[0].latitude, response.coordinates[0].longitude];
        vm.zoomLevel = 11;
        vm.locations[0].latitude = response.coordinates[0].latitude;
        vm.locations[0].longitude = response.coordinates[0].longitude;
        vm.locations[0].parentId = response.id;
        vm.locations[0].parentName = response.name;
        vm.locations[0].coordinates[0].latitude = response.coordinates[0].latitude;
        vm.locations[0].coordinates[0].longitude = response.coordinates[0].longitude;
        vm.organizationWithFullLocation.location = vm.locations[0];
      });

    }

    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 16, 17, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          });
        });
    }

    function onLocationSelected($item, $model) {

      vm.organization.location.parentId = $item.id;

    }
    function placeChanged() {
      vm.place = this.getPlace();
      console.log('location', vm.place.geometry.location);
      NgMap.getGeoLocation(vm.address).then(function (latlng) {
        console.log(latlng);

        console.log(latlng.lat());
        console.log(latlng.lng());
        vm.latlng = [latlng.lat(), latlng.lng()];
      });
    }

    function handleClick() {

      vm.lat2 = parseFloat(vm.lat2);
      vm.long2 = parseFloat(vm.lon2);

      vm.latlng2 = [vm.lat2, vm.long2];
      if (vm.lat2 === "" || vm.long2 === "" || vm.lat2 === null || vm.long2 === null || vm.lat2 === undefined || vm.long2 === undefined) {

        console.log("ERROR!! Ensure you've put correct and all values for both latitude and longitude!");

      } else {

        console.log("vm.latlng2");
        vm.latlng = [vm.lat2, vm.long2];
        console.log(vm.latlng2);
      }
      vm.zoomLevel = 9;
    }
  }

})();
