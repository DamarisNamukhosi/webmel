(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OrganizationRegisterDialogController', OrganizationRegisterDialogController);

  OrganizationRegisterDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$state', '$uibModalInstance', '$localStorage', 'entity', 'OrganizationsService', 'OrganizationTypesService', 'LocationsService', 'URLS', 'CoordinatesService', 'NgMap'];

  function OrganizationRegisterDialogController($timeout, $scope, $stateParams, $state, $uibModalInstance, $localStorage, entity, OrganizationsService, OrganizationTypesService, LocationsService, URLS, CoordinatesService, NgMap) {
    var vm = this;

    vm.organization = entity;
    console.log(vm.organization);
    vm.clear = clear;
    vm.save = save;
    vm.organizationTypeChange = organizationTypeChange;
    vm.parentLocationChange = parentLocationChange;
    vm.initCoordinates = initCoordinates;
    vm.initWhenEditing = initWhenEditing;
    vm.handleClick = handleClick;
    vm.placeChanged = placeChanged;
    vm.tempParentLocations = [];
    vm.child = {
      "longitude": null,
      "latitude": null
    };
    // vm.place = null;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.setOrganizationTypeVisible = false;
    vm.editing = false;
    vm.geoCode = null;
    initWhenEditing();
    initCoordinates();

    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
    vm.pauseLoading = true;
    //will disable submit button and show loading gif
    // console.log("Starting a timer to wait for 2 seconds before the map will start loading");




    $timeout(function () {
      console.debug("Showing the map. The google maps api should load now.");
      vm.pauseLoading = false;
    }, 2000);

    //vm.organisation_types = $localStorage.organisation_types;

    vm.getpos = function (event) {
      vm.latlng = [event.latLng.lat(), event.latLng.lng()];
      vm.lat = event.latLng.lat();
      vm.long = event.latLng.lng();

      vm.zoomLevel = 9;
    };

    function handleClick() {

      vm.lat2 = parseFloat(vm.lat2);
      vm.long2 = parseFloat(vm.lon2);

      vm.latlng2 = [vm.lat2, vm.long2];
      if (vm.lat2 === "" || vm.long2 === "" || vm.lat2 === null || vm.long2 === null || vm.lat2 === undefined || vm.long2 === undefined) {

        console.log("ERROR!! Ensure you've put correct and all values for both latitude and longitude!");

      } else {

        console.log("vm.latlng2");
        vm.latlng = [vm.lat2, vm.long2];
        console.log(vm.latlng2);
      }
      vm.zoomLevel = 9;
    }


    function placeChanged() {
      vm.place = this.getPlace();
      console.log('location', vm.place.geometry.location);
      NgMap.getGeoLocation(vm.address).then(function (latlng) {
        console.log(latlng);

        console.log(latlng.lat());
        console.log(latlng.lng());
        vm.latlng = [latlng.lat(), latlng.lng()];
      });
    }


    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      vm.organization.latitude = vm.latlng[0];
      vm.organization.longitude = vm.latlng[1];
      vm.organization.altitude = 0;

      vm.organization.parentOrganisationId = $localStorage.current_organisation.id;
      console.log('vm.organization');
      console.log(vm.organization.parentOrganisationId);
      console.log(vm.organization);
      if (vm.organization.id === null || vm.organization.id === "undefined") {
        console.log("registering...");
        OrganizationsService.register(vm.organization, onSaveSuccess, onSaveError);
      } else {
        console.log("updating..");
        LocationsService.update(vm.organization, onSaveSuccess, onSaveError);

      }

    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function organizationTypeChange(id) {
      vm.tempParentLocations = [];
      console.log('changed location Type id');
      console.log(id);
      //LocationsService.LocationsService
      angular.forEach(vm.locations, function (location) {
        vm.maxParent = 9;
        if ((location.typeId <= vm.maxParent || location.typeId == 12 || location.typeId == 13)) {
          vm.tempParentLocations.push(location);
        }
      })

    }

    function parentLocationChange(id) {
      //vm.ParentLocations= [];
      console.log('changed parent location id');
      console.log(id);
      vm.locationCoordinates = [];

      CoordinatesService.getCoordinatesByLocation({
        id: $stateParams.id
      }).$promise.then(function (response) {
        angular.forEach(response, function (object) {
          vm.child.longitude = object.longitude;
          vm.child.latitude = object.latitude;
          vm.str = vm.child.latitude, vm.child.longitude;
          vm.zoomLevel = 9;
        });
      });

    }

    function initCoordinates() {

      if (vm.editing === false) {
        if (($stateParams.long !== null && $stateParams.long !== undefined) || ($stateParams.lat !== null && $stateParams.lat !== undefined)) {
          console.log('editing parent');
          vm.latlng2 = [];
          vm.latlng = [$stateParams.lat, $stateParams.long];
          vm.lat = $stateParams.lat;
          vm.long = $stateParams.long;
          vm.str = vm.long, vm.lat;
          vm.zoomLevel = 9;
          console.log(vm.str);
        } else {
          console.log('new parent');
          if ($localStorage.hasOwnProperty("current_location_coordinates")) {
            if ($localStorage.current_location_coordinates !== "undefined") {
              console.log("vm.latlng");
              console.log($localStorage.current_location_coordinates);
              vm.latlng = [$localStorage.current_location_coordinates.latitude, $localStorage.current_location_coordinates.longitude];
              console.log("vm.latlng from current_location_coordinates");
              console.log(vm.latlng);
              vm.organization.latitude = vm.latlng[0];
              vm.organization.longitude = vm.latlng[1];
            } else if ($stateParams.id !== undefined) {
              console.log($stateParams.id);
              console.log('under parent');
              CoordinatesService.getCoordinatesByLocation({
                id: $stateParams.id
              }).$promise.then(function (response) {
                angular.forEach(response, function (object) {
                  vm.child.longitude = object.longitude;
                  vm.child.latitude = object.latitude;
                  vm.str = vm.child.latitude, vm.child.longitude;
                  vm.zoomLevel = 9;
                });
              });
            } else {
              console.log('from home');
              vm.latlng = [0.8349313860427184, 37.4853515625];
              vm.lat = 0.8349313860427184;
              vm.long = 37.4853515625;
              vm.str = vm.lat, vm.long;
              vm.zoomLevel = 9;
              console.log(vm.str);
              vm.organization.latitude = vm.latlng[0];
              vm.organization.longitude = vm.latlng[1];
              vm.IsSaving = false;
            }
          } else {
            console.log('current_location_coordinates isnt there');
            vm.latlng = [0.8349313860427184, 37.4853515625];
            vm.lat = 0.8349313860427184;
            vm.long = 37.4853515625;
            vm.str = vm.lat, vm.long;
            vm.zoomLevel = 9;
            console.log(vm.str);
            vm.organization.latitude = vm.latlng[0];
            vm.organization.longitude = vm.latlng[1];
            vm.IsSaving = false;
          }
        }
      } else {
        console.log("loc edit");
        if (vm.organization.latitude === null || vm.organization.latitude === undefined && vm.organization.longitude === null || vm.organization.longitude === undefined) {
          console.log("loc doesnt have lat & long");
          console.log($localStorage.hasOwnProperty("current_location_coordinates"));
          if ($localStorage.hasOwnProperty("current_location_coordinates")) {
            if ($localStorage.current_location_coordinates !== undefined || $localStorage.current_location_coordinates !== null) {
              console.log("localstorage has  lat & long");
              console.log($localStorage.current_location_coordinates);
              vm.latlng = [$localStorage.current_location_coordinates.latitude, $localStorage.current_location_coordinates.longitude];
              console.log("vm.latlng");
              console.log(vm.latlng);
              vm.organization.latitude = vm.latlng[0];
              vm.organization.longitude = vm.latlng[1];
            }
          } else {
            console.log("set lat & long to default coords");
            vm.latlng = [0.8349313860427184, 37.4853515625];
            console.log("vm.latlng");
            console.log(vm.latlng);
            vm.organization.latitude = vm.latlng[0];
            vm.organization.longitude = vm.latlng[1];
          }
        } else {
          console.log("loc has lat & long");
          vm.latlng = [vm.organization.latitude, vm.organization.longitude];
          console.log("vm.latlng");
          console.log(vm.latlng);
          vm.organization.latitude = vm.latlng[0];
          vm.organization.longitude = vm.latlng[1];
        }
      }
    }

    function initWhenEditing() {
      if (vm.organization.id !== null && vm.organization.id !== "undefined") {
        console.log("called first editing");
        console.log(vm.organization.id !== null);
        vm.editing = true;
        vm.setOrganizationTypeVisible = (vm.organization.typeId === undefined ||
          vm.organization.typeId === null);
        vm.setParentLocationVisible = (vm.organization.parentId === undefined ||
          vm.organization.parentId === null);

      } else {
        console.log("called first not editing");
        console.log(vm.organization.id);
        vm.setOrganizationTypeVisible = (vm.organization.organisationTypeId === undefined ||
          vm.organization.organisationTypeId === null);
        vm.setParentLocationVisible = (vm.organization.parentLocationId === undefined ||
          vm.organization.parentLocationId === null);

      }


      if (vm.setOrganizationTypeVisible) {
        vm.organizationTypes = OrganizationTypesService.query();
      }
      if (vm.setOrganizationTypeVisible) {
        LocationsService.query().$promise.then(function (result) {
          vm.locations = result;

          console.log('result');
          console.log(result);

          vm.tempParentLocations = vm.locations;

          organizationTypeChange(vm.organization.organisationTypeId);
        });
      }
    }
    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 16, 17, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {

      vm.organization.parentLocationId = $item.id;

    }
  }
})();
