(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationMealPlanController', OrganizationMealPlanController);

    OrganizationMealPlanController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'organization', 'OrganizationsService'];

    function OrganizationMealPlanController($scope, $rootScope, $stateParams, previousState, organization, OrganizationsService) {
        var vm = this;

        vm.organizationId = $stateParams.id;
        vm.organization = organization;
        console.log(vm.organization);
        vm.previousState = previousState.name;
    }
})();
