(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationRoomDialogController', OrganizationRoomDialogController);

    OrganizationRoomDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OrganizationsService', 'OrganizationTypesService'];

    function OrganizationRoomDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, OrganizationsService, OrganizationTypesService) {
        var vm = this;

        vm.room = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.room.id !== null) {
                OrganizationsService.updateRoom(vm.room, onSaveSuccess, onSaveError);
            } else {
                OrganizationsService.createRoom(vm.room, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
