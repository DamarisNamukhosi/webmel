(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationDetailController', OrganizationDetailController);

        OrganizationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'OrganizationsService'];

    function OrganizationDetailController($scope, $rootScope, $stateParams, previousState, entity, OrganizationsService) {
        var vm = this;
        
        $stateParams.orgName = vm.organization.name;
        console.log($stateParams);
        vm.organization = entity;
        vm.previousState = previousState.name;
    }
})();
