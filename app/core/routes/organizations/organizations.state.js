(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('organizations', {
        parent: 'app',
        url: '/organizations',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organizations.html',
            controller: 'OrganizationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organizations: [
            '$stateParams',
            '$localStorage',
            'OrganizationsService',
            function ($stateParams, $localStorage, OrganizationsService) {
              console.log('getting managed orgs..');
              return OrganizationsService.getManagedOrganizations({
                org_id: $localStorage.current_organisation.id,
                relationship_type: 1
              }).$promise;
            }
          ],
          params: {
            page: {
              value: '1',
              squash: true
            },
            sort: {
              value: 'id,desc',
              squash: true
            },
            search: null
          },
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ]
        }
      })
      .state('organizations.new', {
        parent: 'organizations',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: null,
                      parentLocationId: null,
                      parentOrganisationId: $localStorage
                        .current_organisation.id,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('organizations', null, {
                    reload: 'organizations'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-detail.edit', {
        parent: 'organization-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationEditDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'OrganizationsService',
                    function ($stateParams, OrganizationsService) {
                      return OrganizationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-detail', null, {
                    reload: 'organization-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organizations-managed', {
        parent: 'app',
        url: '/my-organizations',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organizations-managed.html',
            controller: 'OrganizationsManagedController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organizations: [
            '$stateParams',
            '$localStorage',
            'OrganizationsService',
            function (
              $stateParams,
              $localStorage,
              OrganizationsService
            ) {
              return OrganizationsService.getManagedOrganizations({
                org_id: $localStorage.current_organisation.id,
                relationship_type: '1'
              });
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organizations-managed',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organizations-managed.new', {
        parent: 'organizations-managed',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER'],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: null,
                      parentLocationId: null,
                      parentOrganisationId: $localStorage
                        .current_organisation.id,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('organizations-managed', null, {
                    reload: 'organizations-managed'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-detail', {
        parent: 'organizations-managed',
        url: '/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-overview.html',
            controller: 'OverviewController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'organizations-managed',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('organization-detail-manage', {
        parent: 'organizations-managed',
        url: '/{id}/manage',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-overview.html',
            controller: 'OverviewController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'organizations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('organization-mealplans', {
        parent: 'organizations',
        url: '/{id}/meal-plans',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-mealplans.html',
            controller: 'OrganizationMealPlanController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getMealPlansByOrganizations({
                id: $stateParams.id
              }).$promise;
            }
          ],
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organization-mealplans.new', {
        parent: 'organization-mealplans',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/meal-plans/meal-plans-dialog.html',
            controller: 'MealPlansDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: function () {
                return {
                  id: null,
                  uuid: null,
                  description: null,
                  generalMealPlanId: null,
                  generalMealPlanName: null,
                  organisationId: $stateParams.id,
                  organisationName: null
                };
              }
            }
          }).result.then(function () {
            $state.go('organization-mealplans', null, {
              reload: 'organization-mealplans'
            });
          }, function () {
            $state.go('organization-mealplans');
          });
        }]
      })
      .state('organization-mealplans-detail', {
        parent: 'organization-mealplans',
        url: '/meal-plans/{mealPlanId}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Meal Plans Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-mealplans-detail.html',
            controller: 'MealPlansDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: ['$stateParams', 'MealPlansService', function ($stateParams, MealPlansService) {
            return MealPlansService.get({ id: $stateParams.mealPlanId }).$promise;
          }],
          previousState: ["$state", function ($state) {
            var currentStateData = {
              name: $state.current.name || 'organization-mealplans',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }]
        }
      })
      .state('organization-mealplans.edit', {
        parent: 'organization-mealplans',
        url: '/{mealPlanId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/meal-plans/meal-plans-dialog.html',
                controller: 'MealPlansDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'MealPlansService',
                    function (MealPlansService) {
                      return MealPlansService.get({
                        id: $stateParams.mealPlanId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-mealplans', null, {
                    reload: 'organization-mealplans'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-mealplans.delete', {
        parent: 'organization-mealplans',
        url: '/{mealPlanId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/meal-plans/meal-plans-delete-dialog.html',
                controller: 'MealPlansDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'MealPlansService',
                    function (MealPlansService) {
                      return MealPlansService.get({
                        id: $stateParams.mealPlanId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-mealplans', null, {
                    reload: 'organization-mealplans'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-room-categories', {
        parent: 'organizations',
        url: '/{id}/room-categories',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-room-categories.html',
            controller: 'OrganizationRoomCategoriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getRoomCategoriesByOrganizations({
                id: $stateParams.id
              }).$promise;
            }
          ],
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          objectLabelsByObjectType: [
            "ObjectLabelsService",
            function (ObjectLabelsService) {
              return ObjectLabelsService.getLabelsByObjectType({
                objectType: "ROOM"
              }).$promise;
            }
          ],

          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organization-room-categories.newRoom', {
        parent: 'organization-room-categories',
        url: '/new-categories',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Room detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/room-categories/room-category-dialog.html',
                controller: 'RoomCategoryDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      identification: null,
                      brief: null,
                      description: null,
                      notes: null,
                      building: null,
                      floor: null,
                      floorPosition: null,
                      roomArea: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      roomCategoryId: $stateParams.id
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go(
                    'organization-room-categories',
                    null, {
                      reload: 'organization-room-categories'
                    }
                  );
                },
                function () {
                  $state.go('organization-room-categories');
                }
              );
          }
        ]
      })
      .state('organization-rooms-by-categories', {
        parent: 'organization-room-categories',
        url: '/{catId}/rooms',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-rooms.html',
            controller: 'OrganizationRoomsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getRoomsByRoomCategories({
                id: $stateParams.catId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-room-categories',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      /*****************************************organization-rooms-by categories********************************************************/
      .state('organization-room', {
        parent: 'organization-room-categories',
        url: '/{roomId}',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-room.html',
            controller: 'OrganizationRoomController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getRoom({
                id: $stateParams.roomId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organization-room.edit', {
        parent: 'organization-room',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-room-dialog.html',
                controller: 'OrganizationRoomDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.getRoom({
                        id: $stateParams.roomId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-room', null, {
                    reload: 'organization-room'
                  });
                },
                function () {
                  $state.go('organization-room');
                }
              );
          }
        ]
      })
      /*****************************************organization-contacts**************************************************************/
      .state('organization-contacts', {
        parent: 'organizations-managed',
        url: '/{id}/contacts',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-contacts.html',
            controller: 'OrganizationContactsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          contact: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getContactsByOrganisation({
                id: $stateParams.id
              }).$promise;
            }
          ],
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      /*****************************************organization-contacts**************************************************************/
      /****************************************organisation-contact-detail-edit***************************************************/
      .state('organization-contacts.edit', {
        parent: 'organization-contacts',
        url: '/{contactId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-contacts-edit-dialog.html',
                controller: 'OrganizationContactEditDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.getContact({
                        id: $stateParams.contactId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-contacts', null, {
                    reload: 'organization-contacts'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      /*****************************************organization-contact-detail-edit**************************************************************/
      /****************************************organisation-contact-addresses-detail-edit***************************************************/
      .state('organization-contacts.editAddress', {
        parent: 'organization-contacts',
        url: '/{contactId}/contact-address/{addressId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-contact-address-edit-dialog.html',
                controller: 'OrganizationContactEditDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.getContactAddress({
                        id: $stateParams.addressId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organizations', null, {
                    reload: 'organizations'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      /*****************************************organization-contact-addresses**************************************************************/
      /*****************************************organization-hours********************************************************/
      .state('organization-hours', {
        parent: 'app',
        url: '/{id}/operating-hours',
        data: {
          requiresAuthentication: true,
          authorities: ['MASTER_USER', 'MANAGER_USER']
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-operating-hours.html',
            controller: 'OrganizationHoursController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          organization: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.getOperatingHoursByOrganisation({
                id: $stateParams.id
              }).$promise;
            }
          ],
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name ||
                  'organization-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      //****************************new operating hours ********************/
      .state('organization-operating-hours.new', {
        parent: 'organization-hours',
        url: '/{id}/operating-hours',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/operating-hours/operating-hour-dialog.html',
                controller: 'OperatingHourDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      openingTime: null,
                      operatingHoursType: null,
                      organisationId: $stateParams.id,
                      organisationName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('organization-hours', null, {
                    reload: 'organization-hours'
                  });
                },
                function () {
                  $state.go('organization-hours');
                }
              );
          }
        ]
      })
      /*****************************************organization-operating-hours********************************************************/
      /****************************************organisation-hours-edit***************************************************/
      .state('organization-hours.edit', {
        parent: 'organization-hours',
        url: '/{hourId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-hours-edit-dialog.html',
                controller: 'OrganizationHoursEditDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.getOrganizationHour({
                        id: $stateParams.hourId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-hours', null, {
                    reload: 'organization-hours'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      /*****************************************organization-hours-edit**************************************************************/
      /***********************************************organizations.edit************************************************************/
      .state('organizations.edit', {
        parent: 'organizations',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-dialog.html',
                controller: 'OrganizationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organizations', null, {
                    reload: 'organizations'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      /***********************************************organizations.edit************************************************************/
      .state('organization-detail.suspend', {
        parent: 'organization-detail',
        url: '/suspend',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-suspend-dialog.html',
                controller: 'OrganizationSuspendDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-detail', null, {
                    reload: 'organization-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-detail.delete', {
        parent: 'organization-detail',
        url: '{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-delete-dialog.html',
                controller: 'OrganizationDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('organization-detail', null, {
                    reload: 'organization-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-detail.profile', {
        parent: 'organization-detail',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('organization-detail', null, {
                    reload: 'organization-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-galleries', {
        parent: 'organizations-managed',
        url: '/{id}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [
            'MASTER_USER',
            'D_AUTHORITY_USER',
            'MANAGE_LOCATIONS',
            'MANAGER_USER',
            'ORGANISATION_ADMIN'
          ],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-galleries.html',
            controller: 'OrganizationGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'organizations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organization-album-detail', {
        parent: 'organization-galleries',
        url: '/{albumId}/album',
        data: {
          requiresAuthentication: true,
          authorities: [
            'MASTER_USER',
            'D_AUTHORITY_USER',
            'MANAGE_LOCATIONS'
          ],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/organizations/organization-album-detail.html',
            controller: 'OrganizationAlbumDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'OrganizationsService',
            function ($stateParams, OrganizationsService) {
              return OrganizationsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          album: [
            '$stateParams',
            'AlbumService',
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'organizations-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('organization-galleries.new', {
        parent: 'organization-galleries',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-create-dialog.html',
                controller: 'AlbumCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OrganizationsService',
                    function (OrganizationsService) {
                      return OrganizationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: 'GENERAL',
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('organization-galleries', null, {
                    reload: 'organization-galleries'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-album-detail.edit', {
        parent: 'organization-album-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'organization-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-dialog.html',
                controller: 'AlbumDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AlbumService',
                    function (AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go(
                    'organization-album-detail',
                    null, {
                      reload: 'organization-album-detail'
                    }
                  );
                },
                function () {
                  $state.go('organization-album-detail');
                }
              );
          }
        ]
      })
      .state('organization-album-detail.upload', {
        parent: 'organization-album-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-upload-dialog.html',
                controller: 'AlbumUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'AlbumService',
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go(
                    'organization-album-detail',
                    null, {
                      reload: 'organization-album-detail'
                    }
                  );
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('organization-album-detail.makeCoverImage', {
        parent: 'organization-album-detail',
        url: '/{imageId}/cover-image',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'organizations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
                controller: 'AlbumChangeCoverImageDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go(
                    'organization-album-detail',
                    null, {
                      reload: 'organization-album-detail'
                    }
                  );
                },
                function () {
                  $state.go('organization-album-detail');
                }
              );
          }
        ]
      })
      .state('organization-album-detail.deleteImage', {
        parent: 'organization-album-detail',
        url: '/{imageId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'organizations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
                controller: 'AlbumImageDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go(
                    'organization-album-detail',
                    null, {
                      reload: 'organization-album-detail'
                    }
                  );
                },
                function () {
                  $state.go('organization-album-detail');
                }
              );
          }
        ]
      });
  }
})();
