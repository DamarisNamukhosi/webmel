(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OrganizationsController', OrganizationsController);

  OrganizationsController.$inject = ['$scope', '$location', '$state', 'organizations', 'OrganizationTypesService', 'OrganizationsService', 'ParseLinks','pagingParams','paginationConstants' ];

  function OrganizationsController($scope, $location, $state, organizations, OrganizationTypesService, OrganizationsService, ParseLinks,pagingParams,paginationConstants ) {
    var vm = this;
    vm.account = null;
    vm.isAuthenticated = null;
    vm.organizations = organizations;
    vm.organizationTypes = OrganizationTypesService.query();

    vm.activate = function (organization) {
      if (organization.profileStatus == "ACTIVE") {
        organization.profileStatus = "INACTIVE"
      } else {
        organization.profileStatus = "ACTIVE"
      }
      OrganizationsService.update(organization, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(response) {
      $state.reload();
    }

    function onSaveError(error) {
      console.log(error);
      alert(error);
    }

  }
})();
