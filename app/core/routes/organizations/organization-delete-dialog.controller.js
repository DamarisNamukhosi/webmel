(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationDeleteDialogController',OrganizationDeleteDialogController);

        OrganizationDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'OrganizationsService'];

    function OrganizationDeleteDialogController($uibModalInstance, entity, OrganizationsService) {
        var vm = this;

        vm.organization = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            
            vm.organization.contentStatus = "DELETED";
            
            // OrganizationsService.deleteOwn(id,
            //     function () {
            //         $uibModalInstance.close(true);
            //     });

            OrganizationsService.update(vm.organization,
                function () {
                    $uibModalInstance.close(true);
                });
            //OrganizationsService.update({},)
        }
    }
})();
