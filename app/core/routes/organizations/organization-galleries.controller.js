(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("OrganizationGalleriesController", OrganizationGalleriesController);

  OrganizationGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService"
  ]; //

  function OrganizationGalleriesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.organization = entity;
    vm.albums = AlbumService.getAlbums({ uuid: vm.organization.uuid });
    console.log(vm.albums);
    console.log(vm.organization);

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;

    var permissions = $localStorage.current_user_permissions;

    /**
       * This hack checks whether the logged in user is a MASTER_USER
       * or not then allocates the appropriate sref for the
       * organizations-view
       */

    vm.previousState =
      permissions.indexOf("MASTER_USER") > -1 ? "organizations" : "organizations-own";
  }
})();
