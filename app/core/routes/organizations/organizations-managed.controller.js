(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OrganizationsManagedController', OrganizationsManagedController);

  OrganizationsManagedController.$inject = ['$scope', '$location', '$state', 'organizations', 'OrganizationsService', 'URLS'];

  function OrganizationsManagedController($scope, $location, $state, organizations, OrganizationsService, URLS) {
    var vm = this;
    vm.account = null;
    vm.isAuthenticated = null;
   // vm.organizationTypes = OrganizationTypesService.query();
    vm.organizations = organizations;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;

    // vm.activate = function(organization){
    //   if (organization.profileStatus == "ACTIVE"){
    //     organization.profileStatus = "INACTIVE"
    //   }else{
    //     organization.profileStatus = "ACTIVE"
    //   }
    //   OrganizationsService.update(organization, onSaveSuccess, onSaveError);
    // }

    function onSaveSuccess(response){
      // $state.transitionTo($state.current, $stateParams, { 
      //   reload: true, inherit: false, notify: true
      // });
      $state.reload();
    }

    function onSaveError(error){
      console.log(error);
      alert(error);
    }

  }
})();
