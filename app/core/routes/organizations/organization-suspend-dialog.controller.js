(function() {
  'use strict';

  angular
      .module('webApp')
      .controller('OrganizationSuspendDialogController',OrganizationSuspendDialogController);

      OrganizationSuspendDialogController.$inject = ['$uibModalInstance', 'entity', 'OrganizationsService'];

  function OrganizationSuspendDialogController($uibModalInstance, entity, OrganizationsService) {
      var vm = this;

      vm.organization = entity;
      vm.clear = clear;
      vm.comfirmSuspend = comfirmSuspend;

      function clear () {
          $uibModalInstance.dismiss('cancel');
      }

      function comfirmSuspend(organization) {
          organization.profileStatus = "SUSPENDED";
          OrganizationsService.update(organization,
              function () {
                  $uibModalInstance.close(true);
              });
      }
  }
})();
