(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationContactAddressesController', OrganizationContactAddressesController);

    OrganizationContactAddressesController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'organization', 'OrganizationsService'];

    function OrganizationContactAddressesController($scope, $rootScope, $stateParams, previousState, organization, OrganizationsService) {
        var vm = this;

        console.log($stateParams);
        vm.organizationId = $stateParams.id;
        vm.organization = organization;
        console.log(vm.organization);
        vm.previousState = previousState.name;
    }
})();