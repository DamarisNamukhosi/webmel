(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("OrganizationHoursController", OrganizationHoursController);

  OrganizationHoursController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "organization",
    "entity",
    "OrganizationsService"
  ];

  function OrganizationHoursController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    organization,
    entity,
    OrganizationsService
  ) {
    var vm = this;

    console.log($stateParams);
    vm.organizationId = $stateParams.id;
    vm.organization = organization;
    vm.entity = entity;
    console.log("Operating Hours Controller");
    console.log(vm.organization);
    vm.previousState = previousState.name;
    vm.displayOperatingHours = true;

    if (vm.organization.length === 0) {
      vm.displayOperatingHours = false;
    }
  }
})();
