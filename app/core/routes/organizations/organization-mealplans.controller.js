(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "OrganizationMealPlanController",
      OrganizationMealPlanController
    );

  OrganizationMealPlanController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "organization",
    "entity",
    "OrganizationsService"
  ];

  function OrganizationMealPlanController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    organization,
    entity,
    OrganizationsService
  ) {
    var vm = this;

    vm.organizationId = $stateParams.id;
    vm.organizations = organization;
    vm.entity = entity;
    console.log("org meal planss...");
    console.log(vm.organization);
    vm.previousState = previousState.name;
    vm.displayMealPlans = true;

    if (vm.organizations.length === 0) {
      vm.displayMealPlans = false;
    }
  }
})();
