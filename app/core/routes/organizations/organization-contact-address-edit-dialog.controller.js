(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationContactEditDialogController', OrganizationContactEditDialogController);

    OrganizationContactEditDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OrganizationsService', 'OrganizationTypesService'];

    function OrganizationContactEditDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, OrganizationsService, OrganizationTypesService) {
        var vm = this;

        vm.contact = entity;
        vm.clear = clear;
        vm.save = save;

        vm.organizationId = $stateParams.id;
        console.log(vm.contact);

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.contact.id !== null) {
                OrganizationsService.updateContactAddress(vm.contact, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
