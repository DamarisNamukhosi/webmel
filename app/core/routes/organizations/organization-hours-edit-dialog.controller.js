(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationHoursEditDialogController', OrganizationHoursEditDialogController);

    OrganizationHoursEditDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OrganizationsService', 'OrganizationTypesService'];

    function OrganizationHoursEditDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, OrganizationsService, OrganizationTypesService) {
        var vm = this;

        vm.contact = entity;
        vm.clear = clear;
        vm.save = save;

        vm.organizationId = $stateParams.id;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.contact.id !== null) {
                OrganizationsService.updateHours(vm.contact, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
