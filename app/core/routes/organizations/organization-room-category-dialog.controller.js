(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationRoomCategoryDialogController', OrganizationRoomCategoryDialogController);

    OrganizationRoomCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OrganizationsService', 'OrganizationTypesService'];

    function OrganizationRoomCategoryDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, OrganizationsService, OrganizationTypesService) {
        var vm = this;

        vm.roomCategory = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.roomCategory.id !== null) {
                OrganizationsService.updateRoomCategory(vm.roomCategory, onSaveSuccess, onSaveError);
            } else {
                OrganizationsService.createRoomCategory(vm.roomCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
