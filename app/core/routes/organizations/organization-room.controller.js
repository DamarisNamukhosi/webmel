(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationRoomController', OrganizationRoomController);

    OrganizationRoomController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'organization', 'OrganizationsService'];

    function OrganizationRoomController($scope, $rootScope, $stateParams, previousState, organization, OrganizationsService) {
        var vm = this;

        vm.organizationId = $stateParams.id;
        vm.catId = $stateParams.catId;
        vm.room = organization;
        console.log(vm.room);
        vm.previousState = previousState.name;
    }
})();