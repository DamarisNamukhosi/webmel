(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenusController', MenusController);

    MenusController.$inject = ['$scope', '$location', '$state', 'menus', 'URLS' ];

  function MenusController($scope, $location, $state, menus, URLS ) {
        var vm = this;
        vm.account = null;



        vm.isAuthenticated = null;
        vm.menus = menus;
        console.log( vm.menus );
        vm.menuNotes = vm.menus;

        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound = (vm.menus === undefined || vm.menus.length === 0);

        // console.log(vm.menus.notes.split("***"));
        // console.log(vm.menuNotes);
    }
})();
