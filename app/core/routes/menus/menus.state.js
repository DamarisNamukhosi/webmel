(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("menus", {
        parent: "app",
        url: "/menus",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menus/menus.html",
            controller: "MenusController",
            controllerAs: "vm"
          }
        },
        resolve: {
          menus: [
            "$stateParams",
            "$localStorage",
            "MenusService",
            function($stateParams, $localStorage, MenusService) {
              return MenusService.getMenusByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("menus.new", {
        parent: "menus",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "facilities.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/menu-dialog.html",
                controller: "MenuDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      order: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      parentId: null,
                      parentName: null,
                      generalMenuTypeId: null,
                      generalMenuTypeName: null
                    };
                  },
                  editParentMenu: function() {
                    return true;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menus", null, { reload: "menus" });
                },
                function() {
                  $state.go("menus");
                }
              );
          }
        ]
      })
      .state("menu-detail", {
        parent: "menus",
        url: "/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.menu.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menus/menu-detail.html",
            controller: "MenuDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.get({ id: $stateParams.id }).$promise;
            }
          ],
          menus: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.getMenusByParentId({ id: $stateParams.id })
                .$promise;
            }
          ],
          menuItems: [
            "$stateParams",
            "MenuItemsService",
            function($stateParams, MenuItemsService) {
              return MenuItemsService.getMenuItemsByMenu({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "menus",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-detail.image", {
        parent: "menu-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/image-upload-dialog.html",
                controller: "MenuImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  menu: [
                    "$stateParams",
                    "MenusService",
                    function($stateParams, MenusService) {
                      return MenusService.get({ id: $stateParams.id }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-detail", null, { reload: "menu-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-detail.submenus", {
        parent: "menus",
        url: "/{id}/submenus",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.menu.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menus/menu-submenus.html",
            controller: "MenuSubMenusController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.get({ id: $stateParams.id }).$promise;
            }
          ],
          menus: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.getMenusByParentId({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "menu-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-detail.edit", {
        parent: "menu-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/menu-dialog.html",
                controller: "MenuDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenusService",
                    function(MenusService) {
                      return MenusService.get({ id: $stateParams.id }).$promise;
                    }
                  ],
                  editParentMenu: function() {
                    return true;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menu-detail", null, { reload: "menu-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-detail-new-menu", {
        parent: "menu-detail.submenus",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/menu-dialog.html",
                controller: "MenuDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      order: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      parentId: $stateParams.id,
                      parentName: null,
                      generalMenuTypeId: null,
                      generalMenuTypeName: null
                    };
                  },
                  editParentMenu: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menu-detail.submenus", null, {
                    reload: "menu-detail.submenus"
                  });
                },
                function() {
                  $state.go("menu-detail.submenus");
                }
              );
          }
        ]
      })
      .state("menu-detail-new-menu-item", {
        parent: "menu-detail",
        url: "/menu-item/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menu-items/menu-item-dialog.html",
                controller: "MenuItemDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenuItemsService",
                    function(MenusService) {
                      return {
                        id: null,
                        uuid: null,
                        name: null,
                        brief: null,
                        notes: null,
                        description: null,
                        order: null,
                        contentStatus: "DRAFT",
                        statusReason: null,
                        menuId: $stateParams.id,
                        menuName: null,
                        generalMenuItemTypeId: null,
                        generalMenuItemTypeName: null,
                        servingOptionsGroupId: null,
                        servingOptionsGroupName: null
                      };
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-detail", null, { reload: "menu-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menus.edit", {
        parent: "menus",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/menu-dialog.html",
                controller: "MenuDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "MenusService",
                    function(MenusService) {
                      return MenusService.get({ id: $stateParams.id }).$promise;
                    }
                  ],
                  editParentMenu: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menus", null, { reload: "menus" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-detail.delete", {
        parent: "menu-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menus/menu-delete-dialog.html",
                controller: "MenuDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "MenusService",
                    function(MenusService) {
                      return MenusService.get({ id: $stateParams.id }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menus", null, { reload: "menus" });
                },
                function() {
                  $state.go("menus");
                }
              );
          }
        ]
      })
      .state("menu-galleries", {
        parent: "app",
        url: "/menus/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menus/menu-galleries.html",
            controller: "MenuGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-galleries.new", {
        parent: "menu-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenusService",
                    function(MenusService) {
                      return MenusService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menu-galleries", null, {
                    reload: "menu-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-galleries.delete", {
        parent: "menu-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("menu-galleries", null, {
                    reload: "menu-galleries"
                  });
                },
                function() {
                  $state.go("menu-galleries");
                }
              );
          }
        ]
      })
      .state("menu-album-detail", {
        parent: "menu-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menus/menu-album-detail.html",
            controller: "MenuAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenusService",
            function($stateParams, MenusService) {
              return MenusService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "menu-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-album-detail.edit", {
        parent: "menu-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "menu-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService",function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("menu-album-detail", null, { reload: "menu-album-detail" });
              },
              function () {
                $state.go("menu-album-detail");
              }
              );
          }
        ]
      })
      .state("menu-album-detail.makeCoverImage", {
        parent: "menu-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-album-detail", null, {
                    reload: "menu-album-detail"
                  });
                },
                function() {
                  $state.go("menu-album-detail");
                }
              );
          }
        ]
      })
      .state("menu-album-detail.deleteImage", {
        parent: "menu-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-album-detail", null, {
                    reload: "menu-album-detail"
                  });
                },
                function() {
                  $state.go("menu-album-detail");
                }
              );
          }
        ]
      })
      .state("menu-album-detail.upload", {
        parent: "menu-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-album-detail", null, {
                    reload: "menu-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-detail.addLabels", {
        parent: "menu-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "MenusService",
                    function (MenusService) {
                      return MenusService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("menu-detail", null, {
                  reload: "menu-detail"
                });
              },
              function () {
                $state.go("menu-detail");
              });
          }
        ]
      })
       .state("menu-detail.deleteLabel", {
                parent: "menu-detail",
               url: "/{itemToDelete}/delete-label",

                data: {
                  requiresAuthentication: true,
                  authorities: []
                },
                onEnter: [
                  "$stateParams",
                  "$state",
                  "$uibModal",
                  function ($stateParams, $state, $uibModal) {
                    $uibModal
                      .open({
                        templateUrl:
                          "core/routes/labels/object-label-delete-dialog.html",
                        controller: "ObjectLabelGroupDeleteDialogController",
                        controllerAs: "vm",
                        backdrop: "static",
                        size: "md"
                      })
                      .result.then(
                      function () {
                        $state.go("menu-detail", null, {
                          reload: "menu-detail"
                        });
                      },
                      function () {
                        $state.go("menu-detail");
                      }
            );
          }
        ]
      });
  }
})();
