(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuSubMenusController', MenuSubMenusController);

    MenuSubMenusController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'menus'];

    function MenuSubMenusController($scope, $rootScope, $stateParams,previousState, entity, menus) {
        var vm = this;

        vm.menu = entity;
        vm.menus = menus;

        vm.previousState = previousState.name;

        vm.menusNotFound = (vm.menus === undefined || vm.menus.length === 0);

    }
})();
