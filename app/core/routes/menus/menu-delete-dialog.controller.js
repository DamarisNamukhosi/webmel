(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuDeleteDialogController',MenuDeleteDialogController);

        MenuDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'MenusService'];

    function MenuDeleteDialogController($uibModalInstance, entity, MenusService) {
        var vm = this;

        vm.menu = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (menu) {
          menu.contentStatus = "DELETED";
            MenusService.update(menu,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
