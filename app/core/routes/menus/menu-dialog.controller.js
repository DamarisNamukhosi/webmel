
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuDialogController', MenuDialogController);

        MenuDialogController.$inject = ['$timeout', '$scope', '$stateParams','$localStorage', '$uibModalInstance', 'entity', 'editParentMenu', 'MenusService', 'MenuTypesService'];

    function MenuDialogController ($timeout, $scope, $stateParams,$localStorage, $uibModalInstance, entity, editParentMenu, MenusService, MenuTypesService) {
        var vm = this;

        vm.menu = entity;
        vm.clear = clear;
        vm.save = save;
        vm.editParentMenu = editParentMenu;
        
        vm.menus = MenusService.getMenusByOrganizationId({id: $localStorage.current_organisation.id});
        vm.menuTypes = MenuTypesService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.menu.id !== null) {
                MenusService.update(vm.menu, onSaveSuccess, onSaveError);
            } else {
                MenusService.create(vm.menu, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
