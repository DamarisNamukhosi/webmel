(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('MenusService', MenusService);

  MenusService.$inject = ['$resource', '$localStorage', 'URLS'];

    function MenusService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
            'getMenusByOrganizationId': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/menus/filter-by-organisation/:id'
            },
            'getMenusByParentId': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/menus/filter-by-parent-menu/:id'
            },
            'get': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                },
                isArray: false,
                url: URLS.BASE_URL + 'contentservice/api/menus/:id'
            }, 
            'update': {
                method: 'PUT',
                headers : {
                'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menus'
            }, 
            'create': {
                method: 'POST',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menus'
            },
            'delete': {
                method: 'DELETE',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menus/:id'
            }
        });
    }
})();
