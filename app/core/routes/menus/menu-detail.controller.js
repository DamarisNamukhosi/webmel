(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuDetailController', MenuDetailController);

    MenuDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'menus', 'menuItems', '$localStorage', 'ObjectLabelsService', 'URLS'];

    function MenuDetailController($scope, $rootScope, $stateParams, previousState, entity, menus, menuItems, $localStorage, ObjectLabelsService, URLS) {
        var vm = this;

        vm.menu = entity;


        vm.menus = menus;
  
        vm.menuNotes = vm.menu.notes.split("nl");
        console.log(vm.menuNotes);

        vm.menuItems = menuItems;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.menu.uuid;

        vm.previousState = previousState.name;

        vm.menusNotFound = (vm.menus === undefined || vm.menus.length === 0);
        vm.menuItemsNotFound = (vm.menuItems === undefined || vm.menuItems.length === 0);

        vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.menu.uuid });
        console.log('object labels...');
        console.log(vm.objectLabels);

    }
})();
