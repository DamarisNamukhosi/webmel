(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('SpecialDaysDialogController', SpecialDaysDialogController);

    SpecialDaysDialogController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        '$stateParams',
        '$uibModalInstance',
        'SpecialDaysService',
        'SeasonBandsService',
        'entity'
    ];

    function SpecialDaysDialogController(
        $scope,
        $location,
        $state,
        $localStorage,
        $stateParams,
        $uibModalInstance,
        SpecialDaysService,
        SeasonBandsService,
        entity
    ) {
        var vm = this;

        //injected values
        vm.entity = entity;

        //assign functions to vm
        vm.account = $localStorage.account;
        vm.addBand = addBand;
        vm.removeBand = removeBand;
        vm.clear = clear;
        vm.save = save;

        //initiliaze data
        vm.tag = 0;
        vm.years = [];
        vm.singleBand = {
            contentStatus: 'DRAFT',
            fromDate: null,
            id: null,
            seasonId: null,
            seasonName: null,
            specialDayGroupId: null,
            specialDayGroupName: null,
            toDate: null,
            uuid: null,
            openedFrom: false,
            openedTo: false,
            tag: 1
        };

        vm.dateOptions = {
            maxDate: new Date(2050, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        //init functions
        initYears();
        initDatePickers();

        function initYears() {
            var currentYear = new Date().getFullYear();
            var totalYears = 10; //number of years visible
            var numberOfYearsBack = 3; //how many years back
            for (var i = 1; i <= totalYears; i++) {
                var year = currentYear + i - numberOfYearsBack;
                vm.years.push(year);
            }
        }

       

        vm.openFrom = function () {
            vm.openedFrom = true;
          };
      
          vm.openTo = function () {
            vm.openedTo = true;
          };
      

        function addBand() {
            vm.entity.bands.push({
                contentStatus: 'DRAFT',
                fromDate: null,
                id: null,
                seasonId: null,
                seasonName: null,
                specialDayGroupId: null,
                specialDayGroupName: null,
                toDate: null,
                uuid: null,
                openedFrom: false,
                openedTo: false,
                tag: vm.entity.bands.length + 1
            });
        }

        function removeBand(id, $index) {
            console.log($index);
            if (vm.entity.id !== null) {

                SeasonBandsService.delete({ id: id }, function (result) {
                    vm.entity.bands.splice($index, 1);
                });
            } else {
                vm.entity.bands.splice($index, 1);
            }
        }

        function formatDateBands() {
            angular.forEach(vm.entity.bands, function (band) {
                //take the from time and the to time and format them
                var start = null;
                var end = null;
                start = moment(band.fromDate).format('YYYY-MM-DD');
                end = moment(band.toDate).format('YYYY-MM-DD');
                band.fromDate = start;
                band.toDate = end;

                //delete keys
                delete band.openedFrom;
                delete band.openedTo;
                delete band.tag;
            });
        }

        function initDatePickers() {
            console.log("vm.entity.bands");
            console.log(vm.entity);
            console.log(vm.entity.bands);
            // if (vm.entity.bands.length != 0) {
            //     angular.forEach(vm.entity.bands, function (band) {
            //         band.fromDate = new Date(band.fromDate);
            //         band.toDate = new Date(band.toDate);
            //     });
            // }
            if (vm.entity.id == null || vm.entity.id == undefined) {
                angular.forEach(vm.entity.bands, function (band) {
                    band.fromDate = new Date();
                    band.toDate = new Date();
                });
                // vm.entity.startDate = new Date();
                // vm.entity.startDate.setFullYear(vm.entity.startDate.getFullYear() + 1);
                // vm.entity.endDate = new Date();
                // vm.entity.endDate.setFullYear(vm.entity.endDate.getFullYear() + 2);
                // vm.entity.year = new Date().getFullYear() + 1;
              } 
        }

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            formatDateBands();
            var finalEntity = [];
            finalEntity.push(vm.entity);
            if (vm.entity.id == null) {
                SpecialDaysService.createList(
                    finalEntity,
                    onSaveSuccess,
                    onSaveError
                );
            } else {
                SpecialDaysService.updateList(
                    finalEntity,
                    onSaveSuccess,
                    onSaveError
                );
            }
        }

        function onSaveSuccess(result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveError() {
            vm.isSaving = false;
        }
    }
})();
