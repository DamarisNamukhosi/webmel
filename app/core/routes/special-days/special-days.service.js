(function() {
    'use strict';
    angular.module('webApp').factory('SpecialDaysService', SpecialDaysService);

    SpecialDaysService.$inject = ['$resource', '$localStorage', 'URLS'];

    function SpecialDaysService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(
            resourceUrl,
            {},
            {
                query: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url: URLS.BASE_URL + 'contentservice/api/special-day-groups'
                },
                get: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/:id'
                },
                getFull: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/get-full/:id'
                },
                getByOrganization: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/filter-by-organization/:id?year=:year'
                },
                update: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'contentservice/api/special-day-groups'
                },
                createList: {
                    method: 'POST',
                    isArray: true,
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/create-list'
                },
                updateList: {
                    method: 'PUT',
                    isArray: true,
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/update-list'
                },
                deleteFull: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/delete-full/:id'
                },
                delete: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/special-day-groups/:id'
                }
            }
        );
    }
})();
