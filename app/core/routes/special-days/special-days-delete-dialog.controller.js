(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'SpecialDaysDeleteDialogController',
            SpecialDaysDeleteDialogController
        );

    SpecialDaysDeleteDialogController.$inject = [
        '$uibModalInstance',
        '$stateParams',
        '$localStorage',
        'SpecialDaysService'
    ];

    function SpecialDaysDeleteDialogController(
        $uibModalInstance,
        $stateParams,
        $localStorage,
        SpecialDaysService
    ) {
        var vm = this;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete() {
            SpecialDaysService.deleteFull({ id: $stateParams.id }, function() {
                $uibModalInstance.close(true);
            });
        }
    }
})();
