(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('SpecialDaysController', SpecialDaysController);

    SpecialDaysController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        'entities',
        'SpecialDaysService',
        'SeasonBandsService'
    ];

    function SpecialDaysController(
        $scope,
        $location,
        $state,
        $localStorage,
        entities,
        SpecialDaysService,
        SeasonBandsService
    ) {
        var vm = this;

        vm.entities = entities;

        vm.search = search;
        vm.deleteBand = deleteBand;

        vm.account = $localStorage.current_organisation;
        vm.years = [];

        //init functions
        initYears();

        function initYears() {
            var currentYear = new Date().getFullYear();
            var totalYears = 5; //number of years visible
            var numberOfYearsBack = 3; //how many years back
            for (var i = 1; i <= totalYears; i++) {
                var year = currentYear + i - numberOfYearsBack;
                vm.years.push(year);
            }
        }

        function search() {
            var year = vm.year;
            //search
            SpecialDaysService.getByOrganization(
                {
                    id: $localStorage.current_organisation.id,
                    year: year
                },
                function(result) {
                    vm.entities = result;
                }
            );
        }

        function deleteBand(bandId, entity, $index) {
            //deleteBand
            SeasonBandsService.delete(
                {
                    id: bandId
                },
                function(result) {
                    entity.bands.splice($index, 1);
                }
            );
        }
    }
})();
