(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "AlbumCreateDialogController",
      AlbumCreateDialogController
    );

  AlbumCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "Upload",
    "$localStorage",
    "album",
    "URLS",
    "AlbumService"
  ];

  function AlbumCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    Upload,
    $localStorage,
    album,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.image_response = null;
    vm.isUploading = false;
    vm.album = album;
    vm.images = [];
    vm.files = [];
    vm.mediaItemDTOs = [];

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      angular.forEach(vm.files, function(file) {
        console.log(file);

        var temp=

        {
          albumId: null,
          albumName: null,
          caption: file.caption,
          filename: file.result.fileName,
          id: null,
          mediaType: "IMAGE",
          mimeType: file.result.contentType,
          name: file.result.originalFileName,
          uuid: null
        }

        vm.mediaItemDTOs.push(temp);

        });

      var myAlbum = {
        "id": null,
        "albumType": "GENERAL",
        "caption": vm.album.caption,
        "coverName": null,
        "coverUuid": null,
        "defaultAlbum": false,
        "mediaType": "IMAGE",
        "name": vm.album.name,
        "objectUuid": vm.entity.uuid,
        "mediaItemDTOList": vm.mediaItemDTOs
      }

      console.log("album");
      console.log(myAlbum);
      AlbumService.createAlbumFull(myAlbum, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.remove = function (index) {
      vm.files.splice(index, 1);
    }

    vm.upload = function (files, errFiles) {
      vm.isUploading = true;

      vm.errFile = errFiles && errFiles[0];
      angular.forEach(files, function(file) {
        console.log(file);
        vm.files.push(file);
        file.upload = Upload.upload({
          url: URLS.MEDIA_ITEM_UPLOAD,
          data: {
            file: file
          },
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          }
        });

        file.upload.then(
          function (response) {
            vm.images.push(response.data);
            console.log(vm.images);
            vm.image_response = response.data;
            console.log("response data")
            console.log(vm.image_response);
            $timeout(function () {
              file.result = response.data;
            });
          },
          function (response) {

            if (response.status > 0)
              vm.errorMsg = response.status + ": " + response.data;
          },
          function (evt) {
            file.progress = Math.min(
              100,
              parseInt(100.0 * evt.loaded / evt.total)
            );
          }
        );
    });
      vm.isUploading = false;
    };
  }
})();
