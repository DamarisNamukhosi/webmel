(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "AlbumChangeCoverImageDialogController",
      AlbumChangeCoverImageDialogController
    );

  AlbumChangeCoverImageDialogController.$inject = [
    "$uibModalInstance",
    "entity",
    "$localStorage",
    "AlbumService",
    "$stateParams"
  ];

  function AlbumChangeCoverImageDialogController(
    $uibModalInstance,
    entity,
    $localStorage,
    AlbumService,
    $stateParams
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clear = clear;
    vm.confirm = confirm;
    vm.coverId = $stateParams.imageId;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirm() {
      vm.entity.coverId = vm.coverId;
      AlbumService.update(vm.entity, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
