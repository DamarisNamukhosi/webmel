(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "AlbumImageDeleteDialogController",
      AlbumImageDeleteDialogController
    );

  AlbumImageDeleteDialogController.$inject = [
    "$uibModalInstance",
    "$localStorage",
    "MediaItemsService",
    "$stateParams"
  ];

  function AlbumImageDeleteDialogController(
    $uibModalInstance,
    $localStorage,
    MediaItemsService,
    $stateParams
  ) {
    var vm = this;
    vm.clear = clear;
    vm.confirm = confirm;
    vm.imageId = $stateParams.imageId;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirm() {
      //delete image
      MediaItemsService.deleteFull({ id: vm.imageId }, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
