(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "AlbumDeleteDialogController",
      AlbumDeleteDialogController
    );

  AlbumDeleteDialogController.$inject = [
    "$uibModalInstance",
    "$localStorage",
    "AlbumService",
    "$stateParams"
  ];

  function AlbumDeleteDialogController(
    $uibModalInstance,
    $localStorage,
    AlbumService,
    $stateParams
  ) {
    var vm = this;
    vm.clear = clear;
    vm.confirm = confirm;
    vm.albumId = $stateParams.albumId;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirm() {
      //delete image
      AlbumService.deleteFull({ id: vm.albumId }, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
