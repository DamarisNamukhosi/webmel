(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "AlbumUploadDialogController",
      AlbumUploadDialogController
    );

  AlbumUploadDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "Upload",
    "$localStorage",
    "URLS",
    "AlbumService"
  ];

  function AlbumUploadDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    Upload,
    $localStorage,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.image_response = null;
    vm.isUploading = false;

    vm.files = [];
    vm.mediaItemDTOs = [];

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {

      angular.forEach(vm.files, function(file) {

        var temp= {
          albumId: vm.entity.id,
          albumName: null,
          caption: file.caption,
          filename: file.result.fileName,
          id: null,
          mediaType: "IMAGE",
          mimeType: file.result.contentType,
          name: file.result.originalFileName,
          uuid: null
        }

        vm.mediaItemDTOs.push(temp);

        });
      AlbumService.saveMultiple(vm.mediaItemDTOs, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.remove = function (index) {
      vm.files.splice(index, 1);
    }

    vm.upload = function (files, errFiles) {
      vm.isUploading = true;

      vm.errFile = errFiles && errFiles[0];
      angular.forEach(files, function(file) {
        console.log(file);
        vm.files.push(file);
        file.upload = Upload.upload({
          url: URLS.MEDIA_ITEM_UPLOAD,
          data: {
            file: file
          },
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          }
        });

        file.upload.then(
          function (response) {
            vm.image_response = response.data;
            $timeout(function () {
              file.result = response.data;
            });
          },
          function (response) {

            if (response.status > 0)
              vm.errorMsg = response.status + ": " + response.data;
          },
          function (evt) {
            file.progress = Math.min(
              100,
              parseInt(100.0 * evt.loaded / evt.total)
            );
          }
        );
    });
      vm.isUploading = false;
    };
  }
})();
