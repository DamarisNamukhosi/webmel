
(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('AlbumDialogController', AlbumDialogController);

    AlbumDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$uibModalInstance', 'entity', 'AlbumService'];

    function AlbumDialogController($timeout, $scope, $stateParams, $localStorage, $uibModalInstance, entity, AlbumService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.entity.id !== null) {
                AlbumService.update(vm.entity, onSaveSuccess, onSaveError);
            } else {
                AlbumService.create(vm.entity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
