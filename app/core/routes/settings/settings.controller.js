(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['$scope', '$location', '$state', '$localStorage', 'URLS', 'users'];

    function SettingsController ($scope, $location, $state, $localStorage,URLS, users) {
        var vm = this;

        vm.profile = $localStorage.account;
        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.hello = function () {
          alert("hello");
        }
        vm.records = users;
        console.log(vm.records);
        vm.recordsFound = vm.records.length > 0 ? true : false;
    }
})();
