(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('SettingsAccountController', SettingsAccountController);

    SettingsAccountController.$inject = ['$scope', '$location', '$state', '$localStorage'];

    function SettingsAccountController ($scope, $location, $state, $localStorage) {
        var vm = this;

        vm.profile = $localStorage.account;
    }
})();
