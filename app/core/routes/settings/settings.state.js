(function () {
  'use strict';

  angular
    .module('webApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider.state('settings', {
        parent: 'app',
        url: '/settings/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'settings'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/settings/settings.html',
            controller: 'SettingsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          users: [
            '$stateParams',
            '$localStorage',
            'OrganizationUsersService',
            function ($stateParams, $localStorage, OrganizationUsersService) {
              return OrganizationUsersService
                .getOrganizationUsers({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ]
        }
      }).state('account', {
        parent: 'settings',
        url: '/account',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Organization Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/settings/settings.html',
            controller: 'SettingsController',
            controllerAs: 'vm'
          }
        }
      }).state('settings.profile', {
        parent: 'settings',
        url: '/image/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/settings/image-upload/image-upload-dialog.html',
                controller: 'ImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings', null, {
                  reload: 'settings'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      }).state('settings-age-limits', {
        parent: 'app',
        url: '/settings/age-limits',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.rooms.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/age-limits/age-limits.html',
            controller: 'AgeLimitsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          records: [
            '$stateParams',
            '$localStorage',
            'AgeLimitsService',
            function ($stateParams, $localStorage, AgeLimitsService) {
              return AgeLimitsService
                .getAgeLimits({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      }).state('settings-age-limits.edit', {
        parent: 'settings-age-limits',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/age-limits/age-limit-dialog.html',
                controller: 'AgeLimitDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AgeLimitsService',
                    function (AgeLimitsService) {
                      return AgeLimitsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-age-limits', null, {
                  reload: 'settings-age-limits'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      }).state('settings-age-limits.delete', {
        parent: 'settings-age-limits',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/age-limits/age-limit-delete-dialog.html',
                controller: 'AgeLimitDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'AgeLimitsService',
                    function (AgeLimitsService) {
                      return AgeLimitsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-age-limits', null, {
                  reload: 'settings-age-limits'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      }).state('settings-age-limits.new', {
        parent: 'settings-age-limits',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'age-limits.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/age-limits/age-limit-dialog.html',
                controller: 'AgeLimitDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      minAge: null,
                      maxAge: null,
                      upperLimitType: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      generalAgeBracketId: null,
                      generalAgeBracketName: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-age-limits', null, {
                  reload: 'settings-age-limits'
                });
              }, function () {
                $state.go('settings-age-limits');
              });
          }
        ]
      }).state('settings-meal-plans', {
        parent: 'app',
        url: '/settings/meal-plans',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.rooms.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/meal-plans/meal-plans.html',
            controller: 'MealPlansController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          records: [
            '$stateParams',
            '$localStorage',
            'MealPlansService',
            function ($stateParams, $localStorage, MealPlansService) {
              return MealPlansService
                .getMealPlans({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-meal-plans.edit', {
        parent: 'settings-meal-plans',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-plans/meal-plans-dialog.html',
                controller: 'MealPlansDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'MealPlansService',
                    function (MealPlansService) {
                      return MealPlansService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-plans', null, {
                  reload: 'settings-meal-plans'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      }).state('settings-meal-plans.delete', {
        parent: 'settings-meal-plans',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-plans/meal-plans-delete-dialog.html',
                controller: 'MealPlansDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'MealPlansService',
                    function (MealPlansService) {
                      return MealPlansService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-plans', null, {
                  reload: 'settings-meal-plans'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      }).state('settings-meal-plans.new', {
        parent: 'settings-meal-plans',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-plans/meal-plans-dialog.html',
                controller: 'MealPlansDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      description: null,
                      generalMealPlanId: null,
                      generalMealPlanName: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-plans', null, {
                  reload: 'settings-meal-plans'
                });
              }, function () {
                $state.go('settings-meal-plans');
              });
          }
        ]
      }).state('settings-users', {
        parent: 'app',
        url: '/settings/settings',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/settings/settings.html',
            controller: 'OrganizationUsersController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          users: [
            '$stateParams',
            '$localStorage',
            'OrganizationUsersService',
            function ($stateParams, $localStorage, OrganizationUsersService) {
              return OrganizationUsersService
                .getOrganizationUsers({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ]
        }
      }).state('settings-users.new', {
        parent: 'settings',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/organization-users/organization-user-dialog.html',
                controller: 'OrganizationUserDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      emailAddress: null,
                      langKey: null,
                      fullName: null,
                      organisationId: $localStorage.current_organisation.id
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings', null, {
                  reload: 'settings'
                });
              }, function () {
                $state.go('settings');
              });
          }
        ]
      })
      .state('settings-account', {
        parent: 'settings',
        url: '/settings/auth',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/settings/password/change-password-dialog.html',
                controller: 'ChangePasswordDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  auth: function () {
                    return {
                      password: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings', null, {
                  reload: 'settings'
                });
              }, function () {
                $state.go('settings');
              });
          }
        ]
      })
      .state('settings-organization-relationships', {
        parent: 'app',
        url: '/settings/associations',
        data: {
          authorities: []
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/settings/organization-relationships/organization-relationships.html',
            controller: 'OrganizationRelationshipsController',
            controllerAs: 'vm'
          }
        }
      })
      .state('settings-organization-relationships.new', {
        parent: 'settings-organization-relationships',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/associations/association-dialog.html',
                controller: 'AssociationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      parentId: $localStorage.current_organisation.id,
                      parentName: null,
                      childId: null,
                      childName: null,
                      typeId: null,
                      typeName: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-organization-relationships', null, {
                  reload: 'settings-organization-relationships'
                });
              }, function () {
                $state.go('settings-organization-relationships');
              });
          }
        ]
      })
      .state('settings-organization-relationships.edit', {
        parent: 'settings-organization-relationships',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/associations/association-dialog.html',
                controller: 'AssociationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AssociationsService',
                    function (AssociationsService) {
                      return AssociationsService
                        .getAssociations({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-organization-relationships', null, {
                  reload: 'settings-organization-relationships'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-organization-relationships.delete', {
        parent: 'settings-organization-relationships',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/associations/association-delete-dialog.html',
                controller: 'AssociationDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'AssociationsService',
                    function (AssociationsService) {
                      return AssociationsService
                        .getAssociations({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-organization-relationships', null, {
                  reload: 'settings-organization-relationships'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-operating-hours', {
        parent: 'app',
        url: '/settings/operating-hrs',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'title!'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/operating-hours/operating-hours.html',
            controller: 'OperatingHoursController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          operatingHours: [
            '$stateParams',
            '$localStorage',
            'OperatingHoursService',
            function ($stateParams, $localStorage, OperatingHoursService) {
              return OperatingHoursService
                .getOperatingHoursByOrganizaitonId({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ]
        }
      })
      .state('settings-operating-hours.new', {
        parent: 'settings-operating-hours',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/operating-hours/operating-hour-dialog.html',
                controller: 'OperatingHourDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      openingTime: null,
                      operatingHoursType: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-operating-hours', null, {
                  reload: 'settings-operating-hours'
                });
              }, function () {
                $state.go('settings-operating-hours');
              });
          }
        ]
      })
      .state('settings-operating-hours.edit', {
        parent: 'settings-operating-hours',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/operating-hours/operating-hour-dialog.html',
                controller: 'OperatingHourDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'OperatingHoursService',
                    function (OperatingHoursService) {
                      return OperatingHoursService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-operating-hours', null, {
                  reload: 'settings-operating-hours'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-operating-hours.delete', {
        parent: 'settings-operating-hours',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/operating-hours/operating-hour-delete-dialog.html',
                controller: 'OperatingHourDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'OperatingHoursService',
                    function (OperatingHoursService) {
                      return OperatingHoursService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-operating-hours', null, {
                  reload: 'settings-operating-hours'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-documents', {
        parent: 'app',
        url: '/settings/documents',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.documents.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/documents-own/documents-own.html',
            controller: 'DocumentsOwnController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          documents: [
            '$stateParams',
            '$localStorage',
            'DocumentsOwnService',
            function ($stateParams, $localStorage, DocumentsOwnService) {
              return DocumentsOwnService
                .getDocumentsByOrganizationUuid({
                  uuid: $localStorage.current_organisation.uuid
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-documents-detail.edit', {
        parent: 'settings-documents-detail',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/documents-own/document-own-dialog.html',
                controller: 'DocumentOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'DocumentsOwnService',
                    function (DocumentsOwnService) {
                      return DocumentsOwnService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-documents-detail', null, {
                  reload: 'settings-documents-detail'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('document-own.profile', {
        parent: 'settings-documents-detail',
        url: '/image/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/documents-own/image-upload/image-upload-dialog.html',
                controller: 'DocumentOwnImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    '$stateParams',
                    'DocumentsOwnService',
                    function ($stateParams, DocumentsOwnService) {
                      return DocumentsOwnService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-documents-detail', null, {
                  reload: 'settings-documents-detail'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-documents.edit', {
        parent: 'settings-documents',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/documents-own/document-own-dialog.html',
                controller: 'DocumentOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'DocumentsOwnService',
                    function (DocumentsOwnService) {
                      return DocumentsOwnService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-documents', null, {
                  reload: 'settings-documents'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-documents-detail', {
        parent: 'settings-documents',
        url: '/document/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Documents Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/documents-own/document-own-detail.html',
            controller: 'DocumentOwnDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'DocumentsOwnService',
            function ($stateParams, DocumentsOwnService) {
              return DocumentsOwnService
                .get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'settings-documents',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-documents.delete', {
        parent: 'settings-documents',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/documents-own/document-own-delete-dialog.html',
                controller: 'DocumentOwnDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'DocumentsOwnService',
                    function (DocumentsOwnService) {
                      return DocumentsOwnService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-documents', null, {
                  reload: 'settings-documents'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-documents.new', {
        parent: 'settings-documents',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/documents-own/document-own-dialog.html',
                controller: 'DocumentOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      effectiveDate: null,
                      expiryDate: null,
                      documentStatus: 'VALID',
                      statusReason: 'new document',
                      objectId: $localStorage.current_organisation.uuid,
                      generalDocumentId: null,
                      generalDocumentName: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-documents', null, {
                  reload: 'settings-documents'
                });
              }, function () {
                $state.go('settings-documents');
              });
          }
        ]
      })
      .state('settings-markets', {
        parent: 'app',
        url: '/settings/markets',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Market Settings'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/markets/markets.html',
            controller: 'MarketsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'MarketsService',
            '$localStorage',
            function ($stateParams, MarketsService, $localStorage) {
              return MarketsService
                .getByOrganisation({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'settings',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-markets.new', {
        parent: 'settings-markets',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markets/market-create-dialog.html',
                controller: 'MarketCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      return {
                        contentStatus: 'DRAFT',
                        id: null,
                        default: false,
                        name: null,
                        organisationId: $localStorage.current_organisation.id,
                        organisationName: null,
                        uuid: null,
                        marketCountryDTOList: []
                      };
                    }
                  ],
                  countries: [
                    'CountriesService',
                    function (CountriesService) {
                      return CountriesService
                        .get()
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-markets', null, {
                  reload: 'settings-markets'
                });
              }, function () {
                $state.go('settings-markets');
              });
          }
        ]
      })
      .state('settings-markets.edit', {
        parent: 'settings-markets',
        url: '/{marketId}/editMarket',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markets/market-dialog.html',
                controller: 'MarketDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'MarketsService',
                    function (MarketsService) {
                      console.log($stateParams);
                      console.log($state.params);
                      console.log($state);
                      return MarketsService
                        .getById({
                          id: $stateParams.marketId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-markets', null, {
                  reload: 'settings-markets'
                });
              }, function () {
                $state.go('settings-markets');
              });
          }
        ]
      })
      .state('settings-markets.delete', {
        parent: 'settings-markets',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markets/market-delete-dialog.html',
                controller: 'MarketDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'MarketsService',
                    function ($localStorage, MarketsService) {
                      return MarketsService
                        .getById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-markets', null, {
                  reload: 'settings-markets'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-markets.deleteCountry', {
        parent: 'settings-markets',
        url: '/{id}/deleteCountry',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/markets/market-country-delete-dialog.html',
                controller: 'MarketCountryDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result
              .then(function () {
                $state.go('settings-markets', null, {
                  reload: 'settings-markets'
                });
              }, function () {
                $state.go('settings-markets');
              });
          }
        ]
      })
      .state('settings-markets.addCountry', {
        parent: 'settings-markets',
        url: '/{id}/country/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markets/market-country-dialog.html',
                controller: 'MarketCountryDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'MarketsService',
                    function ($localStorage, MarketsService) {
                      return MarketsService
                        .getFullById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  countries: [
                    'CountriesService',
                    function (CountriesService) {
                      return CountriesService
                        .get()
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-markets', null, {
                  reload: 'settings-markets'
                });
              }, function () {
                $state.go('settings-markets');
              });
          }
        ]
      })
      .state('settings-seasons', {
        parent: 'app',
        url: '/settings/seasons',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'title!'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/seasons/seasons.html',
            controller: 'SeasonsController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'year,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage($stateParams.page),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate($stateParams.sort),
                ascending: PaginationUtil.parseAscending($stateParams.sort),
                search: $stateParams.search
              };
            }
          ],
          seasons: [
            '$stateParams',
            '$localStorage',
            'SeasonGroupsService',
            function ($stateParams, $localStorage, SeasonGroupsService) {
              return SeasonGroupsService
                .getByOranizationId({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ]
        }
      })
      .state('settings-seasons.new', {
        parent: 'settings-seasons',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-create-dialog.html',
                controller: 'SeasonCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      contentStatus: 'DRAFT',
                      default: true,
                      description: null,
                      id: null,
                      name: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      seasons: [{
                        abbreviation: null,
                        contentStatus: 'DRAFT',
                        id: null,
                        name: null,
                        organisationId: null,
                        organisationName: null,
                        seasonBandDTOList: [{
                          contentStatus: 'DRAFT',
                          fromDate: null,
                          id: null,
                          seasonId: null,
                          seasonName: null,
                          specialDayGroupId: null,
                          specialDayGroupName: null,
                          toDate: null,
                          uuid: null,
                          tag: 1
                        }],
                        seasonGroupId: null,
                        seasonGroupName: null,
                        uuid: null,
                        tag: 1
                      }],
                      uuid: null,
                      year: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('settings-seasons');
              });
          }
        ]
      })
      .state('settings-seasons.editGroup', {
        parent: 'settings-seasons',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-create-dialog.html',
                controller: 'SeasonCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'SeasonGroupsService',
                    function (SeasonGroupsService) {
                      return SeasonGroupsService
                        .getFull({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('settings-seasons');
              });
          }
        ]
      })
      .state('settings-seasons.edit', {
        parent: 'settings-seasons',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'menu-item-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-dialog.html',
                controller: 'SeasonDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'SeasonsService',
                    function (SeasonsService) {
                      return SeasonsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('settings-seasons');
              });
          }
        ]
      })
      .state('settings-seasons.delete', {
        parent: 'settings-seasons',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-delete-dialog.html',
                controller: 'SeasonDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'SeasonsService',
                    function (SeasonsService) {
                      return SeasonsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-seasons.newBand', {
        parent: 'settings-seasons',
        url: '/{id}/new-band',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-band-dialog.html',
                controller: 'SeasonBandDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      contentStatus: 'DRAFT',
                      fromDate: null,
                      id: null,
                      seasonId: $stateParams.id,
                      toDate: null,
                      uuid: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('settings-seasons');
              });
          }
        ]
      })
      .state('settings-seasons.deleteBand', {
        parent: 'settings-seasons',
        url: '/{id}/delete-band',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-band-delete-dialog.html',
                controller: 'SeasonBandDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'SeasonBandsService',
                    function (SeasonBandsService) {
                      return SeasonBandsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-day-bands', {
        parent: 'app',
        url: '/settings/day-bands',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.days.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/day-bands/day-bands.html',
            controller: 'DayBandsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          dayBands: [
            '$stateParams',
            '$localStorage',
            'DayBandsService',
            function ($stateParams, $localStorage, DayBandsService) {
              return DayBandsService
                .getDayBandsByOrganizaitonId({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-day-bands.new', {
        parent: 'settings-day-bands',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/day-bands/day-band-create-dialog.html',
                controller: 'DayBandCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      dayDTOList: [],
                      id: null,
                      name: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      uuid: null
                    };
                  },
                  days: [
                    '$stateParams',
                    '$localStorage',
                    'DayBandsService',
                    function ($stateParams, $localStorage, DayBandsService) {
                      return DayBandsService
                        .getDayBandsByOrganizaitonId({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-day-bands', null, {
                  reload: 'settings-day-bands'
                });
              }, function () {
                $state.go('settings-day-bands');
              });
          }
        ]
      })
      .state('settings-day-bands.edit', {
        parent: 'settings-day-bands',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/day-bands/day-band-dialog.html',
                controller: 'DayBandDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'DayBandsService',
                    function (DayBandsService) {
                      return DayBandsService
                        .getFullBand({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-day-bands', null, {
                  reload: 'settings-day-bands'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-day-bands.delete', {
        parent: 'settings-day-bands',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/day-bands/day-band-delete-dialog.html',
                controller: 'DayBandDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'DayBandsService',
                    function (DayBandsService) {
                      return DayBandsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-day-bands', null, {
                  reload: 'settings-day-bands'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-day-bands.addDay', {
        parent: 'settings-day-bands',
        url: '/{bandId}/add-days',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/day-bands/days/day-label-create-dialog.html',
                controller: 'DayLabelCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'DayBandsService',
                    function (DayBandsService) {
                      return DayBandsService
                        .getFullBand({
                          id: $stateParams.bandId
                        })
                        .$promise;
                    }
                  ],
                  days: [
                    '$stateParams',
                    '$localStorage',
                    'DayBandsService',
                    function ($stateParams, $localStorage, DayBandsService) {
                      return DayBandsService
                        .getDayBandsByOrganizaitonId({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-day-bands', null, {
                  reload: 'settings-day-bands'
                });
              }, function () {
                $state.go('settings-day-bands');
              });
          }
        ]
      })
      .state('settings-day-bands.deleteDay', {
        parent: 'settings-day-bands',
        url: '/day/{dayId}/delete',

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/day-bands/days/day-label-delete-dialog.html',
                controller: 'DayLabelDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result
              .then(function () {
                $state.go('settings-day-bands', null, {
                  reload: 'settings-day-bands'
                });
              }, function () {
                $state.go('settings-day-bands');
              });
          }
        ]
      })
      .state('settings-services', {
        parent: 'app',
        url: '/settings/services',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.days.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/services/services.html',
            controller: 'ServicesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          records: [
            '$stateParams',
            '$localStorage',
            'ServicesService',
            function ($stateParams, $localStorage, ServicesService) {
              return ServicesService
                .getByOrganizationId({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-services.new', {
        parent: 'settings-services',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/services/service-dialog.html',
                controller: 'ServiceDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: 'DRAFT',
                      description: null,
                      generalServiceId: null,
                      generalServiceName: null,
                      id: null,
                      name: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      uuid: null
                    };
                  },
                  generalServices: [
                    '$stateParams',
                    '$localStorage',
                    'GeneralServicesService',
                    function ($stateParams, $localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .getByOrganizationTypeId({
                          id: $localStorage.current_organisation.organisationTypeId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-services', null, {
                  reload: 'settings-services'
                });
              }, function () {
                $state.go('settings-services');
              });
          }
        ]
      })
      .state('settings-services.edit', {
        parent: 'settings-services',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/services/service-dialog.html',
                controller: 'ServiceDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'ServicesService',
                    function (ServicesService) {
                      return ServicesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  generalServices: [
                    '$stateParams',
                    '$localStorage',
                    'GeneralServicesService',
                    function ($stateParams, $localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .getByOrganizationTypeId({
                          id: $localStorage.current_organisation.organisationTypeId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-services', null, {
                  reload: 'settings-services'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-services.delete', {
        parent: 'settings-services',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/services/service-delete-dialog.html',
                controller: 'ServiceDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'ServicesService',
                    function (ServicesService) {
                      return ServicesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-services', null, {
                  reload: 'settings-services'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-policies', {
        parent: 'app',
        url: '/settings/policies',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.days.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/policies/policies.html',
            controller: 'PoliciesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          policies: [
            '$stateParams',
            '$state',
            '$localStorage',
            'PoliciesService',
            function ($stateParams, $state, $localStorage, PoliciesService) {
              return PoliciesService
                .getByOrganization({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          roomOccupancies: [
            'RoomOccupanciesService',
            '$localStorage',
            function (RoomOccupanciesService, $localStorage) {
              return RoomOccupanciesService
                .getByOrganization({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-policies.new', {
        parent: 'settings-policies',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/policies/policy-create-dialog.html',
                // templateUrl: 'core/routes/policies/policy-dialog.html',
                // controller: 'PolicyDialogController',
                controller: 'PolicyCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  policies: [
                    '$localStorage',
                    function ($localStorage) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return {
                        "id": null,
                        "name": null,
                        "policyType": "ROOM_OCCUPANCY",
                        "uuid": null,
                        "isDefault": false,
                        "year": "2019",
                        "contentStatus": "DRAFT",
                        "organisationId": $localStorage.current_organisation.id,
                        "organisationName": null
                      }
                    }
                  ],
                  roomOccupancies: [
                    'RoomOccupanciesService',
                    '$localStorage',
                    function (RoomOccupanciesService, $localStorage) {
                      return RoomOccupanciesService
                        .getByOrganization({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('settings-policies');
              });
          }
        ]
      })
      .state('settings-policies.createRules', {
        parent: 'settings-policies',
        url: '/{occupancyPolicyId}/createRules',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                // templateUrl: 'core/routes/policies/policy-dialog.html',
                // controller: 'PolicyDialogController',
                templateUrl: 'core/routes/policies/rules/rule-create-dialog.html',
                controller: 'RuleCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  rule: [
                    '$localStorage',
                    'PoliciesService',
                    function ($localStorage, PoliciesService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return {
                        "id": null,
                        "description": null,
                        "contentStatus": "DRAFT",
                        "childPattern": null,
                        "childBaseRateId": null,
                        "childBaseRateName": null,
                        "childBaseRate": null,
                        "childRate": null,
                        "childRateType": null,
                        "adultBaseRateId": null,
                        "adultBaseRateName": null,
                        "adultPattern": null,
                        "adultBaseRate": null,
                        "adultRate": null,
                        "adultRateType": null,
                        "occupancyPolicyId": $stateParams.occupancyPolicyId,
                        "organisationId": $localStorage.current_organisation.id,
                        "youngAdultBaseRateId": null,
                        "youngAdultBaseRateName": null,
                        "youngAdultPattern": null,
                        "youngAdultRate": null,
                        "youngAdultRateType": null,
                        "infantBaseRateId": null,
                        "infantBaseRateName": null,
                        "infantPattern": null,
                        "infantRate": null,
                        "infantRateType": null,
                        "agedBaseRateId": null,
                        "agedBaseRateName": null,
                        "agedPattern": null,
                        "agedRate": null,
                        "agedRateType": null,
                        "allowed": true,
                        "occupantsPattern": "[2-3]",
                        "global": false,
                        "occupantsBaseRateId": 0,
                        "occupantsBaseRateName": "string",
                        "occupantsRate": 0,
                        "occupantsRateType": "PER_ROOM",

                      };
                    }
                  ],
                  roomOccupancies: [
                    'RoomOccupanciesService',
                    '$localStorage',
                    function (RoomOccupanciesService, $localStorage) {
                      return RoomOccupanciesService
                        .getByOrganization({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ],
                  organisationAgeLimits: null
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('settings-policies');
              });
          }
        ]
      })
      .state('settings-policies.edit', {
        parent: 'settings-policies',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                // templateUrl: 'core/routes/policies/policy-dialog.html',
                // controller: 'PolicyDialogController',
                templateUrl: 'core/routes/policies/policy-create-dialog.html',
                controller: 'PolicyCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  policies: [
                    'PoliciesService',
                    function (PoliciesService) {
                      return PoliciesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  roomOccupancies: [
                    'RoomOccupanciesService',
                    '$localStorage',
                    function (RoomOccupanciesService, $localStorage) {
                      return RoomOccupanciesService
                        .getByOrganization({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('settings-policies');
              });
          }
        ]
      })
      .state('settings-policies.editRules', {
        parent: 'settings-policies',
        url: '/{ruleId}/editRules',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                // templateUrl: 'core/routes/policies/policy-dialog.html',
                // controller: 'PolicyDialogController',
                templateUrl: 'core/routes/policies/rules/rule-create-dialog.html',
                controller: 'RuleCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  rule: [
                    'PoliciesService',
                    function (PoliciesService) {
                      return PoliciesService
                        .getRule({
                          id: $stateParams.ruleId
                        })
                        .$promise;
                    }
                  ],
                  roomOccupancies: [
                    'RoomOccupanciesService',
                    '$localStorage',
                    function (RoomOccupanciesService, $localStorage) {
                      return RoomOccupanciesService
                        .getByOrganization({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ],
                  organisationAgeLimits: [
                    'AgeLimitsService',
                    '$localStorage',
                    function (AgeLimitsService, $localStorage) {
                      return AgeLimitsService.getAgeLimits({
                        id: $localStorage.current_organisation.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('settings-policies');
              });
          }
        ]
      })
      .state('settings-policies.viewRules', {
        parent: 'settings-policies',
        url: '/{ruleId}/viewRules',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                // templateUrl: 'core/routes/policies/policy-dialog.html',
                // controller: 'PolicyDialogController',
                templateUrl: 'core/routes/policies/rules/rule-create-dialog.html',
                controller: 'RuleCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  rule: [
                    'PoliciesService',
                    function (PoliciesService) {
                      return PoliciesService
                        .getRule({
                          id: $stateParams.ruleId
                        })
                        .$promise;
                    }
                  ],
                  roomOccupancies: [
                    'RoomOccupanciesService',
                    '$localStorage',
                    function (RoomOccupanciesService, $localStorage) {
                      return RoomOccupanciesService
                        .getByOrganization({
                          id: $localStorage.current_organisation.id
                        })
                        .$promise;
                    }
                  ],
                  organisationAgeLimits: [
                    'AgeLimitsService',
                    '$localStorage',
                    function (AgeLimitsService, $localStorage) {
                      return AgeLimitsService.getAgeLimits({
                        id: $localStorage.current_organisation.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('settings-policies');
              });
          }
        ]
      })
      .state('settings-policies.delete', {
        parent: 'settings-policies',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/policies/policy-delete-dialog.html',
                controller: 'PolicyDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'PoliciesService',
                    function ($localStorage, PoliciesService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return PoliciesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-policies.deleteRules', {
        parent: 'settings-policies',
        url: '/{ruleId}/deleteRule',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/policies/policy-delete-dialog.html',
                controller: 'PolicyDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'PoliciesService',
                    function ($localStorage, PoliciesService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return PoliciesService.getRule({
                        id: $stateParams.ruleId
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state("settings-policies.addRoomCategory", {
        parent: "settings-policies",
        url: "/{occupancyPolicyId}/add-room",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/policies/rooms/room-category-create-dialog.html",
                controller: "RoomsWithOcuupancyCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  roomCategories: [
                    "$localStorage",
                    "RoomCategoriesService",
                    function ($localStorage, RoomCategoriesService) {
                      return RoomCategoriesService.getRoomCategoriesByOrganizationId({
                        id: $localStorage.current_organisation.id
                      }).$promise;
                    }
                  ]
                  // ,
                  // entity: [
                  //   "RoomCategoriesService",
                  //   function(RoomCategoriesService) {
                  //     return null;
                  //   }
                  // ],
                  // savedselectedRoomCategories: [
                  //   "ObjectFeaturesService",
                  //   function(ObjectFeaturesService) {
                  //     return null;
                  //   }
                  // ]
                }
              })
              .result.then(
                function () {
                  $state.go("settings-policies", null, {
                    reload: "settings-policies"
                  });
                },
                function () {
                  $state.go("settings-policies");
                }
              );
          }
        ]
      })
      .state("settings-policies.deleteRoomCategory", {
        parent: "settings-policies",
        url: "/{categoryToDelete}/delete-room",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/policies/rooms/room-category-update-dialog.html",
                controller: "RoomCategoryWithoutOccController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    '$stateParams',
                    'RoomCategoriesService',
                    function ($stateParams, RoomCategoriesService) {
                      return RoomCategoriesService.getById({
                        id: $stateParams.categoryToDelete
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("settings-policies", null, {
                    reload: "settings-policies"
                  });
                },
                function () {
                  $state.go("settings-policies");
                }
              );
          }
        ]
      })
      .state('settings-policies.copy', {
        parent: 'settings-policies',
        url: '/{occupancyPolicyId}/{currentOrg}/{parent}/copy',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/policies/policy-copy-dialog.html',
                controller: 'PolicyCopyDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'PoliciesService',
                    function ($localStorage, PoliciesService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return PoliciesService.get({
                        id: $stateParams.occupancyPolicyId
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-policies', null, {
                  reload: 'settings-policies'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings.gettingStarted', {
        parent: 'settings',
        url: '/getting-started',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/home/getting-started-dialog.html',
                controller: 'GettingStartedDialogController',
                controllerAs: 'vm',
                size: 'md'
              })
              .result
              .then(function () {
                $state.go('home', null, {
                  reload: 'home'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-special-days', {
        parent: 'app',
        url: '/settings/special-days',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.days.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/special-days/special-days.html',
            controller: 'SpecialDaysController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entities: [
            '$stateParams',
            '$state',
            '$localStorage',
            'SpecialDaysService',
            function ($stateParams, $state, $localStorage, SpecialDaysService) {
              return SpecialDaysService
                .getByOrganization({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-special-days.new', {
        parent: 'settings-special-days',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/special-days/special-days-dialog.html',
                controller: 'SpecialDaysDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      return {
                        id: null,
                        name: null,
                        description: null,
                        contentStatus: 'DRAFT',
                        uuid: null,
                        year: null,
                        organisationId: $localStorage.current_organisation.id,
                        organisationName: $localStorage.current_organisation.name,
                        bands: []
                      };
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-special-days', null, {
                  reload: 'settings-special-days'
                });
              }, function () {
                $state.go('settings-special-days');
              });
          }
        ]
      })
      .state('settings-special-days.edit', {
        parent: 'settings-special-days',
        url: '/{spId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/special-days/special-days-dialog.html',
                controller: 'SpecialDaysDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'SpecialDaysService',
                    function (SpecialDaysService) {
                      return SpecialDaysService
                        .getFull({
                          id: $stateParams.spId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-special-days', null, {
                  reload: 'settings-special-days'
                });
              }, function () {
                $state.go('settings-special-days');
              });
          }
        ]
      })
      .state('settings-special-days.delete', {
        parent: 'settings-special-days',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/special-days/special-days-delete-dialog.html',
                controller: 'SpecialDaysDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result
              .then(function () {
                $state.go('settings-special-days', null, {
                  reload: 'settings-special-days'
                });
              }, function () {
                $state.go('settings-special-days');
              });
          }
        ]
      })
      .state('settings-seasons.deleteGroup', {
        parent: 'settings-seasons',
        url: '/{id}/delete-group',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/seasons/season-group-delete-dialog.html',
                controller: 'SeasonGroupDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'SeasonGroupsService',
                    function (SeasonGroupsService) {
                      return SeasonGroupsService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-seasons', null, {
                  reload: 'settings-seasons'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state("settings-room-categories", {
        parent: "app",
        url: "/setting-room-categories",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/settings/room-categories/room-categories.html",
            controller: "RoomCategoriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          roomCategories: [
            "$stateParams",
            "$localStorage",
            "RoomCategoriesService",
            function ($stateParams, $localStorage, RoomCategoriesService) {
              return RoomCategoriesService.getRoomCategoriesByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('settings-meal-types', {
        parent: 'app',
        url: '/settings/meal-types',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.rooms.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/meal-types/meal-types.html',
            controller: 'MealTypesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          records: [
            '$stateParams',
            '$localStorage',
            'MealTypesService',
            function ($stateParams, $localStorage, MealTypesService) {
              return MealTypesService
                .getMealTypes({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-meal-types.new', {
        parent: 'settings-meal-types',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-types/meal-types-dialog.html',
                controller: 'MealTypesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      description: null,
                      generalMealTypeId: null,
                      generalMealTypeName: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      contentStatus: "DRAFT",
                      order: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-types', null, {
                  reload: 'settings-meal-types'
                });
              }, function () {
                $state.go('settings-meal-types');
              });
          }
        ]
      })
      .state('settings-meal-types.edit', {
        parent: 'settings-meal-types',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-types/meal-types-dialog.html',
                controller: 'MealTypesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'MealTypesService',
                    function (MealTypesService) {
                      return MealTypesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-types', null, {
                  reload: 'settings-meal-types'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-meal-types.delete', {
        parent: 'settings-meal-types',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/meal-types/meal-types-delete-dialog.html',
                controller: 'MealTypesDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'MealTypesService',
                    function (MealTypesService) {
                      return MealTypesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-meal-types', null, {
                  reload: 'settings-meal-types'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-menu-categories', {
        parent: 'app',
        url: '/settings/menu-categories',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'webApp.rooms.detail.title'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/menu-categories/menu-categories.html',
            controller: 'MenuCategoriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          records: [
            '$stateParams',
            '$localStorage',
            'MenuCategoriesService',
            function ($stateParams, $localStorage, MenuCategoriesService) {
              return MenuCategoriesService
                .getMenuCategories({
                  id: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'app',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('settings-menu-categories.new', {
        parent: 'settings-menu-categories',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/menu-categories/menu-categories-dialog.html',
                controller: 'MenuCategoriesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      name: null,
                      abbreviation: null,
                      description: null,
                      brief: null,
                      notes: null,
                      uuid: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      contentStatus: "DRAFT",
                      order: null
                    };
                  }
                }
              })
              .result
              .then(function () {
                $state.go('settings-menu-categories', null, {
                  reload: 'settings-menu-categories'
                });
              }, function () {
                $state.go('settings-menu-categories');
              });
          }
        ]
      })
      .state('settings-menu-categories.edit', {
        parent: 'settings-menu-categories',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/menu-categories/menu-categories-dialog.html',
                controller: 'MenuCategoriesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'MenuCategoriesService',
                    function (MenuCategoriesService) {
                      return MenuCategoriesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-menu-categories', null, {
                  reload: 'settings-menu-categories'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('settings-menu-categories.delete', {
        parent: 'settings-menu-categories',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/menu-categories/menu-categories-delete-dialog.html',
                controller: 'MenuCategoriesDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'MenuCategoriesService',
                    function (MenuCategoriesService) {
                      return MenuCategoriesService
                        .get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('settings-menu-categories', null, {
                  reload: 'settings-menu-categories'
                });
              }, function () {
                $state.go('^');
              });
          }
        ]
      });
  }
})();
