(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ChangePasswordDialogController', ChangePasswordDialogController);

  ChangePasswordDialogController.$inject = [
    '$timeout',
    '$scope',
    '$state',
    'auth',
    'AuthService',
    '$localStorage',
    'Flash',
    '$uibModalInstance'
  ];

  function ChangePasswordDialogController($timeout, $scope, $state, auth, AuthService, $localStorage, Flash, $uibModalInstance) {
    var vm = this;

    vm.confirm_password = '';

    vm.password = auth;

    vm.account = $localStorage.current_organisation;

    vm.clear = clear;
    vm.save = save;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      console.log(vm.password.password);
      AuthService
        .updatePassword(vm.password.password)
        .then(function () {
          //success
          vm.isSaving = false;
          vm.password.password = '';
          vm.confirm_password = '';
          var message = '<strong>Success!</strong> Your password has been changed.';
          Flash.create('success', message);
        })
        .catch(function () {
          //error
          vm.isSaving = false;
          vm.password.password = '';
          vm.confirm_password = '';
          var message = '<strong>Error!</strong> There was an error while trying to update your password.';
          Flash.create('danger', message);
        });
    }

  }
})();
