(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationRelationshipsController', OrganizationRelationshipsController);

    OrganizationRelationshipsController.$inject = ['$scope', '$location', '$state', '$localStorage', 'URLS'];

    function OrganizationRelationshipsController ($scope, $location, $state, $localStorage, URLS) {
        var vm = this;

        vm.records = $localStorage.current_managed_organisations;
        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;

        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);
    }
})();
