(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationFacilitiesController', LocationFacilitiesController);

  LocationFacilitiesController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'facilities']; //

    function LocationFacilitiesController($scope, $rootScope, $stateParams, previousState, entity, facilities) {
        var vm = this;

        vm.location = entity;
        vm.facilities = facilities;

        vm.displayFacilities = (vm.facilities.length !== 0);

        vm.previousState = previousState.name;
        vm.previousStateParent = previousState.name;
    }
})();
