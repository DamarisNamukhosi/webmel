(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationEventsController', LocationEventsController);

  LocationEventsController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'events']; //

    function LocationEventsController($scope, $rootScope, $stateParams, previousState, entity, events) {
        var vm = this;

        vm.location = entity;
        vm.events = events;


        {{vm.events}}

        vm.displayEvents = (vm.events.length !== 0);

        vm.previousState = previousState.name;
    }
})();
