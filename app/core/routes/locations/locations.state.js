(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('locations', {
        parent: 'app',
        url: '/locations',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER', 'D_AUTHORITY_USER', 'ORGANISATION_ADMIN'],
          pageTitle: 'Locations'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/locations.html',
            controller: 'LocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.query({ types: 2 }).$promise;
            }
          ]
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state("location-requirements", {
        parent: "locations-locations",
        url: "/{locationId}/{locationType}/requirements",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/locations/location-requirements.html",
            controller: "RequirementsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "$localStorage",
            "RequirementsService",
            function ($stateParams, $localStorage, RequirementsService) {
              return RequirementsService.getByLocationId({
                id: $stateParams.locationId, orgId: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('locations-locations', {
        parent: 'locations',
        url: '/{countryId}/countries',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER', 'D_AUTHORITY_USER', 'ORGANISATION_ADMIN'],
          pageTitle: 'Locations'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/locations.html',
            controller: 'LocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              // type id 11 is properties
              return LocationsService.query({
                types: 7

              }).$promise;
            }
          ]
        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state('locations-locations.new', {
        parent: 'locations-locations',
        url: '/{parentId}/newLocation',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: $stateParams.parentId,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations', null, {
                    reload: 'locations-locations'
                  });
                },
                function () {
                  $state.go('locations-locations');
                }
              );
          }
        ]
      })
      .state('locations.new', {
        parent: 'locations',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: null,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations', null, {
                    reload: 'locations'
                  });
                },
                function () {
                  $state.go('locations');
                }
              );
          }
        ]
      })
      .state('locations-own.new', {
        parent: 'locations-own',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog-create.html',
                controller: 'LocationDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      locationTypeId: null,
                      longitude: null,
                      name: null,
                      organisationId: $localStorage
                        .current_organisation.id,
                      parentId: null
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-own', null, {
                    reload: 'locations-own'
                  });
                },
                function () {
                  $state.go('locations-own');
                }
              );
          }
        ]
      })
      .state('locations-own', {
        parent: 'locations',
        url: '/own',
        data: {
          requiresAuthentication: true,
          authorities: ['D_AUTHORITY_USER', 'MANAGE_LOCATIONS', 'MANAGER_USER'],
          pageTitle: 'Locations'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/locations-own.html',
            controller: 'LocationsOwnController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          locations: [
            '$stateParams',
            '$localStorage',
            'LocationsService',
            function (
              $stateParams,
              $localStorage,
              LocationsService
            ) {
              return LocationsService.getLocationsByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('location-galleries', {
        parent: 'locations-own',
        url: '/{locationId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-galleries.html',
            controller: 'LocationGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'locations-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('locations-locations.galleries', {
        parent: 'locations-locations',
        url: '/{locationId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-galleries.html',
            controller: 'LocationGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.locationId
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'locations-locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      // .state('location-galleries.new', {
      //   parent: 'location-galleries',
      //   url: '/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: []
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$uibModal',
      //     function ($stateParams, $state, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/albums/album-create-dialog.html',
      //           controller: 'AlbumCreateDialogController',
      //           controllerAs: 'vm',
      //           backdrop: 'static',
      //           size: 'lg',
      //           resolve: {
      //             entity: [
      //               'LocationsService',
      //               function (LocationsService) {
      //                 return LocationsService.get({
      //                   id: $stateParams.id
      //                 }).$promise;
      //               }
      //             ],
      //             album: function () {
      //               return {
      //                 albumType: 'GENERAL',
      //                 caption: null, //album name
      //                 coverName: null, //uploaded cover image file name
      //                 coverUuid: null, //uploaded cover image
      //                 isDefaultAlbum: true, //put option slider
      //                 name: null, //album name
      //                 objectUuid: null //location uuid
      //               };
      //             }
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go('location-galleries', null, {
      //               reload: 'location-galleries'
      //             });
      //           },
      //           function () {
      //             $state.go('^');
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('location-galleries.new', {
        parent: 'locations-locations.galleries',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-create-dialog.html',
                controller: 'AlbumCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locationId
                      }).$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: 'GENERAL',
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.galleries', null, {
                    reload: 'locations-locations.galleries'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-galleries.delete', {
        parent: 'location-galleries',
        url: '/{albumId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-delete-dialog.html',
                controller: 'AlbumDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('location-galleries', null, {
                    reload: 'location-galleries'
                  });
                },
                function () {
                  $state.go('location-galleries');
                }
              );
          }
        ]
      })
      .state('location-album-detail', {
        parent: 'location-galleries',
        url: '/{albumId}/album',
        data: {
          requiresAuthentication: true,
          authorities: [
            'MASTER_USER',
            'D_AUTHORITY_USER',
            'MANAGE_LOCATIONS'
          ],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-album-detail.html',
            controller: 'LocationAlbumDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          album: [
            '$stateParams',
            'AlbumService',
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'locations-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-album-detail.edit', {
        parent: 'location-album-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-dialog.html',
                controller: 'AlbumDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AlbumService',
                    function (AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('location-album-detail', null, {
                    reload: 'location-album-detail'
                  });
                },
                function () {
                  $state.go('location-album-detail');
                }
              );
          }
        ]
      })
      .state('location-album-detail.upload', {
        parent: 'location-album-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-upload-dialog.html',
                controller: 'AlbumUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'AlbumService',
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('location-album-detail', null, {
                    reload: 'location-album-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-album-detail.makeCoverImage', {
        parent: 'location-album-detail',
        url: '/{imageId}/cover-image',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
                controller: 'AlbumChangeCoverImageDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('location-album-detail', null, {
                    reload: 'location-album-detail'
                  });
                },
                function () {
                  $state.go('location-album-detail');
                }
              );
          }
        ]
      })
      .state('location-album-detail.deleteImage', {
        parent: 'location-album-detail',
        url: '/{imageId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
                controller: 'AlbumImageDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('location-album-detail', null, {
                    reload: 'location-album-detail'
                  });
                },
                function () {
                  $state.go('location-album-detail');
                }
              );
          }
        ]
      })
      .state('location-detail', {
        parent: 'locations-own',
        url: '/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-detail.html',
            controller: 'LocationDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.getFullLocation({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'locations-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('locations-locations.overview', {
        parent: 'locations-locations',
        url: '/{locationId}/{locationType}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-detail.html',
            controller: 'LocationDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.getFullLocation({
                id: $stateParams.locationId
              })
                .$promise;
            }
          ],
          coordinates: [
            '$stateParams',
            'CoordinatesService',
            function ($stateParams, CoordinatesService) {
              return CoordinatesService.getCoordinatesByLocation({
                id: $stateParams.locationId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'locations-locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-detail.edit', {
        parent: 'location-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.getFullLocation({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-detail', null, {
                    reload: 'location-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      //locations-locations.overview
      .state('locations-locations.overview.edit', {
        parent: 'locations-locations.overview',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                // templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                // controller: 'OrganizationRegisterDialogController',
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.getFullLocation({
                        id: $stateParams.locationId
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.overview', null, {
                    reload: 'locations-locations.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-detail.addLabels', {
        parent: 'locations-locations.overview',
        url: '/{uuid}/{objectType}/add-labels',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-create-dialog.html',
                controller: 'ObjectLabelGroupCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  labelGroups: [
                    'LabelGroupsService',
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locationId
                      }).$promise;
                    }
                  ],
                  savedselectedLabels: [
                    'ObjectLabelsService',
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.overview', null, {
                    reload: 'locations-locations.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-detail.deleteLabel', {
        parent: 'locations-locations.overview',
        url: '/{itemToDelete}/delete-label',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-delete-dialog.html',
                controller: 'ObjectLabelGroupDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('locations-locations.overview', null, {
                    reload: 'locations-locations.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('locations-locations.detail', {
        parent: 'locations-locations',
        url: '/{locationId}/{locationType}/detail',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Attractions'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/properties/location-properties.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },

        // locations: [
        //   '$stateParams',
        //   'LocationsService',
        //   function ($stateParams, LocationsService) {
        //     return LocationsService.getLocationsByParentIdAndType({
        //       id:$stateParams.locationId,
        //       types: 10

        //     }).$promise;
        //   }
        // ],
        // entity: [
        //   '$stateParams',
        //   'LocationsService',
        //   function ($stateParams, LocationsService) {
        //     return LocationsService.get({
        //         id: $stateParams.locationId
        //       })
        //       .$promise;
        //   }
        // ]
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },

        resolve: {

        }
      })

      .state('location-attractions', {
        parent: 'locations-own',
        url: '/{id}/attractions',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Attractions'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-attractions.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              // type id 11 is properties
              return LocationsService.query({
                types: [6, 7, 8]

              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                parent_name: $state.current.name || 'locations-own',
                name: $state.current.name || 'location-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      // .state('location-attractions.new', {
      //   parent: 'location-attractions',
      //   url: '/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: []
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$uibModal',
      //     function ($stateParams, $state, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/locations/location-attractions-dialog.html',
      //           controller: 'LocationAttractionsDialogController',
      //           controllerAs: 'vm',
      //           backdrop: 'static',
      //           size: 'lg',
      //           resolve: {
      //             entity: [
      //               '$localStorage',
      //               '$stateParams',
      //               function ($localStorage, $stateParams) {
      //                 return {
      //                   id: null,
      //                   uuid: null,
      //                   name: null,
      //                   brief: null,
      //                   description: null,
      //                   order: null,
      //                   contentStatus: 'DRAFT',
      //                   statusReason: null,
      //                   locationId: $stateParams.id,
      //                   locationName: null,
      //                   generalAttractionId: null,
      //                   generalAttractionName: null
      //                 };
      //               }
      //             ]
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go('location-attractions', null, {
      //               reload: 'location-attractions'
      //             });
      //           },
      //           function () {
      //             $state.go('location-attractions');
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('location-attractions.new', {
        parent: 'location-attractions',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'NewAttractions'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog-create.html',
                controller: 'LocationDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      typeId: [6, 7, 8],
                      longitude: null,
                      name: null,
                      managerId: $localStorage.current_organisation.id
                    };
                  },
                  setParentVisible: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-attractions', null, {
                    reload: 'location-attractions'
                  });
                },
                function () {
                  $state.go('location-attractions');
                }
              );
          }
        ]
      })
      // .state("location-attractions.edit", {
      //   parent: "location-attractions",
      //   url: "/{attractionId}/edit",
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: []
      //   },
      //   onEnter: [
      //     "$stateParams",
      //     "$state",
      //     "$uibModal",
      //     function ($stateParams, $state, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/locations/location-attractions-dialog.html',
      //           controller: 'LocationAttractionsDialogController',
      //           controllerAs: "vm",
      //           backdrop: "static",
      //           size: "lg",
      //           resolve: {
      //             entity: [
      //               "AttractionsService",
      //               function (AttractionsService) {
      //                 return AttractionsService.get({
      //                     id: $stateParams.attractionId
      //                   })
      //                   .$promise;
      //               }
      //             ]
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go("location-attractions", null, {
      //               reload: "location-attractions"
      //             });
      //           },
      //           function () {
      //             $state.go("^");
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('location-attractions.edit', {
        parent: 'location-attractions',
        url: '/{attractionId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.attractionId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('location-attractions', null, {
                    reload: 'location-attractions'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('location-activities', {
        parent: 'locations-own',
        url: '/{id}/activities',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Activities'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-activities.html',
            controller: 'LocationActivitiesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          activities: [
            '$stateParams',
            'ActivityService',
            function ($stateParams, ActivityService) {
              return ActivityService.getActivitiesByLocId({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-activities.new', {
        parent: 'location-activities',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/activities/activity-dialog.html',
                controller: 'ActivityDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      locationId: $stateParams.id,
                      locationName: null,
                      generalActivityId: null,
                      generalActivityName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-activities', null, {
                    reload: 'location-activities'
                  });
                },
                function () {
                  $state.go('location-activities');
                }
              );
          }
        ]
      })
      .state("location-activities.edit", {
        parent: "location-activities",
        url: "/{activityId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/activities/activity-dialog.html",
                controller: "ActivityDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({
                        id: $stateParams.activityId
                      })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("location-activities", null, {
                    reload: "location-activities"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state('location-locations', {
        parent: 'locations',
        url: '/{id}/locations',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Locations'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-locations.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.getLocationsByParentId({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-locations.new', {
        parent: 'location-locations',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog-create.html',
                controller: 'LocationDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      locationTypeId: null,
                      longitude: null,
                      name: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      parentId: $stateParams.id
                    };
                  },
                  setParentVisible: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-locations', null, {
                    reload: 'location-locations'
                  });
                },
                function () {
                  $state.go('location-locations');
                }
              );
          }
        ]
      })
      .state('location-facilities', {
        parent: 'locations-own',
        url: '/{id}/facilities',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Facilities'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-facilities.html',
            controller: 'LocationFacilitiesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          facilities: [
            '$stateParams',
            'FacilityService',
            function ($stateParams, FacilityService) {
              return FacilityService.getFacilities({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-facilities.new', {
        parent: 'location-facilities',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/facilities/facility-dialog.html',
                controller: 'FacilityDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      locationId: $stateParams.id,
                      locationName: null,
                      generalFacilityId: null,
                      generalFacilityName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-facilities', null, {
                    reload: 'location-facilities'
                  });
                },
                function () {
                  $state.go('location-facilities');
                }
              );
          }
        ]
      })
      .state("location-facilities.edit", {
        parent: "location-facilities",
        url: "/{facilityId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/facilities/facility-dialog.html',
                controller: 'FacilityDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "FacilityService",
                    function (FacilityService) {
                      return FacilityService.get({
                        id: $stateParams.facilityId
                      })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("location-facilities", null, {
                    reload: "location-facilities"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state('location-events', {
        parent: 'locations-own',
        url: '/{id}/events',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Location Events'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/location-events.html',
            controller: 'LocationEventsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          events: [
            '$stateParams',
            'EventsService',
            function ($stateParams, EventsService) {
              return EventsService.getEvents({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('location-events.new', {
        parent: 'location-events',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-dialog.html",
                controller: "FullEventDialogController",
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      evntInstanceDTO: {
                        id: null,
                        uuid: null,
                        name: null,
                        brief: null,
                        description: null,
                        startTime: null,
                        endTime: null,
                        contentStatus: 'DRAFT',
                        statusReason: null,
                        evntId: null,
                        evntName: null,
                        locationId: $stateParams.id,
                        locationName: null
                      }
                    };
                  },
                  locationSet: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('location-events', null, {
                    reload: 'location-events'
                  });
                },
                function () {
                  $state.go('location-events');
                }
              );
          }
        ]
      })
      .state('location-properties', {
        parent: 'locations-own',
        url: '/{id}/properties',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/properties/location-properties.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              // type id 11 is properties
              return LocationsService.getLocationsByParentIdAndType({
                id: $stateParams.id,
                type_id: 10
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      // .state('location-properties.new', {
      //   parent: 'location-properties',
      //   url: '/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: [],
      //     pageTitle: ''
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$localStorage',
      //     '$uibModal',
      //     function ($stateParams, $state, $localStorage, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/organizations/organization-register-dialog.html',
      //           controller: 'OrganizationRegisterDialogController',
      //           controllerAs: 'vm',
      //           backdrop: 'static',
      //           size: 'lg',
      //           resolve: {
      //             entity: function () {
      //               return {
      //                 id:null,
      //                 abbreviation: null,
      //                 altitude: null,
      //                 brief: null,
      //                 description: null,
      //                 langKey: 'en',
      //                 latitude: null,
      //                 longitude: null,
      //                 name: null,
      //                 organisationTypeId: 4, // organization type for property is 4
      //                 parentLocationId: $stateParams.id,
      //                 parentOrganisationId: $localStorage
      //                   .current_organisation.id,
      //                 userEmailAddress: null,
      //                 userFullName: null
      //               };
      //             }
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go('location-properties', null, {
      //               reload: 'location-properties'
      //             });
      //           },
      //           function () {
      //             $state.go('^');
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('location-properties.new', {
        parent: 'locations-locations.detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: 4, // organization type for property is 4
                      parentLocationId: $stateParams.locationId,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.detail', null, {
                    reload: 'locations-locations.detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-properties.edit', {
        parent: 'locations-locations.detail',
        url: '/{locId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.detail', null, {
                    reload: 'locations-locations.detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('location-restaurants', {
        parent: 'locations-own',
        url: '/{id}/restaurants',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/restaurants/location-restaurants.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              return LocationsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          locations: [
            '$stateParams',
            'LocationsService',
            function ($stateParams, LocationsService) {
              // type id 11 is restaurants
              return LocationsService.getLocationsByParentIdAndType({
                id: $stateParams.id,
                type_id: 11
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'locations',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('locations-locations.restaurants', {
        parent: 'locations-locations',
        url: '/{locationId}/{locationType}/restaurants',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/restaurants/location-restaurants.html',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        // resolve: {
        //   entity: [
        //     '$stateParams',
        //     'LocationsService',
        //     function ($stateParams, LocationsService) {
        //       return LocationsService.get({
        //           id: $stateParams.locationId
        //         })
        //         .$promise;
        //     }
        //   ],
        //   locations: [
        //     '$stateParams',
        //     'LocationsService',
        //     function ($stateParams, LocationsService) {
        //       return LocationsService.getLocationsByParentIdAndType({
        //         id: $stateParams.locationId,
        //         types: 11
        //       }).$promise;
        //     }
        //   ],
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        previousState: [
          '$state',
          function ($state) {
            var currentStateData = {
              name: $state.current.name || 'locations-locations',
              params: $state.params,
              url: $state.href(
                $state.current.name,
                $state.params
              )
            };
            return currentStateData;
          }
        ]
      })
      .state('location-restaurants.new', {
        parent: 'locations-locations.restaurants',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: 5, // organization type for restaurant is 5
                      parentLocationId: $stateParams.locationId,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  },
                  hideLocationType: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.restaurants', null, {
                    reload: 'locations-locations.restaurants'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-restaurants.edit', {
        parent: 'locations-locations.restaurants',
        url: '/{locId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.restaurants', null, {
                    reload: 'locations-locations.restaurants'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('locations.edit', {
        parent: 'locations',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations', null, {
                    reload: 'locations'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('locations-own.edit', {
        parent: 'locations-own',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-own', null, {
                    reload: 'locations-own'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-detail.delete', {
        parent: 'locations-locations.overview',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-delete-dialog.html',
                controller: 'LocationDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.overview', null, {
                    reload: 'locations-locations.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('locations-own.delete', {
        parent: 'locations-own',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-delete-dialog.html',
                controller: 'LocationDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'LocationsService',
                    function (LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('locations-own', null, {
                    reload: 'locations-own'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('locations.profile', {
        parent: 'locations-locations.overview',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.overview', null, {
                    reload: 'locations-locations.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('location-activities.profile', {
        parent: 'location-activities',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('location-activities', null, {
                    reload: 'location-activities'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('location-attractions.profile', {
        parent: 'location-attractions',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('location-attractions', null, {
                    reload: 'location-attractions'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('location-facilities.profile', {
        parent: 'location-facilities',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('location-facilities', null, {
                    reload: 'location-facilities'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('location-locations.profile', {
        parent: 'location-locations',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('location-locations', null, {
                    reload: 'location-locations'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-properties.profile', {
        parent: 'locations-locations.detail',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('locations-locations.detail', null, {
                    reload: 'locations-locations.detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('location-restaurants.profile', {
        parent: 'locations-locations.restaurants',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
                controller: 'LocationImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('locations-locations.restaurants', null, {
                    reload: 'locations-locations.restaurants'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('locations-locations.others', {
        parent: 'locations-locations',
        url: '/{locationId}/{locationType}/others',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/locations/others/location-others.html',
            //controller: 'LocationOthersController',
            controller: 'LocationLocationsController',
            controllerAs: 'vm'
          }
        },
        // resolve: {
        //   entity: [
        //     '$stateParams',
        //     'LocationsService',
        //     function ($stateParams, LocationsService) {
        //       return LocationsService.get({
        //           id: $stateParams.locationId
        //         })
        //         .$promise;
        //     }
        //   ],
        //   locations: [
        //     '$stateParams',
        //     'LocationsService',
        //     function ($stateParams, LocationsService) {
        //       return LocationsService.getLocationsByParentIdAndType({
        //         id: $stateParams.locationId,
        //         types: [2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
        //       }).$promise;
        //     }
        //   ],
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        previousState: [
          '$state',
          function ($state) {
            var currentStateData = {
              name: $state.current.name || 'locations-locations',
              params: $state.params,
              url: $state.href(
                $state.current.name,
                $state.params
              )
            };
            return currentStateData;
          }
        ]
      })
      .state('locations-locations.others.new', {
        parent: 'locations-locations.others',
        url: '/newOthers',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/locations/location-dialog.html',
                controller: 'LocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: 8,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      parentId: $stateParams.locationId,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('locations-locations.others', null, {
                    reload: 'locations-locations.others'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state("location-requirements.new", {
        parent: "location-requirements",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "facilities.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-create-dialog.html",
                controller: "RequirementsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function () {
                    return {
                      name: null,
                      description: null,
                      notes: null,
                      specialNotes: null,
                      type: "REQUIREMENT",
                      orgId: $localStorage.current_organisation.id,
                      orgName: $localStorage.current_organisation.name,
                      locationId: $stateParams.locationId,
                      locationName: null,
                      generalServiceType: "LOCATION_MANAGEMENT",
                      generalServiceId: null,
                      generalServiceName: null,
                      serviceId: null,
                      serviceName: null,
                      supplierId: null,
                      supplierName: null,
                      issuerId: null,
                      issuerName: null,
                      serviceCategory: "COVERAGE",
                      objectType: "PERSON",
                      objectCategory: null,
                      perTrip: false,
                      perObject: true,
                      perDay: true,
                      optional: false,
                      timeStrict: true,
                      global: false,
                      visible: true,
                      contentStatus: "DRAFT",
                      packId: $stateParams.packId,
                      packName: null
                    };
                  },
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function ($localStorage, GeneralServicesService) {
                      return GeneralServicesService.query().$promise;
                      // .getByOrganizationTypeId({
                      //   id: $localStorage.current_organisation.organisationTypeId
                      // })
                      // .$promise;
                    }
                  ],
                  location: [
                    '$stateParams',
                    'LocationsService',
                    function ($stateParams, LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locationId
                      })
                        .$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  editParentRequirements: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go("location-requirements", null, {
                    reload: "location-requirements"
                  });
                },
                function () {
                  $state.go("location-requirements");
                }
              );
          }
        ]
      })
      .state("location-requirements.edit", {
        parent: "location-requirements",
        url: "/{requirementsId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-create-dialog.html",
                controller: "RequirementsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RequirementsService",
                    function (RequirementsService) {
                      return RequirementsService.getById({ id: $stateParams.requirementsId }).$promise;
                    }
                  ],
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function ($localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .query()
                        .$promise;
                    }
                  ],
                  location: [
                    '$stateParams',
                    'LocationsService',
                    function ($stateParams, LocationsService) {
                      return LocationsService.get({
                        id: $stateParams.locationId
                      })
                        .$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  editParentRequirements: function () {
                    return false;
                  }
                }

              })
              .result.then(
                function () {
                  $state.go("location-requirements", null, {
                    reload: "location-requirements"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("location-requirements.delete", {
        parent: "location-requirements",
        url: "/{requirementsId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-delete-dialog.html",
                controller: "RequirementsDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "RequirementsService",
                    function (RequirementsService) {
                      return RequirementsService.getById({ id: $stateParams.requirementsId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("location-requirements", null, {
                    reload: "location-requirements"
                  });
                },
                function () {
                  $state.go("location-requirements");
                }
              );
          }
        ]
      })

      ;

  }
})();
