(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationActivitiesController', LocationActivitiesController);

  LocationActivitiesController.$inject = ['$scope', '$rootScope', '$stateParams','$state','previousState', 'entity', 'activities']; //

    function LocationActivitiesController($scope, $rootScope, $stateParams, $state,previousState, entity, activities) {
        var vm = this;

        vm.location = entity;
        vm.activities = activities;

        vm.displayActivities = (vm.activities.length !== 0);
        
        vm.previousState = previousState.name;

        vm.currentState = $state.current.name;
        vm.previousStateParent = previousState.name;
    }
})();
