(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('LocationsController', LocationsController);

  LocationsController.$inject = ['$scope', '$location', '$state', 'LocationsService', 'ParseLinks','pagingParams','paginationConstants','URLS', '$stateParams', "LocationTypesService"
  ];

  function LocationsController($scope, $location, $state, LocationsService,ParseLinks,pagingParams,paginationConstants,URLS, $stateParams,LocationTypesService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
    vm.locations = [];
    vm.whenFiltering = false;
    vm.genLocations = [];

    //declare functions
    vm.loadPage = loadPage;
    vm.transition = transition;
    //pagination init
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.locationTypeChange = locationTypeChange;
    //initialization

   initLocations();

    // LocationsService.getLocationsPageable({
    //     page: pagingParams.page - 1,
    //     size: vm.itemsPerPage,
    //     sort: 'id,desc'
    //   },
    //   function (data, headers) {
    //     vm.links = ParseLinks.parse(headers('link'));
    //     vm.totalItems = headers('X-Total-Count');
    //     vm.queryCount = vm.totalItems;
    //     vm.locations = data;
    //     vm.page = pagingParams.page;
    //     vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
    //     vm.isLoading = false;
    //     vm.noRecordsFound = vm.locations === undefined || vm.locations.length === 0;
    //   },
    //   function (error) {
    //     console.log('error getting locations');
    //     console.log(error);
    //     vm.isLoading = false;
    //   }
    // );
    function initLocations(){
      vm.previousState = $state.current.parent;
      if(vm.previousState == 'app'){
      LocationsService.query({types: 2}).$promise.then(function (response){
        vm.locations = response;
        console.log(vm.locations);
      } );
    }else if(vm.previousState == 'locations'){
      console.log("locations under")
      LocationsService.getLocationsByParentIdAndType(
        {
          id: $stateParams.countryId,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage,
          sort: 'id,desc'
        },
        function(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.contracts = data;
            vm.page = pagingParams.page;
            vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
            vm.isLoading = false;

            initLocs(data);
        },
        function(error) {
            console.log('error getting contracts');
            console.log(error);
            vm.isLoading = false;
        }
    );


    LocationTypesService.query().$promise.then(function (response){
      vm.genLocations = response;
      console.log(vm.genLocations);
    })
    }
  }
  function initLocs(data){
    angular.forEach(data, function(loc){
      vm.locations.push(loc);

    });

  }
    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        countryId: $stateParams.countryId,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    vm.activate = function (location) {

      if (location.contentStatus == 'PUBLISHED') {
        location.contentStatus = "UNPUBLISHED"
      } else {
        location.contentStatus = "PUBLISHED"
      }

      console.log(location);
      LocationsService.update(location, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(response) {
      $state.reload();
    }

    function onSaveError(error) {
      console.log(error);
      alert(error);
    }


    function locationTypeChange(id,val){
      vm.whenFiltering = true;
      vm.locations= [];
      console.log(val);
        vm.searchedProperty = false;
          return LocationsService
            .query({
              name: val,
              types: [id]
            })
            .$promise
            .then(function (results) {
              angular.forEach(results, function(r){
                if(r.parentId == $stateParams.countryId){
                  vm.locations.push(r);
                }
              })

              console.log(vm.locations);
              return results.map(function (item) {

                return item;
              })
            });
        }


  }
})();
