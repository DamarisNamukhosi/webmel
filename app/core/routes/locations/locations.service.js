(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('LocationsService', LocationsService);

    LocationsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function LocationsService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations/general-filter/?name=:name&types=:types'
            },
            'generalFilter': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations'
            },

            'getLocationsPageable': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations/pageable'
            },
            'getLocations': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations'
            },
            'getFullLocation': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'contentservice/api/locations/get-full/:id'
            },
            'getLocationsByOrganizationId': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations/filter-by-organisation/:id'
            },
            'getLocationsByParentId': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations/filter-by-parent-location/:id'
            },
            'getLocationsByParentIdAndType': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/locations/filter-by-parent-location/:id'
            },
            'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'contentservice/api/locations/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/locations'
            },
            'updateFull': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/locations/update-full'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/locations/create-with-coordinates'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/locations/:id'
            },
            'deleteOwn': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/locations/delete/:organisationId/:locationId'
            }
        });
    }
})();
