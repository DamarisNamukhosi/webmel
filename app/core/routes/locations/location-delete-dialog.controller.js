(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationDeleteDialogController',LocationDeleteDialogController);

        LocationDeleteDialogController.$inject = ['$uibModalInstance', 'entity', '$localStorage', 'LocationsService'];

    function LocationDeleteDialogController($uibModalInstance, entity, $localStorage, LocationsService) {
        var vm = this;

        vm.location = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (location) {
          var organizationId = $localStorage.current_organisation.id;
          console.log("Current Org ID: " + organizationId);
          location.contentStatus = "DELETED";
            LocationsService.update(location,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
