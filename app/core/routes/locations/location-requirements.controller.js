(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("LocationRequirementsController", LocationRequirementsController);

  LocationRequirementsController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "$state",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "LocationsService",
    "NgMap",
    "URLS"
  ]; //

  function LocationRequirementsController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $state,
    previousState,
    entity,
    ObjectLabelsService,
    LocationsService,
    NgMap,
    URLS
  ) {
    var vm = this;

    vm.location = entity;


     console.log(vm.location);
    // NgMap.getMap().then(function(map) {
    //   vm.map = map;
    // });




    /**
     * This hack checks whether the logged in user is a MASTER_USER
     * or not then allocates the appropriate sref for the
     * locations-view
     */

    vm.previousState =
      permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";

      function initCountry() {
        console.log("init");
        console.log($rootScope.stateObject.locationType);
        console.log(vm.location.typeId);
        console.log($rootScope.stateObject.locationType != vm.location.typeId);

        if($rootScope.stateObject.locationType != vm.location.typeId){
          //$rootScope.stateObject.locationType = vm.location.typeId;
         // $state.reload();
          $state.go($state.current, {
              locationId : vm.location.id,
              locationType: vm.location.typeId
            }, {reload: true});
        }


        console.log($stateParams.countryId > 0);
        console.log($stateParams.countryId);
        console.log($stateParams);
        console.log($stateParams.locationId);
        vm.showCountry = true;
        if ($stateParams.countryId > 0) {
          LocationsService.get({
              id: $stateParams.countryId
            })
            .$promise.then(function (results) {
              vm.country = results;
            });
        }
      }

    }


})();
