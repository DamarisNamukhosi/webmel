(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationDialogController', LocationDialogController);

    LocationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'setParentVisible', 'LocationsService', 'LocationTypesService', 'URLS','OrganizationsService'];

    function LocationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, setParentVisible, LocationsService, LocationTypesService, URLS,OrganizationsService) {
        var vm = this;

        vm.location = entity;
        vm.clear = clear;
        vm.save = save;
        vm.locationTypeChange = locationTypeChange;
        vm.getLocation=getLocation;
        vm.getOrganization = getOrganization;
        vm.onLocationSelected= onLocationSelected;
        vm.onLocationManagerSelected = onLocationManagerSelected;
        vm.tempParentLocations=[];
        vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;


        initDialogModels();
        initLocCoordinates();
        function initDialogModels(){

         LocationsService.getLocations().$promise.then(function(response){
            vm.locations = response;
            if(vm.location.id !== null){
                locationTypeChange(vm.location.typeId);
            }
            vm.tempParentLocations= vm.locations;

         });


        }
        // vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E';

        function initLocCoordinates(){
        if(vm.location.id == null){
        vm.latlng = [0.8349313860427184, 37.4853515625];
        vm.location.latitude = 0.8349313860427184;
        vm.location.longitude = 37.4853515625;
        }else{
            if(vm.location.coordinates.length > 0){
            vm.latlng = [vm.location.coordinates[0].latitude,vm.location.coordinates[0].longitude];
        }else{
            vm.latlng = [0.8349313860427184, 37.4853515625];

        }
        }
    }
        vm.getpos = function (event) {
            vm.latlng = [event.latLng.lat(), event.latLng.lng()];

            vm.location.latitude = event.latLng.lat();
            vm.location.longitude = event.latLng.lng();
        };

        vm.setParentVisible = setParentVisible;


        vm.locationTypes = LocationTypesService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.location.id !== null) {
                console.log(vm.location);
                var temp = {
                    abbreviation: vm.location.abbreviation,
                    brief : vm.location.brief,
                    contentStatus :vm.location.contentStatus,
                    coordinates : [{
                        altitude: vm.location.coordinates[0].altitude,
                        id:  vm.location.coordinates[0].id,
                        latitude: vm.latlng[0],
                        locationId:vm.location.coordinates[0].locationId,
                        locationName: vm.location.coordinates[0].locationName,
                        longitude : vm.latlng[1],
                        name: vm.location.coordinates[0].name,
                        type: vm.location.coordinates[0].type,
                        uuid: vm.location.coordinates[0].uuid
                    }],
                    description: vm.location.description,
                    id: vm.location.id,
                    latitude : vm.location.latitude,
                    longitude: vm.location.longitude,
                    managerId : vm.location.managerId,
                    managerName: vm.location.managerName,
                    name : vm.location.name,
                    parentId : vm.location.parentId,
                    parentName : vm.location.parentName,
                    statusReason : vm.location.statusReason,
                    typeId: vm.location.typeId,
                    typeName: vm.location.typeName,
                    uuid: vm.location.uuid
                };
                console.log(temp);
                LocationsService.updateFull(temp, onSaveSuccess, onSaveError);
            } else {

                LocationsService.create(vm.location, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function locationTypeChange(id){
            vm.tempParentLocations= vm.locations;
            console.log('changed location Type id');

            // angular.forEach(vm.locations, function(location){
            //     if((location.typeId < id || (id ==  10 || id ==11) && (location.typeId == 12 || location.typeId == 13) ) && !((location.typeId == 10 || location.typeId == 11) && (id ==  12 || id ==13))){
            //         vm.tempParentLocations.push(location);
            //     }
            // })

        }

        function getOrganization(val) {
            vm.searchedProperty = false;
              return OrganizationsService
                .query({
                  name: val,
                  types: [2]
                })
                .$promise
                .then(function (results) {
                  vm.searchedItems = results;
                  return results.map(function (item) {

                    return item;
                  })
                });
            }

            function getLocation(val) {
                vm.searchedProperty = false;
                  return LocationsService
                    .query({
                      name: val,
                      types: [1,2,3,4,5,6,7,8,9,16,17,18,19]
                    })
                    .$promise
                    .then(function (results) {
                      vm.searchedItems = results;
                      return results.map(function (item) {

                        return item;
                      })
                    });
                }

            function onLocationSelected($item, $model) {

              vm.location.parentId=$item.id;

            }
            function onLocationManagerSelected($item, $model) {

                vm.location.managerId=$item.id;

              }
    }
})();
