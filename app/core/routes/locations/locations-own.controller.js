(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationsOwnController', LocationsOwnController);

    LocationsOwnController.$inject = ['$scope', '$location', '$state', 'locations', 'URLS'];

    function LocationsOwnController($scope, $location, $state, locations, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.locations = trimOfficelocations(locations);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound = (vm.locations === undefined || vm.locations.length === 0);

        function trimOfficelocations(locations) {
            var nonOfficeLocations = [];
            angular.forEach(locations, function (location) {
                if (location.typeId !== 16) {
                    nonOfficeLocations.push(location);
                }
            });
            return nonOfficeLocations;
        }



    }
})();
