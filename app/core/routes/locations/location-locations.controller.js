(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('LocationLocationsController', LocationLocationsController);

  LocationLocationsController.$inject = ['$scope', '$rootScope', '$stateParams', '$localStorage', '$state', 'OrganizationsService', 'CoordinatesService', 'URLS', 'NavbarService', 'LocationsService', 'ParseLinks', 'pagingParams', 'paginationConstants'];

  function LocationLocationsController($scope, $rootScope, $stateParams, $localStorage, $state, OrganizationsService, CoordinatesService, URLS, NavbarService, LocationsService, ParseLinks, pagingParams, paginationConstants) {
    var vm = this;

    vm.location = [];
    console.log(vm.location);
    vm.locations = [];
    vm.organizations = [];
    vm.coordinates = $localStorage.current_location_coordinates;
    vm.zoom = 10;
    vm.currentLocation = [];
    vm.setCurrentLocation = setCurrentLocation;
    vm.setCurrentOrganization = setCurrentOrganization;
    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
    vm.currentOrganization = null;
    vm.country = [];
    vm.showCountry = false;
    vm.child = {
      "longitude": null,
      "latitude": null
    };
    vm.entity = {};
    vm.startDate = new Date();
    vm.startDate.setDate(vm.startDate.getDay() - 7);
    vm.endDate = new Date();
    vm.entity.startDate = vm.startDate;
    vm.entity.endDate = vm.endDate;
    vm.openedFrom = false;
    vm.openedTo = false;
    vm.dateOptions = {
      startDate: moment().subtract(7, 'days'),
      endDate: moment(),
      maxDate: new Date(2037, 5, 22),
      startingDay: 1
    };

    //declare functions
    vm.refresh = refresh;
    vm.getLocation = getLocation;
    vm.loadPage = loadPage;
    vm.transition = transition;
    //pagination init
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.displayLocations = false;
    vm.zoomLevel = 7;


    vm.previousState = $state.current.name;
    vm.previousStateParent = $state.current.name;
    vm.shortenString = shortenString;
    //setCurrentOrganization();
    loadLocationsForFilter();
    initCountry();
    vm.child = {
      "latitude": null,
      "longitude": null
    };

    function shortenString(str) {
      if (str !== null && str.length > 130) {
        return str.substring(0, 170);
      }
    }
    function setCurrentLocation($event, id) {
      id = id.$;
      angular.forEach(vm.locations, function (location) {
        if (location.id === id) {
          vm.currentLocation = location;
        }
      })
      setCurrentOrganization();
    }


    function setCurrentOrganization() {
      if (vm.currentLocation === undefined) {
        vm.currentOrganization = null;
      } else {
        // vm.currentLocation. =
        OrganizationsService.get({
          id: vm.currentLocation.managerId
        }).$promise.then(function (response) {
          vm.currentOrganization = response;
        })
      }
      if ($localStorage.current_managed_organisations[0].id !== $localStorage.current_organisation.id) {
        var data = NavbarService.switchOrganization({
          id: $localStorage.current_immediate_parent_organisation.id
        }).$promise
          .then(function (response) {
            //update location_id
            console.log("check this");
            console.log(response);
            $localStorage.current_location_id = response.locationId;
            //update current_organisation
            $localStorage.current_organisation = response.organisation;
            $localStorage.current_managed_organisations = response.managedOrganisations;
            //update current_user_permissions
            $localStorage.current_user_permissions = response.authorities;

            $localStorage.current_immediate_parent_organisation = response.immediateParent;

            //go to home page
            if ($state.includes('location-properties')) {
              $state.reload();
            } else {
              $state.go("location-detail", {
                id: $localStorage.current_location_id
              }, {
                  reload: true
                });
            }
          }, function (error) {
            console.log(error);
          });
      } else { }
      if ($localStorage.current_location_coordinates !== undefined) {
        if ($localStorage.current_location_coordinates.id !== $localStorage.current_location_id) {
          $stateParams.id = $localStorage.current_location_coordinates.id;
        }
      }
    }


    function loadLocationsForFilter() {
      console.log($state);
      console.log($state.current.name === "locations-locations.detail");
      if ($state.current.name === "locations-locations.detail") {
        // LocationsService.getLocationsByParentIdAndType({
        //   id: $stateParams.locationId,
        //   types: 10
        // }).$promise.then(function (response) {
        //   vm.organizations = response;
        //   vm.locations = response;
        // })
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          types: 10,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage,
          sort: 'id,desc'
        },
          function (data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.contracts = data;
            vm.page = pagingParams.page;
            vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
            vm.isLoading = false;
            console.log("data");
            console.log(data);
            initLocs(data);
          },
          function (error) {
            console.log('error getting contracts');
            console.log(error);
            vm.isLoading = false;
          }
        );
      } else if ($state.current.name === "locations-locations.restaurants") {
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          types: 11
        }).$promise.then(function (response) {
          vm.organizations = response;
          vm.locations = response;

        });
      } else if ($state.current.name === "locations-locations.others") {
        // LocationsService.getLocationsByParentIdAndType({
        //   id: $stateParams.locationId,
        //   types: [2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
        // }).$promise
        console.log("other locs");
        var test = "2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19";
        var url = urlEncoder(test);
        console.log(url);
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 16, 17, 18, 19],
          //  "types[]": [2,3,4,5,6,7,8,9,12,13,14,15,17,18].toString(),
          // page: pagingParams.page - 1,
          // size: vm.itemsPerPage,
          // sort: 'id,desc'
        },
          function (data, headers) {
            // vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.contracts = data;
            vm.page = pagingParams.page;
            vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
            vm.isLoading = false;
            console.log("data");
            console.log(data);
            initLocs(data);
          },
          function (error) {
            console.log('error getting contracts');
            console.log(error);
            vm.isLoading = false;
          }
        );
      }
      else if ($state.current.name === "locations-locations.restaurants") {
        // LocationsService.getLocationsByParentIdAndType({
        //   id: $stateParams.locationId,
        //   types: [2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
        // }).$promise
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          types: 11,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage,
          sort: 'id,desc'
        },
          function (data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;
            vm.contracts = data;
            vm.page = pagingParams.page;
            vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
            vm.isLoading = false;
            console.log("data");
            console.log(data);
            initLocs(data);
          },
          function (error) {
            console.log('error getting contracts');
            console.log(error);
            vm.isLoading = false;
          }
        );
      }
      //

    }
    function urlEncoder(string) {
      console.log(string);
      console.log(string.split(","));
      console.log(string.split(",").join("%2C"));
      return string.split(",").join("%2C");

    }
    function initLocs(data) {
      console.log("init data");
      console.log(data);
      angular.forEach(data, function (loc) {

        if ($state.current.name !== "locations-locations.others") {
          vm.locations.push(loc);
          vm.organizations.push(loc);
          $localStorage.current_location_id = vm.locations.id;
          vm.displayLocations = (vm.locations.length !== 0);
          $localStorage.current_location = vm.locations;
        } else {
          console.log("otherss..");
          //"typeId":11
          console.log(loc);
          if (loc.typeId !== 10 && loc.typeId !== 11) {
            vm.locations.push(loc);
            vm.organizations.push(loc);
            $localStorage.current_location_id = vm.locations.id;
            vm.displayLocations = (vm.locations.length !== 0);
            $localStorage.current_location = vm.locations;
          }
        }



      });


    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        countryId: $stateParams.countryId,
        locationId: $stateParams.locationId,
        locationType: $stateParams.locationType,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    function initCountry() {
      console.log(vm.locations);
      console.log($stateParams.countryId > 0);
      console.log($stateParams.countryId);
      console.log($stateParams);
      console.log($stateParams.locationId);
      vm.showCountry = true;
      if ($stateParams.countryId > 0) {
        LocationsService.get({
          id: $stateParams.countryId
        })
          .$promise.then(function (results) {
            vm.country = results;
          });
      }
      if ($stateParams.locationId > 0) {
        // LocationsService.getLocationsByParentIdAndType({
        //     id: $stateParams.locationId,
        //     types: 10
        //   })
        //   .$promise.then(function (results) {
        //     vm.locations = results;
        //   });
        LocationsService.get({
          id: $stateParams.locationId
        })
          .$promise.then(function (results) {
            vm.location = results;
          });
      }
    }

    function getLocation(val) {
      return LocationsService
        .query({
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
    }
    vm.openFrom = function () {
      console.log("clicked");
      vm.openedFrom = true;
    };

    vm.openTo = function () {
      vm.openedTo = true;
    };

    function refresh($item, $model) {
      console.log("refreshiing...");
      console.log($state.current.name);
      if ($state.current.name === "locations-locations.detail") {
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          type_id: 10
        }).$promise.then(function (response) {
          vm.organizations = response;
          vm.locations = response;
        });
      } else if ($state.current.name === "locations-locations.restaurants") {
        LocationsService.getLocationsByParentIdAndType({
          id: $stateParams.locationId,
          type_id: 11
        }).$promise.then(function (response) {
          vm.organizations = response;
          vm.locations = response;
        });
      }

    }
    vm.switchAccount = function (orgId) {


      var data = NavbarService.switchOrganization({
        id: orgId
      }).$promise
        .then(function (response) {
          //update location_id
          $localStorage.current_location_id = response.locationId;
          //update current_organisation
          $localStorage.current_organisation = response.organisation;
          $localStorage.current_managed_organisations = response.managedOrganisations;
          //update current_user_permissions
          $localStorage.current_user_permissions = response.authorities;

          $localStorage.current_immediate_parent_organisation = response.immediateParent;

          //go to home page
          if ($state.includes('home')) {
            $state.reload();
          } else {
            $state.go("home", {}, {
              reload: true
            });
          }
        }, function (error) {
          console.log(error);
        });

    }

  }
})();
