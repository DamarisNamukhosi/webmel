(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('LocationDialogCreateController', LocationDialogCreateController);

  LocationDialogCreateController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$localStorage', '$state', 'entity', 'setParentVisible', 'LocationsService', 'LocationTypesService'];

  function LocationDialogCreateController($timeout, $scope, $stateParams, $uibModalInstance, $localStorage, $state, entity, setParentVisible, LocationsService, LocationTypesService) {
    var vm = this;

    vm.location = entity;
    vm.clear = clear;
    vm.save = save;
    vm.initCoordinates = initCoordinates;

    vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?libraries=places&key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E';

    vm.latlng = [0.8349313860427184, 37.4853515625];
    vm.location.latitude = 0.8349313860427184;
    vm.location.longitude = 37.4853515625;

    vm.location.managerId = vm.location.organisationId;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;


    vm.setParentVisible = setParentVisible;
    initCoordinates();
    initLocationTypes();

    vm.locations = LocationsService.getLocations();

    vm.getpos = function (event) {
      vm.latlng = [event.latLng.lat(), event.latLng.lng()];
      // console.log(event.latLng);
      vm.location.latitude = event.latLng.lat();
      vm.location.longitude = event.latLng.lng();
    };

    function initCoordinates() {

      if (($stateParams.long !== null && $stateParams.long !== undefined) || ($stateParams.lat !== null && $stateParams.lat !== undefined)) {
        console.log('editing parent');
        vm.latlng = [$stateParams.lat, $stateParams.long];
        vm.lat = $stateParams.lat;
        vm.long = $stateParams.long;
        vm.str = vm.long + "," + vm.lat;
        vm.zoomLevel = 11;
        console.log(vm.str);
      } else {
        console.log('new parent');
        if ($localStorage.current_location_coordinates !== "undefined") {
          vm.latlng = [$localStorage.current_location_coordinates.latitude, $localStorage.current_location_coordinates.longitude];
          console.log("vm.latlng");
          console.log(vm.latlng);
        } else if ($stateParams.id !== undefined) {
          console.log($stateParams.id);
          console.log('under parent');
          CoordinatesService.getCoordinatesByLocation({
            id: $stateParams.id
          }).$promise.then(function (response) {
            angular.forEach(response, function (object) {
              vm.child.longitude = object.longitude;
              vm.child.latitude = object.latitude;
              vm.str = vm.child.latitude + "," + vm.child.longitude;
              vm.zoomLevel = 10;
            });
          });
        } else {
          console.log('from home');
          vm.latlng = [0.8349313860427184, 37.4853515625];
          vm.lat = 0.8349313860427184;
          vm.long = 37.4853515625;
          vm.str = vm.lat + "," + vm.long;
          vm.zoomLevel = 7;
          console.log(vm.str);
          vm.IsSaving = false;
        }
      }
    }

    function initLocationTypes() {
      LocationTypesService.query().$promise.then(function (response) {
        console.log("response");
        console.log(response);
        vm.locationTypes = response;
        vm.locTypes = vm.locationTypes;

      });

    }

    // console.log(vm.locationTypes);
    // console.log($state.current.parent === "location-attractions");
    // vm.locationTypes = LocationTypesService.query().$promise;
    //     vm.locTypes = vm.locationTypes;
    // console.log(vm.locationTypes);
    // if ($state.current.parent === "location-attractions") {
    //   angular.forEach(vm.locationTypes, function (locationType) {
    //     if((location.typeId < id || (id ==  10 || id ==11) && (location.typeId == 12 || location.typeId == 13) ) && !((location.typeId == 10 || location.typeId == 11) && (id ==  12 || id ==13))){
    //         vm.tempParentLocations.push(location);
    //     }            position =  vm.contract.rates.indexOf(rate);
    //     console.log(locationType.id == 6);

    //     console.log(locationType);
    //     if (!(locationType.id == 6 && locationType.id == 7 && locationType.id == 8)) {
    //         console.log(vm.locationTypes.indexOf(locationType));
    //       vm.locTypes.splice(vm.locationTypes.indexOf(locationType),1);
    //     }
    //   });
    //   console.log(vm.locTypes)
    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;

      console.log(angular.toJson(vm.location));
      LocationsService.create(vm.location, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {

      vm.location.parentId = $item.id;

    }

  }
})();
