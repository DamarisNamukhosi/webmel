(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('LocationAttractionsController', LocationAttractionsController);

  LocationAttractionsController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'attractions']; //

    function LocationAttractionsController($scope, $rootScope, $stateParams, previousState, entity, attractions) {
        var vm = this;

        vm.location = entity;
        vm.attractions = attractions;

        vm.attractionsCount = attractions.length;

        vm.displayAttractions = true; 
        if (vm.attractionsCount == 0) {
            vm.displayAttractions = false;
        }

        vm.previousState = previousState.name;
        vm.previousStateParent = previousState.parent_name;

    }
})();
