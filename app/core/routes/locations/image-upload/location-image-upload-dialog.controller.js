(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("LocationImageUploadDialogController", LocationImageUploadDialogController);

  LocationImageUploadDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "object",
    "MediaItemsService",
    "Upload",
    "$localStorage",
    "URLS",
    "$state"
  ];

  function LocationImageUploadDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, object, MediaItemsService, Upload, $localStorage, URLS,$state) {
    var vm = this;

    vm.image = entity;
    vm.object = object;
    console.log('object....');
    console.log(vm.object);
    vm.clear = clear;
    vm.save = save;
    vm.image_response = null;
    vm.isUploading = false;
    vm.onFileSelect = onFileSelect;
    vm.myImage = '';
    vm.croppedImage = '';
    vm.file = '';
    vm.resBlob = {};
    vm.resultUpload = {};

    vm.currentState=$state.current.name;
    console.log('vm.currentState');
    console.log(vm.currentState);
    vm.previousState=$stateParams.currentState;

    function clear() {
    $uibModalInstance.dismiss("cancel");
    }

    function onFileSelect(event) {
      console.log("On File Select");
      var file = event.currentTarget.files[0];
      console.log("File");
      console.log(file);

      var reader = new FileReader();

      reader.onload = function (event) {
        $scope
          .$apply(function ($scope) {
            console.log("Reader Event");
            console.log(event);
            vm.myImage = event.target.result;
            vm.file = vm.myImage;
          });
      };
      reader.readAsDataURL(file);

      console.log("Image");
      console.log(vm.image);

      console.log("Image File");
      console.log(vm.image.file);

      console.log("File");
      console.log(file);
      vm.file = file;
    }

    function save() {
      //createMediaItemWithObjectDTO
      var mediaItemWithObjectDTO = {
        "albumType": "PROFILE",
        "caption": vm.object.name,
        "filename": vm.image_response.fileName,
        "mediaType": "IMAGE",
        "mimeType": vm.image_response.contentType,
        "name": vm.object.name,
        "objectUuid": vm.object.uuid
      };

      console.log("mediaItemWithObjectDTO");
      console.log(mediaItemWithObjectDTO);
      MediaItemsService.uploadProfilePhoto(mediaItemWithObjectDTO, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.upload = function (file, errFiles) {

      vm.isUploading = true;
      vm.errFile = errFiles && errFiles[0];
      if (vm.resBlob) {
        console.log(vm.resBlob);
        vm.resBlob.upload = Upload.upload({
          url: URLS.BASE_URL + "mediaservice/api/media-items/upload",
          data: {
            file: vm.resBlob
          },
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            }
          })
          .then(function (response) {
            //save
            vm.image_response = response.data;
            console.log("response data");
            console.log(vm.image_response);

            save();

          }, function (response) {
            if (response.status > 0)
              vm.errorMsg = response.status + ": " + response.data;
            }
          );
      }
      vm.isUploading = false;
    };
  }
})();
