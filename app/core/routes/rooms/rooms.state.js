(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("rooms", {
        parent: "app",
        url: "/rooms",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/rooms/rooms.html",
            controller: "RoomsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          rooms: [
            "$stateParams",
            "$localStorage",
            "RoomsService",
            function($stateParams, $localStorage, RoomsService) {
              return RoomsService.getRooms({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("rooms.new", {
        parent: "rooms",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "documents-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-dialog.html",
                controller: "RoomDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      identification: null,
                      brief: null,
                      description: null,
                      notes: null,
                      building: null,
                      floor: null,
                      floorPosition: null,
                      roomArea: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      roomCategoryId: $stateParams.id,
                      roomCategoryName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("rooms", null, { reload: "rooms" });
                },
                function() {
                  $state.go("rooms");
                }
              );
          }
        ]
      })
      .state("room-detail", {
        parent: "app",
        url: "/rooms/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.rooms.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/rooms/room-detail.html",
            controller: "RoomDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomsService",
            function($stateParams, RoomsService) {
              return RoomsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-detail.image", {
        parent: "room-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-image-upload-dialog.html",
                controller: "RoomImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  room: [
                    "$stateParams",
                    "RoomsService",
                    function($stateParams, RoomsService) {
                      return RoomsService.get({ id: $stateParams.id }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-detail", null, { reload: "room-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-detail.delete", {
        parent: "room-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Delete room ?"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-delete-dialog.html",
                controller: "RoomDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "RoomsService",
                    function($stateParams, RoomsService) {
                      return RoomsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-detail", {}, { reload: true });
                },
                function() {
                  $state.go("room-detail");
                }
              );
          }
        ]
      })
      .state("rooms.edit", {
        parent: "room-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-dialog.html",
                controller: "RoomDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RoomsService",
                    function(RoomsService) {
                      return RoomsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-detail", null, {
                    reload: "room-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("rooms.delete", {
        parent: "rooms",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-delete-dialog.html",
                controller: "RoomDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "RoomsService",
                    function(RoomsService) {
                      return RoomsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("rooms", null, { reload: "rooms" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-galleries", {
        parent: "app",
        url: "/rooms/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/rooms/room-galleries.html",
            controller: "RoomGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomsService",
            function($stateParams, RoomsService) {
              return RoomsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-galleries.new", {
        parent: "room-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RoomsService",
                    function(RoomsService) {
                      return RoomsService.get({ id: $stateParams.id }).$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("room-galleries", null, {
                    reload: "room-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-galleries.delete", {
        parent: "room-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("room-galleries", null, {
                    reload: "room-galleries"
                  });
                },
                function() {
                  $state.go("room-galleries");
                }
              );
          }
        ]
      })
      .state("room-album-detail", {
        parent: "room-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/rooms/room-album-detail.html",
            controller: "RoomAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomsService",
            function($stateParams, RoomsService) {
              return RoomsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "room-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-album-detail.edit", {
        parent: "room-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "room-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AlbumService",
                    function(AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-album-detail", null, {
                    reload: "room-album-detail"
                  });
                },
                function() {
                  $state.go("room-album-detail");
                }
              );
          }
        ]
      })
      .state("room-album-detail.makeCoverImage", {
        parent: "room-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-album-detail", null, {
                    reload: "room-album-detail"
                  });
                },
                function() {
                  $state.go("room-album-detail");
                }
              );
          }
        ]
      })
      .state("room-album-detail.deleteImage", {
        parent: "room-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-album-detail", null, {
                    reload: "room-album-detail"
                  });
                },
                function() {
                  $state.go("room-album-detail");
                }
              );
          }
        ]
      })
      .state("room-album-detail.upload", {
        parent: "room-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-album-detail", null, {
                    reload: "room-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-detail.addLabels", {
        parent: "room-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "RoomsService",
                    function (RoomsService) {
                      return RoomsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("room-detail", null, {
                  reload: "room-detail"
                });
              },
              function () {
                $state.go("^");
              }
              );
          }
        ]
      })
      .state("room-detail.deleteLabel", {
        parent: "room-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
              function () {
                $state.go("room-detail", null, {
                  reload: "room-detail"
                });
              },
              function () {
                $state.go("room-detail");
              }
              );
          }
        ]
      })
      .state("room-detail.addFeature", {
        parent: "room-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-create-dialog.html",
                controller: "ObjectFeatureCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  features: [
                    "ObjectFeaturesService",
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "RoomsService",
                    function (RoomsService) {
                      return RoomsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    "ObjectFeaturesService",
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("room-detail", null, {
                  reload: "room-detail"
                });
              },
              function () {
                $state.go("room-detail");
              });
          }
        ]
      })
      .state("room-detail.deleteFeature", {
        parent: "room-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-delete-dialog.html",
                controller: "ObjectFeatureDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
              function () {
                $state.go("room-detail", null, {
                  reload: "room-detail"
                });
              },
              function () {
                $state.go("room-detail");
              }
              );
          }
        ]
      });
  }
})();
