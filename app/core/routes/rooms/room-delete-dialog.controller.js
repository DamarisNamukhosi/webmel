(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomDeleteDialogController',RoomDeleteDialogController);

        RoomDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'RoomsService'];

    function RoomDeleteDialogController($uibModalInstance, entity, RoomsService) {
        var vm = this;

        vm.room = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (room) {
          room.contentStatus = "DELETED";
            RoomsService.update(room,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
