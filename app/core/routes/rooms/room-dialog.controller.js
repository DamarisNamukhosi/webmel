
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomDialogController', RoomDialogController);

    RoomDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity','RoomsService','RoomOccupanciesService', 'RoomCategoryService'];

    function RoomDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RoomsService,RoomOccupanciesService, RoomCategoryService) {
        var vm = this;

        vm.room = entity;
        vm.clear = clear;
        vm.save = save;

        vm.roomCategory = RoomCategoryService.get({id : $stateParams.id});
        //vm.roomOccupancies= RoomOccupanciesService.query();
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.room.id !== null) {
                RoomsService.update(vm.room, onSaveSuccess, onSaveError);
            } else {
                RoomsService.create(vm.room, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
