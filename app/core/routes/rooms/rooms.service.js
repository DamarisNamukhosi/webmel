(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('RoomsService', RoomsService);

  RoomsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function RoomsService ($resource, $localStorage, URLS) {
        var resourceUrl =  '';

        return $resource(resourceUrl, {}, {
          'getRooms': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/rooms/filter-by-organisation/:id'
          },
          'getRoomsByCategory': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/rooms/filter-by-room-category/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
          
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/rooms/:id'
          },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/rooms'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/rooms'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/rooms/:id'
        }
      });
    }
})();
