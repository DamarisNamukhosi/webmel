(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomDetailController', RoomDetailController);

    RoomDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ObjectLabelsService', 'ObjectFeaturesService','URLS'];

    function RoomDetailController($scope, $rootScope, $stateParams, previousState, entity, ObjectLabelsService, ObjectFeaturesService,URLS) {
        var vm = this;
        vm.room = entity;
        console.log(vm.room);
        vm.previousState = previousState.name;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.room.uuid;

        vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.room.uuid });
        console.log('object labels...');
        vm.previousState = previousState.name;

        vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({ uuid: vm.room.uuid });
        console.log('object labels...');
        console.log(vm.objectFeatures);
    }
})();
