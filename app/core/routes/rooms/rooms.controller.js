(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomsController', RoomsController);

    RoomsController.$inject = ['$scope', '$location', '$state', 'rooms'];

    function RoomsController ($scope, $location, $state, rooms) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.rooms = rooms;
    }
})();
