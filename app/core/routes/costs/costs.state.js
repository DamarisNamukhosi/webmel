(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('costs', {
        parent: 'app',
        url: '/costs',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'costs'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/costs/costs.html',
            controller: 'CostsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          costs: [
            '$stateParams',
            '$localStorage',
            'CostsService',
            function ($stateParams, $localStorage, CostsService) {
              return CostsService.getCosts({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('cost-detail', {
        parent: 'costs',
        url: '/costs/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/costs/cost-detail.html',
            controller: 'CostDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'CostsService',
            function ($stateParams, $localStorage, CostsService) {
              return CostsService.getCosts({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'costs',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }

          ]
        }
      })
      .state('cost-contracts.new', {
        parent: 'cost-contracts',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          '$localStorage',
          function ($stateParams, $state, $uibModal, $localStorage) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-own-dialog.html',
                controller: 'ContractOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    //var year = new Date().getFullYear()+1;
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      targetId: null,
                      type: 'CONTRACT',
                      startDate: null,
                      endDate: null,
                      contentStatus: 'DRAFT',
                      status: 'DRAFT',
                      marketId: null,
                      currencyId: 204,
                      currencyAbbreviation: null,
                      statusReason: null,
                      serviceId: $stateParams.id,
                      dimensionSetId: null,
                      dimensionSetName: null,
                      year: null,
                      issuerId: $localStorage.current_organisation.id,
                      issuerName: null,
                      issuerUuid: null,
                      supplierId:null,
                      dim1: null,
                      dim2: null,
                      dim3: null,
                      dim4: null,
                      dim5: null,
                      objectType: "PERSON"

                    };
                  },
                  dimensionSets: [
                    'CostsService',
                    function (CostsService) {
                      return CostsService.getServiceDimensionSet({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  markets: [
                    '$stateParams',
                    'MarketsService',
                    '$localStorage',
                    function (
                      $stateParams,
                      MarketsService,
                      $localStorage
                    ) {
                      return MarketsService.getByOrganisation({
                        id: $localStorage
                          .current_organisation
                          .id
                      }).$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  organizationTypes: [
                    '$stateParams',
                    'OrganizationTypesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      OrganizationTypesService,
                      $localStorage
                    ) {
                      return OrganizationTypesService.query().$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts', null, {
                    reload: 'cost-contracts'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('cost-contracts', {
        parent: 'app',
        url: '/costs/{id}/{generalServiceId}/contracts',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'contract'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/contracts.html',
            controller: 'ContractsController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          services: [
            '$stateParams',
            '$localStorage',
            'GeneralServicesService',
            function (
              $stateParams,
              $localStorage,
              GeneralServicesService
            ) {
              return GeneralServicesService.get().$promise;
            }
          ],
          organizations: [
            'OrganizationsService',
            function (OrganizationsService) {
              return OrganizationsService.query(
                {
                  types: [1]
                }
              ).$promise;
            }
          ],
          dimensionSets: [
            'CostsService',
            '$stateParams',
            function (CostsService, $stateParams) {
              return CostsService.getServiceDimensionSet({
                id: $stateParams.id
              }).$promise;
            }
          ],
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'costs',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('cost-contracts.newDimension', {
        parent: 'cost-contracts',
        url: '/dimension-set/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'services.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/dimension-sets/dimension-set-create-dialog.html',
                controller: 'DimensionSetsCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      default: false,
                      dimensionDTOList: [],
                      id: null,
                      name: null,
                      serviceId: null,
                      uuid: null
                    };
                  },
                  dimensionTypes: [
                    'ServicesService',
                    '$stateParams',
                    'GeneralServicesDimensionsService',
                    function (
                      ServicesService,
                      $stateParams,
                      GeneralServicesDimensionsService
                    ) {
                      var service = ServicesService.getById({
                        id: $stateParams.id
                      }).$promise;

                      return GeneralServicesDimensionsService.getByGeneralServiceId({
                        id: service.generalServiceId
                      }).$promise;
                    }
                  ],
                  service: [
                      'ServicesService',
                      '$stateParams',
                      function(
                          ServicesService,
                          $stateParams
                      ) {
                          return ServicesService.getById({
                              id: $stateParams.id
                          }).$promise;
                      }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts', null, {
                    reload: 'cost-contracts'
                  });
                },
                function () {
                  $state.go('cost-contracts');
                }
              );
          }
        ]
      })
      .state('costs.edit', {
        parent: 'costs',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/costs/cost-dialog.html',
                controller: 'CostDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'CostsService',
                    function (CostsService) {
                      return CostsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('costs', null, {
                    reload: 'costs'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('costs.new', {
        parent: 'costs',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/services/service-dialog.html',
                controller: 'ServiceDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: 'DRAFT',
                      description: null,
                      generalServiceId: null,
                      generalServiceName: null,
                      id: null,
                      name: null,
                      notes: null,
                      organisationId: $localStorage
                        .current_organisation.id,
                      organisationName: null,
                      uuid: null
                    };
                  },
                  generalServices: [
                    '$stateParams',
                    '$localStorage',
                    'GeneralServicesService',
                    function (
                      $stateParams,
                      $localStorage,
                      GeneralServicesService
                    ) {
                      return GeneralServicesService.getByOrganizationTypeId({
                        id: $localStorage
                          .current_organisation
                          .organisationTypeId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('costs', null, {
                    reload: 'costs'
                  });
                },
                function () {
                  $state.go('costs');
                }
              );
          }
        ]
      })
      .state('cost-contracts-detail.edit', {
        parent: 'cost-contracts-detail',
        url: '/{contractId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'cost-.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/rates/rates-dialog.html',
                controller: 'RatesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  srvice: [
                    '$stateParams',
                    'CostsService',
                    function ($stateParams, CostsService) {
                      return CostsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  entity: [
                    '$stateParams',
                    'RatesService',
                    function ($stateParams, RatesService) {
                      return RatesService.getRatesByContract({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ],
                  dimensionSet: [
                    'CostsService',
                    function (CostsService) {
                      return CostsService.getServiceDimensionSet({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],

                  rates: [
                    '$stateParams',
                    'RatesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      RatesService,
                      $localStorage
                    ) {
                      return RatesService.getRatesByContract({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ],
                  currency: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.get()
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts-detail', null, {
                    reload: 'cost-contracts-detail'
                  });
                },
                function () {
                  $state.go('cost-contracts-detail');
                }
              );
          }
        ]
      })
      .state('cost-contracts.edit', {
        parent: 'cost-contracts',
        url: '/{contractId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-dialog.html',
                controller: 'ContractDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ],
                  dimensionSets: [
                    'CostsService',
                    function (CostsService) {
                      return CostsService.getServiceDimensionSet({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],

                  markets: [
                    '$stateParams',
                    'MarketsService',
                    '$localStorage',
                    function (
                      $stateParams,
                      MarketsService,
                      $localStorage
                    ) {
                      return MarketsService.getByOrganisation({
                        id: $localStorage
                          .current_organisation
                          .id
                      }).$promise;
                    }
                  ],
                  currency: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query()
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts', null, {
                    reload: 'cost-contracts'
                  });
                },
                function () {
                  $state.go('cost-contracts');
                }
              );
          }
        ]
      })
      .state('cost-contracts-detail', {
        parent: 'cost-contracts',
        url: '/{contractId}/{versionId}/rates/{setId}/{seasonGroupId}/{year}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            // templateUrl: 'core/routes/contracts/contract-user-detail.html',
            // controller: 'ContractDetailController',
            templateUrl: 'core/routes/contracts/new/contract-detail.html',
            controller: 'ContractDetail2Controller',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            '$localStorage',
            'ContractsService',
            function (
              $stateParams,
              $localStorage,
              ContractsService
            ) {
              return ContractsService.getFull({
                id: $stateParams.contractId,
                versionId: $stateParams.versionId
              }).$promise;
            }
          ],
          dimensionSet: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getSetDimensionExistenciesBySetId({
                id: $stateParams.setId,
                seasonGroupId: $stateParams.seasonGroupId,
                year:$stateParams.year
              }).$promise;
            }
          ],
          dimensions: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getAllDimensions().$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'contracts-own-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }

      })
      //contracts-own-detail
      .state('contracts-own-detail.statusUpdate', {
        parent: 'contracts-own-detail',
        url: '/{status}/update',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-update-status-dialog.html',
                controller: 'ContractUpdateStatusDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage', 'ContractsService',
                    function ($localStorage, ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('cost-contracts-detail.statusUpdate', {
        parent: 'cost-contracts-detail',
        url: '/{status}/update',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-update-status-dialog.html',
                controller: 'ContractUpdateStatusDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage', 'ContractsService',
                    function ($localStorage, ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts-detail', null, {
                    reload: 'cost-contracts-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('cost-contracts-detail.new', {
        parent: 'cost-contracts-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'rates-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/rates/rates-dialog.html',
                controller: 'RatesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  srvice: [
                    '$stateParams',
                    'CostsService',
                    function ($stateParams, CostsService) {
                      return CostsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      DimensionSetsService
                    ) {
                      console.log('$stateParams.setId');
                      console.log($stateParams.setId);
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        organisationId: $localStorage
                          .current_organisation
                          .id
                      }).$promise;
                    }
                  ],

                  entity: [
                    '$stateParams',
                    'RatesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      RatesService,
                      $localStorage
                    ) {
                      return RatesService.getRatesByContract({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('cost-contracts-detail', null, {
                    reload: 'cost-contracts-detail'
                  });
                },
                function () {
                  $state.go('cost-contracts-detail');
                }
              );
          }
        ]
      })
      .state('cost-contract-rates-detail', {
        parent: 'cost-contract-rates',
        url: '/{setId}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/dimension-sets/dimension-set-detail.html',
            controller: 'DimensionSetDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'CostsService',
            function ($stateParams, CostsService) {
              return CostsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          contract: [
            '$stateParams',
            'ContractsService',
            function ($stateParams, ContractsService) {
              return ContractsService.get({
                id: $stateParams.contractId
              }).$promise;
            }
          ],
          dimensionSet: [
            '$stateParams',
            'DimensionSetsService',
            function ($stateParams, DimensionSetsService) {
              console.log('$stateParams.setId');
              console.log($stateParams);

              return DimensionSetsService.getById({
                id: $stateParams.setId
              }).$promise;
            }
          ],
          rates: [
            '$stateParams',
            'RatesService',
            function ($stateParams, RatesService) {
              return RatesService.getRatesByContract({
                id: $stateParams.contractId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'costs',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state("cost-contracts.newSafari", {
        parent: "cost-contracts",
        url: "/{statePackageType}/{year}/{locationId}/{groupId}/{countryId}/{targetId}/Safari",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: 'core/routes/packages/package-dialog.html',
            controller: 'PackageDialogController',
            controllerAs: "vm"
          }
        },
        resolve: {
          entity:
           [
            "$stateParams",
            "$localStorage",
            function ($stateParams, $localStorage) {
              return {
                country: $stateParams.countryId,
                contentStatus: "DRAFT",
                id: null,
                orgId: $localStorage.current_organisation.id,
                orgName: $localStorage.current_organisation.name,
                dmcId: $localStorage.current_organisation.id,
                dmcName: $localStorage.current_organisation.name,
                name: null,
                pax: null,
                seasonGroupId: null,
                seasonGroupName: null,
                targetId: null,
                targetName: null,
                type: $stateParams.statePackageType,
                uuid: null,
                year: $stateParams.year,
                currencyConfigs: null,
                currencyConversionGroupId: null,
                currencyConversionGroupName: null,
                currencyId: null,
                currencyName: null,
                cycle: "DAILY",
                dayModel: "DATE",
                defaultLanguage: "en",
                description: null,
                dimensionSetId: null,
                dimensionSetName: null,
                dropOffPointId: null,
                dropOffPointName: null,
                dstLocationId: null,
                dstLocationName: null,
                endDay: null,
                endTime: null,
                expiryTime: null,
                groupId: $stateParams.groupId,
                groupName: null,
                locationId: $stateParams.locationId,
                locationName: $localStorage.current_location.name,
                markUpCategoryId: null,
                markUpCategoryName: null,
                markUpConfigs: null,
                maxBookingPax: null,
                maxPricingPax: null,
                minBookingPax: 1,
                minPricingPax: 1,
                numOfDays: null,
                numOfNights: null,
                operationEndDate: "2019-08-13",
                operationStartDate: "2019-08-13",
                operationStatus: "PENDING",
                order: 0,
                pickUpPointId: null,
                pickUpPointName: null,
                profile: "VIP",
                purpose: "HOLIDAY",
                reference: null,
                requirementsPackId: null,
                requirementsPackName: null,
                residence: null,
                safariPackConfigs: null,
                safariTemplate: null,
                scope: "PRIVATE",
                serviceConfigs: null,
                serviceExclusionNotes: null,
                serviceExclusions: null,
                serviceId: null,
                serviceInclusionNotes: null,
                serviceInclusions: null,
                serviceName: null,
                specialNotes: null,
                srcLocationId: null,
                srcLocationName: null,
                startDay: null,
                startTime: null,
                status: "DRAFT",
                travellerGroupConfigs: null,
                travellerGroupId: null,
                travellerGroupName: null,
                vehicleGroupConfigs: null,
                virtualTravellers: true,
                virtualVehicles: true
              }
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "cost-contract",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      });
  }
})();
