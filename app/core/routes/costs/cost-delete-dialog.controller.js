(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('CostDeleteDialogController',CostDeleteDialogController);

        CostDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'FacilityService'];

    function CostDeleteDialogController($uibModalInstance, entity, FacilityService) {
        var vm = this;

        vm.cost = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(cost) {
          cost.contentStatus = "DELETED";
          FacilityService.update(cost, function() {
            $uibModalInstance.close(true);
          });
        }
    }
})();
