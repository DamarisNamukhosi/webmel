(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("CostDetailController", CostDetailController);

  CostDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "CostsService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function CostDetailController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    ObjectLabelsService,
    CostsService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;

    vm.cost = entity;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.cost.uuid });
    console.log(vm.objectLabels);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.cost.uuid;

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({ uuid: vm.cost.uuid });
    console.log('object labels...');
    console.log(vm.objectFeatures);

    vm.currentLocationIdSet = !(
      $localStorage.current_location_id === undefined ||
      $localStorage.current_location_id === null ||
      $localStorage.current_location_id === "null"
    );
  }
})();
