(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('CostsController', CostsController);

    CostsController.$inject = ['$scope', '$location', '$state', 'costs', 'URLS'];

        function CostsController($scope, $location, $state, costs, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.costs = costs;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound = (vm.costs === undefined || vm.costs.length === 0);
        console.log(vm.costs);
    }
})();
