(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('CostDialogController', CostDialogController);

        CostDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CostsService'];

    function CostDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CostsService) {
        var vm = this;

        vm.cost = entity;
        vm.clear = clear;
        vm.save = save;
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.cost.id !== null) {
                CostsService.update(vm.cost, onSaveSuccess, onSaveError);
            } else {
                CostsService.create(vm.cost, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
