(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalDetailController', ProfessionalDetailController);

    ProfessionalDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ProfessionalsService', 'ObjectLabelsService','URLS'];

    function ProfessionalDetailController($scope, $rootScope, $stateParams, previousState, entity, ProfessinalsService, ObjectLabelsService, URLS) {
        var vm = this;

        vm.professional = entity;
        vm.previousState = previousState.name;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.professional.uuid;
        vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.professional.uuid });
        console.log(vm.objectLabels);
    }
})();
