(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("ProfessionalGalleriesController", ProfessionalGalleriesController);

  ProfessionalGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService"
  ]; //

  function ProfessionalGalleriesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.professional = entity;
    console.log(vm.professional);
    vm.albums = AlbumService.getAlbums({ uuid: vm.professional.uuid });

    vm.previousState = previousState.name;

    var permissions = $localStorage.current_user_permissions;

    // vm.previousState =
    //   permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";
  }
})();
