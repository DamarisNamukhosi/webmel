(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalsController', ProfessionalsController);

    ProfessionalsController.$inject = ['$scope', '$location', '$state', 'professionals', 'URLS','ProfessionalsService','GeneralSkills','GeneralLanguages'];

    function ProfessionalsController($scope, $location, $state, professionals, URLS,ProfessionalsService,GeneralSkills,GeneralLanguages) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.professionals = professionals;
        vm.displayProfessionals= true;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;

        if(vm.professionals.length === 0){
            vm.displayProfessionals = false;
        }


        GeneralSkills.query().$promise.then(function (response){
            angular.forEach(response, function(skill){
                vm.generalSkills.push(skill);
            });
            console.log(vm.generalSkills);
        });


        GeneralLanguages.query().$promise.then(function(response){
          
            angular.forEach(response, function(language){
                vm.generalLanguages.push(language);
            });
            console.log(vm.generalLanguages);
        });
    

    function search(skill,language) {
        if(skill !== null){
            console.log("skill " +skill);
        }
        if(language !== null){
            console.log("language " +language);
        }
        
        //get suppliers if the service (id) is set, will have a positive value -
        if (skill > 0) {
             
            if(language > 0){
                ProfessionalsService.getFiltered(
                {
                    id: $localStorage.current_organisation.id,
                    skill: skill,
                    language:language
                },
                function(data) {
                    vm.professionals = data;
                }
            );
        } else{
            ProfessionalsService.getFiltered(
                {
                    id: $localStorage.current_organisation.id,
                    skill: skill,
                    language:null,
                },
                function(data) {
                    vm.professionals = data;
                }
            );
        }
    }
        else {
            console.log("skill not set");
            if(language > 0){
                ProfessionalsService.getFiltered(
                {
                    id: $localStorage.current_organisation.id,
                    language:language,
                    skill:null
                },
                function(data) {
                    vm.professionals = data;
                }
            );
        } else{
            ProfessionalsService.getFiltered(
                {
                    id: $localStorage.current_organisation.id,
                    language:null,
                    skill:null
                },
                function(data) {
                    vm.professionals = data;
                }
            );
        }
        }
        console.log('after filtering');
        console.log(vm.professionals);
    }
    }
})();
