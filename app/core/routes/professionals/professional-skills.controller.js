(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalSkillsController', ProfessionalSkillsController);

    ProfessionalSkillsController.$inject = ['$scope', '$location', '$state', 'skills','entity'];

    function ProfessionalSkillsController ($scope, $location, $state, skills, entity) {
        var vm = this;

        vm.account = null;
        vm.professional= entity;
        vm.isAuthenticated = null;
        vm.skills = skills;

        vm.records = skills;
        
        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);
        
    }
})();
