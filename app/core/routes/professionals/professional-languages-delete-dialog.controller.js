(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalLanguagesDeleteDialogController',ProfessionalLanguagesDeleteDialogController);

    ProfessionalLanguagesDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ProfessionalsService'];

    function ProfessionalLanguagesDeleteDialogController($uibModalInstance, entity, ProfessionalsService) {
        var vm = this;

        vm.professionalLanguage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            console.log('about to delete : '+id);
            ProfessionalsService.deleteLanguages({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
