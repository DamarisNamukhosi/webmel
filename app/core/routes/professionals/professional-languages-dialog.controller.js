
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalLanguagesDialogController', ProfessionalLanguagesDialogController);

    ProfessionalLanguagesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance','languages','ProfessionalsService','GeneralLanguages'];

    function ProfessionalLanguagesDialogController ($timeout, $scope, $stateParams, $uibModalInstance,languages, ProfessionalsService,GeneralLanguages ) {
        var vm = this;

        vm.professional = languages;
        console.log('inside skills dialog');
        console.log(vm.professional);
        
        vm.clear = clear;
        vm.save = save;
        vm.professionalSkills = GeneralLanguages.query();
        vm.isSaving = false;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.professional.id !== null) {
                ProfessionalsService.updateLanguages(vm.professional, onSaveSuccess, onSaveError);
            } else {
                ProfessionalsService.createLanguages(vm.professional, onSaveSuccess, onSaveError);
            }
        }       

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
