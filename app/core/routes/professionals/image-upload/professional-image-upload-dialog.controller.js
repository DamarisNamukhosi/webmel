(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("ProfessionalImageUploadDialogController", ProfessionalImageUploadDialogController);

    ProfessionalImageUploadDialogController.$inject = [ "$timeout","$scope","$stateParams",
      "$uibModalInstance", "entity", "professional", "ProfessionalsService","Upload", "$localStorage", "URLS"
  ];

    function ProfessionalImageUploadDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, professional, ProfessionalsService,Upload, $localStorage,URLS
  ) {
    var vm = this;

    vm.image = entity;
    vm.professional=professional;
    console.log('professional....');
    console.log(vm.professional);
    vm.clear = clear;
    vm.save = save;
    vm.image_response = null;
    vm.isUploading = false;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      //createMediaItemWithObjectDTO
      var mediaItemWithObjectDTO = {
        "albumType": "PROFILE",
        "caption": vm.professional.name,
        "filename": vm.image_response.fileName,
        "mediaType": "IMAGE",
        "mimeType": vm.image_response.contentType,
        "name": vm.professional.name,
        "objectUuid": vm.professional.uuid
      };

      console.log("mediaItemWithObjectDTO");
      console.log(mediaItemWithObjectDTO);
      ProfessionalsService.uploadProfilePhoto(mediaItemWithObjectDTO, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.upload = function(file, errFiles) {
      vm.isUploading = true;
      vm.f = file;
      vm.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: URLS.BASE_URL + "mediaservice/api/media-items/upload",
          data: { file: file },
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        }
        });

        file.upload.then(
          function(response) {
            vm.image_response = response.data;
            console.log("response data")
            console.log(vm.image_response);
            $timeout(function() {
              file.result = response.data;
            });
          },
          function(response) {

            if (response.status > 0)
              vm.errorMsg = response.status + ": " + response.data;
          },
          function(evt) {
            file.progress = Math.min(
              100,
              parseInt(100.0 * evt.loaded / evt.total)
            );
          }
        );
      }

      vm.isUploading = false;
    };
  }
})();
