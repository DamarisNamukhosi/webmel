(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalLanguagesController', ProfessionalLanguagesController);

    ProfessionalLanguagesController.$inject = ['$scope', '$location', '$state', 'languages', 'entity'];

    function ProfessionalLanguagesController ($scope, $location, $state, languages, entity) {
        var vm = this;

        vm.account = null;
        vm.professional = entity;
        vm.isAuthenticated = null;
        vm.languages = languages;

        vm.records = languages;
        console.log('inside languagest');
        console.log(vm.records);
        
        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);
        
    }
})();
