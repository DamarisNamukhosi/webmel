(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalContactsController', ProfessionalContactsController);

    ProfessionalContactsController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'contacts', 'entity','ContactsService'];

    function ProfessionalContactsController($scope, $rootScope, $stateParams, previousState, contacts, entity,ContactsService) {
        var vm = this;

        vm.professional = entity;
        console.log('vm.professional..');
        console.log(vm.professional);
        vm.contacts = contacts;
        console.log('contacts');    
        console.log(vm.contacts);    

        vm.address=[];
        angular.forEach(vm.contacts, function(contact){
        ContactsService.getProfessionalContactAdresses({contactId: contact.id,}).$promise.then(function(result) {
        vm.address=result;
        });
            console.log('address');    
            console.log(vm.address);            
        }); 
        console.log('address');    
        console.log(vm.address);    
        vm.noRecordsFound = (vm.address === undefined || vm.address.length === 0);

        console.log('noRecordsFound');    
        console.log(vm.noRecordsFound);  
        console.log($stateParams);
        vm.organizationId = $stateParams.id;
        vm.previousState = previousState.name;
    }
})();