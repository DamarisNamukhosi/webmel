(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('professionals', {
                parent: 'app',
                url: '/professionals',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MASTER_USER']
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professionals.html',
                        controller: 'ProfessionalsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    professionals: [
                        '$stateParams',
                        '$localStorage',
                        'ProfessionalsService',
                        function($stateParams,$localStorage, ProfessionalsService) {
                            return ProfessionalsService.getFiltered({
                                id:$localStorage.current_organisation.id,
                                language:null,
                                skill:null
                            }).$promise;
                        }
                    ]
                }
            })
            .state('professionals-own', {
                parent: 'app',
                url: '/professionals-own',
                data: {
                    requiresAuthentication: true,
                    authorities: [
                        'TRANSPORTER_USER',
                        'PROFESSIONAL_USER',
                        'T_OPERATOR_USER',
                        'MANAGE_PROFESSIONALS'
                    ],
                    pageTitle: 'profsApp.professional.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professionals-own.html',
                        controller: 'ProfessionalsOwnController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    professionals: [
                        '$stateParams',
                        '$localStorage',
                        'ProfessionalsService',
                        function(
                            $stateParams,
                            $localStorage,
                            ProfessionalsService
                        ) {
                            return ProfessionalsService.getFiltered({
                                id:$localStorage.current_organisation.id,
                                language:null,
                                skill:null
                            }).$promise;
                        }
                    ]
                }
            })
            .state('professionals-own.new', {
                parent: 'professionals-own',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'profsApp.professional.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-dialog.html',
                                controller: 'ProfessionalDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            id: null,
                                            uuid: null,
                                            name: null,
                                            brief: null,
                                            description: null,
                                            notes: null,
                                            dateOfBirth: null,
                                            idNumber: null,
                                            profileStatus: 'ACTIVE',
                                            contentStatus: 'DRAFT',
                                            statusReason:
                                                'A new professional created',
                                            organisationId:
                                                $localStorage
                                                    .current_organisation.id,
                                            organisationName: null,
                                            professionalTypeId: null,
                                            professionalTypeName: null
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professionals-own', null, {
                                        reload: 'professionals-own'
                                    });
                                },
                                function() {
                                    $state.go('professionals-own');
                                }
                            );
                    }
                ]
            })
            .state('professionals.new', {
                parent: 'professionals',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'profsApp.professional.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-dialog.html',
                                controller: 'ProfessionalDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            id: null,
                                            uuid: null,
                                            name: null,
                                            brief: null,
                                            description: null,
                                            notes: null,
                                            dateOfBirth: null,
                                            idNumber: null,
                                            profileStatus: 'ACTIVE',
                                            contentStatus: 'DRAFT',
                                            statusReason:
                                                'A new professional created',
                                            organisationId: null,
                                            organisationName: null,
                                            professionalTypeId: null,
                                            professionalTypeName: null
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professionals', null, {
                                        reload: 'professionals'
                                    });
                                },
                                function() {
                                    $state.go('professionals');
                                }
                            );
                    }
                ]
            })
            .state('professional-detail', {
                parent: 'professionals',
                url: '/{id}/overview',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'profsApp.professional.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-detail.html',
                        controller: 'ProfessionalDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'professionals',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professionals-own-detail', {
                parent: 'professionals-own',
                url: '/{id}/overview',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'profsApp.professional.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-own-detail.html',
                        controller: 'ProfessionalDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name:
                                    $state.current.name || 'professionals-own',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-detail.delete', {
                parent: 'professional-detail',
                url: '/{id}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-delete-dialog.html',
                                controller:
                                    'ProfessionalDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-detail', null, {
                                        reload: 'professional-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-own-detail.delete', {
                parent: 'professional-own-detail',
                url: '/{id}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-delete-dialog.html',
                                controller:
                                    'ProfessionalDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-own-detail', null, {
                                        reload: 'professional-own-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professionals.edit', {
                parent: 'professionals-own',
                url: '/{id}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-dialog.html',
                                controller: 'ProfessionalDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professionals-own', null, {
                                        reload: 'professionals-own'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professionals-own.edit', {
                parent: 'professionals-own-detail',
                url: '/{id}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-dialog.html',
                                controller: 'ProfessionalDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professionals-own-detail',
                                        null,
                                        {
                                            reload: 'professionals-own-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professionals.delete', {
                parent: 'professionals',
                url: '/{id}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-delete-dialog.html',
                                controller:
                                    'ProfessionalDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professionals', null, {
                                        reload: 'professionals'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professionals-own.delete', {
                parent: 'professionals-own',
                url: '/{id}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-delete-dialog.html',
                                controller:
                                    'ProfessionalDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professionals-own', null, {
                                        reload: 'professionals-own'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-skills', {
                parent: 'professionals',
                url: '/{id}/skills',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-skills.html',
                        controller: 'ProfessionalSkillsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    skills: [
                        '$stateParams',
                        '$localStorage',
                        'ProfessionalsService',
                        function(
                            $stateParams,
                            $localStorage,
                            ProfessionalsService
                        ) {
                            return ProfessionalsService.getProfessionalSkills({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'app',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-skills-detail', {
                parent: 'professional-skills',
                url: '/{id}/skills',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-skills-detail.html',
                        controller: 'ProfessionalSkillsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    skills: [
                        '$stateParams',
                        '$localStorage',
                        'ProfessionalsService',
                        function(
                            $stateParams,
                            $localStorage,
                            ProfessionalsService
                        ) {
                            console.log(
                                ProfessionalsService.getProfessionalSkills({
                                    id: $stateParams.id
                                }).$promise
                            );

                            return ProfessionalsService.getProfessionalSkills({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'app',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-skills.edit', {
                parent: 'professional-skills',
                url: '/{skillId}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-skills-dialog.html',
                                controller:
                                    'ProfessionalSkillsDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    skills: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.getSkills(
                                                {
                                                    id: $stateParams.skillId
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-skills', null, {
                                        reload: 'professional-skills'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-skills.delete', {
                parent: 'professional-skills',
                url: '/{skillId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-skills-delete-dialog.html',
                                controller:
                                    'ProfessionalSkillsDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            console.log('deleting skills');
                                            console.log($stateParams.skillId);
                                            return ProfessionalsService.getSkills(
                                                {
                                                    id: $stateParams.skillId
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-skills', null, {
                                        reload: 'professional-skills'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-skills.new', {
                parent: 'professional-skills',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ' '
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-skills-dialog.html',
                                controller:
                                    'ProfessionalSkillsDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    skills: function() {
                                        return {
                                            id: null,
                                            uuid: null,
                                            generalSkillId: null,
                                            generalSkillName: null,
                                            brief: null,
                                            description: null,
                                            level: null,
                                            professionalId: $stateParams.id,
                                            professionalName: null,
                                            yearsOfExperience: null
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-skills', null, {
                                        reload: 'professional-skills'
                                    });
                                },
                                function() {
                                    $state.go('professional-skills');
                                }
                            );
                    }
                ]
            })
            .state('professional-languages', {
                parent: 'professionals',
                url: '/{id}/languages',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'webApp.rooms.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-languages.html',
                        controller: 'ProfessionalLanguagesController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    languages: [
                        '$stateParams',
                        '$localStorage',
                        'ProfessionalsService',
                        function(
                            $stateParams,
                            $localStorage,
                            ProfessionalsService
                        ) {
                            return ProfessionalsService.getProfessionalLanguages(
                                {
                                    id: $stateParams.id
                                }
                            ).$promise;
                        }
                    ],
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'app',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-languages.edit', {
                parent: 'professional-languages',
                url: '/{languageId}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-languages-dialog.html',
                                controller:
                                    'ProfessionalLanguagesDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    languages: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.getLanguages(
                                                {
                                                    id: $stateParams.languageId
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-languages', null, {
                                        reload: 'professional-languages'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-languages.delete', {
                parent: 'professional-languages',
                url: '/{languageId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-languages-delete-dialog.html',
                                controller:
                                    'ProfessionalLanguagesDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            console.log('deleting lang');
                                            console.log($stateParams.id);
                                            return ProfessionalsService.getLanguages(
                                                {
                                                    id: $stateParams.languageId
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-languages', null, {
                                        reload: 'professional-languages'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-languages.new', {
                parent: 'professional-languages',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/professionals/professional-languages-dialog.html',
                                controller:
                                    'ProfessionalLanguagesDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    languages: function() {
                                        return {
                                            id: null,
                                            uuid: null,
                                            brief: null,
                                            professionalId: $stateParams.id,
                                            professionalName: null,
                                            fluencyLevel: null,
                                            generalLanguageId: null,
                                            generalLanguageName: null,
                                            notes: null
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-languages', null, {
                                        reload: 'professional-languages'
                                    });
                                },
                                function() {
                                    $state.go('professional-languages');
                                }
                            );
                    }
                ]
            })
            .state('professionals.profile', {
                parent: 'professionals-own-detail',
                url: '/image/upload',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/profile/profile-dialog.html',
                                controller:
                                    'ProfileImageUploadDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    profile: function() {
                                        return {
                                            file: null
                                        };
                                    },
                                    entity: [
                                        '$stateParams',
                                        'ProfessionalsService',
                                        function(
                                            $stateParams,
                                            ProfessionalsService
                                        ) {
                                            return ProfessionalsService.getProfessionalById(
                                                {
                                                    id: $stateParams.id
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professionals-own-detail',
                                        null,
                                        {
                                            reload: 'professionals-own-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-contacts', {
                parent: 'professionals',
                url: '/{id}/contacts/{objectId}',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-contacts.html',
                        controller: 'ProfessionalContactsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    contacts: [
                        '$stateParams',
                        '$localStorage',
                        'ContactsService',
                        function($stateParams, $localStorage, ContactsService) {
                            return ContactsService.getProfessionalContacts({
                                objectId: $stateParams.objectId
                            }).$promise;
                        }
                    ],
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'app',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-galleries', {
                parent: 'app',
                url: '/professionals/{id}/gallery',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'Gallery'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-galleries.html',
                        controller: 'ProfessionalGalleriesController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state, $) {
                            var currentStateData = {
                                name: $state.current.name || 'locations-own',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-galleries.new', {
                parent: 'professional-galleries',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-create-dialog.html',
                                controller: 'AlbumCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ],
                                    album: function() {
                                        return {
                                            albumType: 'GENERAL',
                                            caption: null, //album name
                                            coverName: null, //uploaded cover image file name
                                            coverUuid: null, //uploaded cover image
                                            isDefaultAlbum: true, //put option slider
                                            name: null, //album name
                                            objectUuid: null //location uuid
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-galleries', null, {
                                        reload: 'professional-galleries'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-galleries.delete', {
                parent: 'professional-galleries',
                url: '/{albumId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-delete-dialog.html',
                                controller: 'AlbumDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md'
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-galleries', null, {
                                        reload: 'professional-galleries'
                                    });
                                },
                                function() {
                                    $state.go('professional-galleries');
                                }
                            );
                    }
                ]
            })
            .state('professional-album-detail', {
                parent: 'professional-galleries',
                url: '/{albumId}/album',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'Gallery'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/professionals/professional-album-detail.html',
                        controller: 'ProfessionalAlbumDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ProfessionalsService',
                        function($stateParams, ProfessionalsService) {
                            return ProfessionalsService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    album: [
                        '$stateParams',
                        'AlbumService',
                        function($stateParams, AlbumService) {
                            return AlbumService.getAlbum({
                                albumId: $stateParams.albumId
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state, $) {
                            var currentStateData = {
                                name:
                                    $state.current.name ||
                                    'professional-galleries',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('professional-album-detail.edit', {
                parent: 'professional-album-detail',
                url: '/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'professional-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-dialog.html',
                                controller: 'AlbumDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        function(AlbumService) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professional-album-detail',
                                        null,
                                        { reload: 'professional-album-detail' }
                                    );
                                },
                                function() {
                                    $state.go('professional-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('professional-album-detail.makeCoverImage', {
                parent: 'professional-album-detail',
                url: '/{imageId}/cover-image',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-change-cover-image-dialog.html',
                                controller:
                                    'AlbumChangeCoverImageDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        '$stateParams',
                                        function(AlbumService, $stateParams) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professional-album-detail',
                                        null,
                                        {
                                            reload: 'professional-album-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('professional-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('professional-album-detail.deleteImage', {
                parent: 'professional-album-detail',
                url: '/{imageId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-image-delete-dialog.html',
                                controller: 'AlbumImageDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        '$stateParams',
                                        function(AlbumService, $stateParams) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professional-album-detail',
                                        null,
                                        {
                                            reload: 'professional-album-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('professional-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('professional-album-detail.upload', {
                parent: 'professional-album-detail',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-upload-dialog.html',
                                controller: 'AlbumUploadDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        '$stateParams',
                                        'AlbumService',
                                        function($stateParams, AlbumService) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professional-album-detail',
                                        null,
                                        {
                                            reload: 'professional-album-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professionals-own-detail.addLabels', {
                parent: 'professionals-own-detail',
                url: '/{uuid}/{objectType}/add-labels',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    '$localStorage',
                    function($stateParams, $state, $uibModal, $localStorage) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/labels/object-label-create-dialog.html',
                                controller:
                                    'ObjectLabelGroupCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    labelGroups: [
                                        'LabelGroupsService',
                                        function(LabelGroupsService) {
                                            return LabelGroupsService.getLabelGroupsByObjectType(
                                                {
                                                    objectType:
                                                        $stateParams.objectType
                                                }
                                            ).$promise;
                                        }
                                    ],
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ],
                                    savedselectedLabels: [
                                        'ObjectLabelsService',
                                        function(ObjectLabelsService) {
                                            return ObjectLabelsService.getLabelsByObjectId(
                                                { uuid: $stateParams.uuid }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professionals-own-detail',
                                        null,
                                        {
                                            reload: 'professionals-own-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('^');
                                }
                            ); //
                    }
                ]
            })
            .state('professional-detail.addLabels', {
                parent: 'professional-detail',
                url: '/{uuid}/{objectType}/add-labels',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    '$localStorage',
                    function($stateParams, $state, $uibModal, $localStorage) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/labels/object-label-create-dialog.html',
                                controller:
                                    'ObjectLabelGroupCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    labelGroups: [
                                        'LabelGroupsService',
                                        function(LabelGroupsService) {
                                            return LabelGroupsService.getLabelGroupsByObjectType(
                                                {
                                                    objectType:
                                                        $stateParams.objectType
                                                }
                                            ).$promise;
                                        }
                                    ],
                                    entity: [
                                        'ProfessionalsService',
                                        function(ProfessionalsService) {
                                            return ProfessionalsService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ],
                                    savedselectedLabels: [
                                        'ObjectLabelsService',
                                        function(ObjectLabelsService) {
                                            return ObjectLabelsService.getLabelsByObjectId(
                                                {
                                                    uuid: $stateParams.uuid
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-detail', null, {
                                        reload: 'professional-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            ); //
                    }
                ]
            })
            .state('professionals-own-detail.deleteLabel', {
                parent: 'professionals-own-detail',
                url: '/{itemToDelete}/delete-label',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/labels/object-label-delete-dialog.html',
                                controller:
                                    'ObjectLabelGroupDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md'
                            })
                            .result.then(
                                function() {
                                    $state.go(
                                        'professionals-own-detail',
                                        null,
                                        {
                                            reload: 'professionals-own-detail'
                                        }
                                    );
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('professional-detail.deleteLabel', {
                parent: 'professional-detail',
                url: '/{itemToDelete}/delete-label',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/labels/object-label-delete-dialog.html',
                                controller:
                                    'ObjectLabelGroupDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md'
                            })
                            .result.then(
                                function() {
                                    $state.go('professional-detail', null, {
                                        reload: 'professional-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            });
    }
})();
