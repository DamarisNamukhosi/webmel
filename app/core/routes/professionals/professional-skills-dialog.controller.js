
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalSkillsDialogController', ProfessionalSkillsDialogController);

    ProfessionalSkillsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'skills','ProfessionalsService','GeneralSkills'];

    function ProfessionalSkillsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, skills, ProfessionalsService,GeneralSkills ) {
        var vm = this;

        vm.professional = skills;
        console.log('inside skills dialog');
        console.log(vm.professional);
        
        vm.clear = clear;
        vm.save = save;
        vm.professionalSkills = GeneralSkills.query();
        vm.isSaving = false;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.professionalSkills.id !== null) {
                ProfessionalsService.updateSkills(vm.professional, onSaveSuccess, onSaveError);
            } else {
                ProfessionalsService.createSkills(vm.professional, onSaveSuccess, onSaveError);
            }
        }       

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
