(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalDeleteDialogController',ProfessionalDeleteDialogController);

    ProfessionalDeleteDialogController.$inject = ['$uibModalInstance', '$localStorage' ,'entity', 'ProfessionalsService'];

    function ProfessionalDeleteDialogController($uibModalInstance,$localStorage, entity, ProfessionalsService) {
        var vm = this;

        vm.professional = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ProfessionalsService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }

        function confirmDelete (professional) {
            var organizationId = $localStorage.current_organisation.id;
            console.log("Current Org ID: " + organizationId);
            professional.contentStatus = "DELETED";
              ProfessionalsService.update(professional,
                  function () {
                      $uibModalInstance.close(true);
                  });
          }
    }
})();
