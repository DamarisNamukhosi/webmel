
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalDialogController', ProfessionalDialogController);

    ProfessionalDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ProfessionalsService','ProfessionalTypesService'];

    function ProfessionalDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ProfessionalsService,ProfessionalTypesService ) {
        var vm = this;

        vm.professional = entity;
        vm.date = new Date(vm.professional.dateOfBirth);
        vm.clear = clear;
        vm.save = save;
        vm.professionalTypes = ProfessionalTypesService.query();
        vm.isSaving = false;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.professional.id !== null) {
                ProfessionalsService.update(vm.professional, onSaveSuccess, onSaveError);
            } else {
                ProfessionalsService.create(vm.professional, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
