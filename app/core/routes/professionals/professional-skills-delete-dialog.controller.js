(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProfessionalSkillsDeleteDialogController',ProfessionalSkillsDeleteDialogController);

    ProfessionalSkillsDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ProfessionalsService'];

    function ProfessionalSkillsDeleteDialogController($uibModalInstance, entity, ProfessionalsService) {
        var vm = this;

        vm.professionalSkill = entity;

        console.log('delelete dialog');
        console.log(vm.professionalSkill);
        
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            console.log('about to delete : '+id);
            ProfessionalsService.deleteSkill({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
