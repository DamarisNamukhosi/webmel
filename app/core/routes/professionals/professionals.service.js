
(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('ProfessionalsService', ProfessionalsService);

    ProfessionalsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ProfessionalsService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getFiltered': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true, 
            //-suppliers/:orgId?generalService=:genSvcId'
            //'costingservice/api/contracts/get-filtered/?target=:target&generalService=:service&supplier=:supplier&year=:year&name=:name'

            url: URLS.BASE_URL + 'contentservice/api/professionals/general-filter/:id?language=:language&skill=:skill'
          },

          //
          'getProfessionals': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/professionals'
          },
          'getProfessionalContacts': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/contacts/filter-by-object/:objectId'
          },
          'getProfessionalContactAdresses': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/contact-addresses/filter-by-contact/:contactId'
          },

          'getProfessionalLanguages': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/professional-languages/filter-by-professional/:id'
          },
          'getLanguages': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/professional-languages/:id'
          },
          'getProfessionalSkills': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/skills/filter-by-professional/:id'
          },
          'getSkills': {
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/skills/:id'
          },
          
          'getProfessionalsByOrganizationId': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/professionals/filter-by-organisation/:id'
          },
          
          'getProfessionalById': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/professionals/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                    console.log(data);
                    console.log('This is a test');
                }
                return data;
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/professionals/:id'
          },
          'updateSkills': {
            method: 'PUT',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/skills'
        },
        'updateLanguages': {
          method: 'PUT',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/professional-languages'
      },
          'update': {
              method: 'PUT',
              headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/professionals'
          },
          'create': {
              method: 'POST',
              headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/professionals'
          },
          'createSkills': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/skills'
        },

        'createLanguages': {
          method: 'POST',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/professional-languages'
      },
        'deleteSkill': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/skills/:id'
        },

        'deleteLanguages': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/professional-languages/:id'
        },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/professionals/:id'
          },
           'uploadProfilePhoto': {
            method: 'POST',
            headers: {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
          }
      });
    }
})();
