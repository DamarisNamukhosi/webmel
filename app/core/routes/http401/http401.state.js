(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('http401', {
            parent: 'app',
            url: '/http401',
            data: {
                requiresAuthentication: false,
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/http401/http401.html',
                    controller: 'Http401Controller',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();
