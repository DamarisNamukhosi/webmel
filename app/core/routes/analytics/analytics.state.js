(function () {
  'use strict';

  angular
    .module('webApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider.state('analytics', {
      parent: 'app',
      url: '/analytics',
      data: {
        authorities: []
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/analytics/analytics.html',
          controller: 'AnalyticsController',
          controllerAs: 'vm'
        }
      },
      resolve: {
        dashboard: ['$stateParams', 'AnalyticsService', function ($stateParams, AnalyticsService) {
          return AnalyticsService.dashboard().$promise;
        }]
      }
    });
  }
})();
