(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AnalyticsController', AnalyticsController);

  AnalyticsController.$inject = ['$scope', '$location', '$state', 'dashboard'];

    function AnalyticsController ($scope, $location, $state, dashboard) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

      vm.data = dashboard.data;
      vm.formatNumber = formatNumber;

      function formatNumber(num){
        var c = 0;
        var d = ".";
        var t = ",";
        var j;
        var n = num,
          c = isNaN(c = Math.abs(c)) ? 2 : c,
          d = d == undefined ? "." : d,
          t = t == undefined ? "," : t,
          s = n < 0 ? "-" : "",
          i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
          j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
      }


    }
})();
