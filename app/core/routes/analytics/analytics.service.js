(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('AnalyticsService', AnalyticsService);

  AnalyticsService.$inject = ['$resource'];

    function AnalyticsService ($resource) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'dashboard': {
            method: 'GET',
            url: 'data/data.json'
          }
        });
    }
})();
