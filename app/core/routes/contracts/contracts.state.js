(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('contracts.new', {
        parent: 'contracts',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'contracts.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contracts-dialog.html',
                controller: 'ContractsDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      locationId: $localStorage.current_location_id,
                      locationName: null,
                      generalcontractsId: null,
                      generalcontractsName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('contracts', null, {
                    reload: 'contracts'
                  });
                },
                function () {
                  $state.go('contracts');
                }
              );
          }
        ]
      })
      .state('contracts-detail.edit', {
        parent: 'contracts-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contracts-dialog.html',
                controller: 'ContractDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts.edit', {
        parent: 'app',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-dialog.html',
                controller: 'ContractDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('app', null, {
                    reload: 'app'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-detail.image', {
        parent: 'contracts-detail',
        url: '/image/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contracts-image-upload-dialog.html',
                controller: 'ContractsImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  contracts: [
                    '$stateParams',
                    'ContractsService',
                    function (
                      $stateParams,
                      ContractsService
                    ) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.delete', {
        parent: 'contracts-own-detail',
        url: '/{status}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-delete-dialog.html',
                controller: 'ContractDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-galleries', {
        parent: 'app',
        url: '/contracts/{id}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/contracts-galleries.html',
            controller: 'ContractsGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'ContractsService',
            function ($stateParams, ContractsService) {
              return ContractsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'contracts-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('contracts-galleries.new', {
        parent: 'contracts-galleries',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-create-dialog.html',
                controller: 'AlbumCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: 'GENERAL',
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-galleries', null, {
                    reload: 'contracts-galleries'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-galleries.delete', {
        parent: 'contracts-galleries',
        url: '/{albumId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-delete-dialog.html',
                controller: 'AlbumDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('contracts-galleries', null, {
                    reload: 'contracts-galleries'
                  });
                },
                function () {
                  $state.go('contracts-galleries');
                }
              );
          }
        ]
      })
      .state('contracts-album-detail', {
        parent: 'contracts-galleries',
        url: '/{albumId}/album',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/contracts-album-detail.html',
            controller: 'ContractsAlbumDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'ContractsService',
            function ($stateParams, ContractsService) {
              return ContractsService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          album: [
            '$stateParams',
            'AlbumService',
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name ||
                  'contracts-galleries',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('contracts-album-detail.edit', {
        parent: 'contracts-album-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'contracts-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-dialog.html',
                controller: 'AlbumDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AlbumService',
                    function (AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-album-detail', null, {
                    reload: 'contracts-album-detail'
                  });
                },
                function () {
                  $state.go('contracts-album-detail');
                }
              );
          }
        ]
      })
      .state('contracts-album-detail.makeCoverImage', {
        parent: 'contracts-album-detail',
        url: '/{imageId}/cover-image',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
                controller: 'AlbumChangeCoverImageDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-album-detail', null, {
                    reload: 'contracts-album-detail'
                  });
                },
                function () {
                  $state.go('contracts-album-detail');
                }
              );
          }
        ]
      })
      .state('contracts-album-detail.deleteImage', {
        parent: 'contracts-album-detail',
        url: '/{imageId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'locations-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
                controller: 'AlbumImageDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-album-detail', null, {
                    reload: 'contracts-album-detail'
                  });
                },
                function () {
                  $state.go('contracts-album-detail');
                }
              );
          }
        ]
      })
      .state('contracts-album-detail.upload', {
        parent: 'contracts-album-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-upload-dialog.html',
                controller: 'AlbumUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'AlbumService',
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-album-detail', null, {
                    reload: 'contracts-album-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-detail.addLabels', {
        parent: 'contracts-detail',
        url: '/{uuid}/{objectType}/add-labels',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-create-dialog.html',
                controller: 'ObjectLabelGroupCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  labelGroups: [
                    'LabelGroupsService',
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  savedselectedLabels: [
                    'ObjectLabelsService',
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-detail.deleteLabel', {
        parent: 'contracts-detail',
        url: '/{itemToDelete}/delete-label',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-delete-dialog.html',
                controller: 'ObjectLabelGroupDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-detail.addFeature', {
        parent: 'contracts-detail',
        url: '/{uuid}/{objectType}/add-features',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/features/object-feature-create-dialog.html',
                controller: 'ObjectFeatureCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  features: [
                    'ObjectFeaturesService',
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    'ObjectFeaturesService',
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('contracts-detail');
                }
              );
          }
        ]
      })
      .state('contracts-detail.deleteFeature', {
        parent: 'contracts-detail',
        url: '/{itemToDelete}/delete-feature',

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/features/object-feature-delete-dialog.html',
                controller: 'ObjectFeatureDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('contracts-detail', null, {
                    reload: 'contracts-detail'
                  });
                },
                function () {
                  $state.go('contracts-detail');
                }
              );
          }
        ]
      })
      .state('contracts-own', {
        parent: 'app',
        url: '/contracts/{supplierId}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'contracts'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/contracts-own.html',
            controller: 'ContractsOwnController',
            controllerAs: 'vm'
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          services: [
            '$stateParams',
            '$localStorage',
            'GeneralServicesService',
            function (
              $stateParams,
              $localStorage,
              GeneralServicesService
            ) {
              return GeneralServicesService.get().$promise;
            }
          ],
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ]
        }
      })
      .state('contracts-own.newContract', {
        parent: 'contracts-own',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-own-dialog.html',
                controller: 'ContractOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      return {
                        id: null,
                        uuid: null,
                        name: null,
                        targetId: $localStorage.current_organisation.id,
                        issuerId: null,
                        type: 'CONTRACT',
                        startDate: null,
                        endDate: null,
                        contentStatus: 'DRAFT',
                        status: 'DRAFT',
                        marketId: null,
                        currencyId: 204, //USD
                        currencyAbbreviation: "null",
                        statusReason: null,
                        serviceId: null,
                        dimensionSetId: null,
                        dimensionSetName: null,
                        year: null,
                        seasonGroupId: null,
                        supplierId: null,
                          dim1: null,
                          dim2: null,
                          dim3: null,
                          dim4: null,
                          dim5: null,
                          objectType: "PERSON",
                        contractParamDTOList: [{
                          contractId: null,
                          exclusive: false,
                          id: null,
                          notes: null,
                          parameter: null,
                          type: "DIMENSION",
                          uuid: null,
                          value: null
                        }],
                      };
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  organizationTypes: [
                    '$stateParams',
                    'OrganizationTypesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      OrganizationTypesService,
                      $localStorage
                    ) {
                      return OrganizationTypesService.query().$promise;
                    }
                  ],
                  markets: null
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own', null, {
                    reload: 'contracts-own'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.editContract', {
        parent: 'contracts-own-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-own-edit-dialog.html',
                controller: 'ContractOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.getFull({
                        id: $stateParams.contractId,
                        versionId: $stateParams.versionId
                      }).$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  organizationTypes: [
                    function () {
                      return [];
                    }
                  ],
                  markets: [
                    '$stateParams',
                    'MarketsService',
                    '$localStorage',
                    function (
                      $stateParams,
                      MarketsService,
                      $localStorage
                    ) {
                      return MarketsService.getByOrganisation({
                        id: $localStorage
                          .current_organisation
                          .id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-copy', {
        parent: 'contracts-own-user',
        url: '/copy',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/copy/contract-copy-dialog.html',
                controller: 'ContractCopyDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'ContractsService',
                    function (ContractsService) {
                      return ContractsService.getFull({
                        id: $stateParams.contractId,
                        versionId: $stateParams.versionId
                      }).$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  organizationTypes: [
                    function () {
                      return [];
                    }
                  ]
                }
              })
          }
        ]
      })
      //
      // .state('contracts-own-statusUpdate', {
      //   parent: 'cost-contracts-detail',
      //   url: '/{status}/update',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: []
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$uibModal',
      //     function ($stateParams, $state, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/contracts/contract-update-status-dialog.html',
      //           controller: 'ContractUpdateStatusDialogController',
      //           controllerAs: 'vm',
      //           backdrop: 'static',
      //           size: 'md',
      //           resolve: {
      //             entity: [
      //               '$localStorage', 'ContractsService',
      //               function ($localStorage, ContractsService) {
      //                 return ContractsService.get({
      //                   id: $stateParams.contractId
      //                 }).$promise;
      //               }
      //             ]
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go('cost-contracts-detail', null, {
      //               reload: 'cost-contracts-detail '
      //             });
      //           },
      //           function () {
      //             $state.go('^');
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('contracts-own-statusUpdate', {
        parent: 'contracts-own-detail',
        url: '/{status}/update',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-update-status-dialog.html',
                controller: 'ContractUpdateStatusDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage', 'ContractsService',
                    function ($localStorage, ContractsService) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail '
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })

      .state('contracts-own-list', {
        parent: 'app',
        url: '/contracts/{supId}/list',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'contracts'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/contracts-own-list.html',
            controller: 'ContractsOwnListController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            '$localStorage',
            'ContractsService',
            function (
              $stateParams,
              $localStorage,
              ContractsService
            ) {
              return ContractsService.getFiltered({
                orgId: $localStorage.current_organisation.id,
                supId: $stateParams.orgId
              }).$promise;
            }
          ],
          suppliers: [
            '$stateParams',
            '$localStorage',
            'ContractsService',
            function (
              $stateParams,
              $localStorage,
              ContractsService
            ) {
              return ContractsService.getSupplier({
                orgId: $localStorage.current_organisation.id
              }).$promise;
            }
          ],
          service: [
            '$stateParams',
            '$localStorage',
            'GeneralServicesService',
            function (
              $stateParams,
              $localStorage,
              GeneralServicesService
            ) {
              return GeneralServicesService.get().$promise;
            }
          ]
        }
      })
      .state('contracts-own-list.newContract', {
        parent: 'contracts-own-list',
        url: 'contracts/{supId}/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/contract-own-dialog.html',
                controller: 'ContractOwnDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      return {
                        id: null,
                        uuid: null,
                        name: null,
                        targetId: $localStorage.current_organisation.id,
                        issuerId: null,
                        type: 'CONTRACT',
                        startDate: null,
                        endDate: null,
                        contentStatus: 'DRAFT',
                        status: 'ACTIVE',
                        marketId: null,
                        currencyId: 204,
                        currencyAbbreviation: null,
                        statusReason: null,
                        serviceId: $stateParams.id,
                        dimensionSetId: null,
                        dimensionSetName: null,
                        year: null,
                        seasonGroupId: null
                      };
                    }
                  ],
                  markets: [
                    '$stateParams',
                    'MarketsService',
                    '$localStorage',
                    function (
                      $stateParams,
                      MarketsService,
                      $localStorage
                    ) {
                      return MarketsService.getByOrganisation({
                        id: $localStorage
                          .current_organisation
                          .id
                      }).$promise;
                    }
                  ],
                  currency: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-list', null, {
                    reload: 'contracts-own-list'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })

      .state('contracts-own-detail', {
        parent: 'contracts-own',
        url: '/{contractId}/{versionId}/{setId}/overview/{seasonGroupId}/{year}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/new/contract-detail.html',
            controller: 'ContractDetail2Controller',
            // templateUrl: 'core/routes/contracts/contract-detail.html',
            // controller: 'ContractDetailController',
            // templateUrl: 'core/routes/contracts/contract-4D-detail.html',
            // controller: 'ContractUserDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            '$localStorage',
            'ContractsService',
            function (
              $stateParams,
              $localStorage,
              ContractsService
            ) {
              return ContractsService.getFull({
                id: $stateParams.contractId,
                versionId: $stateParams.versionId
              }).$promise;
            }
          ],
          dimensionSet: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getSetDimensionExistenciesBySetId({
                id: $stateParams.setId,
                seasonGroupId: null,
                year: $stateParams.year
              }).$promise;
            }
          ],
          dimensions: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getAllDimensions().$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'contracts-own-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }

      })
      // .state('contracts-detail-new', {
      //   parent: 'contracts-own',
      //   url: '{contractId}/{setId}/overview/{seasonGroupId}/{year}/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: [],
      //     pageTitle: 'Cost Details'
      //   },
      //   views: {
      //     'content@': {
      //       templateUrl: 'core/routes/contracts/new/contract-detail.html',
      //       controller: 'ContractDetail2Controller',
      //       controllerAs: 'vm'
      //     }
      //   },
      //   resolve: {
      //     entity: [
      //       '$stateParams',
      //       '$localStorage',
      //       'ContractsService',
      //       function (
      //         $stateParams,
      //         $localStorage,
      //         ContractsService
      //       ) {
      //         return ContractsService.getFull({
      //           id: $stateParams.contractId
      //         }).$promise;
      //       }
      //     ],
      //     dimensionSet: [
      //       '$stateParams',
      //       '$localStorage',
      //       'DimensionSetsService',
      //       function (
      //         $stateParams,
      //         $localStorage,
      //         DimensionSetsService
      //       ) {
      //         return DimensionSetsService.getSetDimensionExistenciesBySetId({
      //           id: $stateParams.setId,
      //           seasonGroupId: $stateParams.seasonGroupId,
      //           year: $stateParams.year
      //         }).$promise;
      //       }
      //     ],
      //     dimensions: [
      //       '$stateParams',
      //       '$localStorage',
      //       'DimensionSetsService',
      //       function (
      //         $stateParams,
      //         $localStorage,
      //         DimensionSetsService
      //       ) {
      //         return DimensionSetsService.getAllDimensions().$promise;
      //       }
      //     ],
      //     previousState: [
      //       '$state',
      //       function ($state) {
      //         var currentStateData = {
      //           name: $state.current.name || 'contracts-own',
      //           params: $state.params,
      //           url: $state.href(
      //             $state.current.name,
      //             $state.params
      //           )
      //         };
      //         return currentStateData;
      //       }
      //     ]
      //   }
      // })
      .state('contracts-own-user', {
        parent: 'contracts-own',
        url: '/{serviceId}/{contractId}/{versionId}/{setId}/overview/user/{seasonGroupId}/{year}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/contracts/new/contract-detail.html',
            controller: 'ContractDetail2Controller',
            // templateUrl: 'core/routes/contracts/contract-user-detail.html',
            // controller: 'ContractDetailController',
            // templateUrl: 'core/routes/contracts/contract-4D-detail.html',
            // controller: 'ContractUserDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            '$localStorage',
            'ContractsService',
            function (
              $stateParams,
              $localStorage,
              ContractsService
            ) {
              return ContractsService.getFull({
                serviceId: $stateParams.serviceId,
                id: $stateParams.contractId,
                versionId: $stateParams.versionId

              }).$promise;
            }
          ],
          dimensionSet: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getSetDimensionExistenciesBySetId({
                id: $stateParams.setId,
                seasonGroupId: null,
                year: $stateParams.year,
              }).$promise;
            }
          ],
          dimensions: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function (
              $stateParams,
              $localStorage,
              DimensionSetsService
            ) {
              return DimensionSetsService.getAllDimensions().$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'contracts-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }

      })

      .state('contracts-own-detail.newSupplement', {
        parent: 'contracts-own-detail',
        url: '/{dimensionId}/{currencyId}/{organisationId}/new-supplement/{dimensionType}',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/supplements/supplement-dialog.html',
                controller: 'SupplementDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {

                      $stateParams.editing = false;
                      return [{
                        billingType: "VALUE",
                        contentStatus: "DRAFT",
                        contractId: $stateParams.contractId,
                        currencyId: $stateParams.currencyId,
                        description: null,
                        dimensionCount: $stateParams.dimensionType == -1 ? 0 : 1,
                        dimensionType1: $stateParams.dimensionType,
                        existence1: null,
                        id: null,
                        mandatory: false,
                        name: null,
                        organisationId: $stateParams.organisationId,
                        supplementType: "SUPPLEMENT",
                        unit: "INDIVIDUAL",
                        uuid: null,
                        value: null,
                        dimensionId: $stateParams.dimensionId,
                        contractVersionId: $stateParams.versionId
                      }];
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.editSupplement', {
        parent: 'contracts-own-user',
        url: '/{organisationId}/{supplementId}/edit-supplement',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $stateParams.editing = true;
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/supplements/supplement-dialog.html',
                controller: 'SupplementDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'SupplementsService',
                    function (
                      $localStorage,
                      SupplementsService
                    ) {
                      return SupplementsService.getById({
                        id: $stateParams.supplementId
                      }).$promise;
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-user', null, {
                    reload: 'contracts-own-user'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.deleteSupplement', {
        parent: 'contracts-own-user',
        url: '/{type}/{supplementId}/delete-supplement',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            console.log('supplementId');
            console.log($stateParams.supplementId);
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/parameters/contract-parameter-delete.html',
                controller: 'ContractParameterDeleteController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$stateParams',
                    'SupplementsService',
                    function ($localStorage, SupplementsService) {
                      var exclusive = false;
                      if ($stateParams.type == 'DIMENSION') {
                        exclusive = true;
                      }
                      return SupplementsService.getById({
                        id: $stateParams.supplementId
                      }).$promise;
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ],
                  dimensions: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getAllDimensions().$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-user', null, {
                    reload: 'contracts-own-user'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.newParameter', {
        parent: 'contracts-own-detail',
        url: '/{type}/new-parameter',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/parameters/contract-parameter-dialog.html',
                controller: 'ContractParameterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      var exclusive = false;
                      if ($stateParams.type == 'DIMENSION') {
                        exclusive = true;
                      }
                      return {
                        id: null,
                        type: $stateParams.type,
                        parameter: null,
                        value: null,
                        exclusive: exclusive,
                        notes: null,
                        uuid: null,
                        contractId: $stateParams.contractId,
                        contractVersionId: $stateParams.versionId
                      };
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ],
                  dimensions: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getAllDimensions().$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.editParameter', {
        parent: 'contracts-own-user',
        url: '/{type}/edit-parameter/{paramId}/{year}',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/parameters/contract-parameter-dialog.html',
                controller: 'ContractParameterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$stateParams',
                    'ContractParametersService',
                    function ($localStorage, ContractParametersService) {
                      return ContractParametersService.getById({
                        id: $stateParams.paramId
                      }).$promise;
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ],
                  dimensions: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getAllDimensions().$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-user', null, {
                    reload: 'contracts-own-user'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })

      .state('contracts-own-detail.deleteParameter', {
        parent: 'contracts-own-user',
        url: '/{type}/{paramId}/delete-parameter',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/parameters/contract-parameter-delete.html',
                controller: 'ContractParameterDeleteController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$stateParams',
                    'ContractParametersService',
                    function ($localStorage, ContractParametersService) {
                      return ContractParametersService.getById({
                        id: $stateParams.paramId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-user', null, {
                    reload: 'contracts-own-user'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('contracts-own-detail.image', {
        parent: 'contracts-own-detail',
        url: '/image/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/profile/profile-dialog.html',
                controller: 'ProfileImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  profile: function () {
                    return {
                      file: null
                    };
                  },
                  entity: [
                    '$stateParams',
                    'ContractsService',
                    function (
                      $stateParams,
                      ContractsService
                    ) {
                      return ContractsService.get({
                        id: $stateParams.contractId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('contracts-own-detail', null, {
                    reload: 'contracts-own-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      });
  }
})();
