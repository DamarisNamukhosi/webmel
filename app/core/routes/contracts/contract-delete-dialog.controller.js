(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractDeleteDialogController',ContractDeleteDialogController);

        ContractDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ContractsService'];

    function ContractDeleteDialogController($uibModalInstance, entity, ContractsService) {
        var vm = this;

        vm.contract = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(contract) {
          contract.contentStatus = "DELETED";
          ContractsService.update(contract, function() {
            $uibModalInstance.close(true);
          });
        }
    }
})();
