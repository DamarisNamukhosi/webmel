(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractUserDetailController', ContractUserDetailController);

  ContractUserDetailController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$localStorage',
    '$state',
    'previousState',
    'entity',
    'ContractsService',
    'URLS',
    'dimensionSet',
    'RatesService',
    'dimensions',
    'SupplementsService'
  ];

  function ContractUserDetailController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $state,
    previousState,
    entity,
    ContractsService,
    URLS,
    dimensionSet,
    RatesService,
    dimensions,
    SupplementsService
  ) {
    var vm = this;
    //injected values
    vm.contract = entity;
    vm.dimensionSet = dimensionSet;
    vm.dimensions = dimensions;
    //$state.go($state.current.name, {}, {reload: true});

    //declare functions
    vm.initSupplementDimensions = initSupplementDimensions;
    vm.filterSupplements = filterSupplements;
    vm.resolveExistenceName = resolveExistenceName;
    vm.resolveDimensionNameByType = resolveDimensionNameByType;
    vm.newSupplements = vm.contract.supplements.length == 0;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.contract.uuid;
    vm.cancel = cancel;
    //initialization
    vm.currentState = $state.current;

    vm.rates = [];
    $scope.isEdit = false;
    vm.supplementDimensions = [];
    vm.dimensionExclusiveParameters = [];
    vm.basicParameters = [];
    vm.notesParameters = [];
    vm.discountParameters = [];
    vm.timeParameters = [];
    vm.cancellationParameters = [];
    vm.newRates = [];
    vm.ratesAvailable = false;
    vm.oldRates = [];
    vm.value = [];
    vm.myStack = [];
    vm.dimensionNotExclusiveParameters = [];
    vm.dimensionNotExclusiveParameterGroups = [];
    vm.firstRow = vm.dimensionSet[0].existenceDTOList;
    vm.dimLength = '';
    vm.getRatePosition = getRatePosition;
    vm.getRatePositionWithCreate = getRatePositionWithCreate;
    vm.updateSpecialSeasonDates = updateSpecialSeasonDates;
    vm.thirdItem = {
      type: 'BASIC',
      existenceDTOList: [{
        id: 11,
        uuid: '00d50256-5675-4a38-a758-cddf3b79fb0d',
        name: 'Default',
        brief: null
      }]
    };

    var i = 0;

    var j = 0;
    vm.y = [1, 2, 3];
    vm.exiList = {};
    vm.exitList = [];
    vm.basicDim = [];
    vm.basicDimId = [];

    vm.testArray = [];
    //call functions needed to initialize data 
    initParameters();
    initSupplementDimensions();
    initRates();
    $scope.table = buildTable();



    $scope.saving = function (figures, edit) {

      vm.fig = figures;
      vm.edit = edit;

      if (vm.edit !== true) {
        console.log("not saving...");
        $scope.isEdit = !edit;
      } else {
        console.log("saving...");
        $scope.isEdit = !edit;

        if (vm.ratesAvailable === false) {
          console.log('no rates... create');
          console.log(vm.contract.rates);
          RatesService.createList(vm.contract.rates, onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            console.log('success');
            $timeout(function () {
              $state.go($state.current.name, {}, {
                reload: true
              });
            }, 1000);

          }

          function onSaveError() {
            console.log('error  ');
          }
        } else {
          console.log(' rates... update');
          console.log(vm.contract.rates);
          RatesService.updateList(vm.oldRates, onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            console.log('success');
            console.log(result);
            $timeout(function () {
              $state.go($state.current.name, {}, {
                reload: true
              });
            }, 1000);

          }

          function onSaveError() {
            console.log('error  ');
          }
        }
      }
    };

    $scope.removeCol = function ($index) {
      if ($index > -1 && $scope.table.columns.length > 1) {
        $scope.table.columns.splice($index, 1);
        for (var i = 0, len = $scope.table.rows.length; i < len; i++) {
          $scope.table.rows[i].cells.splice($index, 1);
        }
      }
    };

    $scope.removeRow = function ($index) {
      if ($index > -1 && $scope.table.rows.length > 1) {
        $scope.table.rows.splice($index, 1);
      }
    };

    $scope.addCol = function () {
      var len = $scope.table.columns.length,
        rowLen = $scope.table.rows.length;
      $scope.table.columns.push({
        value: 'Col ' + len
      });
      for (var i = 0; i < rowLen; i++) {
        $scope.table.rows[i].cells.push({
          value: 'Call ' + i + ',' + len
        });
      }
    };

    $scope.addRow = function () {
      var row = {
          cells: []
        },
        rowLen = $scope.table.rows.length,
        colLen = $scope.table.columns.length;

      for (var i = 0; i < colLen; i++) {
        row.cells.push({
          value: 'Cell ' + rowLen + ',' + i
        });
      }
      $scope.table.rows.push(row);
    };

    function buildTable() {


      console.log("build table");
      console.log(vm.dimensionSet);
      console.log(vm.basicDim);
      console.log(vm.basicDimId);
      
      return {
        columns: [{
          value: 'Col 0'
        }, {
          value: 'Col 1'
        }, {
          value: 'Col 2'
        }],

        rows: [{
          cells: [{
            value: 'Cell 0,0'
          }, {
            value: 'Cell 0,1'
          }, {
            value: 'Cell 0,2'
          }]
        }, {
          cells: [{
            value: 'Cell 1,0'
          }, {
            value: 'Cell 1,1'
          }, {
            value: 'Cell 1,2'
          }]
        }, {
          cells: [{
            value: 'Cell 2,0'
          }, {
            value: 'Cell 2,1'
          }, {
            value: 'Cell 2,2'
          }]
        }]
      };
    }

    function cancel() {
      console.log('clicked');
      $scope.table = buildTable();
      $scope.isEdit = false;
    }

    ContractsService.getOrganizationPolicy({
      organisationId: vm.contract.issuerId
    }).$promise.then(function (result) {
      vm.policy = result;
    });

    function initSupplementDimensions() {
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.type == 'SUPPLEMENT') {
          vm.supplementDimensions.push(dimension);
          if(dimension.dimensionType == "MEAL_PLAN"){
            vm.mealPlanDim =  dimension;
            if(dimension.existenceDTOList.length > 1){
              console.log("dimension.existenceDTOList");
              console.log(dimension.existenceDTOList);
              angular.forEach(dimension.existenceDTOList, function(ex){
                console.log("ex");
                console.log(ex);
                if(vm.basicParameters.length > 0){
                angular.forEach(vm.basicParameters, function(param){
                  console.log("param");
                  console.log(param);
                  if(param.parameter == "MEAL_PLAN" && param.value == ex.id){
                    vm.defaultMealPlan  = ex;
                    console.log("vm.defaultMealPlan");
                    console.log(vm.defaultMealPlan);
                  }
                });
              }
              });
              
            }
            
          }
        }
      });
      console.log("vm.mealPlanDim");
      console.log(vm.mealPlanDim);
    }

    function initParameters() {
      console.log("init params");
      if (vm.contract.params.length > 0) {
        angular.forEach(vm.contract.params, function (parameter) {
          if (parameter.type == "DIMENSION") {
            if (parameter.exclusive == true) {
              vm.dimensionExclusiveParameters.push(parameter);
            } else {
              vm.basicParameters.push(parameter);
            }
            //vm.dimensionExclusiveParameters.push(parameter);
          } else if (parameter.type == "NOTE") {
            vm.notesParameters.push(parameter);
          } else if (parameter.type == "DISCOUNT") {
            vm.discountParameters.push(parameter);
          } else if (parameter.type == "ARRIVAL_AND_CHECKOUT") {
            vm.timeParameters.push(parameter);
          } else if (parameter.type == "CANCELLATION_POLICY") {
            vm.cancellationParameters.push(parameter);
          }
          //    vm.noteCategory = ["DIMENSION", "BASIC", "NOTE", "CANCELLATION_POLICY", "DISCOUNT", "ARRIVAL_AND_CHECKOUT" ];

        });
      } else {
        var temp = [];
        var count = 0;
        var groupSize = 2; // number of parameters in the basic information table

        //categorize the basi info parameters into groups
        angular.forEach(vm.dimensionNotExclusiveParameters, function (parameter) {

          var valueTmp = {};
          valueTmp.type = "value";
          valueTmp.value = parameter.value;
          valueTmp.dimensionType = parameter.parameter;

          var parameterTmp = {};
          parameterTmp.type = "parameter";
          parameterTmp.value = parameter.parameter;

          if ((count % groupSize) == 0 && count > 0) {
            vm.dimensionNotExclusiveParameterGroups.push(temp);
            temp = [];
          }

          temp.push(parameterTmp);
          temp.push(valueTmp);

          count = count + 1; //increment count 
        });

        if (temp.length > 0) {
          vm.dimensionNotExclusiveParameterGroups.push(temp);
        }

      }
    }

    function initRates() {
      //check if there are rates
      if (vm.contract.rates.length === 0) {
        vm.ratesAvailable = false;
        console.log("no rates");
        initDimensions();
      

      } else {
        //if rates are present...
        vm.ratesAvailable = true;
        vm.oldRates = entity.rates;
        angular.forEach(vm.oldRates, function (rate) {
          vm.value.push(rate.value);
        });
        console.log("edit rates");
        initDimensions();
       

        // vm.dimLength = vm.basicDim.length;
        // console.log("vm.dimLength");
        // console.log(vm.dimLength);
        // if (vm.dimLength === 3) {
        //   vm.inner = _.chunk(
        //     vm.value,
        //     vm.dimensionSet[0].existenceDTOList.length *
        //     vm.dimensionSet[1].existenceDTOList.length
        //   );
        //   vm.figures = [];

        //   angular.forEach(vm.inner, function (obj) {
        //     vm.figures.push(
        //       _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
        //     );
        //   });
        // } else if (vm.dimLength === 2) {
        //   vm.inner = vm.value;
        //   vm.figures = [];
        //   vm.figures.push(
        //     _.chunk(
        //       vm.inner,
        //       vm.dimensionSet[0].existenceDTOList.length
        //     )
        //   );
        // } else if (vm.dimLength == 1) {
        //   vm.inner = vm.value;
        //   vm.figures = [];

        //   vm.figures.push(vm.inner);
        // }
      }
    } //end initialization of rates
    function initDimensions(){
     
      angular.forEach(vm.dimensionSet, function (dimension) {
        
        i++;
       if (i == 3) {
          vm.thirdItem = dimension;
       }
        if (dimension.type == 'BASIC') {
          vm.basicDim.push(dimension);

          vm.basicDimId.push(dimension.id);
          if(dimension.dimensionType == "ROOM_OCCUPANCY"){
            vm.roomOccupancyDim =  dimension;
          }else if(dimension.dimensionType == "SEASON" || dimension.dimensionType == "DAY_BAND"){
            vm.seasonDim =  dimension;
            //DAY_BAND
          }else if(dimension.dimensionType == "ROOM_CATEGORY"){
            vm.roomCategoryDim =  dimension;
          }else if(dimension.dimensionType == "MEAL_PLAN"){
            vm.mealPlanDim =  dimension;
            console.log("vm.mealPlanDim");
            console.log(vm.mealPlanDim);

          }
          vm.basicExist = dimension.existenceDTOList;

          vm.exiList[j] = [];
          angular.forEach(vm.basicExist, function (ex) {
            vm.exiList[j].push(ex.id);
            vm.exitList.push(ex.id);
          });
          j++;
        }
      });

      vm.dimLength = vm.basicDim.length;
      if (vm.dimLength == 3) {
        vm.inner = _.chunk(
          vm.value,
          vm.dimensionSet[0].existenceDTOList.length *
          vm.dimensionSet[1].existenceDTOList.length
        );
        vm.figures = [];

        angular.forEach(vm.inner, function (obj) {
          vm.figures.push(
            _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
          );
        });
      } else if (vm.dimLength == 2) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(
          _.chunk(
            vm.inner,
            vm.dimensionSet[0].existenceDTOList.length
          )
        );

      } else if (vm.dimLength == 1) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(vm.inner);

      }
    }
    function filterSupplements(dimensionType) {
      var list = [];

      // if (dimensionType == -1) {
      //   dimensionType = null;
      // }

      angular.forEach(vm.contract.supplements, function (supplement) {
        if (supplement.dimensionType1 == dimensionType) {
          list.push(supplement);
        }
      });
      return list;

    }


    function resolveDimensionNameByType(type) {
      var name = type;
      angular.forEach(vm.dimensions, function (dimension) {
        if (dimension.type == type) {
          name = dimension.name;
        }
      });

      return name;
    }


    function resolveExistenceName(dimensionType, existenceId) {
      var name = existenceId;
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.dimensionType == dimensionType) {
          angular.forEach(dimension.existenceDTOList, function (existence) {

            if (existence.id == existenceId) {
              name = existence.name;
            }

          });
        }
      });
      return name;
    }

    function updateSpecialSeasonDates(supplement) {
      var exist = supplement.existence1;
      var supplementType = supplement.dimensionType1;
      console.log(exist);
      console.log(vm.dimensionSet);
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (supplementType == dimension.dimensionType) {
          vm.existencies = dimension.existenceDTOList;
          angular.forEach(vm.existencies, function (existence) {
            if (exist == existence.id) {
              // if( vm.existenceLabel === "Special Day" ){
              //   vm.entity.description = existence.brief;
              // }
              supplement.description = existence.brief;
              SupplementsService.update(supplement, onSupplementSaveSuccess, onSupplementSaveError);

            }
          });
        }
      })


    }

    function onSupplementSaveSuccess(result) {
      console.log("supplement updated successfully");
      $state.reload();
    }

    function onSupplementSaveError() {
      console.log("error updating.");
    }


    function getRatePosition(existence1, existence2, existence3, existence4) {
      console.log("getRatePosition");
      //console.log(existence1 +" " +existence2 +" " +existence3+" " +existence4);
      var position = null;
      var dimLength = vm.basicDim.length;
      angular.forEach(vm.contract.rates, function (rate) {
        if (vm.basicDim.length == 2) {
          if (rate.dim1 === existence1 && rate.dim2 === existence2) {
            position = vm.contract.rates.indexOf(rate);

          }
        } else if (vm.basicDim.length == 3) {
          if (rate.dim1 === existence1 && rate.dim2 === existence2 && rate.dim3 === existence3) {
            position = vm.contract.rates.indexOf(rate);
          }
        }
        else if (vm.basicDim.length == 4) {
          if (rate.dim1 === existence1 && rate.dim2 === existence2 && rate.dim3 === existence3&& rate.dim4 === existence4) {
            position = vm.contract.rates.indexOf(rate);
          }
        }
      })
      return position
    }


    function getRatePositionWithCreate(existence1, existence2, existence3,existence4) {
      var position = null;
      var dimLength = vm.basicDim.length;
      console.log("getRatePositionWithCreate")
      position = getRatePosition(existence1, existence2, existence3,existence4);

      var tempRate = null;
      if (position === null) {
        // create a new rate object

        tempRate = {
          "id": null,
          "value": null,
          "dim1": existence1,
          "dim2": existence2,
          "dim3": existence3,
          "dim4": existence4,
          "contractId": vm.contract.id
        }

        vm.contract.rates.push(tempRate);

        position = vm.contract.rates.indexOf(tempRate);
      }

      return position
    }

  }



})();
