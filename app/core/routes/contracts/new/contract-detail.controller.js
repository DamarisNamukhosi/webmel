
(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractDetail2Controller', ContractDetail2Controller);

  ContractDetail2Controller.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$localStorage',
    '$state',
    '$timeout',
    'previousState',
    'entity',
    'ContractsService',
    'URLS',
    'dimensionSet',
    'RatesService',
    'dimensions',
    'SupplementsService',
    'RoomOccupanciesService',
    'DimensionSetsService'
  ];

  function ContractDetail2Controller(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $state,
    $timeout,
    previousState,
    entity,
    ContractsService,
    URLS,
    dimensionSet,
    RatesService,
    dimensions,
    SupplementsService,
    RoomOccupanciesService,
    DimensionSetsService
  ) {
    var vm = this;

    //injected values
    vm.contract = entity;
    console.log("vm.contract");
    console.log(vm.contract);
    console.log(previousState);
    console.log($state.current.name);
    vm.dimensionSet = dimensionSet;
    vm.dimensions = dimensions;
    vm.previousState = $state.current.name;
    console.log(vm.previousState);

    //declare functions
    vm.filterSupplements = filterSupplements;
    vm.resolveExistenceName = resolveExistenceName;
    vm.resolveDimensionNameByType = resolveDimensionNameByType;
    vm.cancel = cancel;
    vm.shortenName = shortenName;
    vm.initDimensions = initDimensions;
    vm.initDimensionsv2 = initDimensionsv2;
    vm.generateRateKey = generateRateKey;
    vm.isFirstExistence = isFirstExistence;
    vm.getDimension = getDimension;
    vm.ratesResolve = ratesResolve;
    vm.manage = manage;
    vm.editRates = editRates;
    vm.saveRates = saveRates;
    vm.compareWithoutType = compareWithoutType;
    vm.toggleResolutionDetails = toggleResolutionDetails;
    vm.showResolutionDetails = showResolutionDetails;
    vm.truncate = truncate;

    //initialization
    //vm.newSupplements = vm.contract.supplements.length == 0;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.contract.uuid;
    vm.currentState = $state.current;
    vm.rates = [];
    vm.isRatesEdit = false;
    vm.isManage = false;
    vm.userPermissions = $localStorage.current_user_permissions;
    vm.showDeleteContractVersionBtn = false;
    vm.showDeleteVersionBtn = false;
    vm.showNewVersionBtn = false;
    vm.showCopyBtn = true;
    vm.showDraftBtn = false;
    vm.showValidateBtn = false;
    vm.showPublishedBtn = false;
    vm.showBanner = false;
    vm.showEditBtn = false;



    console.log(vm.userPermissions);
    vm.contractVersions = [];
    vm.currentPublished = null;
    vm.supplementDimensions = [];
    vm.basicDimensions = [];
    vm.basicDimensionsPositions = [];
    vm.exclusiveDimensions = [];
    vm.policyDimensions = [];

    vm.maxDimensions = 5; //Dimension support

    vm.dummyBasicDimension = {
      "id": null,
      "type": "BASIC",
      "dimensionType": "DUMMY",
      "existenceDTOList": [{
        "id": null,
        "uuid": null,
        "name": null,
        "brief": null,
        "dimensionType": "DUMMY"
      }]
    };

    vm.tripRateDTO = {
      "startDate": null,
      "inputExistences": [],
      "unitCombinations": [{
        "ages": null,
        "adults": 0,
        "children": 0,
        "infants": 0,
        "youngAdults": 0,
        "aged": 0,
        "specifiedExistences": null,
        "unitType": "ROOM"
      }],
      "supplierId": vm.contract.supplierId,
      "targetId": vm.contract.targetId,
      "marketId": vm.contract.marketId,
      "contractId": vm.contract.contractId,
      "serviceId": vm.contract.serviceId,
      "generalServiceId": vm.contract.generalServiceId
    };


    // the 4Ds to be used in looping on the view - initialized in initDimensions
    vm.basicDimension1 = {};
    vm.basicDimension2 = {};
    vm.basicDimension3 = {};
    vm.basicDimension4 = {};
    vm.basicDimension5 = {};

    vm.ratesMap = {}; //to be used to store the rates

    vm.keyPrefix = "idx";

    vm.defaultSupplementDimensions = [];
    vm.dimensionExclusiveParameters = [];
    vm.basicParameters = [];
    vm.notesParameters = [];
    vm.discountParameters = [];
    vm.timeParameters = [];
    vm.cancellationParameters = [];
    vm.occupancyParameters = [];
    vm.paymentParameters = [];
    vm.specialOfferParameters = [];
    vm.minimumStayParameters = [];
    vm.closedParameters = [];
    vm.singleRoomRateParameters = [];
    vm.dimensionNotExclusiveParameters = [];
    vm.dimensionNotExclusiveParameterGroups = [];


    vm.resolutionDisplayPersonDetails = false;
    vm.resolutionDisplayPersonDetailsIndex = false;

    vm.newRates = [];
    vm.ratesAvailable = false;
    vm.oldRates = [];
    vm.value = [];
    vm.myStack = [];

    vm.firstRow = vm.dimensionSet[0].existenceDTOList;
    vm.dimLength = '';
    vm.getRatePosition = getRatePosition;
    vm.getRatePositionWithCreate = getRatePositionWithCreate;
    vm.updateSpecialSeasonDates = updateSpecialSeasonDates;
    vm.checkIfCredit = checkIfCredit;
    vm.resolveOccupancyName = resolveOccupancyName;
    vm.thirdItem = {
      type: 'BASIC',
      existenceDTOList: [{
        id: 11,
        uuid: '00d50256-5675-4a38-a758-cddf3b79fb0d',
        name: 'Default',
        brief: null
      }]
    };

    var i = 0;

    var j = 0;
    vm.y = [1, 2, 3];
    vm.exiList = {};
    vm.exitList = [];
    vm.basicDim = [];
    vm.basicDimId = [];

    vm.testArray = [];
    vm.threeDSupplements = [];
    vm.twoDExtras = [];
    vm.twoDExistencies = [];
    vm.twoDSupExistencies = [];
    vm.threeDExistencies = [];
    vm.threeDChunks = [];



    //call functions needed to initialize data
    initDimensionsv2();
    initContractVersionActions();
    //initSupplementDimensions();
    initContractResolutionConfigs();
    // initParameters();
    //initRates();
    initRates2();
    initOccupancies();
    initTripRatesDTO();
    //$scope.table = buildTable();




    $scope.saving = function(figures, edit) {

      vm.fig = figures;
      vm.edit = edit;

      if (vm.edit !== true) {
        console.log("not saving...");
        $scope.isEdit = !edit;
      } else {
        console.log("saving...");
        $scope.isEdit = !edit;

        if (vm.ratesAvailable === false) {
          console.log('no rates... create');

          RatesService.createList(vm.contract.rates, onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            console.log('success');
            $timeout(function() {
              $state.go($state.current.name, {}, {
                reload: true
              });
            }, 1000);

          }

          function onSaveError() {
            console.log('error  ');
          }
        } else {
          RatesService.updateList(vm.oldRates, onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);

            $timeout(function() {
              $state.go($state.current.name, {}, {
                reload: true
              });
            }, 1000);

          }

          function onSaveError() {
            console.log('error  ');
          }
        }
      }
    };

    $scope.removeCol = function($index) {
      if ($index > -1 && $scope.table.columns.length > 1) {
        $scope.table.columns.splice($index, 1);
        for (var i = 0, len = $scope.table.rows.length; i < len; i++) {
          $scope.table.rows[i].cells.splice($index, 1);
        }
      }
    };

    $scope.removeRow = function($index) {
      if ($index > -1 && $scope.table.rows.length > 1) {
        $scope.table.rows.splice($index, 1);
      }
    };

    $scope.addCol = function() {
      var len = $scope.table.columns.length,
        rowLen = $scope.table.rows.length;
      $scope.table.columns.push({
        value: 'Col ' + len
      });
      for (var i = 0; i < rowLen; i++) {
        $scope.table.rows[i].cells.push({
          value: 'Call ' + i + ',' + len
        });
      }
    };

    $scope.addRow = function() {
      var row = {
          cells: []
        },
        rowLen = $scope.table.rows.length,
        colLen = $scope.table.columns.length;

      for (var i = 0; i < colLen; i++) {
        row.cells.push({
          value: 'Cell ' + rowLen + ',' + i
        });
      }
      $scope.table.rows.push(row);
    };


    function cancel() {
      console.log('clicked');
      //$scope.table = buildTable();
      $scope.isEdit = false;
    }

    ContractsService.getOrganizationPolicy({
      organisationId: vm.contract.supplierId
    }).$promise.then(function(result) {
      vm.policy = result;
    });

    function initSupplementDimensions() {

      angular.forEach(vm.dimensionSet, function(dimension) {
        if (dimension.type == 'SUPPLEMENT') {
          console.log("SUPPLEMENT ");
          vm.supplementDimensions.push(dimension);
          if (dimension.hasDefault == true) {
            console.log("has default")
            vm.defaultSupplementDimensions.push(dimension);
            if (dimension.existenceDTOList.length > 0) {
              console.log(dimension.existenceDTOList);
              angular.forEach(dimension.existenceDTOList, function(ex) {
                console.log("ex");
                console.log(ex);
                vm.defaultSupDim = ex;
              });

            }

          }
        }
      });
    }

    function initParameters() {
      console.log(vm.exclusiveContractParameters);
      vm.dimensionExclusiveParameters = vm.exclusiveContractParameters;
      if(vm.contract.params != null){
      if (vm.contract.params.length > 0) {
        angular.forEach(vm.contract.params, function(parameter) {
          if (parameter.type == "DIMENSION") {

            if (parameter.exclusive == true) {
              console.log("vm.exclusiveContractParameters");
              console.log(vm.exclusiveContractParameters);
              angular.forEach(vm.exclusiveContractParameters, function(excDim) {
                var index = (vm.exclusiveContractParameters.indexOf(excDim) + 1);
                  var dimX = "dim" + index;
                  if (vm.contract[dimX] != null) {

                    angular.forEach(excDim.existenceDTOList, function(exist) {
                      if (exist.id == vm.contract[dimX]) {
                        excDim.selectedValue = exist.id
                        excDim.selectedName = exist.name
                      }
                    });
                  }
                if (parameter.parameter == excDim.dimensionType) {
                  //vm.dimensionExclusiveParameters.splice(vm.dimensionExclusiveParameters.indexOf(excDim),1);
                } 
              });

            } else {
              vm.basicParameters.push(parameter);
            }

            //vm.dimensionExclusiveParameters.push(parameter);
          } else if (parameter.type == "BASIC") {
            vm.basicParameters.push(parameter);
          } else if (parameter.type == "NOTE") {
            vm.notesParameters.push(parameter);
          } else if (parameter.type == "DISCOUNT") {
            vm.discountParameters.push(parameter);
          } else if (parameter.type == "ARRIVAL_AND_CHECKOUT") {
            vm.timeParameters.push(parameter);
          } else if (parameter.type == "CANCELLATION") {
            vm.cancellationParameters.push(parameter);
          } else if (parameter.type == "PAYMENT") {
            vm.paymentParameters.push(parameter);
          } else if (parameter.type == "OCCUPANCY") {
            vm.occupancyParameters.push(parameter);
          } else if (parameter.type == "MINIMUM_STAY") {
            vm.minimumStayParameters.push(parameter);
          } else if (parameter.type == "SPECIAL_OFFER") {
            vm.specialOfferParameters.push(parameter);
          } else if (parameter.type == "CLOSED") {
            vm.closedParameters.push(parameter);
          } else if (parameter.type == "SINGLE_ROOM_RATE_POLICY") {
            vm.singleRoomRateParameters.push(parameter);
          }
        });
      } else {
        var temp = [];
        var count = 0;
        var groupSize = 2; // number of parameters in the basic information table

        //categorize the basi info parameters into groups
        angular.forEach(vm.dimensionNotExclusiveParameters, function(parameter) {

          var valueTmp = {};
          valueTmp.type = "value";
          valueTmp.value = parameter.value;
          valueTmp.dimensionType = parameter.parameter;

          var parameterTmp = {};
          parameterTmp.type = "parameter";
          parameterTmp.value = parameter.parameter;

          if ((count % groupSize) == 0 && count > 0) {
            vm.dimensionNotExclusiveParameterGroups.push(temp);
            temp = [];
          }

          temp.push(parameterTmp);
          temp.push(valueTmp);

          count = count + 1; //increment count
        });

        if (temp.length > 0) {
          vm.dimensionNotExclusiveParameterGroups.push(temp);
        }
      }
    }
    }


    //sets the rates map to be used in ng-model
    function initRates2() {

      //copy all rates into rates map
      angular.forEach(vm.contract.rates, function(rate) {
        // console.log(rate);
        var key = null;
        angular.forEach(vm.basicDimensions, function(dimension) {
          if (key === null) {
            key = vm.keyPrefix;
          }
          var position = dimension.position;
          //only convert rate if postion is set
          if (position > -1) {
            position = position + 1; // convert from 0-index to 1-index

            var dimX = "dim" + position;
            var dimValue = rate[dimX];

            if (dimension.dimensionType === "DUMMY") {
              dimValue = null;
              rate[dimX] = null;
            }

            // console.log("dimX: " + dimX + ", dimValue: " + dimValue);

            key = key + "_" + dimValue;
          }
        });
        // console.log("key: " + key);

        if (key != null) {
          vm.ratesMap[key] = rate;
        }

      }); // end outer forEach


      //initialize rates based on dimensions (useful for new contracts or contracts whose structure has been modified)
      vm.expandedRatesList = [{
        "key": vm.keyPrefix,
        "value": {
          "id": null,
          "value": null,
          "contractId": vm.contract.id,
          "contractVersionId": $stateParams.versionId
        }
      }];

      //expand the rates from dimensions
      angular.forEach(vm.basicDimensions, function(dimension) {
        var index = vm.basicDimensions.indexOf(dimension);
        vm.expandedRatesList = expandDefaultRates(vm.expandedRatesList, dimension, index);

      });


      //update rates map with expanded list (if values do not exist)
      angular.forEach(vm.expandedRatesList, function(object) {
        var key = object.key;
        var value = object.value;
        var backendValue = vm.ratesMap[key];

        //check if backend has a value
        if (backendValue === null || backendValue === undefined) {
          //use the object from the expanded rates map
          vm.ratesMap[key] = value;
        }

      });

    } //end initRates2


    function expandDefaultRates(inputList, dimension, index) {

      //gets dimension position
      var dimPosition = dimension.position;

      //default to index (position of the dimension in the dimension list - outside the scope of this function)
      if (dimPosition === null || dimPosition === undefined) {
        dimPosition = index;
      }

      //convert position from 0-index to 1-index
      dimPosition = dimPosition + 1;

      //set dimX
      var dimX = "dim" + dimPosition;


      //create a new list
      var myNewList = [];

      angular.forEach(dimension.existenceDTOList, function(existence) {

        var existenceId = existence.id;

        //if dummy dimension - set the type to DUMMY
        if (dimension.dimensionType === "DUMMY") {
          existenceId = null;
        }

        //loop through input list myList
        angular.forEach(inputList, function(inputObject) {

          var inputKey = inputObject.key;
          var inputValue = inputObject.value;

          //create a new key
          var newKey = inputKey + "_" + existenceId;
          var newValue = angular.copy(inputValue);
          newValue[dimX] = existenceId;

          //create a new object to be added to the list
          var newObject = {};
          newObject["key"] = newKey;
          newObject["value"] = newValue;


          // console.log("######################### processing new row################");
          // console.log("existenceId: " + existenceId, "dimX: " + dimX);
          // console.log(newObject);


          //add the new object to the my new list
          myNewList.push(newObject);
        });

      });

      //return the new list
      return myNewList;
    }



    function initRates() {
      //check if there are rates
      if (vm.contract.rates.length === 0) {
        vm.ratesAvailable = false;

        console.log("no rates");
        console.log("calling initDim.");
        initDimensions();
        console.log("initDim called.");

      } else {
        //if rates are present...
        vm.ratesAvailable = true;
        vm.oldRates = entity.rates;
        angular.forEach(vm.oldRates, function(rate) {
          vm.value.push(rate.value);
        });
        console.log("edit rates");
        initDimensions();
      }
    } //end initialization of rates



    function initDimensionsv2() {

      angular.forEach(vm.dimensionSet, function(dimension) {
        if (dimension.type == 'BASIC'|| dimension.type == 'EXCLUSIVE') {
          vm.basicDimensions.push(dimension);
          var position = dimension.position;

          //if position is null, we use the index of the object
          if (position === null || position === undefined) {
            position = vm.basicDimensions.indexOf(dimension);
          }
          //increment position
          position = position + 1;

          vm.basicDimensionsPositions.push(position);
        } else if (dimension.type == 'SUPPLEMENT') {
          vm.supplementDimensions.push(dimension);
        } else if (dimension.type == 'EXCLUSIVE') {
          vm.exclusiveDimensions.push(dimension);
        } else if (dimension.type == 'POLICY') {
          vm.policyDimensions.push(dimension);
        }
      });


      //support auto fill to support upto 4D, auto fill other dimensions
      var noOfBasicDimensions = vm.basicDimensions.length;
      if (noOfBasicDimensions < vm.maxDimensions) {
        var diff = vm.maxDimensions - noOfBasicDimensions;
        for (var i = 0; i < diff; i++) {
          var copyOfDummy = angular.copy(vm.dummyBasicDimension);
          copyOfDummy.position = noOfBasicDimensions + i;
          vm.basicDimensions.push(copyOfDummy)
        }
      }

      //populate dimensions to be used in the view
      angular.forEach(vm.basicDimensions, function(dimension) {
        var index = vm.basicDimensions.indexOf(dimension);
        if (index === 0) {
          vm.basicDimension1 = dimension;
        } else if (index === 1) {
          vm.basicDimension2 = dimension;
        } else if (index === 2) {
          vm.basicDimension3 = dimension;
        } else if (index === 3) {
          vm.basicDimension4 = dimension;
        } else if (index === 4) {
          vm.basicDimension5 = dimension;
        }
      });

    }


    function initDimensions() {
      console.log("initDimensions");
      angular.forEach(vm.dimensionSet, function(dimension) {

        i++;
        if (i == 3) {
          vm.thirdItem = dimension;
          console.log("vm.thirdItem");
          console.log(vm.thirdItem);
        }
        if (dimension.type == 'BASIC') {
          vm.basicDim.push(dimension);

          vm.basicDimId.push(dimension.id);
          //
          vm.basicExist = dimension.existenceDTOList;

          vm.exiList[j] = [];
          angular.forEach(vm.basicExist, function(ex) {
            vm.exiList[j].push(ex.id);
            vm.exitList.push(ex.id);
          });
          j++;
        }
      });

      vm.dimLength = vm.basicDim.length;
      if (vm.dimLength == 3) {
        vm.inner = _.chunk(
          vm.value,
          vm.dimensionSet[0].existenceDTOList.length *
          vm.dimensionSet[1].existenceDTOList.length
        );
        vm.figures = [];

        angular.forEach(vm.inner, function(obj) {
          vm.figures.push(
            _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
          );
        });
      } else if (vm.dimLength == 2) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(
          _.chunk(
            vm.inner,
            vm.dimensionSet[0].existenceDTOList.length
          )
        );

      } else if (vm.dimLength == 1) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(vm.inner);

      }
    }

    function filterSupplements(dimensionType) {
      var list = [];

      // if (dimensionType == -1) {
      //   dimensionType = null;
      // }

      angular.forEach(vm.contract.supplements, function(supplement) {
        if (supplement.dimensionType1 == dimensionType) {
          list.push(supplement);
        }
      });
      return list;

    }


    function resolveDimensionNameByType(type) {
      var name = type;
      angular.forEach(vm.dimensions, function(dimension) {
        if (dimension.type == type) {
          name = dimension.name;
        }
      });

      return name;
    }


    function resolveExistenceName(dimensionType, existenceId) {
      var name = existenceId;
      angular.forEach(vm.dimensionSet, function(dimension) {
        if (dimension.dimensionType == dimensionType) {
          angular.forEach(dimension.existenceDTOList, function(existence) {

            if (existence.id == existenceId) {
              name = existence.name;
            }

          });
        }
      });
      return name;
    }

    function updateSpecialSeasonDates(supplement) {
      var exist = supplement.existence1;
      var supplementType = supplement.dimensionType1;
      console.log(exist);
      console.log(vm.dimensionSet);
      angular.forEach(vm.dimensionSet, function(dimension) {
        if (supplementType == dimension.dimensionType) {
          vm.existencies = dimension.existenceDTOList;
          angular.forEach(vm.existencies, function(existence) {
            if (exist == existence.id) {
              // if( vm.existenceLabel === "Special Day" ){
              //   vm.entity.description = existence.brief;
              // }
              supplement.description = existence.brief;
              SupplementsService.update(supplement, onSupplementSaveSuccess, onSupplementSaveError);

            }
          });
        }
      })


    }

    function onSupplementSaveSuccess(result) {
      console.log("supplement updated successfully");
      $state.reload();
    }

    function onSupplementSaveError() {
      console.log("error updating.");
    }


    function getRatePosition(existence1, position1, existence2, position2, existence3, position3, existence4, position4) {
      var position = null;
      var dimLength = vm.basicDim.length;
      angular.forEach(vm.contract.rates, function(rate) {
        if (vm.basicDim.length === 2) {
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2)) {


            position = vm.contract.rates.indexOf(rate);

            // console.log("position");
            // console.log(position);

          }
        } else if (vm.basicDim.length === 3) {
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2) && rateExists(rate, existence3, position3)) {
            position = vm.contract.rates.indexOf(rate);
          }

        } else if (vm.basicDim.length === 4) {
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2) && rateExists(rate, existence3, position3) && rateExists(rate, existence4, position4)) {
            position = vm.contract.rates.indexOf(rate);
          }
        }

      });
      return position
    }


    function getRatePositionWithCreate(existence1, position1, existence2, position2, existence3, position3, existence4, position4) {


      var position = null;
      var dimLength = vm.basicDim.length;

      position = getRatePosition(existence1, position1, existence2, position2, existence3, position3, existence4, position4);
      // console.log("position");
      // console.log(position);
      var tempRate = null;
      if (position === null) {
        // create a new rate object

        tempRate = {
          "id": null,
          "value": null,
          "dim1": existence1,
          "dim2": existence2,
          "dim3": existence3,
          "dim4": existence4,
          "contractId": vm.contract.id,
          "contractVersionId": $stateParams.versionId
        }

        vm.contract.rates.push(tempRate);

        position = vm.contract.rates.indexOf(tempRate);

      }

      return position
    }

    function checkIfCredit(supplement) {

      if (supplement.value < 0) {
        var nameIndex = supplement.name.search("Supplement");
        vm.isCredit = true;
        if (nameIndex > 0) {
          supplement.name = supplement.name.slice(0, supplement.name.search("Supplement")) + "Credit";
        }
        return Math.abs(supplement.value);
      }

      return supplement.value;

    }

    function resolveOccupancyName(usedOccupancy) {
      var occupancyName = null;
      angular.forEach(vm.roomOccupancies, function(occupancy) {
        if (usedOccupancy == occupancy.id) {
          occupancyName = occupancy.abbreviation;
        }
      });
      return occupancyName;
    }

    function initOccupancies() {
      RoomOccupanciesService
        .getByOrganization({
          id: vm.contract.supplierId
        })
        .$promise.then(function(results) {
          vm.roomOccupancies = results;
        });
    }


    function shortenName(name) {

      return name.substr(0, (name.indexOf("rates") + 5));
    }

    function rateExists(rate, existence, position) {
      position += 1;
      if (position === 1) {
        return existence === rate.dim1;
      } else if (position === 2) {
        return existence === rate.dim2;
      } else if (position === 3) {
        return existence === rate.dim3;
      } else if (position === 4) {
        return existence === rate.dim4;
      } else if (position === 5) {
        return existence === rate.dim5;
      } else if (position === undefined || position === null) {
        return true;
      }

      return false;
    }

    function generateRateKey(existenceId1, existenceId2, existenceId3, existenceId4, existenceId5) {
      return vm.keyPrefix + "_" + existenceId1 + "_" + existenceId2 + "_" + existenceId3 + "_" + existenceId4 + "_" + existenceId5;
    }

    function isFirstExistence(existencesList, existence) {
      var index = existencesList.indexOf(existence);
      return index === 0; //first
    }

    function getDimension(dimensionSet, dimensionType) {
      var result = null;
      angular.forEach(dimensionSet, function(dimension) {
        if (dimension.dimensionType === dimensionType) {
          result = dimension;
        }
      });
      return result;
    }


    function initTripRatesDTO() {
      //set start date
      vm.tripRateDTO.startDate = new Date();
      vm.tripRateDTO.contractId = vm.contract.id;

      //populate form values at the dto level
      angular.forEach(vm.dimensionSet, function(dimension) {
        var formValue = null;
        if (!(dimension.existenceDTOList === null || dimension.existenceDTOList === undefined) && dimension.existenceDTOList.length > 0) {
          formValue = dimension.existenceDTOList[0].id;
        }
        dimension.formValue = formValue;
      });

      //populate age limits default values
      var ageLimitDimension = getDimension(vm.dimensionSet, 'AGE_LIMIT');
      if (!(ageLimitDimension === null || ageLimitDimension === undefined)) {
        angular.forEach(ageLimitDimension.existenceDTOList, function(existence) {
          console.log(existence.generalName + " - age limit");
          if (existence.generalName === 'Infant') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Child') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Young Adult') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Adult') {
            existence.formValue = 1;
          } else if (existence.generalName === 'Aged') {
            existence.formValue = 0;
          }
        });
      }
    }


    function ratesResolve() {

      //reset the result
      vm.tripRateDTOResp = null;

      vm.tripRateDTO.inputExistences = [];

      vm.resolutionDisplayPersonDetails = false;

      //populate vm.tripRateDTO with inputExistences with formValues from dimensions
      angular.forEach(vm.dimensionSet, function(dimension) {
        var formValue = dimension.formValue;
        if (!(dimension.dimensionType === 'ROOM_OCCUPANCY' ||
            dimension.dimensionType === 'SEASON' ||
            dimension.dimensionType === 'DAY_BAND' ||
            dimension.dimensionType === 'AGE_LIMIT' ||
            dimension.dimensionType === 'SPECIAL_DAY')) {

          // console.log("Checking: " + dimension.dimensionName + ", formValue: " + formValue);

          // loop and existence with id same as formValue
          if (!(formValue === null || formValue === undefined)) {
            // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + "formValue is set");
            angular.forEach(dimension.existenceDTOList, function(existence) {
              // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              if (existence.id == formValue) {
                vm.tripRateDTO.inputExistences.push(existence);
                // console.log("MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              } else {
                // console.log("NOT MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              }
            });
          } else {
            // console.log("Matched and added : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + " - " + existence.name + ", formValue is null");
          }
        } else {
          console.log("Ignoring: " + dimension.dimensionName);
        }

      });

      //populate vm.tripRateDTO with age limit values
      var ageLimitDimension = getDimension(vm.dimensionSet, 'AGE_LIMIT');
      if (!(ageLimitDimension === null || ageLimitDimension === undefined)) {
        angular.forEach(ageLimitDimension.existenceDTOList, function(existence) {
          if (existence.generalName === 'Infant') {
            vm.tripRateDTO.unitCombinations[0].infants = existence.formValue;
          } else if (existence.generalName === 'Child') {
            vm.tripRateDTO.unitCombinations[0].children = existence.formValue;
          } else if (existence.generalName === 'Young Adult') {
            vm.tripRateDTO.unitCombinations[0].youngAdults = existence.formValue;
          } else if (existence.generalName === 'Adult') {
            vm.tripRateDTO.unitCombinations[0].adults = existence.formValue;
          } else if (existence.generalName === 'Aged') {
            vm.tripRateDTO.unitCombinations[0].aged = existence.formValue;
          }
        });
      }

      //resolve rates
      RatesService.ratesResolve(vm.tripRateDTO,
        function(data) {
          vm.tripRateDTOResp = data;
        }
      );
    }

    function manage() {
      vm.isManage = !vm.isManage;
      if (vm.isManage) {

      } else {
        //reset the edit rates panel
        vm.isRatesEdit = false;

      }
    }

    function editRates() {
      vm.isRatesEdit = true;
    }


    function saveRates() {

      //prepare rate to save
      var ratesToSave = [];
      angular.forEach(vm.ratesMap, function(rate, key) {
        ratesToSave.push(rate);
      });

      RatesService.saveList(ratesToSave, onSaveSuccess, onSaveError);

      function onSaveSuccess(result) {
        console.log('success');
        $state.reload();
      }

      function onSaveError(error) {
        console.log('error  on save ');
        console.log(error);
      }
    }


    function compareWithoutType(value1, value2) {
      var same = value1 == value2;
      // console.log("comparing : " + value1 + " and " + value2+", "+same);
      return value1 == value2;
    }





    function toggleResolutionDetails(index) {
      vm.resolutionDisplayPersonDetails = !vm.resolutionDisplayPersonDetails;
      vm.resolutionDisplayPersonDetailsIndex = index;
    }

    function showResolutionDetails(index) {
      if (vm.resolutionDisplayPersonDetails) {
        return vm.resolutionDisplayPersonDetailsIndex == index;
      }
      return false;
    }
    //helper function that takes a decimal number then tracates to x places.
    function truncate(num, places) {
      return Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
    }


    function initContractResolutionConfigs() {
      console.log(vm.contract.year);
      // console.log(vm.entity.dimensionType);
      vm.exclusiveContractParameters = [];
      DimensionSetsService.getContractResolutionConfig({
        id: vm.contract.serviceId,
        year: vm.contract.year,
        type: vm.contract.objectType
      }).
      $promise.then(function(response) {
        console.log(response);
        vm.exclusiveContractParameters = response;
        console.log("exclusiveContractParameters");
        console.log(vm.exclusiveContractParameters);
        console.log(vm.exclusiveContractParameters.length);
        $timeout(function () {
          if(vm.exclusiveContractParameters.length > 0){
            console.log("results...");
            initParameters();
        }
      }, 2000);
      //   if(vm.exclusiveContractParameters.length > 0){
      //     console.log("results...");
      //     initParameters();
      // }
      });
    }


    function removeDimensionType(dimType) {
      console.log("called...");
      angular.forEach(vm.exclusiveContractParameters, function(dimType) {
        console.log("reach loop");

        angular.forEach(vm.dimensionTypes, function(dimenType) {
          if (dimType.dimensionType != dimenType.type) {

            vm.myDim.push(dimenType);

          }
        });

      });
    }

    function initContractVersionActions() {
      console.log(vm.contract);
      console.log(vm.contract.displayedVersion);
      if(vm.contract.contractVersionDTOList.length > 0){
      angular.forEach(vm.contract.contractVersionDTOList, function(version){
        vm.contractVersions.push(version);
        if(version.status == "PUBLISHED" && version.id == vm.contract.publishedVersionId){
          vm.currentPublished = version;
        }
      });

      }
      if(vm.contract.displayedVersion != null){
      if (vm.contract.displayedVersion.id == vm.contract.publishedVersionId) {
        console.log("PUBLISHED VERSION");
        vm.showPublishedVersion = true;
        vm.showNewVersionBtn = true;


      } else if (vm.contract.displayedVersion.id == vm.contract.draftVersionId) {
        vm.showDraftVersion = true;
        vm.showBanner = true;

        console.log("DRAFT VERSION");
        if (vm.contract.displayedVersion.status == 'VALID') {
          console.log("VALID status");
          vm.showPublishedBtn = true;
          vm.showDraftBtn = true;
          vm.showEditBtn = true;


        } else if (vm.contract.displayedVersion.status == 'DRAFT') {
          console.log("DRAFT status");

          vm.showEditBtn = true;
          vm.showValidateBtn = true;
          vm.showDeleteContractVersionBtn = true;
          vm.showDeleteVersionBtn = true;
        }
      } else {
        console.log("HISTORICAL VERSION");
        vm.showNewVersionBtn = true;
        vm.showBanner = true;

      }
}








    }

  }



})();
