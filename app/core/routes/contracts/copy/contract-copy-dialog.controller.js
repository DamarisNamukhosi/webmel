(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractCopyDialogController', ContractCopyDialogController);

  ContractCopyDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$uibModalInstance',
    '$localStorage',
    '$state',
    'entity',
    'ContractsService',
    'OrganizationsService',
    'ServicesService',
    'DimensionSetsService',
    'currencies',
    'organizationTypes',
    'MarketsService',
    'LocationsService'
  ];

  function ContractCopyDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    $state,
    entity,
    ContractsService,
    OrganizationsService,
    ServicesService,
    DimensionSetsService,
    currencies,
    organizationTypes,
    MarketsService,
    LocationsService
  ) {
    var vm = this;

    //injected values 
    vm.entity = entity;
    vm.contract = [];
    vm.contract.contractId = entity.id;
    vm.contract.newContractName = entity.name;
    vm.contract.contractType = "PUBLISHED";
    vm.contract.targetId = entity.targetId;
    vm.contract.issuerId = entity.issuerId;
    vm.contract.issuerName = entity.issuerName;
    vm.contract.issuerUuid = entity.issuerUuid;
    vm.contract.discount = 0;
    vm.contract.copyParams = true;
    vm.contract.copySupplements = true;
    vm.contract.supplementDiscount = 0;
    vm.contract.marketId = entity.marketId;
    vm.contract.round = true;
    vm.contract.supplementType = "MarkUp" ; 

    console.log(vm.entity);

    vm.currencies = currencies;
    vm.organizationTypes = organizationTypes;
    vm.inPropertyOrgType = $localStorage.current_organisation.organisationTypeName == 'PROPERTY';
    //assign functions to vm 
    vm.clear = clear;
    vm.save = save;
    
    vm.getLocation = getLocation;
    vm.refresh = refresh;
    vm.getLocs = getLocs;
    vm.onTargetSelected = onTargetSelected;
    vm.searchTargetOrg = searchTargetOrg;
    vm.supplementTypeChange =supplementTypeChange;
    vm.dmState = $state.current.name.includes("cost-contracts.new");

    //initiliaze data
    vm.dimensionSets = [];
    vm.services = [];
    vm.markets = [];
    vm.contractYears = [];
    vm.disableSelectBox = false;
    vm.disableServiceBox = false;
    vm.disableDimensionSetsBox = false;
    vm.organizationType = 0;
    vm.suppliers = [];
    vm.contractParameters = [];
    vm.contractExclusiveParameters = [];
    vm.contractParams = [];
    vm.setDimensionsWithExistences = [];
    vm.organizationServices = [];
    vm.organizations = [];
    vm.serviceName = "";
    vm.targetName = "";
    vm.basicDimensionCount = 0;
    vm.basicDimensionSets = [];
    vm.contractParamsCount = 0;
    vm.disablePreview = false;
    //vm.disablePreviewFunction = disablePreviewFunction;
    vm.showTargetOrg = false;
    vm.nonContractOrganizationTypes = [1, 8, 9];


    //init functions  
    initOrganizationServices();


   

    function refresh($item, $model) {
      console.log("refreshing...");
      vm.entity.targetId = $item.id;
      vm.entity.targetName = $item.name;
    }

    function getLocation(name, type) {
      console.log("getLocation...");
      console.log(name);
      console.log(type);
      return OrganizationsService.getOrganizationsByType({
          type: type,
          name: name
        }).$promise
        .then(function (results) {
          return results.map(function (item) {
            console.log(item);
            return item;
          })
        });
    }


    function getLocs(val, type) {
      return OrganizationsService
        .getOrganizationsByType({
          type: type,
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
      organizationChange();
    }

    function onTargetSelected($item, $model) {
      vm.entity.targetId = $item.id;
      vm.entity.targetName = $item.name;
      if (vm.entity.supplierId > 0) {
        OrganizationsService.getParent({
          id: vm.entity.supplierId
        }).$promise.then(function (respose) {
          vm.issuers = respose;
        });
      }
    }

    function searchTargetOrg(name) {
      console.log("serarching target...");
      console.log(name);
      getLocation(name, 4)
    }

    function supplementTypeChange(){

    }
    console.log(vm.organizationTypes);
    console.log('vm.organizationTypes');

    // remove invalid orgs
    var validOrgTypes = [];
    angular.forEach(vm.organizationTypes, function (organizationType) {
      if (vm.nonContractOrganizationTypes.includes(organizationType.id)) {
        console.log('organizationType');
        console.log(organizationType.name, organizationType.id);
      } else {
        validOrgTypes.push(organizationType);
      }
    });

    console.log(validOrgTypes);
    console.log('validOrgTypes');

    vm.organizationTypes = validOrgTypes;





    //set preset dates for new contracts  
    if (vm.entity.id == null || vm.entity.id == undefined) {
      vm.entity.startDate = new Date();
      vm.entity.startDate.setFullYear(vm.entity.startDate.getFullYear() + 1);
      vm.entity.endDate = new Date();
      vm.entity.endDate.setFullYear(vm.entity.endDate.getFullYear() + 2);
      vm.entity.year = new Date().getFullYear() + 1;
    } else {
      //what do we do?
    }

    function organizationChange(editFlag) {
      //get services by org
      if (editFlag) {
        OrganizationsService.getOrganizations().$promise.then(function (response) {
          vm.organizations = response;
        });
        console.log(vm.organizations);
        var orgId = $stateParams.id;
        ServicesService.get({
          id: orgId
        }).$promise.then(function (response) {
          vm.services = response;
          console.log("vm.services");
          console.log(vm.services);
          vm.serviceName = response.name;
          OrganizationsService.getParent({
            id: vm.services.organisationId
          }).$promise.then(function (response) {
            vm.issuers = response;
          });
        });
        serviceChange(editFlag);
      } else {
        console.log('editlog is not true');
        var orgId = vm.entity.supplierId;

        ServicesService.getByOrganizationId({
          id: orgId
        }).$promise.then(function (response) {
          vm.services = response;
        });
      }
    }

    
    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {

      vm.entity.contractParamDTOList = [];
      console.log('saving..');
      console.log(vm.contract);
      vm.isSaving = true;
     
      console.log(vm.contract.newContractName );
      //console.log(vm.contract.newContractName.slice(0,name));
        if(vm.contract.supplementType == "MarkUp"){
          vm.contract.discount= "-"+vm.contract.discount

        }else if(vm.contract.supplementType == "Discount"){
          vm.contract.discount= vm.contract.discount
            
        }
      if (vm.contract.contractType == "PUBLISHED") {
        vm.contract.newContractName = vm.contract.newContractName.slice(0,vm.contract.newContractName.search("CONTRACT")) + "RACK rates";
        var newContract = {
          "contractId": vm.contract.contractId,
          "contractType": vm.contract.contractType,
          "copyParams": vm.contract.copyParams,
          "copySupplements": vm.contract.copySupplements,
          "issuerId": vm.contract.issuerId,
          "issuerName": vm.contract.issuerName,
          "issuerUuid": vm.contract.issuerUuid,
          "marketId": vm.contract.marketId,
          "newContractName": vm.contract.newContractName,
          "round": vm.contract.round,
          "discount": vm.contract.discount,
          "supplementDiscount": vm.contract.supplementDiscount,
          "targetId": null
        };
      } else {
        var newContract = {
          "contractId": vm.contract.contractId,
          "contractType": vm.contract.contractType,
          "copyParams": vm.contract.copyParams,
          "copySupplements": vm.contract.copySupplements,
          "issuerId": vm.contract.issuerId,
          "issuerName": vm.contract.issuerName,
          "issuerUuid": vm.contract.issuerUuid,
          "marketId": vm.contract.marketId,
          "newContractName": vm.contract.newContractName,
          "round": vm.contract.round,
          "discount": vm.contract.discount,
          "supplementDiscount": vm.contract.supplementDiscount,
          "targetId": vm.contract.targetId
        };
      }
      console.log(newContract);
      ContractsService.copy(
        newContract,
        onSaveSuccess,
        onSaveError
      );
    }

    function onSaveSuccess(result) {
      $stateParams.supplierId= result.supplierId,
      $stateParams.contractId= result.id,
      $stateParams.setId=result.dimensionSetId,
      $stateParams.seasonGroupId=result.seasonGroupId,
      $stateParams.year= result.year
      $state.go('contracts-own-detail', {
        supplierId: result.supplierId,
        contractId: result.id,
        setId: result.dimensionSetId,
        seasonGroupId: result.seasonGroupId,
        year: result.year
      },{reload: true,inherit: false}); 
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  
    function resolveMarket(id) {
      var value = '';
      angular.forEach(vm.markets, function (market) {
        if (market.id == id) {
          value = market.name;
        }
      });
      return value;
    }

   


    function initOrganizationServices() {
        if(vm.inPropertyOrgType){
            vm.contractTypes = ["PUBLISHED", "CONTRACT"];

        }else{
            vm.contractTypes = ["PUBLISHED"];

        }
      OrganizationsService.getParent({
        id: vm.entity.supplierId
      }).$promise.then(function (respose) {
        vm.issuers = respose;
      });
    }
  }
})();
