(function () {
    'use strict';
  
    angular
      .module('webApp')
      .controller('ContractDetailOldController', ContractDetailOldController);
  
    ContractDetailOldController.$inject = [
      '$scope',
      '$rootScope',
      '$stateParams',
      '$localStorage',
      '$state',
      '$timeout',
      'previousState',
      'entity',
      'ContractsService',
      'URLS',
      'dimensionSet',
      'RatesService',
      'dimensions'
    ];
  
    function ContractDetailOldController(
      $scope,
      $rootScope,
      $stateParams,
      $localStorage,
      $state,
      $timeout,
      previousState,
      entity,
      ContractsService,
      URLS,
      dimensionSet,
      RatesService,
      dimensions
    ) {
      var vm = this;
      //injected values
      vm.contract = entity;
      vm.dimensionSet = dimensionSet;
      vm.dimensions = dimensions;
      console.log("vm.dimensionSet");
      console.log(vm.dimensionSet);
      console.log('rates');
      console.log(vm.contract.rates.length);
      console.log($state.current.name);
      //$state.go($state.current.name, {}, {reload: true});
  
      //declare functions
      vm.initSupplementDimensions = initSupplementDimensions;
      vm.filterSupplements = filterSupplements;
      vm.resolveExistenceName = resolveExistenceName;
      vm.resolveDimensionNameByType = resolveDimensionNameByType;
      vm.newSupplements = vm.contract.supplements.length == 0;
      vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.contract.uuid;
      vm.cancel = cancel;
      //initialization
      vm.rates = [];
      $scope.isEdit = false;
      vm.supplementDimensions = [];
      vm.dimensionExclusiveParameters = [];
      vm.basicParameters = [];
      vm.notesParameters = [];
      vm.newRates = [];
      vm.ratesAvailable = false;
      vm.oldRates = [];
      vm.value = [];
      vm.myStack = [];
      vm.dimensionNotExclusiveParameters = [];
      vm.dimensionNotExclusiveParameterGroups = [];
      vm.firstRow = vm.dimensionSet[0].existenceDTOList;
      vm.dimLength = '';
  
      vm.thirdItem = {
        type: 'BASIC',
        existenceDTOList: [{
          id: 11,
          uuid: '00d50256-5675-4a38-a758-cddf3b79fb0d',
          name: 'Default',
          brief: null
        }]
      };
  
      var i = 0;
  
      var j = 0;
      vm.y = [1, 2, 3];
      vm.exiList = {};
      vm.exitList = [];
      vm.basicDim = [];
      vm.basicDimId = [];
      vm.rateMatrix=null;
      vm.testArray= [];
      //call functions needed to initialize data 
  
      initSupplementDimensions();
      initParameters();
      initRates();
      $scope.table = buildTable();
  
  
  
      $scope.saving = function (figures, edit) {
        console.log("inside saving...");
        console.log(edit);
        vm.fig = figures;
        vm.edit = edit;
  
        if (vm.edit !== true) {
          console.log("not saving...");
          $scope.isEdit = !edit;
        } else {
          console.log("saving...");
          $scope.isEdit = !edit;
          if (vm.dimLength === 3) {
            console.log('three array');
            console.log('figures');
            console.log(vm.fig);
  
            angular.forEach(vm.fig, function (value, index) {
              console.log('index');
              console.log(index); // 0, 1, 2, 3
              console.log('value');
              console.log(value); // A, B, C, D
  
              angular.forEach(value, function (obj, i) {
                console.log('i');
                console.log(index, ',', i); // 0, 1, 2, 3
                console.log('obj');
                console.log(obj); // A, B, C, D
  
                angular.forEach(obj, function (obj1, j) {
                  console.log('j');
                  console.log(index, ',', i, ',', j); // 0, 1, 2, 3
                  console.log('obj1');
                  console.log(obj1); // A, B, C, D
                  if (vm.ratesAvailable === false) {
  
                    console.log('rates not available...');
                    console.log(vm.ratesAvailable);
                    if(obj1 === null || obj1 === undefined){
                      obj1 = 0;
                    }
                    vm.rates.push({
                      'contractId': vm.contract.id,
                      'dim1': vm.exiList[0][j],
                      'dim2': vm.exiList[1][i],
                      'dim3': vm.exiList[2][index],
                      'value': obj1
                    });
  
                  } else {
                    console.log('rates available...');
                    console.log(vm.ratesAvailable);
                    angular.forEach(vm.oldRates, function (oldRate) {
                      if (oldRate.dim1 === vm.exiList[0][j] && oldRate.dim2 === vm.exiList[1][i] && oldRate.dim3 === vm.exiList[2][index]) {
                        if (oldRate.value !== obj1) {
                          oldRate.value = obj1;
                        }
                      }
                    });
  
                  }
  
                });
  
              });
            });
            //
  
          }
          else if (vm.dimLength === 2) {
            console.log('two array');
            vm.fig = figures;
            vm.array2 = figures;
            console.log(vm.array2);
            console.log(vm.array2.length);
            if(vm.array2.length > 1){
            angular.forEach(vm.array2, function (value, index) {
              console.log('index');
              console.log(index); // 0, 1, 2, 3
              console.log('value');
              console.log(value); // A, B, C, D
              
              angular.forEach(value, function (obj, i) {
                console.log('i');
                console.log(index,  ',', i); // 0, 1, 2, 3
                console.log('obj');
                console.log(obj); // A, B, C, D
                if(obj === null || obj === undefined){
                  obj = 0;
                }
                vm.rates.push({ 'contractId': vm.contract.id, 'dim1': vm.exiList[0][i], 'dim2': vm.exiList[1][index], 'value': obj });
              });
            });
          }else {
            console.log("loop through an array of lenght one");
            console.log(vm.array2.length === 1 );
            angular.forEach(vm.array2, function(value, index){
              console.log('index');
              console.log(index); // 0, 1, 2, 3
              console.log('value');
              console.log(value); // A, B, C, D
              angular.forEach(value, function (obj, i) {
                console.log('i');
                console.log(index,  ',', i); // 0, 1, 2, 3
                console.log('obj');
                console.log(obj); // A, B, C, D
                angular.forEach(obj, function (ob, j) {
                  console.log('i');
                  console.log(index,  ',', j); // 0, 1, 2, 3
                  console.log('obj');
                  console.log(ob); // A, B, C, D
                  //vm.rates.push({ 'contractId': vm.contract.id, 'dim1': vm.exiList[0][j], 'dim2': vm.exiList[1][i], 'value': ob });
                  if (vm.ratesAvailable === false) {
  
                    console.log('rates not available...');
                    console.log(vm.ratesAvailable);
                    console.log(ob);
                    if(ob === null || ob === undefined){
                      ob = 0;
                    }
                    vm.rates.push({
                      'contractId': vm.contract.id,
                      'dim1': vm.exiList[0][j],
                      'dim2': vm.exiList[1][i],
                      'value': ob
                    });
  
                  } else {
                    console.log('rates available...');
                    console.log(vm.ratesAvailable);
                    angular.forEach(vm.oldRates, function (oldRate) {
                      if (oldRate.dim1 === vm.exiList[0][j] && oldRate.dim2 === vm.exiList[1][i]) {
                        if (oldRate.value !== ob) {
                          oldRate.value = ob;
                        }
                      }
                    });
  
                  }
                });
              });
            });
          }
            
          }
          if (vm.ratesAvailable === false) {
            console.log('no rates... create');
  
            console.log('rates');
            console.log(vm.rates);
            RatesService.createList(vm.rates, onSaveSuccess, onSaveError);
  
            function onSaveSuccess(result) {
              //$scope.$emit('gatewayApp:locationUpdate', result);
              console.log('success');
              $timeout(function () {
                $state.go($state.current.name, {}, {reload: true});
            }, 1000);
              
            }
  
            function onSaveError() {
              console.log('error  ');
            }
          } else {
            console.log("updating...");
            console.log(vm.oldRates);
            RatesService.updateList(vm.oldRates , onSaveSuccess, onSaveError);
  
            function onSaveSuccess(result) {
              //$scope.$emit('gatewayApp:locationUpdate', result);
              console.log('success');
              console.log(result);
              $timeout(function () {
                $state.go($state.current.name, {}, {reload: true});
            }, 1000);
              
            }
  
            function onSaveError() {
              console.log('error  ');
            }
          }
        }
      };
  
      $scope.removeCol = function ($index) {
        if ($index > -1 && $scope.table.columns.length > 1) {
          $scope.table.columns.splice($index, 1);
          for (var i = 0, len = $scope.table.rows.length; i < len; i++) {
            $scope.table.rows[i].cells.splice($index, 1);
          }
        }
      };
  
      $scope.removeRow = function ($index) {
        if ($index > -1 && $scope.table.rows.length > 1) {
          $scope.table.rows.splice($index, 1);
        }
      };
  
      $scope.addCol = function () {
        var len = $scope.table.columns.length,
          rowLen = $scope.table.rows.length;
        $scope.table.columns.push({
          value: 'Col ' + len
        });
        for (var i = 0; i < rowLen; i++) {
          $scope.table.rows[i].cells.push({
            value: 'Call ' + i + ',' + len
          });
        }
      };
  
      $scope.addRow = function () {
        var row = {
            cells: []
          },
          rowLen = $scope.table.rows.length,
          colLen = $scope.table.columns.length;
  
        for (var i = 0; i < colLen; i++) {
          row.cells.push({
            value: 'Cell ' + rowLen + ',' + i
          });
        }
        $scope.table.rows.push(row);
      };
  
      function buildTable() {
        return {
          columns: [{
            value: 'Col 0'
          }, {
            value: 'Col 1'
          }, {
            value: 'Col 2'
          }],
  
          rows: [{
            cells: [{
              value: 'Cell 0,0'
            }, {
              value: 'Cell 0,1'
            }, {
              value: 'Cell 0,2'
            }]
          }, {
            cells: [{
              value: 'Cell 1,0'
            }, {
              value: 'Cell 1,1'
            }, {
              value: 'Cell 1,2'
            }]
          }, {
            cells: [{
              value: 'Cell 2,0'
            }, {
              value: 'Cell 2,1'
            }, {
              value: 'Cell 2,2'
            }]
          }]
        };
      }
  
      function cancel() {
        console.log('clicked');
        $scope.table = buildTable();
        $scope.isEdit = false;
      }
  
      ContractsService.getOrganizationPolicy({
        organisationId: vm.contract.issuerId
      }).$promise.then(function (result) {
        vm.policy = result;
      });
  
      function initSupplementDimensions() {
        angular.forEach(vm.dimensionSet, function (dimension) {
          if (dimension.type == 'SUPPLEMENT') {
            vm.supplementDimensions.push(dimension);
          }
        })
      }
  
      function initParameters() {
   
        if (vm.contract.params.length > 0) {
          console.log("contract params present");
          console.log(vm.contract.params);
          angular.forEach(vm.contract.params, function (parameter) {
            if (parameter.type == "DIMENSION") {
              if (parameter.exclusive == true) {
                vm.dimensionExclusiveParameters.push(parameter);
              } else {
                vm.basicParameters.push(parameter);
              }
              //vm.dimensionExclusiveParameters.push(parameter);
            } else if (parameter.type == "NOTE") {
              vm.notesParameters.push(parameter);
            } else {
              vm.basicParameters.push(parameter);
            }
          });
          console.log(vm.dimensionExclusiveParameters);
        } else {
          console.log("contract params not present");
          var temp = [];
          var count = 0;
          var groupSize = 2; // number of parameters in the basic information table
  
          //categorize the basi info parameters into groups
          angular.forEach(vm.dimensionNotExclusiveParameters, function (parameter) {
  
            var valueTmp = {};
            valueTmp.type = "value";
            valueTmp.value = parameter.value;
            valueTmp.dimensionType = parameter.parameter;
  
            var parameterTmp = {};
            parameterTmp.type = "parameter";
            parameterTmp.value = parameter.parameter;
  
            if ((count % groupSize) == 0 && count > 0) {
              vm.dimensionNotExclusiveParameterGroups.push(temp);
              temp = [];
            }
  
            temp.push(parameterTmp);
            temp.push(valueTmp);
  
            count = count + 1; //increment count 
          });
  
          if (temp.length > 0) {
            vm.dimensionNotExclusiveParameterGroups.push(temp);
          }
  
        }
      }
  
      function initRates() {
        //check if there are rates
        //vm.rateMatrix = math.matrix() // Matrix, size [0],       []
        console.log(vm.rateMatrix);
        console.log("vm.contract.rates");
        console.log(vm.contract.rates);
        console.log(vm.contract.rates.length);
        if (vm.contract.rates.length === 0) {
          vm.ratesAvailable = false;
  
          angular.forEach(vm.dimensionSet, function (set) {
            i++;
            if (i == 3) {
              vm.thirdItem = set;
            }
          });
  
          angular.forEach(vm.dimensionSet, function (dimension) {
            if (vm.dimensionSet.length == 3) {
              vm.thirdItem = dimension;
            }
          });
          angular.forEach(vm.dimensionSet, function (dimension) {
            if (dimension.type == 'BASIC') {
              vm.basicDim.push(dimension);
  
              vm.basicDimId.push(dimension.id);
  
              vm.exist = dimension.existenceDTOList;
  
              vm.exiList[j] = [];
              angular.forEach(vm.exist, function (ex) {
                vm.exiList[j].push(ex.id);
                vm.exitList.push(ex.id);
              });
              j++;
            }
          });
  
          vm.dimLength = vm.basicDim.length;
          if (vm.dimLength == 3) {
            console.log('dimLength three')
            vm.inner = _.chunk(
              vm.value,
              vm.dimensionSet[0].existenceDTOList.length *
              vm.dimensionSet[1].existenceDTOList.length
            );
            vm.figures = [];
  
            angular.forEach(vm.inner, function (obj) {
              vm.figures.push(
                _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
              );
            });
          } else if (vm.dimLength == 2) {
            vm.inner = vm.value;
            vm.figures = [];
            a.resize([])
            vm.figures.push(
              _.chunk(
                vm.inner,
                vm.dimensionSet[0].existenceDTOList.length
              )
            );
  
          } else if (vm.dimLength == 1) {
            vm.inner = vm.value;
            vm.figures = [];
  
            vm.figures.push(vm.inner);
            
          }
          
        } else {
          //if rates are present...
          console.log("rates present");
          vm.ratesAvailable = true;
          console.log(vm.ratesAvailable === true);
          vm.oldRates = entity.rates;
          angular.forEach(vm.oldRates, function (rate) {
            vm.value.push(rate.value);
          });
  
          angular.forEach(vm.dimensionSet, function (set) {
            i++;
            if (i == 3) {
              vm.thirdItem = set;
            }
          });
  
          angular.forEach(vm.dimensionSet, function (dimension) {
            if (vm.dimensionSet.length == 3) {
              vm.thirdItem = dimension;
            }
          });
          angular.forEach(vm.dimensionSet, function (dimension) {
            if (dimension.type == 'BASIC') {
              vm.basicDim.push(dimension);
  
              vm.basicDimId.push(dimension.id);
  
              vm.exist = dimension.existenceDTOList;
  
              vm.exiList[j] = [];
              angular.forEach(vm.exist, function (ex) {
                vm.exiList[j].push(ex.id);
                vm.exitList.push(ex.id);
              });
              j++;
            }
          });
  
          vm.dimLength = vm.basicDim.length;
          if (vm.dimLength === 3) {
            console.log('dimLength three')
            vm.inner = _.chunk(
              vm.value,
              vm.dimensionSet[0].existenceDTOList.length *
              vm.dimensionSet[1].existenceDTOList.length
            );
            vm.figures = [];
  
            angular.forEach(vm.inner, function (obj) {
              vm.figures.push(
                _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
              );
            });
            console.log("vm.figures");
            console.log(vm.figures);
          } else if (vm.dimLength === 2) {
            vm.inner = vm.value;
            vm.figures = [];
            console.log("dimLength 2");
            vm.figures.push(
              _.chunk(
                vm.inner,
                vm.dimensionSet[0].existenceDTOList.length
              )
            );
            console.log("fibures 2");
            console.log(vm.figures);
          } else if (vm.dimLength == 1) {
            vm.inner = vm.value;
            vm.figures = [];
  
            vm.figures.push(vm.inner);
          }
        }
      } //end initialization of rates
  
      function filterSupplements(dimensionType) {
        var list = [];
  
        // if (dimensionType == -1) {
        //   dimensionType = null;
        // }
  
        angular.forEach(vm.contract.supplements, function (supplement) {
          if (supplement.dimensionType1 == dimensionType) {
            list.push(supplement);
          }
        });
        return list;
  
      }
  
  
      function resolveDimensionNameByType(type) {
        var name = type;
        angular.forEach(vm.dimensions, function (dimension) {
          if (dimension.type == type) {
            name = dimension.name;
          }
        });
  
        return name;
      }
  
  
      function resolveExistenceName(dimensionType, existenceId) {
        var name = existenceId;
        console.log("vm.dimensionSet");
        console.log(vm.dimensionSet);
        angular.forEach(vm.dimensionSet, function (dimension) {
          if (dimension.dimensionType == dimensionType) {
            angular.forEach(dimension.existenceDTOList, function (existence) {
  
              if (existence.id == existenceId) {
                name = existence.name;
              }
  
            });
          }
        });
        return name;
      }
    }
  
  
  
  })();
  