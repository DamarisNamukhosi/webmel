(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractUpdateStatusDialogController',ContractUpdateStatusDialogController);

        ContractUpdateStatusDialogController.$inject = ['$uibModalInstance', '$stateParams', '$localStorage', 'entity', 'ContractsService'];

    function ContractUpdateStatusDialogController($uibModalInstance, $stateParams, $localStorage , entity, ContractsService) {
        var vm = this;

        vm.contract = entity;
        console.log(vm.contract);
        console.log($stateParams);
        vm.updateContract  = {
            "contractId": vm.contract.id,
            "status": $stateParams.status,
            "statusReason": " ",
            "versionId": $stateParams.versionId
          }
        if($stateParams.status ===  "UNPUBLISHED" || $stateParams.status ===  "DRAFT"){
            vm.contract.contentStatus = "DRAFT";
        }else if($stateParams.status === "PUBLISHED"){
            vm.contract.contentStatus = "PUBLISHED";
        }
        else if($stateParams.status === "VALID"){
            vm.contract.contentStatus = "VALID";
        }
        vm.clear = clear;
        vm.confirmUpdate = confirmUpdate;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmUpdate(contract) {

            contract.lastUpdatedBy= $localStorage.account.login;
            var today= new Date();
            contract.lastUpdatedOn = moment(formatDate(today), 'YYYY-MM-DDTHH:mm:ss.SSS').format('YYYY-MM-DD HH:mm:ss');

            console.log("contract");
            console.log(vm.updateContract);
          ContractsService.updateVersion(vm.updateContract, function() {
            $uibModalInstance.close(true);
          });
        }

        function formatDate(startDate) {
            if (startDate !== null && startDate !== 'Invalid date') {
                console.log("startDate");
                console.log(startDate);
                var theDate = new Date(startDate.toString());
                return (
                    theDate.getFullYear() +
                    '-' +
                    (theDate.getMonth() + 1) +
                    '-' +
                    theDate.getDate() +
                    ' ' +
                    theDate.getHours() +
                    ':' +
                    theDate.getMinutes() +
                    ':' +
                    theDate.getSeconds() +
                    '.' +
                    theDate.getMilliseconds()
                );
            } else {
                return '';
            }
        }
    }
})();
