(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractParameterDialogController', ContractParameterDialogController);

  ContractParameterDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ContractParametersService', 'dimensionSet', 'dimensions'];

  function ContractParameterDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ContractParametersService, dimensionSet, dimensions) {
    var vm = this;

    vm.entity = entity;
    vm.dimensionSet = dimensionSet;
    vm.dimensions = dimensions;
    console.log(vm.dimensions);
    vm.basicDimensions=[];

    //init variables
    vm.existencies = [];
    vm.noteCategory = ["DISCOUNT", "PAYMENT", "OCCUPANCY" , "CANCELLATION",  "ARRIVAL_AND_CHECKOUT", "NOTE" ,"MINIMUM_STAY" ,"DIMENSION", "BASIC","SPECIAL_OFFER", "CLOSED","SINGLE_ROOM_RATE_POLICY"];
    //declare functions
    vm.clear = clear;
    vm.save = save;
    vm.updateDimensionExistencesByType = updateDimensionExistencesByType;

    //init function calls here 
    initDimensionsCleanUp();
    initExistences();

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;

      //if type == DIMENSION, copy the existence name to notes 

      if (vm.entity.type == 'DIMENSION') {
        angular.forEach(vm.existencies, function (object) {
          if (object.id == vm.entity.value) {
            vm.entity.notes = object.name;
          }
        });
      }

      if (vm.entity.id !== null) {
        console.log("updating..");
        console.log(vm.entity);
        ContractParametersService.update(vm.entity, onSaveSuccess, onSaveError);
      } else {
        console.log("saving..");
        console.log(vm.entity);
       ContractParametersService.create(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function initDimensionsCleanUp() {
      //we need to remove any dimensions in the set from the vm.dimensions list
      angular.forEach(vm.dimensionSet, function (dimension) {
        
           if(dimension.hasDefault === true){
            var dim = {};
            dim.id = dimension.dimensionId;
            dim.type = dimension.dimensionType;
            dim.name = dimension.dimensionName;
            vm.basicDimensions.push(dim);
           }
      });
    }

    function updateDimensionExistencesByType(dimensionType) {
      var list = [];
      if (vm.dimensionSet !== undefined) {
        angular.forEach(vm.dimensionSet, function (dimension) {
          if (dimensionType == dimension.dimensionType)
            vm.existencies = dimension.existenceDTOList;
        })
      }
    }

    function initExistences() {
      if (vm.entity.type == 'DIMENSION') {
        if (vm.entity.parameter !== undefined && vm.entity.parameter !== null) {
          updateDimensionExistencesByType(vm.entity.parameter);
        }
      }
    }

  }
})();
