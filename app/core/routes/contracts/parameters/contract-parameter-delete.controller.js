(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractParameterDeleteController', ContractParameterDeleteController);

  ContractParameterDeleteController.$inject = ['$uibModalInstance', 'entity', 'ContractParametersService', 'SupplementsService'];

  function ContractParameterDeleteController($uibModalInstance, entity, ContractParametersService, SupplementsService) {
    var vm = this;

    vm.entity = entity;
    // console.log('vm.entity');
    // console.log(vm.entity);
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;
    vm.entity.type == 'DIMENSION' || vm.entity.type == 'BASIC' || vm.entity.type == 'NOTE' ? vm.entity.displayName = vm.entity.parameter : vm.entity.displayName = vm.entity.name;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(contract) {
      if (vm.entity.type == 'DIMENSION' || vm.entity.type == 'BASIC' || vm.entity.type == 'NOTE' || vm.entity.type == 'OCCUPANCY' || vm.entity.type == 'PAYMENT' || vm.entity.type == 'DISCOUNT'||  vm.entity.type == 'CANCELLATION' || vm.entity.type == 'ARRIVAL_AND_CHECKOUT') {
        vm.id = contract;
        // console.log("deleting paramenter " + vm.id);
        ContractParametersService.delete({
          id: vm.id
        }, onSaveSuccess, onSaveError);
      } else {
        vm.id = contract;
        console.log("deleting supplement " + vm.id);
        SupplementsService.delete({
          id: vm.id
        }, onSaveSuccess, onSaveError);
      }

      function onSaveSuccess(result) {
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }

      function onSaveError() {
        vm.isSaving = false;
      };
    }
  }
})();
