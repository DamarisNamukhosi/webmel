(function() {
    'use strict';
    angular.module('webApp').factory('ContractsService', ContractsService);

    ContractsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ContractsService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(
            resourceUrl,
            {},
            {
                
                getFiltered: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/get-filtered/?target=:target&generalService=:generalService&supplier=:supplier&issuer=:issuer&year=:year&name=:name&showDeleted=:showDeleted&showRack=:showRack'
                },
                //http://51.15.233.87:8080/costingservice/api/contracts/get-filtered?target=1&generalService=1&supplier=16&issuer=34&year=2019&name=sarova

                getSupplier: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/get-suppliers/:orgId?generalService=:genSvcId'
                },
                getOrganizationPolicy: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/get-full-by-organisation/:organisationId'
                },
                getFull: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/get-full/:id?versionId=:versionId'
                },
                getContractsByTarget: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/filter-by-target/:id'
                },
                getContractsByServiceId: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/filter-by-service/:id'
                },
                getByChain: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/get-grouped/:id'
                },
                getAll: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url: URLS.BASE_URL + 'costingservice/api/contracts/:id'
                },
                get: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url: URLS.BASE_URL + 'costingservice/api/contracts/:id'
                },
                update: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/contracts'
                },
                create: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/contracts'
                },
                createFull: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/create-full'
                },
                updateVersion: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/update-version'
                },
                updateFull: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/contracts/update-full'
                },
                delete: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/contracts/:id'
                },
                copy: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/contracts/copy'
                }
                ,
                //
                'getContractResolutionConfig': {
                  method: 'GET',
                  headers : {
                      'Authorization': 'Bearer ' + $localStorage.user
                  },
                  isArray: true,
                  //http://51.15.233.87:8080/costingservice/api/contract-resolution-configs
                  url: URLS.BASE_URL + 'costingservice/api/set-dimensions/get-exclusives/:id?year=:year&objectType=:type'
                }
            }
        ); 
    }
})();
