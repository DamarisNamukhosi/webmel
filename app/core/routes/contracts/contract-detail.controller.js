(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractDetailController', ContractDetailController);

  ContractDetailController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$localStorage',
    '$state',
    '$timeout',
    'previousState',
    'entity',
    'ContractsService',
    'URLS',
    'dimensionSet',
    'RatesService',
    'dimensions',
    'SupplementsService',
    'RoomOccupanciesService'
  ];

  function ContractDetailController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $state,
    $timeout,
    previousState,
    entity,
    ContractsService,
    URLS,
    dimensionSet,
    RatesService,
    dimensions,
    SupplementsService,
    RoomOccupanciesService
  ) {
    var vm = this;
    //injected values
    vm.contract = entity;
    console.log("vm.contract");
    console.log(vm.contract);
    vm.dimensionSet = dimensionSet;
    vm.dimensions = dimensions;
    //$state.go($state.current.name, {}, {reload: true});

    //declare functions
   // vm.initSupplementDimensions = initSupplementDimensions;
    vm.filterSupplements = filterSupplements;
    vm.resolveExistenceName = resolveExistenceName;
    vm.resolveDimensionNameByType = resolveDimensionNameByType;
    vm.newSupplements = vm.contract.supplements.length == 0;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.contract.uuid;
    vm.cancel = cancel;
    vm.shortenName=shortenName;
    vm.initDimensions = initDimensions;
    //initialization
    vm.currentState= $state.current;

    vm.rates = [];
    $scope.isEdit = false;
    vm.supplementDimensions = [];
    vm.defaultSupplementDimensions = [];
    vm.dimensionExclusiveParameters = [];
    vm.basicParameters = [];
    vm.notesParameters = [];
    vm.discountParameters=[];
    vm.timeParameters=[];
    vm.cancellationParameters=[];
    vm.occupancyParameters=[];
    vm.paymentParameters =[];
    vm.specialOfferParameters=[];
    vm.minimumStayParameters=[];
    vm.closedParameters=[];
    vm.singleRoomRateParameters=[];
    vm.newRates = [];
    vm.ratesAvailable = false;
    vm.oldRates = [];
    vm.value = [];
    vm.myStack = [];
    vm.dimensionNotExclusiveParameters = [];
    vm.dimensionNotExclusiveParameterGroups = [];
    vm.firstRow = vm.dimensionSet[0].existenceDTOList;
    vm.dimLength = '';
    vm.getRatePosition = getRatePosition;
    vm.getRatePositionWithCreate = getRatePositionWithCreate;
    vm.updateSpecialSeasonDates =updateSpecialSeasonDates;
    vm.checkIfCredit= checkIfCredit;
    vm.resolveOccupancyName=resolveOccupancyName;
    vm.thirdItem = {
      type: 'BASIC',
      existenceDTOList: [{
        id: 11,
        uuid: '00d50256-5675-4a38-a758-cddf3b79fb0d',
        name: 'Default',
        brief: null
      }]
    };

    var i = 0;

    var j = 0;
    vm.y = [1, 2, 3];
    vm.exiList = {};
    vm.exitList = [];
    vm.basicDim = [];
    vm.basicDimId = [];

    vm.testArray= [];
    vm.threeDSupplements=[];
    vm.twoDExtras=[];
    vm.twoDExistencies=[];
    vm.twoDSupExistencies=[];
    vm.threeDExistencies=[];
    vm.threeDChunks=[];
    //call functions needed to initialize data 

    initSupplementDimensions();
    initParameters();
    initRates();
    initOccupancies();
    //$scope.table = buildTable();



    $scope.saving = function (figures, edit) {
      
      vm.fig = figures;
      vm.edit = edit;

      if (vm.edit !== true) {
        console.log("not saving...");
        $scope.isEdit = !edit;
      } else {
        console.log("saving...");
        $scope.isEdit = !edit;

        if (vm.ratesAvailable === false) {
          console.log('no rates... create');
          
          RatesService.createList(vm.contract.rates, onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            console.log('success');
            $timeout(function () {
              $state.go($state.current.name, {}, {reload: true});
          }, 1000);
            
          }

          function onSaveError() {
            console.log('error  ');
          }
        } else {
          RatesService.updateList(vm.oldRates , onSaveSuccess, onSaveError);

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
           
            $timeout(function () {
              $state.go($state.current.name, {}, {reload: true});
          }, 1000);
            
          }

          function onSaveError() {
            console.log('error  ');
          }
        }
      }
    };

    $scope.removeCol = function ($index) {
      if ($index > -1 && $scope.table.columns.length > 1) {
        $scope.table.columns.splice($index, 1);
        for (var i = 0, len = $scope.table.rows.length; i < len; i++) {
          $scope.table.rows[i].cells.splice($index, 1);
        }
      }
    };

    $scope.removeRow = function ($index) {
      if ($index > -1 && $scope.table.rows.length > 1) {
        $scope.table.rows.splice($index, 1);
      }
    };

    $scope.addCol = function () {
      var len = $scope.table.columns.length,
        rowLen = $scope.table.rows.length;
      $scope.table.columns.push({
        value: 'Col ' + len
      });
      for (var i = 0; i < rowLen; i++) {
        $scope.table.rows[i].cells.push({
          value: 'Call ' + i + ',' + len
        });
      }
    };

    $scope.addRow = function () {
      var row = {
          cells: []
        },
        rowLen = $scope.table.rows.length,
        colLen = $scope.table.columns.length;

      for (var i = 0; i < colLen; i++) {
        row.cells.push({
          value: 'Cell ' + rowLen + ',' + i
        });
      }
      $scope.table.rows.push(row);
    };


    function cancel() {
      console.log('clicked');
      //$scope.table = buildTable();
      $scope.isEdit = false;
    }

    ContractsService.getOrganizationPolicy({
      organisationId: vm.contract.issuerId
    }).$promise.then(function (result) {
      vm.policy = result;
    });

    function initSupplementDimensions() {
      console.log("initSupplementDimensions");
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.type == 'SUPPLEMENT') {
          console.log("SUPPLEMENT ");
          vm.supplementDimensions.push(dimension);
          if(dimension.hasDefault == true){
            console.log("has default")
            vm.defaultSupplementDimensions.push(dimension);
            if(dimension.existenceDTOList.length > 0){
              console.log(dimension.existenceDTOList);
              angular.forEach(dimension.existenceDTOList, function(ex){
                console.log("ex");
                console.log(ex);
                vm.defaultSupDim  = ex;
              });
              
            }
            
          }
        }
      });
    }

    function initParameters() {
      
      if (vm.contract.params.length > 0) {
        angular.forEach(vm.contract.params, function (parameter) {
          if (parameter.type == "DIMENSION") {
            if (parameter.exclusive == true) {
              vm.dimensionExclusiveParameters.push(parameter);
            } else {
              vm.basicParameters.push(parameter);
            }
            //vm.dimensionExclusiveParameters.push(parameter);
          }else if (parameter.type == "BASIC"){
            vm.basicParameters.push(parameter);
          } 
          else if (parameter.type == "NOTE") {
            vm.notesParameters.push(parameter);
          } else if (parameter.type == "DISCOUNT") {
            vm.discountParameters.push(parameter);
          }
          else if (parameter.type == "ARRIVAL_AND_CHECKOUT") {
            vm.timeParameters.push(parameter);
          }
          else if (parameter.type == "CANCELLATION") {
            vm.cancellationParameters.push(parameter);
          }
          else if (parameter.type == "PAYMENT") {
            vm.paymentParameters.push(parameter);
          }
          else if (parameter.type == "OCCUPANCY") {
            vm.occupancyParameters.push(parameter);
          }
          else if (parameter.type == "MINIMUM_STAY") {
            vm.minimumStayParameters.push(parameter);
          }
          else if (parameter.type == "SPECIAL_OFFER") {
            vm.specialOfferParameters.push(parameter);
          }
          else if (parameter.type == "CLOSED") {
            vm.closedParameters.push(parameter);
          }
          else if (parameter.type == "SINGLE_ROOM_RATE_POLICY") {
            vm.singleRoomRateParameters.push(parameter);
            console.log("vm.singleRoomRateParameters");
            console.log(vm.singleRoomRateParameters);

          }
          //MAXIMUM_ROOM_OCCUPANCY
          //   vm.noteCategory = ["DISCOUNT", "PAYMENT", "OCCUPANCY" , "CANCELLATION",  "ARRIVAL_AND_CHECKOUT", "NOTE" ];

        });
      } else {
        var temp = [];
        var count = 0;
        var groupSize = 2; // number of parameters in the basic information table

        //categorize the basi info parameters into groups
        angular.forEach(vm.dimensionNotExclusiveParameters, function (parameter) {

          var valueTmp = {};
          valueTmp.type = "value";
          valueTmp.value = parameter.value;
          valueTmp.dimensionType = parameter.parameter;

          var parameterTmp = {};
          parameterTmp.type = "parameter";
          parameterTmp.value = parameter.parameter;

          if ((count % groupSize) == 0 && count > 0) {
            vm.dimensionNotExclusiveParameterGroups.push(temp);
            temp = [];
          }

          temp.push(parameterTmp);
          temp.push(valueTmp);

          count = count + 1; //increment count 
        });

        if (temp.length > 0) {
          vm.dimensionNotExclusiveParameterGroups.push(temp);
        }

      }
    }

    function initRates() {
      //check if there are rates
      if (vm.contract.rates.length === 0) {
        vm.ratesAvailable = false;

        console.log("no rates");
        console.log("calling initDim.");
        initDimensions();
        console.log("initDim called.");
      
      } else {
        //if rates are present...
        vm.ratesAvailable = true;
        vm.oldRates = entity.rates;
        angular.forEach(vm.oldRates, function (rate) {
          vm.value.push(rate.value);
        });
        console.log("edit rates");
        initDimensions();
      }
    } //end initialization of rates
    function initDimensions(){
      console.log("initDimensions");
      angular.forEach(vm.dimensionSet, function (dimension) {
        
        i++;
       if (i == 3) {
          vm.thirdItem = dimension;
          console.log("vm.thirdItem");
          console.log(vm.thirdItem);
       }
        if (dimension.type == 'BASIC') {
          vm.basicDim.push(dimension);

          vm.basicDimId.push(dimension.id);
          // 
          vm.basicExist = dimension.existenceDTOList;

          vm.exiList[j] = [];
          angular.forEach(vm.basicExist, function (ex) {
            vm.exiList[j].push(ex.id);
            vm.exitList.push(ex.id);
          });
          j++;
        }
      });

      vm.dimLength = vm.basicDim.length;
      if (vm.dimLength == 3) {
        vm.inner = _.chunk(
          vm.value,
          vm.dimensionSet[0].existenceDTOList.length *
          vm.dimensionSet[1].existenceDTOList.length
        );
        vm.figures = [];

        angular.forEach(vm.inner, function (obj) {
          vm.figures.push(
            _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
          );
        });
      } else if (vm.dimLength == 2) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(
          _.chunk(
            vm.inner,
            vm.dimensionSet[0].existenceDTOList.length
          )
        );

      } else if (vm.dimLength == 1) {
        vm.inner = vm.value;
        vm.figures = [];

        vm.figures.push(vm.inner);

      }
    }
    function filterSupplements(dimensionType) {
      var list = [];

      // if (dimensionType == -1) {
      //   dimensionType = null;
      // }

      angular.forEach(vm.contract.supplements, function (supplement) {
        if (supplement.dimensionType1 == dimensionType) {
          list.push(supplement);
        }
      });
      return list;

    }


    function resolveDimensionNameByType(type) {
      var name = type;
      angular.forEach(vm.dimensions, function (dimension) {
        if (dimension.type == type) {
          name = dimension.name;
        }
      });

      return name;
    }


    function resolveExistenceName(dimensionType, existenceId) {
      var name = existenceId;
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.dimensionType == dimensionType) {
          angular.forEach(dimension.existenceDTOList, function (existence) {

            if (existence.id == existenceId) {
              name = existence.name;
            }

          });
        }
      });
      return name;
    }
    function updateSpecialSeasonDates(supplement) {
      var exist = supplement.existence1;
       var supplementType = supplement.dimensionType1;
      console.log(exist);
      console.log(vm.dimensionSet);
      angular.forEach(vm.dimensionSet, function(dimension){
        if(supplementType == dimension.dimensionType){
          vm.existencies = dimension.existenceDTOList;
          angular.forEach(vm.existencies, function (existence) {
            if (exist == existence.id) {
              // if( vm.existenceLabel === "Special Day" ){
              //   vm.entity.description = existence.brief;
              // }
              supplement.description = existence.brief;
              SupplementsService.update(supplement, onSupplementSaveSuccess, onSupplementSaveError);

            }
          });
        }
      })
      

    }
    function onSupplementSaveSuccess(result) {
      console.log("supplement updated successfully");
      $state.reload();
    }

    function onSupplementSaveError() {
      console.log("error updating.");
    }


    function getRatePosition(existence1, position1, existence2, position2, existence3, position3, existence4, position4) {
      var position = null;
      var dimLength = vm.basicDim.length;
      angular.forEach(vm.contract.rates, function(rate){
        if (vm.basicDim.length === 2){
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2)) {
            

            position =  vm.contract.rates.indexOf(rate);

            // console.log("position");
            // console.log(position);
           
          }
        } else if (vm.basicDim.length === 3) {
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2) && rateExists(rate, existence3, position3)){
            position =  vm.contract.rates.indexOf(rate);
          }
          
        }
        else if (vm.basicDim.length === 4) {
          if (rateExists(rate, existence1, position1) && rateExists(rate, existence2, position2) && rateExists(rate, existence3, position3)&& rateExists(rate, existence4, position4)){
            position =  vm.contract.rates.indexOf(rate);
          }
        }
        
      });
      return position
    }


    function getRatePositionWithCreate (existence1, position1, existence2, position2, existence3, position3, existence4, position4){
      // console.log("getRatePositionWithCreate");
      // console.log(existence1);
      // console.log("position1");
      // console.log(position1);

      // console.log(existence2);
      // console.log("position2");
      // console.log(position2);

      // console.log(existence3);
      // console.log("position3");
      // console.log(position3);

      // console.log(existence4);
      // console.log("position4");
      // console.log(position4);

      var position = null;
      var dimLength = vm.basicDim.length;

      position = getRatePosition(existence1, position1, existence2, position2, existence3, position3, existence4, position4);
      console.log("position");
      console.log(position);
      var tempRate = null;
      if (position === null){
        // create a new rate object
        
        tempRate = {
            "id": null,
            "value": null,
            "dim1": existence1,
            "dim2": existence2,
            "dim3": existence3,
            "dim4": existence4,
            "contractId": vm.contract.id
          }

          vm.contract.rates.push(tempRate);

          position = vm.contract.rates.indexOf(tempRate);
          
      }
      
      return position
    }
    function checkIfCredit(supplement){
     
        if(supplement.value < 0){
          var nameIndex =supplement.name.search("Supplement");
          vm.isCredit = true;
          if(nameIndex > 0){
          supplement.name = supplement.name.slice(0,supplement.name.search("Supplement")) + "Credit";
        }
          return Math.abs(supplement.value) ;
        }
  
      return supplement.value;
      
    }
    function resolveOccupancyName(usedOccupancy){
      var  occupancyName= null;
      angular.forEach(vm.roomOccupancies, function(occupancy){
        if(usedOccupancy == occupancy.id){
            occupancyName= occupancy.abbreviation;
        }
    });
      return occupancyName;
  }

  function initOccupancies(){
    RoomOccupanciesService
    .getByOrganization({
      id: vm.contract.supplierId
    })
    .$promise.then(function (results){
      vm.roomOccupancies= results;
    });
  }

  function shortenName(name){

    return name.substr(0, (name.indexOf("rates")+ 5));
}

    function rateExists(rate, existence, position) {
      position +=1;
      if (position === 1) {
        return existence === rate.dim1;
      } else if (position === 2) {
        return existence === rate.dim2;
      } else if (position === 3) {
        return existence === rate.dim3;
      } else if (position === 4) {
        return existence === rate.dim4;
      } else if (position === 5) {
        return existence === rate.dim5;
      } else if (position === undefined || position === null){
        return true;
      }

      return false;
    }

  }



})();
