(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractsController', ContractsController);

    ContractsController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        'URLS',
        'CostsService',
        'ContractsService',
        'dimensionSets',
        'ParseLinks',
        'pagingParams',
        'paginationConstants',
        '$stateParams',
        'organizations'
    ];

    function ContractsController(
        $scope,
        $location,
        $state,
        $localStorage,
        URLS,
        CostsService,
        ContractsService,
        dimensionSets,
        ParseLinks,
        pagingParams,
        paginationConstants,
        $stateParams,
        organizations
    ) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.dimensionSets = dimensionSets;
        vm.organizations = organizations;

        vm.hideCreateContract = vm.dimensionSets.length < 1 ? true : false;

        // vm.contracts = entity;

        //declare functions
        vm.loadPage = loadPage;
        vm.transition = transition;
        vm.search = search;

        //our initialization
        vm.account = null;
        vm.isAuthenticated = null;
        vm.years = [];
        vm.targets = [];
        vm.entity = {};
        vm.entity.target = '';
        vm.entity.year = '';
        vm.entity.name = '';
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.name = '';
        vm.greeting = '';
        vm.isLoading = false;

        //call functions needed to initialize data
        initYears(); //initialize years
        vm.search(); //load contracts

        function initYears() {
            var currentYear = new Date().getFullYear();
            var totalYears = 10; //number of years visible
            var numberOfYearsBack = 3; //how many years back
            for (var i = 1; i <= totalYears; i++) {
                var year = currentYear + i - numberOfYearsBack;
                vm.years.push(year);
            }
        }

        function search(source) {
            if (source == 'service') {
                //the change from service, we need to reset the target
                vm.entity.target = '';
                vm.targets = [];
            }
            console.log("init contracts");
            console.log($stateParams);
            vm.isLoading = true;
            vm.contracts = []; //clear the current view
            ContractsService.getFiltered(
                {
                    supplier: $localStorage.current_organisation.id,
                    generalService: $stateParams.generalServiceId,
                    target: vm.entity.target,
                    year: vm.entity.year,
                    name: vm.entity.name,
                    showDeleted:false,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: 'id,desc'
                },
                function(data, headers) {
                    vm.links = ParseLinks.parse(headers('link'));
                    vm.totalItems = headers('X-Total-Count');
                    vm.queryCount = vm.totalItems;
                    vm.contracts = data;
                    vm.page = pagingParams.page;
                    vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
                    vm.isLoading = false;

                    vm.noRecordsFound =
                        vm.contracts === undefined || vm.contracts.length === 0;

                    initTargets(data);
                },
                function(error) {
                    console.log('error getting contracts');
                    console.log(error);
                    vm.isLoading = false;
                }
            );
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function initTargets(data) {
            angular.forEach(data, function(org) {
                var temp = { id: org.targetId };
                vm.targets.push(temp);
            });
            resolveTargetNames();
        }

        function resolveTargetNames() {
            angular.forEach(vm.targets, function(target) {
                //get targetId
                var targetId = target.id;
                angular.forEach(vm.organizations, function(organization) {
                    if (organization.id == targetId) {
                        target.name = organization.name;
                    }
                });
            });
        }
    }
})();
