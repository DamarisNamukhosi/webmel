(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractDialogController', ContractDialogController);

    ContractDialogController.$inject = ['$timeout', '$scope', '$stateParams','$localStorage', '$uibModalInstance', 'entity', 'ContractsService', 'OrganizationsService', 'dimensionSets','ServicesService', 'markets', 'currencies'];

    function ContractDialogController($timeout, $scope, $stateParams,$localStorage, $uibModalInstance, entity, ContractsService, OrganizationsService, dimensionSets,ServicesService, markets, currencies) {
        var vm = this;

        vm.contract = entity;
        vm.clear = clear;
        vm.save = save;
        vm.dimensionSets = dimensionSets;
        vm.markets = markets;
        vm.currencies = currencies;
        
        vm.contract.startDate = new Date();
        vm.contract.startDate.setFullYear(vm.contract.startDate.getFullYear() + 1);
        vm.contract.endDate = new Date();
        vm.contract.endDate.setFullYear(vm.contract.endDate.getFullYear() + 2);
        
        vm.openedFrom = false;
        vm.openedTo = false;
        vm.contract.year= new Date().getFullYear() +1;
        console.log('year: '+ vm.contract.year);
        vm.contractYears = [2019, 2018,2017,2016,2015];
        vm.contractTypes = ["PUBLISHED", "STO", "CONTRACT"];
        vm.organizations= OrganizationsService.getOrganizations();
        vm.target = OrganizationsService.get({ id: vm.contract.targetId}); 
    
        ServicesService.get({ id: $stateParams.id}).$promise.then(function(response) {
            vm.service = response;
            vm.service.name= vm.service.name
        });

        ServicesService.getByOrganizationId({ id: $localStorage.current_organisation.id}).$promise.then(function(response) {
            vm.organizationServices = response;
        });

        //

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.contract.id !== null){
                ContractsService.updateFull(vm.contract, onSaveSuccess, onSaveError);
            } else {

                console.log('contract');
                console.log(vm.contract);
                vm.contract.name = year + " " + market + " " + service + " " + type + " rates for " + supplier + " issued to " + target;

                ContractsService.createFull(vm.contract, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.openFrom = function () {
            vm.openedFrom = true;
          };
      
          vm.openTo = function () {
            vm.openedTo = true;
          };
      

    }
})();
