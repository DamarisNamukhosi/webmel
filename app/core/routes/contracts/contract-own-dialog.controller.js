(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ContractOwnDialogController', ContractOwnDialogController);

  ContractOwnDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$uibModalInstance',
    '$localStorage',
    '$state',
    'entity',
    'ContractsService',
    'OrganizationsService',
    'ServicesService',
    'DimensionSetsService',
    'currencies',
    'organizationTypes',
    'MarketsService',
    'LocationsService',
    'markets',
    'SeasonGroupsService'
  ];

  function ContractOwnDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    $state,
    entity,
    ContractsService,
    OrganizationsService,
    ServicesService,
    DimensionSetsService,
    currencies,
    organizationTypes,
    MarketsService,
    LocationsService,
    markets,
    SeasonGroupsService
  ) {
    var vm = this;

    //injected values
    vm.entity = entity;
    console.log(vm.entity);
    vm.markets = markets;
    vm.currencies = currencies;
    vm.organizationTypes = organizationTypes;
    
    //assign functions to vm
    vm.clear = clear;
    vm.save = save;
    vm.organizationTypeChange = organizationTypeChange;
    vm.serviceChange = serviceChange;
    vm.supplierChange = supplierChange;
    vm.issuerChanged = issuerChanged;
    vm.organizationChange = organizationChange;
    vm.dimensionSetChange = dimensionSetChange;
    vm.resolveDimensionExistencesByType = resolveDimensionExistencesByType;
    vm.updateDimensionType = updateDimensionType;
    vm.initDatePickers = initDatePickers;
    vm.yearChange = yearChange;
    vm.objectChange = objectChange;
    vm.seasonGroupChange = seasonGroupChange;
    //vm.initOrganizationServices = initOrganizationServices;
    vm.onContractTypeChange = onContractTypeChange;
    vm.getLocation = getLocation;
    vm.refresh = refresh;
    vm.getLocs = getLocs;
    vm.onSupplierSelected = onSupplierSelected;
    vm.onTargetSelected = onTargetSelected;
    vm.searchTargetOrg = searchTargetOrg;
    vm.dmState = $state.current.name.includes("cost-contracts.new");

    //initiliaze data
    vm.dimensionSets = [];
    vm.exclusiveContractParameters = [];
    vm.services = [];
    vm.seasonGroups = [];
    vm.contractYears = [];
    vm.openedFrom = false;
    vm.openedTo = false;
    vm.disableSelectBox = false;
    vm.disableServiceBox = false;
    vm.disableDimensionSetsBox = false;
    vm.contractTypes = ["PUBLISHED", "STO", "CONTRACT"];
    vm.organizationType = 0;
    vm.suppliers = [];
    vm.contractParameters = [];
    vm.contractExclusiveParameters = [];
    vm.contractParams = [];
    vm.setDimensionsWithExistences = [];
    vm.organizationServices = [];
    vm.organizations = [];
    vm.serviceName = "";
    vm.targetName = "";
    vm.basicDimensionCount = 0;
    vm.basicDimensionSets = [];
    vm.contractParamsCount = 0;
    vm.disablePreview = false;
    vm.disablePreviewFunction = disablePreviewFunction;
    vm.showTargetOrg = false;
    vm.nonContractOrganizationTypes = [1, 8, 9];
    vm.objectTypes = ["PERSON", "VEHICLE"];
    vm.inDestinationAuthority =  false;


    //init functions
    initYears();
    initDatePickers();
    initContractParameters();
    initOrganizationServices();
    initContractResolutionConfigs();
    resolveService($stateParams.id);


    function onContractTypeChange() {
      if (vm.entity.type === 'CONTRACT') {
        vm.showTargetOrg = !vm.showTargetOrg;
      }
    }
    function issuerChanged(objectId) {
      angular.forEach(vm.issuers, function (issuer) {
        if (issuer.id == objectId) {
          vm.entity.issuerName = issuer.name;
          vm.entity.issuerUuid = issuer.uuid;
        }
      });
    }

    function refresh($item, $model) {
      console.log("refreshing...");
      vm.entity.targetId = $item.id;
      vm.entity.targetName = $item.name;
    }

    function getLocation(name, type) {
      console.log("getLocation...");
      console.log(name);
      console.log(type);
      return OrganizationsService.getOrganizationsByType({
        type: type,
        name: name
      }).$promise
        .then(function (results) {
          return results.map(function (item) {
            console.log(item);
            return item;
          })
        });
    }


    function getLocs(val, type) {
      return OrganizationsService
        .getOrganizationsByType({
          type: type,
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
      organizationChange();
    }

    function onSupplierSelected($item, $model) {
      vm.entity.supplierId = $item.id;
      vm.entity.supplierName = $item.name;
      if (vm.entity.supplierId > 0) {
        OrganizationsService.getParent({
          id: vm.entity.supplierId
        }).$promise.then(function (respose) {
          vm.issuers = respose;
        });
      }
      organizationChange();
    }

    function onTargetSelected($item, $model) {
      vm.entity.targetId = $item.id;
      vm.entity.targetName = $item.name;
      if (vm.entity.supplierId > 0) {
        OrganizationsService.getParent({
          id: vm.entity.supplierId
        }).$promise.then(function (respose) {
          vm.issuers = respose;
        });
      }
    }

    function searchTargetOrg(name) {
      console.log("serarching target...");
      console.log(name);
      getLocation(name, 4)
    }

    function disablePreviewFunction() {
      vm.disablePreview = true;
      console.log('vm.disablePreview');
      console.log(vm.disablePreview);
    }


    console.log(vm.organizationTypes);
    console.log('vm.organizationTypes');

    // remove invalid orgs
    var validOrgTypes = [];
    angular.forEach(vm.organizationTypes, function (organizationType) {
      if (vm.nonContractOrganizationTypes.includes(organizationType.id)) {
        console.log('organizationType');
        console.log(organizationType.name, organizationType.id);
      } else {
        validOrgTypes.push(organizationType);
      }
    });

    console.log(validOrgTypes);
    console.log('validOrgTypes');

    vm.organizationTypes = validOrgTypes;





    //set preset dates for new contracts
    if (vm.entity.id == null || vm.entity.id == undefined) {
      vm.entity.startDate = new Date();
      vm.entity.startDate.setFullYear(vm.entity.startDate.getFullYear());
      vm.entity.endDate = new Date();
      vm.entity.endDate.setFullYear(vm.entity.endDate.getFullYear() + 1);
      vm.entity.year = new Date().getFullYear();
      getSeasonGroups();
    } else {
      //what do we do?
    }
    function yearChange() {
      console.log("yearChange");
      getSeasonGroups();
      initContractResolutionConfigs();
    }
    function objectChange() {
      console.log("objectChange");
      // getSeasonGroups();
      initContractResolutionConfigs();
    }
    function getSeasonGroups() {
      SeasonGroupsService.getByOranizationIdAndYear({ id: vm.entity.supplierId, year: vm.entity.year }).$promise.then(function (response) {
        vm.seasonGroups = response;
        console.log("vm.seasonGroups");
        console.log(vm.seasonGroups);
      });
    }
    function seasonGroupChange(seasonGroup) {
      console.log("seasonGroup");
      console.log(seasonGroup.seasons[0].seasonBandDTOList[0].fromDate);
      console.log(seasonGroup.seasons[(seasonGroup.seasons.length - 1)].seasonBandDTOList[0].toDate);
      //loc_array.length-1
      vm.entity.startDate = seasonGroup.seasons[0].seasonBandDTOList[0].fromDate;
      //vm.entity.startDate.setFullYear(vm.entity.startDate.getFullYear());
      console.log(seasonGroup.seasons[(seasonGroup.seasons.length - 1)]);
      vm.entity.endDate = seasonGroup.seasons[(seasonGroup.seasons.length - 1)].seasonBandDTOList[0].toDate
      initDatePickers();
      //vm.entity.endDate.setFullYear(vm.entity.endDate.getFullYear() + 1);
    }
    function organizationChange(editFlag) {
      //get services by org
      if (editFlag) {
        OrganizationsService.getOrganizations().$promise.then(function (response) {
          vm.organizations = response;
        });
        console.log(vm.organizations);
        var orgId = $stateParams.id;
        ServicesService.get({
          id: orgId
        }).$promise.then(function (response) {
          vm.services = response;
          console.log("vm.services");
          console.log(vm.services);
          vm.serviceName = response.name;
          OrganizationsService.getParent({
            id: vm.services.organisationId
          }).$promise.then(function (response) {
            vm.issuers = response;
          });
        });
        serviceChange(editFlag);
      } else {
        console.log('editlog is not true');
        var orgId = vm.entity.supplierId;

        ServicesService.getByOrganizationId({
          id: orgId
        }).$promise.then(function (response) {
          vm.services = response;
        });
        MarketsService.getByOrganisation({
          id: vm.entity.supplierId
        },
          function (data) {
            vm.markets = data;
          },
          function (error) {
            console.log('error getting markets for issuer ' + vm.entity.supplierId);
            console.log(error);
          });

      }
      getSeasonGroups();
    }

    function serviceChange(editFlag) {
      console.log('service changed to..');
      console.log(vm.entity.serviceId);
      if (editFlag == true) {
        var svcId = $stateParams.id;
        DimensionSetsService.getByServiceId({
          id: svcId
        }).$promise.then(function (response) {
          vm.dimensionSets = response;
        });
      } else {

        var svcId = vm.entity.serviceId;
        DimensionSetsService.getByServiceId({
          id: svcId
        }).$promise.then(function (response) {
          vm.dimensionSets = response;
        });

        OrganizationsService.getOrganizations().$promise.then(function (response) {
          vm.organizations = response;
        });
      }
      initContractResolutionConfigs();
    };

    vm.addDimensionRow = function () {
      var param = {
        "exclusive": false,
        "id": null,
        "notes": null,
        "parameter": null,
        "type": "DIMENSION",
        "uuid": null,
        "value": null
      };

      vm.contractParams.push(param);
    };

    vm.removeDimensions = function ($index) {
      vm.contractParams.splice($index, 1);
    };


    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      console.log(vm.entity);
      vm.entity.contractParamDTOList = [];
      console.log('saving..');
      console.log(vm.entity);
      vm.isSaving = true;
      if (vm.entity.id !== null) {
        // vm.contractParameters // we need to copy from here
        console.log('vm.entity.id');
        console.log(vm.entity.id);
        vm.entity.params = []; //reset the params


        angular.forEach(vm.contractParameters, function (parameter) {

          var param = {};
          param.id = parameter.id;
          param.type = parameter.type;
          param.parameter = parameter.parameter;
          param.value = parameter.value;
          param.exclusive = parameter.exclusive;
          param.notes = parameter.notes;
          param.uuid = parameter.uuid;
          param.contractId = parameter.contractId;

          vm.entity.contractParamDTOList.push(param);

        });

        //delete the parameters that is not needed
        delete vm.entity.rates;
        delete vm.entity.supplements;


        console.log("entity before update");
        console.log(vm.entity);
        ContractsService.updateFull(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      } else {
        console.log("new contract");
        vm.entity.contractParamDTOList = [];

        // vm.contractParameters // we need to copy from here
        console.log("b4 adding exlcusive params");
        console.log(vm.contractParameters);
        console.log(vm.entity);
        angular.forEach(vm.contractParameters, function (parameter) {
          if (parameter.exclusive == true) {
            if (angular.isObject(parameter.value)) {
              angular.forEach(parameter.value, function (val) {
                if (val > 1) {
                  var oneValue = {
                    "id": null,
                    "exclusive": parameter.exclusive,
                    "contractId": null,
                    "contractName": null,
                    "notes": parameter.notes,
                    "parameter": parameter.parameter,
                    "type": "DIMENSION",
                    "uuid": null,
                    "value": val
                  };
                  vm.entity.contractParamDTOList.push(oneValue);
                }
              });

            } else {
              var singleValue = {
                "id": null,
                "exclusive": parameter.exclusive,
                "contractId": null,
                "contractName": null,
                "notes": parameter.notes,
                "parameter": parameter.parameter,
                "type": "DIMENSION",
                "uuid": null,
                "value": parameter.value
              };
              vm.entity.contractParamDTOList.push(singleValue);

            }
          } else {
            var param = {
              "id": null,
              "exclusive": parameter.exclusive,
              "contractId": null,
              "contractName": null,
              "notes": parameter.notes,
              "parameter": parameter.parameter,
              "type": "DIMENSION",
              "uuid": null,
              "value": parameter.value
            };
          }
          vm.entity.contractParamDTOList.push(param);
        });
        populateContractName();
        console.log(vm.exclusiveContractParameters);
        angular.forEach(vm.exclusiveContractParameters, function (parameter) {
          var i = 1;
          var index = (vm.exclusiveContractParameters.indexOf(parameter) + 1);
          var dimX = "dim" + index;
          vm.entity[dimX] = parameter.selectedValue;
          angular.forEach(parameter.existenceDTOList, function (exist) {
            if (exist.id == parameter.selectedValue) {
              //"dimensionType" : "MARKET",
              if (exist.dimensionType == "MARKET") {
                parameter.notes = exist.name + " MARKET";
                vm.entity.name = vm.entity.name + "For " + parameter.notes;

              } else {
                parameter.notes = exist.name;
                vm.entity.name = vm.entity.name + "For " + parameter.notes;

              }
              var target = resolveTarget(vm.entity.targetId);

              if (target == '') {
                vm.entity.name = vm.entity.name;
              } else {
                vm.entity.name =  vm.entity.name + " issued to " + target;
              }

            }
          });
          var oneValue = {
            "id": null,
            "exclusive": true,
            "contractId": null,
            "contractName": null,
            "notes": parameter.notes,
            "parameter": parameter.dimensionType,
            "type": "DIMENSION",
            "uuid": null,
            "value": parameter.selectedValue
          };
          i = i + 1;
          vm.entity.contractParamDTOList.push(oneValue);

        });
        console.log(vm.entity.contractParamDTOList);
        angular.forEach(vm.entity.contractParamDTOList, function (param) {
          if (param === undefined) {
            vm.entity.contractParamDTOList.splice(vm.entity.contractParamDTOList.indexOf(param), 1);
          }
        });

        console.log("vm.entity");

        console.log("ready to created...");
        console.log(vm.entity);

        ContractsService.createFull(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
    vm.openFrom = function () {
      vm.openedFrom = true;
    };

    vm.openTo = function () {
      vm.openedTo = true;
    };

    function resolveMarket(id) {
      var value = '';
      angular.forEach(vm.markets, function (market) {
        if (market.id == id) {
          value = market.name;
        }
      });
      return value;
    }

    function resolveService(id) {
      console.log("resolveService...");
      console.log($stateParams);
      var value = '';
      console.log("resolve service")

      if (!vm.inPropertyOrgType) {
        console.log("services")
        console.log(vm.services)
        angular.forEach(vm.services, function (object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      } else {
        angular.forEach(vm.services, function (object) {
          if (object.id == vm.entity.serviceId) {
            value = object.name;
          }
        });
        return value;
      }
    }

    function resolveSupplier(id) {
      var value = '';
      if (!vm.inPropertyOrgType) {
        angular.forEach(vm.suppliers, function (object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      } else {
        return $localStorage.current_organisation.name;
      }
    }

    function resolveTarget(id) {
      console.log("id");
      console.log(id);
      if (vm.inPropertyOrgType) {
        console.log("target");
        console.log(vm.organizations);
        angular.forEach(vm.organizations, function (object) {
          if (object.id == id) {
            target = object.name;
          }
        });
        console.log("return");
        console.log(target);
        return target;
        // var target = vm.targetName;
        // return target;
      } else {
        var target = $localStorage.current_organisation.name;
        if (target == undefined) {
          return '';
        }
        return target;
      }

    }

    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 10; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = (currentYear + i) - numberOfYearsBack;
        vm.contractYears.push(year);
      }
    }

    function organizationTypeChange() {

      vm.suppliers = [];
      vm.markets = [];
      vm.dimensionSets = [];
      vm.services = [];
      vm.entity.supplierId = null;
      vm.entity.marketId = null;
      vm.entity.dimensionSetId = null;
      vm.entity.serviceId = null;
      if (vm.inPropertyOrgType) {
        console.log(vm.inPropertyOrgType);
        console.log(vm.orgType);

      } else {
        vm.organizationType = 4
        OrganizationsService.getOrganizationsByType({
          type: vm.organizationType
        },
          function (data) {
            vm.suppliers = data;
          },
          function (error) {
            console.log('error getting organizations of type ' + vm.organizationType);
            console.log(error);
          });
      }
    }


    function supplierChange() {
      console.log("supplierChange");
      console.log(vm.entity.supplierId);

      console.log('changed supplier to : ' + vm.entity.supplierId);
      vm.dimensionSets = [];
      vm.contractParameters = [];
      vm.markets = [];
      vm.dimensionSets = [];
      vm.services = [];
      vm.entity.marketId = null;
      vm.entity.dimensionSetId = null;
      vm.entity.serviceId = null;

      ServicesService.getByOrganizationId({
        id: vm.entity.supplierId
      },
        function (data) {
          vm.services = data;
          console.log(vm.services);
        },
        function (error) {
          console.log('error getting services of organization ' + vm.entity.supplierId);
          console.log(error);
        });

      MarketsService.getByOrganisation({
        id: vm.entity.supplierId
      },
        function (data) {
          vm.markets = data;
        },
        function (error) {
          console.log('error getting markets for issuer ' + vm.entity.supplierId);
          console.log(error);
        });
      getSeasonGroups();
    }

    function setBasicCount() {
      angular.forEach(vm.setDimensionsWithExistences, function (setDimension) {
        if (setDimension.type === 'BASIC') {
          vm.basicDimensionCount = vm.basicDimensionCount + 1;
        }
      })
    }

    function dimensionSetChange(editFlag) {
      vm.contractParameters = [];
      console.log('changed dim set to : ' + vm.entity.dimensionSetId);
      console.log('year' + vm.entity.year);
      console.log('seasongrp' + vm.entity.seasonGroupId);


      //get the set dimensions with their existences in this set
      DimensionSetsService.getSetDimensionExistenciesBySetId({
        id: vm.entity.dimensionSetId,
        year: vm.entity.year,
        seasonGroupId: vm.entity.seasonGroupId
      }, function (data) {
        vm.setDimensionsWithExistences = data;

        vm.basicDimensionCount = 0;
        setBasicCount();

        // console.log('vm.setDimensionsWithExistences');
        // console.log(vm.setDimensionsWithExistences);


        var param = {
          "id": null,
          "exclusive": false,
          "contractId": null,
          "contractName": null,
          "notes": null,
          "parameter": null,
          "type": "DIMENSION",
          "uuid": null,
          "value": null
        };

        // vm.contractParams.push(param);


        if (editFlag) {

          //add logic for edit

          angular.forEach(vm.entity.params, function (parameter) {

            if (parameter.type == 'DIMENSION' && parameter.exclusive == false) {
              angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
                if (dimension.dimensionType == parameter.parameter) {
                  parameter.displayName = dimension.dimensionName;
                  parameter.existences = dimension.existenceDTOList;
                }
              });

              vm.contractParameters.push(parameter);
            } else if (parameter.type == 'DIMENSION' && parameter.exclusive == true) {
              angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
                if (dimension.dimensionType == parameter.parameter) {
                  parameter.displayName = dimension.dimensionName;
                  parameter.existences = dimension.existenceDTOList;
                }
              });

              vm.exclusiveContractParameters.push(parameter);
            }

          });

          console.log(vm.entity);
          console.log(vm.entity.serviceId);
          var svcId = vm.entity.serviceId;
          dimensionSetChange(false);
          console.log("vm.dimensionSets");
          console.log(vm.dimensionSets);
          angular.forEach(vm.dimensionSets, function (current) {
            if (current.id == vm.entity.dimensionSetId) {
              selectedSet = current;

              vm.contractParamsCount = 0;
              angular.forEach(current.dimensionDTOList, function (dimension) {
                if (dimension.type == 'SUPPLEMENT' && dimension.hasDefault == true) {

                  var param = {
                    "type": "DIMENSION",
                    "parameter": dimension.dimensionType, // we are support the enum and not id
                    "displayName": dimension.dimensionName,
                    "disabled": true,
                    "value": null,
                    "exclusive": false,
                    "notes": null,
                    "uuid": null,
                    "contract": null,
                    "existences": resolveDimensionExistencesv1(dimension.dimensionId)
                  };
                  vm.contractParameters.push(param);
                  vm.contractParamsCount = vm.contractParamsCount + 1;
                }
                if (dimension.type == 'EXCLUSIVE') {

                  var param = {
                    "type": "DIMENSION",
                    "parameter": dimension.dimensionType, // we are support the enum and not id
                    "displayName": dimension.dimensionName,
                    "disabled": true,
                    "value": null,
                    "exclusive": true,
                    "notes": null,
                    "uuid": null,
                    "contract": null,
                    "existences": resolveDimensionExistencesv1(dimension.dimensionId)
                  };
                  vm.contractParameters.push(param);
                  vm.contractParamsCount = vm.contractParamsCount + 1;
                }
              });

              console.log('parameters');
              console.log(vm.contractParameters);

            }
          });
          console.log('vm.contractParameters');
          console.log(vm.contractParameters);
          console.log("exclusiveContractParameters");
          console.log(vm.exclusiveContractParameters);
          // var nameIndex =vm.entity.name.search("for");
          // console.log(nameIndex);
          console.log(vm.exclusiveContractParameters.length);

          console.log(vm.exclusiveContractParameters);


          console.log("vm.entity");
          console.log(vm.entity);
        } else {
          //logic for new

          //get the set dimensions for the dimensionSetId
          var selectedSet = null;
          angular.forEach(vm.dimensionSets, function (current) {
            if (current.id == vm.entity.dimensionSetId) {
              selectedSet = current;

              vm.contractParamsCount = 0;
              angular.forEach(current.dimensionDTOList, function (dimension) {
                if (dimension.type == 'SUPPLEMENT' && dimension.hasDefault == true) {

                  var param = {
                    "type": "DIMENSION",
                    "parameter": dimension.dimensionType, // we are support the enum and not id
                    "displayName": dimension.dimensionName,
                    "disabled": true,
                    "value": null,
                    "exclusive": false,
                    "notes": null,
                    "uuid": null,
                    "contract": null,
                    "existences": resolveDimensionExistencesv1(dimension.dimensionId)
                  };
                  vm.contractParameters.push(param);
                  vm.contractParamsCount = vm.contractParamsCount + 1;
                }
                if (dimension.type == 'EXCLUSIVE') {

                  var param = {
                    "type": "DIMENSION",
                    "parameter": dimension.dimensionType, // we are support the enum and not id
                    "displayName": dimension.dimensionName,
                    "disabled": true,
                    "value": null,
                    "exclusive": true,
                    "notes": null,
                    "uuid": null,
                    "contract": null,
                    "existences": resolveDimensionExistencesv1(dimension.dimensionId)
                  };
                  vm.contractParameters.push(param);
                  vm.contractParamsCount = vm.contractParamsCount + 1;
                }
              });

              console.log('parameters');
              console.log(vm.contractParameters);

            }
          });
        }
      });

    }

    function resolveDimensionExistencesv1(dimensionId) {
      var list = [];
      if (vm.setDimensionsWithExistences !== undefined) {
        angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
          // console.log(dimension);
          if (dimensionId == dimension.dimensionId) {
            list = dimension.existenceDTOList;
          }
        })
      }
      return list;
    }


    function resolveDimensionExistencesByType(dimensionType) {
      // console.log('looking for existences for ' + dimensionId);
      var list = [];
      if (vm.setDimensionsWithExistences !== undefined) {
        angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
          // console.log('looking for existences for ' + dimensionId + ' compare with ' + dimension.id);
          console.log(dimension);
          if (dimensionType == dimension.dimensionType)
            list = dimension.existenceDTOList;
          // console.log('found a dimension' + dimensionId);
        })
      }
      return list;
    }

    function updateDimensionType(parameter, dimensionType) {

      console.log(parameter);
      console.log(dimensionType);

      if (parameter.type == 'DIMENSION') {
        paramter.parameter = dimensionType;
      }

    }

    function initDatePickers() {
      //check if date returned in object is null
      //if not null
      //populate the date pickers
      //new Date(year, month, day, hours, minutes, seconds, milliseconds);
      if (vm.entity.startDate !== null && vm.entity.endDate !== null) {
        // vm.entity.startDate = new Date(Date.parse(vm.entity.startDate)).toISOString();
        vm.entity.startDate = new Date(vm.entity.startDate);
        vm.entity.endDate = new Date(vm.entity.endDate);
      }
    }


    function initContractParameters() {

      if (vm.entity.id !== null) {
        console.log('calling dimensionSetChange');
        dimensionSetChange(true);
      }

    }

    function initOrganizationServices() {
      //organisationTypeName:"TOUR OPERATOR"
      vm.currentOrganization = $localStorage.current_organisation;
      vm.inPropertyOrgType = true;
      if(vm.currentOrganization.organisationTypeId == 'TOUR OPERATOR'){
        if($localStorage.current_user_permissions.includes('TRANSPORTER_USER')){
          vm.inPropertyOrgType = true;
        }else{
          vm.inPropertyOrgType = false;
        }
      }
      if(vm.currentOrganization.organizationType == 'DESTINATION AUTHORITY'){
        vm.inDestinationAuthority = true;
      }
      //DESTINATION AUTHORITY	
        console.log('vm.inPropertyOrgType');
        console.log(vm.inPropertyOrgType);
      if (vm.inPropertyOrgType && vm.entity !== undefined) {
        console.log('calling organizationChange');
        if (vm.entity.supplierId > 0) {
          OrganizationsService.getParent({
            id: vm.entity.supplierId
          }).$promise.then(function (respose) {
            vm.issuers = respose;
          });


          serviceChange(true);
        } else {
          vm.entity.supplierId = $localStorage.current_organisation.id;
          OrganizationsService.getParent({
            id: vm.entity.supplierId
          }).$promise.then(function (respose) {
            vm.issuers = respose;
          });
          serviceChange(false);

        }
        ServicesService.getByOrganizationId({ id: vm.entity.supplierId }).$promise.then(function (results) {
          vm.services = results;
          angular.forEach(vm.services, function (orgSvc) {
            if ($stateParams.id > 0) {
              if ($stateParams.id == orgSvc.id) {
                vm.serviceName = orgSvc.name;
              }
            }
          })
        });

      } else {
        console.log('not true');
        if (vm.entity.supplierId > 0) {
          OrganizationsService.getParent({
            id: vm.entity.supplierId
          }).$promise.then(function (respose) {
            vm.issuers = respose;
          });
          serviceChange(true);
        } else {
          serviceChange(false);
        }
      }
      ServicesService.getByOrganizationId({ id: vm.entity.supplierId }).$promise.then(function (results) {
        vm.services = results;
      });
    }

    function initContractResolutionConfigs() {
      console.log(vm.entity.year);
      console.log(vm.entity);
      console.log(vm.entity.supplierId);
      vm.exclusiveContractParameters = [];
      ContractsService.getContractResolutionConfig({ id: vm.entity.serviceId, year: vm.entity.year, type: vm.entity.objectType }).
        $promise.then(function (response) {
          console.log(response);
          angular.forEach(response, function (dim) {
            if (dim.existenceDTOList.length > 0) {
              //angu
              angular.forEach(dim.existenceDTOList, function (exit) {
                exit.selectedValue = exit.id;
              })
              dim.selectedValue = dim.existenceDTOList[0].id;
            }
          });
          vm.exclusiveContractParameters = response;
          console.log(vm.exclusiveContractParameters);
          console.log(vm.exclusiveContractParameters.length);
        });
    }
    function populateContractName() {
      var year = vm.entity.year;
      //var market = resolveMarket(vm.entity.marketId);
      var service = resolveService(vm.entity.serviceId);
      //var supplier = resolveSupplier(vm.entity.supplierId);
      var type = vm.entity.type;

      //different names
      vm.entity.name = year + " " + service + " " + type + " rates ";

    }
  }
})();
