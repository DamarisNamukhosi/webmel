(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractsOwnListController', ContractsOwnListController);

    ContractsOwnListController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$stateParams',
        '$localStorage',
        'entity',
        'URLS',
        'CostsService',
        'services',
        'suppliers',
        'ContractsService'
    ];

    function ContractsOwnListController(
        $scope,
        $location,
        $state,
        $stateParams,
        $localStorage,
        entity,
        URLS,
        CostsService,
        services,
        suppliers,
        ContractsService
    ) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.supplierId = $stateParams.supId;

        console.log('supid: ' + vm.supplierId);

        vm.contracts = entity;
        console.log('vm.contracts');
        console.log(vm.contracts);

        vm.service = service;
        vm.suppliers = suppliers;

        angular.forEach(vm.suppliers, function(sup) {
            console.log('sup.name');
            console.log(sup.name);
        });

        console.log('vm.suppliers');
        console.log(vm.suppliers);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound =
            vm.contracts === undefined || vm.contracts.length === 0;
        vm.name = '';
        vm.greeting = '';
        vm.greet = function greet() {
            vm.greeting = vm.name ? 'Hey, ' + vm.name + '!' : '';
            console.log(vm.greeting);
        };
        //genSvcId
        console.log('vm.contracts.targetId');
        console.log(vm.contracts.targetId);
        vm.getSupplierList = function() {
            vm.contracts = ContractsService.getFiltered({
                orgId: $localStorage.current_organisation.id,
                genSvcId: $scope.svc.id
            });
            console.log('new vm.contracts');
            console.log(vm.contracts);
        };
        vm.getContractListByYear = function() {
            console.log('vm.contracts.targetId');

            console.log($scope.year);

            vm.contracts = ContractsService.getFiltered({
                orgId: $localStorage.current_organisation.id,
                yr: $scope.year
            });
            console.log('new vm.contracts');
            console.log(vm.contracts);
        };
        //
        vm.getContractsListBySupplier = function() {
            console.log('vm.obj.id');

            console.log($stateParams.supId);

            vm.contracts = ContractsService.getFiltered({
                orgId: $localStorage.current_organisation.id,
                supId: $stateParams.supId
            });
            console.log('new vm.contracts');
            console.log(vm.contracts);
        };
        vm.search = function() {
            console.log('filtering...');

            if (
                $scope.svc.id !== 'undefined' &&
                $stateParams.supId !== 'undefined' &&
                $scope.year !== 'undefined'
            ) {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id,
                    genSvcId: $scope.svc.id,
                    supId: $stateParams.supId,
                    yr: $scope.year
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
                //$scope.noRecordsFound = (vm.contracts === undefined || vm.contracts.length === 0);
            } else if (
                $scope.svc.id !== 'undefined' &&
                $stateParams.supId !== 'undefined'
            ) {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id,
                    genSvcId: $scope.svc.id,
                    supId: $stateParams.supId
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
                //$scope.noRecordsFound = (vm.contracts === undefined || vm.contracts.length === 0);
            } else if (
                $scope.ob.id !== 'undefined' &&
                $scope.year !== 'undefined'
            ) {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id,
                    supId: $stateParams.supId,
                    yr: $scope.year
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
                //$scope.noRecordsFound = (vm.contracts === undefined || vm.contracts.length === 0);
            } else if (
                $scope.obj !== 'undefined' &&
                $scope.year !== 'undefined'
            ) {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id,
                    supId: $stateParams.supId,
                    yr: $scope.year
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
                //$scope.noRecordsFound = (vm.contracts === undefined || vm.contracts.length === 0);
            } else if (
                $scope.svc.id !== 'undefined' &&
                $scope.year !== 'undefined'
            ) {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id,
                    genSvcId: $scope.svc.id,
                    yr: $scope.year
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
                // $scope.noRecordsFound = (vm.contracts === undefined || vm.contracts.length === 0);
            } else {
                vm.contracts = ContractsService.getFiltered({
                    orgId: $localStorage.current_organisation.id
                });
                console.log('vm.contracts');
                console.log(vm.contracts);
            }
            console.log('$stateParams.supId');
            console.log($stateParams.supId);
            console.log('$scope.svc.id');
            console.log($scope.svc.id);
            console.log('$scope.year');
            console.log($scope.year);
        };

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }
    }
})();
