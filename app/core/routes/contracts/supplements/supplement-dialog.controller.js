(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SupplementDialogController', SupplementDialogController);

  SupplementDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$localStorage', 'entity', 'SupplementsService', 'dimensionSet', 'OrganizationsService', 'GeneralServicesDimensionsService', 'DimensionSetsService'];

  function SupplementDialogController($timeout, $scope, $stateParams, $uibModalInstance, $localStorage, entity, SupplementsService, dimensionSet, OrganizationsService, GeneralServicesDimensionsService, DimensionSetsService) {
    var vm = this;
    console.log("editing?" + $stateParams.editing);
    // angular.fromJson

    vm.dimensionSet = dimensionSet;
    vm.rates
    //init variables
    vm.existencies = [];
    vm.existenceLabel = "Dimension";
    vm.units = ["VEHICLE", "INDIVIDUAL", "GROUP"];
    vm.billingTypes = ["VALUE", "PERCENTAGE", "RATIO"];


    //declare functions
    vm.clear = clear;
    vm.save = save;
    vm.initDimensionExistences = initDimensionExistences;
    vm.updateEntityName = updateEntityName;
    vm.resolveDimenstionType = resolveDimenstionType;
    vm.resolveExistenceName = resolveExistenceName;
    //dimensionId
    vm.checkMandatory = checkMandatory;
    vm.generalDimensions = [];
    vm.dimensionSetChange = dimensionSetChange;
    vm.dimensionSetChange2 = dimensionSetChange2;
    //init function calls here 
    initEntity($stateParams.editing);
    initDimensionExistences(vm.entity.dimensionId);
    initGeneralDimensionsService();
    vm.multiDimensinal = false;
    vm.dimensionSet2 = false;

    function resolveExistenceName(dimensionType, existenceId) {
      var name = existenceId;
      console.log("vm.dimensionSet");
      console.log(vm.dimensionSet);
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.dimensionType == dimensionType) {
          angular.forEach(dimension.existenceDTOList, function (existence) {

            if (existence.id == existenceId) {
              name = existence.name;
            }

          });
        }
      });
      console.log("resolved" + name);
      return name;
    }

    function initEntity(editing) {
      if (editing === false) {
        console.log(typeof (entity));
        console.log(_.values(entity));
        //vm.entity =  angular.fromJson(entity[0]);
        vm.entity = entity[0];
        vm.entity.dimensionSetId2 = null;
        console.log(typeof (vm.entity));

        console.log('vm.entity');
        console.log(angular.fromJson(vm.entity));
        vm.entity.dimensionSetId = null;
        console.log("paramenters");
        console.log(vm.entity.id);
        console.log(vm.entity.currencyId);
        console.log("vm.entity.dimensionId");
        console.log(vm.entity.dimensionId);
        vm.entity.dimensionId == null ? vm.entity.dimensionId = -1 : vm.entity.dimensionId;
        console.log("vm.entity.dimensionId");
        console.log(vm.entity.dimensionId);

      } else {
        console.log(typeof (entity));
        console.log(_.values(entity));
        //vm.entity =  angular.fromJson(entity[0]);
        vm.entity = entity;
        console.log(typeof (vm.entity));

        console.log('vm.entity');
        vm.entity.dimensionSetId = null;
        console.log("paramenters");
        console.log(vm.entity.id);
        console.log(vm.entity.currencyId);
        console.log("vm.entity.dimensionId");
        console.log(vm.entity.dimensionId);
        vm.entity.dimensionId == null ? vm.entity.dimensionId = -1 : vm.entity.dimensionId;
        console.log("vm.entity.dimensionId");
        console.log(vm.entity.dimensionId);

      }
    }

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      // dimensionType1: null,
      //   existence1: null,
      //   organisationName: null,

      if (vm.entity.dimensionId == -1) {
        vm.entity.dimensionId = null;
      }
      //initDimensionExistences(vm.entity.existenceId);
      if (vm.entity.id !== null) {
        console.log('updating...');
        console.log(vm.entity);
        if (vm.multiDimensinal === false) {
          console.log('entity...');
          console.log(vm.entity);
          SupplementsService.update(vm.entity, onSaveSuccess, onSaveError);
        } else {
          console.log("vm.supplement");
          console.log(vm.supplements);
          SupplementsService.update(vm.supplements, onSaveSuccess, onSaveError);
        }
        //
      } else {
        //Check if current org is Tour Operator?
        //False: organisationName will be current Org Name
        //True resolve org name.
        console.log("creating...");
        if ($localStorage.current_organisation.organisationTypeId == 3) {
          OrganizationsService.get({
            id: vm.entity.organisationId
          }).$promise.then(function (response) {
            vm.entity.organisationName = response.name;
          });
        } else if ($localStorage.current_organisation.organisationTypeId == 4 || $localStorage.current_organisation.organisationTypeId == 5) {
          vm.entity.organisationName = $localStorage.current_organisation.name;
        }
        console.log("vm.supplements.length");
        console.log(vm.supplements === undefined);
        console.log(vm.supplements == "undefined");
        console.log(vm.supplements === undefined);

        if (vm.supplements !== undefined) {
          if (vm.supplements.length > 1) {
            vm.entity.dimensionType1 = resolveDimenstionType(vm.entity.dimensionId);
            //delete vm.entity.dimensionId;
            console.log('saving...');
            console.log(vm.entity);
            console.log(vm.multiDimensinal);
            console.log(vm.dimensionSet2);
            console.log("vm.figures");
            console.log(vm.figures);
            if (vm.multiDimensinal === false) {
              console.log('saving...');
              console.log(vm.entity.dimensionType1 == -1);
              vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
              console.log(vm.entity);
              SupplementsService.create(vm.entity, onSaveSuccess, onSaveError);
            } else {
              if (vm.dimensionSet2) {
                vm.threeD = [];
                var j = 0;
                var k = 0;
                console.log("vm.supplement");
                console.log(vm.supplements);
                console.log("vm.supplement2");
                console.log(vm.supplements2);
                console.log(vm.dimensionSet == true);
                angular.forEach(vm.supplements, function (dim1) {
                  angular.forEach(vm.supplements2, function (dim2) {
                    console.log("vm.figures[j][k]" + j + " " + k);
                    console.log(vm.figures[j][k]);
                    //vm.resolveExistenceName(row.dimensionType2,row.existence2)
                    if (dim2.dimensionType1 !== null) {
                      if (vm.entity.dimensionId == -1) {
                        console.log("here");
                        vm.dimName = vm.entity.name + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2);
                      } else {
                        console.log("here");
                        vm.dimName = vm.resolveExistenceName($stateParams.dimensionType, vm.entity.existence1) + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2) + " " + "For" + " " + vm.resolveExistenceName(dim2.dimensionType2, dim2.existence2);
                      }

                    } else {
                      if ($stateParams.dimensionId == -1) {
                        console.log("here");
                        vm.dimName = vm.entity.name + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2) + " " + "For" + " " + vm.resolveExistenceName(dim2.dimensionType2, dim2.existence2);
                      } else {
                        //dimensionType vm.entity.existence1
                        console.log("here");
                        vm.dimName = vm.resolveExistenceName($stateParams.dimensionType, vm.entity.existence1) + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2) + " " + "For" + " " + vm.resolveExistenceName(dim2.dimensionType2, dim2.existence2);
                      }
                    }
                    var temp = {
                      billingType: dim1.billingType,
                      contentStatus: dim1.contentStatus,
                      contractId: dim1.contractId,
                      currencyId: dim1.currencyId,
                      description: dim1.description,
                      dimensionCount: dim1.dimensionCount + 1,
                      dimensionType1: dim1.dimensionType1,
                      existence1: dim1.existence1,
                      dimensionType2: dim1.dimensionType2,
                      existence2: dim1.existence2,
                      dimensionType3: dim2.dimensionType2,
                      existence3: dim2.existence2,
                      id: null,
                      mandatory: dim1.mandatory,
                      name: vm.dimName,
                      organisationId: dim1.organisationId,
                      supplementType: dim1.supplementType,
                      unit: dim1.unit,
                      uuid: null,
                      value: vm.figures[j][k],
                      dimensionId: dim1.dimensionId,
                      contractVersionId: $stateParams.versionId

                    };
                    vm.threeD.push(temp);
                    k = k + 1;
                  });
                  k = 0;
                  j = j + 1;
                });
                console.log("vm.threeD");
                console.log(vm.threeD);
                SupplementsService.createList(vm.threeD, onSaveSuccess, onSaveError);
              } else {
                console.log("3D");
                var i = 0;
                angular.forEach(vm.supplements, function (dim1) {
                  if (dim1.dimensionType1 !== null) {
                    if (vm.entity.dimensionId == -1) {
                      console.log("here");
                      vm.dimName = vm.entity.name + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2);
                      vm.supplements.name = vm.dimName;
                    } else {
                      console.log("here");
                      vm.dimName = vm.resolveExistenceName(dim1.dimensionType2, vm.entity.existence2);
                      vm.supplements.name = vm.dimName;
                    }

                  } else {
                    if ($stateParams.dimensionId == -1) {
                      console.log("here");
                      console.log(dim1);
                      vm.dimName = vm.entity.name + " " + "For" + " " + vm.resolveExistenceName(dim1.dimensionType2, dim1.existence2);
                      vm.supplements.name = vm.dimName;
                    } else {
                      //dimensionType vm.entity.existence1
                      console.log("here");
                      vm.dimName = vm.resolveExistenceName($stateParams.dimensionType, vm.entity.existence1);
                      vm.supplements.name = vm.dimName;
                    }
                  }
                  console.log("dim1");
                  console.log(dim1);
                  dim1.value = vm.figures[0][i];
                  i = i + 1;
                });

                console.log("vm.supplements");
                console.log(vm.supplements);
                SupplementsService.createList(vm.supplements, onSaveSuccess, onSaveError);
              }
            }
          } else {
            console.log("one d extras");
            console.log("vm.entity");
            console.log(vm.entity);
            console.log("vm.supplements");
            console.log(vm.supplements);
            if (vm.entity.id == null) {
              vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
              SupplementsService.create(vm.entity, onSaveSuccess, onSaveError);
            } else {
              vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
              SupplementsService.update(vm.entity, onSaveSuccess, onSaveError);
            }
          }
        } else {
          console.log("vm.entity");
          console.log(vm.entity);
          vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
          if (vm.entity.id == null) {
            vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
            SupplementsService.create(vm.entity, onSaveSuccess, onSaveError);
          } else {
            vm.entity.dimensionType1 == -1 ? vm.entity.dimensionType1 = null : null;
            SupplementsService.update(vm.entity, onSaveSuccess, onSaveError);
          }
        }
        //
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }


    function initDimensionExistences(dimensionId) {
      console.log(dimensionId);
      if (vm.dimensionSet !== undefined) {
        angular.forEach(vm.dimensionSet, function (dimension) {
          if (dimensionId == dimension.dimensionId) {
            vm.existencies = dimension.existenceDTOList;
            vm.existenceLabel = dimension.dimensionName;
          }
          if (dimension.type !== "SUPPLEMENT" && dimension.type !== "EXCLUSIVE") {
            vm.generalDimensions.push(dimension);
            console.log("vm.generalDimensions");
            console.log(vm.generalDimensions);
          }
        })
      }
      console.log($stateParams.setId);
      console.log($stateParams.year);
      console.log($stateParams.seasonGroupId);
      if ($stateParams.setId !== null && $stateParams.setId !== undefined) {
        console.log($stateParams.setId);
        console.log($stateParams.setId);
        vm.contractParameters = [];
        vm.setDimensionsWithExistences = [];
        vm.basicDimensionCount = null;
        console.log('changed dim set to : ' + $stateParams.setId);
        console.log('year' + $stateParams.year);
        console.log('seasongrp' + $stateParams.seasonGroupId);


        //get the set dimensions with their existences in this set 
        DimensionSetsService.getSetDimensionExistenciesBySetId({
          id: $stateParams.setId,
          year: $stateParams.year,
          seasonGroupId: $stateParams.seasonGroupId
        }, function (data) {
          vm.setDimensionsWithExistences = data;

          vm.basicDimensionCount = 0;
          //setBasicCount();

          console.log('vm.setDimensionsWithExistences');
          console.log(vm.setDimensionsWithExistences);


          var param = {
            "id": null,
            "exclusive": false,
            "contractId": null,
            "contractName": null,
            "notes": null,
            "parameter": null,
            "type": "DIMENSION",
            "uuid": null,
            "value": null
          };
        });
      }
    }

    function updateEntityName() {
      angular.forEach(vm.existencies, function (existence) {
        if (existence.id == vm.entity.existence1) {
          vm.entity.name = existence.name + " Supplement";
          if( vm.existenceLabel === "Special Day" ){
            console.log("SPECIAL Days.");
            vm.entity.description = existence.brief;
          }
        }
      });

    }
    function updateSpecialSeasonDates() {
      angular.forEach(vm.existencies, function (existence) {
        if (existence.id == vm.entity.existence1) {
          if( vm.existenceLabel === "Special Day" ){
            vm.entity.description = existence.brief;
          }
        }
      });

    }
    // function spliceAndFormatDates(){
    //   vm.startDate = vm.entity.description.splice(0,)
    // }
    function resolveDimenstionType(dimensionId) {
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimensionId == dimension.dimensionId) {
          vm.existencies = dimension.existenceDTOList;
          vm.entity.dimensionType1 = dimension.dimensionType;
        }
      });
      return vm.entity.dimensionType1;
    }

    function checkMandatory() {
      if (vm.entity.dimensionId == 'null') {
        vm.entity.dimensionId = -1;
      }
      console.log('vm.entity');
      console.log(vm.entity);

    }

    function initGeneralDimensionsService() {
      GeneralServicesDimensionsService.get().$promise.then(function (result) {

      })
    }

    function dimensionSetChange(id) {
      console.log("changed dims" + id);
      vm.dimensionExistencies = [];
      angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
        if (dimension.dimensionId === id) {
          vm.supplements = [];
          vm.dimensionExistencies = dimension.existenceDTOList;
          console.log("vm.dimensionExistencies");
          console.log(vm.dimensionExistencies);
          console.log("vm.existenceLabel");
          console.log(vm.existenceLabel);
          console.log($stateParams);

          console.log($stateParams.dimensionType !== "-1");
          angular.forEach(vm.dimensionExistencies, function (existence) {
            vm.entity.dimensionId == -1 ? vm.existenceName = vm.entity.name : vm.existenceName == existence.name;
            if ($stateParams.dimensionType !== "-1") {
              var temp = {
                billingType: vm.entity.billingType,
                contentStatus: "DRAFT",
                contractId: $stateParams.contractId,
                currencyId: $stateParams.currencyId,
                description: null,
                dimensionCount: 1 + 1,
                dimensionType1: $stateParams.dimensionType,
                existence1: vm.entity.existence1,
                dimensionType2: existence.dimensionType,
                existence2: existence.id,
                id: null,
                mandatory: vm.entity.mandatory,
                name: existence.name,
                organisationId: $stateParams.organisationId,
                supplementType: "SUPPLEMENT",
                unit: vm.entity.unit,
                uuid: null,
                value: null,
                dimensionId: $stateParams.dimensionId,
                contractVersionId: $stateParams.versionId

              };
              vm.supplements.push(temp);
              console.log("vm.supplements");
              console.log(vm.supplements);
            } else {
              console.log("extras")
              var temp = {
                billingType: vm.entity.billingType,
                contentStatus: "DRAFT",
                contractId: $stateParams.contractId,
                currencyId: $stateParams.currencyId,
                description: null,
                dimensionCount: 1 + 1,
                dimensionType1: null,
                existence1: null,
                dimensionType2: existence.dimensionType,
                existence2: existence.id,
                id: null,
                mandatory: vm.entity.mandatory,
                name: vm.entity.name,
                organisationId: $stateParams.organisationId,
                supplementType: "SUPPLEMENT",
                unit: vm.entity.unit,
                uuid: null,
                value: null,
                dimensionId: $stateParams.dimensionId,
                contractVersionId: $stateParams.versionId

              };
              vm.supplements.push(temp);
              console.log("vm.supplements");
              console.log(vm.supplements);
            }

          })
        }
      });
      vm.multiDimensinal = true;
      vm.inner = vm.value;
      vm.figures = [];
      vm.figures.push(vm.inner);

    }


    function dimensionSetChange2(id) {
      vm.dimensionSet2 = true;
      console.log("dimension set w changed");
      console.log("changed dims" + id);
      vm.dimensionExistencies = [];
      angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
        if (dimension.dimensionId === id) {
          vm.supplements2 = [];
          vm.dimensionExistencies = dimension.existenceDTOList;
          vm.inner = vm.value;
          vm.figures = [];

          vm.figures.push(
            _.chunk(
              vm.inner,
              dimension.existenceDTOList.length
            )
          );
          console.log("vm.figures");
          console.log(vm.figures);
          console.log("vm.dimensionExistencies");
          console.log(vm.dimensionExistencies);
          console.log("vm.existenceLabel");
          console.log(vm.existenceLabel);
          console.log($stateParams.dimensionType);

          console.log($stateParams.dimensionType !== "-1");
          angular.forEach(vm.dimensionExistencies, function (existence) {
            vm.entity.dimensionId == -1 ? vm.existenceName = vm.entity.name : vm.existenceName == existence.name;
            if ($stateParams.dimensionType !== "-1") {
              var temp = {
                billingType: vm.entity.billingType,
                contentStatus: "DRAFT",
                contractId: $stateParams.contractId,
                currencyId: $stateParams.currencyId,
                description: null,
                dimensionCount: 1 + 1,
                dimensionType1: $stateParams.dimensionType,
                existence1: vm.entity.existence1,
                dimensionType2: existence.dimensionType,
                existence2: existence.id,
                id: null,
                mandatory: vm.entity.mandatory,
                name: existence.name,
                organisationId: $stateParams.organisationId,
                supplementType: "SUPPLEMENT",
                unit: vm.entity.unit,
                uuid: null,
                value: null,
                dimensionId: $stateParams.dimensionId,
                contractVersionId: $stateParams.versionId

              };
              vm.supplements2.push(temp);
              console.log("vm.supplements");
              console.log(vm.supplements);
            } else {
              console.log("extras")
              var temp = {
                billingType: vm.entity.billingType,
                contentStatus: "DRAFT",
                contractId: $stateParams.contractId,
                currencyId: $stateParams.currencyId,
                description: null,
                dimensionCount: 1 + 1,
                dimensionType1: null,
                existence1: null,
                dimensionType2: existence.dimensionType,
                existence2: existence.id,
                id: null,
                mandatory: vm.entity.mandatory,
                name: vm.entity.name,
                organisationId: $stateParams.organisationId,
                supplementType: "SUPPLEMENT",
                unit: vm.entity.unit,
                uuid: null,
                value: null,
                dimensionId: $stateParams.dimensionId,
                contractVersionId: $stateParams.versionId

              };
              vm.supplements2.push(temp);
              console.log("vm.supplements");
              console.log(vm.supplements);
            }

          })
        }
      });
      vm.multiDimensinal = true;
    }
  }
})();
