(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ContractsOwnController', ContractsOwnController);

    ContractsOwnController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        'URLS',
        'CostsService',
        'services',
        'ContractsService',
        'ParseLinks',
        'pagingParams',
        'paginationConstants',
        '$stateParams',
        'OrganizationsService'
    ];

    function ContractsOwnController(
        $scope,
        $location,
        $state,
        $localStorage,
        URLS,
        CostsService,
        services,
        ContractsService,
        ParseLinks,
        pagingParams,
        paginationConstants,
        $stateParams,
        OrganizationsService
    ) {
        var vm = this;

        //injected values
        vm.services = services;

        //declare functions
        vm.loadPage = loadPage;
        vm.transition = transition;
        vm.search = search;
        vm.shortenName =shortenName;

        //our initialization
        vm.account = null;
        vm.isAuthenticated = null;
        vm.years = [];
        vm.suppliers = [];
        vm.entity = {};
        vm.entity.service = '';
        vm.entity.supplier = '';
        vm.entity.year = '';
        vm.entity.name = '';
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.name = '';
        vm.greeting = '';
        vm.isLoading = false;

        //pagination init
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.itemsPerPage = paginationConstants.itemsPerPage;

        //call functions needed to initialize data
        initYears(); //initialize years
        initSupplier(); //check if supplier id is set on state
        vm.search(); //load contracts

        function search(source) {
            if (source == 'service') {
                //the change from service, we need to reset the supplier
                vm.entity.supplier = '';
                vm.suppliers = [];
            }

            //get suppliers if the service (id) is set, will have a positive value -
            if (vm.entity.service > 0) {
                ContractsService.getSupplier(
                    {
                        orgId: $localStorage.current_organisation.id,
                        genSvcId: vm.entity.service
                    },
                    function(data) {
                        vm.suppliers = data;
                    }
                );
            } else {
            }

            vm.isLoading = true;
            vm.contracts = []; //clear the current view
            ContractsService.getFiltered(
                {
                    target: $localStorage.current_organisation.id,
                    service: vm.entity.service,
                    supplier: vm.entity.supplier,
                    issuer: null,
                    year: vm.entity.year,
                    name: vm.entity.name,
                    page: pagingParams.page - 1,
                    size: vm.itemsPerPage,
                    sort: 'id,desc'
                },
                function(data, headers) {
                    vm.links = ParseLinks.parse(headers('link'));
                    vm.totalItems = headers('X-Total-Count');
                    vm.queryCount = vm.totalItems;
                    vm.contracts = data;
                    vm.page = pagingParams.page;
                    vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
                    vm.isLoading = false;

                    vm.noRecordsFound =
                        vm.contracts === undefined || vm.contracts.length === 0;
                },
                function(error) {
                    console.log('error getting contracts');
                    console.log(error);
                    vm.isLoading = false;
                }
            );
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function initYears() {
            var currentYear = new Date().getFullYear();
            var totalYears = 10; //number of years visible
            var numberOfYearsBack = 3; //how many years back
            for (var i = 1; i <= totalYears; i++) {
                var year = currentYear + i - numberOfYearsBack;
                vm.years.push(year);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        function initSupplier() {
            if ($stateParams.supplierId !== undefined) {
                vm.entity.supplier = $stateParams.supplierId;

                OrganizationsService.query({}, function(data) {
                    vm.suppliers = data;
                });
            }
        }

        function shortenName(name){
            return name.substr(0, (name.indexOf("rates")+ 5));
        }
    }
})();
