(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('VehicleTypesDialogController', VehicleTypesDialogController);

  VehicleTypesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'VehicleTypesService', 'GeneralVehicleTypesService'];

  function VehicleTypesDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, VehicleTypesService, GeneralVehicleTypesService) {
    var vm = this;

    vm.vehicleTypes = entity;
    vm.clear = clear;
    vm.save = save;
    vm.selectedGeneralType = selectedGeneralType;

    vm.subCategories = GeneralVehicleTypesService.query();

    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.vehicleTypes.id !== null) {
        VehicleTypesService.update(vm.vehicleTypes, onSaveSuccess, onSaveError);
      } else {
        VehicleTypesService.create(vm.vehicleTypes, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function selectedGeneralType(generalTypeId) {
      angular.forEach(vm.subCategories, function(subCategory) {
        if (subCategory.id == generalTypeId) {
          vm.vehicleTypes.name = subCategory.name;
          vm.vehicleTypes.abbreviation = subCategory.abbreviation;
          vm.vehicleTypes.order = subCategory.order;
          vm.vehicleTypes.description = subCategory.description;
          vm.vehicleTypes.capacity = subCategory.capacity;
          vm.vehicleTypes.extendedCapacity = subCategory.extendedCapacity;

        }
      });
    }

  }
})();
