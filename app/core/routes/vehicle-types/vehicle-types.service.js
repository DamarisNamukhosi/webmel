(function() {
  'use strict';
  angular
    .module('webApp')
    .factory('VehicleTypesService', VehicleTypesService);

  VehicleTypesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function VehicleTypesService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'getVehicleTypes': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types'
      },
      'getVehicleTypesByOrganizationId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types/filter-by-organisation/:id?sort=order%2Casc'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        // isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types/:id'
      },
      'updateList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicle-types/save-list'
      },
      'uploadProfilePhoto': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
      }
    });
  }
})();
