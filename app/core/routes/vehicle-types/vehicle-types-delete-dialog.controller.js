(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleTypesDeleteDialogController',VehicleTypesDeleteDialogController);

    VehicleTypesDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'VehicleTypesService'];

    function VehicleTypesDeleteDialogController($uibModalInstance, entity, VehicleTypesService) {
        var vm = this;

        vm.vehicleTypes = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (vehicleTypes) {
            console.log('changing vehicleTypes contentStatus to DELETED');
            vm.vehicleTypes.contentStatus = "DELETED";
            VehicleTypesService.update(vehicleTypes,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
