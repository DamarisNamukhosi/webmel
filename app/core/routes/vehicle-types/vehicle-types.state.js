(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicle-types", {
        parent: "app",
        url: "/vehicle-types",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types.html",
            controller: "VehicleTypesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicleTypes: [
            "$stateParams",
            "$localStorage",
            "VehicleTypesService",
            function($stateParams,$localStorage, VehicleTypesService) {
              return VehicleTypesService.getVehicleTypesByOrganizationId({id : $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicle-types.new', {
        parent: 'vehicle-types',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/vehicle-types/vehicle-types-dialog.html',
                controller: 'VehicleTypesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function() {
                    return {
                      name: null,
                      abbreviation: null,
                      description: null,
                      capacity: null,
                      extendedCapacity: null,
                      contentStatus: "DRAFT",
                      generalTransportSubCategoryId: null,
                      generalTransportSubCategoryName: null,
                      id: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName:  $localStorage.current_organisation.name,
                      statusReason: null,
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go('vehicle-types', null, {
                    reload: 'vehicle-types'
                  });
                },
                function() {
                  $state.go('vehicle-types');
                }
              );
          }
        ]
      })
      .state("vehicle-types-detail", {
        parent: "vehicle-types",
        url: "/{id}/details",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle-types.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types-detail.html",
            controller: "VehicleTypesDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleTypesService",
            function($stateParams, VehicleTypesService) {
              return VehicleTypesService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-types-detail.edit", {
        parent: "vehicle-types-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-dialog.html",
                controller: "VehicleTypesDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-detail", null, {
                    reload: "vehicle-types-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own", {
        parent: "vehicle-types",
        url: "/own",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types-own.html",
            controller: "VehicleTypesOwnController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicleTypes: [
            "$stateParams",
            "$localStorage",
            "VehicleTypesService",
            function($stateParams, $localStorage, VehicleTypesService) {
              return VehicleTypesService.getVehicleTypesByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("vehicle-types-own.new", {
        parent: "vehicle-types-own",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER",
            "PROFESSIONAL_USER",
            "T_OPERATOR_USER",
            "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-dialog.html",
                controller: "VehicleTypesDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      registration: null,
                      brief: null,
                      description: null,
                      notes: null,
                      capacity: null,
                      profileStatus: "INACTIVE",
                      contentStatus: "DRAFT",
                      statusReason: "Created",
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      generalTransportSubCategoryId: null,
                      generalTransportSubCategoryName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own", null, {
                    reload: "vehicle-types-own"
                  });
                },
                function() {
                  $state.go("vehicle-types-own");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own-detail", {
        parent: "vehicle-types-own",
        url: "/{id}/vehicle",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle-types.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types-detail.html",
            controller: "VehicleTypesDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleTypesService",
            function($stateParams, VehicleTypesService) {
              return VehicleTypesService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "vehicle-types-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-types.edit", {
        parent: "vehicle-types",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-dialog.html",
                controller: "VehicleTypesDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types", null, {
                    reload: "vehicle-types"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own.edit", {
        parent: "vehicle-types-own",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-dialog.html",
                controller: "VehicleTypesDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own", null, {
                    reload: "vehicle-types-own"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types.delete", {
        parent: "vehicle-types",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-delete-dialog.html",
                controller: "VehicleTypesDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types", null, {
                    reload: "vehicle-types"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own.delete", {
        parent: "vehicle-types-own-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/vehicle-types-delete-dialog.html",
                controller: "VehicleTypesDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types.profile", {
        parent: "vehicle-types-own-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-types/image-upload/image-upload-dialog.html",
                controller: "VehicleTypesImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    "$stateParams",
                    "VehicleTypesService",
                    function($stateParams, VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-galleries", {
        parent: "app",
        url: "/vehicle-types/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types-galleries.html",
            controller: "VehicleTypesGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleTypesService",
            function($stateParams, VehicleTypesService) {
              return VehicleTypesService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-types-galleries.new", {
        parent: "vehicle-types-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-galleries", null, {
                    reload: "vehicle-types-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-galleries.delete", {
        parent: "vehicle-types-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-galleries", null, {
                    reload: "vehicle-types-galleries"
                  });
                },
                function() {
                  $state.go("vehicle-types-galleries");
                }
              );
          }
        ]
      })
      .state("vehicle-types-album-detail", {
        parent: "vehicle-types-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-types/vehicle-types-album-detail.html",
            controller: "VehicleTypesAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleTypesService",
            function($stateParams, VehicleTypesService) {
              return VehicleTypesService.get({
                id: $stateParams.id
              }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "vehicle-types-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-types-album-detail.edit", {
        parent: "vehicle-types-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "vehicle-types-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService", function(AlbumService) {
                    return AlbumService.get({
                      id: $stateParams.albumId
                    }).$promise;
                  }]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-album-detail", null, {
                    reload: "vehicle-types-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-types-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-types-album-detail.makeCoverImage", {
        parent: "vehicle-types-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({
                          id: $stateParams.albumId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-album-detail", null, {
                    reload: "vehicle-types-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-types-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-types-album-detail.deleteImage", {
        parent: "vehicle-types-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({
                          id: $stateParams.albumId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-album-detail", null, {
                    reload: "vehicle-types-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-types-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-types-album-detail.upload", {
        parent: "vehicle-types-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-album-detail", null, {
                    reload: "vehicle-types-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own-detail.addLabels", {
        parent: "vehicle-types-own-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          "$localStorage",
          function($stateParams, $state, $uibModal, $localStorage) {
            $uibModal
              .open({
                templateUrl: "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                          objectType: $stateParams.objectType
                        })
                        .$promise;
                    }
                  ],
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              ); //
          }
        ]
      })
      .state("vehicle-types-own-detail.deleteLabel", {
        parent: "vehicle-types-own-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-types-own-detail.addFeature", {
        parent: "vehicle-types-own-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/features/object-feature-create-dialog.html",
                controller: "ObjectFeatureCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  features: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "VehicleTypesService",
                    function(VehicleTypesService) {
                      return VehicleTypesService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("vehicle-types-own-detail");
                });
          }
        ]
      })
      .state("vehicle-types-own-detail.deleteFeature", {
        parent: "vehicle-types-own-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/features/object-feature-delete-dialog.html",
                controller: "ObjectFeatureDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-types-own-detail", null, {
                    reload: "vehicle-types-own-detail"
                  });
                },
                function() {
                  $state.go("vehicle-types-own-detail");
                }
              );
          }
        ]
      });
  }
})();
