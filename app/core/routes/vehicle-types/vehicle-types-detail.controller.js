(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("VehicleTypesDetailController", VehicleTypesDetailController);

  VehicleTypesDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function VehicleTypesDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    ObjectLabelsService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;
    vm.vehicleTypes = entity;
    console.log(vm.vehicleTypes);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.vehicleTypes.uuid;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({
      uuid: vm.vehicleTypes.uuid
    });
    console.log(vm.objectLabels);

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({
      uuid: vm.vehicleTypes.uuid
    });
    console.log("object features");
    console.log(vm.objectFeatures);
  }
})();
