(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleTypesController', VehicleTypesController);

    VehicleTypesController.$inject = ['$scope', '$location', '$state', 'vehicleTypes', 'URLS', 'VehicleTypesService'];

  function VehicleTypesController($scope, $location, $state, vehicleTypes, URLS, VehicleTypesService) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.profile_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound=true;
        vm.vehicleTypes = vehicleTypes;
        console.log('vehicleTypes....');
        console.log(vm.vehicleTypes);

        if (vm.vehicleTypes.length === 0){
            vm.noRecordsFound = false;
        }

        vm.sortableOptions = {
          stop: function (e, ui) {
            vm.dragAlert = false;
            updateRank();
          }
        }

        function updateRank() {
          console.log("Updating Rank");
          var count = 1;

          angular.forEach(vm.vehicleTypes, function (vehicleTypes) {

            vehicleTypes.order = count * 100;

            count = count + 1;
          });

          console.log("vm.vehicleTypes");
          console.log(vm.vehicleTypes);


          // call update
          VehicleTypesService.updateList(vm.vehicleTypes);

        }

    }
})();
