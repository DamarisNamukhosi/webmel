(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleTypesOwnController', VehicleTypesOwnController);

    VehicleTypesOwnController.$inject = ['$scope', '$location', '$state', 'vehicleTypes', 'URLS'];

    function VehicleTypesOwnController ($scope, $location, $state, vehicleTypes, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.profile_url = URLS.PROFILE_IMAGE_URL;

        vm.vehicleTypes = vehicleTypes;
        vm.noRecordsFound= false;
        if(vm.vehicleTypes.length === 0){
            vm.noRecordsFound = true;
        }
    }
})();
