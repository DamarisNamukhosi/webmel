(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('TransportTripsService', TransportTripsService);

    TransportTripsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function TransportTripsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getTransportTrip': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/transport-trips'
        },
        'getTransportTripsByOrganizationId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/transport-trips/filter-by-organisation/:id?sort=order%2Casc'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'contentservice/api/transport-trips/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/transport-trips'
        },
        'updateList': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/transport-trips/save-list'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/transport-trips'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/transport-trips/:id'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        }
      });
    }
})();
