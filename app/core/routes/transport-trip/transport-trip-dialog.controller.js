
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TransportTripDialogController', TransportTripDialogController);

  TransportTripDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TransportTripsService', 'GeneralTransportTripsService'];

  function TransportTripDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, TransportTripsService, GeneralTransportTripsService) {
    var vm = this;

    vm.transportTrip = entity;
    vm.clear = clear;
    vm.save = save;
    vm.generalTransportTrips = [];
    vm.selectedTripType = selectedTripType;
    initGeneralTransportTrip();




    function initGeneralTransportTrip() {
      GeneralTransportTripsService.query().$promise.then(function (response) {
        vm.generalTransportTrips = response;
      });
    }


    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.transportTrip.id !== null) {
        TransportTripsService.update(vm.transportTrip, onSaveSuccess, onSaveError);
      } else {
        TransportTripsService.create(vm.transportTrip, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function selectedTripType(generalTripId) {
      angular.forEach(vm.generalTransportTrips, function (generalTransportTrip) {
        if (generalTransportTrip.id === generalTripId) {
          vm.transportTrip.name = generalTransportTrip.name;
          vm.transportTrip.abbreviation = generalTransportTrip.abbreviation;
          vm.transportTrip.brief = generalTransportTrip.brief;
          vm.transportTrip.description = generalTransportTrip.description;
          vm.transportTrip.order = generalTransportTrip.order;
        }
      });
    }

  }
})();

