(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TransportTripsController', TransportTripsController);

  TransportTripsController.$inject = ['$scope', '$location', '$state', 'transportTrips', 'URLS', 'TransportTripsService'];

  function TransportTripsController($scope, $location, $state, transportTrips, URLS, TransportTripsService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_url = URLS.PROFILE_IMAGE_URL;
    vm.noRecordsFound = false;
    vm.transportTrips = transportTrips;
    console.log('transportTrips....');
    console.log(vm.transportTrips);

    if (vm.transportTrips.length === 0) {
      vm.noRecordsFound = true;
    }

    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.transportTrips, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.transportTrips");
      console.log(vm.transportTrips);


      // call update
      TransportTripsService.updateList(vm.transportTrips, onSaveSuccess, onSaveError);

    }
    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("Update successful");
    }

    function onSaveError() {
      console.log("Update not successfull");
    }
  }
})();
