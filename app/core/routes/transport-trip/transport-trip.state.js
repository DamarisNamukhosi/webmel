(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicles-transport-trip", {
        parent: "app",
        url: "/transport-trip",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/transport-trip/transport-trip.html",
            controller: "TransportTripsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          transportTrips: [
            "$stateParams",
            "$localStorage",
            "TransportTripsService",
            function ($stateParams,$localStorage, TransportTripsService) {
              return TransportTripsService.getTransportTripsByOrganizationId({id: $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicles-transport-trip.new', {
        parent: 'vehicles-transport-trip',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/transport-trip/transport-trip-dialog.html',
                controller: 'TransportTripDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: "DRAFT",
                      description: null,
                      generalTransportTripId: null,
                      generalTransportTripName: null,
                      id: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      statusReason: null,
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('vehicles-transport-trip', null, {
                    reload: 'vehicles-transport-trip'
                  });
                },
                function () {
                  $state.go('vehicles-transport-trip');
                }
              );
          }
        ]
      })
      .state("vehicles-transport-trip.edit", {
        parent: "vehicles-transport-trip",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/transport-trip/transport-trip-dialog.html",
                controller: "TransportTripDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "TransportTripsService",
                    function (TransportTripsService) {
                      return TransportTripsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-transport-trip", null, {
                    reload: "vehicles-transport-trip"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-transport-trip.delete", {
        parent: "vehicles-transport-trip",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/transport-trip/transport-trip-delete-dialog.html",
                controller: "TransportTripDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "TransportTripsService",
                    function (TransportTripsService) {
                      return TransportTripsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-transport-trip", null, { reload: "vehicles-transport-trip" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      });

  }
})();
