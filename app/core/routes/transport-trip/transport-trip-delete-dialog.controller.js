(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TransportTripDeleteDialogController', TransportTripDeleteDialogController);

  TransportTripDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'TransportTripsService'];

  function TransportTripDeleteDialogController($uibModalInstance, entity, TransportTripsService) {
    var vm = this;

    vm.transportTrip = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(transportTrip) {
      console.log('changing transport trip contentStatus to DELETED');
      vm.transportTrip.contentStatus = "DELETED";
      TransportTripsService.update(transportTrip,
        function () {
          $uibModalInstance.close(true);
        });
    }
  }
})();
