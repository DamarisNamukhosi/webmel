(function () {
  'use strict';

  angular.module('webApp').controller('DashboardCompactViewController', DashboardCompactViewController);

  DashboardCompactViewController.$inject = [
    '$localStorage',
    '$state',
    '$scope',
    '$window',
    '$stateParams',
    'AccountService',
    'HomeService',
    'OrganizationsService',
    'NavbarService',
    'ProfessionalsService',
    'ContractsService',
    'VehiclesService',
    'MarketsService',
    'GeneralServicesService',
    'URLS',
    'LocationsService',
    'NgMap',
    '$timeout',
    'RatesService',
    'CurrenciesService',
    'TextUtil'
  ];

  function DashboardCompactViewController(
    $localStorage,
    $state,
    $scope,
    $window,
    $stateParams,
    AccountService,
    HomeService,
    OrganizationsService,
    NavbarService,
    ProfessionalsService,
    ContractsService,
    VehiclesService,
    MarketsService,
    GeneralServicesService,
    URLS,
    LocationsService,
    NgMap,
    $timeout,
    RatesService,
    CurrenciesService,
    TextUtil
  ) {
    //injected values
    var vm = this;

    console.log("dashboard controller");

    // function declaration
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;

    vm.showCompressedView = showCompressedView;
    vm.searching = searching;
    vm.sort = sort;
    vm.shortenName = shortenName;
    vm.getRoomCategoryExistencies = getRoomCategoryExistencies;
    vm.getServicePlanExistencies = getServicePlanExistencies;
    //vm.filterCurrency = filterCurrency;
    vm.changeItemsPerPage = changeItemsPerPage;
    //my initialization 
    vm.locationId = $stateParams.locationId;
    vm.resultsPresent = false;
    vm.isLoading = false;
    vm.openedFrom = false;
    vm.openedTo = false;
    vm.disableCompressedView = false;
    vm.rowCollection = [];
    vm.rowCollectionBase = [];
    vm.itemsPerPage = 10;
    vm.itemsPerPageStr = "10";
    vm.servicePlanExistencies = [];
    vm.roomCategoryExistencies = [];
  
    vm.dateOptions = {
      startDate: moment().subtract(7, 'days'),
      endDate: moment(),
      maxDate: new Date(2037, 5, 22),
      startingDay: 1
    };
    vm.filterObjects = ["Wifi", "Start", "Offers"]
    vm.zoom = 10;
    vm.zoomLevel = 8;
    vm.mapView = false;
    vm.date = new Date();
    //vm.dateOfVisit.setFullYear(vm.dateOfVisit.getFullYear() + 1);
    vm.searchedItems = [];
    vm.currencies = [];
    vm.request = {
      "date": null,
      "generalServiceId": 1,
      "locationId": null,
      "targetId": 1
    };
    vm.filteredOrgs = [];
    vm.visitLocation = null;
    vm.searchedProperty = false; //check if Visited location is a property.
    vm.organizationsRates = [];




    search();
    initCurrencies();

    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {
      console.log("selecteditem");
      console.log($item.id);
      vm.visitLocation = $item;
      console.log("vm.visitLocation");
      console.log(vm.visitLocation);
      vm.request.locationId = $item.id;
      // vm.request.typeId=$item.typeId,
      // vm.request.typeName=$item.typeName,
      console.log($item.name);
      console.log(vm.request);
      $localStorage.current_location_id = vm.request.locationId;
      refresh($item);
    }

    vm.openFrom = function () {
      vm.openedFrom = true;
    };

    vm.openTo = function () {
      vm.openedTo = true;
    };

    function search() {

      vm.rowCollection = [];
      vm.rowCollectionBase = [];
      if (vm.date !== undefined && vm.date !== '') {

        vm.request.date = moment(formatDate(vm.date), 'YYYY-MM-DDTHH:mm:ss.SSS').format('YYYY-MM-DD');

        if (vm.request.date == 'Invalid date') {
          vm.request.date = null;
        }
      }
      console.log(vm.request);
      //RatesService.compressedRates(vm.request, onSaveSuccess, onSaveError)
      console.log("vm.request");
      console.log(vm.request);
      RatesService.compressedRates(vm.request, onSaveSuccess, onSaveError)
      vm.resultsPresent = true;
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      vm.response = result;
      vm.organizationsRates = [];
      angular.forEach(vm.response, function (response) {
        if (response.contractId !== null) {
          vm.organizationsRates.push(response);
        }
      });

      vm.rowCollection = _.orderBy(vm.organizationsRates, [function (org) {
        return org.organisationName.toLowerCase();
      }], ['asc']);
    }

    function onSaveError() {
      vm.isSaving = false;
    }

 
    function initCurrencies() {
      CurrenciesService.query().$promise.then(function (response) {
        angular.forEach(response, function (res) {
          if (res.id == 204 || res.id == 211 || res.id == 137 || res.id == 140 | res.id == 151) {
            vm.currencies.push(res);
          }
        });

      });
    }


    function formatDate(startDate) {
      if (startDate !== null && startDate !== 'Invalid date') {
        console.log("startDate");
        console.log(startDate);
        var theDate = new Date(startDate.toString());
        return (
          theDate.getFullYear() +
          '-' +
          (theDate.getMonth() + 1) +
          '-' +
          theDate.getDate() +
          ' ' +
          theDate.getHours() +
          ':' +
          theDate.getMinutes() +
          ':' +
          theDate.getSeconds() +
          '.' +
          theDate.getMilliseconds()
        );
      } else {
        return '';
      }
    }

    function refresh(location) {
      console.log("refreshiing...");
      console.log(location);
      vm.isLoading = true;
      if (location.typeId == 4 || location.typeId == 5 || location.typeId == 6 || location.typeId == 7 || location.typeId == 8) {
        console.log("locations");
        console.log(location.typeId);
        console.log(location.parentId);
        vm.request.locationId = location.id;
        LocationsService.getLocationsByParentIdAndType({
          id: location.id,
          type_id: 10
        }).$promise.then(function (response) {
          vm.organizations = response;
          //  this.splice(to, 0, this.splice(from, 1)[0]);
          vm.isLoading = false;
        });
      } else if (location.typeId == 10 || location.typeId == 11) {
        vm.searchedProperty = true;
        console.log("property");
        console.log(location.typeId);
        console.log(location.parentId);
        vm.request.locationId = location.parentId;
        LocationsService.getLocationsByParentIdAndType({
          id: location.parentId,
          type_id: 10
        }).$promise.then(function (response) {
          vm.organizations = response;
          //  this.splice(to, 0, this.splice(from, 1)[0]);

          vm.isLoading = false;
        });
        angular.forEach(vm.searchedItems, function (item) {
          if (item.id == vm.request.locationId) {
            vm.visitLocation = item;
          }
        });
      }

    }

    function shortenName(name) {
      return TextUtil.shorten(name, 65);
    }

  

    function sort(keyname) {
      $scope.sortKey = keyname; //set the sortKey to the param passed
      $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    function showCompressedView() {
      console.log("query compressed view");
      $state.go("dashboard-compressed", {
        locationId: vm.request.locationId,
        date: vm.request.date,
        targetId: vm.request.targetId
      })
    }

    function searching() {
      console.log("searching");
      search();
    }


    function getRoomCategoryExistencies(org) {
      if (org.usedExistences !== null) {
        if (org.usedExistences.length > 1) {
          angular.forEach(org.usedExistences, function (exit) {
            if (exit.dimensionType == "ROOM_CATEGORY") {
              vm.roomCategoryExistencies.push(exit);
            }
          });
        } else {
          if (org.usedExistences[0].dimensionType == "ROOM_CATEGORY") {
            vm.roomCategoryExistencies = org.usedExistences[0];
          }
          vm.roomCategoryExistencies = org.usedExistences;
        }
        return vm.roomCategoryExistencies;

      }

    }

    function getServicePlanExistencies(org) {
      console.log("org");
      console.log(org);
      if (org.usedExistences !== null) {
        if (org.usedExistences.length > 1) {
          angular.forEach(org.usedExistences, function (exit) {
            if (exit.dimensionType == "MEAL_PLAN") {
              vm.servicePlanExistencies.push(exit);
            }
          });
        } else {
          if (org.usedExistences[0].dimensionType == "MEAL_PLAN") {
            vm.servicePlanExistencies = org.usedExistences[0];
          }
        }
        return vm.servicePlanExistencies;

      }

    }


    //utuility function to change the items per page 

    function changeItemsPerPage(strValue) {

      console.log("converting to int: " + strValue);
      vm.itemsPerPage = parseInt(strValue, 10);
    }
  }
})();
