(function () {
    'use strict';

    angular.module('webApp').controller('DashboardController', DashboardController);

    DashboardController.$inject = [
        '$localStorage',
        '$state',
        '$scope',
        '$window',
        'AccountService',
        'HomeService',
        'OrganizationsService',
        'NavbarService',
        'ProfessionalsService',
        'ContractsService',
        'VehiclesService',
        'MarketsService',
        'GeneralServicesService',
        'URLS',
        'LocationsService',
        'NgMap',
        '$timeout',
        'RatesService',
    ];

    function DashboardController(
        $localStorage,
        $state,
        $scope,
        $window,
        AccountService,
        HomeService,
        OrganizationsService,
        NavbarService,
        ProfessionalsService,
        ContractsService,
        VehiclesService,
        MarketsService,
        GeneralServicesService,
        URLS,
        LocationsService,
        NgMap,
        $timeout,
        RatesService
    ) {
        //injected values
        var vm = this;

        console.log("dashboard controller");

        // function declaration
        vm.getLocation=getLocation;
        vm.onLocationSelected= onLocationSelected;
        vm.search= search;
        vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
        vm.setCurrentLocation=setCurrentLocation;
        vm.showMapping=showMapping;
        vm.switching = switching;
        vm.showCompressedView= showCompressedView;
        vm.shortenName=shortenName;
        //my initialization 
        vm.locationId=null;
        vm.resultsPresent= false;
        vm.isLoading = false;
        vm.startDate = new Date();
        vm.startDate.setDate(vm.startDate.getDay() - 7);
        vm.endDate = new Date();
        vm.startDate = vm.startDate;
        vm.endDate = vm.endDate;
        vm.openedFrom = false;
        vm.openedTo = false;
        vm.disableCompressedView = false;
        vm.dateOptions = {
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            maxDate: new Date(2037, 5, 22),
            startingDay: 1
        };
        vm.filterObjects=["Wifi", "Start", "Offers"]
        vm.zoom = 10;
        vm.zoomLevel = 8;
        vm.mapView = false;
        vm.dateOfVisit = new Date();
        //vm.dateOfVisit.setFullYear(vm.dateOfVisit.getFullYear() + 1);
        vm.searchedItems = [];
        vm.request={
          "locationId": null,
          "dateOfVisit": new Date(),
          "adults": "1",
          "children": "0",
          "targetId":$localStorage.current_organisation.id,
          "organisationTypes":[4,5],
          "setExistenceDTOList":[],
          "typeId": null,
          "typeName": null
        }
        vm.visitLocation=null;
        vm.searchedProperty = false; //check if Visited location is a property.
        function getLocation(val) {
          vm.searchedProperty = false;
            return LocationsService
              .query({
                name: val,
                types: [1,2,3,4,5,6,7,8,9,17,18]
              })
              .$promise
              .then(function (results) {
                vm.searchedItems = results;
                return results.map(function (item) {
                 
                  return item;
                })
              });
          }

          function onLocationSelected($item, $model) {
            console.log("selecteditem");
            console.log($item.id);
            vm.visitLocation=$item;
            console.log("vm.visitLocation");
            console.log(vm.visitLocation);
            vm.request.locationId=$item.id;
            vm.request.typeId=$item.typeId,
            vm.request.typeName=$item.typeName,
            console.log($item.name);
            $localStorage.current_location_id=vm.request.locationId;
            refresh($item);
          }
          
        vm.openFrom = function () {
            vm.openedFrom = true;
          };
      
          vm.openTo = function () {
            vm.openedTo = true;
          };

          function search(){
              console.log("ready to get contracts//");
              console.log(vm.request);
              if (vm.dateOfVisit !== undefined && vm.dateOfVisit !== '') {
                vm.request.dateOfVisit = moment(formatDate(vm.dateOfVisit), 'YYYY-MM-DDTHH:mm:ss.SSS').format('YYYY-MM-DD');
                if (vm.request.dateOfVisit == 'Invalid date') {
                    vm.request.dateOfVisit = null;
                }
            }
            console.log(vm.request);
              //RatesService.compressedRates(vm.request, onSaveSuccess, onSaveError)
              RatesService.compareRates(vm.request, onSaveSuccess, onSaveError)
              vm.resultsPresent= true;
          }

          function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            vm.response=result;
            vm.organizationsRates=result;
            console.log(vm.locationName);
            console.log(vm.organizationsRates);
            angular.forEach(vm.organizationsRates, function(org){
              console.log(org.organisationDTO != null);
              if(org.organisationDTO != null){
                vm.organizationsRates.splice(0, 1);
              
            }
            });
            //$state.go("dashboard-compare", {reload:true});
            console.log("vm.organizationsRates");
            //console.log(vm.organizationsRates);
            // angular.forEach(vm.response,function(response){

            // } );
          }
      
          function onSaveError() {
            vm.isSaving = false;
          }
          
          function switching(org){
            console.log("switching to contract");
            console.log(org);
            //http://localhost:9000/#/contracts/20/408/1251/overview/user//2019
            $scope.url = 'http://localhost:9000/#/contracts/'+org.organisation.id+'/'+org.contractDTO.id+'/'+org.contractDTO.dimensionSetId+'/overview/user'+'/'+'/'+org.contractDTO.year;

            //$window.open('https://www.google.com', '_blank');
            //$window.open('http://localhost:9000/#/contracts/org.organisation.id/org.contractDTO.id/org.contractDTO.dimensionSetId/overview/user/org.contractDTO.seasonGroupId/org.contractDTO.year', '_blank');
            //supplierId: org.organisation.id,contractId: org.contractDTO.id,setId: org.contractDTO.dimensionSetId,seasonGroupId: org.contractDTO.seasonGroupId, year:org.contractDTO.year
            //http://localhost:9000/#/contracts/60/31/116/overview/user//2019

          }
        function formatDate(startDate) {
          if (startDate !== null && startDate !== 'Invalid date') {
              console.log("startDate");
              console.log(startDate);
              var theDate = new Date(startDate.toString());
              return (
                  theDate.getFullYear() +
                  '-' +
                  (theDate.getMonth() + 1) +
                  '-' +
                  theDate.getDate() +
                  ' ' +
                  theDate.getHours() +
                  ':' +
                  theDate.getMinutes() +
                  ':' +
                  theDate.getSeconds() +
                  '.' +
                  theDate.getMilliseconds()
              );
          } else {
              return '';
          }
      }
          function refresh(location){
            console.log("refreshiing...");
            console.log(location);
            vm.isLoading = true;
            if(location.typeId == 4 || location.typeId == 5 || location.typeId == 6 || location.typeId == 7 || location.typeId == 8){
              console.log("locations");
              console.log(location.typeId);
            console.log(location.parentId);
            vm.request.locationId = location.id;
              LocationsService.getLocationsByParentIdAndType({
                id:  location.id,
                type_id: 10
              }).$promise.then(function (response) {
                vm.organizations = response;
                //  this.splice(to, 0, this.splice(from, 1)[0]);
                vm.isLoading = false;
              });
          }else if( location.typeId == 10 || location.typeId == 11){
            vm.searchedProperty =true;
            console.log("property");
            console.log(location.typeId);
            console.log(location.parentId);
            vm.request.locationId = location.parentId;
            LocationsService.getLocationsByParentIdAndType({
              id:  location.parentId,
              type_id: 10
            }).$promise.then(function (response) {
              vm.organizations = response;
              //  this.splice(to, 0, this.splice(from, 1)[0]);

              vm.isLoading = false;
            });
            angular.forEach(vm.searchedItems,function(item){
              if(item.id == vm.request.locationId){
                vm.visitLocation = item;
              }
            });
          }
      
          }

         
    function setCurrentLocation($event, id) {
      console.log("$event");
      console.log($event);
      console.log("id");
      console.log(id);
      console.log(id.$);
      vm.selectedCurrentLocation= id.$;
      NgMap.getMap("map").then(function(map) {
        console.log('map', map);
        vm.map = map;
        console.log("vm.map.markers");
        console.log(vm.map.markers);
          for (var key in vm.map.markers) {
            console.log(key)
              var mid = parseInt(key);
              console.log(mid)
              var m = vm.map.markers[key];
              console.log(m);
              console.log(id.$.id);
              if (m == id.$) {
                console.log("animate");
                m.setAnimation(google.maps.Animation.BOUNCE);
                
              }
              else {
                console.log("hhhh");
              }
          }


      });
      console.log("vm.map");
      console.log(vm.map);
      console.log(vm.response);
      // angular.forEach(vm.response, function (location) {
      //   if (location.id == id.$.id) {
      //     vm.currentLocation = location;
      //     console.log("vm.currentLocation");
      //     console.log(vm.currentLocation);
      //   }
      // });
    }

    function showMapping(){
      console.log("showMapping");
      console.log(vm.organizations);
      vm.mapView = !vm.mapView;
      if(vm.mapView){
        LocationsService.getFullLocation({id: vm.request.locationId}).$promise.then(function(response){
          console.log("response");
          console.log(response);
          vm.currentLocation =response;
        });
      }
    }
    vm.sort = function(keyname){
      $scope.sortKey = keyname;   //set the sortKey to the param passed
      $scope.reverse = !$scope.reverse; //if true make it false and vice versa
  }
        function showCompressedView(){
          console.log("query compressed view");
          $state.go("dashboard-compressed", {locationId: vm.request.locationId, dateOfVisit:vm.request.dateOfVisit, targetId:vm.request.targetId})
        }
        
        function shortenName(name){
          console.log(name != undefined);
          if(name != undefined){
          return name.substr(0, (name.indexOf("rates")+ 5));
        }else{
          return name
        }
      }
      
    }
})();
