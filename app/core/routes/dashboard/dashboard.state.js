(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('dashboard', {
                parent: 'app',
                url: '/dashboard',
                data: {
                    requiresAuthentication: false,
                    authorities: [],
                    pageTitle: 'My Organizations'
                },
                views: {
                    'content@': {
                        //templateUrl: 'core/routes/dashboard/dashboard-compact-view.html',
                        // templateUrl: 'core/routes/dashboard/dashboard.html',
                        // controller: 'DashboardController',
                        templateUrl: 'core/routes/dashboard/dashboard-compact-view.html',
                        controller: 'DashboardCompactViewController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {}
            })
            .state('dashboard-compare', {
                parent: 'dashboard',
                url: '/dashboard',
                data: {
                    requiresAuthentication: false,
                    authorities: [],
                    pageTitle: 'My Organizations'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/dashboard/dashboard.html',
                        controller: 'DashboardController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {}
            })
            .state('dashboard-compressed', {
                parent: 'dashboard',
                url: '/compressed/{targetId}/{locationId}/{dateOfVisit}/',
                data: {
                    requiresAuthentication: false,
                    authorities: [],
                    pageTitle: 'My Organizations'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/dashboard/dashboard-compact-view.html',
                        controller: 'DashboardCompactViewController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {}
            })
            .state('dashboard-details', {
                parent: 'dashboard',
                url: '/{orgId}',
                data: {
                    requiresAuthentication: false,
                    authorities: [],
                    pageTitle: 'My Organizations'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/dashboard/dashboard-organization-view.html',
                        controller: 'DashboardDetailsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {}
            })
            //
            ;
    }
})();
