(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FacilityDialogController', FacilityDialogController);

        FacilityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'FacilityService', 'FacilityTypesService'];

    function FacilityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, FacilityService, FacilityTypesService) {
        var vm = this;

        vm.facility = entity;
        vm.clear = clear;
        vm.save = save;

        vm.facilityTypes = FacilityTypesService.query();
        //vm.facility = FacilityService.getFacility();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.facility.id !== null) {
                FacilityService.update(vm.facility, onSaveSuccess, onSaveError);
            } else {
                FacilityService.create(vm.facility, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
