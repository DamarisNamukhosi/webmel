(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FacilityDeleteDialogController',FacilityDeleteDialogController);

        FacilityDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'FacilityService'];

    function FacilityDeleteDialogController($uibModalInstance, entity, FacilityService) {
        var vm = this;

        vm.facilty = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FacilityService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
