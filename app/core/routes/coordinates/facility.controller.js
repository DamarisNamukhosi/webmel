(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FacilityController', FacilityController);

        FacilityController.$inject = ['$scope', '$location', '$state', 'facilities'];

    function FacilityController($scope, $location, $state, facilities) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.facilities = facilities;

    }
})();
