(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FacilityDetailController', FacilityDetailController);

        FacilityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'FacilityService'];

    function FacilityDetailController($scope, $rootScope, $stateParams, previousState, entity, FacilityService) {
        var vm = this;
        
        vm.facility = entity;
        vm.previousState = previousState.name;
    }
})();
