(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('RoutesController', RoutesController);

  RoutesController.$inject = ['$scope', '$location', '$state', 'RoutesService', '$localStorage', 'URLS', 'ParseLinks', 'pagingParams', '$stateParams'];

  function RoutesController($scope, $location, $state, RoutesService, $localStorage, URLS, ParseLinks, pagingParams, $stateParams) {
    var vm = this;
    // ParseLinks,
    // pagingParams,
    // paginationConstants,
    vm.account = $localStorage.current_organisation;
    console.log($localStorage.current_organisation);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
    console.log(vm.profile_image_url);
    vm.isAuthenticated = null;
    vm.routeType = ['LOCATIONGROUP', 'LOCATION'];

    vm.itemsPerPage = 20;
    vm.itemsPerPageStr = "20";
    vm.records = [];
    vm.schudules = [];

    // vm.predicate = pagingParams.predicate;
    // vm.reverse = pagingParams.descending;
    // vm.loadPage = loadPage;
    // vm.transition = transition;
    vm.changeItemsPerPage = changeItemsPerPage;

    initRoutes();



    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };



    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.rowCollectionDisplayed, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.rowCollectionDisplayed");
      console.log(vm.rowCollectionDisplayed);


      // call update
      RoutesService.updateList(vm.rowCollectionDisplayed, onSaveSuccess, onSaveError);

    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("update sucessful");
    }

    function onSaveError() {
      console.log("update not sucessful");
    }
    function initRoutes() {
      //{id: $localStorage.current_organisation.id}
      RoutesService.getRoutes({
        id: $localStorage.current_organisation.id,
        page: pagingParams.page - 1,
        size: 1000
      },
        function (data, headers) {
          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.records = data;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.isLoading = false;

          vm.noRecordsFound =
            vm.routes === undefined || vm.routes.length === 0;
        },
        function (error) {
          console.log('error getting contracts');
          console.log(error);
          vm.isLoading = false;
        }

      );

    }

    //getSchedules
    // function loadPage(page) {
    //   vm.page = page;
    //   vm.transition();
    // }

    // function transition() {
    //   $state.transitionTo($state.$current, {
    //     page: vm.page,
    //     sort: vm.predicate + ',' + (vm.reverse ? 'desc' : 'asc'),
    //     search: vm.currentSearch
    //   });
    // }
    function changeItemsPerPage(strValue) {

      console.log("converting to int: " + strValue);
      vm.itemsPerPage = parseInt(strValue, 10);
    }
  }
})();
