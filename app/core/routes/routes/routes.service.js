(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('RoutesService', RoutesService);

        RoutesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function RoutesService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getRoutes': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            //url: URLS.BASE_URL + 'contentservice/api/routes'

            url: URLS.BASE_URL + 'contentservice/api/routes/filter-by-organisation/:id?sort=order%2Casc'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/routes/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/routes'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/routes'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/routes/:id'
          },
          'updateList': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/routes/save-list'
          },
          'getSchedules': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/transport-schedules/filter-by-organisation/:id?routeId=:routeId&sort=order%2Casc'
          },
        });
    }
})();
