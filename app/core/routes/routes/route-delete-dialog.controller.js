(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RouteDeleteDialogController',RouteDeleteDialogController);

        RouteDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'RoutesService'];

    function RouteDeleteDialogController($uibModalInstance, entity, RoutesService) {
        var vm = this;

        vm.route = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RoutesService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
