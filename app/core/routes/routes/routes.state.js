(function () {
  'use strict';

  angular
    .module('webApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider.state('routes', {
      parent: 'app',
      url: '/routes',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'routes'
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/routes/routes.html',
          controller: 'RoutesController',
          controllerAs: 'vm'
        }
      },
      params: {
        page: {
          value: '1',
          squash: true
        },
        sort: {
          value: 'id',
          squash: true
        },
        search: null
      },

      resolve: {
        pagingParams: [
          '$stateParams',
          'PaginationUtil',
          function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage(
                $stateParams.page
              ),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate(
                $stateParams.sort
              ),
              ascending: PaginationUtil.parseAscending(
                $stateParams.sort
              ),
              search: $stateParams.search
            };
          }
        ]
      }
    })
      .state('routes.new', {
        parent: 'routes',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/routes/routes-dialog.html',
            controller: 'RoutesDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
              entity: function () {
                return {
                  id: null,
                  uuid: null,
                  fromType: "LOCATION",
                  fromId: null,
                  fromName: null,
                  fromAbbreviation: null,
                  toType: "LOCATION",
                  toId: null,
                  toName: null,
                  toAbbreviation: null,
                  notes: null,
                  order: null,
                  contentStatus: "DRAFT",
                  statusReason: null,
                  createdOn: null,
                  createdBy: null,
                  lastUpdatedOn: null,
                  lastUpdatedBy: null,
                  organisationId: $localStorage.current_organisation.id,
                  organisationName: $localStorage.current_organisation.name
                };
              }
            }
          }).result.then(function () {
            $state.go('routes', null, { reload: 'routes' });
          }, function () {
            $state.go('routes');
          });
        }]
      })
      .state('routes.edit', {
        parent: 'routes',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/routes/routes-dialog.html',
            controller: 'RoutesDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['RoutesService', function (RoutesService) {
                return RoutesService.get({ id: $stateParams.id }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('routes', null, { reload: 'routes' });
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('routes.delete', {
        parent: 'routes',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/routes/route-delete-dialog.html',
            controller: 'RouteDeleteDialogController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['RoutesService', function (RoutesService) {
                return RoutesService.get({ id: $stateParams.id }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('routes', null, { reload: 'routes' });
          }, function () {
            $state.go('^');
          });
        }]
      });
  }
})();
