(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoutesDialogController', RoutesDialogController);

        RoutesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RoutesService','LocationsService'];

    function RoutesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, RoutesService,LocationsService) {
        var vm = this;

        vm.route = entity;
        vm.clear = clear;
        vm.save = save;
        vm.getFromLocation= getFromLocation;
        vm.getToLocation = getToLocation;
        vm.onFromLocationSelected = onFromLocationSelected;
        vm.onToLocationSelected = onToLocationSelected;

        console.log('meal-plans....');
        console.log(vm.route);
        //vm.Routes = RoutesService.getRoutes();
        //vm.generalRoutes = GeneralRoutesService.query();    

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.route.id !== null) {
                RoutesService.update(vm.route, onSaveSuccess, onSaveError);
            } else {
                RoutesService.create(vm.route, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function getFromLocation(val) {
            vm.searchedProperty = false;
              return LocationsService
                .query({
                  name: val,
                  types: [1,2,3,4,5,6,7,8,9,10,11,12,13,17,18,19]
                })
                .$promise
                .then(function (results) {
                  vm.searchedItems = results;
                  return results.map(function (item) {
                   
                    return item;
                  })
                });
            }
            function getToLocation(val) {
                vm.searchedProperty = false;
                  return LocationsService
                    .query({
                      name: val,
                      types: [1,2,3,4,5,6,7,8,9,10,11,12,13,17,18,19]
                    })
                    .$promise
                    .then(function (results) {
                      vm.searchedItems = results;
                      return results.map(function (item) {
                       
                        return item;
                      })
                    });
                }

        function onFromLocationSelected($item, $model) {
          
          vm.route.fromId=$item.id;
          vm.route.fromAbbreviation =  $item.abbreviation;
          
        }
        function onToLocationSelected($item, $model) {
            console.log($item);
            vm.route.toId=$item.id;
            vm.route.toAbbreviation =  $item.abbreviation;
            
          }


    }
})();
