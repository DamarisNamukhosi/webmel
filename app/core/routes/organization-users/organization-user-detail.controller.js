(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationUserDetailController', OrganizationUserDetailController);

        OrganizationUserDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'OrganizationUsersService'];

    function OrganizationUserDetailController($scope, $rootScope, $stateParams, previousState, entity, OrganizationUsersService) {
        var vm = this;
        
        vm.organizationUser = entity;
        vm.previousState = previousState.name;
    }
})();
