(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('organization-users', {
            parent: 'app',
            url: '/organization-users',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/organization-users/organization-users.html',
                    controller: 'OrganizationUsersController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
              users: ['$stateParams', '$localStorage', 'OrganizationUsersService', function ($stateParams, $localStorage, OrganizationUsersService) {
                return OrganizationUsersService.getOrganizationUsers({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('organization-users.new', {
            parent: 'organization-users',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'organization-users.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/organization-users/organization-user-dialog.html',
                    controller: 'OrganizationUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                emailAddress : null,
                                langKey : null,
                                fullname : null,
                                organisationId : null,
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('organization-users', null, { reload: 'organization-users' });
                }, function() {
                    $state.go('organization-users');
                });
            }]
        })

        .state('organization-user-detail', {
            parent: 'organization-users',
            url: '/organization-users/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Organization Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/organization-users/organization-user-detail.html',
                    controller: 'OrganizationUserDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'OrganizationsService', function($stateParams, OrganizationsService) {
                    return OrganizationsService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'organization-users',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('organization-users.edit', {
            parent: 'organizations',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/organization-users/organization-user-dialog.html',
                    controller: 'OrganizationUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['OrganizationUsersService', function(OrganizationUsersService) {
                            return OrganizationUsersService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('organization-users', null, { reload: 'organization-users' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
