(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationUsersController', OrganizationUsersController);

    OrganizationUsersController.$inject = ['$scope', '$location', '$state', 'users', '$localStorage', 'URLS'];

    function OrganizationUsersController ($scope, $location, $state, users, $localStorage, URLS) {
        var vm = this;

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;

        vm.isAuthenticated = null;

        vm.records = users;

        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);

    }
})();
