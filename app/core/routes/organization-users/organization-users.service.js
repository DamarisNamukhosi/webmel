(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('OrganizationUsersService', OrganizationUsersService);

    OrganizationUsersService.$inject = ['$resource', '$localStorage', 'URLS'];

    function OrganizationUsersService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
        'getOrganizationUsers': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/organisation-users/get-organisation-users/:id' 
        },
        'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/organisation-users/:id'
            },
        'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
                },
            url: URLS.BASE_URL + 'contentservice/api/account/create-organisation-user'
        },
        'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/account/create-organisation-user'
        }
    });
    }
})();
