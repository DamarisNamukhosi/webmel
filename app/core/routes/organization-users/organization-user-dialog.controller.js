(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OrganizationUserDialogController', OrganizationUserDialogController);

        OrganizationUserDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'OrganizationUsersService','OrganizationTypesService'];

    function OrganizationUserDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, OrganizationUsersService, OrganizationTypesService) {
        var vm = this;

        vm.user = entity;
        vm.clear = clear;
        vm.create = create;
        vm.update = update;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function create() {        
            vm.isSaving = true;
            OrganizationUsersService.create(vm.user, onSaveSuccess, onSaveError);
        }

        function update () {
            vm.isSaving = true;
            OrganizationUsersService.update(vm.user, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
