(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AgeLimitsController', AgeLimitsController);

    AgeLimitsController.$inject = ['$scope', '$location', '$state', 'records', '$localStorage', 'URLS'];

    function AgeLimitsController ($scope, $location, $state, records, $localStorage, URLS) {
        var vm = this;

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        vm.isAuthenticated = null;

        vm.records = records;

        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);

    }
})();
