(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AgeLimitDeleteDialogController',AgeLimitDeleteDialogController);

        AgeLimitDeleteDialogController.$inject = ['$uibModalInstance', 'entity','AgeLimitsService'];

    function AgeLimitDeleteDialogController($uibModalInstance, entity,AgeLimitsService) {
        var vm = this;

        vm.ageLimit = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AgeLimitsService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
