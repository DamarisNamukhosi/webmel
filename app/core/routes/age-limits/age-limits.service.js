(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('AgeLimitsService', AgeLimitsService);

        AgeLimitsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function AgeLimitsService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getAgeLimits': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/organisation-age-limits/filter-by-organisation/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/organisation-age-limits/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/organisation-age-limits'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/organisation-age-limits'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/organisation-age-limits/:id'
          }
        });
    }
})();
