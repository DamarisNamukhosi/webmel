(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AgeLimitDetailController', AgeLimitDetailController);

        AgeLimitDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity'];

    function AgeLimitDetailController($scope, $rootScope, $stateParams, previousState, entity) {
        var vm = this;
        
        vm.ageLimit = entity;
        vm.previousState = previousState.name;
    }
})();
