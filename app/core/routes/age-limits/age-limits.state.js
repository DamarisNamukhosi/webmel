(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('age-limits', {
            parent: 'app',
            url: '/age-limits',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'age-limits'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/age-limits/age-limits.html',
                    controller: 'AgeLimitsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                ageLimits: ['$stateParams', '$localStorage', 'AgeLimitsService', function ($stateParams, $localStorage, AgeLimitsService ) {
                return AgeLimitsService.getAgeLimits({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('age-limits.new', {
            parent: 'age-limits',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'age-limits.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/age-limits/age-limit-dialog.html',
                    controller: 'AgeLimitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                name : null,
                                brief : null,
                                description : null,
                                notes : null,
                                minAge : null,
                                maxAge : null,
                                upperLimitType : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null,
                                generalAgeBracketId : null,
                                generalAgeBracketName : null
                              };
                              
                        }
                    }
                }).result.then(function() {
                    $state.go('age-limits', null, { reload: 'age-limits' });
                }, function() {
                    $state.go('age-limits');
                });
            }]
        })
        .state('age-limit-detail', {
            parent: 'age-limits',
            url: '/age-limits/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Age Limit Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/age-limits/age-limit-detail.html',
                    controller: 'AgeLimitDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AgeLimitsService', function($stateParams, AgeLimitsService) {
                    return AgeLimitsService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'age-limits',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('age-limits.edit', {
            parent: 'age-limits',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/age-limits/age-limit-dialog.html',
                    controller: 'AgeLimitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AgeLimitsService', function(AgeLimitsService) {
                            return AgeLimitsService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('age-limits', null, { reload: 'age-limits' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('age-limits.delete', {
            parent: 'age-limits',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/age-limits/age-limit-delete-dialog.html',
                    controller: 'AgeLimitDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AgeLimitsService', function(AgeLimitsService) {
                            return AgeLimitsService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('age-limits', null, { reload: 'age-limits' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
