(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AgeLimitDialogController', AgeLimitDialogController);

        AgeLimitDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AgeLimitsService','GeneralAgeBracketsService'];

    function AgeLimitDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity,AgeLimitsService, GeneralAgeBracketsService) {
        var vm = this;

        vm.ageLimit = entity;
        vm.clear = clear;
        vm.save = save;

        //vm.ageLimits = AgeLimitsService.getAgeLimits();
        vm.generalAgeLimits = GeneralAgeBracketsService.query();    

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.ageLimit.id !== null) {
                AgeLimitsService.update(vm.ageLimit, onSaveSuccess, onSaveError);
            } else {
                AgeLimitsService.create(vm.ageLimit, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
