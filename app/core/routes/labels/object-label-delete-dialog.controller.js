(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "ObjectLabelGroupDeleteDialogController",
      ObjectLabelGroupDeleteDialogController
    );

  ObjectLabelGroupDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "ObjectLabelsService"
  ];

  function ObjectLabelGroupDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    ObjectLabelsService
  ) {
    var vm = this;

    vm.clickedItem = $stateParams.itemToDelete;
    console.log("Id to delete: " + vm.clickedItem);

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    vm.save = function save() {
      vm.isSaving = true;
      ObjectLabelsService.delete({id: vm.clickedItem}, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
