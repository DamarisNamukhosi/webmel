(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "ObjectLabelGroupCreateDialogController",
      ObjectLabelGroupCreateDialogController
    );

  ObjectLabelGroupCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "ObjectLabelsService",
    "labelGroups",
    "savedselectedLabels"
  ];

  function ObjectLabelGroupCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    ObjectLabelsService,
    labelGroups,
    savedselectedLabels
  ) {
    var vm = this;

    vm.labelGroups = labelGroups;
    vm.savedselectedLabels = savedselectedLabels;
    // console.log('savedselectedLabels///');
    // console.log(vm.savedselectedLabels);
    vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.selectedLabels = [];
    vm.savedList = [];

    vm.noGroupsFound = (vm.labelGroups === undefined || vm.labelGroups.length === 0);

    vm.checkIfExists = function (id) {
      
      var found = false;
      angular.forEach(vm.savedselectedLabels, function(item){
        
        if(!found){
         
          if (item.labelId == id){
          
            found = true;

          }
        }

      });

      return found;
    }

    vm.toggleSelection = function(label) {
      var id = vm.selectedLabels.indexOf(label);
      if (id > -1) {
        vm.selectedLabels.splice(id, 1);
      } else {
        vm.selectedLabels.push(label);
      }
    };

    $timeout(function() {
      angular.element(".form-group:eq(1)>input").focus();
    });

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      angular.forEach(vm.selectedLabels, function(my_label) {
        var temp = {
          labelId: my_label.id,
          objectId: vm.entity.uuid
        };

        vm.savedList.push(temp);
      });
      console.log('savedlist...');
      console.log(vm.savedList);  
      ObjectLabelsService.createList(vm.savedList, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
