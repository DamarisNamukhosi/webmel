(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicles", {
        parent: "app",
        url: "/vehicles",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicles.html",
            controller: "VehiclesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicles: [
            "$stateParams",
            "VehiclesService",
            function($stateParams, VehiclesService) {
              return VehiclesService.getVehicles().$promise;
            }
          ]
        }
      })
      .state('vehicles.new', {
        parent: 'vehicles',
        url: '/new',
        data: {
            requiresAuthentication: true,
            authorities: [],
            pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
            '$stateParams',
            '$state',
            '$localStorage',
            '$uibModal',
            function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal
                    .open({
                        templateUrl:
                            'core/routes/vehicles/vehicle-dialog.html',
                        controller: 'VehicleDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function() {
                                return {
                                  brief: null,
                                  capacity: null,
                                  contentStatus: "DRAFT",
                                  description: null,
                                  generalTransportSubCategoryId: null,
                                  generalTransportSubCategoryName: null,
                                  id: null,
                                  notes: null,
                                  organisationId: $localStorage.current_organisation.id,
                                  organisationName: $localStorage.current_organisation.name,
                                  profileStatus: "INACTIVE",
                                  registration: null,
                                  statusReason: null,
                                  uuid: null
                                };
                            }
                        }
                    })
                    .result.then(
                        function() {
                            $state.go('vehicles', null, {
                                reload: 'vehicles'
                            });
                        },
                        function() {
                            $state.go('vehicles');
                        }
                    );
            }
        ]
    })
      .state("vehicles-detail", {
        parent: "vehicles",
        url: "/{id}/details",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicle-detail.html",
            controller: "VehicleDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehiclesService",
            function($stateParams, VehiclesService) {
              return VehiclesService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicles-detail.edit", {
        parent: "vehicles-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-dialog.html",
                controller: "VehicleDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-detail", null, {
                    reload: "vehicles-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-own", {
        parent: "vehicles",
        url: "/own",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicles-own.html",
            controller: "VehiclesOwnController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicles: [
            "$stateParams",
            "$localStorage",
            "VehiclesService",
            function($stateParams, $localStorage, VehiclesService) {
              return VehiclesService.getVehiclesByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("vehicles-own.new", {
        parent: "vehicles-own",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER",
            "PROFESSIONAL_USER",
            "T_OPERATOR_USER",
            "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-dialog.html",
                controller: "VehicleDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      registration: null,
                      brief: null,
                      description: null,
                      notes: null,
                      capacity: null,
                      profileStatus: "INACTIVE",
                      contentStatus: "DRAFT",
                      statusReason: "Created",
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      generalTransportSubCategoryId: null,
                      generalTransportSubCategoryName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-own", null, { reload: "vehicles-own" });
                },
                function() {
                  $state.go("vehicles-own");
                }
              );
          }
        ]
      })
      .state("vehicles-own-detail", {
        parent: "vehicles-own",
        url: "/{id}/vehicle",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicle-detail.html",
            controller: "VehicleDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehiclesService",
            function($stateParams, VehiclesService) {
              return VehiclesService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "vehicles-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicles.edit", {
        parent: "vehicles",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-dialog.html",
                controller: "VehicleDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-own", null, { reload: "vehicles-own" });
                },
                function() {
                  $state.go("vehicles-own");
                }
              );
          }
        ]
      })
      .state("vehicles-own.edit", {
        parent: "vehicles-own",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-dialog.html",
                controller: "VehicleDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-own", null, { reload: "vehicles-own" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles.delete", {
        parent: "vehicles",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-delete-dialog.html",
                controller: "VehicleDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles", null, { reload: "vehicles" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-own.delete", {
        parent: "vehicles-own-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicles/vehicle-delete-dialog.html",
                controller: "VehicleDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-own", null, {
                    reload: "vehicles-own"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles.profile", {
        parent: "vehicles-own-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/vehicles/image-upload/image-upload-dialog.html",
                controller: "VehicleImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    "$stateParams",
                    "VehiclesService",
                    function($stateParams, VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicles-own-detail", null, {
                    reload: "vehicles-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-galleries", {
        parent: "app",
        url: "/vehicles/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicle-galleries.html",
            controller: "VehicleGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehiclesService",
            function($stateParams, VehiclesService) {
              return VehiclesService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-galleries.new", {
        parent: "vehicle-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehiclesService",
                    function(VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-galleries", null, {
                    reload: "vehicle-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-galleries.delete", {
        parent: "vehicle-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-galleries", null, {
                    reload: "vehicle-galleries"
                  });
                },
                function() {
                  $state.go("vehicle-galleries");
                }
              );
          }
        ]
      })
      .state("vehicle-album-detail", {
        parent: "vehicle-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicles/vehicle-album-detail.html",
            controller: "VehicleAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehiclesService",
            function($stateParams, VehiclesService) {
              return VehiclesService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "vehicle-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-album-detail.edit", {
        parent: "vehicle-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "vehicle-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService",function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("vehicle-album-detail", null, { reload: "vehicle-album-detail" });
              },
              function () {
                $state.go("vehicle-album-detail");
              }
              );
          }
        ]
      })
      .state("vehicle-album-detail.makeCoverImage", {
        parent: "vehicle-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-album-detail", null, {
                    reload: "vehicle-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-album-detail.deleteImage", {
        parent: "vehicle-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-album-detail", null, {
                    reload: "vehicle-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-album-detail.upload", {
        parent: "vehicle-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-album-detail", null, {
                    reload: "vehicle-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-own-detail.addLabels", {
        parent: "vehicles-own-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          "$localStorage",
          function ($stateParams, $state, $uibModal, $localStorage) {
            $uibModal
              .open({
                templateUrl: "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType})
                        .$promise;
                    }
                  ],
                  entity: [
                    "VehiclesService",
                    function (VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("vehicles-own-detail", null, {
                  reload: "vehicles-own-detail"
                });
              },
              function () {
                $state.go("^");
              }
            );//
          }
        ]
      })
      .state("vehicles-own-detail.deleteLabel", {
        parent: "vehicles-own-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
              function () {
                $state.go("vehicles-own-detail", null, {
                  reload: "vehicles-own-detail"
                });
              },
              function () {
                $state.go("^");
              }
              );
          }
        ]
      })
      .state("vehicles-own-detail.addFeature", {
        parent: "vehicles-own-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-create-dialog.html",
                controller: "ObjectFeatureCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  features: [
                    "ObjectFeaturesService",
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "VehiclesService",
                    function (VehiclesService) {
                      return VehiclesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    "ObjectFeaturesService",
                    function (ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("vehicles-own-detail", null, {
                  reload: "vehicles-own-detail"
                });
              },
              function () {
                $state.go("vehicles-own-detail");
              });
          }
        ]
      })
      .state("vehicles-own-detail.deleteFeature", {
        parent: "vehicles-own-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-delete-dialog.html",
                controller: "ObjectFeatureDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
              function () {
                $state.go("vehicles-own-detail", null, {
                  reload: "vehicles-own-detail"
                });
              },
              function () {
                $state.go("vehicles-own-detail");
              }
              );
          }
        ]
      });
  }
})();
