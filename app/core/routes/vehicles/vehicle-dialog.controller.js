(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleDialogController', VehicleDialogController);

        VehicleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'VehiclesService', 'VehicleTypesService'];

    function VehicleDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, VehiclesService, VehicleTypesService) {
        var vm = this;

        vm.vehicle = entity;
        vm.clear = clear;
        vm.save = save;
        vm.selectedGeneralType = selectedGeneralType;


        vm.subCategories = VehicleTypesService.getVehicleTypes();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.vehicle.id !== null) {
                VehiclesService.update(vm.vehicle, onSaveSuccess, onSaveError);
            } else {
                VehiclesService.create(vm.vehicle, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function selectedGeneralType(generalTypeId) {
          angular.forEach(vm.subCategories, function(subCategory) {
            if (subCategory.id == generalTypeId) {
              vm.vehicle.order = subCategory.order;
              vm.vehicle.brief = subCategory.brief;
              vm.vehicle.description = subCategory.description;
              vm.vehicle.capacity = subCategory.capacity;
              vm.vehicle.extendedCapacity = subCategory.extendedCapacity;

            }
          });
        }


    }
})();
