(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('VehiclesOwnController', VehiclesOwnController);

  VehiclesOwnController.$inject = ['$scope', '$location', '$state', 'vehicles', 'URLS', 'VehiclesService'];

  function VehiclesOwnController($scope, $location, $state, vehicles, URLS, VehiclesService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;

    vm.profile_url = URLS.PROFILE_IMAGE_URL;

    vm.vehicles = vehicles;
    vm.noRecordsFound = false;
    if (vm.vehicles.length === 0) {
      vm.noRecordsFound = true;
    }

    vm.sortableOptions = {
      stop: function(e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    }

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.vehicles, function(vehicles) {

        vehicles.order = count * 100;

        count = count + 1;
      });

      console.log("vm.vehicles");
      console.log(vm.vehicles);


      // call update
      VehiclesService.updateList(vm.vehicles);

    }

  }
})();
