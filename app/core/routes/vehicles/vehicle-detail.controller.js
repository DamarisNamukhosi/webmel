(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("VehicleDetailController", VehicleDetailController);

  VehicleDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function VehicleDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    ObjectLabelsService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;
    vm.vehicle = entity;
    console.log(vm.vehicle);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.vehicle.uuid;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({
      uuid: vm.vehicle.uuid
    });
    console.log(vm.objectLabels);

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({
      uuid: vm.vehicle.uuid
    });
    console.log("object features");
    console.log(vm.objectFeatures);
  }
})();
