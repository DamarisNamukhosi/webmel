(function() {
  'use strict';
  angular
    .module('webApp')
    .factory('VehiclesService', VehiclesService);

  VehiclesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function VehiclesService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'getVehicles': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicles'
      },
      'getVehiclesByOrganizationId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicles/filter-by-organisation/:id?sort=order%2Casc'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'contentservice/api/vehicles/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicles'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicles'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/vehicles/:id'
      },
      'updateList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/vehicles/save-list'
      },
      'uploadProfilePhoto': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
      }
    });
  }
})();
