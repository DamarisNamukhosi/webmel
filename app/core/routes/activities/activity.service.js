(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('ActivityService', ActivityService);

        ActivityService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ActivityService ($resource, $localStorage, URLS) {
        var resourceUrl =  '';

        return $resource(resourceUrl, {}, {
          'getActivitiesByLocId': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/activities/filter-by-location/:id'
          },
          'getActivitiesByOrgId': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/activities/filter-by-organisation/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/activities/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/activities'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/activities'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/activities/:id'
          }
        });
    }
})();
