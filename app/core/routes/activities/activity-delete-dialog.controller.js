(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ActivityDeleteDialogController',ActivityDeleteDialogController);

    ActivityDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ActivityService'];

    function ActivityDeleteDialogController($uibModalInstance, entity, ActivityService) {
        var vm = this;

        vm.activity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        
        function confirmDelete (activity) {
            activity.contentStatus = "DELETED";
            ActivityService.update(activity,
                  function () {
                      $uibModalInstance.close(true);
                  });
          }
    }
})();
