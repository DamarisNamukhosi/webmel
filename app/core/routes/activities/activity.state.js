(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("activities", {
        parent: "app",
        url: "/activities",
        data: {
          requiresAuthentication: true,
          authorities: ["PROPERTY_USER"],
          pageTitle: "activities"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/activities/activity.html",
            controller: "ActivityController",
            controllerAs: "vm"
          }
        },
        resolve: {
          activities: [
            "$stateParams",
            "$localStorage",
            "ActivityService",
            function ($stateParams, $localStorage, ActivityService) {
              return ActivityService.getActivitiesByLocId({
                id: $localStorage.current_location_id
              }).$promise;
            }
          ]
        }
      })
      .state("activities.new", {
        parent: "activities",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "documents.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/activities/activity-dialog.html",
                controller: "ActivityDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      locationId: $localStorage.current_location_id,
                      locationName: $localStorage.current_organisation.name,
                      generalActivityId: null,
                      generalActivityName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go("activities", null, { reload: "activities" });
                },
                function () {
                  $state.go("activities");
                }
              );
          }
        ]
      })
      .state("activity-detail", {
        parent: "activities",
        url: "/activities/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Activity Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/activities/activity-detail.html",
            controller: "ActivityDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "ActivityService",
            function ($stateParams, ActivityService) {
              var data = ActivityService.get({ id: $stateParams.id }).$promise;
              console.log(data);
              return data;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "activities",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("activity-detail.image", {
        parent: "activity-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/activities/activity-image-upload-dialog.html",
                controller: "ActivitiesImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  activity: [
                    "$stateParams",
                    "ActivityService",
                    function ($stateParams, ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-detail", null, {
                    reload: "activity-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-detail.edit", {
        parent: "activity-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/activities/activity-dialog.html",
                controller: "ActivityDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-detail", null, {
                    reload: "activity-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-detail.delete", {
        parent: "activity-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/activities/activity-delete-dialog.html",
                controller: "ActivityDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-detail", null, {
                    reload: "activity-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activities.edit", {
        parent: "activities",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/activities/activity-dialog.html",
                controller: "ActivityDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activities", null, { reload: "activities" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activities.delete", {
        parent: "activities",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/activities/activity-delete-dialog.html",
                controller: "ActivityDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activities", null, { reload: "activities" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-galleries", {
        parent: "app",
        url: "/activities/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS","ORGANISATION_ADMIN"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/activities/activity-galleries.html",
            controller: "ActivityGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "ActivityService",
            function ($stateParams, ActivityService) {
              return ActivityService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("activity-galleries.delete", {
        parent: "activity-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function () {
                  $state.go("activity-galleries", null, {
                    reload: "activity-galleries"
                  });
                },
                function () {
                  $state.go("activity-galleries");
                }
              );
          }
        ]
      })
      .state("activity-galleries.new", {
        parent: "activity-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go("activity-galleries", null, {
                    reload: "activity-galleries"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-album-detail", {
        parent: "activity-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/activities/activity-album-detail.html",
            controller: "ActivityAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "ActivityService",
            function ($stateParams, ActivityService) {
              return ActivityService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || "activity-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("activity-album-detail.edit", {
        parent: "activity-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "activity-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService", function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-album-detail", null, { reload: "activity-album-detail" });
                },
                function () {
                  $state.go("activity-album-detail");
                }
              );
          }
        ]
      })
      .state("activity-album-detail.makeCoverImage", {
        parent: "activity-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-album-detail", null, {
                    reload: "activity-album-detail"
                  });
                },
                function () {
                  $state.go("activity-album-detail");
                }
              );
          }
        ]
      })
      .state("activity-album-detail.deleteImage", {
        parent: "activity-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-album-detail", null, {
                    reload: "activity-album-detail"
                  });
                },
                function () {
                  $state.go("activity-album-detail");
                }
              );
          }
        ]
      })
      .state("activity-album-detail.upload", {
        parent: "activity-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-album-detail", null, {
                    reload: "activity-album-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-detail.addLabels", {
        parent: "activity-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("activity-detail", null, {
                    reload: "activity-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("activity-detail.deleteLabel", {
        parent: "activity-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function () {
                  $state.go("activity-detail", null, {
                    reload: "activity-detail"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      });
  }
})();
