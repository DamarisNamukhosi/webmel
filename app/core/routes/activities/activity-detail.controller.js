(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("ActivityDetailController", ActivityDetailController);

  ActivityDetailController.$inject = [
    "URLS",
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "ActivityService"
  ];

  function ActivityDetailController(
    URLS,
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    ObjectLabelsService,
    ActivityService
  ) {
    var vm = this;

    vm.activity = entity;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.activity.uuid });
    console.log(vm.objectLabels);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.activity.uuid;

    vm.currentLocationIdSet = !(
      $localStorage.current_location_id === undefined ||
      $localStorage.current_location_id === null ||
      $localStorage.current_location_id === "null"
    );
  }
})();
