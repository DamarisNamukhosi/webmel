(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ActivityDialogController', ActivityDialogController);

        ActivityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ActivityService', 'ActivityTypesService'];

    function ActivityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ActivityService, ActivityTypesService) {
        var vm = this;

        vm.activity = entity;
        vm.clear = clear;
        vm.save = save;

        vm.activityTypes = ActivityTypesService.query();
        //vm.activities = ActivityService.getActivities();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            
            console.log(vm.activity);
            vm.isSaving = true;
            if (vm.activity.id !== null) {
                ActivityService.update(vm.activity, onSaveSuccess, onSaveError);
            } else {
                ActivityService.create(vm.activity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
