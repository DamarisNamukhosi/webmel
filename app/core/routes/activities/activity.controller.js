(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ActivityController', ActivityController);

    ActivityController.$inject = ['$scope', '$location', '$state', 'activities', 'URLS'];

        function ActivityController($scope, $location, $state, activities, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.activities = activities;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound =false
         if(vm.activities === undefined || vm.activities.length === 0){
          vm.noRecordsFound= true;  
         }
            ;

    }
})();
