(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DayBandDialogController', DayBandDialogController);

    DayBandDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$uibModalInstance', 'DayBandsService', 'entity'];

    function DayBandDialogController($timeout, $scope, $stateParams, $localStorage, $uibModalInstance, DayBandsService, entity) {
        var vm = this;
       
        vm.dayBand = entity;
        vm.clear = clear;
        vm.save = save;
        vm.isEmpty = false;
        console.log('dayband....');
        console.log(vm.dayBand);

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        
        function save () {
            vm.isSaving = true;
            if (vm.dayBand.id !== null) {
                DayBandsService.update(vm.dayBand, onSaveSuccess, onSaveError);
            } else {
                DayBandsService.create(vm.dayBand, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
