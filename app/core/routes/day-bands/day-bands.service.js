(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('DayBandsService', DayBandsService);

    DayBandsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function DayBandsService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'getDayBandsByOrganizaitonId': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/day-bands/filter-by-organisation/:id'
            },
            'getFullBand': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands/get-full/:id'
            },
            'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands'
            },

            'createFull': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands/create-full'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/day-bands/:id'
            },
            'deleteDays': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/days/:id'
            }
        });
    }
})();
