(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DayBandsController', DayBandsController);

    DayBandsController.$inject = ['$scope', '$location', '$state', 'dayBands', '$localStorage', 'DayBandsService','URLS'];

    function DayBandsController($scope, $location, $state, dayBands, $localStorage, DayBandsService,URLS) {
        var vm = this;
        console.log("Hey");

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        vm.isAuthenticated = null;

        vm.records = dayBands;
        console.log('day bands..');
        console.log(vm.records);
        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);


    }
})();
