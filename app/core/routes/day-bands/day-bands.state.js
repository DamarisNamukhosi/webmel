(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('day-bands', {
            parent: 'app',
            url: '/day-bands',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'day-bands'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/day-bands/day-bands.html',
                    controller: 'OperatingHoursController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
              operatingHours: ['$stateParams', '$localStorage', 'OperatingHoursService', function ($stateParams, $localStorage, OperatingHoursService ) {
                return OperatingHoursService.getOperatingHoursByOrganizaitonId  ({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('day-bands.new', {
            parent: 'day-bands',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'profsApp.professional.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/day-bands/day-band-dialog.html',
                    controller: 'OperatingHourDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                name : null,
                                openingTime : null,
                                operatingHoursType : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('day-bands', null, { reload: 'day-bands' });
                }, function() {
                    $state.go('day-bands');
                });
            }]
        })
        .state('day-band-detail', {
            parent: 'day-bands',
            url: '/day-bands/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Operating Hours Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/day-bands/day-band-detail.html',
                    controller: 'OperatingHourDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'OperatingHoursService', function($stateParams, OperatingHoursService) {
                    return OperatingHoursService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'day-bands',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
            .state('day-bands.edit', {
            parent: 'day-bands',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/day-bands/day-band-dialog.html',
                    controller: 'OperatingHourDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['OperatingHoursService', function(OperatingHoursService) {
                            return OperatingHoursService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('day-bands', null, { reload: 'day-bands' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
            .state('day-bands.delete', {
                parent: 'day-bands',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/day-bands/day-band-delete-dialog.html',
                    controller: 'OperatingHourDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['OperatingHoursService', function(OperatingHoursService) {
                            return OperatingHoursService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('day-bands', null, { reload: 'day-bands' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
