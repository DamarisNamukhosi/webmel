(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("DayBandCreateDialogController", DayBandCreateDialogController);

  DayBandCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "DayBandsService",
    "days"
  ];

  function DayBandCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    DayBandsService,
    days
  ) {
    var vm = this;

    vm.isSaving = false;
    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    vm.days = days;
    vm.exsitingDays = [];
    vm.workingDays = ['MONDAY','TUESDAY', 'WEDNESDAY','THURSDAY','FRIDAY','SATURDAY','SUNDAY'];
    var i = 1;
    angular.forEach(vm.days, function (days) {
      i++;
      angular.forEach(days.dayDTOList, function (weekdays) {
        vm.exsitingDays.push(weekdays.weekDay);
      });
    });

    console.log(vm.exsitingDays);

    vm.difference = [];
    vm.difference = _.difference(vm.workingDays, vm.exsitingDays);

    vm.selectedList = [];

      vm.toggleSelection = function (day) {
          console.log('inside toggle');
          
          var id = vm.selectedList.indexOf(day);
          console.log(id);
          if (id > -1) {
              vm.selectedList.splice(id, 1);
          } else {
              vm.selectedList.push(day);

          }
      };


    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      console.log('vm.selectedList..');
      console.log(vm.selectedList);
      angular.forEach(vm.selectedList, function (item) {
        var temp = {
          bandId: vm.entity.id,
          bandName: vm.entity.name,
          id: null,
          uuid: null,
          weekDay: item
        };
        console.log('temp.,,');
        console.log(temp);
        vm.entity.dayDTOList.push(temp);
      });

      console.log('saving..');
      console.log(vm.entity);
        console.log('entity id..');
        console.log(vm.entity.id);

      if (vm.entity.id !== null) {
          console.log('inside ');
          DayBandsService.create(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      }else{
          DayBandsService.createFull(
              vm.entity,
              onSaveSuccess,
              onSaveError
          );
      }
    }

    function onSaveSuccess(result) {
      console.log("done");
      vm.isSaving = false;
      $uibModalInstance.close(result);
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
