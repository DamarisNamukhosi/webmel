(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DayBandDetailController', DayBandDetailController);

    DayBandDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DayBandsService'];

    function DayBandDetailController($scope, $rootScope, $stateParams, previousState, entity, DayBandsService) {
        var vm = this;
        
        vm.dayBand = entity;
        vm.previousState = previousState.name;

        console.log(vm.dayBand.days);

        vm.objectLabels = DayBandsService.getLabelsByObjectId({ uuid: vm.menuItem.uuid });
        console.log('object labels...');
    }
})();
