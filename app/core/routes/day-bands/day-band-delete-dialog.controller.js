(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DayBandDeleteDialogController',DayBandDeleteDialogController);

        DayBandDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'DayBandsService'];

    function DayBandDeleteDialogController($uibModalInstance, entity, DayBandsService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function confirmDelete() {
            vm.entity.contentStatus = "DELETED";
            console.log('about to delete : '+vm.entity.id);
            console.log(vm.entity);
            DayBandsService.update(entity, function() {
              $uibModalInstance.close(true);
            });
          }    }
})();
