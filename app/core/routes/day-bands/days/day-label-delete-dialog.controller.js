(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "DayLabelDeleteDialogController",
    DayLabelDeleteDialogController
    );

  DayLabelDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "DayBandsService"
  ];

  function DayLabelDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    DayBandsService
  ) {
    var vm = this;

    vm.clickedItem = $stateParams.dayId;
    console.log("Id to delete: " + vm.clickedItem);

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    vm.save = function save() {
      vm.isSaving = true;
      DayBandsService.deleteDays({id: vm.clickedItem}, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
