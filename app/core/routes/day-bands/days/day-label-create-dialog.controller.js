(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("DayLabelCreateDialogController", DayLabelCreateDialogController);

  DayLabelCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "DayBandsService",
    "DaysService",
    "days"
  ];

  function DayLabelCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    DayBandsService,
    DaysService,
    days
  ) {
    var vm = this;

    vm.isSaving = false;
    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;

    vm.days = days;

    vm.days1 = [];

    //remove days already on the DayBand
    vm.setDayBands = vm.entity.dayDTOList; //array

    vm.days = days;
    vm.exsitingDays = [];
    vm.workingDays = ["MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY","FRIDAY","SATURDAY", "SUNDAY"];
    var i = 1;
    angular.forEach(vm.days, function (days) {
      i++;
      angular.forEach(days.dayDTOList, function (weekdays) {
        vm.exsitingDays.push(weekdays.weekDay);
      });
      console.log("vm.exsitingDays");
      console.log(vm.exsitingDays);
    });

    vm.difference = [];
    vm.difference = _.difference(vm.workingDays, vm.exsitingDays);
    console.log("vm.difference");
    console.log(vm.difference);
    vm.selectedList = [];

    vm.toggleSelection = function (day) {
      var id = vm.selectedList.indexOf(day);
      console.log(id);
      if (id > -1) {
        vm.selectedList.splice(id, 1);
      } else {
        vm.selectedList.push(day);

      }
    };

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }
    vm.savedList = [];
    function save() {
      vm.isSaving = true;
      angular.forEach(vm.selectedList, function (item) {
        var temp = {
          "bandId": vm.entity.id,
          "weekDay": item
        };
        vm.savedList.push(temp);
      });
      DaysService.saveMultiple(vm.savedList, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      console.log("done");
      vm.isSaving = false;
      $uibModalInstance.close(result);
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();