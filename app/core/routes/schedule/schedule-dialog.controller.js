(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('ScheduleDialogController', ScheduleDialogController);

  ScheduleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ScheduleService', 'LocationsService', 'RoutesService', '$localStorage'];

  function ScheduleDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, ScheduleService, LocationsService, RoutesService, $localStorage) {
    var vm = this;

    vm.schedule = entity;

    vm.clear = clear;
    vm.save = save;
    vm.getFromLocation = getFromLocation;
    vm.getToLocation = getToLocation;
    vm.onFromLocationSelected = onFromLocationSelected;
    vm.onToLocationSelected = onToLocationSelected;
    vm.scheduleTypes = ["DIRECT", "INDIRECT"];

    //vm.selectedRoute = selectedRoute;
    vm.routes = [];

    vm.routeChanged = routeChanged;

    console.log('meal-plans....');
    console.log(vm.schedule);
    //vm.Schedule = ScheduleService.getSchedule();
    //vm.generalSchedule = GeneralScheduleService.query();
    initRoutes();

    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.schedule.id !== null) {
        ScheduleService.update(vm.schedule, onSaveSuccess, onSaveError);
      } else {
        ScheduleService.create(vm.schedule, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function getFromLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 18, 19]
        })
        .$promise
        .then(function(results) {
          vm.searchedItems = results;
          return results.map(function(item) {

            return item;
          })
        });
    }

    function getToLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 18, 19]
        })
        .$promise
        .then(function(results) {
          vm.searchedItems = results;
          return results.map(function(item) {

            return item;
          })
        });
    }

    function onFromLocationSelected($item, $model) {

      //   vm.schedule.fromId=$item.id;
      //   vm.schedule.fromAbbreviation =  $item.abbreviation;

    }

    function onToLocationSelected($item, $model) {
      console.log($item);
      // vm.schedule.toIdtoId=$item.id;
      // vm.schedule.toAbbreviation =  $item.abbreviation;

    }




    function initRoutes() {
      RoutesService.getRoutes({
        id: $localStorage.current_organisation.id,
        size: 1000
      }).$promise.then(function(response) {
        vm.routes = response;
        angular.forEach(vm.routes, function(route){
          if(vm.schedule.routeId == route.id){
            vm.schedule.fromName = route.fromAbbreviation;
            vm.schedule.toName = route.toAbbreviation;
            vm.schedule.name = route.fromAbbreviation + ' - ' + route.toAbbreviation;
          }
        })
      });

    }

    // function selectedRoute(routeId) {
    //   angular.forEach(vm.routes, function(route) {
    //     if (route.id == routeId) {
    //       vm.schedule.fromName = route.fromName;
    //       vm.schedule.toName = route.fromName;
    //
    //
    //     }
    //   });
    // }
    function routeChanged(routeId) {
      console.log("some text");
      angular.forEach(vm.routes, function(route){
        if(route.id == routeId){
          console.log(route);
          vm.schedule.sourceTerminals = route.fromName;
           vm.schedule.destinationTerminals = route.toName;
           vm.schedule.name = route.fromAbbreviation + ' - ' + route.toAbbreviation;

        }
      });
    }


  }
})();
