(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ScheduleDeleteDialogController',ScheduleDeleteDialogController);

        ScheduleDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ScheduleService'];

    function ScheduleDeleteDialogController($uibModalInstance, entity, ScheduleService) {
        var vm = this;

        vm.schedule = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ScheduleService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
