(function () {
  'use strict';

  angular
    .module('webApp')
    .config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider.state('schedule', {
      parent: 'app',
      url: '/schedule',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'schedule'
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/schedule/schedule.html',
          controller: 'ScheduleController',
          controllerAs: 'vm'
        }
      },
      params: {
        page: {
          value: '1',
          squash: true
        },
        sort: {
          value: 'id',
          squash: true
        },
        search: null
      },
      resolve: {
        pagingParams: [
          '$stateParams',
          'PaginationUtil',
          function ($stateParams, PaginationUtil) {
            return {
              page: PaginationUtil.parsePage(
                $stateParams.page
              ),
              sort: $stateParams.sort,
              predicate: PaginationUtil.parsePredicate(
                $stateParams.sort
              ),
              ascending: PaginationUtil.parseAscending(
                $stateParams.sort
              ),
              search: $stateParams.search
            };
          }
        ],
        schedule: ['$localStorage', 'ScheduleService', function ($localStorage, ScheduleService) {
          return ScheduleService.getSchedules({ id: $localStorage.current_organisation.id }).$promise;
        }]
      }
    })
      .state('schedule.new', {
        parent: 'schedule',
        url: '/{routeId}/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/schedule/schedule-dialog.html',
            controller: 'ScheduleDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
              entity: function () {
                return {
                  id : null,
                  uuid : null,
                  name:null,
                  routeId : $stateParams.routeId,
                  sourceTerminals : null,
                  destinationTerminals : null,
                  type : "DIRECT",
                  segments: 1,
                  eta : null,
                  etd : null,
                  carrier : null,
                  notes : null,
                  order : null,
                  contentStatus : "DRAFT",
                  statusReason : null,
                  createdOn : null,
                  createdBy : null,
                  lastUpdatedOn : null,
                  lastUpdatedBy : null,
                  organisationId : $localStorage.current_organisation.id,
                  organisationName : $localStorage.current_organisation.name
                };
              }
            }
          }).result.then(function () {
            $state.go('schedule', null, { reload: 'schedule' });
          }, function () {
            $state.go('schedule');
          });
        }]
      })
      .state('schedue-detail', {
        parent: 'app',
        url: '/schedule/{scheduleId}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Meal Plans Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/schedule/schedule-detail.html',
            controller: 'ScheduleDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: ['$stateParams', 'ScheduleService', function ($stateParams, ScheduleService) {
            return ScheduleService.get({ id: $stateParams.scheduleId }).$promise;
          }],
          previousState: ["$state", function ($state) {
            var currentStateData = {
              name: $state.current.name || 'schedule',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }]
        }
      })
      .state('schedule.edit', {
        parent: 'schedule',
        url: '/{scheduleId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/schedule/schedule-dialog.html',
            controller: 'ScheduleDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: ['ScheduleService', function (ScheduleService) {
                return ScheduleService.get({ id: $stateParams.scheduleId }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('schedule', null, { reload: 'schedule' });
          }, function () {
            $state.go('^');
          });
        }]
      })
      .state('schedule.delete', {
        parent: 'schedule',
        url: '/{scheduleId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/schedule/schedule-delete-dialog.html',
            controller: 'ScheduleDeleteDialogController',
            controllerAs: 'vm',
            size: 'md',
            resolve: {
              entity: ['ScheduleService', function (ScheduleService) {
                return ScheduleService.get({ id: $stateParams.scheduleId }).$promise;
              }]
            }
          }).result.then(function () {
            $state.go('schedule', null, { reload: 'schedule' });
          }, function () {
            $state.go('schedule');
          });
        }]
      });

  }
})();
