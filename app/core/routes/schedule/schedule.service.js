(function() {
  'use strict';
  angular
    .module('webApp')
    .factory('ScheduleService', ScheduleService);

  ScheduleService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ScheduleService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'getSchedules': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        //filter-by-organisation/736?routeId=4&sort=asc

        url: URLS.BASE_URL + 'contentservice/api/transport-schedules/filter-by-organisation/:id?&routeId=:routeId&sort=order%2Casc'
        //contentservice/api/routes/filter-by-organisation/:id?sort=order%2Casc
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/transport-schedules/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/transport-schedules'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/transport-schedules'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/transport-schedules/:id'
      },
      'updateList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/transport-schedules/save-list'
      },
    });
  }
})();
