(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ScheduleController', ScheduleController);

  ScheduleController.$inject = ['$scope', '$location', '$state', 'ScheduleService', '$localStorage', 'URLS', 'RoutesService', 'ParseLinks', 'pagingParams', '$stateParams'];

  function ScheduleController($scope, $location, $state, ScheduleService, $localStorage, URLS, RoutesService, ParseLinks, pagingParams, $stateParams) {
    var vm = this;
    //
    vm.account = $localStorage.current_organisation;
    console.log($localStorage.current_organisation);



    vm.scheduleType = ['LOCATIONGROUP', 'LOCATION'];

    vm.records = [];
    vm.schedules = [];
    vm.routes = [];
    vm.noRecordsFound = true;
    vm.itemsPerPage = 20;
    vm.itemsPerPageStr = "20";

    vm.selectedRoute = selectedRoute;
    vm.routeChanged = routeChanged;

    vm.changeItemsPerPage = changeItemsPerPage;
    initSchedules();
    initRoutes();





    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.rowCollectionDisplayed, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.rowCollectionDisplayed");
      console.log(vm.rowCollectionDisplayed);


      // call update
      ScheduleService.updateList(vm.rowCollectionDisplayed, onSaveSuccess, onSaveError);

    }

    function onSaveSuccess() {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("update sucessful");
    }

    function onSaveError() {
      console.log("update not sucessful");
    }


    function initSchedules() {
      //{id: $localStorage.current_organisation.id}
      ScheduleService.getSchedules({
        id: $localStorage.current_organisation.id,
        page: pagingParams.page - 1,
        size: 1000,
        sort: 'id'
      },
        function (data, headers) {
          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.records = data;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.isLoading = false;

          vm.noRecordsFound =
            vm.routes === undefined || vm.routes.length === 0;
        },
        function (error) {
          console.log('error getting contracts');
          console.log(error);
          vm.isLoading = false;
        }
      ).$promise.then(function (response) {
        vm.schedules = response;
        if (vm.schedules.length > 0) {
          vm.noRecordsFound = false;
        }
      });

    }
    //getSchedules
    // vm.routes
    function initRoutes() {
      RoutesService.getRoutes({
        id: $localStorage.current_organisation.id,
        size: 1000
      }).$promise.then(function (response) {
        vm.routes = response;
      });

    }

    function selectedRoute(routeId) {
      angular.forEach(vm.routes, function (route) {
        if (route.id == routeId) {
          vm.schedule.fromName = route.fromName;
          vm.schedule.toName = route.toName;


        }
      });
    }

    function routeChanged(routeId) {
      console.log("some text");
      ScheduleService.getSchedules({
        routeId: routeId,
        id: $localStorage.current_organisation.id
      }).$promise.then(function (response) {
        vm.schedules = response;
        if (vm.schedules.length > 0) {
          vm.noRecordsFound = false;
        }
      }
      );
    }

    function changeItemsPerPage(strValue) {

      console.log("converting to int: " + strValue);
      vm.itemsPerPage = parseInt(strValue, 10);
    }


  }
})();
