(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FacilityController', FacilityController);

    FacilityController.$inject = ['$scope', '$location', '$state', 'facilities', 'URLS'];

        function FacilityController($scope, $location, $state, facilities, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.facilities = facilities;
        console.log(vm.facilities);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound = false;
         if(vm.facilities === undefined || vm.facilities.length === 0){
            vm.noRecordsFound= true;
         };

    }
})();
