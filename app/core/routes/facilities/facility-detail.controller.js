(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("FacilityDetailController", FacilityDetailController);

  FacilityDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "FacilityService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function FacilityDetailController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    ObjectLabelsService,
    FacilityService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;

    vm.facility = entity;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.facility.uuid });
    console.log(vm.objectLabels);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.facility.uuid;

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({ uuid: vm.facility.uuid });
    console.log('object labels...');
    console.log(vm.objectFeatures);

    vm.currentLocationIdSet = !(
      $localStorage.current_location_id === undefined ||
      $localStorage.current_location_id === null ||
      $localStorage.current_location_id === "null"
    );
  }
})();
