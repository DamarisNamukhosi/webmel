(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('FacilityService', FacilityService);

        FacilityService.$inject = ['$resource', '$localStorage', 'URLS'];

    function FacilityService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getFacilities': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/facilities/filter-by-location/:id'
          },
          'getFacilitiesByOrgId': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/facilities/filter-by-organisation/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/facilities/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/facilities'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/facilities'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/facilities/:id'
          }
        });
    }
})();
