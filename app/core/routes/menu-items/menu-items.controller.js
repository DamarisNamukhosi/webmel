(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuItemsController', MenuItemsController);

    MenuItemsController.$inject = ['$scope', '$location', '$state', 'menuItems'];

    function MenuItemsController ($scope, $location, $state, menuItems) {
        var vm = this;
        vm.account = null;

        vm.isAuthenticated = null;
        vm.menuItems = menuItems;

        vm.noRecordsFound = (vm.menuItems === undefined || vm.menuItems.length === 0);
    }
})();
