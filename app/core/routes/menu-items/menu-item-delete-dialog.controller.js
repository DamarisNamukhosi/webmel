(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuItemDeleteDialogController',MenuItemDeleteDialogController);

    MenuItemDeleteDialogController.$inject = ['$uibModalInstance', 'entity', "MenuItemsService"];

    function MenuItemDeleteDialogController($uibModalInstance, entity, MenuItemsService) {
      var vm = this;

      vm.menuItem = entity;
      vm.clear = clear;
      vm.confirmDelete = confirmDelete;

      function clear() {
        $uibModalInstance.dismiss("cancel");
      }

      function confirmDelete(menuItem) {
        menuItem.contentStatus = "DELETED";
        MenuItemsService.update(menuItem, function() {
          $uibModalInstance.close(true);
        });
      }
    }
})();
