(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('MenuItemsService', MenuItemsService);

    MenuItemsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function MenuItemsService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/menu-items'
            },
            'getMenuItemsByMenu': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/menu-items/filter-by-menu/:id'
            },
            'get': {
                method: 'GET',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                },
                isArray: false,
                url: URLS.BASE_URL + 'contentservice/api/menu-items/:id'
            }, 
            'update': {
                method: 'PUT',
                headers : {
                'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menu-items'
            }, 
            'create': {
                method: 'POST',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menu-items'
            },
            'delete': {
                method: 'DELETE',
                headers : {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/menu-items/:id'
            }
        });
    }
})();
