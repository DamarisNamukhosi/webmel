(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("MenuItemGalleriesController", MenuItemGalleriesController);

  MenuItemGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService"
  ]; //

  function MenuItemGalleriesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    vm.albums = AlbumService.getAlbums({ uuid: vm.entity.uuid });

    vm.previousState = previousState.name;

    var permissions = $localStorage.current_user_permissions;

    // vm.previousState =
    //   permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";
  }
})();
