
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuItemDialogController', MenuItemDialogController);

    MenuItemDialogController.$inject = ['$timeout', '$scope', '$stateParams','$localStorage', '$uibModalInstance', 'entity', 'MenuItemsService', 'GeneralMenuItemTypesService'];

    function MenuItemDialogController ($timeout, $scope, $stateParams,$localStorage, $uibModalInstance, entity, MenuItemsService, GeneralMenuItemTypesService) {
        var vm = this;

        vm.menuItem = entity;
        
        vm.clear = clear;
        vm.save = save;
        vm.menuItemTypes = GeneralMenuItemTypesService.query();
        
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.menuItem.id !== null) {
                MenuItemsService.update(vm.menuItem, onSaveSuccess, onSaveError);
            } else {
                MenuItemsService.create(vm.menuItem, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
