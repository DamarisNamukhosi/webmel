(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("menu-items", {
        parent: "app",
        url: "/menu-items",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menu-items/menu-items.html",
            controller: "MenuItemsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          menuItems: [
            "$stateParams",
            "$localStorage",
            "MenuItemsService",
            function($stateParams, $localStorage, MenuItemsService) {
              return MenuItemsService.query().$promise;
            }
          ]
        }
      })
      .state("menu-items.new", {
        parent: "menu-items",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "facilities.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menu-items/menu-item-dialog.html",
                controller: "MenuItemDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      notes: null,
                      description: null,
                      order: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      menuId: $stateParams.id,
                      menuName: null,
                      generalMenuItemTypeId: null,
                      generalMenuItemTypeName: null,
                      servingOptionsGroupId: null,
                      servingOptionsGroupName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menu-items", null, { reload: "menu-items" });
                },
                function() {
                  $state.go("menu-items");
                }
              );
          }
        ]
      })
      .state("menu-item-detail", {
        parent: "menu-items",
        url: "/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.menu.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menu-items/menu-item-detail.html",
            controller: "MenuItemDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenuItemsService",
            function($stateParams, MenuItemsService) {
              return MenuItemsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "menu-items",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-item-detail.image", {
        parent: "menu-item-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menu-items/image-upload-dialog.html",
                controller: "MenuItemImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  menuItem: [
                    "$stateParams",
                    "MenuItemsService",
                    function($stateParams, MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-detail", null, {
                    reload: "menu-item-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-item-detail.edit", {
        parent: "menu-item-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menu-items/menu-item-dialog.html",
                controller: "MenuItemDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenuItemsService",
                    function(MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-detail", null, {
                    reload: "menu-item-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-items.edit", {
        parent: "menu-items",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/menu-items/menu-dialog.html",
                controller: "MenuDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenuItemsService",
                    function(MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-items", null, { reload: "menu-items" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-item-detail.delete", {
        parent: "menu-item-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/menu-items/menu-item-delete-dialog.html",
                controller: "MenuItemDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "MenuItemsService",
                    function(MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menus", null, { reload: "menus" });
                },
                function() {
                  $state.go("menus");
                }
              );
          }
        ]
      })
      .state("menu-item-galleries", {
        parent: "app",
        url: "/menu-items/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menu-items/menu-item-galleries.html",
            controller: "MenuItemGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenuItemsService",
            function($stateParams, MenuItemsService) {
              return MenuItemsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-item-galleries.new", {
        parent: "menu-item-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "MenuItemsService",
                    function(MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-galleries", null, {
                    reload: "menu-item-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-item-galleries.delete", {
        parent: "menu-item-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("menu-item-galleries", null, {
                    reload: "menu-item-galleries"
                  });
                },
                function() {
                  $state.go("menu-item-galleries");
                }
              );
          }
        ]
      })
      .state("menu-item-album-detail", {
        parent: "menu-item-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/menu-items/menu-item-album-detail.html",
            controller: "MenuItemAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MenuItemsService",
            function($stateParams, MenuItemsService) {
              return MenuItemsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "menu-item-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("menu-item-album-detail.edit", {
        parent: "menu-item-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "menu-item-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AlbumService",
                    function(AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-album-detail", null, {
                    reload: "menu-item-album-detail"
                  });
                },
                function() {
                  $state.go("menu-item-album-detail");
                }
              );
          }
        ]
      })
      .state("menu-item-album-detail.makeCoverImage", {
        parent: "menu-item-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-album-detail", null, {
                    reload: "menu-item-album-detail"
                  });
                },
                function() {
                  $state.go("menu-item-album-detail");
                }
              );
          }
        ]
      })
      .state("menu-item-album-detail.deleteImage", {
        parent: "menu-item-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-album-detail", null, {
                    reload: "menu-item-album-detail"
                  });
                },
                function() {
                  $state.go("menu-item-album-detail");
                }
              );
          }
        ]
      })
      .state("menu-item-album-detail.upload", {
        parent: "menu-item-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("menu-item-album-detail", null, {
                    reload: "menu-item-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("menu-item-detail.addLabels", {
        parent: "menu-item-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "MenuItemsService",
                    function (MenuItemsService) {
                      return MenuItemsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("menu-item-detail", null, {
                  reload: "menu-item-detail"
                });
              },
              function () {
                $state.go("menu-item-detail");
              });
          }
        ]
      })
      .state("menu-item-detail.deleteLabel", {
        parent: "menu-item-detail",
        url: "/{itemToDelete}/delete-label",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
              function () {
                $state.go("menu-item-detail", null, {
                  reload: "menu-item-detail"
                });
              },
              function () {
                $state.go("menu-item-detail");
              }
              );
          }
        ]
      });
  }
})();
