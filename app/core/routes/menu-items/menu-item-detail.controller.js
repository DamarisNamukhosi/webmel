(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuItemDetailController', MenuItemDetailController);

    MenuItemDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', '$localStorage', 'ObjectLabelsService', 'URLS'];

    function MenuItemDetailController($scope, $rootScope, $stateParams, previousState, entity, $localStorage, ObjectLabelsService, URLS) {
        var vm = this;

        vm.menuItem = entity;
        vm.previousState = previousState.name;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.menuItem.uuid;

        vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.menuItem.uuid });
        console.log('object labels...');
    }
})();
