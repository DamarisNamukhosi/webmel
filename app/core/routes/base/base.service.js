(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('BasesService', BasesService);

    BasesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function BasesService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getBase': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/bases'
        },
        'getBasesByOrganizationId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/bases/filter-by-organisation/:id?sort=order%2Casc'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'contentservice/api/bases/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/bases'
        },
        'updateList': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/bases/save-list'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/bases'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/bases/:id'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        }
      });
    }
})();
