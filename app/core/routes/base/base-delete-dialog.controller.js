(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('BaseDeleteDialogController', BaseDeleteDialogController);

  BaseDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'BasesService'];

  function BaseDeleteDialogController($uibModalInstance, entity, BasesService) {
    var vm = this;

    vm.base = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(base) {
      console.log('changing base contentStatus to DELETED');
      vm.base.contentStatus = "DELETED";
      BasesService.update(base,
        function () {
          $uibModalInstance.close(true);
        });
    }
  }
})();
