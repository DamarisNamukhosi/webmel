(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicles-base", {
        parent: "app",
        url: "/base",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/base/base.html",
            controller: "BasesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          bases: [
            "$stateParams",
            "$localStorage",
            "BasesService",
            function ($stateParams,$localStorage, BasesService) {
              return BasesService.getBasesByOrganizationId({id: $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicles-base.new', {
        parent: 'vehicles-base',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/base/base-dialog.html',
                controller: 'BaseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: "DRAFT",
                      description: null,
                      id: null,
                      notes: null,
                      locationName: null,
                      locationId: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      statusReason: null,
                      type: "TRANSPORT",
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('vehicles-base', null, {
                    reload: 'vehicles-base'
                  });
                },
                function () {
                  $state.go('vehicles-base');
                }
              );
          }
        ]
      })
      .state("vehicles-base.edit", {
        parent: "vehicles-base",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/base/base-dialog.html",
                controller: "BaseDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "BasesService",
                    function (BasesService) {
                      return BasesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-base", null, {
                    reload: "vehicles-base"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-base.delete", {
        parent: "vehicles-base",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/base/base-delete-dialog.html",
                controller: "BaseDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "BasesService",
                    function (BasesService) {
                      return BasesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-base", null, { reload: "vehicles-base" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      });

  }
})();
