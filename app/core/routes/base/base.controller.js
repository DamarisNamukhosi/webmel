(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('BasesController', BasesController);

  BasesController.$inject = ['$scope', '$location', '$state', 'bases', 'URLS', 'BasesService'];

  function BasesController($scope, $location, $state, bases, URLS, BasesService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_url = URLS.PROFILE_IMAGE_URL;
    vm.noRecordsFound = false;
    vm.bases = bases;
    console.log('bases....');
    console.log(vm.bases);

    if (vm.bases.length === 0) {
      vm.noRecordsFound = true;
    }

    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.bases, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.bases");
      console.log(vm.bases);


      // call update
      BasesService.updateList(vm.bases, onSaveSuccess, onSaveError);

    }
    function onSaveSuccess() {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("Update successful");
    }

    function onSaveError() {
      console.log("Update not successfull");
    }

  }
})();
