
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('BaseDialogController', BaseDialogController);

  BaseDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BasesService', 'LocationsService'];

  function BaseDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, BasesService, LocationsService) {
    var vm = this;

    vm.base = entity;
    vm.clear = clear;
    vm.save = save;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.base.id !== null) {
        BasesService.update(vm.base, onSaveSuccess, onSaveError);
      } else {
        BasesService.create(vm.base, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          });
        });
    }

    function onLocationSelected($item, $model) {
      vm.base.locationId = $item.id;
      vm.base.locationName = $item.name;
      }

  }
})();

