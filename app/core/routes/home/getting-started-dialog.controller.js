(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'GettingStartedDialogController',
            GettingStartedDialogController
        );

    GettingStartedDialogController.$inject = [
        '$scope',
        '$rootScope',
        '$stateParams',
        '$localStorage',
        '$uibModalInstance'
    ];

    function GettingStartedDialogController(
        $scope,
        $rootScope,
        $stateParams,
        $localStorage,
        $uibModalInstance
    ) {
        var vm = this;

        vm.clear = clear;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
