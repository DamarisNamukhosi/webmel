(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('HomeService', HomeService);

  HomeService.$inject = ['$http', '$localStorage', 'URLS'];

  function HomeService($http, $localStorage, URLS) {

    var serve = {};
    var user = $localStorage.user;

    serve.getOrganisationTypes = function () {
      return $http({
        method: 'GET',
        url: URLS.BASE_URL + 'contentservice/api/general-organisation-types',
        headers: {
          'Authorization': 'Bearer ' + user
        }
      });
    };

    serve.PostNewUserLocation = function (data) {
      return $http({
        method: 'POST',
        url: URLS.BASE_URL + 'contentservice/api/organisations/register',
        headers: {
          'Authorization': 'Bearer ' + user
        },
        data: data
      })
    };

    serve.getAccountData = function () {
      return $http({
        method: 'GET',
        url: URLS.BASE_URL + 'contentservice/api/locations',
        headers: {
          'Authorization': 'Bearer ' + user
        }
      })
    };

    return serve;

  }
})();
