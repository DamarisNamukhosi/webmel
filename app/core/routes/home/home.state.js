(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('home', {
                parent: 'app',
                url: '/',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'My Organizations'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/home/home.html',
                        controller: 'HomeController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {}
            })
            .state('home.edit', {
                parent: 'home',
                url: 'edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$localStorage',
                    '$state',
                    '$uibModal',
                    function($stateParams, $localStorage, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/organizations/organization-dialog.html',
                                controller: 'OrganizationDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'OrganizationsService',
                                        function(OrganizationsService) {
                                            return OrganizationsService.get({
                                                id:
                                                    $localStorage
                                                        .current_organisation.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('home', null, {
                                        reload: true
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('home.new', {
                parent: 'home',
                url: '/register',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/organizations/organization-register-dialog.html',
                                controller:
                                    'OrganizationRegisterDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            id:null,
                                            abbreviation: null,
                                            altitude: null,
                                            brief: null,
                                            description: null,
                                            langKey: 'en',
                                            latitude: null,
                                            longitude: null,
                                            name: null,
                                            organisationTypeId: null,
                                            parentLocationId: null,
                                            parentOrganisationId:$localStorage.current_organisation.id
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('home', null, {
                                        reload: 'home'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('home.gettingStarted', {
                parent: 'home',
                url: 'getting-started',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/home/getting-started-dialog.html',
                                controller: 'GettingStartedDialogController',
                                controllerAs: 'vm',
                                size: 'md'
                            })
                            .result.then(
                                function() {
                                    $state.go('home', null, {
                                        reload: 'home'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            });
    }
})();
