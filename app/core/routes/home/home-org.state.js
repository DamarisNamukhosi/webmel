(function () {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('org', {
            parent: 'app',
            url: '/add-organisation',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/home/org.html',
                    controller: 'OrganisationController',
                    controllerAs: 'vm'
                }
            }
        })
            .state('org.type', {
                url: '/type',
                templateUrl: 'core/routes/home/org-type.html'
            })
            .state('org.details', {
                url: '/details',
                templateUrl: 'core/routes/home/org-details.html'
            })
            .state('org.map', {
                url: '/map',
                templateUrl: 'core/routes/home/org-map.html'
            });
    }
})();
