(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('OrganisationController', OrganisationController);

  OrganisationController.$inject = ['$localStorage', 'HomeService', '$timeout', 'AccountService', '$state', 'URLS'];

  function OrganisationController($localStorage, HomeService, $timeout, AccountService, $state, URLS) {
    var vm = this;

    vm.formData = {};

    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
    vm.pauseLoading = true;
    // console.log("Starting a timer to wait for 2 seconds before the map will start loading");

    $timeout(function () {
      console.debug("Showing the map. The google maps api should load now.");
      vm.pauseLoading = false;
    }, 2000);

    vm.latlng = [0.8349313860427184, 37.4853515625];
    vm.lat = 0.8349313860427184;
    vm.long = 37.4853515625;
    vm.IsSaving = false; //will disable submit button and show loading gif

    vm.organisation_types = $localStorage.organisation_types;

    vm.getpos = function (event) {
      vm.latlng = [event.latLng.lat(), event.latLng.lng()];
      // console.log(event.latLng);
      vm.lat = event.latLng.lat();
      vm.long = event.latLng.lng();
    };

    vm.save = function () {
      vm.IsSaving = true;
      vm.formData.lat = vm.lat;
      vm.formData.long = vm.long;
      var data = {
        "abbreviation": vm.formData.name,
        "brief": vm.formData.brief,
        "description": vm.formData.description,
        "latitude": vm.formData.lat,
        "longitude": vm.formData.long,
        "name": vm.formData.name,
        "organisationTypeId": vm.formData.organisation_type,
        "parentOrganisationId": $localStorage.current_organisation.id
        //   "parentLocationId": 1,
      };

      HomeService.PostNewUserLocation(data)
        .then(function (data) {
          onSaveSuccess(data);
        }, function (error) {
          onSaveError(error);
        });
    };

    function onSaveSuccess(result) {
      console.log("successful return: " + result);
      vm.IsSaving = false;
      //reload the organisation data
      AccountService.getAccountData()
        .then(function (response) {
          console.log(response.data);
          $localStorage.account = response.data;
          $state.go('home');
        });
    }

    function onSaveError(error) {
      console.log("error during save: " + error);
      vm.IsSaving = false;
    }

  }
})();
