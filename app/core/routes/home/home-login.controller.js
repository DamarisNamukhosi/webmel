(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('HomeLoginController', HomeLoginController);

    HomeLoginController.$inject = ['$scope', '$state', '$uibModalInstance', 'HomeService'];

    function HomeLoginController ($scope, $state, $uibModalInstance, HomeService) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.clear = clear;
        vm.save = save;
        
        
      function clear () {
        $uibModalInstance.dismiss('cancel');
      }

      function save () {
        vm.isSaving = true;
        HomeService.login(vm.data, onSaveSuccess, onSaveError);
      }

      function onSaveSuccess (result) {
        $scope.$emit('arApp:consent', result);
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }

      function onSaveError () {
        vm.isSaving = false;
      }

    }
})();
