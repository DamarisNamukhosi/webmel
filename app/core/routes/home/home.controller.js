(function () {
  'use strict';

  angular.module('webApp').controller('HomeController', HomeController);

  HomeController.$inject = [
    '$localStorage',
    '$state',
    '$scope',
    'AccountService',
    'HomeService',
    'OrganizationsService',
    'NavbarService',
    'ProfessionalsService',
    'ContractsService',
    'VehiclesService',
    'MarketsService',
    'GeneralServicesService',
    'URLS'
  ];

  function HomeController(
    $localStorage,
    $state,
    $scope,
    AccountService,
    HomeService,
    OrganizationsService,
    NavbarService,
    ProfessionalsService,
    ContractsService,
    VehiclesService,
    MarketsService,
    GeneralServicesService,
    URLS
  ) {
    //injected values
    var vm = this;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;


    //declare functions
    vm.initUserPermissions = initUserPermissions;
    //our initialization
    vm.displayPage = false;
    vm.isAuthenticated = null;
    vm.organisationTypes = null;
    vm.contracts_count = 0;
    vm.userPermissions = false;
    //call functions needed to initialize data

    vm.isLoading = true;

    initUserPermissions();

    if (!$localStorage.hasOwnProperty('account') ||
      $localStorage.account === null
    ) {
      AccountService.getAccountData().then(
        function (response) {
          // console.log(response.data);
          $localStorage.account = response.data;
          $localStorage.current_organisation =
            response.data.organisation;
          $localStorage.current_managed_organisations =
            response.data.managedOrganisations;
          $localStorage.current_user_permissions =
            response.data.authorities;
          $localStorage.current_location_id =
            response.data.locationId;
          vm.account = response.data;
          //vm.userPermissions= response.

          HomeService.getOrganisationTypes().then(function (response) {
            $localStorage.organisation_types = response.data;
          });

          // response.data.organisation.id

          $state.reload();
          vm.displayPage = true;
        },
        function (error) {
          vm.displayPage = false;
          $localStorage.account = error;
        }
      );
    } else {
      vm.displayPage = true;
    }

    //get managed organisations
    if ($localStorage.hasOwnProperty('account')) {
      vm.user = $localStorage.account;
      vm.currentOrganization = $localStorage.current_organisation;
      console.log("vm.currentOrganization");
      console.log(vm.currentOrganization);
      if(vm.currentOrganization == undefined || vm.currentOrganization == null){
        // $state.go(
        //       'login', {}, {
        //         reload: true
        //       }
        //     );
      }
      vm.organizations = $localStorage.current_managed_organisations;
      vm.parentOrganization = $localStorage.account.organisation;
      if ($localStorage.current_immediate_parent_organisation != undefined) {
        vm.immediateParentOrganization =
          $localStorage.current_immediate_parent_organisation;
      }
      if ($localStorage.current_organisation != undefined && $localStorage.current_organisation != null) {
        vm.managedOrganisations = OrganizationsService.getManagedOrganizations({
          org_id: $localStorage.current_organisation.id,
          relationship_type: '1'
        });
      }
      vm.account = $localStorage.account;
      if (vm.currentOrganization != undefined && vm.currentOrganization != null) {

        OrganizationsService.getManagedOrganizations({
          org_id: vm.currentOrganization.id,
          relationship_type: 1
        },
          function (response) {
            vm.managedOrganisations = response;
          }
        );
      }
      // target=:target&generalService=:service&supplier=:supplier&year=:year&name=:name
      var target = null;
      var generalService = 1;
      var supplier = null;
      var year = null;
      var name = null;
      var issuer = null;
      if (vm.currentOrganization != undefined && vm.currentOrganization != null) {
        if (vm.currentOrganization.organisationTypeId === '3') {
          target = vm.currentOrganization.id;
        }

        if (vm.currentOrganization.organisationTypeId === '3') {
          target = vm.currentOrganization.id;
        } else {
          supplier = vm.currentOrganization.id;
        }

        if (vm.currentOrganization.organisationTypeId !== 3) {

          vm.isLoading = true;

          ContractsService.getFiltered({
            target: target,
            generalService: generalService,
            supplier: supplier,
            issuer: issuer,
            showDeleted: false,
            year: year,
            name: name
          },
            function (response) {
              vm.contracts = response;
              console.log(vm.contracts);
              vm.contracts_count = response.length;

              vm.isLoading = false;
            },
            function () {
              vm.isLoading = false;
            }
          );

        }
      }

      if ($localStorage.current_user_permissions != undefined && $localStorage.current_user_permissions != null) {
        if ($localStorage.current_user_permissions.includes("T_OPERATOR_USER")) {
          ProfessionalsService.getProfessionalsByOrganizationId({
            id: vm.currentOrganization.id
          },
            function (response) {
              vm.professionals_count = response.length;
            }
          );

          VehiclesService.getVehiclesByOrganizationId({
            id: vm.currentOrganization.id
          },
            function (response) {
              vm.fleet_count = response.length;
            }
          );

          MarketsService.getByOrganisation({
            id: vm.currentOrganization.id
          },
            function (response) {
              // console.log('markets');
              // console.log(response.length);
              vm.markets_count = response.length;
            }
          );
          ContractsService.getByChain({
            id: vm.currentOrganization.id
          },
            function (response) {
              vm.contract_groups = response;
              vm.isLoading = false;
            },
            function () {
              vm.isLoading = false;
            }
          );


        }
      }
    } // end if

    vm.switchAccount = function (orgId) {
      console.log('id: ' + orgId);
      console.log($localStorage.user);

      var data = NavbarService.switchOrganization({
        id: orgId
      }).$promise.then(
        function (response) {
          // console.log(response);
          //update location_id
          $localStorage.current_location_id = response.locationId;
          //update current_organisation
          $localStorage.current_organisation = response.organisation;
          //update current_organisation
          $localStorage.current_managed_organisations = OrganizationsService.getManagedOrganizations({
            org_id: orgId,
            relationship_type: '1'
          });
          //update current_user_permissions
          $localStorage.current_user_permissions =
            response.authorities;

          $localStorage.current_immediate_parent_organisation =
            response.immediateParent;

          //go to home page
          if ($state.includes('home')) {
            $state.reload();
          } else {
            $state.go(
              'home', {}, {
                reload: true
              }
            );
          }
        },
        function (error) {
          console.log(error);
        }
      );
    };

    function initUserPermissions() {

      if ($localStorage.current_user_permissions !== undefined) {
        if ($localStorage.current_user_permissions.length > 0) {
          console.log($localStorage.current_user_permissions);
          angular.forEach($localStorage.current_user_permissions, function (permissions) {
            if (permissions == "MASTER_USER" || permissions == "MANAGER_USER") {
              vm.userPermissions = true;
              vm.managerU
            }
          })
        } else {
          //if user_permissions is one value
          vm.userPermissions = $localStorage.current_user_permissions == "MASTER_USER" || $localStorage.current_user_permissions == "MANAGER_USER" ? true : false;

        }
      }

    }
  }
})();
