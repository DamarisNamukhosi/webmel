(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "DimensionSetsCreateDialogController",
      DimensionSetsCreateDialogController
    );

  DimensionSetsCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "DimensionSetsService",
    "GeneralServicesDimensionsService",
    "entity",
    "dimensionTypes",
    "service",
  ];

  function DimensionSetsCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    DimensionSetsService,
    GeneralServicesDimensionsService,
    entity,
    dimensionTypes,
    service
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    vm.service = service;
    vm.toggleDefault = toggleDefault;
    vm.generalServiceId = vm.service.generalServiceId;

    vm.entity.serviceId = vm.service.id;


    console.log(vm.entity);

    vm.dimensionTypes = dimensionTypes;
    vm.showObjectType = true;
    vm.setDimensionTypes = ["BASIC", "SUPPLEMENT", "EXCLUSIVE", "POLICY", "DERIVED"];
    // vm.years = [];
    vm.objectTypes = [];
    vm.initialSet = [{
      type: null,
      id: null,
      number: 1
    }];

    vm.yearChange = yearChange;
    vm.objectChange = objectChange;
    vm.exclusiveContractParameters = [];
    vm.contractYears = [];
    vm.myDim = [];



    initExclusiveDimensions();
    yearChange();
    initYears();

    vm.addDimension = function() {
      vm.initialSet.push({
        type: null,
        id: null,
        number: vm.initialSet.length + 1,
        hasDefault: false
      });
    };

    vm.removeDimension = function($index) {
   
      vm.initialSet.splice($index, 1);

    };

    vm.sortableOptions = {
      "ui-floating": true,
      update: function(e, ui) {
        //  console.log(ui);
      }
    };

    vm.changed = function(index) {

    }

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      vm.number = 1;
      if(vm.entity.id == null){

      angular.forEach(vm.initialSet, function(item) {
        var temp = {
          dimensionId: item.id,
          dimensionName: null,
          id: null,
          number: item.number,
          setId: null,
          setName: vm.entity.name,
          type: item.type,
          uuid: null,
          hasDefault: item.hasDefault
        };

        // vm.number = vm.number + 1;

        // temp.number = vm.number;
        vm.entity.dimensionDTOList.push(temp);
      });
        DimensionSetsService.createFull(vm.entity, onSaveSuccess, onSaveError);

    }else{
      DimensionSetsService.update(vm.entity, onSaveSuccess, onSaveError);

    }
    }
      console.log(vm.entity.dimensionDTOList);
      vm.entity
      console.log(vm.entity);


    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function initYears() {
      console.log("init years");
      var currentYear = new Date().getFullYear();
      var totalYears = 7; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = (currentYear + i) - numberOfYearsBack;
        vm.contractYears.push(year);
      }
    }

    function initExclusiveDimensions() {

      //logic to init Exclusive DIms
      console.log($stateParams);
      if (vm.service.generalServiceId == 6) {
        vm.entity.objectType = "PERSON"
        vm.showObjectType = true;
        vm.objectTypes = ["PERSON", "VEHICLE"];
        vm.entity.name = vm.entity.year +" " + vm.entity.objectType  + " Default Rates Structure"; 

      }else{
        vm.entity.objectType = "PERSON"
        vm.showObjectType = false;
        vm.entity.name = vm.entity.year + " Default Rates Structure"; 

        //vm.objectTypes = ["PERSON", "VEHICLE"];

      }

    }

    function yearChange() {
      console.log("yearChange");
      vm.entity.name = vm.entity.year + " Default Rates Structure"; 
      // getSeasonGroups();
      initContractResolutionConfigs();
    }

    function objectChange() {
      console.log("objectChange");
      vm.entity.name = vm.entity.year +" " + vm.entity.objectType  + " Default Rates Structure"; 

      // getSeasonGroups();
      initContractResolutionConfigs();
    }


    function initContractResolutionConfigs() {
      console.log(vm.entity.year);
      // console.log(vm.entity.dimensionType);
      console.log($stateParams);
      if (vm.service.generalServiceId == 6) {
        vm.entity.name = vm.entity.year +" " + vm.entity.objectType  + " Default Rates Structure"; 

      }else{
        vm.entity.name = vm.entity.year + " Default Rates Structure"; 

      }
      vm.exclusiveContractParameters = [];
      vm.myDim = [];
      DimensionSetsService.getContractResolutionConfig({
        id: vm.entity.serviceId,
        year: vm.entity.year,
        type: vm.entity.objectType
      }).
      $promise.then(function(response) {
        console.log(response);
        angular.forEach(response, function(dim) {
          if (dim.existenceDTOList.length > 0) {
            dim.selectedValue = dim.existenceDTOList[0].id;
          }
        });
        vm.exclusiveContractParameters = response;
        console.log("exclusiveContractParameters");
        console.log(vm.exclusiveContractParameters);
        console.log(vm.exclusiveContractParameters.length);
        if(vm.exclusiveContractParameters.length  > 0){
          removeDimensionType();

        }else{
          vm.myDim =  vm.dimensionTypes;
        }
      });
    }


    function removeDimensionType(dimType) {
      console.log("called...");
      vm.myDim = vm.dimensionTypes;
      angular.forEach(vm.exclusiveContractParameters, function(dimType) {
        console.log("reach loop");

        angular.forEach(vm.dimensionTypes, function(dimenType) {
          if (dimType.dimensionType == dimenType.type) {

            vm.myDim.splice(vm.myDim.indexOf(dimenType),1);

          }
        });

      });
      console.log("vm.myDim");
      console.log(vm.myDim);
    }
    function serviceChange(serviceId){
      console.log("service change");
      console.log(serviceId);
      angular.forEach(vm.generalServices, function(genSvc){
        if(serviceId == genSvc.id){
          vm.entity.name = $localStorage.current_organisation.abbreviation + " " + genSvc.name;
        }
      })

    }

    function toggleDefault(check){
      console.log(check); 
      console.log("toggle");
      console.log(vm.entity.isDefault);
      if(vm.entity.isDefault){
        if (vm.service.generalServiceId == 6) {
          vm.entity.name = vm.entity.year +" " + vm.entity.objectType  + " Default Rates Structure"; 
  
        }else{
          vm.entity.name = vm.entity.year + " Default Rates Structure"; 
  
        }
      }else{
        if (vm.service.generalServiceId == 6) {
          vm.entity.name = vm.entity.year +" " + vm.entity.objectType  + " Rates Structure"; 
  
        }else{
          vm.entity.name = vm.entity.year + " Rates Structure"; 
  
        }
        
      }
    }


  }
})();
