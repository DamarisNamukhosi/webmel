(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('DimensionSetsService', DimensionSetsService);

  DimensionSetsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function DimensionSetsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getById': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets/:id'
        },
        'getSetDimensionExistenciesBySetId': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/set-dimensions/get-full-by-set/:id?year=:year&seasonGroupId=:seasonGroupId'
        },
        'filterBySet': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/set-dimensions/filter-by-set/:id'
        },
        'getByServiceId': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets/filter-by-service/:id'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets'
        },
        'create': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets'
        },
        'createFull': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets/create-full'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/service-dimension-sets/:id'
        },
        'getAllDimensions': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/dimensions'
        },
        'updateList': {
        method: 'PUT',
          headers : {
          'Authorization': 'Bearer ' + $localStorage.user
        },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/set-dimensions/update-list'
        },
        'getContractResolutionConfig': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          //http://51.15.233.87:8080/costingservice/api/contract-resolution-configs
          url: URLS.BASE_URL + 'costingservice/api/set-dimensions/get-exclusives/:id?year=:year&objectType=:type'
        },
        'getExistencies': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          //http://51.15.233.87:8080/costingservice/api/contract-resolution-configs
          url: URLS.BASE_URL + 'costingservice/api/set-dimensions/get-existences/:orgId?dimensionType=:dimensionType&year=:year&seasonGroupId=:seasonGroupId'
        }
      });
  }
})();
