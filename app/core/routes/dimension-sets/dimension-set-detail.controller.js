(function () {
    "use strict";

    angular
        .module("webApp")
        .controller("DimensionSetDetailController", DimensionSetDetailController);

    DimensionSetDetailController.$inject = [
        "$scope",
        "$rootScope",
        "$stateParams",
        "$localStorage",
        "previousState",
        "entity",
        "dimensionSet",
        "contract",
        "URLS",
        'rates'
    ];

    function DimensionSetDetailController(
        $scope,
        $rootScope,
        $stateParams,
        $localStorage,
        previousState,
        entity,
        dimensionSet,
        contract,
        URLS,
        rates
    ) {
        var vm = this;

        vm.entity = entity;
        vm.previousState = previousState.name;
        vm.dimensionSet = dimensionSet;
        console.log('vm.dimensionSet..');
        console.log(vm.dimensionSet);
        vm.rates = rates;
        console.log('vm.rates..');
        console.log(vm.rates);
       
        vm.ratesFound = true;
        console.log(vm.ratesFound);
        if (vm.rates.length === 0) {
            vm.ratesFound = false;
        }
        vm.service = entity;
        console.log('vm.service..');
        console.log(vm.service);

        vm.contract = contract;
        console.log('vm.contract..');
        console.log(vm.contract);
        $localStorage.setId = vm.contract.dimensionSetId;
        console.log('$localStorage.setId ');
        console.log($localStorage.setId );
        
    }
})();
