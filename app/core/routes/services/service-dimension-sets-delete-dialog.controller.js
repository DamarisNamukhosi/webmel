(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "ServiceDimensionDeleteController",
      ServiceDimensionDeleteController
    );

  ServiceDimensionDeleteController.$inject = [
    "$uibModalInstance",
    "entity",
    "DimensionSetsService"
  ];

  function ServiceDimensionDeleteController(
    $uibModalInstance,
    entity,
    DimensionSetsService
  ) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirmDelete() {
      entity.contentStatus = "DELETED";
      DimensionSetsService.update(entity, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
