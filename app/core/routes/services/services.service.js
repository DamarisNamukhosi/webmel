(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('ServicesService', ServicesService);

    ServicesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ServicesService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getByOrganizationId': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/srvices/filter-by-organisation/:id'
          },
          'getOrganizationByServicesId': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/srvices/filter-by-general-service/:id'
          },
          'getById': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/srvices/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/srvices/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/srvices'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/srvices'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/srvices/:id'
          }
        });
    }
})();
