(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ServicesController', ServicesController);

        ServicesController.$inject = ['$scope', '$location', '$state', '$localStorage', 'URLS', 'records'];

    function ServicesController ($scope, $location, $state, $localStorage, URLS, records) {
        var vm = this;

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        vm.isAuthenticated = null;

        vm.records = records;
        console.log(vm.records);

        console.log("length: "+vm.records.length);
        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);

    }
})();
