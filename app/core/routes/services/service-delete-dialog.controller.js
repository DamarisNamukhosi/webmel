(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('ServiceDeleteDialogController', ServiceDeleteDialogController);

    ServiceDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ServicesService'];

    function ServiceDeleteDialogController($uibModalInstance, entity, ServicesService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(entity) {
            entity.contentStatus = "DELETED";
            ServicesService.update(entity,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
