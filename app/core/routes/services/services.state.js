(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('services', {
                parent: 'app',
                url: '/services',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/services/services.html',
                        controller: 'ServiceController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    services: [
                        '$stateParams',
                        '$localStorage',
                        'ServicesService',
                        function($stateParams, $localStorage, ServicesService) {
                            return ServicesService.getByOrganizationId({
                                id: $localStorage.current_organisation.id
                            }).$promise;
                        }
                    ]
                }
            })
            .state('services.new', {
                parent: 'services',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/service-dialog.html',
                                controller: 'ServiceDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            brief: null,
                                            contentStatus: 'DRAFT',
                                            description: null,
                                            generalServiceId: null,
                                            generalServiceName: null,
                                            id: null,
                                            name: null,
                                            notes: null,
                                            organisationId:
                                                $localStorage
                                                    .current_organisation.id,
                                            organisationName: null,
                                            uuid: null
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('services', null, {
                                        reload: 'services'
                                    });
                                },
                                function() {
                                    $state.go('services');
                                }
                            );
                    }
                ]
            })
            .state('service-detail', {
                parent: 'app',
                url: '/services/{id}/overview',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services Details'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/services/service-detail.html',
                        controller: 'ServiceDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ServicesService',
                        function($stateParams, ServicesService) {
                            return ServicesService.getById({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    entities: [
                        '$stateParams',
                        'DimensionSetsService',
                        'ServicesService',
                        function(
                            $stateParams,
                            DimensionSetsService,
                            ServicesService
                        ) {
                            return DimensionSetsService.getByServiceId({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'services',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('service-detail.edit', {
                parent: 'service-detail',
                url: '/{id}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/service-dialog.html',
                                controller: 'ServiceDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ServicesService',
                                        function(ServicesService) {
                                            return ServicesService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ],
                                    generalServices: [
                                        '$localStorage',
                                        'GeneralServicesService',
                                        function ( $localStorage, GeneralServicesService) {
                                          return GeneralServicesService
                                            .getByOrganizationTypeId({
                                              id: $localStorage.current_organisation.organisationTypeId
                                            })
                                            .$promise;
                                        }
                                      ]
                                },

                            })
                            .result.then(
                                function() {
                                    $state.go('service-detail', null, {
                                        reload: 'service-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('services.edit', {
                parent: 'services',
                url: '/{id}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/service-dialog.html',
                                controller: 'ServiceDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ServicesService',
                                        function(ServicesService) {
                                            return ServicesService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('services', null, {
                                        reload: 'services'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('service-detail.delete', {
                parent: 'service-detail',
                url: '/{id}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/service-delete-dialog.html',
                                controller: 'ServiceDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'ServicesService',
                                        function(ServicesService) {
                                            return ServicesService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('settings-services', null, {
                                        reload: 'settings-services'
                                    });
                                },
                                function() {
                                    $state.go('settings-services');
                                }
                            );
                    }
                ]
            })
            .state('service-detail.profile', {
                parent: 'service-detail',
                url: '/image/upload',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/image-upload/image-upload-dialog.html',
                                controller:
                                    'ServiceImageUploadDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: function() {
                                        return {
                                            file: null
                                        };
                                    },
                                    service: [
                                        'ServicesService',
                                        '$stateParams',
                                        function(
                                            ServicesService,
                                            $stateParams
                                        ) {
                                            return ServicesService.getById({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-detail', null, {
                                        reload: 'service-detail'
                                    });
                                },
                                function() {
                                    $state.go('service-detail');
                                }
                            );
                    }
                ]
            })
            .state('service-galleries', {
                parent: 'app',
                url: '/services/{id}/gallery',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'Gallery'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/services/service-galleries.html',
                        controller: 'ServiceGalleriesController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ServicesService',
                        function($stateParams, ServicesService) {
                            return ServicesService.get({ id: $stateParams.id })
                                .$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state, $) {
                            var currentStateData = {
                                name: $state.current.name || 'service-detail',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('service-galleries.new', {
                parent: 'service-galleries',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-create-dialog.html',
                                controller: 'AlbumCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'ServicesService',
                                        function(ServicesService) {
                                            return ServicesService.get({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ],
                                    album: function() {
                                        return {
                                            albumType: 'GENERAL',
                                            caption: null, //album name
                                            coverName: null, //uploaded cover image file name
                                            coverUuid: null, //uploaded cover image
                                            isDefaultAlbum: true, //put option slider
                                            name: null, //album name
                                            objectUuid: null //location uuid
                                        };
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-galleries', null, {
                                        reload: 'service-galleries'
                                    });
                                },
                                function() {
                                    $state.go('service-galleries');
                                }
                            );
                    }
                ]
            })
            .state('service-galleries.delete', {
                parent: 'service-galleries',
                url: '/{albumId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-delete-dialog.html',
                                controller: 'AlbumDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md'
                            })
                            .result.then(
                                function() {
                                    $state.go('service-galleries', null, {
                                        reload: 'service-galleries'
                                    });
                                },
                                function() {
                                    $state.go('service-galleries');
                                }
                            );
                    }
                ]
            })
            .state('service-album-detail', {
                parent: 'service-galleries',
                url: '/{albumId}/album',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'Gallery'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/services/service-album-detail.html',
                        controller: 'ServiceAlbumDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ServicesService',
                        function($stateParams, ServicesService) {
                            return ServicesService.get({ id: $stateParams.id })
                                .$promise;
                        }
                    ],
                    album: [
                        '$stateParams',
                        'AlbumService',
                        function($stateParams, AlbumService) {
                            return AlbumService.getAlbum({
                                albumId: $stateParams.albumId
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state, $) {
                            var currentStateData = {
                                name:
                                    $state.current.name || 'service-galleries',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('service-album-detail.edit', {
                parent: 'service-album-detail',
                url: '/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'service-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-dialog.html',
                                controller: 'AlbumDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        function(AlbumService) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-album-detail', null, {
                                        reload: 'service-album-detail'
                                    });
                                },
                                function() {
                                    $state.go('service-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('service-album-detail.makeCoverImage', {
                parent: 'service-album-detail',
                url: '/{imageId}/cover-image',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-change-cover-image-dialog.html',
                                controller:
                                    'AlbumChangeCoverImageDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        '$stateParams',
                                        function(AlbumService, $stateParams) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-album-detail', null, {
                                        reload: 'service-album-detail'
                                    });
                                },
                                function() {
                                    $state.go('service-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('service-album-detail.deleteImage', {
                parent: 'service-album-detail',
                url: '/{imageId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'locations-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-image-delete-dialog.html',
                                controller: 'AlbumImageDeleteDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'AlbumService',
                                        '$stateParams',
                                        function(AlbumService, $stateParams) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-album-detail', null, {
                                        reload: 'service-album-detail'
                                    });
                                },
                                function() {
                                    $state.go('service-album-detail');
                                }
                            );
                    }
                ]
            })
            .state('service-album-detail.upload', {
                parent: 'service-album-detail',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/albums/album-upload-dialog.html',
                                controller: 'AlbumUploadDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        '$stateParams',
                                        'AlbumService',
                                        function($stateParams, AlbumService) {
                                            return AlbumService.get({
                                                id: $stateParams.albumId
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-album-detail', null, {
                                        reload: 'service-album-detail'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('service-dimension-sets', {
                parent: 'app',
                url: '/services/{id}/dimension-sets',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services Details'
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/services/service-dimension-sets.html',
                        controller: 'ServiceDimensionSetsController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entities: [
                        '$stateParams',
                        'DimensionSetsService',
                        'ServicesService',
                        function(
                            $stateParams,
                            DimensionSetsService,
                            ServicesService
                        ) {
                            return DimensionSetsService.getByServiceId({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    service: [
                        'ServicesService',
                        '$stateParams',
                        function(ServicesService, $stateParams) {
                            return ServicesService.getById({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'services',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('service-dimension-sets.new', {
                parent: 'service-detail',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/dimension-sets/dimension-set-create-dialog.html',
                                controller:
                                    'DimensionSetsCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: function() {
                                        return {
                                            default: false,
                                            dimensionDTOList: [],
                                            id: null,
                                            name: null,
                                            serviceId: $stateParams.id,
                                            uuid: null,
                                            year:"2019",
                                            isDefault: true,
                                            objectType: "PERSON"
                                        };
                                    },
                                    dimensionTypes: [
                                        'ServicesService',
                                        '$stateParams',
                                        'GeneralServicesDimensionsService',
                                        'DimensionSetsService',
                                        function(
                                            ServicesService,
                                            $stateParams,
                                            GeneralServicesDimensionsService,
                                            DimensionSetsService
                                        ) {

                                            return DimensionSetsService.getAllDimensions().$promise;
                                        }
                                    ],
                                    service: [
                                        'ServicesService',
                                        '$stateParams',
                                        function(
                                            ServicesService,
                                            $stateParams
                                        ) {
                                            return ServicesService.getById({
                                                id: $stateParams.id
                                            }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-detail', null, {
                                        reload: 'service-detail'
                                    });
                                },
                                function() {
                                    $state.go('service-detail');
                                }
                            );
                    }
                ]
            })
            .state('service-dimension-sets.edit', {
              parent: 'service-detail',
              url: '/{editId}/editDim',
              data: {
                  requiresAuthentication: true,
                  authorities: [],
                  pageTitle: 'services.detail.title'
              },
              onEnter: [
                  '$stateParams',
                  '$state',
                  '$localStorage',
                  '$uibModal',
                  function($stateParams, $state, $localStorage, $uibModal) {
                      $uibModal
                          .open({
                              templateUrl:
                                  'core/routes/dimension-sets/dimension-set-create-dialog.html',
                              controller:
                                  'DimensionSetsCreateDialogController',
                              controllerAs: 'vm',
                              backdrop: 'static',
                              size: 'md',
                              resolve: {
                                  entity:[
                                    'DimensionSetsService',
                                    function(
                                        DimensionSetsService
                                    ) {

                                        return DimensionSetsService.get({id : $stateParams.editId}).$promise;
                                    }
                                ],
                                  dimensionTypes: [
                                      'ServicesService',
                                      '$stateParams',
                                      'GeneralServicesDimensionsService',
                                      'DimensionSetsService',
                                      function(
                                          ServicesService,
                                          $stateParams,
                                          GeneralServicesDimensionsService,
                                          DimensionSetsService
                                      ) {

                                          return DimensionSetsService.getAllDimensions().$promise;
                                      }
                                  ],
                                  service: [
                                      'ServicesService',
                                      '$stateParams',
                                      function(
                                          ServicesService,
                                          $stateParams
                                      ) {
                                        console.log($stateParams);
                                          return ServicesService.get({
                                              id: $stateParams.id
                                          }).$promise;
                                      }
                                  ]
                              }
                          })
                          .result.then(
                              function() {
                                  $state.go('service-detail', null, {
                                      reload: 'service-detail'
                                  });
                              },
                              function() {
                                  $state.go('service-detail');
                              }
                          );
                  }
              ]
          })
            .state('service-dimension-sets.delete', {
                parent: 'service-dimension-sets',
                url: '/{itemId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'service-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/services/service-dimension-sets-delete-dialog.html',
                                controller: 'ServiceDimensionDeleteController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        'DimensionSetsService',
                                        function(DimensionSetsService) {
                                            return DimensionSetsService.getById(
                                                {
                                                    id: $stateParams.itemId
                                                }
                                            ).$promise;
                                        }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-dimension-sets', null, {
                                        reload: 'service-dimension-sets'
                                    });
                                },
                                function() {
                                    $state.go('service-dimension-sets');
                                }
                            );
                    }
                ]
            })
            .state('service-dimension-sets.newContract', {
                parent: 'service-dimension-sets',
                url: '/services/{serviceId}/dimension-sets/{dimenId}/new',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/contracts/contract-own-dialog.html',
                                controller: 'ContractOwnDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: [
                                        '$localStorage',
                                        function($localStorage) {
                                            return {
                                                id: null,
                                                uuid: null,
                                                name: null,
                                                targetId:$localStorage.current_organisation.id,
                                                issuerId: null,
                                                type: 'CONTRACT',
                                                startDate: null,
                                                endDate: null,
                                                contentStatus: 'DRAFT',
                                                status: 'ACTIVE',
                                                marketId: null,
                                                currencyId: 2,
                                                currencyAbbreviation: null,
                                                statusReason: null,
                                                serviceId: $stateParams.id,
                                                dimensionSetId: null,
                                                dimensionSetName: null,
                                                year: null,
                                                supplierId: null
                                            };
                                        }
                                    ],
                                    markets: [
                                        '$stateParams',
                                        'MarketsService',
                                        '$localStorage',
                                        function(
                                            $stateParams,
                                            MarketsService,
                                            $localStorage
                                        ) {
                                            return MarketsService.getByOrganisation(
                                                {
                                                    id:
                                                        $localStorage
                                                            .current_organisation
                                                            .id
                                                }
                                            ).$promise;
                                        }
                                    ],
                                    currencies: [
                                        '$stateParams',
                                        'CurrenciesService',
                                        '$localStorage',
                                        function(
                                            $stateParams,
                                            CurrenciesService,
                                            $localStorage
                                        ) {
                                            return CurrenciesService.query()
                                                .$promise;
                                        }
                                    ],
                                    organizationTypes: [
                                      '$stateParams',
                                      'OrganizationTypesService',
                                      '$localStorage',
                                      function (
                                        $stateParams,
                                        OrganizationTypesService,
                                        $localStorage
                                      ) {
                                        return OrganizationTypesService.query().$promise;
                                      }
                                    ]
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('service-dimension-sets', null, {
                                        reload: 'service-dimension-sets'
                                    });
                                },
                                function() {
                                    $state.go('^');
                                }
                            );
                    }
                ]
            })
            .state('service-inclusions', {
                parent: 'service-detail',
                url: '/inclusions',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'services Details'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/services/service-detail.html',
                        controller: 'ServiceDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    entity: [
                        '$stateParams',
                        'ServicesService',
                        function($stateParams, ServicesService) {
                            return ServicesService.getById({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    entities: [
                        '$stateParams',
                        'DimensionSetsService',
                        'ServicesService',
                        function(
                            $stateParams,
                            DimensionSetsService,
                            ServicesService
                        ) {
                            return DimensionSetsService.getByServiceId({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'service-detail',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params
                                )
                            };
                            return currentStateData;
                        }
                    ]
                }
            });
    }
})();
