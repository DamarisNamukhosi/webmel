(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("ServiceDialogController", ServiceDialogController);

  ServiceDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "generalServices",
    "ServicesService",
    "GeneralServicesService"
  ];

  function ServiceDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    generalServices,
    ServicesService,
    GeneralServicesService
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.serviceChange = serviceChange;

    vm.generalServices = generalServices;

    console.log(vm.generalServices);
    $timeout(function() {
      angular.element(".form-group:eq(1)>input").focus();
    });

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      if (vm.entity.id !== null) {
        ServicesService.update(vm.entity, onSaveSuccess, onSaveError);
      } else {
        ServicesService.create(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function serviceChange(serviceId) {
      console.log("service change");
      console.log(serviceId);
      angular.forEach(vm.generalServices, function(genSvc) {
        if (serviceId == genSvc.id) {
          vm.entity.name = $localStorage.current_organisation.abbreviation + " " + genSvc.name;
        }
      })

    }
  }
})();
