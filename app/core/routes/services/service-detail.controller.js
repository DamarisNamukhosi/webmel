(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("ServiceDetailController", ServiceDetailController);

  ServiceDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "entities",
    "ObjectLabelsService",
    "$localStorage",
    "DimensionSetsService",
    "$state"
  ];

  function ServiceDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    entities,
    ObjectLabelsService,
    $localStorage,
    DimensionSetsService,
    $state
  ) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    vm.entities = entities;
    console.log(vm.entities);

    vm.orgUuid = $localStorage.current_organisation.uuid;
    vm.previousState = previousState.name;
    vm.currentSet = null;
    vm.currentSetName = null;

    vm.updateDimensionSets = updateDimensionSets;

    vm.getDimensionSets = getDimensionSets;

    vm.isSorted = false;

    vm.sortableOptions = {
      stop: function (e, ui) {
        updateRank();
      }
    };


    function getDimensionSets(id, name) {
      console.log("id");
      console.log(id);
      vm.currentSetName = name;
      DimensionSetsService.filterBySet({ id: id }).$promise.then(function (response) {
        vm.currentSet = response;
      });
    }

    function updateDimensionSets() {
      console.log("currentSet");
      console.log(vm.currentSet);

      DimensionSetsService.updateList(vm.currentSet, onSaveSuccess, onSaveError);
    }


    function updateRank() {
      var count = 1;
      vm.isSorted = true;

      angular.forEach(vm.currentSet, function (dimensionSet) {
        dimensionSet.number = count;
        count += 1;
      });
    }
    function onSaveSuccess(result) {
      vm.isSaving = false;
      DimensionSetsService.getByServiceId({id: vm.serviceId}).$promise.then(function (response) {
        vm.entities =  response;
      });
      $state.reload();
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
})();
