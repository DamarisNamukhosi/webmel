(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealTypesDialogController', MealTypesDialogController);

        MealTypesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MealTypesService', 'GeneralMealTypesService'];

    function MealTypesDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MealTypesService, GeneralMealTypesService) {
        var vm = this;

        vm.mealType = entity;
        vm.clear = clear;
        vm.save = save;

        console.log('meal-plans....');
        console.log(vm.mealType);
        //vm.MealTypes = MealTypesService.getMealTypes();
        vm.generalMealTypes = GeneralMealTypesService.query();    

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            angular.forEach(vm.generalMealTypes, function(genMealType){
                if(genMealType.id = vm.mealType.generalMealTypeId){
                    vm.mealType.order =  genMealType.order;
                    vm.mealType.generalMealTypeName = genMealType.generalMealTypeName;
                }
            });
            vm.isSaving = true;
            if (vm.mealType.id !== null) {
                MealTypesService.update(vm.mealType, onSaveSuccess, onSaveError);
            } else {
                MealTypesService.create(vm.mealType, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
