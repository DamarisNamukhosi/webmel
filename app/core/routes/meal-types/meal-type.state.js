(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('meal-types', {
            parent: 'app',
            url: '/meal-types',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'meal-types'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/meal-types/meal-types.html',
                    controller: 'MealTypesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                MealTypes: ['$stateParams', '$localStorage', 'MealTypesService', function ($stateParams, $localStorage, MealTypesService) {
                return MealTypesService.getMealTypes({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('meal-types.new', {
            parent: 'meal-types',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'profsApp.professional.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-types/meal-types-dialog.html',
                    controller: 'MealTypesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                description : null,
                                generalMealPlanId : null,
                                generalMealPlanName : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('meal-types', null, { reload: 'meal-types' });
                }, function() {
                    $state.go('meal-types');
                });
            }]
        })
        .state('meal-types-detail', {
            parent: 'app',
            url: '/meal-types/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Meal Plans Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/meal-types/meal-types-detail.html',
                    controller: 'MealTypesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'MealTypesService', function($stateParams, MealTypesService) {
                    return MealTypesService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'meal-types',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('meal-types.edit', {
            parent: 'meal-types',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-types/meal-types-dialog.html',
                    controller: 'MealTypesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: ['MealTypesService', function(MealTypesService) {
                            return MealTypesService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('meal-types', null, { reload: 'meal-types' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('meal-types.delete', {
            parent: 'meal-types',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-types/meal-types-delete-dialog.html',
                    controller: 'MealTypesDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MealTypesService', function(MealTypesService) {
                            return MealTypesService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('meal-types', null, { reload: 'meal-types' });
                }, function() {
                    $state.go('^');
                });
            }]
        })

    }
})();
