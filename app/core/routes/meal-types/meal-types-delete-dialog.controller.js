(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealTypesDeleteDialogController',MealTypesDeleteDialogController);

        MealTypesDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'MealTypesService'];

    function MealTypesDeleteDialogController($uibModalInstance, entity, MealTypesService) {
        var vm = this;

        vm.mealType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MealTypesService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
