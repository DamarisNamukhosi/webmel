(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PasswordController', PasswordController);

  PasswordController.$inject = [
    '$timeout',
    '$scope',
    '$state',
    'auth',
    'AuthService',
    '$localStorage',
    'Flash'
  ];

  function PasswordController($timeout, $scope, $state, auth, AuthService, $localStorage, Flash) {
    var vm = this;

    vm.confirm_password = '';
    vm.password = auth;
    vm.save = save;
    vm.doNotMatch = null;
    vm.error = null;
    vm.success = null;

    // Principal.identity().then(function(account) {
    //     vm.account = account;
    // });

    function save() {
      // if (vm.password !== vm.confirmPassword) {
      //     vm.error = null;
      //     vm.success = null;
      //     vm.doNotMatch = 'ERROR';
      // } else {
      //     vm.doNotMatch = null;
      //     Auth.changePassword(vm.password).then(function () {
      //         vm.error = null;
      //         vm.success = 'OK';
      //     }).catch(function () {
      //         vm.success = null;
      //         vm.error = 'ERROR';
      //     });
      // }
      vm.isSaving = true;
      console.log(vm.password.password);
      AuthService
        .updatePassword(vm.password.password)
        .then(function () {
          //success
          vm.isSaving = false;
          vm.password.password = '';
          vm.confirm_password = '';

          vm.error = null;
          vm.success = 'OK';
          vm.doNotMatch = null;

          var message = '<strong>Success!</strong> Your password has been changed.';
          Flash.create('success', message);
        })
        .catch(function () {
          //error
          vm.isSaving = false;
          vm.password.password = '';
          vm.confirm_password = '';
          var message = '<strong>Error!</strong> There was an error while trying to update your password.';
          Flash.create('danger', message);

          vm.success = null;
          vm.error = 'ERROR';
        });
    }
  }
})();
