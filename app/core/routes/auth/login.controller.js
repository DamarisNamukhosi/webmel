(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$state', '$localStorage', 'AuthService', 'data','Flash'];

    function LoginController($state, $localStorage, AuthService, data,Flash) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        // $state.reload();
        // $localStorage.account = data;

        vm.data = {};

        vm.data = data;

        vm.save = save;
        var user = $localStorage.user;

        if(user !== undefined) {
            
            $state.transitionTo('home');
        }else{
            console.log("no user");
        }


        function save() {
            vm.isSaving = true;
            var login = 'username=' + vm.data.username + '&password=' + vm.data.password + '&grant_type=password';
            AuthService.login(login)
                .then(function (response) {
                    console.log(response);
                    onSaveSuccess(response);
                }, function (response) {
                    console.log(response);
                    //console.log(response.error);
                    //console.log(response.error_description);
                    var message = '<strong>Error!</strong> There was an error while trying to login';

                    Flash.create('danger', message);

                    onSaveError(response);
                });
        }

        function onSaveSuccess(result) {
            console.log(result);
            if (result.access_token !== null) {
                $localStorage.user = result.access_token;
                $localStorage.refresh_token = result.refresh_token;
                $localStorage.expires_in = result.expires_in;
                console.log($state);
                $state.go("home", {}, { reload: true });
            }

            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function logout() {
            console.log("Logout");
        }

    }
})();
