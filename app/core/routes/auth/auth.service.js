(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$resource', '$localStorage', '$q', '$http', 'URLS'];

  function AuthService($resource, $localStorage, $q, $http, URLS) {

    var auth = {};

    auth.login = function (data) {
      var deferred = $q.defer();
      $http({
          method: 'POST',
          url: URLS.BASE_URL +'auth/oauth/token',
          headers: {
            'Authorization': 'Basic d2ViX2FwcDo=',
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          data: data
        })
        .success(function (response) {
            deferred.resolve(response);
        })
        .error(function (response) {
            deferred.reject(response);
          });

      return deferred.promise;
    }

    auth.logout = function () {
      delete $localStorage.user; //lean up the user details
      delete $localStorage.account; //clean up account info
      delete $localStorage.current_organisation; //clean up current otg
      delete $localStorage.current_user_permissions; //clean up permissions info
      delete $localStorage.current_location_id; //clean up location id
      delete $localStorage.expires_in;
      delete $localStorage.refresh_token;
      delete $localStorage.organisation_types;
      delete $localStorage.current_managed_organisations;
      delete $localStorage.current_immediate_parent_organisation;
      delete $localStorage.current_immediate_parent_organisation;
      delete $localStorage.current_managed_organisations;
      delete $localStorage.organisationTypes;
    }

    auth.checkPermissionForView = function (view) {
      if (!view.requiresAuthentication) {
        return true;
      }

      return userHasPermissionForView(view);
    };


    var userHasPermissionForView = function (view) {
      if (!auth.isLoggedIn()) {
        return false;
      }

      if (!view.permissions || !view.permissions.length) {
        return true;
      }

      return auth.userHasPermission(view.permissions);
    };


    auth.userHasPermission = function (permissions) {
      if (!auth.isLoggedIn()) {
        return false;
      }

      var found = false;
      angular.forEach(permissions, function (permission, index) {
        if ($localStorage.current_user_permissions !== undefined && $localStorage.current_user_permissions.indexOf(permission) >= 0) {
          found = true;
          return;
        }
      });

      return found;
    };

    auth.updatePassword = function (data) {
      return $http({
        method: 'POST',
        url: URLS.BASE_URL + 'auth/api/account/change_password',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        data: data
      });
    };

    auth.currentUser = function () {
      return $localStorage.user;
    };


    auth.isLoggedIn = function () {
      var deferred = $q.defer();
      if ($localStorage.hasOwnProperty('user')) {
        //console.log($localStorage.hasOwnProperty('user'));
        $q.resolve('true');
      } else {
        $q.reject('false');
      }

      return deferred.promise;
    };


    return auth;

  }
})();
