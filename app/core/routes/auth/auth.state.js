(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('login', {
            parent: 'app',
            url: '/login',
            data: {
                requiresAuthentication: false,
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/auth/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
              data: function () {
                return {
                  username: '',
                  password: '',
                  grant_type: 'password'
                };
              }
            }
        })
        .state('password', {
          parent: 'app',
          url: '/password',
          data: {
              authorities: ['ROLE_USER'],
              pageTitle: 'Password'
          },
          views: {
              'content@': {
                  templateUrl: 'core/routes/auth/password.html',
                  //templateUrl: 'core/routes/settings/password/change-password.html',
                  //controller: 'ChangePasswordController',
                  // templateUrl: 'app/account/password/password.html',
                  controller: 'PasswordController',
                  controllerAs: 'vm'
              }
          },
          resolve: {
            auth: function () {
              return {
                password: null
              };
            }
          }
      });
    }
})();
