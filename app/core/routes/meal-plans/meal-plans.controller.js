(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealPlansController', MealPlansController);

        MealPlansController.$inject = ['$scope', '$location', '$state', 'records', 'GeneralMealPlansService', '$localStorage', 'URLS', 'MealPlansService'];

    function MealPlansController($scope, $location, $state, records, GeneralMealPlansService, $localStorage, URLS, MealPlansService) {
        var vm = this;
        vm.records = records;
        console.log("records");
        console.log(vm.records);
        vm.account = $localStorage.current_organisation;
        console.log($localStorage.current_organisation);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        console.log(vm.profile_image_url);
        vm.isAuthenticated = null;

      vm.sortableOptions = {
        stop: function (e, ui) {
          vm.dragAlert = false;
          updateRank();
        }
      }

      function updateRank() {
        console.log("Updating Rank");
        var count = 1;

        angular.forEach(vm.records, function (record) {

          record.rank = count * 100;

          count = count + 1;
        });

        console.log("vm.records");
        console.log(vm.records);


        // call update
        MealPlansService.updateList(vm.records);

      }

    }
})();
