(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('MealPlansService', MealPlansService);

        MealPlansService.$inject = ['$resource', '$localStorage', 'URLS'];

    function MealPlansService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getMealPlans': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/meal-plans/filter-by-organisation/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/meal-plans/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/meal-plans'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/meal-plans'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/meal-plans/:id'
          },
          'updateList': {
            method: 'PUT',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/meal-plans/update-list'
          }
        });
    }
})();
