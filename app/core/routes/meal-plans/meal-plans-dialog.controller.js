(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealPlansDialogController', MealPlansDialogController);

        MealPlansDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MealPlansService', 'GeneralMealPlansService'];

    function MealPlansDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, MealPlansService, GeneralMealPlansService) {
        var vm = this;

        vm.mealPlan = entity;
        vm.clear = clear;
        vm.save = save;

        console.log('meal-plans....');
        console.log(vm.mealPlan);
        //vm.mealPlans = MealPlansService.getMealPlans();
        vm.generalMealPlans = GeneralMealPlansService.query();    

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.mealPlan.id !== null) {
                MealPlansService.update(vm.mealPlan, onSaveSuccess, onSaveError);
            } else {
                MealPlansService.create(vm.mealPlan, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
