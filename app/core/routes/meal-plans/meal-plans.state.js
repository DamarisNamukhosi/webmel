(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('meal-plans', {
            parent: 'app',
            url: '/meal-plans',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'meal-plans'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/meal-plans/meal-plans.html',
                    controller: 'MealPlansController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mealPlans: ['$stateParams', '$localStorage', 'MealPlansService', function ($stateParams, $localStorage, MealPlansService) {
                return MealPlansService.getMealPlans({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('meal-plans.new', {
            parent: 'meal-plans',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'profsApp.professional.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-plans/meal-plans-dialog.html',
                    controller: 'MealPlansDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                description : null,
                                generalMealPlanId : null,
                                generalMealPlanName : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('meal-plans', null, { reload: 'meal-plans' });
                }, function() {
                    $state.go('meal-plans');
                });
            }]
        })
        .state('meal-plans-detail', {
            parent: 'app',
            url: '/meal-plans/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Meal Plans Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/meal-plans/meal-plans-detail.html',
                    controller: 'MealPlansDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'MealPlansService', function($stateParams, MealPlansService) {
                    return MealPlansService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'meal-plans',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('meal-plans.edit', {
            parent: 'meal-plans',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-plans/meal-plans-dialog.html',
                    controller: 'MealPlansDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: ['MealPlansService', function(MealPlansService) {
                            return MealPlansService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('meal-plans', null, { reload: 'meal-plans' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('meal-plans.delete', {
            parent: 'meal-plans',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/meal-plans/meal-plans-delete-dialog.html',
                    controller: 'MealPlansDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MealPlansService', function(MealPlansService) {
                            return MealPlansService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('meal-plans', null, { reload: 'meal-plans' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
