(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealPlansDeleteDialogController',MealPlansDeleteDialogController);

        MealPlansDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'MealPlansService'];

    function MealPlansDeleteDialogController($uibModalInstance, entity, MealPlansService) {
        var vm = this;

        vm.mealPlan = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MealPlansService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
