(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MealPlansDetailController', MealPlansDetailController);

        MealPlansDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MealPlansService'];

    function MealPlansDetailController($scope, $rootScope, $stateParams, previousState, entity, MealPlansService) {
        var vm = this;
        
        vm.mealPlan = entity;
        vm.previousState = previousState.name;
    }
})();
