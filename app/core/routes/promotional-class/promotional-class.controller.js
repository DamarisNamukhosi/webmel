(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PromotionalClassesController', PromotionalClassesController);

  PromotionalClassesController.$inject = ['$scope', '$location', '$state', 'promotionalClasses', 'URLS', 'PromotionalClassesService'];

  function PromotionalClassesController($scope, $location, $state, promotionalClasses, URLS, PromotionalClassesService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_url = URLS.PROFILE_IMAGE_URL;
    vm.noRecordsFound = false;
    vm.promotionalClasses = promotionalClasses;
    console.log('promotionalClasses....');
    console.log(vm.promotionalClasses);

    if (vm.promotionalClasses.length === 0) {
      vm.noRecordsFound = true;
    }

    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.promotionalClasses, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.promotionalClasses");
      console.log(vm.promotionalClasses);


      // call update
      PromotionalClassesService.updateList(vm.promotionalClasses, onSaveSuccess, onSaveError);

    }
    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("Update successful");
    }

    function onSaveError() {
      console.log("Update not successfull");
    }

  }
})();
