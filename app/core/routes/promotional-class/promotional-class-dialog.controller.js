
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PromotionalClassDialogController', PromotionalClassDialogController);

  PromotionalClassDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PromotionalClassesService'];

  function PromotionalClassDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, PromotionalClassesService) {
    var vm = this;

    vm.promotionalClass = entity;
    vm.clear = clear;
    vm.save = save;

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.promotionalClass.id !== null) {
        PromotionalClassesService.update(vm.promotionalClass, onSaveSuccess, onSaveError);
      } else {
        PromotionalClassesService.create(vm.promotionalClass, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
})();

