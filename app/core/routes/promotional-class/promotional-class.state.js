(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicles-promotional-class", {
        parent: "app",
        url: "/promotional-class",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/promotional-class/promotional-class.html",
            controller: "PromotionalClassesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          promotionalClasses: [
            "$stateParams",
            "$localStorage",
            "PromotionalClassesService",
            function ($stateParams,$localStorage, PromotionalClassesService) {
              return PromotionalClassesService.getPromotionalClassesByOrganizationId({id: $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicles-promotional-class.new', {
        parent: 'vehicles-promotional-class',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/promotional-class/promotional-class-dialog.html',
                controller: 'PromotionalClassDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: "DRAFT",
                      description: null,
                      id: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      statusReason: null,
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('vehicles-promotional-class', null, {
                    reload: 'vehicles-promotional-class'
                  });
                },
                function () {
                  $state.go('vehicles-promotional-class');
                }
              );
          }
        ]
      })
      .state("vehicles-promotional-class.edit", {
        parent: "vehicles-promotional-class",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/promotional-class/promotional-class-dialog.html",
                controller: "PromotionalClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "PromotionalClassesService",
                    function (PromotionalClassesService) {
                      return PromotionalClassesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-promotional-class", null, {
                    reload: "vehicles-promotional-class"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-promotional-class.delete", {
        parent: "vehicles-promotional-class",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/promotional-class/promotional-class-delete-dialog.html",
                controller: "PromotionalClassDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "PromotionalClassesService",
                    function (PromotionalClassesService) {
                      return PromotionalClassesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-promotional-class", null, { reload: "vehicles-promotional-class" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      });

  }
})();
