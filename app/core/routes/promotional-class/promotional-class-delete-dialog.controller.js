(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PromotionalClassDeleteDialogController', PromotionalClassDeleteDialogController);

  PromotionalClassDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'PromotionalClassesService'];

  function PromotionalClassDeleteDialogController($uibModalInstance, entity, PromotionalClassesService) {
    var vm = this;

    vm.promotionalClass = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(promotionalClass) {
      console.log('changing promotional class contentStatus to DELETED');
      vm.promotionalClass.contentStatus = "DELETED";
      PromotionalClassesService.update(promotionalClass,
        function () {
          $uibModalInstance.close(true);
        });
    }
  }
})();
