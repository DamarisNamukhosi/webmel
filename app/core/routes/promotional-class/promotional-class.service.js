(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('PromotionalClassesService', PromotionalClassesService);

    PromotionalClassesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function PromotionalClassesService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getPromotionalClass': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes'
        },
        'getPromotionalClassesByOrganizationId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes/filter-by-organisation/:id?sort=order%2Casc'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes'
        },
        'updateList': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes/save-list'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/promotional-classes/:id'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        }
      });
    }
})();
