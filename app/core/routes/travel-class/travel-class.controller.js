(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TravelClassesController', TravelClassesController);

  TravelClassesController.$inject = ['$scope', '$location', '$state', 'travelClasses', 'URLS', 'TravelClassesService'];

  function TravelClassesController($scope, $location, $state, travelClasses, URLS, TravelClassesService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_url = URLS.PROFILE_IMAGE_URL;
    vm.noRecordsFound = false;
    vm.travelClasses = travelClasses;
    console.log('travelClasses....');
    console.log(vm.travelClasses);

    if (vm.travelClasses.length === 0) {
      vm.noRecordsFound = true;
    }

    vm.sortableOptions = {
      stop: function (e, ui) {
        vm.dragAlert = false;
        updateRank();
      }
    };

    function updateRank() {
      console.log("Updating Rank");
      var count = 1;

      angular.forEach(vm.travelClasses, function (record) {

        record.order = count * 100;

        count = count + 1;
      });

      console.log("vm.travelClasses");
      console.log(vm.travelClasses);


      // call update
      TravelClassesService.updateList(vm.travelClasses, onSaveSuccess, onSaveError);

    }
    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      console.log("Update successful");
    }

    function onSaveError() {
      console.log("Update not successfull");
    }
  }
})();
