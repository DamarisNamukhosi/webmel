(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicles-travel-class", {
        parent: "app",
        url: "/travel-class",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/travel-class/travel-class.html",
            controller: "TravelClassesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          travelClasses: [
            "$stateParams",
            "$localStorage",
            "TravelClassesService",
            function ($stateParams,$localStorage, TravelClassesService) {
              return TravelClassesService.getTravelClassesByOrganizationId({id: $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicles-travel-class.new', {
        parent: 'vehicles-travel-class',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  'core/routes/travel-class/travel-class-dialog.html',
                controller: 'TravelClassDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      brief: null,
                      contentStatus: "DRAFT",
                      description: null,
                      id: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: $localStorage.current_organisation.name,
                      statusReason: null,
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('vehicles-travel-class', null, {
                    reload: 'vehicles-travel-class'
                  });
                },
                function () {
                  $state.go('vehicles-travel-class');
                }
              );
          }
        ]
      })
      .state("vehicles-travel-class.edit", {
        parent: "vehicles-travel-class",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/travel-class/travel-class-dialog.html",
                controller: "TravelClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "TravelClassesService",
                    function (TravelClassesService) {
                      return TravelClassesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-travel-class", null, {
                    reload: "vehicles-travel-class"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicles-travel-class.delete", {
        parent: "vehicles-travel-class",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/travel-class/travel-class-delete-dialog.html",
                controller: "TravelClassDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "TravelClassesService",
                    function (TravelClassesService) {
                      return TravelClassesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("vehicles-travel-class", null, { reload: "vehicles-travel-class" });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      });

  }
})();
