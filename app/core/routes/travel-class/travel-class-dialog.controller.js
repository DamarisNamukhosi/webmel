
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TravelClassDialogController', TravelClassDialogController);

  TravelClassDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TravelClassesService'];

  function TravelClassDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, TravelClassesService) {
    var vm = this;

    vm.travelClass = entity;
    vm.clear = clear;
    vm.save = save;

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.travelClass.id !== null) {
        TravelClassesService.update(vm.travelClass, onSaveSuccess, onSaveError);
      } else {
        TravelClassesService.create(vm.travelClass, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
})();

