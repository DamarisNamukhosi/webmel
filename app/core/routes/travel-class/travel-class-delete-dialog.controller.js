(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('TravelClassDeleteDialogController', TravelClassDeleteDialogController);

  TravelClassDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'TravelClassesService'];

  function TravelClassDeleteDialogController($uibModalInstance, entity, TravelClassesService) {
    var vm = this;

    vm.travelClass = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(travelClass) {
      console.log('changing travel class contentStatus to DELETED');
      vm.travelClass.contentStatus = "DELETED";
      TravelClassesService.update(travelClass,
        function () {
          $uibModalInstance.close(true);
        });
    }
  }
})();
