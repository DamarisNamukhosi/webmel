(function() {
  'use strict';

  angular
      .module('webApp')
      .controller('LocationAlbumDetailController', LocationAlbumDetailController);

      LocationAlbumDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$localStorage', 'previousState', 'entity', 'URLS', 'LocationsService', 'album'];

  function LocationAlbumDetailController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, LocationsService, album) {
      var vm = this;


      vm.location = entity;
      vm.album = album;
      vm.previousState = previousState.name;

      var permissions = $localStorage.current_user_permissions;

      /**
       * This hack checks whether the logged in user is a MASTER_USER
       * or not then allocates the appropriate sref for the
       * locations-view
       */

      vm.previousState = permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";

  }
})();
