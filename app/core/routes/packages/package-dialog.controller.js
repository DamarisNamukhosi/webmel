(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PackageDialogController', PackageDialogController);

  PackageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', 'entity', 'PackagesService', 'OrganizationsService', 'SeasonGroupsService','LocationsService','CurrenciesService','safariConstants','pakage','CurrencyService', 'MarkupCategoriesService', 'WelcomePacksService', 'CountriesService'];

  function PackageDialogController(  $timeout, $scope, $stateParams, $localStorage, entity, PackagesService, OrganizationsService, SeasonGroupsService, LocationsService, CurrenciesService,safariConstants,pakage,CurrencyService,MarkupCategoriesService, WelcomePacksService,CountriesService) {
    var vm = this;

    vm.safari = entity;
    vm.save = save;
    $localStorage.safari = vm.safari;
    vm.pakage = pakage;
    console.log("vm.pakage"); 
    console.log($stateParams);
    //initialization
    vm.packageYears = [2019, 2018, 2017, 2016, 2015];
    vm.packageTypes =safariConstants.packageTypes
    vm.seasonGroups = [];
    vm.organizations = [];
    vm.currencies=[];
    vm.years=[];
    vm.supportedLanguages = safariConstants.supportedLanguages;
    vm.travellerProfiles = safariConstants.travellerProfile;
    vm.safariVehicleCategory = safariConstants.safariVehicleCategory;
    vm.purposes = safariConstants.purposeOfVisit;
    vm.safariCycles = safariConstants.safariCycle;
    vm.dayModels = safariConstants.dayModel;
    vm.safariScopes = safariConstants.safariScope;
    vm.travellerCategories = safariConstants.travellerCategory;
    // vm.fit= false;
    ////declare functions
    // vm.initChains = initChains;
    vm.initSeasonGroups = initSeasonGroups;
    vm.onYearChange = onYearChange;
    vm.getLocation= getLocation;
    vm.onEndDestinationSelected=onEndDestinationSelected;
    vm.onstartDestinationSelected=onstartDestinationSelected;
    vm.onPackageTypeChange=onPackageTypeChange;
    vm.onLocationSelected= onLocationSelected;
    vm.onSrcLocationSelected = onSrcLocationSelected;
    vm.onDstLocationNameSelected = onDstLocationNameSelected;
    vm.getPickLocations=getPickLocations;
    vm.search = search;
    vm.onCountryChange = onCountryChange;
    vm.onPaxChange = onPaxChange;
    //vm.getStepTemplate = getStepTemplate;
    //call functions needed to initialize data 
    initSteps();
    initSeasonGroups();
    initCurrencies();
    initYears();
    initDefaultsBySafariType();
    initEntities();

    function initEntities(){
      MarkupCategoriesService.getByOrganisation({id:$localStorage.current_organisation.id}).$promise.then(function response(result){
        vm.markUpCats = result;
        angular.forEach(vm.markUpCats, function(markUpCat){
  
          if(markUpCat.isDefault){
            vm.safari.markUpCategoryId = markUpCat.id;
          }
        });
      });
      WelcomePacksService.getByOrgId({id:$localStorage.current_organisation.id}).$promise.then(function response(result){
        vm.welcomePacks = result;
        angular.forEach(vm.welcomePacks, function(welcomePack){
          if(welcomePack.isDefault){
            vm.safari.requirementsPackId = welcomePack.id;
          }
        });
      });

      CurrencyService.getByOrganization({id:$localStorage.current_organisation.id}).$promise.then(function response(result){
        vm.currencyGroups = result;
        angular.forEach(vm.currencyGroups, function(group){
  
          if(group.usage == "PRODUCTS"){
            vm.safari.currencyConversionGroupId = group.id;
          }
        })
      });
      CountriesService.query().$promise.then(function response(result){
        vm.countries = result;
      });
      //

    }

    function initDefaultsBySafariType(){
      console.log("defauls....")
      console.log(vm.safari.type ==="FIT");
      console.log(vm.safari.type);
      if($stateParams.id !=null){
        vm.safari.serviceId = $stateParams.id;
      }else{
        vm.safari.serviceId = 694;

      }
    if(vm.safari.type ==="FIT" ){
      vm.fit = true;
      vm.safari.cycle = "OTHER";
      vm.safari.virtualVehicles = false;
      vm.safari.virtualTravellers = false
      vm.safari.minBookingPax= vm.safari.pax;
      vm.safari.maxBookingPax= vm.safari.pax;
      //virtualTravellers: false,
      onPaxChange(vm.safari.pax);
    }else if(vm.safari.type ==="SET_DEPARTURE"){

    }
  
  }
  
  function onCountryChange(code){
    console.log("country change");
    console.log(code);
    angular.forEach(vm.countries, function(country){
      if(code == country.code){
        vm.safari.residence = country.name;
        console.log(vm.safari.residence);
        console.log("safari");
      console.log(vm.safari);
      }
    });
  }
  function onPaxChange(pax){
    console.log("onPaxChange " + pax);
    vm.tempTravellerGroup= [];
    vm.safari.trallerGroup=[];
    vm.tempTravellerGroup = {
      "name": vm.safari.name,
      "notes": "some notes",
      "order": 0,
      "pax": vm.safari.pax,
      "profile": vm.safari.profile,
      "purpose": vm.safari.purpose,
      "reference": vm.safari.name,
      "residence": vm.safari.residence,
      "travellers": [],
      "eta": vm.safari.startDay,
      "etd": vm.safari.endDay,
      "tel":null,
      "country":vm.safari.country,
      "description": vm.safari.description,
      "email": null

    };

    for (var i = 0; i < vm.safari.pax; i++) {
      // if (vm.currentStep == vm.steps[i].step) {
      //     return vm.steps[i].templateUrl;
      // }
      var tempTraveller = {
        "age": 21,
        "category": "TRAVELLER",
        "configs": null,
        "contentStatus": "DRAFT",
        "country": vm.safari.country,
        
        "description": null,
        "dob": "1998",
        "document": null,
        "email": null,
        "eta":  vm.tempTravellerGroup.eta,
        "etd":  vm.tempTravellerGroup.etd,
        "groupId": $stateParams.groupId,
        
        "id":  null,
        "name":  vm.safari.name,
        "notes":  null,
        "order": 0,
        "profession":  null,
        "profile": vm.safari.profile,
        "purpose": vm.safari.purpose,
        "reference": vm.safari.name,
        "requiresCompleteness": true,
        "residence": vm.safari.residence,
        "statusReason":  null,
        "tel":  vm.tempTravellerGroup.tel,
        "travelDetails":  null,
        "user":  null
      };
      vm.tempTravellerGroup.travellers.push(tempTraveller);
  }
  vm.safari.trallerGroup = vm.tempTravellerGroup;

  console.log(vm.safari);
  updateVehicleInfo();
  }

  function updateVehicleInfo(){
    console.log("updateVehicleInfo");
    var tempSafariVehicle = 
      {
        "baseLocationId": vm.safari.locationId,
        "baseLocationName":vm.safari.locationName, 
        "category": "SAFARI",
        "categoryId":null,
        "categoryName": null,
        "configs": null,
        "contentStatus": "DRAFT",
        "description": null,
        "eta": vm.tempTravellerGroup.eta,
        "etd": vm.tempTravellerGroup.etd,
        "generalVehicleTypeId": null,
        "generalVehicleTypeName": null,
        "id": null, 
        "issuerId": $localStorage.current_organisation.id,
        "issuerName: string": $localStorage.current_organisation.name, 
        
        "name": "SAFARI Vehicle",
        "notes": null,
        "ref": null,
        "registration": null,
        "safariId": vm.safari.id,
        "safariName": vm.safari.name,
        "statusReason": null,
        "supplierId": $localStorage.current_organisation.id,
        "supplierName":$localStorage.current_organisation.name,
        "uuid":null,
        "vehicleId": null,
        "vehicleName": null,
        "vehicleTypeId": null,
        "vehicleTypeName": null
      }
    
      vm.safari.safariVehicles.push(tempSafariVehicle);
  }

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    vm.openFrom = function () {
      vm.openedFrom = true;
    };
  
    vm.openTo = function () {
      vm.openedTo = true;
    };
    function onPackageTypeChange(type){
      console.log("package type change" + " " +type)
      if(type === "INDEPENDENT_TOUR"){
        vm.fit = true;
      }else{
        vm.fit = false;
      }
    }
    function getLocation(val) {
      vm.searchedProperty = false;
        return LocationsService
          .query({
            name: val,
            types: [1,2,3,4,5,6,7,8,9,12,13,16,17,18,19,20,21,22,23,26]
          })
          .$promise
          .then(function (results) {
            vm.searchedItems = results;
            return results.map(function (item) {

              return item;
            })
          });
      }
      function getPickLocations(val) {
        vm.searchedProperty = false;
          return LocationsService
            .query({
              name: val,
              types: [1,2,3,4,5,6,7,8,9,10,11,12,13,16,17,18,19,20,21,22,23,26]
            })
            .$promise
            .then(function (results) {
              vm.searchedItems = results;
              return results.map(function (item) {
  
                return item;
              })
            });
        }
  function onLocationSelected($item, $model) {

    vm.safari.locationId=$item.id;
    vm.safari.locationName=$item.name;
    console.log(vm.safari);

  }

  function onEndDestinationSelected($item, $model){
    vm.safari.dropOffPointId = $item.id;
    vm.safari.dropOffPointName = $item.name;
    console.log(vm.safari);

}
function onstartDestinationSelected($item, $model){
  vm.safari.pickUpPointId = $item.id;
  vm.safari.pickUpPointName = $item.name;
  console.log(vm.safari);

}
//onDstLocationNameSelected

function onDstLocationNameSelected($item, $model){
  vm.safari.dstLocationId = $item.id;
  vm.safari.dstLocationName = $item.name;
  console.log(vm.safari);

}

function onSrcLocationSelected($item, $model){
  
  vm.safari.srcLocationId = $item.id;
  vm.safari.srcLocationName = $item.name;
  console.log(vm.safari);

}

function initCurrencies() {
  
  CurrenciesService.query().$promise.then(function (response){
    angular.forEach(response, function(obj){
        vm.currencies.push(obj);
    });
    console.log(vm.currencies);
});
}
    function save() {
      //setAttributes();
      vm.isSaving = true;
      if (vm.safari.id !== null) {
        PackagesService.update(vm.safari, onSaveSuccess, onSaveError);
      } else {
        PackagesService.saveFull(vm.safari, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      vm.isSaving = false;
      console.log("result");
      console.log(result);
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function initYears() {
      //endDay
      vm.safari.endDay= new Date(vm.safari.endDay);
      vm.safari.startDay= new Date(vm.safari.startDay);

      var currentYear = new Date().getFullYear();
      var totalYears = 10; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm.years.push(year);
      }
    }

    function search(year, type) {
      console.log("redirecting..");
      console.log(type);
      console.log(year);
      $state.go($state.current, { statePackageType: type, year: year }, { reload: true });
     //      url: "/packages/{statePackageType}/{year}/{locationId}/{groupId}/{countryId}/{targetId}/",
 
    }

    function initSteps() {
      // OrganizationsService.getOrganizationsByType({
      //   type: 8
      // }).$promise.then(function (response) {
      //   angular.forEach(response, function (org) {
      //     vm.organizations.push(org);
      //   });
      // });
      vm.safariFilterParams = $localStorage.safariFilterParams;

      vm.currentStep = 1;
      vm.steps = [
        {
          step: 1,
          name: "Basic Info",
          templateUrl: "core/routes/packages/steps/basic-info.html",
          visible:true
        },
        {
          step: 2,
          name: "Location Info",
          templateUrl: "core/routes/packages/steps/location-info.html",
          visible:true
        },   
        {
          step: 3,
          name: "Client Info",
          templateUrl: "core/routes/packages/steps/client-info.html",
          visible:true
        },   
        {
          step: 4,
          name: "Transport Info",
          templateUrl: "core/routes/packages/steps/vehicle-info.html",
          visible:true
        },  
        {
          step: 5,
          name: "Preview",
          templateUrl: "core/routes/packages/steps/preview-info.html",
          visible:true
        },              
      ];
      vm.user = {};
      
      //Functions
   
    }
    vm.gotoStep = function(newStep) {
      vm.currentStep = newStep;
    }
    
    vm.getStepTemplate = function(){
      for (var i = 0; i < vm.steps.length; i++) {
            if (vm.currentStep == vm.steps[i].step) {
                return vm.steps[i].templateUrl;
            }
        }
    }
    function initSeasonGroups() {
      SeasonGroupsService.getByOranizationId({
        id: $localStorage.current_organisation.id
      }).$promise.then(function (response) {
        angular.forEach(response, function (obj) {
          vm.seasonGroups.push(obj);
        });
      });
    }

    function onYearChange(year) {
      vm.seasonGroups = [];
      SeasonGroupsService.getByOranizationIdAndYear({
        id: $localStorage.current_organisation.id,
        year: year
      }).$promise.then(function (response) {
        angular.forEach(response, function (obj) {
          vm.seasonGroups.push(obj);
        });
      });
    }

    function setAttributes() {
      // targetName : null,
      // seasonGroupName : null,
      // chainName : null
      if (vm.safari.chainId !== null || vm.safari.chainId !== "undefined") {
        angular.forEach(vm.seasonGroups, function (season) {
          if (season.id == vm.safari.seasonGroupId) {
            vm.safari.seasonGroupName = season.name;
          }
        })
      }
      if (vm.safari.chainId !== null || vm.safari.chainId !== "undefined") {
        angular.forEach(vm.organizations, function (org) {
          if (vm.safari.chainId == org.id) {
            vm.safari.chainName = org.name;
          }
        })
      }
      if (vm.safari.targetId !== null || vm.safari.targetId !== "undefined") {
        angular.forEach(vm.organizations, function (org) {
          if (vm.safari.targetId == org.id) {
            vm.safari.targetName = org.name;
          }
        })
      }
      if (vm.safari.issuerId !== null || vm.safari.issuerId !== "undefined") {
          console.log(vm.safari.issuerName === null);
          if(vm.safari.issuerName === null){
            // angular.forEach(vm.organizations, function (org) {
            //     if (vm.safari.issuerId == org.id) {
            //       vm.safari.issuerName = org.name;
            //     }
            //   })
            vm.safari.issuerName = $localStorage.current_organisation.name; 
          }
        
      }
    }
    //

  }
})();
