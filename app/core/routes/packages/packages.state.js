(function () {
  "use strict";

  angular
    .module("webApp")
    .config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider.state("packages", {
      parent: "app",
      url: "/{id}/{generalServiceId}/packages/{statePackageType}/{year}/{locationId}/{countryId}/{targetId}/",
      data: {
        authorities: []
      },
      views: {
        "content@": {
          templateUrl: "core/routes/packages/packages.html",
          controller: "PackagesController",
          controllerAs: "vm"
        }
      },
      resolve: {
        packages: [
          "$stateParams",
          "$localStorage",
          "PackagesService",
          function ($stateParams, $localStorage, PackagesService) {
            return PackagesService
              .getSafariGroups()
              .$promise;
          }
        ]
      }
    })
      // .state('packages.new', {
      //   parent: 'packages',
      //   url: '/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: [],
      //     pageTitle: 'profsApp.professional.detail.title'
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$localStorage',
      //     '$uibModal',
      //     function ($stateParams, $state, $localStorage, $uibModal) {
      //       $uibModal.open({
      //         templateUrl: 'core/routes/packages/package-dialog.html',
      //         controller: 'PackageDialogController',
      //         controllerAs: 'vm',
      //         backdrop: 'static',
      //         size: 'lg',
      //         resolve: {
      //           entity: function () {
      //             return {
      //               country: $stateParams.countryId,
      //               contentStatus: "DRAFT",
      //               id: null,
      //               orgId: $localStorage.current_organisation.id,
      //               orgName: $localStorage.current_organisation.name,
      //               dmcId: $localStorage.current_organisation.id,
      //               dmcName: $localStorage.current_organisation.name,
      //               name: null,
      //               pax: null,
      //               seasonGroupId: null,
      //               seasonGroupName: null,
      //               targetId: null,
      //               targetName: null,
      //               type: $stateParams.statePackageType,
      //               uuid: null,
      //               year: $stateParams.year,
      //               currencyConfigs: null,
      //               currencyConversionGroupId: null,
      //               currencyConversionGroupName: null,
      //               currencyId: null,
      //               currencyName: null,
      //               cycle: "DAILY",
      //               dayModel: "DATE",
      //               defaultLanguage: "en",
      //               description: null,
      //               dimensionSetId: null,
      //               dimensionSetName: null,
      //               dropOffPointId: null,
      //               dropOffPointName: null,
      //               dstLocationId: null,
      //               dstLocationName: null,
      //               endDay: null,
      //               endTime: null,
      //               expiryTime: null,
      //               groupId: $stateParams.groupId,
      //               groupName: null,
      //               locationId: $stateParams.locationId,
      //               locationName: $localStorage.current_location.name,
      //               markUpCategoryId: null,
      //               markUpCategoryName: null,
      //               markUpConfigs: null,
      //               maxBookingPax: null,
      //               maxPricingPax: null,
      //               minBookingPax: 1,
      //               minPricingPax: 1,
      //               numOfDays: null,
      //               numOfNights: null,
      //               operationEndDate: "2019-08-13",
      //               operationStartDate: "2019-08-13",
      //               operationStatus: "PENDING",
      //               order: 0,
      //               pickUpPointId: null,
      //               pickUpPointName: null,
      //               profile: "VIP",
      //               purpose: "HOLIDAY",
      //               reference: null,
      //               requirementsPackId: null,
      //               requirementsPackName: null,
      //               residence: null,
      //               safariPackConfigs: null,
      //               safariTemplate: null,
      //               scope: "PRIVATE",
      //               serviceConfigs: null,
      //               serviceExclusionNotes: null,
      //               serviceExclusions: null,
      //               serviceId: null,
      //               serviceInclusionNotes: null,
      //               serviceInclusions: null,
      //               serviceName: null,
      //               specialNotes: null,
      //               srcLocationId: null,
      //               srcLocationName: null,
      //               startDay: null,
      //               startTime: null,
      //               status: "DRAFT",
      //               travellerGroupConfigs: null,
      //               travellerGroupId: null,
      //               travellerGroupName: null,
      //               vehicleGroupConfigs: null,
      //               virtualTravellers: true,
      //               virtualVehicles: true
      //             };
      //           }
      //         }
      //       })
      //         .result
      //         .then(function () {
      //           $state.go('packages', null, { reload: 'packages' });
      //         }, function () {
      //           $state.go('packages');
      //         });
      //     }
      //   ]
      // })
      //
      .state("packages.new", {
        parent: "packages",
        url: "/{groupId}/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: 'core/routes/packages/package-dialog.html',
            controller: 'PackageDialogController',
            controllerAs: "vm"
          }
        },
        resolve: {
          entity:
           [
            "$stateParams",
            "$localStorage",
            function ($stateParams, $localStorage) {
              console.log("$stateParams.countryId");
              console.log($stateParams.countryId);
              return {
                country: $stateParams.countryId == " " ? 'KE' : $stateParams.countryId,
                contentStatus: "DRAFT",
                id: null,
                orgId: $localStorage.current_organisation.id,
                orgName: $localStorage.current_organisation.name,
                dmcId: $localStorage.current_organisation.id,
                dmcName: $localStorage.current_organisation.name,
                pax: 2,
                name: 'NameX2',
                seasonGroupId: null,
                seasonGroupName: null,
                targetId: null,
                targetName: null,
                type: $stateParams.statePackageType,
                uuid: null,
                year: $stateParams.year,
                currencyConfigs: null,
                currencyConversionGroupId: null,
                currencyConversionGroupName: null,
                currencyId: 204,
                currencyName: null,
                cycle: "DAILY",
                dayModel: "DATE",
                defaultLanguage: "en",
                description: null,
                dimensionSetId: null,
                dimensionSetName: null,
                dropOffPointId: null,
                dropOffPointName: null,
                dstLocationId: null,
                dstLocationName: null,
                endDay: "2019-08-28",
                endTime: null,
                expiryTime: null,
                groupId: $stateParams.groupId,
                groupName: null,
                locationId: null,
                locationName: null,
                markUpCategoryId: null,
                markUpCategoryName: null,
                markUpConfigs: null,
                maxBookingPax: null,
                maxPricingPax: null,
                minBookingPax: 1,
                minPricingPax: 1,
                numOfDays: 5,
                numOfNights: 4,
                operationEndDate: "2019-08-13",
                operationStartDate: "2019-08-13",
                operationStatus: "PENDING",
                order: 0,
                pickUpPointId: null,
                pickUpPointName: null,
                profile: "FAMILY",
                purpose: "HOLIDAY",
                reference: null,
                requirementsPackId: null,
                requirementsPackName: null,
                residence: null,
                safariPackConfigs: null,
                safariTemplate: null,
                scope: "PRIVATE",
                serviceConfigs: null,
                serviceExclusionNotes: null,
                serviceExclusions: null,
                serviceId: null,
                serviceInclusionNotes: null,
                serviceInclusions: null,
                serviceName: null,
                specialNotes: null,
                srcLocationId: null,
                srcLocationName: null,
                startDay: "2019-08-23",
                startTime: null,
                status: "DRAFT",
                travellerGroupConfigs: null,
                travellerGroupId: null,
                travellerGroupName: null,
                vehicleGroupConfigs: null,
                virtualTravellers: true,
                virtualVehicles: true,
                safariVehicles:[]
              }
            }
          ],
          pakage:[
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              if($stateParams.groupId > 0){
              return PackagesService
                .getById({ id: $stateParams.groupId })
                .$promise;
            }else{
              return null
            }
          }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "packages",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })

      .state("packages-detail", {
        parent: "packages",
        url: "/{groupId}/details",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/package-detail.html",
            controller: "PackageDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity:
           [
            "$stateParams",
            "$localStorage",
            function ($stateParams, $localStorage) {
              return {
                country: $stateParams.countryId,
                contentStatus: "DRAFT",
                id: null,
                orgId: $localStorage.current_organisation.id,
                orgName: $localStorage.current_organisation.name,
                dmcId: $localStorage.current_organisation.id,
                dmcName: $localStorage.current_organisation.name,
                name: null,
                pax: null,
                seasonGroupId: null,
                seasonGroupName: null,
                targetId: null,
                targetName: null,
                type: $stateParams.statePackageType,
                uuid: null,
                year: $stateParams.year,
                currencyConfigs: null,
                currencyConversionGroupId: null,
                currencyConversionGroupName: null,
                currencyId: 204,
                currencyName: null,
                cycle: "DAILY",
                dayModel: "DpATE",
                defaultLanguage: "en",
                description: null,
                dimensionSetId: null,
                dimensionSetName: null,
                dropOffPointId: null,
                dropOffPointName: null,
                dstLocationId: null,
                dstLocationName: null,
                endDay: "2019-08-28",
                endTime: null,
                expiryTime: null,
                groupId: $stateParams.groupId,
                groupName: null,
                locationId: $stateParams.locationId,
                locationName: $localStorage.current_location.name,
                markUpCategoryId: null,
                markUpCategoryName: null,
                markUpConfigs: null,
                maxBookingPax: null,
                maxPricingPax: null,
                minBookingPax: 1,
                minPricingPax: 1,
                numOfDays: 5,
                numOfNights: 4,
                operationEndDate: "2019-08-13",
                operationStartDate: "2019-08-13",
                operationStatus: "PENDING",
                order: 0,
                pickUpPointId: null,
                pickUpPointName: null,
                profile: "VIP",
                purpose: "HOLIDAY",
                reference: null,
                requirementsPackId: 11,
                requirementsPackName: null,
                residence: null,
                safariPackConfigs: null,
                safariTemplate: null,
                scope: "PRIVATE",
                serviceConfigs: null,
                serviceExclusionNotes: null,
                serviceExclusions: null,
                serviceId: $stateParams.id,
                serviceInclusionNotes: null,
                serviceInclusions: null,
                serviceName: null,
                specialNotes: null,
                srcLocationId: null,
                srcLocationName: null,
                startDay: "2019-08-23",
                startTime: null,
                status: "DRAFT",
                travellerGroupConfigs: null,
                travellerGroupId: null,
                travellerGroupName: null,
                vehicleGroupConfigs: null,
                virtualTravellers: false,
                virtualVehicles: false,
                safariVehicles:[]
              }
            }
          ],
          pakage:[
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              return PackagesService
                .getById({ id: $stateParams.groupId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "packages-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })

      .state("packages-detail-view", {
        parent: "packages",
        url: "/{id}/view",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/package-detail-view.html",
            controller: "PackageDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          pakage: [
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              return PackagesService
                .getFull({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "packages-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("packages-detail.edit", {
        parent: "packages-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/package-dialog.html",
              controller: "PackageDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .getById({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-detail", null, { reload: "packages-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("programme-detail", {
        parent: "packages-detail",
        url: "/{progId}/manage",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.programme.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/programmes/programme-detail.html",
            controller: "ProgrammeDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          programme: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getFull({ id: $stateParams.progId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("programme-detail-view.manage", {
        parent: "programme-detail-view",
        url: "{progId}/manage/",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.programme.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/programmes/programme-detail.html",
            controller: "ProgrammeDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          programme: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getFull({ id: $stateParams.progId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "programme-detail-view",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("programme-detail-view", {
        parent: "packages-detail-view",
        url: "/{progId}/view",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.programme.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/programmes/programme-detail-view.html",
            controller: "ProgrammeDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          programme: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getFull({ id: $stateParams.progId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("programme-detail.edit", {
        parent: "programme-detail",
        url: "/{type}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/programme-dialog.html",
              controller: "ProgrammeDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "ProgrammesService",
                  function (ProgrammesService) {
                    return ProgrammesService
                      .getById({ id: $stateParams.progId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("programme-detail", null, { reload: "programme-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state('programme-detail.newParam', {
        parent: 'programme-detail',
        url: '/{paramType}/fff-new-parameter',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/programmes/parameters/programme-parameter-dialog.html',
              controller: 'ProgrammeParameterDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'md',
              resolve: {
                entity: [
                  function () {
                    return {
                      description: null,
                      id: null,
                      name: null,
                      programmeId: $stateParams.progId,
                      programmeName: null,
                      title: null,
                      type: null,
                      uuid: null
                    };
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go('programme-detail', null, { reload: 'programme-detail' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('programme-detail.editParameter', {
        parent: 'programme-detail',
        url: '/{type}/edit-parameter/{paramId}',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/programmes/parameters/programme-parameter-dialog.html',
              controller: 'ProgrammeParameterDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'md',
              resolve: {
                entity: [
                  '$stateParams',
                  'ProgrammeParametersService',
                  function ($localStorage, ProgrammeParametersService) {
                    return ProgrammeParametersService
                      .get({ id: $stateParams.paramId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go('programme-detail', null, { reload: 'programme-detail' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('programme-detail.deleteParameter', {
        parent: 'programme-detail',
        url: '/{type}/delete-parameter/{paramId}',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/programmes/parameters/programme-parameter-delete.html',
              controller: 'ProgrammeParameterDeleteController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'md',
              resolve: {
                entity: [
                  '$stateParams',
                  'ProgrammeParametersService',
                  function ($localStorage, ProgrammeParametersService) {
                    return ProgrammeParametersService
                      .get({ id: $stateParams.paramId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go('programme-detail', null, { reload: 'programme-detail' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('programme-detail.profile', {
        parent: 'programme-detail',
        url: '/image/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/profile/profile-dialog.html',
              controller: 'ProfileImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'md',
              resolve: {
                profile: function () {
                  return { file: null };
                },
                entity: [
                  '$stateParams',
                  'ProgrammesService',
                  function ($stateParams, ProgrammesService) {
                    return ProgrammesService
                      .getById({ id: $stateParams.progId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go('programme-detail', null, { reload: 'programme-detail' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('programme-detail.newSupplement', {
        parent: 'programme-detail',
        url: '/{dimensionId}/{currencyId}/{organisationId}/new-supplement/',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/contracts/supplements/supplement-dialog.html',
                controller: 'SupplementDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      var mandatory = !($stateParams.dimensionId == -1);
                      return {
                        id: null,
                        name: null,
                        organisationId: $stateParams.organisationId,
                        value: null,
                        unit: null,
                        billingType: "VALUE",
                        mandatory: mandatory,
                        //existenceId: null,
                        contentStatus: "DRAFT",
                        description: null,
                        dimensionId: $stateParams.dimensionId,
                        currencyId: $stateParams.currencyId,
                        contractId: $stateParams.contractId,
                        dimensionCount: 1,
                        dimensionType1: null,
                        existence1: null,
                        organisationName: null,
                        supplementType: "SUPPLEMENT"
                      };
                    }
                  ],
                  dimensionSet: [
                    '$stateParams',
                    '$localStorage',
                    'DimensionSetsService',
                    function (
                      $stateParams,
                      $localStorage,
                      DimensionSetsService
                    ) {
                      return DimensionSetsService.getSetDimensionExistenciesBySetId({
                        id: $stateParams.setId,
                        year: $stateParams.year
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('programme-detail', null, {
                    reload: 'programme-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state("programme-itinerary", {
        parent: "packages-detail",
        url: "/{progId}/itinerary",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.programme.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/programmes/programme-itinerary.html",
            controller: "ProgrammeItineraryController",
            controllerAs: "vm"
          }
        },
        resolve: {
          programme: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getFull({ id: $stateParams.progId })
                .$promise;
            }
          ],
          entity: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getById({ id: $stateParams.progId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('programme-itinerary.profile', {
        parent: 'programme-itinerary',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
              controller: 'LocationImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    file: null
                  };
                },
                object: function () {
                  return {
                    uuid: $stateParams.objectUuid,
                    name: $stateParams.objectName,
                  };
                }
              }
            })
              .result
              .then(function () {
                $state.go('programme-itinerary', null, { reload: 'programme-itinerary' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state('programme-itinerary.profile-service-group', {
        parent: 'programme-itinerary',
        url: '/{serviceGroupId}/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/locations/image-upload/image-upload-dialog.html',
              controller: 'LocationImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    file: null
                  };
                },
                object: function () {
                  return {
                    uuid: $stateParams.objectUuid,
                    name: $stateParams.objectName,
                  };
                }
              }
            })
              .result
              .then(function () {
                $state.go('programme-itinerary', null, { reload: 'programme-itinerary' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state("programme-detail.createServiceProgramme", {
        parent: "programme-detail",
        url: "/{serviceGroupId}/{day}/new-service",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER", "PROFESSIONAL_USER", "T_OPERATOR_USER", "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/programme-detail-create-programme-service-dialog.html",
              controller: "ProgrammeServiceDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: function () {
                  return {

                    contractId: null,
                    contractName: null,
                    description: null,
                    endTime: null,
                    id: null,
                    markUp: null,
                    name: null,
                    programmeServiceGroupDay: $stateParams.day,
                    programmeServiceGroupId: $stateParams.serviceGroupId,
                    programmeServiceParams: [],
                    providerId: null,
                    providerName: null,
                    serviceId: null,
                    serviceName: null,
                    startTime: null,
                    uuid: null,
                    value: null,
                    visible: true
                  };
                },
                generalServices: [
                  "GeneralServicesService",
                  function (GeneralServicesService) {
                    return GeneralServicesService
                      .query()
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("programme-detail", null, { reload: "programme-detail" });
              }, function () {
                $state.go("programme-detail");
              });
          }
        ]

      })
      .state("programme-detail.editServiceProgramme", {
        parent: "programme-detail",
        url: "/{serviceGroupId}/{day}/{serviceId}/edit-service",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER", "PROFESSIONAL_USER", "T_OPERATOR_USER", "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/programme-detail-create-programme-service-dialog.html",
              controller: "ProgrammeServiceDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: [
                  "ProgrammeServicesService",
                  function (ProgrammeServicesService) {
                    return ProgrammeServicesService
                      .getFull({ id: $stateParams.serviceId })
                      .$promise;
                  }
                ],
                generalServices: [
                  "GeneralServicesService",
                  function (GeneralServicesService) {
                    return GeneralServicesService
                      .query()
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("programme-detail", null, { reload: "programme-detail" });
              }, function () {
                $state.go("programme-detail");
              });
          }
        ]

      })
      .state('programme-detail.deleteProgrammeService', {
        parent: 'programme-detail',
        url: '/{serviceId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/programmes/programme-service-delete-dialog.html',
              controller: 'ProgrammeServiceDeleteDialogController',
              controllerAs: 'vm',
              size: 'md',
              resolve: {
                entity: [
                  'ProgrammeServicesService',
                  function (ProgrammeServicesService) {
                    return ProgrammeServicesService
                      .getById({ id: $stateParams.serviceId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go('programme-detail', null, { reload: 'programme-detail' });
              }, function () {
                $state.go('^');
              });
          }
        ]
      })
      .state("programme-itinerary.editServiceGroup", {
        parent: "programme-itinerary",
        url: "/{serviceGroupId}/{day}/edit-day",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER", "PROFESSIONAL_USER", "T_OPERATOR_USER", "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/service-group-dialog.html",
              controller: "ServiceGroupDialog",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: [
                  "ServiceGroupsService",
                  function (ServiceGroupsService) {
                    return ServiceGroupsService
                      .get({ id: $stateParams.serviceGroupId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("programme-itinerary", null, { reload: "programme-detail" });
              }, function () {
                $state.go("programme-itinerary");
              });
          }
        ]

      })
      .state('programme-rates', {
        parent: 'packages-detail',
        url: '/{progId}/{contractId}/{setId}/overview/{seasonGroupId}/{year}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Cost Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/programmes/programme-rates.html',
            controller: 'ProgrammeRatesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          programme: [
            "$stateParams",
            "ProgrammesService",
            function ($stateParams, ProgrammesService) {
              return ProgrammesService
                .getFull({ id: $stateParams.progId })
                .$promise;
            }
          ],
          contract: [
            '$stateParams',
            '$localStorage',
            'ProgrammesService',
            function ($stateParams, $localStorage, ProgrammesService) {
              return ProgrammesService
                .getContract({ id: $stateParams.progId })
                .$promise;
            }
          ],
          dimensionSet: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function ($stateParams, $localStorage, DimensionSetsService) {
              return DimensionSetsService
                .getSetDimensionExistenciesBySetId({ id: $stateParams.setId, seasonGroupId: null, year: 2018 })
                .$promise;
            }
          ],
          dimensions: [
            '$stateParams',
            '$localStorage',
            'DimensionSetsService',
            function ($stateParams, $localStorage, DimensionSetsService) {
              return DimensionSetsService
                .getAllDimensions()
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'programme-rates',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }

      })
      .state("packages-own", {
        parent: "packages",
        url: "/own",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/packages-own.html",
            controller: "VehiclesOwnController",
            controllerAs: "vm"
          }
        },
        resolve: {
          packages: [
            "$stateParams",
            "$localStorage",
            "PackagesService",
            function ($stateParams, $localStorage, PackagesService) {
              return PackagesService
                .getVehiclesByOrganizationId({ id: $localStorage.current_organisation.id })
                .$promise;
            }
          ]
        }
      })
      .state("packages-detail.newProgramme", {
        parent: "packages-detail",
        url: "/{seasonGroupId}/{year}/{type}/newProgramme",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER", "PROFESSIONAL_USER", "T_OPERATOR_USER", "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/programme-dialog.html",
              controller: "ProgrammeDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: function () {
                  return {
                    contractId: null,
                    contractName: null,
                    contractType: "PUBLISHED",
                    contractParams: [],
                    currencyId: null,
                    currencyName: null,
                    description: null,
                    endLocationId: null,
                    endLocationName: null,
                    generalMarkUp: null,
                    id: null,
                    numberOfNights: null,
                    numberOfDays: null,
                    pakageId: $stateParams.id,
                    pakageName: null,
                    packageType: $stateParams.type,
                    reference: null,
                    startDay: null,
                    startLocationId: null,
                    startLocationName: null,
                    uuid: null,
                    setDimensionId: null,
                    issuerId: $localStorage.current_organisation.id,
                    supplierId: $localStorage.current_organisation.id,
                    seasonGroupId: $stateParams.seasonGroupId,
                    year: $stateParams.year,
                    targetId: $stateParams.targetId,
                    serviceId: null,
                    contentStatus: "DRAFT"
                  };
                }
              }
            })
              .result
              .then(function () {
                $state.go("packages-detail", null, { reload: "packages-detail" });
              }, function () {
                $state.go("packages-detail");
              });
          }
        ]
      })
      .state("packages-own-detail", {
        parent: "packages-own",
        url: "/{id}/package",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/package-detail.html",
            controller: "VehicleDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              return PackagesService
                .get({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "packages-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("packages.edit", {
        parent: "packages",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/package-dialog.html",
              controller: "PackageDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .getById({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages", null, { reload: "packages" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages-own.edit", {
        parent: "packages-own",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/package-dialog.html",
              controller: "VehicleDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-own", null, { reload: "packages-own" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages.delete", {
        parent: "packages",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/package-delete-dialog.html",
              controller: "VehicleDeleteDialogController",
              controllerAs: "vm",
              size: "md",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages", null, { reload: "packages" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages-own.delete", {
        parent: "packages-own-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/package-delete-dialog.html",
              controller: "VehicleDeleteDialogController",
              controllerAs: "vm",
              size: "md",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages.profile", {
        parent: "packages-own-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/packages/image-upload/image-upload-dialog.html",
              controller: "VehicleImageUploadDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: function () {
                  return { file: null };
                },
                package: [
                  "$stateParams",
                  "PackagesService",
                  function ($stateParams, PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("package-galleries", {
        parent: "app",
        url: "/packages/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/package-galleries.html",
            controller: "VehicleGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              return PackagesService
                .get({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("package-galleries.new", {
        parent: "package-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/albums/album-create-dialog.html",
              controller: "AlbumCreateDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ],
                album: function () {
                  return {
                    albumType: "GENERAL", caption: null, //album name
                    coverName: null, //uploaded cover image file name
                    coverUuid: null, //uploaded cover image
                    isDefaultAlbum: true, //put option slider
                    name: null, //album name
                    objectUuid: null //location uuid
                  };
                }
              }
            })
              .result
              .then(function () {
                $state.go("package-galleries", null, { reload: "package-galleries" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("package-galleries.delete", {
        parent: "package-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({ templateUrl: "core/routes/albums/album-delete-dialog.html", controller: "AlbumDeleteDialogController", controllerAs: "vm", backdrop: "static", size: "md" })
              .result
              .then(function () {
                $state.go("package-galleries", null, { reload: "package-galleries" });
              }, function () {
                $state.go("package-galleries");
              });
          }
        ]
      })
      .state("package-album-detail", {
        parent: "package-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/packages/package-album-detail.html",
            controller: "VehicleAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "PackagesService",
            function ($stateParams, PackagesService) {
              return PackagesService
                .get({ id: $stateParams.id })
                .$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function ($stateParams, AlbumService) {
              return AlbumService
                .getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || "package-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("package-album-detail.edit", {
        parent: "package-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "package-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/albums/album-dialog.html",
              controller: "AlbumDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "AlbumService",
                  function (AlbumService) {
                    return AlbumService
                      .get({ id: $stateParams.albumId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("package-album-detail", null, { reload: "package-album-detail" });
              }, function () {
                $state.go("package-album-detail");
              });
          }
        ]
      })
      .state("package-album-detail.makeCoverImage", {
        parent: "package-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/albums/album-change-cover-image-dialog.html",
              controller: "AlbumChangeCoverImageDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: [
                  "AlbumService",
                  "$stateParams",
                  function (AlbumService, $stateParams) {
                    return AlbumService
                      .get({ id: $stateParams.albumId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("package-album-detail", null, { reload: "package-album-detail" });
              }, function () {
                $state.go("package-album-detail");
              });
          }
        ]
      })
      .state("package-album-detail.deleteImage", {
        parent: "package-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/albums/album-image-delete-dialog.html",
              controller: "AlbumImageDeleteDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                entity: [
                  "AlbumService",
                  "$stateParams",
                  function (AlbumService, $stateParams) {
                    return AlbumService
                      .get({ id: $stateParams.albumId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("package-album-detail", null, { reload: "package-album-detail" });
              }, function () {
                $state.go("package-album-detail");
              });
          }
        ]
      })
      .state("package-album-detail.upload", {
        parent: "package-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/albums/album-upload-dialog.html",
              controller: "AlbumUploadDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: [
                  "$stateParams",
                  "AlbumService",
                  function ($stateParams, AlbumService) {
                    return AlbumService
                      .get({ id: $stateParams.albumId })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("package-album-detail", null, { reload: "package-album-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages-own-detail.addLabels", {
        parent: "packages-own-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          "$localStorage",
          function ($stateParams, $state, $uibModal, $localStorage) {
            $uibModal.open({
              templateUrl: "core/routes/labels/object-label-create-dialog.html",
              controller: "ObjectLabelGroupCreateDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                labelGroups: [
                  "LabelGroupsService",
                  function (LabelGroupsService) {
                    return LabelGroupsService
                      .getLabelGroupsByObjectType({ objectType: $stateParams.objectType })
                      .$promise;
                  }
                ],
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ],
                savedselectedLabels: [
                  "ObjectLabelsService",
                  function (ObjectLabelsService) {
                    return ObjectLabelsService
                      .getLabelsByObjectId({ uuid: $stateParams.uuid })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("^");
              }); //
          }
        ]
      })
      .state("packages-own-detail.deleteLabel", {
        parent: "packages-own-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({ templateUrl: "core/routes/labels/object-label-delete-dialog.html", controller: "ObjectLabelGroupDeleteDialogController", controllerAs: "vm", backdrop: "static", size: "md" })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("^");
              });
          }
        ]
      })
      .state("packages-own-detail.addFeature", {
        parent: "packages-own-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/features/object-feature-create-dialog.html",
              controller: "ObjectFeatureCreateDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "md",
              resolve: {
                features: [
                  "ObjectFeaturesService",
                  function (ObjectFeaturesService) {
                    return ObjectFeaturesService
                      .getFeaturesByObjectType({ objectType: $stateParams.objectType })
                      .$promise;
                  }
                ],
                entity: [
                  "PackagesService",
                  function (PackagesService) {
                    return PackagesService
                      .get({ id: $stateParams.id })
                      .$promise;
                  }
                ],
                savedselectedFeatures: [
                  "ObjectFeaturesService",
                  function (ObjectFeaturesService) {
                    return ObjectFeaturesService
                      .getFeaturesByObjectId({ uuid: $stateParams.uuid })
                      .$promise;
                  }
                ]
              }
            })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("packages-own-detail");
              });
          }
        ]
      })
      .state("packages-own-detail.deleteFeature", {
        parent: "packages-own-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({ templateUrl: "core/routes/features/object-feature-delete-dialog.html", controller: "ObjectFeatureDeleteDialogController", controllerAs: "vm", backdrop: "static", size: "md" })
              .result
              .then(function () {
                $state.go("packages-own-detail", null, { reload: "packages-own-detail" });
              }, function () {
                $state.go("packages-own-detail");
              });
          }

        ]

      })
      .state("packages.newFIT", {
        parent: "packages",
        url: "/newFIT",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER", "PROFESSIONAL_USER", "T_OPERATOR_USER", "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
              templateUrl: "core/routes/programmes/fit-dialog.html",
              controller: "ProgrammeDialogController",
              controllerAs: "vm",
              backdrop: "static",
              size: "lg",
              resolve: {
                entity: function () {
                  return {
                    contractId: null,
                    contractName: null,
                    contractType: "PUBLISHED",
                    currencyId: null,
                    currencyName: null,
                    description: null,
                    endLocationId: null,
                    endLocationName: null,
                    generalMarkUp: null,
                    id: null,
                    numberOfNights: null,
                    numberOfDays: null,
                    pakageId: null,
                    pakageName: null,
                    packageType: 'INDEPENDENT_TOUR',
                    contentStatus: 'DRAFT',
                    reference: null,
                    startDay: null,
                    startLocationId: null,
                    startLocationName: null,
                    uuid: null,
                    setDimensionId: null,
                    issuerId: $localStorage.current_organisation.id,
                    year: null,
                    targetId: null,
                    serviceId: null
                  };
                }
              }
            })
              .result
              .then(function () {
                $state.go("packages-detail", null, { reload: "packages-detail" });
              }, function () {
                $state.go("packages-detail");
              });
          }
        ]
      })
  }
})();
