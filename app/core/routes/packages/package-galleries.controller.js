(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("LocationGalleriesController", LocationGalleriesController);

  LocationGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService"
  ]; //

  function LocationGalleriesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.location = entity;
    vm.albums = AlbumService.getAlbums({ uuid: vm.location.uuid });
    console.log(vm.albums);
    console.log(vm.location);

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;

    var permissions = $localStorage.current_user_permissions;

    /**
       * This hack checks whether the logged in user is a MASTER_USER
       * or not then allocates the appropriate sref for the
       * locations-view
       */

    vm.previousState =
      permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";
  }
})();
