(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('PackagesService', PackagesService);

    PackagesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function PackagesService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getVehicles': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/pakages'
        },
        'getVehiclesByOrganizationId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/pakages/filter-by-organisation/:id'
        },
        'getFiltered': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          //general-filter/:id?language=:language&skill=:skill
          url: URLS.BASE_URL + 'costingservice/api/pakages/general-filter/:id?type=:type&year=:year&target=:target'
        },
        ///get-full
        'getFull': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/pakages/get-full/:id'
        },

        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/pakages'
        },

        'getById': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/pakages'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/pakages'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/pakages/:id'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        },
        'saveFull': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/safaris/save-full'
        },

        //POST /api/safaris/save-full
        'getSafariGroupById': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/:id'
        },

        'getSafariGroups': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        }
      });
    }
})();
