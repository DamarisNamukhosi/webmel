(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleDeleteDialogController',VehicleDeleteDialogController);

    VehicleDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'VehiclesService'];

    function VehicleDeleteDialogController($uibModalInstance, entity, VehiclesService) {
        var vm = this;

        vm.vehicle = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (vehicle) {
            console.log('changing vehicle contentStatus to DELETED');
            vm.vehicle.contentStatus = "DELETED";
            VehiclesService.update(vehicle,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
