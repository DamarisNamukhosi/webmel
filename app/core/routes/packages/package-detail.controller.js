(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("PackageDetailController", PackageDetailController);

  PackageDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "pakage",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS",
    "ProgrammesService",
    "PackagesService"
  ];

  function PackageDetailController($scope, $rootScope, $stateParams, previousState, pakage, ObjectLabelsService, ObjectFeaturesService, URLS, ProgrammesService, PackagesService) {
    var vm = this;
    
    vm.pakage = pakage;
    console.log(vm.pakage);
    vm.years = [];
    vm.days = [];

    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
    vm.previousState = previousState.name;
    vm.resolvePackageTypeName = resolvePackageTypeName;
    vm.initProgrammes = initProgrammes;
    vm.programmes = [];
    vm.searchedProgrammes = [];
    vm.programmesFound = true;
    vm.programmeDays = [];

    vm.search = search;


    vm.pakageTypes = [{
      value: "SET_DEPARTURE",
      name: "Set Departure"
    }, {
      value: "PRIVATE_DEPARTURE",
      name: "Private Departure"
    }, {
      value: "FITs",
      name: "FIT"
    }];
    // vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({
    //   uuid: vm.pakage.uuid
    // });
    // console.log(vm.objectLabels);

    // vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({
    //   uuid: vm.pakage.uuid
    // });

    initYears();
    initDays();
    initProgrammes();
    //initItenerary();


    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 5; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function initDays() {
      var numberofDays = 20;
      for (var i = 1; i <= numberofDays; i++) {
        vm
          .days
          .push(i);
      }
    }

    function resolvePackageTypeName(type) {
      angular.forEach(vm.pakageTypes, function (pakageType) {
        if (type === pakageType.value) {
          vm.pakageName = pakageType.name;
        }
      });
      return vm.pakageName;
    }

    function initProgrammes() {
      //programmeDTOList
      console.log("init programms..");
      console.log(vm.pakage.length);
      console.log(vm.pakage > 0);
      console.log(vm.pakage.programmeDTOList > 0);
      if(vm.pakage.length == undefined){
        console.log("not yet resolved");
      PackagesService
                .getFull({ id: $stateParams.id })
                .$promise.then(function (response){
                  vm.pakage = response
                })
                console.log(vm.pakage.length);

              }
              console.log(vm.pakage.length);

      if (vm.pakage.programmeDTOList > 0) {
        angular.forEach(vm.pakage.programmeDTOList, function (programme) {
          vm.programmes.push(programme);
        });
      }

      vm.searchedProgrammes = vm.programmes;
    }


    function search(name) {
      vm.searchedProgrammes = [];
      angular.forEach(vm.programmes, function (programme) {
        if (programme.reference.toLowerCase().includes(name.toLowerCase())) {
          vm.searchedProgrammes.push(programme);
        }
      });
    }
  }
})();
