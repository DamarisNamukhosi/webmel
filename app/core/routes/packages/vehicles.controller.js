(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehiclesController', VehiclesController);

    VehiclesController.$inject = ['$scope', '$location', '$state', 'vehicles', 'URLS'];

  function VehiclesController($scope, $location, $state, vehicles,URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.profile_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound=true;    
        vm.vehicles = vehicles;
        console.log('vehicles....');
        console.log(vm.vehicles);
        
        if (vm.vehicles.length === 0){
            vm.noRecordsFound = false;
        }

    }
})();
