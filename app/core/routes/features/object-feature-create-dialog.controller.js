(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "ObjectFeatureCreateDialogController",
      ObjectFeatureCreateDialogController
    );

  ObjectFeatureCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "ObjectFeaturesService",
    "features",
    "savedselectedFeatures"
  ];

  function ObjectFeatureCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    ObjectFeaturesService,
    features,
    savedselectedFeatures
  ) {
    var vm = this;

    vm.features = features;
    console.log(vm.features);
    vm.savedselectedFeatures = savedselectedFeatures;
    console.log('selected featuress');
    console.log(vm.savedselectedFeatures);
    vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.selectedFeatures = [];
    vm.savedList = [];

    vm.noFeaturesFound = (vm.features === undefined || vm.features.length === 0);

    vm.checkIfExists = function (name) {

      var found = false;
      angular.forEach(vm.savedselectedFeatures, function(item){

        if(!found){

          if (item.name === name){

            found = true;

          }
        }

      });

      return found;
    }

    vm.toggleSelection = function(feature) {
      var id = vm.selectedFeatures.indexOf(feature);
      if (id > -1) {
        vm.selectedFeatures.splice(id, 1);
      } else {
        vm.selectedFeatures.push(feature);
      }
    };

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      angular.forEach(vm.selectedFeatures, function(item) {

        var temp = {
          generalFeatureId: item.id,
          generalFeatureName: item.name,
          objectId: vm.entity.uuid
        };

        vm.savedList.push(temp);
      });
      console.log('savedlist...');
      console.log(vm.savedList);
      ObjectFeaturesService.createList(vm.savedList, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
