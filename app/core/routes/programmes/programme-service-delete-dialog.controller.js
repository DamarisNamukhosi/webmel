(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('ProgrammeServiceDeleteDialogController', ProgrammeServiceDeleteDialogController);

    ProgrammeServiceDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'ProgrammeServicesService'];

    function ProgrammeServiceDeleteDialogController($uibModalInstance, entity, ProgrammeServicesService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(obj) {
            console.log('changing vehicle contentStatus to DELETED');
            vm.entity.contentStatus = "DELETED";
            ProgrammeServicesService.update(vm.entity, function () {
                $uibModalInstance.close(true);
            });
        }
    }
})();
