(function() {
    "use strict";

    angular
        .module("webApp")
        .controller("ProgrammeDetailController", ProgrammeDetailController);

    ProgrammeDetailController.$inject = [
        "$scope",
        "$rootScope",
        "$stateParams",
        "previousState",
        "programme",
        "ObjectLabelsService",
        "ObjectFeaturesService",
        "URLS",
        "ProgrammeParametersService",
        "$filter"
    ];

    function ProgrammeDetailController($scope, $rootScope, $stateParams, previousState, programme, ObjectLabelsService, ObjectFeaturesService, URLS, ProgrammeParametersService,$filter) {
        var vm = this;
        vm.programme = programme;

        console.log($stateParams);

        console.log("vm.programme");
        console.log(vm.programme);

        vm.years = [];
        vm.days = [];
        vm.programmeDays = [];
        vm.count = '';
        vm.programmeParams = [];
        vm.exclusionParams = [];
        vm.inclusionParams = [];
        vm.requirementParams = [];
        vm.otherParams = [];

        vm.progId = vm.programme.id;
        vm.contractId = vm.programme.contractId;
        vm.setId = vm.programme.setDimensionId;
        vm.seasonGroupId = vm.programme.seasonGroupId;
        vm.year = vm.programme.year;
        vm.toOrdinal=toOrdinal;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.programme.uuid;
        vm.previousState = previousState.name;
        vm.fit= vm.programme.packageType === "INDEPENDENT_TOUR" ? vm.fit=true : vm.fit=false;

        initYears();
        initDays();
        initItenerary();
        initProgrammeParams();



        function initYears() {
            var currentYear = new Date().getFullYear();
            var totalYears = 5; //number of years visible
            var numberOfYearsBack = 3; //how many years back
            for (var i = 1; i <= totalYears; i++) {
                var year = currentYear + i - numberOfYearsBack;
                vm
                    .years
                    .push(year);
            }
        }
        function toOrdinal(date){
            var thisDate= date.split('-');
            var newDate= new Date(date);
            var month = newDate.toLocaleString("en-us", { month: "long" });
            console.log(thisDate);
            console.log($filter('ordinal')(thisDate[2]) + "-" +thisDate[1] +"-"+thisDate[0]);
            return $filter('ordinal')(thisDate[2]) + " " +month +" " +thisDate[0];
        }

        function initDays() {
            var numberofDays = 20;
            for (var i = 1; i <= numberofDays; i++) {
                vm
                    .days
                    .push(i);
            }
        }

        function initItenerary() {
            console.log(vm.programme.numberOfDays);
            vm.count = vm.programme.numberOfDays;
            if (vm.count !== null && vm.count !== 0) {
                vm.i = 1;
                for (; vm.i <= vm.count; vm.i++) {
                    vm.day = "Day " + vm.i;
                    vm
                        .programmeDays
                        .push(vm.day);
                }
                console.log("days..");
                console.log(vm.programmeDays);
            }
        }

        function initProgrammeParams() {
            vm.programmeParams = vm.programme.programmeParams;
            angular.forEach(vm.programmeParams, function(param) {
                if (param.type === "INCLUSION") {
                    vm
                        .inclusionParams
                        .push(param);
                } else if (param.type === "EXCLUSION") {
                    vm
                        .exclusionParams
                        .push(param);
                } else if (param.type === "REQUIREMENTS") {
                    vm.requirementParams.push(param);
                } else if (param.type === "OTHERS") {
                    vm.otherParams.push(param);
                }

            });
        }
    }
})();
