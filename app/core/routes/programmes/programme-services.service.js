(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('ProgrammeServicesService', ProgrammeServicesService);

    ProgrammeServicesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ProgrammeServicesService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'createFullService': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/programme-services/create-full'
            },
            'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/programme-services'
            },
            'getById': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'costingservice/api/programme-services/:id'
            },
            'getFull': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'costingservice/api/programme-services/get-full/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/programme-services'
            },
            'updateFullService': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/programme-services/update-full'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/programme-services'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/programme-services/:id'
            }
        });
    }
})();
