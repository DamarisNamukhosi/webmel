(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('ProgrammesService', ProgrammesService);

  ProgrammesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ProgrammesService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'getFull': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/programmes/get-full/:id'
      },
      'updateFull': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programmes'
      },
      'createFull': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programmes/create-full'
      },
      'getContract': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/programmes/get-contract/:id'
      },
      'getVehiclesByOrganizationId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programmes/filter-by-organisation/:id'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programmes'
      },
      'getById': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/programmes/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programmes'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programmes'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programmes/:id'
      },
      'uploadProfilePhoto': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
      }
    });
  }
})();
