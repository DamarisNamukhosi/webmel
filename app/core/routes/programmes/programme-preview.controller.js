(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("ProgrammeDetailPreview", ProgrammeDetailPreview);

  ProgrammeDetailPreview.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService",
    "programme"
  ];

  function ProgrammeDetailPreview($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, AlbumService, programme) {
    var vm = this;

    vm.entity = entity;
    vm.programme = programme;
    vm.albums = AlbumService.getAlbums({
      uuid: vm.entity.uuid
    });
    console.log(vm.albums);
    vm.programmeParams = [];
    vm.inclusionParams = [];
    vm.exclusionParams = [];
    vm.requirementParams = [];
    vm.otherParams = [];

    vm.progId = $stateParams.progId;
    vm.packageId = $stateParams.id;

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;
    vm.genPdf=genPdf;
    initProgrammeParams();

    function genPdf(doc){
      console.log("ready to generate..");
      console.log(doc);
    }

    function initProgrammeParams() {
      console.log("here");
      vm.programmeParams = vm.programme.programmeParams;
      angular.forEach(vm.programmeParams, function (param) {
        if (param.type === "INCLUSION") {
          vm
            .inclusionParams
            .push(param);
        } else if (param.type === "EXCLUSION") {
          vm
            .exclusionParams
            .push(param);
        } else if (param.type === "REQUIREMENTS") {
          vm
            .requirementParams
            .push(param);
        } else if (param.type === "OTHERS") {
          vm
            .otherParams
            .push(param);
        }

      });
    }
  }
})();
