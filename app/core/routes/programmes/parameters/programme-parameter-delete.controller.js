(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ProgrammeParameterDeleteController', ProgrammeParameterDeleteController);

  ProgrammeParameterDeleteController.$inject = ['$uibModalInstance', 'entity', 'ProgrammeParametersService', 'SupplementsService'];

  function ProgrammeParameterDeleteController($uibModalInstance, entity, ProgrammeParametersService, SupplementsService) {
    var vm = this;

    vm.entity = entity;
    // console.log('vm.entity');
    // console.log(vm.entity);
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;
    vm.entity.type !== 'EXLUSION' || vm.entity.type !== 'NOTE' ? vm.entity.displayName = vm.entity.name : vm.entity.displayName = vm.entity.description;

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function confirmDelete(contract) {
      if (vm.entity.type == 'EXLUSION' || vm.entity.type == 'NOTE' || vm.entity.type == 'INLUSION') {
        vm.id = contract;
        // console.log("deleting paramenter " + vm.id);
        ProgrammeParametersService.delete({
          id: vm.id
        }, onSaveSuccess, onSaveError);
      } else {
        vm.id = contract;
        console.log("deleting supplement " + vm.id);
        ProgrammeParametersService.delete({
          id: vm.id
        }, onSaveSuccess, onSaveError);
      }

      function onSaveSuccess(result) {
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }

      function onSaveError() {
        vm.isSaving = false;
      };
    }
  }
})();
