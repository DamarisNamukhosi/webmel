(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ProgrammeParameterDialogController', ProgrammeParameterDialogController);

  ProgrammeParameterDialogController.$inject = ['$timeout', '$stateParams', '$uibModalInstance', 'entity', 'ProgrammeParametersService'];

  function ProgrammeParameterDialogController($timeout, $stateParams, $uibModalInstance, entity, ProgrammeParametersService) {
    var vm = this;

    vm.entity = entity;
    console.log('entity');
    console.log(vm.entity);
    vm.entity.type= $stateParams.paramType;

    //init variables
    vm.existencies = [];

    //declare functions
    vm.clear = clear;
    vm.save = save;

    initExistences();

    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;

      console.log('parameter');
      console.log(vm.entity);

      if (vm.entity.id !== null) {
        ProgrammeParametersService.update(vm.entity, onSaveSuccess, onSaveError);
      } else {
        ProgrammeParametersService.create(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function initExistences() {
      if (vm.entity.type == 'DIMENSION') {
        if (vm.entity.parameter !== undefined && vm.entity.parameter !== null) {
          updateDimensionExistencesByType(vm.entity.parameter);
        }
      }
    }

  }
})();
