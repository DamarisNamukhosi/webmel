(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("ProgrammeItineraryController", ProgrammeItineraryController);

  ProgrammeItineraryController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService",
    "programme"
  ];

  function ProgrammeItineraryController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, AlbumService, programme) {
    var vm = this;

    vm.entity = entity;
    vm.programme = programme;
    console.log("vm.programme");
    console.log(vm.programme);
    vm.albums = AlbumService.getAlbums({
      uuid: vm.entity.uuid
    });
    console.log(vm.albums);
    vm.programmeParams = [];
    vm.inclusionParams = [];
    vm.exclusionParams = [];
    vm.requirementParams = [];
    vm.otherParams = [];

    vm.progId = $stateParams.progId;
    vm.packageId = $stateParams.id;

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;
    vm.exportToPdf = exportToPdf;
    vm.getImage = getImage;
    vm.data = null;
    vm.documentDefinition = [];
    vm.profileImageUrl = URLS.PROFILE_IMAGE_URL;
    initProgrammeParams();

    function getImage(uuid) {
      console.log("getImage called");
      console.log(uuid);
      var image1 = null;
      AlbumService.getProfile({
        uuid: uuid
      }).$promise.then(function (response) {
        var image1 = response;
        console.log("response");
        console.log(response);
      });
      return image1;
    }

    function exportToPdf() {

      var proxyURL = vm.profileImageUrl;
      console.log("ready to print");
      console.log(proxyURL);
      console.log("document.getElementById('exportthis')");
      console.log(document.images);
      var imgData = vm.data;
      vm.doc = new jsPDF();
      vm.i =0;
      var x =35;
      vm.interval= 0;
      angular.forEach(vm.programme.serviceGroups, function (serviceGroup) {
        console.log("i: " + vm.i);
        console.log(document.getElementsByTagName("img")[3+vm.interval].src);
        console.log(document.getElementsByTagName("img.img-responsive"));
        //.img-responsive
        getImageFromUrl(document.getElementsByTagName("img")[3+vm.interval].src.toString(), createPDF);
        vm.x = AlbumService.getProfile({
          uuid: serviceGroup.uuid
        }).$promise.finally(function (response) {
          deferred.resolve(response);
          console.log("deferred promise");
          $timeout(function () {
            deferred.resolve(response);
            console.log(deferred.resolve(response));
          }, 1000);
          return deferred.resolve(response);
        });
        vm.x.then(function (resultImage) {
          console.log("results");
          console.log(resultImage);
          html2canvas(resultImage, {
            allowTaint: false,
            logging: true,
            proxy: proxyURL,
            useCORS: true,
            crossOrigin: "Anonymous",
            onrendered: function (canvas) {
              console.log("canvas");
              console.log(canvas);
              vm.data = canvas.toDataURL("image/png");
              console.log("vm.data");
              console.log(vm.data);
              var temp = {
                content: [{
                  title: vm.programme.reference,
                  image: 'mySuperImage',
                  fit: [100, 100],
                  pageBreak: 'after'
                }],
                images: {
                  mySuperImage: vm.data,
                  fit: [100, 100]
                }
              };
              vm.documentDefinition.push(temp);
              console.log("documentDefinition");
              console.log(vm.documentDefinition);
              // doc.setFontSize(40)
              // doc.text(x, 25, vm.programme.reference)
              // doc.addImage(imgData, 'JPEG', 15, 40, 200, 160)
            }
          });
        });
        console.log("documentDefinition");
        console.log(vm.documentDefinition);
        console.log("ready to print");
        vm.i = vm.i +1;
        x =x + 10;
        vm.interval = vm.interval + 2;
      });
      // var pdfName = vm.programme.reference + "." + "pdf";
      // doc.save(pdfName.toString());
      // var imageCollection = document.images;
    }

    var getImageFromUrl = function(url, callback) {
      vm.loop = 0;
      console.log("getImage called");
      var img = new Image();
      img.onError = function() {
      alert('Cannot load image: "'+url+'"');
      };
      img.crossOrigin= "Anonymous";
      img.onload = function() {
      console.log("inside onload");
      console.log("url");
      console.log(url);
      console.log("vm.loop");
      console.log(vm.loop);
      callback(img);
      };
      img.src = url;
      }
      var createPDF = function(imgData) {
        
      var width = vm.doc.internal.pageSize.width;    
      var height = vm.doc.internal.pageSize.height;
      var options = {
           pagesplit: true
      };
      vm.doc.text(10, 20, 'Crazy Monkey');
      var h1=50;
      var aspectwidth1= (height-h1)*(9/16);
      console.log("imgData");
      console.log(imgData);
      vm.doc.addImage(imgData, 'JPEG', 10, h1, aspectwidth1, (height-h1), 'monkey');
      vm.doc.addPage();
      console.log("vm.i " + vm.i);
      vm.loop = vm.loop +1;
      if (vm.i == 8){
        console.log("vm.i" + vm.i);
        vm.doc.output('datauri');
      }
      // doc.text(10, 20, 'Hello World');
      // var h2=30;
      // var aspectwidth2= (height-h2)*(9/16);
      // doc.addImage(imgData, 'JPEG', 10, h2, aspectwidth2, (height-h2), 'monkey');
      // doc.output('datauri');
  }

    function initProgrammeParams() {
      vm.programmeParams = vm.programme.programmeParams;
      angular.forEach(vm.programmeParams, function (param) {
        if (param.type === "INCLUSION") {
          vm
            .inclusionParams
            .push(param);
        } else if (param.type === "EXCLUSION") {
          vm
            .exclusionParams
            .push(param);
        } else if (param.type === "REQUIREMENTS") {
          vm
            .requirementParams
            .push(param);
        } else if (param.type === "OTHERS") {
          vm
            .otherParams
            .push(param);
        }

      });
    }
  }
})();
