(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('ServiceGroupDialog', ServiceGroupDialog);

    ServiceGroupDialog.$inject = [
        '$timeout',
        '$scope',
        '$stateParams',
        '$uibModalInstance',
        'entity',
        'ServiceGroupsService',
        'LocationsService'
    ];

    function ServiceGroupDialog($timeout, $scope, $stateParams, $uibModalInstance, entity, ServiceGroupsService, LocationsService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.save = save;
        vm.getLocation = getLocation;
        vm.onDestinationSelected = onDestinationSelected;


        function onDestinationSelected($item, $model){
            vm.entity.destination = $item.id;
        }

        function getLocation(val) {
            return LocationsService
                .query({name: val})
                .$promise
                .then(function (results) {
                    return results.map(function (item) {
                        return item;
                    })
                });
        }

        $timeout(function () {
            angular
                .element('.form-group:eq(1)>input')
                .focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.entity.id !== null) {
                console.log(vm.entity);
                ServiceGroupsService.update(vm.entity, onSaveSuccess, onSaveError);
            } else {

                ServiceGroupsService.create(vm.entity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

    }
})();
