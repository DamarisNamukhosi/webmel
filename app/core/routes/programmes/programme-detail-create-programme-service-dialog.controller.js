(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('ProgrammeServiceDialogController', ProgrammeServiceDialogController);

  ProgrammeServiceDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$filter',
    '$localStorage',
    '$uibModalInstance',
    'entity',
    'generalServices',
    'OrganizationsService',
    'ContractsService',
    'DimensionSetsService',
    'ProgrammesService',
    'ProgrammeServicesService'
  ];

  function ProgrammeServiceDialogController($timeout, $scope, $stateParams, $filter, $localStorage, $uibModalInstance, entity, generalServices, OrganizationsService, ContractsService, DimensionSetsService, ProgrammesService, ProgrammeServicesService) {
    var vm = this;

    vm.entity = entity;
    vm.generalServices = generalServices;
    vm.svcInput = false;
    vm.contractInput = false;
    vm.svcSelect = true;
    vm.hideForEdit = true;
    vm.showForEditCustomValue = false;
    vm.providers = [];
    vm.contracts = [];
    vm.dimensionSet = [];
    vm.selectedContract = null;
    vm.selectedProviderDisabled = true;
    vm.selectedContractDisabled = true;
    vm.programme = null;
    vm.programmeSetDimensionExistencies = null;
    vm.programmeServiceParams = [];
    vm.programmeServiceParamsDTOList = [];
    vm.programServiceParamObject = {}
    vm.programmeServiceParamObject = {
      contentStatus: null,
      existenceId: null,
      existenceName: null,
      id: null,
      programmeServiceId: null,
      programmeServiceName: null,
      programmeServiceParamType: "DEFAULT",
      setDimensionId: null,
      setDimensionName: null,
      uuid: null
    };

    vm.clear = clear;
    vm.save = save;
    vm.hideSelectFields = hideSelectFields;
    vm.onChangeSvc = onChangeSvc;
    vm.onChangeProvider = onChangeProvider;
    vm.onChangeContract = onChangeContract;
    vm.initEditService = initEditService;
    vm.onProviderSelected = onProviderSelected;
    vm.getProvider = getProvider;
    vm.toggleCustomService = toggleCustomService;
    vm.toggleContractCostValue = toggleContractCostValue;

    initTimePicker();
    initEditService();

    function toggleContractCostValue() {
      vm.contractInput = !vm.contractInput;
    }

    function toggleCustomService() {
      vm.svcInput = !vm.svcInput;
    }

    function initEditService() {
      if (vm.entity.id !== null) {
        vm.hideForEdit = false;
      }

      if (vm.entity.id !== null && vm.entity.contractId == null) {
        vm.showForEditCustomValue = true;
      }
    }

    function onProviderSelected($item, $model) {
      console.log($item);
      vm.entity.destination = $item.id;
      vm.entity.providerId = $item.id;
      vm.entity.providerName = $item.name;
      ContractsService
        .getFiltered({
          supplier: $item.id
        })
        .$promise
        .then(function(data) {
          vm.contracts = data;
        });
    }

    function getProvider(val) {
      return OrganizationsService
        .filterByGeneralService({
          id: vm.entity.serviceId,
          name: val
        })
        .$promise
        .then(function(data) {
          return data.map(function(item) {
            return item;
          })
        });
    }

    function initTimePicker() {
      var timeNow = new Date();
      if (vm.entity.startTime == null || vm.entity.endTime == null) {
        vm.startTime = new Date(1970, 0, 1, timeNow.getHours(), timeNow.getMinutes(), 0);
        vm.endTime = new Date(1970, 0, 1, timeNow.getHours(), timeNow.getMinutes(), 0);
      } else {
        vm.startTime = parseTime(vm.entity.startTime);
        vm.endTime = parseTime(vm.entity.endTime);
      }
    }

    function parseTime(timeString) {
      var timeArray = new String(timeString);
      return new Date(1970, 0, 1, timeArray[0] + timeArray[1], timeArray[2] + timeArray[3]);
    }

    function onChangeContract() {
      vm.programmeServiceParamsDTOList = [];
      console.log(vm.selectedContract);
      if (vm.selectedContract != null) {
        var selectedContract = vm.selectedContract
        vm.entity.contractId = selectedContract.id;
        vm.entity.contractName = selectedContract.name;
        getDimensionSetData(selectedContract.dimensionSetId, selectedContract.year);
      } else {
        vm.selectedContractDisabled = false;
      }

    }

    function getDimensionSetData(dimensionSetId, year) {
      ProgrammesService
        .getFull({
          id: $stateParams.progId
        })
        .$promise
        .then(function(data) {
          DimensionSetsService
            .getSetDimensionExistenciesBySetId({
              id: data.setDimensionId,
              year: data.year
            })
            .$promise
            .then(function(data) {
              vm.programmeSetDimensionExistencies = data;
              //contract existencies
              DimensionSetsService
                .getSetDimensionExistenciesBySetId({
                  id: dimensionSetId,
                  year: year
                })
                .$promise
                .then(function(data) {
                  console.log(data);
                  // compare programme and contract existencies
                  compareExistencies(vm.programmeSetDimensionExistencies, data);

                });
            });
        });

    }

    function compareExistencies(programmeSetDimensionExistencies, contractSetDimensionExistencies) {
      var newArray = contractSetDimensionExistencies;
      angular.forEach(programmeSetDimensionExistencies, function(programmeExistence) {
        for (var i = 0; i < newArray.length; i++) {
          if (programmeExistence.dimensionId == newArray[i].dimensionId) {
            contractSetDimensionExistencies.splice(i, 1);
          }
        }
        console.log(contractSetDimensionExistencies);
      });
      convertToParamObjects(contractSetDimensionExistencies);
    }

    function convertToParamObjects(contractSetDimensionExistencies) {
      if (contractSetDimensionExistencies.length != 0) {
        angular
          .forEach(contractSetDimensionExistencies, function(obj) {
            if (obj.hasDefault != true) {
              if (obj.type != "EXCLUSIVE") {
                var param = {};
                param.existenceName = null;
                param.existenceId = null;
                param.setDimensionId = obj.dimensionId;
                param.setDimensionName = obj.dimensionName;
                param.existenceDTOList = obj.existenceDTOList;
                vm
                  .programmeServiceParamsDTOList
                  .push(param);
              }
            }
          });
      } else {
        console.log("Length is zero!!");
      }

    }

    function onChangeProvider() {
      vm.contracts = [];
      if (vm.selectedProvider != null) {
        vm.selectedProviderDisabled = true;
        vm.selectedContractDisabled = true;
        // var selectedProvider = JSON.parse(vm.selectedProvider);
        var selectedProvider = vm.selectedProvider
        vm.entity.providerName = selectedProvider.name;
        vm.entity.providerId = selectedProvider.id;
        ContractsService
          .getFiltered({
            supplier: selectedProvider.id
          })
          .$promise
          .then(function(data) {
            vm.contracts = data;
          });
      } else {
        vm.selectedContractDisabled = false;
        vm.selectedContract = "";
      }
    }

    function onChangeSvc() {
      if (vm.selectedService != null) {
        vm.selectedContractDisabled = true;
        var selectedService = vm.selectedService;
        vm.entity.serviceId = selectedService.id;
        vm.entity.serviceName = selectedService.name;
        vm.entity.name = selectedService.name;

      } else {
        vm.entity.serviceName = vm.selectedServiceInput;
        vm.entity.name = vm.selectedServiceInput;
        vm.selectedContractDisabled = false;
        vm.selectedContract == "";
      }

    }

    function hideSelectFields() {
      if (vm.selectInput = "") {
        vm.svcInput = true;
        vm.svcSelect = false;
      }
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function parseTimeForSave(stringTime) {
      console.log(stringTime);
      var timed = stringTime
        .toTimeString()
        .split(":");
      console.log(timed);
      return timed[0] + "" + timed[1];
    }

    function sanitizeData() {
      if(vm.entity.contractId == null) {
        //it means we have a custom provider
        //we shoud not expect the contractId or the contractName to be available.
        vm.entity.providerName = vm.provider;
      }
      //replicate serviceName to name
      vm.entity.name = vm.entity.serviceName;

      //save criticalInfo saveParams

      vm.entity.startTime = parseTimeForSave(vm.startTime);
      vm.entity.endTime = parseTimeForSave(vm.endTime);

      angular.forEach(vm.programmeServiceParamsDTOList, function(param) {
        // var obj = JSON.parse(param.existenceId);
        var obj = param.existenceId;
        var programmeServiceParamObject = {
          contentStatus: "DRAFT",
          existenceId: obj.id,
          existenceName: obj.name,
          id: null,
          programmeServiceId: null,
          programmeServiceName: null,
          programmeServiceParamType: "DEFAULT",
          setDimensionId: param.setDimensionId,
          setDimensionName: param.setDimensionName,
          uuid: null
        };
        vm
          .entity
          .programmeServiceParams
          .push(programmeServiceParamObject);
      });
    }

    function save() {
      sanitizeData();
      vm.isSaving = true;
      if (vm.entity.id !== null) {
        ProgrammeServicesService.updateFullService(vm.entity, onSaveSuccess, onSaveError);
      } else {
        ProgrammeServicesService.createFullService(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
})();
