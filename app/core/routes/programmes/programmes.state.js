(function () {
  "use strict";

  angular
    .module("webApp")
    .config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider.state('programme-galleries', {
      parent: 'packages-detail',
      url: '/{progId}/gallery',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'Gallery'
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/programmes/programme-galleries.html',
          controller: 'ProgrammeGalleriesController',
          controllerAs: 'vm'
        }
      },
      resolve: {
        programme: [
          "$stateParams",
          "ProgrammesService",
          function ($stateParams, ProgrammesService) {
            return ProgrammesService
              .getFull({id: $stateParams.progId})
              .$promise;
          }
        ],
        entity: [
          '$stateParams',
          'ProgrammesService',
          function ($stateParams, ProgrammesService) {
            return ProgrammesService
              .getById({id: $stateParams.id})
              .$promise;
          }
        ],
        previousState: [
          '$state',
          function ($state, $) {
            var currentStateData = {
              name: $state.current.name || 'programme-detail',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ]
      }
    }).state('programme-galleries.new', {
      parent: 'programme-galleries',
      url: '/new',
      data: {
        requiresAuthentication: true,
        authorities: []
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$uibModal',
        function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/albums/album-create-dialog.html',
            controller: 'AlbumCreateDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: [
                'ProgrammesService',
                function (ProgrammesService) {
                  return ProgrammesService
                    .getById({id: $stateParams.id})
                    .$promise;
                }
              ],
              album: function () {
                return {
                  albumType: 'GENERAL', caption: null, //album name
                  coverName: null, //uploaded cover image file name
                  coverUuid: null, //uploaded cover image
                  isDefaultAlbum: true, //put option slider
                  name: null, //album name
                  objectUuid: null //programme uuid
                };
              }
            }
            })
            .result
            .then(function () {
              $state.go('programme-galleries', null, {reload: 'programme-galleries'});
            }, function () {
              $state.go('^');
            });
        }
      ]
    }).state('programme-galleries.delete', {
      parent: 'programme-galleries',
      url: '/{albumId}/delete',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'programme-detail.detail.title'
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal
            .open({templateUrl: 'core/routes/albums/album-delete-dialog.html', controller: 'AlbumDeleteDialogController', controllerAs: 'vm', backdrop: 'static', size: 'md'})
            .result
            .then(function () {
              $state.go('programme-galleries', null, {reload: 'programme-galleries'});
            }, function () {
              $state.go('programme-galleries');
            });
        }
      ]
    }).state('programme-album-detail', {
      parent: 'programme-galleries',
      url: '/{albumId}/album',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'Gallery'
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/programmes/programme-album-detail.html',
          controller: 'ProgrammeAlbumDetailController',
          controllerAs: 'vm'
        }
      },
      resolve: {
        entity: [
          '$stateParams',
          'ProgrammesService',
          function ($stateParams, ProgrammesService) {
            return ProgrammesService
              .getById({id: $stateParams.id})
              .$promise;
          }
        ],
        album: [
          '$stateParams',
          'AlbumService',
          function ($stateParams, AlbumService) {
            return AlbumService
              .getAlbum({albumId: $stateParams.albumId})
              .$promise;
          }
        ],
        previousState: [
          '$state',
          function ($state, $) {
            var currentStateData = {
              name: $state.current.name || 'programme-detail',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ]
      }
    }).state('programme-album-detail.edit', {
      parent: 'programme-album-detail',
      url: '/edit',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'programme-detail.detail.title'
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/albums/album-dialog.html',
            controller: 'AlbumDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: [
                'AlbumService',
                function (AlbumService) {
                  return AlbumService
                    .get({id: $stateParams.albumId})
                    .$promise;
                }
              ]
            }
            })
            .result
            .then(function () {
              $state.go('programme-album-detail', null, {reload: 'programme-album-detail'});
            }, function () {
              $state.go('programme-album-detail');
            });
        }
      ]
    }).state('programme-album-detail.upload', {
      parent: 'programme-album-detail',
      url: '/new',
      data: {
        requiresAuthentication: true,
        authorities: []
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$uibModal',
        function ($stateParams, $state, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/albums/album-upload-dialog.html',
            controller: 'AlbumUploadDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'lg',
            resolve: {
              entity: [
                '$stateParams',
                'AlbumService',
                function ($stateParams, AlbumService) {
                  return AlbumService
                    .get({id: $stateParams.albumId})
                    .$promise;
                }
              ]
            }
            })
            .result
            .then(function () {
              $state.go('programme-album-detail', null, {reload: 'programme-album-detail'});
            }, function () {
              $state.go('^');
            });
        }
      ]
    }).state('programme-album-detail.makeCoverImage', {
      parent: 'programme-album-detail',
      url: '/{imageId}/cover-image',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'programme-detail.detail.title'
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
            controller: 'AlbumChangeCoverImageDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
              entity: [
                'AlbumService',
                '$stateParams',
                function (AlbumService, $stateParams) {
                  return AlbumService
                    .get({id: $stateParams.albumId})
                    .$promise;
                }
              ]
            }
            })
            .result
            .then(function () {
              $state.go('programme-album-detail', null, {reload: 'programme-album-detail'});
            }, function () {
              $state.go('programme-album-detail');
            });
        }
      ]
    }).state('programme-album-detail.deleteImage', {
      parent: 'programme-album-detail',
      url: '/{imageId}/delete',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'programme-detail.detail.title'
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal.open({
            templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
            controller: 'AlbumImageDeleteDialogController',
            controllerAs: 'vm',
            backdrop: 'static',
            size: 'md',
            resolve: {
              entity: [
                'AlbumService',
                '$stateParams',
                function (AlbumService, $stateParams) {
                  return AlbumService
                    .get({id: $stateParams.albumId})
                    .$promise;
                }
              ]
            }
            })
            .result
            .then(function () {
              $state.go('programme-album-detail', null, {reload: 'programme-album-detail'});
            }, function () {
              $state.go('programme-album-detail');
            });
        }
      ]
    }).state('programme-detail-preview', {
      parent: 'programme-detail',
      url: '/{progId}/preview',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: 'Gallery'
      },
      views: {
        'content@': {
          controller: 'ProgrammeDetailPreview',
          controllerAs: 'vm'
        }
      },
      resolve: {
        programme: [
          "$stateParams",
          "ProgrammesService",
          function ($stateParams, ProgrammesService) {
            return ProgrammesService
              .getFull({
                id: $stateParams.progId
              })
              .$promise;
          }
        ],
        entity: [
          "$stateParams",
          "ProgrammesService",
          function ($stateParams, ProgrammesService) {
            return ProgrammesService
              .getById({
                id: $stateParams.progId
              })
              .$promise;
          }
        ],
        previousState: [
          '$state',
          function ($state, $) {
            var currentStateData = {
              name: $state.current.name || 'programme-detail',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }
        ]
      }
    })
  }
})();
