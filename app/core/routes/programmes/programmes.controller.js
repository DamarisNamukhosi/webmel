(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ProgrammesController', ProgrammesController);

    ProgrammesController.$inject = ['$scope', '$location', '$state', 'programmes', 'URLS'];

    function ProgrammesController ($scope, $location, $state, programmes, URLS) {
        var vm = this;
        vm.programmes = programmes;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.resolvePackageTypeName=resolvePackageTypeName;
        vm.profile_url = URLS.PROFILE_IMAGE_URL;
        vm.packageName=''
        //
        //vm.packageTypes = [ {value: "SET_DEPARTURE", name:"Set Departure"},{value: "PRIVATE_DEPARTURE", name:"Private Departure"} ,{value:"FITs", name:"FIT"} ];
      
        vm.noRecordsFound= false;
        if(vm.programmes.length === 0){
            vm.noRecordsFound = true;
        }
        function resolvePackageTypeName(type){
                console.log(type);
                angular.forEach(vm.packageTypes, function(packageType){
                    if(type === packageType.value){
                        vm.packageName = packageType.name;
                    }
            });
            return vm.packageName;
        }
    }
})();
