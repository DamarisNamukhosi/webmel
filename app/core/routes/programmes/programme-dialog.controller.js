
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ProgrammeDialogController', ProgrammeDialogController);

  ProgrammeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$uibModalInstance', 'entity', 'ProgrammesService', 'DimensionSetsService', 'LocationsService', 'ServicesService', 'CurrenciesService'];

  function ProgrammeDialogController($timeout, $scope, $stateParams, $localStorage, $uibModalInstance, entity, ProgrammesService, DimensionSetsService, LocationsService, ServicesService, CurrenciesService) {
    var vm = this;

    vm.programme = entity;
    console.log('inside progs');
    console.log('vm.programmess');
    console.log(vm.programme);
    vm.clear = clear;
    vm.save = save;
    //initialization
    vm.programmeTypes = [{ value: "SET_DEPARTURE", name: "Set Departure" }, { value: "PRIVATE_DEPARTURE", name: "Private Departure" }, { value: "INDEPENDENT_TOUR", name: "FIT" }];
    vm.locations = [];
    vm.dimensionSets = [];
    vm.services = [];
    vm.currencies = [];
    vm.reqStr = '';
    vm.programmeDays = [];
    vm.setDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    vm.privateDays = ["Day1", "Day2", "Day3", "Day4", "Day5", "Day6", "Day7", "Day8", "Day9", "Day10"];
    $stateParams.type == "SET_DEPARTURE" ? vm.programmeDays = vm.setDays : vm.programmeDays = vm.privateDays;
    vm.fit = false;
    $stateParams.type == "INDEPENDENT_TOUR" ? vm.fit = true : vm.fit = false;
    //vm.programme.serviceId='';
    vm.time = null;
    vm.dimensionSetChange = dimensionSetChange;
    vm.contractParameters = [];
    vm.inPropertyOrgType = $localStorage.current_organisation.organisationTypeName == 'PROPERTY';
    vm.funcAsync = funcAsync;
    //vm.services = vm.services;
    vm.initCurrencies = initCurrencies;
    vm.onServiceChange = onServiceChange;
    vm.getLocation = getLocation;
    vm.onEndDestinationSelected = onEndDestinationSelected;
    vm.onstartDestinationSelected = onstartDestinationSelected;
    vm.parseTimeForSave = parseTimeForSave;
    //on load..
    initCurrencies();
    initOrgSerives();


    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }
    if (vm.programme.startDay !== null) {
      vm.programme.startDay = new Date(vm.programme.startDay);
      console.log(vm.programme.startDay);
      vm.time = vm.programme.startDay.getFullYear() + "-" + vm.programme.startDay.getMonth() + 1 + "-" + vm.programme.startDay.getDay;
    }

    function save() {
      console.log('programme');
      console.log(vm.programme);
      if(vm.programme.packageType == "INDEPENDENT_TOUR"){
        vm.programme.startDay = parseTimeForSave(vm.programme.startDay);
      }
      
      //vm.entity.endDate = new Date(vm.entity.endDate);
      console.log("vm.d");
      console.log(vm.d);
      vm.isSaving = true;

      if (vm.programme.year === null) {
        console.log('vm.programme.startDay');
        // vm.programme.startDay
        var startDayStr = vm.programme.startDay.split("-");
        vm.programme.year = startDayStr[0];

        console.log('vm.programme.year');
        console.log(vm.programme.year);
      }

      if (vm.programme.id !== null) {
        // vm.contractParameters // we need to copy from here
        console.log('vm.programme.id');
        console.log(vm.programme.id);
        vm.programme.programmeParams = []; //reset the programmeParams

        angular.forEach(vm.contractParameters, function (parameter) {

          var param = {};
          param.id = parameter.id;
          param.type = parameter.type;
          param.parameter = parameter.parameter;
          param.value = parameter.value;
          param.exclusive = parameter.exclusive;
          param.notes = parameter.notes;
          param.uuid = parameter.uuid;
          param.contractId = parameter.contractId;

          vm.programme.contractParams.push(param);

        });

        //delete the parameters that is not needed
        delete vm.programme.rates;
        delete vm.programme.supplements;


        console.log("programme before update");
        console.log(vm.programme);
        ProgrammesService.updateFull(
          vm.programme,
          onSaveSuccess,
          onSaveError
        );
      } else {

        var year = vm.programme.year;
        // var market = resolveMarket(vm.programme.marketId);
        var service = resolveService(vm.programme.serviceId);
        var supplier = resolveSupplier(vm.programme.issuerId);
        // var target = resolveTarget(vm.programme.targetId);
        var type = vm.programme.pakageType;


        //set the default contract name
        vm.programme.name = year + " " + service + " " + type + " rates for " + supplier + " issued to " + " ";

        //prpepare contractParamDTOList
        vm.programme.contractParamDTOList = [];

        // vm.contractParameters // we need to copy from here
        angular.forEach(vm.contractParameters, function (parameter) {
          var param = {
            "id": null,
            "exclusive": parameter.exclusive,
            "contractId": null,
            "contractName": null,
            "notes": parameter.notes,
            "parameter": parameter.parameter,
            "type": "DIMENSION",
            "uuid": null,
            "value": parameter.value
          };
          vm.programme.contractParams.push(param);
        });

        // vm.contractParams // we need to copy from here
        angular.forEach(vm.contractParams, function (parameter) {
          if (parameter.value !== null) {
            var param = {
              "id": null,
              "exclusive": parameter.exclusive,
              "contractId": null,
              "contractName": null,
              "notes": parameter.notes,
              "parameter": parameter.parameter,
              "type": parameter.type,
              "uuid": null,
              "value": parameter.value
            };
            vm.programme.contractParams.push(param);
          }

        });
        console.log("programme before create");
        console.log(vm.programme);
        ProgrammesService.createFull(
          vm.programme,
          onSaveSuccess,
          onSaveError
        );
      }


    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
    function parseTimeForSave(stringTime) {
      var timed = new Date(stringTime.setDate(stringTime.getDate() + 1));
      var timed1 = new Date(timed.setDate(timed.getDate() + 1));
      var time = timed1.toISOString().split(":");
      console.log(timed);
      console.log(timed1);
      console.log(time);
      var nice = time[0].slice(0, 10).split("-");
      console.log(nice);
      vm.time = nice[0] + "-" + nice[1] + "-" + nice[2];
      console.log(vm.time);
      return vm.time;
    }
    function resolveService(id) {
      var value = '';
      if (!vm.inPropertyOrgType) {
        angular.forEach(vm.services, function (object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      }
      else {
        return vm.serviceName;
      }
    }

    function resolveSupplier(id) {
      var value = '';
      if (!vm.inPropertyOrgType) {
        angular.forEach(vm.suppliers, function (object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      } else {
        return $localStorage.current_organisation.name;
      }
    }
    function initOrgSerives() {
      ServicesService.getByOrganizationId({ id: $localStorage.current_organisation.id }).$promise.then(function (response) {
        angular.forEach(response, function (org) {
          vm.services.push(org);
        });
      });
      if (vm.programme.serviceId !== null) {
        vm.onServiceChange(vm.programme.serviceId);
      }
    }
    //getByOrganizationId
    function onServiceChange(svcId) {
      console.log("service change called" + svcId);
      vm.dimensionSets = [];
      DimensionSetsService.getByServiceId({ id: svcId }).$promise.then(function (data) {
        vm.dimensionSets = data;
        console.log("data");
        console.log(data);
      });
      // dimensionSetChange();
    }
    console.log("dimsets");
    console.log(vm.dimensionSets);
    
    function initCurrencies() {
      CurrenciesService.query().$promise.then(function (response) {
        angular.forEach(response, function (obj) {
          vm.currencies.push(obj);
        });
      });
    }
    //rates changed
    function dimensionSetChange(editFlag) {
      vm.contractParameters = [];
      console.log('changed dim set to : ' + vm.programme.setDimensionId);
      console.log('year' + vm.programme.year);
      console.log('seasongrp' + vm.programme.seasonGroupId);


      //get the set dimensions with their existences in this set
      DimensionSetsService.getSetDimensionExistenciesBySetId({
        id: vm.programme.setDimensionId,
        year: vm.programme.year
      }, function (data) {
        vm.setDimensionsWithExistences = data;

        console.log('vm.setDimensionsWithExistences');
        console.log(vm.setDimensionsWithExistences);


        var param = {
          "id": null,
          "exclusive": false,
          "contractId": null,
          "contractName": null,
          "notes": null,
          "parameter": null,
          "type": "DIMENSION",
          "uuid": null,
          "value": null
        };

        // vm.contractParams.push(param);


        if (editFlag) {

          //add logic for edit

          angular.forEach(vm.programme.programmeParams, function (parameter) {

            if (parameter.type == 'DIMENSION' && parameter.exclusive == false) {
              angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
                if (dimension.dimensionType == parameter.parameter) {
                  parameter.displayName = dimension.dimensionName;
                  parameter.existences = dimension.existenceDTOList;
                }
              });

              vm.contractParameters.push(parameter);
            }

          })
          console.log('vm.contractParameters');
          console.log(vm.contractParameters);
        } else {
          //logic for new

          //get the set dimensions for the dimensionSetId
          var selectedSet = null;
          angular.forEach(vm.dimensionSets, function (current) {
            if (current.id == vm.programme.setDimensionId) {
              selectedSet = current;

              angular.forEach(current.dimensionDTOList, function (dimension) {
                if (dimension.type == 'SUPPLEMENT' && dimension.hasDefault == true) {

                  var param = {
                    "type": "DIMENSION",
                    "parameter": dimension.dimensionType, // we are support the enum and not id
                    "displayName": dimension.dimensionName,
                    "disabled": true,
                    "value": null,
                    "exclusive": false,
                    "notes": null,
                    "uuid": null,
                    "contract": null,
                    "existences": resolveDimensionExistencesv1(dimension.dimensionId)
                  };
                  vm.contractParameters.push(param);
                }

              });

              console.log('parameters');
              console.log(vm.contractParameters);


            }
          });
        }
      });

    }
    function resolveDimensionExistencesv1(dimensionId) {
      var list = [];
      if (vm.setDimensionsWithExistences !== undefined) {
        angular.forEach(vm.setDimensionsWithExistences, function (dimension) {
          // console.log(dimension);
          if (dimensionId == dimension.dimensionId) {
            list = dimension.existenceDTOList;
          }
        })
      }
      return list;
    }

    function onEndDestinationSelected($item, $model) {
      vm.programme.endLocationId = $item.id;
      vm.programme.endLocationName = $item.name;
    }
    function onstartDestinationSelected($item, $model) {
      vm.programme.startLocationId = $item.id;
      vm.programme.startLocationName = $item.name;
    }
    vm.openFrom = function () {
      vm.openedFrom = true;
    };

    vm.openTo = function () {
      vm.openedTo = true;
    };
    //onstartDestinationSelected
    function getLocation(val) {
      return LocationsService
        .query({ name: val })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
    }
    function funcAsync(query) {
      vm.reqStr = query;
      console.log('query');
      console.log(vm.reqStr);
      vm.locations = [];
      if (vm.reqStr !== null && vm.reqStr !== ' ' && vm.reqStr !== "" && vm.reqStr !== "undefined") {
        LocationsService.generalFilter({
          name: vm.reqStr
        }).$promise.then(function (response) {
          console.log('async response');
          console.log(response);
          vm.locations = response;
          if (vm.programme.startLocationId !== null) {
            angular.forEach(vm.locations, function (location) {
              if (location.id === vm.programme.startLocationId) {
                vm.programme.startLocationName = location.name;
              }
            })
          }
          if (vm.programme.endLocationId !== null) {
            angular.forEach(vm.locations, function (location) {
              if (location.id === vm.programme.endLocationId) {
                vm.programme.endLocationName = location.name;
              }
            })
          }

        })
      }
    }
    vm.loc = null;
    function resolveLocationName(location) {
      console.log('resolving');
      console.log(location);
      vm.loc = JSON.parse(location);
      return vm.loc.name;
    }
  }

})();

