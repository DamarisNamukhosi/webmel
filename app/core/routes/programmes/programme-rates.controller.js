(function () {
  'use strict';

  angular.module('webApp').controller('ProgrammeRatesController', ProgrammeRatesController);

  ProgrammeRatesController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$localStorage',
    '$q',
    '$timeout',
    'previousState',
    'contract',
    'ContractsService',
    'URLS',
    'dimensionSet',
    'RatesService',
    'dimensions',
    'programme'
  ];

  function ProgrammeRatesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $q,
    $timeout,
    previousState,
    contract,
    ContractsService,
    URLS,
    dimensionSet,
    RatesService,
    dimensions,
    programme
  ) {
    var vm = this;
    //injected values
    vm.contract = contract;
    vm.cont
    vm.dimensionSet = dimensionSet;
    vm.dimensions = dimensions;
    vm.programme = programme;
    //declare functions
    vm.initSupplementDimensions = initSupplementDimensions;
    vm.filterSupplements = filterSupplements;
    vm.resolveExistenceName = resolveExistenceName;
    vm.resolveDimensionNameByType = resolveDimensionNameByType;
    vm.newSupplements = vm.contract.supplements.length == 0;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.contract.uuid;
    vm.cancel = cancel;
    vm.onStaticValueChange = onStaticValueChange;
    vm.onContractValueChange = onContractValueChange;
    vm.toggleCustomService = toggleCustomService;
    vm.initContractedValues = initContractedValues;
    //initialization
    //progId: vm.progId,contractId :vm.contractId,setId: vm.setId,seasonGroupId :vm.seasonGroupId, year : vm.year
    vm.contractedValues = [];
    vm.progId = vm.programme.id;
    vm.contractId = vm.programme.contractId;
    vm.setId = vm.prog
    vm.rates = [];
    $scope.isEdit = false;
    vm.supplementDimensions = [];
    vm.dimensionExclusiveParameters = [];
    vm.basicParameters = [];
    vm.notesParameters = [];
    vm.newRates = [];
    vm.ratesAvailable = false;
    vm.oldRates = [];
    vm.value = [];
    vm.myStack = [];
    vm.dimensionNotExclusiveParameters = [];
    vm.dimensionNotExclusiveParameterGroups = [];
    vm.firstRow = vm.dimensionSet[0].existenceDTOList;
    vm.dimLength = '';
    vm.contract.dynamicValue = 20;
    vm.contract.total = vm.contract.dynamicValue + vm.contract.staticValue;
    vm.figures = [];
    vm.thirdItem = {
      type: 'BASIC',
      existenceDTOList: [{
        id: 11,
        uuid: '00d50256-5675-4a38-a758-cddf3b79fb0d',
        name: 'Default',
        brief: null
      }]
    };
    var i = 0;
    vm.saved = false;

    var j = 0;
    vm.y = [1, 2, 3];
    vm.exiList = [];
    vm.exitList = [];
    vm.basicDim = [];
    vm.basicDimId = [];

    var temp = [];
    var count = 0;
    var groupSize = 2; // number of parameters in the basic information table
    vm.global = [];
    vm.contractedServiceParam = {
      programmeId: vm.programme.id,
      startDate: null,
      endDate: null,
      existenceDTOList: []
    };

    //call functions needed to initialize data 

    initSupplementDimensions();
    initParameters();
    initRates();
    //$scope.table = buildTable();


    function toggleCustomService(row, item, parent, index) {
      console.log("toggle..")
      console.log(row);
      console.log(item);
      console.log(parent);
      console.log(index);
      vm.date = item.brief;

      var param = {
        id: row.id,
        generalId: row.generalId,
        generalName: row.generalName,
        dimensionType: row.dimensionType,
        dimensionName: row.dimensionName,
        name: row.name,
        uuid: row.uuid,
        brief: row.brief,
        startDate: parseTimeForSave(vm.date.slice(0, 10)),
        endDate: parseTimeForSave(vm.date.slice(13, 26))
      };
      vm.x = RatesService.getContractedValue(vm.contractedServiceParam).$promise.finally(function (response) {
        deferred.resolve(response);
        console.log("deferred promise");
        $timeout(function () {
          deferred.resolve(response);
          console.log(deferred.resolve(response));
        }, 10000);
        return deferred.resolve(response);
      });

      vm.x.then(function (result) {
        vm.result = result;
        console.log(result.value);
        vm.dynamic[parent][index] = result.value;
        console.log("dynamic");
        console.log(vm.dynamic[parent][index]);
        vm.total[parent][index] = vm.dynamic[parent][index] + vm.static[parent][index];
        console.log(vm.total[parent][index]);
        vm.total[j].push(vm.static[j][k]+ vm.dynamic[j][k]);
        return vm.dynamic[j];
      });
      console.log("vm.dynamic[parent][index]");
      console.log(vm.dynamic);
      console.log("vm.x");
      console.log(vm.x);
      console.log("vm.val");
      console.log(vm.val);
      console.log("ready");
    }


    function parseTimeForSave(stringTime) {
      var timed = stringTime.split("/");
      return timed[2] + "-" + timed[1] + "-" + timed[0];
    }


    function onStaticValueChange(staticValue, parent, child) {
      console.log("rates static value changed." + "" + staticValue + " " + parent + " " + child);
      console.log(staticValue);
      console.log(child);
      console.log(parent);
      vm.contract.staticValue = staticValue;
      vm.contract.total = vm.contract.dynamicValue + vm.contract.staticValue;
      if (vm.dimLength === 3) {
        for (var i = 0; i < vm.thirdItem.existenceDTOList.length; i++) {

          vm.total[i] = new Array();
          for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
            vm.total[i][j] = new Array();
            for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
              // vm.static[i][j][k]= new Array();
              if (parent === j && child === k) {
                vm.static[j][k] = staticValue;
                console.log("hapa");
                console.log(vm.static[j][k]);
                console.log(vm.dynamic[j][k]);
                vm.total[j][k] = vm.static[j][k] + vm.dynamic[j][k];
              } else {
                vm.total[j][k] = vm.static[j][k] + vm.dynamic[j][k];
              }
              vm.static[i][j].push(vm.contract.staticValue);
              vm.total[i][j].push(vm.dynamic[i][j] + vm.static[i][j]);
            }
          }

        }
      }
      if (vm.dimLength === 2) {
        console.log("dimLength 2");
        for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
          // vm.total[j] = new Array();

          for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
            // vm.static[i][j][k]= new Array();
            if (parent === j && child === k) {
              // vm.dynamic[j][k] = 20;

              console.log("here");
              console.log(vm.static[j][k]);
              console.log(vm.dynamic[j][k]);
              if (vm.dynamic[j][k] == "undefined" || vm.dynamic[j][k] == null) {
                vm.total[j][k] = vm.static[j][k];
              } else {
                vm.total[j][k] = vm.static[j][k] + vm.dynamic[j][k];
              }
            } else {
              console.log("else" + "" + vm.total[j][k]);
              vm.total[j][k] = vm.total[j][k];
            }

          }
        }

        //}
      }

    }

    function onContractValueChange(dynamicValue, parent, child) {
      console.log("rates dynamic value changed." + "" + dynamicValue + " " + parent + " " + child);
      vm.action = "rev";
      vm.contract.dynamicValue = dynamicValue;
      vm.contract.total = vm.contract.dynamicValue + vm.contract.staticValue;
      for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
        // vm.total[j] = new Array();

        for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
          // vm.static[i][j][k]= new Array();
          if (parent === j && child === k) {
            // vm.dynamic[j][k] = 20;

            console.log("here");
            console.log(vm.static[j][k]);
            console.log(vm.dynamic[j][k]);
            if (vm.static[j][k] == "undefined" || vm.static[j][k] == null) {
              vm.total[j][k] = vm.dynamic[j][k];
            } else {
              vm.total[j][k] = vm.static[j][k] + vm.dynamic[j][k];
            }
          } else {
            console.log("else" + "" + vm.total[j][k]);
            vm.total[j][k] = vm.total[j][k];
          }

        }
      }
    }
    //onContractValueChange
    $scope.saving = function (figures, dynamic, stat, edit) {
      vm.fig = figures;
      vm.dyn = dynamic;
      vm.stat = stat;
      vm.edit = edit;
      console.log("saving");
      console.log(vm.fig);
      console.log(vm.dyn);
      console.log(vm.stat);
      if (vm.edit == true) {
        $scope.isEdit = !edit;
      } else {

        if (vm.dimLength === 3) {
          console.log('three array');
          console.log('figures');
          console.log(vm.fig);


          angular.forEach(value, function (obj, i) {
            console.log('i');
            console.log(index, ',', i); // 0, 1, 2, 3
            console.log('obj');
            console.log(obj); // A, B, C, D

            angular.forEach(obj, function (obj1, j) {
              console.log('j');
              console.log(index, ',', i, ',', j); // 0, 1, 2, 3
              console.log('obj1');
              console.log(obj1); // A, B, C, D
              if (vm.ratesAvailable === false) {

                console.log('rates not available...');
                console.log(vm.ratesAvailable);
                vm.rates.push({
                  'contractId': vm.contract.id,
                  'dim1': vm.exiList[0][j],
                  'dim2': vm.exiList[1][i],
                  'value': obj1
                });

              } else {
                console.log('rates available...');
                console.log(vm.ratesAvailable);
                angular.forEach(vm.oldRates, function (oldRate) {
                  if (oldRate.dim1 === vm.exiList[0][j] && oldRate.dim2 === vm.exiList[1][i]) {
                    if (oldRate.value !== obj1) {
                      oldRate.value = obj1;
                    }
                  }
                });

              }

            });

          });
          //
          if (vm.ratesAvailable === false) {
            console.log('no rates... create');
            RatesService.createList(vm.rates, onSaveSuccess, onSaveError);

            function onSaveSuccess(result) {
              //$scope.$emit('gatewayApp:locationUpdate', result);
              console.log('success');
              vm.saved = true;
            }

            function onSaveError() {
              console.log('error  ');
            }
          } else {
            console.log("updating...");
            console.log(vm.oldRates);
            RatesService.updateList(vm.oldRates, onSaveSuccess, onSaveError);

            function onSaveSuccess(result) {
              //$scope.$emit('gatewayApp:locationUpdate', result);
              console.log('success');
              vm.saved = true;
            }

            function onSaveError() {
              console.log('error  ');
            }
          }

        } else if (vm.dimLength === 2) {
          console.log("ready to save");
          if (vm.dimLength === 2) {
            for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {

              for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
                console.log(j + "" + k);
                console.log(vm.rates);
                console.log(vm.fig[j][k]);
                vm.rates.push({
                  'contractId': vm.programme.contractId,
                  'dim1': vm.dimensionSet[0].existenceDTOList[k].id,
                  'dim2': vm.dimensionSet[1].existenceDTOList[j].id,
                  'staticValue': vm.dyn[j][k],
                  'contractedValue': vm.stat[j][k],
                  'value': vm.fig[j][k]
                });
                console.log("vm.rates");
                console.log(vm.rates);
              }
            }
          }

          if (vm.ratesAvailable === false) {
            console.log('no rates... create');
            RatesService.createList(vm.rates, onSaveSuccess, onSaveError);

            function onSaveSuccess(result) {
              console.log('success');
              vm.saved = true;
            }

            function onSaveError() {
              console.log('error  ');
            }
          } else {
            console.log("updating...");
            console.log(vm.rates);
            RatesService.updateList(vm.rates, onSaveSuccess, onSaveError);

            function onSaveSuccess(result) {
              console.log('success');
              vm.saved = true;
            }

            function onSaveError() {
              console.log('error  ');
            }
          }
        }
      }

    }

    function cancel() {
      console.log('clicked');
      // $scope.table = buildTable();
      $scope.isEdit = false;
    }

    ContractsService.getOrganizationPolicy({
      organisationId: vm.contract.issuerId
    }).$promise.then(function (result) {
      vm.policy = result;
    });

    function initSupplementDimensions() {
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.type == 'SUPPLEMENT') {
          vm.supplementDimensions.push(dimension);
        }
      })
    }

    function initParameters() {

      angular.forEach(vm.contract.params, function (parameter) {
        if (parameter.type == 'DIMENSION') {
          if (parameter.exclusive == true) {
            vm.dimensionExclusiveParameters.push(parameter);
          } else {
            vm.dimensionNotExclusiveParameters.push(parameter);
          }
        } else if (parameter.type == "NOTE") {
          vm.notesParameters.push(parameter);
        } else {
          vm.basicParameters.push(parameter);
        }
      });


      //categorize the basi info parameters into groups
      angular.forEach(vm.dimensionNotExclusiveParameters, function (parameter) {

        var valueTmp = {};
        valueTmp.type = "value";
        valueTmp.value = parameter.value;
        valueTmp.dimensionType = parameter.parameter;

        var parameterTmp = {};
        parameterTmp.type = "parameter";
        parameterTmp.value = parameter.parameter;

        if ((count % groupSize) == 0 && count > 0) {
          vm.dimensionNotExclusiveParameterGroups.push(temp);
          temp = [];
        }

        temp.push(parameterTmp);
        temp.push(valueTmp);

        count = count + 1; //increment count 
      });

      if (temp.length > 0) {
        vm.dimensionNotExclusiveParameterGroups.push(temp);
      }

    }

    function initRates() {
      console.log("inside initRates");
      //check if there are rates
      if (vm.contract.rates.length == 0) {
        vm.ratesAvailable = false;
        console.log("no rates available");
        var i = 0;
        angular.forEach(vm.dimensionSet, function (set) {

          i++;
          if (i == 3) {
            vm.thirdItem = set;
          }
        });

        angular.forEach(vm.dimensionSet, function (dimension) {
          var j = 0;
          vm.exiList[j] = [];
          if (dimension.type == 'BASIC') {
            vm.basicDim.push(dimension);

            vm.basicDimId.push(dimension.id);

            vm.exist = dimension.existenceDTOList;

            angular.forEach(vm.exist, function (ex) {
              vm.exiList[j].push(ex.id);
            });
            vm.exitList.push(vm.exiList[j]);
            vm.exitList.push(vm.exiList[j]);

            j = j + 1;
          }
        });

        vm.dimLength = vm.basicDim.length;
        vm.staticValues = [];
        if (vm.dimLength == 3) {
          console.log('dimension length three');
          vm.inner = _.chunk(
            vm.value,
            vm.dimensionSet[0].existenceDTOList.length *
            vm.dimensionSet[1].existenceDTOList.length
          );
          console.log(vm.thirdItem);
          initValues();
          angular.forEach(vm.inner, function (obj) {
            vm.figures.push(
              _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
            );
          });
        } else if (vm.dimLength == 2) {
          vm.inner = vm.value;
          initValues();
          vm.figures.push(
            _.chunk(
              vm.inner,
              vm.dimensionSet[0].existenceDTOList.length
            )
          );

        } else if (vm.dimLength == 1) {
          vm.inner = vm.value;

          vm.figures.push(vm.inner);
        }

      } else {
        //if rates are present...
        vm.ratesAvailable = true;
        console.log(vm.ratesAvailable === true);
        vm.oldRates = contract.rates;
        angular.forEach(vm.oldRates, function (rate) {
          vm.value.push(rate.value);
        });
        angular.forEach(vm.dimensionSet, function (dimension) {
          if (dimension.type == 'BASIC') {
            vm.basicDim.push(dimension);

            vm.basicDimId.push(dimension.id);

            vm.exist = dimension.existenceDTOList;

            vm.exiList[j] = [];
            angular.forEach(vm.exist, function (ex) {
              vm.exiList[j].push(ex.id);
              vm.exitList.push(ex.id);

            });
            j++;
          }
        });

        vm.dimLength = vm.basicDim.length;
        if (vm.dimLength == 3) {

          vm.inner = _.chunk(
            vm.value,
            vm.dimensionSet[0].existenceDTOList.length *
            vm.dimensionSet[1].existenceDTOList.length
          );
          vm.figures = [];

          angular.forEach(vm.inner, function (obj) {
            vm.figures.push(
              _.chunk(obj, vm.dimensionSet[0].existenceDTOList.length)
            );
          });
        } else if (vm.dimLength == 2) {
          console.log("contract with dimLenth2");
          vm.inner = vm.value;
          console.log(vm.inner);
          vm.existence1 = vm.dimensionSet[1].existenceDTOList;
          vm.existence0 = vm.dimensionSet[0].existenceDTOList;
          console.log(vm.existence1);
          console.log(vm.existence0);
          vm.static = new Array();
          vm.dynamic = new Array();
          vm.total = new Array();
          vm.svcInput = new Array();
          for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
            vm.static[j] = new Array();
            vm.dynamic[j] = new Array();
            vm.total[j] = new Array();
            vm.svcInput[j] = new Array();
            for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
              angular.forEach(vm.oldRates, function (rate) {
                if (rate.dim1 === vm.dimensionSet[0].existenceDTOList[k].id && rate.dim2 === vm.dimensionSet[1].existenceDTOList[j].id) {

                  //vm.value.push(rate.value);
                  vm.static[j].push(rate.staticValue);
                  vm.dynamic[j].push(rate.contractedValue);
                  vm.svcInput[j][k] = true;
                  vm.total[j].push(rate.value);
                  console.log("dessimating");
                  console.log(vm.static[j][k]);
                  console.log(vm.dynamic[j][k]);
                  console.log(vm.total[j][k]);
                  console.log(j + "" + k);
                }
              });


              if (vm.existence0[k].dimensionType === "SEASON") {
                vm.dates = vm.existence0[k].brief;
                console.log("checking for seasons");
                console.log(vm.existence0[k]);
                vm.contractedServiceParam.startDate = parseTimeForSave(vm.dates.slice(0, 10));
                vm.contractedServiceParam.endDate = parseTimeForSave(vm.dates.slice(13, 26));
              }

              var param = {
                id: vm.existence1[j].id,
                generalId: vm.existence1[j].generalId,
                generalName: vm.existence1[j].generalName,
                dimensionType: vm.existence1[j].dimensionType,
                dimensionName: vm.existence1[j].dimensionName,
                name: vm.existence1[j].name,
                uuid: vm.existence1[j].uuid,
                brief: vm.existence1[j].brief
              };
              vm.contractedServiceParam.existenceDTOList.push(param);
              console.log("vm.contractedServiceParam");
              console.log(vm.contractedServiceParam);

              RatesService.getContractedValue(vm.contractedServiceParam).$promise.then(function (response) {
                console.log("de response");
                console.log(response);
              });
            }
          }

          vm.total = _.chunk(
            vm.inner,
            vm.dimensionSet[0].existenceDTOList.length
          );
          console.log("vm.inner");
          console.log(vm.inner);
          console.log("vm.total");
          console.log(vm.total);
        } else if (vm.dimLength == 1) {
          vm.inner = vm.value;
          vm.figures = [];

          vm.figures.push(vm.inner);
        }
      }
    } //end initialization of rates
    function initValues() {
      vm.action = "gen";
      vm.static = new Array();
      vm.dynamic = new Array();
      vm.total = new Array();
      vm.svcInput = new Array();
      vm.number = vm.dimensionSet[0].existenceDTOList.length * vm.dimensionSet[1].existenceDTOList.length;
      if (vm.dimLength === 3) {
        for (var i = 0; i < vm.thirdItem.existenceDTOList.length; i++) {
          vm.static[i] = new Array();
          vm.dynamic[i] = new Array();
          vm.total[i] = new Array();
          for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
            vm.static[i][j] = new Array();
            vm.dynamic[i][j] = new Array();
            vm.total[i][j] = new Array();
            for (var k = 0; k < vm.dimensionSet[0].existenceDTOList.length; k++) {
              // vm.static[i][j][k]= new Array();
              vm.static[i][j].push(vm.contract.staticValue);
              // vm.dynamic[i][j].push(20);
              vm.total[i][j].push(vm.dynamic[i][j] + vm.static[i][j]);
            }
          }

        }
      }
      if (vm.dimLength === 2) {
        console.log("init values");
        console.log("inside dimLength2");
        vm.existence1 = vm.dimensionSet[1].existenceDTOList;
        vm.existence0 = vm.dimensionSet[0].existenceDTOList;
        // for (var j = 0; j < vm.dimensionSet[1].existenceDTOList.length; j++) {
        var j = 0;
        var k = 0;

        angular.forEach(vm.dimensionSet[1].existenceDTOList, function (set1) {
          vm.static[j] = new Array();
          vm.dynamic[j] = new Array();
          vm.total[j] = new Array();
          vm.svcInput[j] = new Array();

          angular.forEach(vm.dimensionSet[0].existenceDTOList, function (set0) {
            console.log("2nd loop");
            console.log(vm.contractedServiceParam);
            vm.contractedServiceParam = {
              programmeId: vm.programme.id,
              startDate: null,
              endDate: null,
              existenceDTOList: []
            };            console.log("reset");
            console.log(vm.contractedServiceParam)
            console.log("dimSetTwo" + " " + j);
            console.log(set1);
            console.log(vm.contractedServiceParam);
            vm.contractedServiceParam.existenceDTOList.push(set1);
            console.log("vm.contractedServiceParam" + " " + j);
            console.log(vm.contractedServiceParam);
            console.log(set1);
            console.log("dimSetOne" + " " + k);
            console.log(set0);
            vm.static[j].push(vm.contract.staticValue);
            vm.svcInput[j][k] = true;
            //vm.total[j].push(vm.static[j][k]);

            console.log(vm.static[j][k]);
            console.log(vm.dynamic[j][k]);
            console.log(j + "" + k);

            if (set0.dimensionType === "SEASON") {
              vm.dates = set0.brief;
              console.log("checking for seasons");
              console.log(vm.dates);
              //2018, 2-10-31"
              //2018-07-01"
              console.log(set0);
              vm.contractedServiceParam.startDate = parseTimeForSave(vm.dates.slice(0, 10));
              vm.contractedServiceParam.endDate = parseTimeForSave(vm.dates.slice(13, 23));
            }

            console.log("vm.contractedServiceParam");
            console.log(vm.contractedServiceParam);
            vm.global.push(vm.contractedServiceParam);
            console.log("vm.global");
            console.log(vm.global);


            vm.contractedValues = initContractedValues(j,k);
            console.log("vm.contractedValues");
            console.log(vm.contractedValues);
            k = k + 1;

          });
          k = 0;
          j = j + 1;
        });


        console.log("vm.contractedServiceParam");
        console.log(vm.global);
      }
    }

    function initContractedValues(j,k) {
      var deferred = $q.defer();
      console.log("deferred");
      console.log(deferred);
      vm.result = null;
      vm.x = RatesService.getContractedValue(vm.contractedServiceParam).$promise.finally(function (response) {
        deferred.resolve(response);
        console.log("deferred promise");
        $timeout(function () {
          deferred.resolve(response);
          console.log(deferred.resolve(response));
        }, 10000);
        return deferred.resolve(response);
      });
      console.log("vm.x");
      console.log(vm.x);
      vm.x.then(function (result) {
        vm.result = result;
        vm.dynamic[j].push(result.value);
        console.log(result.value);
        console.log(vm.dynamic[j][k]);
        console.log(vm.total[j][k]);
        vm.total[j].push(vm.static[j][k]+ vm.dynamic[j][k]);
        return vm.dynamic[j];
      });
      console.log("ready");
      console.log(vm.dynamic[j]);
      return vm.dynamic;
    }

    function filterSupplements(dimensionType) {
      var list = [];

      angular.forEach(vm.contract.supplements, function (supplement) {
        if (supplement.dimensionType1 == dimensionType) {
          list.push(supplement);
        }
      });
      return list;

    }

    function resolveDimensionNameByType(type) {
      var name = type;
      angular.forEach(vm.dimensions, function (dimension) {
        if (dimension.type == type) {
          name = dimension.name;
        }
      });

      return name;
    }

    function resolveExistenceName(dimensionType, existenceId) {
      var name = existenceId;
      angular.forEach(vm.dimensionSet, function (dimension) {
        if (dimension.dimensionType == dimensionType) {
          angular.forEach(dimension.existenceDTOList, function (existence) {

            if (existence.id == existenceId) {
              name = existence.name;
            }

          });
        }
      });
      return name;
    }
  }

})();
