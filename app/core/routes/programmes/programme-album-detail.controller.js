(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ProgrammeAlbumDetailController', ProgrammeAlbumDetailController);

  ProgrammeAlbumDetailController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    '$localStorage',
    'previousState',
    'entity',
    'URLS',
    'album'
  ];

  function ProgrammeAlbumDetailController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, album) {
    var vm = this;

    vm.entity = entity;
    vm.album = album;
    vm.previousState = previousState.name;

    vm.progId = $stateParams.progId;

    vm.packageId = $stateParams.id;

  }
})();
