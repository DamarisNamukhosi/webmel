(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('ServiceGroupsService', ServiceGroupsService);

  ServiceGroupsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ServiceGroupsService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      //

      'getFullByProgId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups/filter-full-by-programme/:id'
      },
      'getByOranizationId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups/filter-by-organisation/:id'
      },
      'createFull': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups/create-full'
      },
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray:false,
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-service-groups/:id'
      }
    });
  }
})();
