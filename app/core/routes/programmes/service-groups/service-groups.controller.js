(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ServiceGroupsController', ServiceGroupsController);

  ServiceGroupsController.$inject = [
    '$scope',
    '$location',
    '$state',
    '$stateParams',
    'entity',
    'programme',
    '$localStorage',
    'URLS',
    'SeasonBandsService',
    'ServiceGroupsService'
  ];

  function ServiceGroupsController($scope, $location, $state, $stateParams, entity, programme, $localStorage, URLS, SeasonBandsService, ServiceGroupsService) {
    var vm = this;

    vm.account = $localStorage.current_organisation;
    vm.programme = programme;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
    vm.isAuthenticated = null;
    vm.entity = entity;
    console.log(vm.entity);
    vm.serviceGroups=entity;
    console.log(vm.serviceGroups);
    vm.entities = [];
    vm.loadPage = loadPage;
    vm.transition = transition;
    vm.loadAll = loadAll;
    vm.itemsPerPage = 4;
    vm.years = [];
    vm.pakageId = $stateParams.id;
    vm.deleteBand = deleteBand;
    vm.search = search;
    vm.serviceGroup = [];
    vm.programmedName = '';
    vm.programmedId = '';

    vm.serviceGroup= [];
    vm.programmedName='';
    vm.programmedId ='';
    vm.progId=$stateParams.progId;
    //loadAll();
    initYears();
    initServiceGroups();

    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 5; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function initServiceGroups() {
      if (vm.entity.length !== 0) {
        vm.programmedName = vm.entity[0].programmeName;
        vm.programmedId = vm.entity[0].programmeId;
      }
      console.log(vm.programmedId);
      angular.forEach(vm.entity, function (group) {
        vm
          .serviceGroup
          .push(group);
      });
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function loadAll() {
      SeasonGroupsService
        .getByOranizationId({
          id: $localStorage.current_organisation.id,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage
        }, function (result, headers) {

          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.entities = result;
        });
    }

    function transition() {
      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse
          ? 'asc'
          : 'desc'),
        search: vm.currentSearch
      });
    }

    function deleteBand(bandId, season, entity, $index) {
      //deleteBand
      SeasonBandsService
        .delete({
          id: bandId
        }, function (result) {
          season
            .seasonBandDTOList
            .splice($index, 1);
        });

      var index = -1;

      //deleteSeason
      if (season.seasonBandDTOList.length == 1) {
        ServiceGroupsService
          .delete({
            id: season.id
          }, function (Result) {
            entity
              .serviceGroups
              .some(function (obj, i) {
                return obj.id == season.id
                  ? (index = i)
                  : false;
              });
            entity
              .serviceGroups
              .splice(index, 1);
          });
      }
    }

    function search() {
      SeasonGroupsService
        .getByOranizationId({
          year: vm.year,
          id: $localStorage.current_organisation.id,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage
        }, function (result, headers) {

          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.entities = result;
        });
    }
  }
})();
