(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("ProgrammeGalleriesController", ProgrammeGalleriesController);

  ProgrammeGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    "URLS",
    "AlbumService",
    "programme"
  ]; //

  function ProgrammeGalleriesController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, AlbumService, programme) {
    var vm = this;

    vm.entity = entity;
    vm.programme = programme;
    console.log(vm.programme);
    vm.albums = AlbumService.getAlbums({uuid: vm.programme.uuid});
    console.log(vm.albums);

    vm.packageId = vm.programme.id;
    vm.progId = vm.programme.id;
    vm.contractId = vm.programme.contractId;
    vm.setId = vm.programme.setDimensionId;
    vm.seasonGroupId = vm.programme.seasonGroupId
    vm.year = vm.programme.year;

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;
  }
})();
