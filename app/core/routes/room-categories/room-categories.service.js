(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('RoomCategoriesService', RoomCategoriesService);

  RoomCategoriesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function RoomCategoriesService ($resource, $localStorage, URLS) {
    var resourceUrl =  'data/data.json';

    return $resource(resourceUrl, {}, {
      'getRoomCategories': {
        method: 'GET',
        headers : {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories'
      },
      'getRoomCategoriesByOrganizationId': {
        method: 'GET',
        headers : {
          'Authorization': 'Bearer '+ $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories/filter-by-organisation/:id'
      },
      'get': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'contentservice/api/room-categories/:id'
      },
      'getById': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories/:id'
      },
      'update': {
        method: 'PUT',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/room-categories'
      },
      'create': {
          method: 'POST',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/room-categories'
      },
      'delete': {
        method: 'DELETE',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/room-categories/:id'
      },
      //updateList
      'updateList': {
        method: 'PUT',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories/update-list'
      },
    });
  }
})();
