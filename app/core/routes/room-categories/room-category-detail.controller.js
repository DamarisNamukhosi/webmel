(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("RoomCatogoryDetailController", RoomCatogoryDetailController);

  RoomCatogoryDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "rooms",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function RoomCatogoryDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    rooms,
    ObjectLabelsService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;
    vm.roomCategory = entity;
    console.log(vm.roomCategory);
    vm.rooms = rooms;
    //console.log(entity);
    vm.roomsFound = true;
    console.log(vm.roomsFound);
    if (vm.rooms.length === 0) {
      vm.roomsFound = false;
    }
    //vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.roomCategory.uuid;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;

    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({
      uuid: vm.roomCategory.uuid
    });
    console.log("object labels...");

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({
      uuid: vm.roomCategory.uuid
    });
    console.log("object labels...");
    console.log(vm.objectFeatures);

    vm.previousState = previousState.name;
  }
})();
