(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "RoomCategoriesDeleteDialogController",
      RoomCategoriesDeleteDialogController
    );

  RoomCategoriesDeleteDialogController.$inject = [
    "$uibModalInstance",
    "entity",
    "RoomCategoriesService",
    "$state"
  ];

  function RoomCategoriesDeleteDialogController(
    $uibModalInstance,
    entity,
    RoomCategoriesService,
    $state
  ) {
    var vm = this;

    vm.category = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirmDelete(category) {
      category.contentStatus = "DELETED";
      RoomCategoriesService.update(category, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
