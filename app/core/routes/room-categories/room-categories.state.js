(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("room-categories", {
        parent: "app",
        url: "/room-categories",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/room-categories/room-categories.html",
            controller: "RoomCategoriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          roomCategories: [
            "$stateParams",
            "$localStorage",
            "RoomCategoriesService",
            function($stateParams, $localStorage, RoomCategoriesService) {
              return RoomCategoriesService.getRoomCategoriesByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("room-categories.new", {
        parent: "room-categories",
        url: "/new-categories",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Room detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-category-dialog.html",
                controller: "RoomCategoryDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      rank: null,
                      brief: null,
                      description: null,
                      notes: null,
                      roomCount: null,
                      roomArea: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      roomOccupancies: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("room-categories", null, { reload: "room-categories" });
                },
                function() {
                  $state.go("room-categories");
                }
              );
          }
        ]
      })
      .state("room-category-detail", {
        parent: "room-categories",
        url: "/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Room Category details"
        },
        views: {
          "content@": {
            templateUrl:
              "core/routes/room-categories/room-category-detail.html",
            controller: "RoomCatogoryDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomCategoriesService",
            function($stateParams, RoomCategoriesService) {
              return RoomCategoriesService.get({ id: $stateParams.id })
                .$promise;
            }
          ],
          rooms: [
            "$stateParams",
            "RoomsService",
            function($stateParams, RoomsService) {
              return RoomsService.getRoomsByCategory({ id: $stateParams.id })
                .$promise;
            }
          ],
          objectLabelsByObjectType: [
            "ObjectLabelsService",
            function(ObjectLabelsService) {
              return ObjectLabelsService.getLabelsByObjectType({
                objectType: "ROOM"
              }).$promise;
            }
          ],

          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "room-categories",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-category-detail.image", {
        parent: "room-category-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-categories-image-upload-dialog.html",
                controller: "RoomCategoriesImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  roomCat: [
                    "$stateParams",
                    "RoomCategoriesService",
                    function($stateParams, RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-category-detail.delete", {
        parent: "room-category-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Delete category ?"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            console.log("hello");
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-categories-delete-dialog.html",
                controller: "RoomCategoriesDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "RoomCategoriesService",
                    function($stateParams, RoomCategoriesService) {
                      return RoomCategoriesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-categories", {}, { reload: true });
                },
                function() {
                  $state.go("room-categories");
                }
              );
          }
        ]
      })
      .state("room-categories.edit", {
        parent: "room-category-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-category-dialog.html",
                controller: "RoomCategoryDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-categories-detail.edit", {
        parent: "room-category-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-category-dialog.html",
                controller: "RoomCategoryDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-categories.delete", {
        parent: "room-categories",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/room-categories/room-category-delete-dialog.html",
                controller: "RoomCategoryDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-categories", null, {
                    reload: "room-categories"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-categories-rooms", {
        parent: "room-categories",
        url: "/{id}/rooms",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Rooms"
        },
        views: {
          "content@": {
            templateUrl:
              "core/routes/room-categories/room-categories-rooms.html",
            controller: "RoomCategoriesRoomsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomCategoriesService",
            function($stateParams, RoomCategoriesService) {
              return RoomCategoriesService.get({ id: $stateParams.id })
                .$promise;
            }
          ],
          rooms: [
            "$stateParams",
            "RoomsService",
            function($stateParams, RoomsService) {
              return RoomsService.getRoomsByCategory({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                parent_name: $state.current.name || "room-categories",
                name: $state.current.name || "room-categories",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-categories-rooms.new", {
        parent: "room-category-detail",
        url: "/new-rooms",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$localStorage",
          "$state",
          "$uibModal",
          function($stateParams, $localStorage, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/rooms/room-dialog.html",
                controller: "RoomDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      identification: null,
                      brief: null,
                      description: null,
                      notes: null,
                      building: null,
                      floor: null,
                      floorPosition: null,
                      roomArea: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      roomCategoryId: $stateParams.id,
                      roomCategoryName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, { reload: "room-category-detail" });
                },
                function() {
                  $state.go("room-category-detail");
                }
              );
          }
        ]
      })
      .state("room-category-galleries", {
        parent: "app",
        url: "/room-categories/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl:
              "core/routes/room-categories/room-category-galleries.html",
            controller: "RoomCategoryGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomCategoriesService",
            function($stateParams, RoomCategoriesService) {
              return RoomCategoriesService.get({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-category-galleries.new", {
        parent: "room-category-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-galleries", null, {
                    reload: "room-category-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-category-galleries.delete", {
        parent: "room-category-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("room-category-galleries", null, {
                    reload: "room-category-galleries"
                  });
                },
                function() {
                  $state.go("room-category-galleries");
                }
              );
          }
        ]
      })
      .state("room-category-album-detail", {
        parent: "room-category-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl:
              "core/routes/room-categories/room-category-album-detail.html",
            controller: "RoomCategoryAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "RoomCategoriesService",
            function($stateParams, RoomCategoriesService) {
              return RoomCategoriesService.get({ id: $stateParams.id })
                .$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "room-category-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("room-category-album-detail.edit", {
        parent: "room-category-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "room-category-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AlbumService",
                    function(AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-album-detail", null, {
                    reload: "room-category-album-detail"
                  });
                },
                function() {
                  $state.go("room-category-album-detail");
                }
              );
          }
        ]
      })
      .state("room-category-album-detail.makeCoverImage", {
        parent: "room-category-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-album-detail", null, {
                    reload: "room-category-album-detail"
                  });
                },
                function() {
                  $state.go("room-category-album-detail");
                }
              );
          }
        ]
      })
      .state("room-category-album-detail.deleteImage", {
        parent: "room-category-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-album-detail", null, {
                    reload: "room-category-album-detail"
                  });
                },
                function() {
                  $state.go("room-category-album-detail");
                }
              );
          }
        ]
      })
      .state("room-category-album-detail.upload", {
        parent: "room-category-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-album-detail", null, {
                    reload: "room-category-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-category-detail.addLabels", {
        parent: "room-category-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("room-category-detail.deleteLabel", {
        parent: "room-category-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("room-category-detail");
                }
              );
          }
        ]
      })
      .state("room-category-detail.addFeature", {
        parent: "room-category-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-create-dialog.html",
                controller: "ObjectFeatureCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  features: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "RoomCategoriesService",
                    function(RoomCategoriesService) {
                      return RoomCategoriesService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("room-category-detail");
                }
              );
          }
        ]
      })
      .state("room-category-detail.deleteFeature", {
        parent: "room-category-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/features/object-feature-delete-dialog.html",
                controller: "ObjectFeatureDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("room-category-detail", null, {
                    reload: "room-category-detail"
                  });
                },
                function() {
                  $state.go("room-category-detail");
                }
              );
          }
        ]
      });
  }
})();
