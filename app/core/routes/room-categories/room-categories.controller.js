(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomCategoriesController', RoomCategoriesController);

    RoomCategoriesController.$inject = ['$scope', '$location', '$state', 'roomCategories', 'URLS', 'RoomCategoriesService', 'TextUtil'];

    function RoomCategoriesController($scope, $location, $state, roomCategories, URLS, RoomCategoriesService, TextUtil) {
        var vm = this;
        vm.isAuthenticated = null;
        vm.roomCategories = roomCategories;
        vm.displayRoomCategories = true;
        vm.shorten = TextUtil.shorten;

        vm.roomCategoriesCount   = roomCategories.length;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;

        if (vm.roomCategoriesCount === 0){
            vm.displayRoomCategories = false;
        }

      vm.dragAlert = false;

      vm.sortableOptions = {
        stop: function (e, ui) {
          vm.dragAlert = false;
          updateRank();
        }
      }

      function updateRank() {
        console.log("Updating Rank");
        var count = 1;

        angular.forEach(vm.roomCategories, function (roomCategory) {

          roomCategory.rank = count * 100;

          count = count + 1;
        });

        console.log("vm.roomCategories");
        console.log(vm.roomCategories);


        // call update
        RoomCategoriesService.updateList(vm.roomCategories);

      }


                
    }
})();
