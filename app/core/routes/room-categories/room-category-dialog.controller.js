
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomCategoryDialogController', RoomCategoryDialogController);

    RoomCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams','$localStorage', '$uibModalInstance','$state', 'entity', 'RoomOccupanciesService', 'RoomCategoriesService'];

    function RoomCategoryDialogController ($timeout, $scope, $stateParams,$localStorage, $uibModalInstance, $state, entity, RoomOccupanciesService, RoomCategoriesService) {
        var vm = this;

        vm.roomCategory = entity;

        vm.clear = clear;
        vm.save = save;
        vm.roomOccupancies=[];
        vm.updateList = false;
        initRoomCategories();
        initRoomOccupancies();

        function initRoomOccupancies(){
        RoomOccupanciesService.getByOrganization({id: $localStorage.current_organisation.id}).$promise.then(function (response){
            vm.roomOccupancies = response;
                
        });
        console.log('vm.roomOccupancies');
        console.log(vm.roomOccupancies);
        }

        function initRoomCategories(){
            console.log("check length of roomCat object");
            if($state.current.name == "room-categories.editList"){
                vm.updateList = true;
                vm.sortableOptions = {
                    stop: function (e, ui) {
                      updateRank();
                    }
                  }
              
            }
        }


        function updateRank() {
            var count = 1;
      
            angular.forEach(vm.roomCategory, function (cat) {
      
              cat.rank = count * 1;
      
              count = count + 1;
            });
          }
      
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if(vm.updateList == false){
            if (vm.roomCategory.id !== null) {
                RoomCategoriesService.update(vm.roomCategory, onSaveSuccess, onSaveError);
            } else {
                RoomCategoriesService.create(vm.roomCategory, onSaveSuccess, onSaveError);
            }
        }else{
            console.log("ready to updatelist");
            console.log(vm.roomCategory);
            RoomCategoriesService.updateList(vm.roomCategory, onSaveSuccess, onSaveError);
        }
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();


