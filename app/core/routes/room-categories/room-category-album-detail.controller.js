(function() {
  'use strict';

  angular
      .module('webApp')
      .controller('RoomCategoryAlbumDetailController', RoomCategoryAlbumDetailController);

      RoomCategoryAlbumDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$localStorage', 'previousState', 'entity', 'URLS', 'LocationsService', 'album'];

  function RoomCategoryAlbumDetailController($scope, $rootScope, $stateParams, $localStorage, previousState, entity, URLS, LocationsService, album) {
      var vm = this;


      vm.entity = entity;
      vm.album = album;
      console.log(album);
      vm.previousState = previousState.name;

      var permissions = $localStorage.current_user_permissions;

      /**
       * This hack checks whether the logged in user is a MASTER_USER
       * or not then allocates the appropriate sref for the
       * locations-view
       */

      vm.previousState = permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";

  }
})();
