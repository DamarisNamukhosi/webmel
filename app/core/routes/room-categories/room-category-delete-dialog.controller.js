(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RoomCategoryDeleteDialogController',RoomCategoryDeleteDialogController);

    RoomCategoryDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'RoomCategoriesService'];

    function RoomCategoryDeleteDialogController($uibModalInstance, entity, RoomCategoriesService) {
        var vm = this;

        vm.roomCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RoomCategoriesService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
