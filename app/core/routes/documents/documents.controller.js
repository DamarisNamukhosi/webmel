(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentsController', DocumentsController);

    DocumentsController.$inject = ['$scope', '$location', '$state', 'documents'];

    function DocumentsController ($scope, $location, $state, documents) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.documents = documents;
        vm.noRecordsFound= false;

        if (vm.documents.length === 0){
            vm.noRecordsFound = true;
        }
    }
})();
