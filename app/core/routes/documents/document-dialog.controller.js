(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentDialogController', DocumentDialogController);

        DocumentDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DocumentsService','GeneralDocumentsService'];

    function DocumentDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DocumentsService,GeneralDocumentsService) {
        var vm = this;

        vm.document = entity;
        vm.clear = clear;
        vm.save = save;
        console.log('Documents');

        console.log(vm.document);
        
       vm.documentTypes = GeneralDocumentsService.query();
       
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.document.id !== null) {
                DocumentsService.update(vm.document, onSaveSuccess, onSaveError);
            } else {
                DocumentsService.create(vm.document, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
