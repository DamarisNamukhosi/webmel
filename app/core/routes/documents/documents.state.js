(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("documents", {
        parent: "app",
        url: "/documents",
        data: {
          requiresAuthentication: true,
          authorities: ["AUTHORITY_USER", "MASTER_USER"],
          pageTitle: "documents"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents/documents.html",
            controller: "DocumentsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          documents: [
            "$stateParams",
            "$localStorage",
            "DocumentsService",
            function($stateParams, $localStorage, DocumentsService) {
              return DocumentsService.getDocumentsByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("documents.new", {
        parent: "documents",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "documents.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-dialog.html",
                controller: "DocumentOwnDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      type: null,
                      renewalCycle: null,
                      objectType: null,
                      issuerId: $localStorage.current_organisation.id,
                      issuerName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("documents", null, {
                    reload: "documents"
                  });
                },
                function() {
                  $state.go("documents");
                }
              );
          }
        ]
      })
      .state("document-detail", {
        parent: "documents",
        url: "/{id}/document",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Documents Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents/document-detail.html",
            controller: "DocumentDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "$localStorage",
            "DocumentsService",
            function($stateParams, $localStorage, DocumentsService) {
              return DocumentsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          objectDocuments: [
            "$stateParams",
            "$localStorage",
            "DocumentsService",
            function($stateParams, $localStorage, DocumentsService) {
              return DocumentsService.getObjectDocuments({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "documents",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("documents-own-detail", {
        parent: "document-detail",
        url: "/{id}/objectdocument",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Documents Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents-own/document-own-detail.html",
            controller: "DocumentOwnDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "DocumentsOwnService",
            function($stateParams, DocumentsOwnService) {
              return DocumentsOwnService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "document-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("document-detail.profile", {
        parent: "document-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents/image-upload/image-upload-dialog.html",
                controller: "DocumentsImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    "$stateParams",
                    "DocumentsService",
                    function($stateParams, DocumentsService) {
                      return DocumentsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("document-detail", null, {
                    reload: "document-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("settings-documents-detail.suspend", {
        parent: "settings-documents-detail",
        url: "/suspend",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Suspend document ?"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            console.log("hello 2");
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/documents-own-suspend-dialog.html",
                controller: "DocumentOwnSuspendDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "DocumentsOwnService",
                    function($stateParams, DocumentsOwnService) {
                      console.log("hello");
                      return DocumentsOwnService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("settings-documents-detail", null, {
                    reload: "settings-documents-detail"
                  });
                },
                function() {
                  $state.go("settings-documents-detail");
                }
              );
          }
        ]
      })
      .state("settings-documents-detail.delete", {
        parent: "settings-documents-detail",
        url: "/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Delete document ?"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-delete-dialog.html",
                controller: "DocumentOwnDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "DocumentsOwnService",
                    function($stateParams, DocumentsOwnService) {
                      return DocumentsOwnService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("settings-documents", {}, { reload: true });
                },
                function() {
                  $state.go("settings-documents");
                }
              );
          }
        ]
      })
      .state("object-document.new", {
        parent: "app",
        url: "document/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: " "
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/documents-own/document-dialog.html",
                controller: "DocumentDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      issuerId: $localStorage.current_organisation.uuid,
                      issuerName: null,
                      objectType: null,
                      renewalCycle: null,
                      type: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("document-detail", null, {
                    reload: "document-detail"
                  });
                },
                function() {
                  $state.go("document-detail");
                }
              );
          }
        ]
      })
      .state("documents.edit", {
        parent: "document-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$localStorage",
          "$state",
          "$uibModal",
          function($stateParams, $localStorage, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/documents/document-dialog.html",
                controller: "DocumentDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "DocumentsService",
                    function(DocumentsService) {
                      return DocumentsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("document-detail", null, {
                    reload: "document-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("documents-detail.edit", {
        parent: "documents-detail",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-dialog.html",
                controller: "DocumentOwnDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "DocumentsOwnService",
                    function(DocumentsOwnService) {
                      return DocumentsOwnService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("documents-detail", null, {
                    reload: "documents-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("documents.delete", {
        parent: "documents",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents/document-delete-dialog.html",
                controller: "DocumentDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "DocumentsService",
                    function(DocumentsService) {
                      return DocumentsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("documents", null, {
                    reload: "documents"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("document-galleries", {
        parent: "app",
        url: "/documents/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents/document-galleries.html",
            controller: "DocumentGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "DocumentsService",
            function($stateParams, DocumentsService) {
              return DocumentsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("document-galleries.new", {
        parent: "document-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "DocumentsService",
                    function(DocumentsService) {
                      return DocumentsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("document-galleries", null, {
                    reload: "document-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("document-album-detail", {
        parent: "document-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents/document-album-detail.html",
            controller: "DocumentAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "DocumentsService",
            function($stateParams, DocumentsService) {
              return DocumentsService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "document-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("document-album-detail.edit", {
        parent: "document-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "document-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService",function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("document-album-detail", null, { reload: "document-album-detail" });
              },
              function () {
                $state.go("document-album-detail");
              }
              );
          }
        ]
      })
      .state("document-album-detail.upload", {
        parent: "document-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("document-album-detail", null, {
                    reload: "document-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
  }
})();
