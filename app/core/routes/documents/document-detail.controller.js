(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentDetailController', DocumentDetailController);

    DocumentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'objectDocuments'];

    function DocumentDetailController($scope, $rootScope, $stateParams, previousState, entity, objectDocuments) {
        var vm = this;
        
        vm.generalDocument = entity;

        console.log('general Documents is ');
        console.log(vm.generalDocument);
        
        console.log('OBJECT Documents is ');
        vm.objectDocuments = objectDocuments;
        console.log(vm.objectDocuments);


        vm.objectDocumentsFound = true;

        console.log(vm.objectDocumentsFound);

        if(vm.objectDocuments.length === 0){
            vm.objectDocumentsFound = false;
            
        }

        console.log(vm.objectDocumentsFound);
        
        vm.previousState = previousState.name;
    }
})();