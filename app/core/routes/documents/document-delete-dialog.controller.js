(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentDeleteDialogController',DocumentDeleteDialogController);

        DocumentDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'DocumentsService'];

    function DocumentDeleteDialogController($uibModalInstance, entity, DocumentsService) {
        var vm = this;

        vm.document = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocumentsService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
