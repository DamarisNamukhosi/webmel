(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SeasonsController', SeasonsController);

  SeasonsController.$inject = [
    '$scope',
    '$location',
    '$state',
    'seasons',
    '$localStorage',
    'URLS',
    'SeasonBandsService',
    'SeasonsService',
    'ParseLinks',
    'pagingParams',
    'paginationConstants',
    'SeasonGroupsService'
  ];

  function SeasonsController($scope, $location, $state, seasons, $localStorage, URLS, SeasonBandsService, SeasonsService, ParseLinks, pagingParams, paginationConstants, SeasonGroupsService) {
    var vm = this;

    vm.account = $localStorage.current_organisation;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
    vm.isAuthenticated = null;

    vm.entities = [];
    vm.loadPage = loadPage;
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.transition = transition;
    vm.loadAll = loadAll;
    vm.itemsPerPage = 4;
    vm.years = [];

    vm.deleteBand = deleteBand;
    vm.search = search;

    loadAll();
    initYears();

    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 5; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function loadAll() {
      SeasonGroupsService
        .getByOranizationId({
          id: $localStorage.current_organisation.id,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage
        }, function (result, headers) {

          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.entities = result;
        });
    }

    function transition() {
      $state.transitionTo($state.$current, {
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse
          ? 'asc'
          : 'desc'),
        search: vm.currentSearch
      });
    }

    function deleteBand(bandId, season, entity, $index) {
      //deleteBand
      SeasonBandsService
        .delete({
          id: bandId
        }, function (result) {
          season
            .seasonBandDTOList
            .splice($index, 1);
        });

      var index = -1;

      //deleteSeason
      if (season.seasonBandDTOList.length == 1) {
        SeasonsService
          .delete({
            id: season.id
          }, function (Result) {
            entity
              .seasons
              .some(function (obj, i) {
                return obj.id == season.id
                  ? (index = i)
                  : false;
              });
            entity
              .seasons
              .splice(index, 1);
          });
      }
    }

    function search() {
      SeasonGroupsService
        .getByOranizationId({
          year: vm.year,
          id: $localStorage.current_organisation.id,
          page: pagingParams.page - 1,
          size: vm.itemsPerPage
        }, function (result, headers) {

          vm.links = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.entities = result;
        });
    }
  }
})();
