(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'SeasonGroupDeleteDialogController',
            SeasonGroupDeleteDialogController
        );

    SeasonGroupDeleteDialogController.$inject = [
        '$uibModalInstance',
        'SeasonGroupsService',
        'entity'
    ];

    function SeasonGroupDeleteDialogController(
        $uibModalInstance,
        SeasonGroupsService,
        entity
    ) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete() {
            entity.contentStatus = 'DELETED';
            SeasonGroupsService.update(entity, function() {
                $uibModalInstance.close(true);
            });
        }
    }
})();
