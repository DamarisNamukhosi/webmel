(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SeasonCreateDialogController', SeasonCreateDialogController);

  SeasonCreateDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$localStorage',
    '$uibModalInstance',
    'entity',
    'SeasonsService',
    'SeasonGroupsService',
    'SeasonBandsService'
  ];

  function SeasonCreateDialogController($timeout, $scope, $stateParams, $localStorage, $uibModalInstance, entity, SeasonsService, SeasonGroupsService, SeasonBandsService) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    if (vm.entity.name == null) {
      vm.entity.name = "Seasons";
    }
    vm.clear = clear;
    vm.save = save;
    vm.addSeason = addSeason;
    vm.addSeasonBandDTO = addSeasonBandDTO;
    vm.initSeasonBandDTO = initSeasonBandDTO;
    vm.onYearChange = onYearChange;
    vm.remove = remove;
    vm.isSaving = false;

    vm.openedFrom = false;
    vm.openedTo = false;
    vm.seasons = vm.entity.seasons;
    if (vm.seasons.length === 0) {
      vm.seasonBandDTO = vm.seasons[0].seasonBandDTOList;
    }

    vm.years = [];
    var i = 0;
    vm.removeBand = removeBand;
    //vm.toUTC = toUTC
    initSeasonBandDTO();
    initYears();
    initDatePickers();

    $timeout(function () {
      angular
        .element('.form-group:eq(1)>input')
        .focus();
    });

    vm.sortableOptions = {
      stop: function (e, ui) {
        updateRank();
      }
    }


    function updateRank() {
      var count = 1;

      angular.forEach(vm.entity.seasons, function (season) {

        season.rank = count * 100;

        count = count + 1;
      });
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function addSeason() {
      var temp = {
        // contentStatus: "DRAFT", fromDate: null, id: null, seasonId: null, toDate:
        // null, uuid: null
        abbreviation: null,
        contentStatus: "DRAFT",
        id: null,
        name: null,
        organisationId: null,
        organisationName: null,
        seasonBandDTOList: [{
          contentStatus: "DRAFT",
          fromDate: null,
          id: null,
          seasonId: null,
          seasonName: null,
          specialDayGroupId: null,
          specialDayGroupName: null,
          toDate: null,
          uuid: null,
          tag: 1
        }],
        seasonGroupId: null,
        seasonGroupName: null,
        uuid: null

      };
      vm
        .entity
        .seasons
        .push(temp);


      updateRank();

      console.log(vm.entity);
    }

    function addSeasonBandDTO($index, obj) {
      console.log($index);
      console.log(obj);
      var temp1 = {
        contentStatus: "DRAFT",
        fromDate: null,
        id: null,
        seasonId: null,
        seasonName: null,
        specialDayGroupId: null,
        specialDayGroupName: null,
        toDate: null,
        uuid: null
      };
      // vm.seasonBandDTO.push(temp1);
      vm.seasons[$index].seasonBandDTOList.push(temp1);

    }

    function remove(item) {
      var index = vm
        .entity
        .seasonBandDTOList
        .indexOf(item);
      vm
        .entity
        .seasonBandDTOList
        .splice(index, 1);
    }
    // vm.toUTC  =function(dt){
    //   console.log((i + 1));
    //   console.log("dt");
    //   console.log(dt);
    //   var rtn = new Date(dt);
    //   console.log("return");
    //   console.log(rtn);
    //   return rtn;

    // }

    function save() {
      vm.isSaving = true;
      console.log("vm.entity b4 dateFormate");
      console.log(vm.entity);
      formatDateBands();
      //      vm.time = vm.programme.startDay.getFullYear() + "-" + vm.programme.startDay.getMonth() + 1 + "-" + vm.programme.startDay.getDay;

      if (vm.entity.id != null) {
        console.log("vm.entity saving");
        console.log(vm.entity);
        SeasonGroupsService.updateFull(vm.entity, onSaveSuccess, onSaveError);
      } else {
        console.log("vm.entity updating");
        console.log(vm.entity);
        SeasonGroupsService.createFull(vm.entity, onSaveSuccess, onSaveError);
      }

    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
    vm.openFrom = function ($index, obj) {
      obj.openedFrom = true;
    };

    vm.openTo = function ($index, obj) {
      obj.openedTo = true;
    };

    function initSeasonBandDTO() {
      if (vm.entity.seasons.length > 1) {

        angular
          .forEach(vm.seasons, function (season) { });
      }
    }

    function formatDateBands() {
      console.log(vm.entity);
      angular.forEach(vm.entity.seasons, function (season) {
        //take the from time and the to time and format them
        angular
          .forEach(season.seasonBandDTOList, function (band) {
            var start = null;
            var end = null;
            start = moment(band.fromDate).format('YYYY-MM-DD');
            end = moment(band.toDate).format('YYYY-MM-DD');
            band.fromDate = start;
            band.toDate = end;
            //delete keys
            delete band.openedFrom;
            delete band.openedTo;
            delete band.tag;
          });
      });
    }

    function initDatePickers() {
      if (vm.entity.id === null || vm.entity.id === undefined) {
        console.log("new dates..");
        angular.forEach(vm.entity.seasons, function (season) {
          //take the from time and the to time and format them
          if (season.seasonBandDTOList.length !== 0) {
            angular
              .forEach(season.seasonBandDTOList, function (band) {
                vm.startDate = new Date();
                vm.startDate.setDate(vm.startDate.getDay() - 20)
                vm.endDate = new Date();
                band.fromDate = vm.startDate;
                band.toDate = vm.endDate;
              });
          }
        });
      } else {
        if (vm.entity.season !== undefined || vm.entity.season !== null) {
          console.log("init dates..");
          angular.forEach(vm.entity.seasons, function (season) {
            //take the from time and the to time and format them
            if (season.seasonBandDTOList.length !== 0) {
              angular
                .forEach(season.seasonBandDTOList, function (band) {
                  band.fromDate = new Date(band.fromDate);
                  //  vm.startDate.setDate(vm.startDate.getDay() - 20)
                  //vm.entity.startDate = new Date(vm.entity.startDate);
                  //vm.entity.endDate = new Date(vm.entity.endDate);
                  band.toDate = new Date(band.toDate);
                });
            }
          });
        }
      }
    }

    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 10; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function onYearChange(year) {
      console.log('year changed');
      vm.entity.name = year + " Seasons"
    }

    function removeBand($index, band, season) {


      if (band.id !== null) {
        SeasonBandsService
          .delete({
            id: band.id
          }, function (result) {

            var index = -1;

            season
              .seasonBandDTOList
              .some(function (obj, i) {
                return obj.id == band.id ?
                  (index = i) :
                  false;
              });
            season
              .seasonBandDTOList
              .splice(index, 1);


            // check if it's the last band
            if (season.seasonBandDTOList.length == 0) {
              removeSeason(season);
            }
          });
        //check if season has bands, delete if bandlength == 0
      } else {
        season
          .seasonBandDTOList
          .splice($index, 1);

        // check if it's the last band
        if (season.seasonBandDTOList.length == 0) {
          removeSeason(season);
        }
      }
    }


    function removeSeason(season) {
      console.log("Removing Season");
      var ind = -1
      if (season.id !== null) {
        console.log("Removing");
        season.contentStatus = "DELETED";
        console.log("ABout to Update");
        console.log(season);
        SeasonsService.update(season, function (result) {
          vm.entity.seasons.some(function (obj, i) {
            return obj.id == season.id ?
              (ind = i) :
              false;
          });
          vm
            .entity
            .seasons
            .splice(ind, 1);
        });
      } else {
        vm
          .entity
          .seasons
          .splice(ind, 1);
      }
    }
  }
})();
