(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('SeasonBandDialogController', SeasonBandDialogController);

    SeasonBandDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$uibModalInstance', 'entity', 'SeasonBandsService'];

    function SeasonBandDialogController($timeout, $scope, $stateParams, $localStorage, $uibModalInstance, entity, SeasonBandsService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.save = save;


        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.entity.id !== null) {
                SeasonBandsService.update(vm.entity, onSaveSuccess, onSaveError);
            } else {
                SeasonBandsService.create(vm.entity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();
