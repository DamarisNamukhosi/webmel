(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('SeasonBandDeleteDialogController', SeasonBandDeleteDialogController);

    SeasonBandDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'SeasonBandsService'];

    function SeasonBandDeleteDialogController($uibModalInstance, entity, SeasonBandsService) {
        var vm = this;

        vm.entity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete() {
            SeasonBandsService.delete({ id: vm.entity.id },
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
