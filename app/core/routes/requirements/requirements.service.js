(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('RequirementsService', RequirementsService);

        RequirementsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function RequirementsService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          getRequirements: {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,

            url: URLS.BASE_URL + 'contentservice/api/requirements/filter-by-organisation/:id'
          },
          getById: {
            method: "GET",
            headers: {
              Authorization: "Bearer " + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + "costingservice/api/safari-service-requirements/:id"
          },
          getByPackId: {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,

            url: URLS.BASE_URL + 'costingservice/api/safari-service-requirements/filter-by-pack/:id'
          },
          getByLocationId: {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,

            url: URLS.BASE_URL + 'costingservice/api/safari-service-requirements/filter-by-location/:id'
          },
          update: {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/safari-service-requirements'
          },
          create: {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/safari-service-requirements'
          },
          delete: {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/safari-service-requirements/:id'
          }
        });
    }
})();
