(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('RequirementsController', RequirementsController);

  RequirementsController.$inject = ['$scope', 'entity', '$state', '$stateParams', '$localStorage', 'URLS', 'RequirementsService','LocationsService'];

  function RequirementsController($scope, entity, $state, $stateParams, $localStorage, URLS, RequirementsService,LocationsService) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    vm.location= null;
    vm.country-= null;
    // vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;

    vm.isAuthenticated = null;
    console.log($stateParams)

    // check state on delete
    vm.deleteLocationsRequirementState = $state.current.name.includes("location-requirements")
    // check state on delete
    vm.WelcomePacksState = $state.current.name.includes("location-requirements")

    initCountry();

    function initCountry() {
      console.log(vm.locations);
      console.log($stateParams.countryId > 0);
      console.log($stateParams.countryId);
      console.log($stateParams);
      console.log($stateParams.locationId);
      vm.showCountry = true;
      if ($stateParams.countryId > 0) {
        LocationsService.get({
            id: $stateParams.countryId
          })
          .$promise.then(function (results) {
            vm.country = results;
          });
      }
      if ($stateParams.locationId > 0) {
        // LocationsService.getLocationsByParentIdAndType({
        //     id: $stateParams.locationId,
        //     types: 10
        //   })
        //   .$promise.then(function (results) {
        //     vm.locations = results;
        //   });
        LocationsService.get({
            id: $stateParams.locationId
          })
          .$promise.then(function (results) {
            vm.location = results;
          });
      }
    }
  }
})();
