(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "RequirementsDeleteDialogController",
      RequirementsDeleteDialogController
    );

  RequirementsDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "RequirementsService",
    "entity"
  ];

  function RequirementsDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    RequirementsService,
    entity
  ) {
    var vm = this;

    vm.entity = entity;

    vm.clickedItem = $stateParams.id;
    console.log("Id to delete: " + vm.clickedItem);

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    vm.save = function save() {
      vm.isSaving = true;
      // RequirementsService.delete({id: vm.clickedItem}, onSaveSuccess, onSaveError);
      //update the entity content-service to delete and send an update
      vm.entity.contentStatus = "DELETED";
      RequirementsService.update(vm.entity, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
