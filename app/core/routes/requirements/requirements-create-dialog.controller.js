(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("RequirementsCreateDialogController", RequirementsCreateDialogController);

  RequirementsCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "$state",
    "entity",
    "RequirementsService",
    "generalServices",
    "GeneralServicesService",
    "safariConstants",
    "WelcomePacksService",
    "ServicesService",
    "OrganizationsService",
    "location",
    "currencies"
  ];

  function RequirementsCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    $state,
    entity,
    RequirementsService,
    generalServices,
    GeneralServicesService,
    safariConstants,
    WelcomePacksService,
    ServicesService,
    OrganizationsService,
    location,
    currencies
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    console.log(vm.entity);
    vm.location = location;
    console.log(vm.location);
    vm.currencies = currencies;

    vm.generalServices = generalServices;
    console.log(vm.generalServices);


    vm.generalServiceType = safariConstants.generalServiceType;
    vm.safariServiceCategory = safariConstants.safariServiceCategory;
    vm.requirementType = safariConstants.requirementType;
    vm.objectType = safariConstants.objectType;


    //function declaration
    vm.getLocs = getLocs;
    vm.onTargetSelected = onTargetSelected;
    vm.issuerChanged = issuerChanged;
    vm.serviceChange = serviceChange;
    vm.genServiceChange = genServiceChange;
    vm.genServiceTypeChange = genServiceTypeChange;
    // check state on creating new
    //
    vm.createLocationRequirementState = angular.equals($state.current.name, "location-requirements.new") || angular.equals($state.current.name, "location-requirements.edit");
    vm.createWelcomePacksRequirementState = angular.equals($state.current.name, "requirements.new");


    initDetails();
    initSupplierIssuer();
    // vm.selectedPack = selectedPack;
    function initDetails() {
      if (vm.createWelcomePacksRequirementState) {
        WelcomePacksService.getByOrgId({ id: $localStorage.current_organisation.id }).$promise.then(function (result) {
          vm.packCategories = result;
        });
      } else {
        if (!angular.equals(vm.location, null)) {
          vm.entity.locationName = vm.location.name;
        }
      }
    }



    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;

      console.log(vm.entity);
      if (vm.entity.id == null) {
        console.log("vm.entity");
        console.log(vm.entity);
        RequirementsService.create(vm.entity, onSaveSuccess, onSaveError);
      } else {
        RequirementsService.update(vm.entity, onSaveSuccess, onSaveError);

      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    // function serviceChange(serviceId) {
    //   console.log("service change");
    //   console.log(serviceId);
    //   angular.forEach(vm.generalServices, function(genSvc) {
    //     if (serviceId == genSvc.id) {
    //       vm.entity.name = $localStorage.current_organisation.abbreviation + " " + genSvc.name;
    //     }
    //   })
    //
    // }

    function onLocationSelected($item, $model) {

      vm.safariGroup.locationId = $item.id;
      vm.safariGroup.locationName = $item.name;

    }

    function getLocs(val, generalServiceId) {
      console.log("val " + val + " " + generalServiceId);
      return OrganizationsService
        .filterByGeneralService({
          id: generalServiceId,
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
    }



    function onTargetSelected($item, $model) {
      vm.entity.supplierId = $item.id;
      vm.entity.supplierName = $item.name;

      initSupplierIssuer();

    }
    function initSupplierIssuer() {
      if (vm.entity.supplierId > 0) {
        ServicesService.getByOrganizationId({ id: vm.entity.supplierId }).$promise.then(function (result) {
          vm.supplierServices = result;
        });
        OrganizationsService.getParent({
          id: vm.entity.supplierId
        }).$promise.then(function (respose) {
          vm.issuers = respose;
        });
      }
    }
    function issuerChanged(objectId) {
      console.log("issuer " + objectId + " " + vm.entity.issuerId);
      angular.forEach(vm.issuers, function (issuer) {
        if (issuer.id == objectId) {
          vm.entity.issuerName = issuer.name;
        }
      });
    }
    function serviceChange(objectId) {
      console.log("service " + objectId + " " + vm.entity.serviceId);
      angular.forEach(vm.supplierServices, function (service) {
        if (service.id == objectId) {
          vm.entity.serviceName = service.name;
        }
      });
    }
    function genServiceChange(objectId) {
      vm.supplierServices = [];
      vm.entity.supplierName = null;
      vm.issuers = [];
      console.log("gen service " + objectId + " " + vm.entity.generalServiceId);
      angular.forEach(vm.generalServices, function (generalService) {
        if (generalService.id == objectId) {
          vm.entity.generalServiceName = generalService.name;
        }
      });
      getLocs("", vm.entity.generalServiceId);
    }
    function genServiceTypeChange(objectType) {
      vm.generalServices = [];
      vm.supplierServices = [];
      vm.entity.supplierName = null;
      console.log("changed type" + vm.entity.generalServiceType + " " + objectType);
      GeneralServicesService.filterByGenSvcType({ type: objectType }).$promise.then(function (results) {
        vm.generalServices = results;
      })
    }
    //genServiceChange
  }
})();
