(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("requirements", {
        parent: "app",
        url: "/requirements/{packId}/{locationId}",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/welcome-packs/welcome-packs-detail.html",
            controller: "RequirementsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "$localStorage",
            "RequirementsService",
            function($stateParams, $localStorage, RequirementsService) {
              return RequirementsService.getByPackId({
                id: $stateParams.packId
              }).$promise;
            }
          ]
        }
      })
      .state("requirements.new", {
        parent: "requirements",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "facilities.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-create-dialog.html",
                controller: "RequirementsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      name: null,
                      description: null,
                      notes: null,
                      specialNotes: null,
                      type: "REQUIREMENT",
                      orgId: $localStorage.current_organisation.id,
                      orgName: $localStorage.current_organisation.name,
                      locationId: $stateParams.locationId,
                      locationName: null,
                      generalServiceType: "ACCOMMODATION",
                      generalServiceId: null,
                      generalServiceName: null,
                      serviceId: null,
                      serviceName: null,
                      supplierId: null,
                      supplierName: null,
                      issuerId: null,
                      issuerName: null,
                      serviceCategory: "COVERAGE",
                      objectType: "PERSON",
                      objectCategory: null,
                      perTrip: false,
                      perObject: true,
                      perDay: true,
                      optional: false,
                      timeStrict: true,
                      global: false,
                      visible: true,
                      contentStatus: "DRAFT",
                      packId: $stateParams.packId,
                      packName: null,
                      currencyId:204,
                      price: 0
                    };
                  },
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function($localStorage, GeneralServicesService) {
                      return GeneralServicesService.query().$promise;
                        // .getByOrganizationTypeId({
                        //   id: $localStorage.current_organisation.organisationTypeId
                        // })
                        // .$promise;
                    }
                  ],
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  location: null,
                  editParentRequirements: function() {
                    return true;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("requirements", null, {
                    reload: "requirements"
                  });
                },
                function() {
                  $state.go("requirements");
                }
              );
          }
        ]
      })
      .state("requirements.edit", {
        parent: "requirements",
        url: "/{requirementsId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-create-dialog.html",
                controller: "RequirementsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "RequirementsService",
                    function(RequirementsService) {
                        return RequirementsService.getById({id : $stateParams.requirementsId}).$promise;
                    }
                  ],
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function($localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .getByOrganizationTypeId({
                          id: $localStorage.current_organisation.organisationTypeId
                        })
                        .$promise;
                    }
                  ],
                  location: null,
                  currencies: [
                    '$stateParams',
                    'CurrenciesService',
                    '$localStorage',
                    function (
                      $stateParams,
                      CurrenciesService,
                      $localStorage
                    ) {
                      return CurrenciesService.query().$promise;
                    }
                  ],
                  editParentRequirements: function() {
                    return false;
                  }
                }

              })
              .result.then(
                function() {
                  $state.go("requirements", null, {
                    reload: "requirements"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("requirements.delete", {
        parent: "requirements",
        url: "/{requirementsId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/requirements/requirements-delete-dialog.html",
                controller: "RequirementsDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "RequirementsService",
                    function(RequirementsService) {
                      return RequirementsService.getById({id : $stateParams.requirementsId}).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("requirements", null, {
                    reload: "requirements"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
    // .state("requirements-detail", {
    //   parent: "requirements",
    //   url: "/{id}/overview",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "webApp.requirements.detail.title"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/requirements/requirements-detail.html",
    //       controller: "RequirementsDetailController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     requirements: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.getRequirementsByParentId({ id: $stateParams.id })
    //           .$promise;
    //       }
    //     ],
    //     requirementsItems: [
    //       "$stateParams",
    //       "RequirementsItemsService",
    //       function($stateParams, RequirementsItemsService) {
    //         return RequirementsItemsService.getRequirementsItemsByRequirements({
    //           id: $stateParams.id
    //         }).$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state) {
    //         var currentStateData = {
    //           name: $state.current.name || "requirements",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("requirements-detail.image", {
    //   parent: "requirements-detail",
    //   url: "/image/upload",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: ""
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/requirements/image-upload-dialog.html",
    //           controller: "RequirementsImageUploadDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: function() {
    //               return {
    //                 file: null
    //               };
    //             },
    //             requirements: [
    //               "$stateParams",
    //               "RequirementsService",
    //               function($stateParams, RequirementsService) {
    //                 return RequirementsService.get({ id: $stateParams.id }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-detail", null, { reload: "requirements-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-detail.subrequirements", {
    //   parent: "requirements",
    //   url: "/{id}/subrequirements",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "webApp.requirements.detail.title"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/requirements/requirements-subrequirements.html",
    //       controller: "RequirementsSubRequirementsController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     requirements: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.getRequirementsByParentId({ id: $stateParams.id })
    //           .$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state) {
    //         var currentStateData = {
    //           name: $state.current.name || "requirements-detail",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("requirements-detail.edit", {
    //   parent: "requirements-detail",
    //   url: "/edit",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/requirements/requirements-dialog.html",
    //           controller: "RequirementsDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "RequirementsService",
    //               function(RequirementsService) {
    //                 return RequirementsService.get({ id: $stateParams.id }).$promise;
    //               }
    //             ],
    //             editParentRequirements: function() {
    //               return true;
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-detail", null, { reload: "requirements-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-detail-new-requirements", {
    //   parent: "requirements-detail.subrequirements",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: ""
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/requirements/requirements-dialog.html",
    //           controller: "RequirementsDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: function() {
    //               return {
    //                 id: null,
    //                 uuid: null,
    //                 name: null,
    //                 brief: null,
    //                 description: null,
    //                 notes: null,
    //                 order: null,
    //                 contentStatus: "DRAFT",
    //                 statusReason: null,
    //                 organisationId: $localStorage.current_organisation.id,
    //                 organisationName: null,
    //                 parentId: $stateParams.id,
    //                 parentName: null,
    //                 generalRequirementsTypeId: null,
    //                 generalRequirementsTypeName: null
    //               };
    //             },
    //             editParentRequirements: function() {
    //               return false;
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-detail.subrequirements", null, {
    //               reload: "requirements-detail.subrequirements"
    //             });
    //           },
    //           function() {
    //             $state.go("requirements-detail.subrequirements");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-detail-new-requirements-item", {
    //   parent: "requirements-detail",
    //   url: "/requirements-item/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/requirements-items/requirements-item-dialog.html",
    //           controller: "RequirementsItemDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "RequirementsItemsService",
    //               function(RequirementsService) {
    //                 return {
    //                   id: null,
    //                   uuid: null,
    //                   name: null,
    //                   brief: null,
    //                   notes: null,
    //                   description: null,
    //                   order: null,
    //                   contentStatus: "DRAFT",
    //                   statusReason: null,
    //                   requirementsId: $stateParams.id,
    //                   requirementsName: null,
    //                   generalRequirementsItemTypeId: null,
    //                   generalRequirementsItemTypeName: null,
    //                   servingOptionsGroupId: null,
    //                   servingOptionsGroupName: null
    //                 };
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-detail", null, { reload: "requirements-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-galleries", {
    //   parent: "app",
    //   url: "/requirements/{id}/gallery",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "Gallery"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/requirements/requirements-galleries.html",
    //       controller: "RequirementsGalleriesController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state, $) {
    //         var currentStateData = {
    //           name: $state.current.name || "locations-own",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("requirements-galleries.new", {
    //   parent: "requirements-galleries",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/albums/album-create-dialog.html",
    //           controller: "AlbumCreateDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "RequirementsService",
    //               function(RequirementsService) {
    //                 return RequirementsService.get({ id: $stateParams.id })
    //                   .$promise;
    //               }
    //             ],
    //             album: function() {
    //               return {
    //                 albumType: "GENERAL",
    //                 caption: null, //album name
    //                 coverName: null, //uploaded cover image file name
    //                 coverUuid: null, //uploaded cover image
    //                 isDefaultAlbum: true, //put option slider
    //                 name: null, //album name
    //                 objectUuid: null //location uuid
    //               };
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-galleries", null, {
    //               reload: "requirements-galleries"
    //             });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-galleries.delete", {
    //   parent: "requirements-galleries",
    //   url: "/{albumId}/delete",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-delete-dialog.html",
    //           controller: "AlbumDeleteDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md"
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-galleries", null, {
    //               reload: "requirements-galleries"
    //             });
    //           },
    //           function() {
    //             $state.go("requirements-galleries");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-album-detail", {
    //   parent: "requirements-galleries",
    //   url: "/{albumId}/album",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "Gallery"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/requirements/requirements-album-detail.html",
    //       controller: "RequirementsAlbumDetailController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "RequirementsService",
    //       function($stateParams, RequirementsService) {
    //         return RequirementsService.get({ id: $stateParams.id }).$promise;
    //       },
    //     ],
    //     album: [
    //       "$stateParams",
    //       "AlbumService",
    //       function($stateParams, AlbumService) {
    //         return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
    //       },
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state, $) {
    //         var currentStateData = {
    //           name: $state.current.name || "requirements-galleries",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("requirements-album-detail.edit", {
    //   parent: "requirements-album-detail",
    //   url: "/edit",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "requirements-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function ($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //           "core/routes/albums/album-dialog.html",
    //           controller: "AlbumDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: ["AlbumService",function (AlbumService) {
    //               return AlbumService.get({ id: $stateParams.albumId }).$promise;
    //             }
    //             ]
    //           }
    //         })
    //         .result.then(
    //         function () {
    //           $state.go("requirements-album-detail", null, { reload: "requirements-album-detail" });
    //         },
    //         function () {
    //           $state.go("requirements-album-detail");
    //         }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-album-detail.makeCoverImage", {
    //   parent: "requirements-album-detail",
    //   url: "/{imageId}/cover-image",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-change-cover-image-dialog.html",
    //           controller: "AlbumChangeCoverImageDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: [
    //               "AlbumService",
    //               "$stateParams",
    //               function(AlbumService, $stateParams) {
    //                 return AlbumService.get({ id: $stateParams.albumId })
    //                   .$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-album-detail", null, {
    //               reload: "requirements-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("requirements-album-detail");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-album-detail.deleteImage", {
    //   parent: "requirements-album-detail",
    //   url: "/{imageId}/delete",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-image-delete-dialog.html",
    //           controller: "AlbumImageDeleteDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: [
    //               "AlbumService",
    //               "$stateParams",
    //               function(AlbumService, $stateParams) {
    //                 return AlbumService.get({ id: $stateParams.albumId })
    //                   .$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-album-detail", null, {
    //               reload: "requirements-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("requirements-album-detail");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-album-detail.upload", {
    //   parent: "requirements-album-detail",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-upload-dialog.html",
    //           controller: "AlbumUploadDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "$stateParams",
    //               "AlbumService",
    //               function ($stateParams, AlbumService) {
    //                 return AlbumService.get({ id: $stateParams.albumId }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("requirements-album-detail", null, {
    //               reload: "requirements-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("requirements-detail.addLabels", {
    //   parent: "requirements-detail",
    //   url: "/{uuid}/{objectType}/add-labels",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function ($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/labels/object-label-create-dialog.html",
    //           controller: "ObjectLabelGroupCreateDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             labelGroups: [
    //               "LabelGroupsService",
    //               function (LabelGroupsService) {
    //                 return LabelGroupsService.getLabelGroupsByObjectType({
    //                   objectType: $stateParams.objectType
    //                 }).$promise;
    //               }
    //             ],
    //             entity: [
    //               "RequirementsService",
    //               function (RequirementsService) {
    //                 return RequirementsService.get({ id: $stateParams.id })
    //                   .$promise;
    //               }
    //             ],
    //             savedselectedLabels: [
    //               "ObjectLabelsService",
    //               function (ObjectLabelsService) {
    //                 return ObjectLabelsService.getLabelsByObjectId({
    //                   uuid: $stateParams.uuid
    //                 }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //         function () {
    //           $state.go("requirements-detail", null, {
    //             reload: "requirements-detail"
    //           });
    //         },
    //         function () {
    //           $state.go("requirements-detail");
    //         });
    //     }
    //   ]
    // })
    //  .state("requirements-detail.deleteLabel", {
    //           parent: "requirements-detail",
    //          url: "/{itemToDelete}/delete-label",
    //
    //           data: {
    //             requiresAuthentication: true,
    //             authorities: []
    //           },
    //           onEnter: [
    //             "$stateParams",
    //             "$state",
    //             "$uibModal",
    //             function ($stateParams, $state, $uibModal) {
    //               $uibModal
    //                 .open({
    //                   templateUrl:
    //                     "core/routes/labels/object-label-delete-dialog.html",
    //                   controller: "ObjectLabelGroupDeleteDialogController",
    //                   controllerAs: "vm",
    //                   backdrop: "static",
    //                   size: "md"
    //                 })
    //                 .result.then(
    //                 function () {
    //                   $state.go("requirements-detail", null, {
    //                     reload: "requirements-detail"
    //                   });
    //                 },
    //                 function () {
    //                   $state.go("requirements-detail");
    //                 }
    //       );
    //     }
    //   ]
    // });
  }
})();
