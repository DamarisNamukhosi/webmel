(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('BasesController', BasesController);

  BasesController.$inject = ['$scope', '$location', '$state', 'BasesService', 'ParseLinks','pagingParams','paginationConstants','URLS', '$stateParams', "LocationGroupsService"
  ];

  function BasesController($scope, $location, $state, BasesService,ParseLinks,pagingParams,paginationConstants,URLS, $stateParams,LocationGroupsService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
    vm.areas = [];
    vm.whenFiltering = false;
    vm.genAreas = ["BASE","AREAS"];
    vm.areas = [];
    //declare functions
    vm.loadPage = loadPage;
    vm.transition = transition;
    //pagination init
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.areaTypeChange = areaTypeChange;
    //initialization

   initAreas();

    // BasesService.getAreasPageable({
    //     page: pagingParams.page - 1,
    //     size: vm.itemsPerPage,
    //     sort: 'id,desc'
    //   },
    //   function (data, headers) {
    //     vm.links = ParseLinks.parse(headers('link'));
    //     vm.totalItems = headers('X-Total-Count');
    //     vm.queryCount = vm.totalItems;
    //     vm.Areas = data;
    //     vm.page = pagingParams.page;
    //     vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
    //     vm.isLoading = false;
    //     vm.noRecordsFound = vm.Areas === undefined || vm.Areas.length === 0;
    //   },
    //   function (error) {
    //     console.log('error getting Areas');
    //     console.log(error);
    //     vm.isLoading = false;
    //   }
    // );
    function initAreas(){
      vm.previousState = $state.current.parent;
    //   if(vm.previousState == 'app'){
    //   BasesService.query({types: 2}).$promise.then(function (response){
    //     vm.areas = response;
    //     console.log(vm.areas);
    //   } );
    // }else if(vm.previousState == 'areas'){
    //   console.log("Areas under")
    BasesService.query().$promise.then(function (response){
          vm.locations = response;
          vm.areas  = response;
          console.log(vm.areas);
        } );
    //   BasesService.query(
    //     {
    //      id: $stateParams.countryId,
    //       page: pagingParams.page - 1,
    //       size: vm.itemsPerPage,
    //       sort: 'id,desc'
    //     },
    //     function(data, headers) {
    //         vm.links = ParseLinks.parse(headers('link'));
    //         vm.totalItems = headers('X-Total-Count');
    //         vm.queryCount = vm.totalItems;
    //         vm.locations = data;
    //         vm.page = pagingParams.page;
    //         vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
    //         vm.isLoading = false;

    //         initLocs(data);
    //     },
    //     function(error) {
    //         console.log('error getting contracts');
    //         console.log(error);
    //         vm.isLoading = false;
    //     }
    // );


    LocationGroupsService.query().$promise.then(function (response){
      vm.locations = response;
      angular.forEach(response, function(res){
        vm.areas.push(res);
      })
      console.log(vm.genAreas);
    });
  }
  function initLocs(data){
    angular.forEach(data, function(loc){
      vm.areas.push(loc);

    });

  }
    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        countryId: $stateParams.countryId,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    vm.activate = function (area) {

      if (area.contentStatus == 'PUBLISHED') {
        area.contentStatus = "UNPUBLISHED"
      } else {
        area.contentStatus = "PUBLISHED"
      }

      console.log(area);
      BasesService.update(area, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(response) {
      $state.reload();
    }

    function onSaveError(error) {
      console.log(error);
      alert(error);
    }


    function areaTypeChange(id,val)
    {
      vm.whenFiltering = true;
      vm.areas= [];
      console.log(val);
        vm.searchedProperty = false;
          return BasesService
            .query({
              name: val,
              types: [id]
            })
            .$promise
            .then(function (results) {
              angular.forEach(results, function(r){
                if(r.parentId == $stateParams.countryId){
                  vm.areas.push(r);
                }
              })

              console.log(vm.areas);;
              return results.map(function (item) {

                return item;
              })
            });
        }


  }
})();
