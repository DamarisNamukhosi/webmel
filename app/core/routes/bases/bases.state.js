(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('bases', {
        parent: 'app',
        url: '/bases',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/bases.html',
            controller: 'BasesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.query({types: 2}).$promise;
            }
          ]
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state('bases-bases', {
        parent: 'bases',
        url: '/{countryId}/countries',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER','D_AUTHORITY_USER'],
          pageTitle: 'bases'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/bases.html',
            controller: 'basesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              // type id 11 is properties
              return BasesService.query({
                types:7

              }).$promise;
            }
          ]
        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state('bases-bases.new', {
        parent: 'bases-bases',
        url: '/newBase',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog.html',
                controller: 'baseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: $stateParams.parentId,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases', null, {
                    reload: 'bases-bases'
                  });
                },
                function () {
                  $state.go('bases-bases');
                }
              );
          }
        ]
      })
      .state('bases.new', {
        parent: 'bases',
        url: '/newLocGroup',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog.html',
                controller: 'baseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: null,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases', null, {
                    reload: 'bases'
                  });
                },
                function () {
                  $state.go('bases');
                }
              );
          }
        ]
      })
      .state('bases-own.new', {
        parent: 'bases-own',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog-create.html',
                controller: 'baseDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      baseTypeId: null,
                      longitude: null,
                      name: null,
                      organisationId: $localStorage
                        .current_organisation.id,
                      parentId: null
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-own', null, {
                    reload: 'bases-own'
                  });
                },
                function () {
                  $state.go('bases-own');
                }
              );
          }
        ]
      })
      .state('bases-own', {
        parent: 'bases',
        url: '/own',
        data: {
          requiresAuthentication: true,
          authorities: ['D_AUTHORITY_USER', 'MANAGE_bases', 'MANAGER_USER'],
          pageTitle: 'bases'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/bases-own.html',
            controller: 'basesOwnController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          bases: [
            '$stateParams',
            '$localStorage',
            'BasesService',
            function (
              $stateParams,
              $localStorage,
              BasesService
            ) {
              return BasesService.getbasesByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('base-galleries', {
        parent: 'bases-own',
        url: '/{baseId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-galleries.html',
            controller: 'baseGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'bases-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('bases-bases.galleries', {
        parent: 'bases-bases',
        url: '/{baseId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-galleries.html',
            controller: 'baseGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.baseId
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'bases-bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-galleries.new', {
        parent: 'bases-bases.galleries',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-create-dialog.html',
                controller: 'AlbumCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.baseId
                      }).$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: 'GENERAL',
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //base uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.galleries', null, {
                    reload: 'bases-bases.galleries'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-galleries.delete', {
        parent: 'base-galleries',
        url: '/{albumId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-delete-dialog.html',
                controller: 'AlbumDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('base-galleries', null, {
                    reload: 'base-galleries'
                  });
                },
                function () {
                  $state.go('base-galleries');
                }
              );
          }
        ]
      })
      .state('base-album-detail', {
        parent: 'base-galleries',
        url: '/{albumId}/album',
        data: {
          requiresAuthentication: true,
          authorities: [
            'MASTER_USER',
            'D_AUTHORITY_USER',
            'MANAGE_bases'
          ],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-album-detail.html',
            controller: 'baseAlbumDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          album: [
            '$stateParams',
            'AlbumService',
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'bases-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-album-detail.edit', {
        parent: 'base-album-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-dialog.html',
                controller: 'AlbumDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AlbumService',
                    function (AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('base-album-detail', null, {
                    reload: 'base-album-detail'
                  });
                },
                function () {
                  $state.go('base-album-detail');
                }
              );
          }
        ]
      })
      .state('base-album-detail.upload', {
        parent: 'base-album-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-upload-dialog.html',
                controller: 'AlbumUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'AlbumService',
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('base-album-detail', null, {
                    reload: 'base-album-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-album-detail.makeCoverImage', {
        parent: 'base-album-detail',
        url: '/{imageId}/cover-image',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
                controller: 'AlbumChangeCoverImageDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('base-album-detail', null, {
                    reload: 'base-album-detail'
                  });
                },
                function () {
                  $state.go('base-album-detail');
                }
              );
          }
        ]
      })
      .state('base-album-detail.deleteImage', {
        parent: 'base-album-detail',
        url: '/{imageId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'bases-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
                controller: 'AlbumImageDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('base-album-detail', null, {
                    reload: 'base-album-detail'
                  });
                },
                function () {
                  $state.go('base-album-detail');
                }
              );
          }
        ]
      })
      .state('base-detail', {
        parent: 'bases-own',
        url: '/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-detail.html',
            controller: 'baseDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.getFullbase({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'bases-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('bases-bases.overview', {
        parent: 'bases-bases',
        url: '/{baseId}/{baseType}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-detail.html',
            controller: 'baseDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.getFullbase({
                  id: $stateParams.baseId
                })
                .$promise;
            }
          ],
          coordinates: [
            '$stateParams',
            'CoordinatesService',
            function ($stateParams, CoordinatesService) {
              return CoordinatesService.getCoordinatesBybase({
                id: $stateParams.baseId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'bases-bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-detail.edit', {
        parent: 'base-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog.html',
                controller: 'baseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.getFullbase({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-detail', null, {
                    reload: 'base-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      //bases-bases.overview
        .state('bases-bases.overview.edit', {
        parent: 'bases-bases.overview',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
            .open({
              // templateUrl: 'core/routes/organizations/organization-register-dialog.html',
              // controller: 'OrganizationRegisterDialogController',
              templateUrl: 'core/routes/bases/base-dialog.html',
              controller: 'baseDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                 entity: [
                  'BasesService',
                  function (BasesService) {
                    return BasesService.getFullbase({
                      id: $stateParams.baseId
                    }).$promise;
                  }
                ],
                setParentVisible: function () {
                  return true;
                }
                }
            })
              .result.then(
                function () {
                  $state.go('bases-bases.overview', null, {
                    reload: 'bases-bases.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-detail.addLabels', {
        parent: 'bases-bases.overview',
        url: '/{uuid}/{objectType}/add-labels',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-create-dialog.html',
                controller: 'ObjectLabelGroupCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  labelGroups: [
                    'LabelGroupsService',
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.baseId
                      }).$promise;
                    }
                  ],
                  savedselectedLabels: [
                    'ObjectLabelsService',
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.overview', null, {
                    reload: 'bases-bases.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-detail.deleteLabel', {
        parent: 'bases-bases.overview',
        url: '/{itemToDelete}/delete-label',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-delete-dialog.html',
                controller: 'ObjectLabelGroupDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('bases-bases.overview', null, {
                    reload: 'bases-bases.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('bases-bases.detail', {
        parent: 'bases-bases',
        url: '/{baseId}/{baseType}/detail',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Attractions'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/properties/base-properties.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },

        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },

        resolve: {

        }
      })

      .state('base-attractions', {
        parent: 'bases-own',
        url: '/{id}/attractions',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Attractions'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-attractions.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              // type id 11 is properties
              return BasesService.query({
                 types:[6,7,8]

              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                parent_name: $state.current.name || 'bases-own',
                name: $state.current.name || 'base-detail',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
    
      .state('base-attractions.new', {
        parent: 'base-attractions',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'NewAttractions'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog-create.html',
                controller: 'baseDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      typeId: [6,7,8],
                      longitude: null,
                      name: null,
                      managerId: $localStorage.current_organisation.id
                    };
                  },
                  setParentVisible: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-attractions', null, {
                    reload: 'base-attractions'
                  });
                },
                function () {
                  $state.go('base-attractions');
                }
              );
          }
        ]
      })
      
      .state('base-attractions.edit', {
        parent: 'base-attractions',
        url: '/{attractionId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                   entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.attractionId
                      }).$promise;
                    }
                  ]
                  }
              })
              .result.then(
                function () {
                  $state.go('base-attractions', null, {
                    reload: 'base-attractions'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('base-activities', {
        parent: 'bases-own',
        url: '/{id}/activities',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Activities'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-activities.html',
            controller: 'baseActivitiesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          activities: [
            '$stateParams',
            'ActivityService',
            function ($stateParams, ActivityService) {
              return ActivityService.getActivitiesByLocId({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-activities.new', {
        parent: 'base-activities',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/activities/activity-dialog.html',
                controller: 'ActivityDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      baseId: $stateParams.id,
                      baseName: null,
                      generalActivityId: null,
                      generalActivityName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-activities', null, {
                    reload: 'base-activities'
                  });
                },
                function () {
                  $state.go('base-activities');
                }
              );
          }
        ]
      })
      .state("base-activities.edit", {
        parent: "base-activities",
        url: "/{activityId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/activities/activity-dialog.html",
                controller: "ActivityDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "ActivityService",
                    function (ActivityService) {
                      return ActivityService.get({
                          id: $stateParams.activityId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("base-activities", null, {
                    reload: "base-activities"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state('base-bases', {
        parent: 'bases',
        url: '/{id}/bases',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base bases'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-bases.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.getbasesByParentId({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-bases.new', {
        parent: 'base-bases',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog-create.html',
                controller: 'baseDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      baseTypeId: null,
                      longitude: null,
                      name: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      parentId: $stateParams.id
                    };
                  },
                  setParentVisible: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-bases', null, {
                    reload: 'base-bases'
                  });
                },
                function () {
                  $state.go('base-bases');
                }
              );
          }
        ]
      })
      .state('base-facilities', {
        parent: 'bases-own',
        url: '/{id}/facilities',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Facilities'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-facilities.html',
            controller: 'baseFacilitiesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          facilities: [
            '$stateParams',
            'FacilityService',
            function ($stateParams, FacilityService) {
              return FacilityService.getFacilities({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-facilities.new', {
        parent: 'base-facilities',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/facilities/facility-dialog.html',
                controller: 'FacilityDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      baseId: $stateParams.id,
                      baseName: null,
                      generalFacilityId: null,
                      generalFacilityName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-facilities', null, {
                    reload: 'base-facilities'
                  });
                },
                function () {
                  $state.go('base-facilities');
                }
              );
          }
        ]
      })
      .state("base-facilities.edit", {
        parent: "base-facilities",
        url: "/{facilityId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/facilities/facility-dialog.html',
                controller: 'FacilityDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "FacilityService",
                    function (FacilityService) {
                      return FacilityService.get({
                          id: $stateParams.facilityId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go("base-facilities", null, {
                    reload: "base-facilities"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state('base-events', {
        parent: 'bases-own',
        url: '/{id}/events',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'base Events'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/base-events.html',
            controller: 'baseEventsController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          events: [
            '$stateParams',
            'EventsService',
            function ($stateParams, EventsService) {
              return EventsService.getEvents({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('base-events.new', {
        parent: 'base-events',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-dialog.html",
                controller: "FullEventDialogController",
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      evntInstanceDTO: {
                        id: null,
                        uuid: null,
                        name: null,
                        brief: null,
                        description: null,
                        startTime: null,
                        endTime: null,
                        contentStatus: 'DRAFT',
                        statusReason: null,
                        evntId: null,
                        evntName: null,
                        baseId: $stateParams.id,
                        baseName: null
                      }
                    };
                  },
                  baseset: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('base-events', null, {
                    reload: 'base-events'
                  });
                },
                function () {
                  $state.go('base-events');
                }
              );
          }
        ]
      })
      .state('base-properties', {
        parent: 'bases-own',
        url: '/{id}/properties',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/properties/base-properties.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              // type id 11 is properties
              return BasesService.getbasesByParentIdAndType({
                id: $stateParams.id,
                type_id: 10
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      // .state('base-properties.new', {
      //   parent: 'base-properties',
      //   url: '/new',
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: [],
      //     pageTitle: ''
      //   },
      //   onEnter: [
      //     '$stateParams',
      //     '$state',
      //     '$localStorage',
      //     '$uibModal',
      //     function ($stateParams, $state, $localStorage, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: 'core/routes/organizations/organization-register-dialog.html',
      //           controller: 'OrganizationRegisterDialogController',
      //           controllerAs: 'vm',
      //           backdrop: 'static',
      //           size: 'lg',
      //           resolve: {
      //             entity: function () {
      //               return {
      //                 id:null,
      //                 abbreviation: null,
      //                 altitude: null,
      //                 brief: null,
      //                 description: null,
      //                 langKey: 'en',
      //                 latitude: null,
      //                 longitude: null,
      //                 name: null,
      //                 organisationTypeId: 4, // organization type for property is 4
      //                 parentbaseId: $stateParams.id,
      //                 parentOrganisationId: $localStorage
      //                   .current_organisation.id,
      //                 userEmailAddress: null,
      //                 userFullName: null
      //               };
      //             }
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go('base-properties', null, {
      //               reload: 'base-properties'
      //             });
      //           },
      //           function () {
      //             $state.go('^');
      //           }
      //         );
      //     }
      //   ]
      // })
      .state('base-properties.new', {
        parent: 'bases-bases.detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id:null,
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: 4, // organization type for property is 4
                      parentbaseId: $stateParams.baseId,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.detail', null, {
                    reload: 'bases-bases.detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-properties.edit', {
        parent: 'bases-bases.detail',
        url: '/{locId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                   entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.locId
                      }).$promise;
                    }
                  ]
                  }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.detail', null, {
                    reload: 'bases-bases.detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('base-restaurants', {
        parent: 'bases-own',
        url: '/{id}/restaurants',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/restaurants/base-restaurants.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              return BasesService.get({
                  id: $stateParams.id
                })
                .$promise;
            }
          ],
          bases: [
            '$stateParams',
            'BasesService',
            function ($stateParams, BasesService) {
              // type id 11 is restaurants
              return BasesService.getbasesByParentIdAndType({
                id: $stateParams.id,
                type_id: 11
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('bases-bases.restaurants', {
        parent: 'bases-bases',
        url: '/{baseId}/{baseType}/restaurants',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/bases/restaurants/base-restaurants.html',
            controller: 'basebasesController',
            controllerAs: 'vm'
          }
        },
        // resolve: {
        //   entity: [
        //     '$stateParams',
        //     'BasesService',
        //     function ($stateParams, BasesService) {
        //       return BasesService.get({
        //           id: $stateParams.baseId
        //         })
        //         .$promise;
        //     }
        //   ],
        //   bases: [
        //     '$stateParams',
        //     'BasesService',
        //     function ($stateParams, BasesService) {
        //       return BasesService.getbasesByParentIdAndType({
        //         id: $stateParams.baseId,
        //         types: 11
        //       }).$promise;
        //     }
        //   ],
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'bases-bases',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
      })
      .state('base-restaurants.new', {
        parent: 'bases-bases.restaurants',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id:null,
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      langKey: 'en',
                      latitude: null,
                      longitude: null,
                      name: null,
                      organisationTypeId: 5, // organization type for restaurant is 5
                      parentbaseId: $stateParams.baseId,
                      userEmailAddress: null,
                      userFullName: null
                    };
                  },
                  hidebaseType: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.restaurants', null, {
                    reload: 'bases-bases.restaurants'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-restaurants.edit', {
        parent: 'bases-bases.restaurants',
        url: '/{locId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/organizations/organization-register-dialog.html',
                controller: 'OrganizationRegisterDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                   entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.locId
                      }).$promise;
                    }
                  ]
                  }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.restaurants', null, {
                    reload: 'bases-bases.restaurants'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }]
      })
      .state('bases.edit', {
        parent: 'bases',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog.html',
                controller: 'baseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases', null, {
                    reload: 'bases'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('bases-own.edit', {
        parent: 'bases-own',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-dialog.html',
                controller: 'baseDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-own', null, {
                    reload: 'bases-own'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('base-detail.delete', {
        parent: 'bases-bases.overview',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-delete-dialog.html',
                controller: 'baseDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.overview', null, {
                    reload: 'bases-bases.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('bases-own.delete', {
        parent: 'bases-own',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/base-delete-dialog.html',
                controller: 'baseDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'BasesService',
                    function (BasesService) {
                      return BasesService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('bases-own', null, {
                    reload: 'bases-own'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('bases.profile', {
        parent: 'bases-bases.overview',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
                controller: 'baseImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('bases-bases.overview', null, {
                    reload: 'bases-bases.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('base-activities.profile', {
        parent: 'base-activities',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
                controller: 'baseImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('base-activities', null, {
                    reload: 'base-activities'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('base-attractions.profile', {
        parent: 'base-attractions',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
                controller: 'baseImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('base-attractions', null, {
                    reload: 'base-attractions'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('base-facilities.profile', {
        parent: 'base-facilities',
        url: '/image/{objectUuid}/{objectName}/upload',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ''
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
                controller: 'baseImageUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      file: null
                    };
                  },
                  object: function () {
                    return {
                      uuid: $stateParams.objectUuid,
                      name: $stateParams.objectName,
                    };
                  }
                }
              }).result.then(
                function () {
                  $state.go('base-facilities', null, {
                    reload: 'base-facilities'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      }).state('base-bases.profile', {
      parent: 'base-bases',
      url: '/image/{objectUuid}/{objectName}/upload',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: ''
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal
            .open({
              templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
              controller: 'baseImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    file: null
                  };
                },
                object: function () {
                  return {
                    uuid: $stateParams.objectUuid,
                    name: $stateParams.objectName,
                  };
                }
              }
            }).result.then(
              function () {
                $state.go('base-bases', null, {
                  reload: 'base-bases'
                });
              },
              function () {
                $state.go('^');
              }
            );
        }
      ]
    })
    .state('base-properties.profile', {
      parent: 'bases-bases.detail',
      url: '/image/{objectUuid}/{objectName}/upload',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: ''
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal
            .open({
              templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
              controller: 'baseImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    file: null
                  };
                },
                object: function () {
                  return {
                    uuid: $stateParams.objectUuid,
                    name: $stateParams.objectName,
                  };
                }
              }
            }).result.then(
              function () {
                $state.go('bases-bases.detail', null, {
                  reload: 'bases-bases.detail'
                });
              },
              function () {
                $state.go('^');
              }
            );
        }
      ]
    })
    .state('base-restaurants.profile', {
      parent: 'bases-bases.restaurants',
      url: '/image/{objectUuid}/{objectName}/upload',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: ''
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal
            .open({
              templateUrl: 'core/routes/bases/image-upload/image-upload-dialog.html',
              controller: 'baseImageUploadDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    file: null
                  };
                },
                object: function () {
                  return {
                    uuid: $stateParams.objectUuid,
                    name: $stateParams.objectName,
                  };
                }
              }
            }).result.then(
              function () {
                $state.go('bases-bases.restaurants', null, {
                  reload: 'bases-bases.restaurants'
                });
              },
              function () {
                $state.go('^');
              }
            );
        }
      ]
    })
    .state('bases-bases.others', {
      parent: 'bases-bases',
      url: '/{baseId}/{baseType}/others',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: ''
      },
      views: {
        'content@': {
          templateUrl: 'core/routes/bases/others/base-others.html',
          //controller: 'baseOthersController',
          controller: 'basebasesController',
          controllerAs: 'vm'
        }
      },
      // resolve: {
      //   entity: [
      //     '$stateParams',
      //     'BasesService',
      //     function ($stateParams, BasesService) {
      //       return BasesService.get({
      //           id: $stateParams.baseId
      //         })
      //         .$promise;
      //     }
      //   ],
      //   bases: [
      //     '$stateParams',
      //     'BasesService',
      //     function ($stateParams, BasesService) {
      //       return BasesService.getbasesByParentIdAndType({
      //         id: $stateParams.baseId,
      //         types: [2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19]
      //       }).$promise;
      //     }
      //   ],
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        previousState: [
          '$state',
          function ($state) {
            var currentStateData = {
              name: $state.current.name || 'bases-bases',
              params: $state.params,
              url: $state.href(
                $state.current.name,
                $state.params
              )
            };
            return currentStateData;
          }
        ]
    })
    .state('bases-bases.others.new', {
      parent: 'bases-bases.others',
      url: '/newOthers',
      data: {
        requiresAuthentication: true,
        authorities: [],
        pageTitle: ''
      },
      onEnter: [
        '$stateParams',
        '$state',
        '$localStorage',
        '$uibModal',
        function ($stateParams, $state, $localStorage, $uibModal) {
          $uibModal
            .open({
              templateUrl: 'core/routes/bases/base-dialog.html',
              controller: 'baseDialogController',
              controllerAs: 'vm',
              backdrop: 'static',
              size: 'lg',
              resolve: {
                entity: function () {
                  return {
                    id: null,
                    uuid: null,
                    name: null,
                    abbreviation: null,
                    brief: null,
                    description: null,
                    typeId: 8,
                    contentStatus: 'DRAFT',
                    statusReason: null,
                    parentId: $stateParams.baseId,
                    managerId: $localStorage
                      .current_organisation.id,
                    managerName: $localStorage
                      .current_organisation.name
                  };
                },
                setParentVisible: function () {
                  return false;
                }
              }
            })
            .result.then(
              function () {
                $state.go('bases-bases.others', null, {
                  reload: 'bases-bases.others'
                });
              },
              function () {
                $state.go('^');
              }
            );
        }
      ]
    })
    ;
  }
})();
