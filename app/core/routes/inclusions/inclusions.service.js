(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('InclusionsService', InclusionsService);

        InclusionsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function InclusionsService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          get: {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,

            url: URLS.BASE_URL + 'costingservice/api/supplier-service-inclusions'
          },
          getById: {
            method: "GET",
            headers: {
              Authorization: "Bearer " + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + "costingservice/api/supplier-service-inclusions/:id"
          },
          getByServiceId: {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,

            url: URLS.BASE_URL + 'costingservice/api/supplier-service-inclusions/filter-by-service/:id'
          },
          update: {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/supplier-service-inclusions'
          },
          create: {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/supplier-service-inclusions'
          },
          delete: {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'costingservice/api/supplier-service-inclusions/:id'
          }
        });

    }
})();
