(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('InclusionsController', InclusionsController);

  InclusionsController.$inject = ['$scope', 'entity', '$state', '$stateParams', '$localStorage', 'URLS', 'InclusionsService',"DimensionSetsService"];

  function InclusionsController($scope, entity, $state, $stateParams, $localStorage, URLS, InclusionsService, DimensionSetsService) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    console.log(vm.entity.name);


    // vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
    vm.serviceId = $stateParams.serviceId;
    vm.isAuthenticated = null;
    console.log($stateParams)
    vm.onDimParameterChange = onDimParameterChange;
    // vm.records = users;

    // vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);
    function onDimParameterChange(supplierId,parameter){
      console.log("suppliersDimParameter change...");
      DimensionSetsService.getExistencies({orgId: supplierId,dimensionType :parameter}).$promise.then(function (results){
        vm.dimensionExistencies  = results;
        angular.forEach(vm.dimensionExistencies, function(dimExt){
          if(dimExt.id == vm.entity.value){
            return parameter +" in" + dimExt.name;
          }
        })
      });
    }

  }
})();
