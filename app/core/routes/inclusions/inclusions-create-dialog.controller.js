(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("InclusionsCreateDialogController", InclusionsCreateDialogController);

  InclusionsCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "$state",
    "entity",
    "InclusionsService",
    "generalServices",
    "GeneralServicesService",
    "safariConstants",
    "WelcomePacksService",
    "ServicesService",
    "LocationsService",
    "DimensionSetsService",
    "OrganizationsService"
  ];

  function InclusionsCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    $state,
    entity,
    InclusionsService,
    generalServices,
    GeneralServicesService,
    safariConstants,
    WelcomePacksService,
    ServicesService,
    LocationsService,
    DimensionSetsService,
    OrganizationsService
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    console.log(vm.entity);

    vm.generalServices = generalServices;
    console.log(vm.generalServices);


    //declaration
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.loadInclusionVariables = loadInclusionVariables;
    vm.genServiceChange = genServiceChange;
    vm.genServiceTypeChange = genServiceTypeChange;
    vm.onTargetSelected = onTargetSelected;
    vm.serviceChange = serviceChange;
    vm.onDimParameterChange = onDimParameterChange;
    vm.getLocs = getLocs;

    vm.generalServiceType = safariConstants.generalServiceType;
    vm.safariServiceCategory = safariConstants.safariServiceCategory;
    vm.inclusionType = safariConstants.inclusionType;
    vm.objectType = safariConstants.objectType;
    // vm.safari.dayOffest = 0;
    // vm.safari.dayOffestStr = "0";
    // vm.startTime = new Date();
    // vm.entity.startTime = moment(vm.startTime).format("hh:mm");


    initObjects();
    // vm.selectedPack = selectedPack;
    function initObjects() {

      WelcomePacksService.getWelcomePacks().$promise.then(function(result) {
        vm.packCategories = result;

        console.log(vm.packCategories);

      });
      loadInclusionVariables(vm.entity.inclusionType);

      initSupplierIssuer();
      // ServicesService.getOrganizationByServicesId({id : $localStorage.current_organisation.id}).$promise.then(function(result){
      //   vm.suppliers = result;

      //   console.log(vm.suppliers);
      // })

      ServicesService.get({
        id: $stateParams.serviceId
      }).$promise.then(function(result) {
        vm.service = result;
        if (vm.service != null) {
          vm.entity.serviceName = vm.service.name;
        }
        console.log(vm.service);
      })
      //getExistencies

    }

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      if (vm.entity.inclusionType == "CUSTOMER_ATTRIBUTE") {
        vm.entity.configs = angular.toJson({
          "travellerProfile": vm.entity.travellerProfile,
          "purposeOfVisit": vm.entity.purposeOfVisit

        });
      } else if (vm.entity.inclusionType == "DIMENSION") {
        vm.entity.configs = angular.toJson({
          "parameter": vm.entity.parameter,
          "value": vm.entity.value

        });
      }

      if (vm.startTime != null) {
        vm.entity.startTime = moment(new Date(vm.startTime), 'HH:mm').format('HH:mm');
        vm.entity.endTime = moment(new Date(vm.endTime), 'HH:mm').format('HH:mm');

        console.log(vm.entity.startTime);
        console.log(vm.entity.endTime);

        //getting time in Hours
        var start = moment(vm.entity.startTime, 'HH:mm');
        var end = moment(vm.entity.endTime, 'HH:mm');
        vm.entity.timeInHours = moment.duration(moment(end, 'HH:mm').diff(moment(start, 'HH:mm'))).asHours();
        console.log(vm.entity.timeInHours);

      }


      console.log(vm.entity);
      if (vm.entity.id == null) {
        console.log("vm.entity");
        console.log(vm.entity);
        console.log(vm.entity.startTime);
        InclusionsService.create(vm.entity, onSaveSuccess, onSaveError);

      } else {
        InclusionsService.update(vm.entity, onSaveSuccess, onSaveError);

      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }


    vm.sortableOptions = {
      "ui-floating": true,
      update: function(e, ui) {
        //  console.log(ui);
      }
    };

    vm.changed = function(index) {

    }


    function getLocation(val) {
      return LocationsService
        .query({
          name: val
        })
        .$promise
        .then(function(results) {
          return results.map(function(item) {
            return item;
          })
        });
    }


    function onLocationSelected($item, $model) {

      vm.entity.locationId = $item.id;
      vm.entity.locationName = $item.name;


    }

    function getLocs(val, generalServiceId) {
      console.log("val " + val + " " + generalServiceId);
      return OrganizationsService
        .filterByGeneralService({
          id: generalServiceId,
          name: val
        })
        .$promise
        .then(function(results) {
          return results.map(function(item) {
            return item;
          })
        });
    }



    function onTargetSelected($item, $model) {
      vm.entity.supplierId = $item.id;
      vm.entity.supplierName = $item.name;

      initSupplierIssuer();

    }

    function initSupplierIssuer() {
      if (vm.entity.supplierId > 0) {
        // ServicesService.getByOrganizationId({id : vm.entity.supplierId}).$promise.then(function(result){
        //   vm.supplierServices = result;
        // });
        OrganizationsService.getParent({
          id: vm.entity.supplierId
        }).$promise.then(function(respose) {
          vm.issuers = respose;
        });
      }
    }

    function issuerChanged(objectId) {
      console.log("issuer " + objectId + " " + vm.entity.issuerId);
      angular.forEach(vm.issuers, function(issuer) {
        if (issuer.id == objectId) {
          vm.entity.issuerName = issuer.name;
        }
      });
    }

    function serviceChange(objectId) {
      console.log("service " + objectId + " " + vm.entity.serviceId);
      angular.forEach(vm.supplierServices, function(service) {
        if (service.id == objectId) {
          vm.entity.serviceName = service.name;
        }
      });
    }

    function genServiceChange(objectId) {
      vm.supplierServices = [];
      vm.entity.supplierName = null;
      vm.issuers = [];
      console.log("gen service " + objectId + " " + vm.entity.generalServiceId);
      angular.forEach(vm.generalServices, function(generalService) {
        if (generalService.id == objectId) {
          vm.entity.generalServiceName = generalService.name;
        }
      });
      getLocs("", vm.entity.generalServiceId);
    }

    function genServiceTypeChange(objectType) {
      vm.generalServices = [];
      vm.supplierServices = [];
      vm.entity.supplierName = null;
      console.log("changed type" + vm.entity.generalServiceType + " " + objectType);
      GeneralServicesService.filterByGenSvcType({
        type: objectType
      }).$promise.then(function(results) {
        vm.generalServices = results;
      })
    }

    function loadInclusionVariables(type) {
      console.log("type " + type);
      if (type == "CUSTOMER_ATTRIBUTE") {
        console.log("type " + "CUSTOMER_ATTRIBUTE");
        vm.travellerProfiles = safariConstants.travellerProfile;
        vm.purposesOfVisit = safariConstants.purposeOfVisit;
      } else if (type == "DIMENSION") {
        console.log("type " + "DIMENSION");

        DimensionSetsService.getAllDimensions().$promise.then(function(results) {
          vm.dimensions = results;
        });

      } else {

      }
    }

    function onDimParameterChange() {
      console.log("suppliersDimParameter change...");
      DimensionSetsService.getExistencies({
        orgId: vm.entity.supplierId,
        dimensionType: vm.entity.parameter
      }).$promise.then(function(results) {
        vm.dimensionExistencies = results;
      });
    }

  }
})();
