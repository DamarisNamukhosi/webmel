(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("inclusions", {
        parent: "app",
        url: "/{serviceId}/inclusions",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/inclusions/inclusions.html",
            controller: "InclusionsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "$localStorage",
            "InclusionsService",
            function($stateParams, $localStorage, InclusionsService) {
              console.log($stateParams);
              return InclusionsService.getByServiceId( {id : $stateParams.serviceId}).$promise;

            }
          ]
        }
      })
      .state("inclusions.new", {
        parent: "inclusions",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "facilities.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/inclusions/inclusions-create-dialog.html",
                controller: "InclusionsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      name: null,
                      description: null,
                      notes: null,
                      specialNotes: null,
                      type: "INCLUSION",
                      orgId: $localStorage.current_organisation.id,
                      orgName: $localStorage.current_organisation.name,
                      locationId: $stateParams.locationId,
                      locationName: null,
                      generalServiceType: "MEAL",
                      generalServiceId: 2,
                      generalServiceName: "Meals",
                      serviceId: $stateParams.serviceId,
                      serviceName: $stateParams.serviceName,
                      supplierId: $localStorage.current_organisation.id,
                      supplierName: $localStorage.current_organisation.name,
                      issuerId: $localStorage.current_organisation.id,
                      issuerName: $localStorage.current_organisation.name,
                      serviceCategory: "COVERAGE",
                      objectType: "PERSON",
                      objectCategory: null,
                      perTrip: false,
                      perObject: true,
                      perDay: true,
                      optional: false,
                      timeStrict: true,
                      global: false,
                      visible: true,
                      fixed: true,
                      configs: null,
                      inclusionType: "DIMENSION",
                      parameter: null,
                      value: null,
                      startTime: null,
                      endTime: null,
                      timeInHours:null,
                      dayOffest: 0,
                      contentStatus: "DRAFT",
                      packId: $stateParams.packId,
                      packName: null
                    };
                  },
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function($localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .getByOrganizationTypeId({
                          id: $localStorage.current_organisation.organisationTypeId
                        })
                        .$promise;
                    }
                  ],
                  editParentInclusions: function() {
                    return true;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("inclusions", null, {
                    reload: "inclusions"
                  });
                },
                function() {
                  $state.go("inclusions");
                }
              );
          }
        ]
      })
      .state("inclusions.edit", {
        parent: "inclusions",
        url: "/{inclusionsId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/inclusions/inclusions-create-dialog.html",
                controller: "InclusionsCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "InclusionsService",
                    function(InclusionsService) {
                        return InclusionsService.getById({id : $stateParams.inclusionsId}).$promise;
                    }
                  ],
                  generalServices: [
                    '$localStorage',
                    'GeneralServicesService',
                    function($localStorage, GeneralServicesService) {
                      return GeneralServicesService
                        .getByOrganizationTypeId({
                          id: $localStorage.current_organisation.organisationTypeId
                        })
                        .$promise;
                    }
                  ],
                  editParentInclusions: function() {
                    return false;
                  }
                }

              })
              .result.then(
                function() {
                  $state.go("inclusions", null, {
                    reload: "inclusions"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("inclusions.delete", {
        parent: "inclusions",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/inclusions/inclusions-delete-dialog.html",
                controller: "InclusionsDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "InclusionsService",
                    function(InclusionsService) {
                      return InclusionsService.getById({ id : $stateParams.id}).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("inclusions", null, {
                    reload: "inclusions"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
    // .state("inclusions-detail", {
    //   parent: "inclusions",
    //   url: "/{id}/overview",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "webApp.inclusions.detail.title"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/inclusions/inclusions-detail.html",
    //       controller: "InclusionsDetailController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     inclusions: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.getInclusionsByParentId({ id: $stateParams.id })
    //           .$promise;
    //       }
    //     ],
    //     inclusionsItems: [
    //       "$stateParams",
    //       "InclusionsItemsService",
    //       function($stateParams, InclusionsItemsService) {
    //         return InclusionsItemsService.getInclusionsItemsByInclusions({
    //           id: $stateParams.id
    //         }).$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state) {
    //         var currentStateData = {
    //           name: $state.current.name || "inclusions",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("inclusions-detail.image", {
    //   parent: "inclusions-detail",
    //   url: "/image/upload",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: ""
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/inclusions/image-upload-dialog.html",
    //           controller: "InclusionsImageUploadDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: function() {
    //               return {
    //                 file: null
    //               };
    //             },
    //             inclusions: [
    //               "$stateParams",
    //               "InclusionsService",
    //               function($stateParams, InclusionsService) {
    //                 return InclusionsService.get({ id: $stateParams.id }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-detail", null, { reload: "inclusions-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-detail.subinclusions", {
    //   parent: "inclusions",
    //   url: "/{id}/subinclusions",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "webApp.inclusions.detail.title"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/inclusions/inclusions-subinclusions.html",
    //       controller: "InclusionsSubInclusionsController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     inclusions: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.getInclusionsByParentId({ id: $stateParams.id })
    //           .$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state) {
    //         var currentStateData = {
    //           name: $state.current.name || "inclusions-detail",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("inclusions-detail.edit", {
    //   parent: "inclusions-detail",
    //   url: "/edit",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/inclusions/inclusions-dialog.html",
    //           controller: "InclusionsDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "InclusionsService",
    //               function(InclusionsService) {
    //                 return InclusionsService.get({ id: $stateParams.id }).$promise;
    //               }
    //             ],
    //             editParentInclusions: function() {
    //               return true;
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-detail", null, { reload: "inclusions-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-detail-new-inclusions", {
    //   parent: "inclusions-detail.subinclusions",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: ""
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/inclusions/inclusions-dialog.html",
    //           controller: "InclusionsDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: function() {
    //               return {
    //                 id: null,
    //                 uuid: null,
    //                 name: null,
    //                 brief: null,
    //                 description: null,
    //                 notes: null,
    //                 order: null,
    //                 contentStatus: "DRAFT",
    //                 statusReason: null,
    //                 organisationId: $localStorage.current_organisation.id,
    //                 organisationName: null,
    //                 parentId: $stateParams.id,
    //                 parentName: null,
    //                 generalInclusionsTypeId: null,
    //                 generalInclusionsTypeName: null
    //               };
    //             },
    //             editParentInclusions: function() {
    //               return false;
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-detail.subinclusions", null, {
    //               reload: "inclusions-detail.subinclusions"
    //             });
    //           },
    //           function() {
    //             $state.go("inclusions-detail.subinclusions");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-detail-new-inclusions-item", {
    //   parent: "inclusions-detail",
    //   url: "/inclusions-item/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/inclusions-items/inclusions-item-dialog.html",
    //           controller: "InclusionsItemDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "InclusionsItemsService",
    //               function(InclusionsService) {
    //                 return {
    //                   id: null,
    //                   uuid: null,
    //                   name: null,
    //                   brief: null,
    //                   notes: null,
    //                   description: null,
    //                   order: null,
    //                   contentStatus: "DRAFT",
    //                   statusReason: null,
    //                   inclusionsId: $stateParams.id,
    //                   inclusionsName: null,
    //                   generalInclusionsItemTypeId: null,
    //                   generalInclusionsItemTypeName: null,
    //                   servingOptionsGroupId: null,
    //                   servingOptionsGroupName: null
    //                 };
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-detail", null, { reload: "inclusions-detail" });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-galleries", {
    //   parent: "app",
    //   url: "/inclusions/{id}/gallery",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "Gallery"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/inclusions/inclusions-galleries.html",
    //       controller: "InclusionsGalleriesController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.get({ id: $stateParams.id }).$promise;
    //       }
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state, $) {
    //         var currentStateData = {
    //           name: $state.current.name || "locations-own",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("inclusions-galleries.new", {
    //   parent: "inclusions-galleries",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl: "core/routes/albums/album-create-dialog.html",
    //           controller: "AlbumCreateDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "InclusionsService",
    //               function(InclusionsService) {
    //                 return InclusionsService.get({ id: $stateParams.id })
    //                   .$promise;
    //               }
    //             ],
    //             album: function() {
    //               return {
    //                 albumType: "GENERAL",
    //                 caption: null, //album name
    //                 coverName: null, //uploaded cover image file name
    //                 coverUuid: null, //uploaded cover image
    //                 isDefaultAlbum: true, //put option slider
    //                 name: null, //album name
    //                 objectUuid: null //location uuid
    //               };
    //             }
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-galleries", null, {
    //               reload: "inclusions-galleries"
    //             });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-galleries.delete", {
    //   parent: "inclusions-galleries",
    //   url: "/{albumId}/delete",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-delete-dialog.html",
    //           controller: "AlbumDeleteDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md"
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-galleries", null, {
    //               reload: "inclusions-galleries"
    //             });
    //           },
    //           function() {
    //             $state.go("inclusions-galleries");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-album-detail", {
    //   parent: "inclusions-galleries",
    //   url: "/{albumId}/album",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "Gallery"
    //   },
    //   views: {
    //     "content@": {
    //       templateUrl: "core/routes/inclusions/inclusions-album-detail.html",
    //       controller: "InclusionsAlbumDetailController",
    //       controllerAs: "vm"
    //     }
    //   },
    //   resolve: {
    //     entity: [
    //       "$stateParams",
    //       "InclusionsService",
    //       function($stateParams, InclusionsService) {
    //         return InclusionsService.get({ id: $stateParams.id }).$promise;
    //       },
    //     ],
    //     album: [
    //       "$stateParams",
    //       "AlbumService",
    //       function($stateParams, AlbumService) {
    //         return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
    //       },
    //     ],
    //     previousState: [
    //       "$state",
    //       function($state, $) {
    //         var currentStateData = {
    //           name: $state.current.name || "inclusions-galleries",
    //           params: $state.params,
    //           url: $state.href($state.current.name, $state.params)
    //         };
    //         return currentStateData;
    //       }
    //     ]
    //   }
    // })
    // .state("inclusions-album-detail.edit", {
    //   parent: "inclusions-album-detail",
    //   url: "/edit",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "inclusions-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function ($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //           "core/routes/albums/album-dialog.html",
    //           controller: "AlbumDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: ["AlbumService",function (AlbumService) {
    //               return AlbumService.get({ id: $stateParams.albumId }).$promise;
    //             }
    //             ]
    //           }
    //         })
    //         .result.then(
    //         function () {
    //           $state.go("inclusions-album-detail", null, { reload: "inclusions-album-detail" });
    //         },
    //         function () {
    //           $state.go("inclusions-album-detail");
    //         }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-album-detail.makeCoverImage", {
    //   parent: "inclusions-album-detail",
    //   url: "/{imageId}/cover-image",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-change-cover-image-dialog.html",
    //           controller: "AlbumChangeCoverImageDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: [
    //               "AlbumService",
    //               "$stateParams",
    //               function(AlbumService, $stateParams) {
    //                 return AlbumService.get({ id: $stateParams.albumId })
    //                   .$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-album-detail", null, {
    //               reload: "inclusions-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("inclusions-album-detail");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-album-detail.deleteImage", {
    //   parent: "inclusions-album-detail",
    //   url: "/{imageId}/delete",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: [],
    //     pageTitle: "locations-own.detail.title"
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$localStorage",
    //     "$uibModal",
    //     function($stateParams, $state, $localStorage, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-image-delete-dialog.html",
    //           controller: "AlbumImageDeleteDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             entity: [
    //               "AlbumService",
    //               "$stateParams",
    //               function(AlbumService, $stateParams) {
    //                 return AlbumService.get({ id: $stateParams.albumId })
    //                   .$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-album-detail", null, {
    //               reload: "inclusions-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("inclusions-album-detail");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-album-detail.upload", {
    //   parent: "inclusions-album-detail",
    //   url: "/new",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/albums/album-upload-dialog.html",
    //           controller: "AlbumUploadDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "lg",
    //           resolve: {
    //             entity: [
    //               "$stateParams",
    //               "AlbumService",
    //               function ($stateParams, AlbumService) {
    //                 return AlbumService.get({ id: $stateParams.albumId }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //           function() {
    //             $state.go("inclusions-album-detail", null, {
    //               reload: "inclusions-album-detail"
    //             });
    //           },
    //           function() {
    //             $state.go("^");
    //           }
    //         );
    //     }
    //   ]
    // })
    // .state("inclusions-detail.addLabels", {
    //   parent: "inclusions-detail",
    //   url: "/{uuid}/{objectType}/add-labels",
    //   data: {
    //     requiresAuthentication: true,
    //     authorities: []
    //   },
    //   onEnter: [
    //     "$stateParams",
    //     "$state",
    //     "$uibModal",
    //     function ($stateParams, $state, $uibModal) {
    //       $uibModal
    //         .open({
    //           templateUrl:
    //             "core/routes/labels/object-label-create-dialog.html",
    //           controller: "ObjectLabelGroupCreateDialogController",
    //           controllerAs: "vm",
    //           backdrop: "static",
    //           size: "md",
    //           resolve: {
    //             labelGroups: [
    //               "LabelGroupsService",
    //               function (LabelGroupsService) {
    //                 return LabelGroupsService.getLabelGroupsByObjectType({
    //                   objectType: $stateParams.objectType
    //                 }).$promise;
    //               }
    //             ],
    //             entity: [
    //               "InclusionsService",
    //               function (InclusionsService) {
    //                 return InclusionsService.get({ id: $stateParams.id })
    //                   .$promise;
    //               }
    //             ],
    //             savedselectedLabels: [
    //               "ObjectLabelsService",
    //               function (ObjectLabelsService) {
    //                 return ObjectLabelsService.getLabelsByObjectId({
    //                   uuid: $stateParams.uuid
    //                 }).$promise;
    //               }
    //             ]
    //           }
    //         })
    //         .result.then(
    //         function () {
    //           $state.go("inclusions-detail", null, {
    //             reload: "inclusions-detail"
    //           });
    //         },
    //         function () {
    //           $state.go("inclusions-detail");
    //         });
    //     }
    //   ]
    // })
    //  .state("inclusions-detail.deleteLabel", {
    //           parent: "inclusions-detail",
    //          url: "/{itemToDelete}/delete-label",
    //
    //           data: {
    //             requiresAuthentication: true,
    //             authorities: []
    //           },
    //           onEnter: [
    //             "$stateParams",
    //             "$state",
    //             "$uibModal",
    //             function ($stateParams, $state, $uibModal) {
    //               $uibModal
    //                 .open({
    //                   templateUrl:
    //                     "core/routes/labels/object-label-delete-dialog.html",
    //                   controller: "ObjectLabelGroupDeleteDialogController",
    //                   controllerAs: "vm",
    //                   backdrop: "static",
    //                   size: "md"
    //                 })
    //                 .result.then(
    //                 function () {
    //                   $state.go("inclusions-detail", null, {
    //                     reload: "inclusions-detail"
    //                   });
    //                 },
    //                 function () {
    //                   $state.go("inclusions-detail");
    //                 }
    //       );
    //     }
    //   ]
    // });
  }
})();
