(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("VehicleClassDetailController", VehicleClassDetailController);

  VehicleClassDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS"
  ];

  function VehicleClassDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    ObjectLabelsService,
    ObjectFeaturesService,
    URLS
  ) {
    var vm = this;
    vm.vehicleClass = entity;
    console.log(vm.vehicleClass);
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.vehicleClass.uuid;
    vm.previousState = previousState.name;
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({
      uuid: vm.vehicleClass.uuid
    });
    console.log(vm.objectLabels);

    vm.objectFeatures = ObjectFeaturesService.getFeaturesByObjectId({
      uuid: vm.vehicleClass.uuid
    });
    console.log("object features");
    console.log(vm.objectFeatures);
  }
})();
