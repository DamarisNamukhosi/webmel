(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleClassDeleteDialogController',VehicleClassDeleteDialogController);

    VehicleClassDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'VehicleClassService'];

    function VehicleClassDeleteDialogController($uibModalInstance, entity, VehicleClassService) {
        var vm = this;

        vm.vehicleClass = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (vehicleClass) {
            console.log('changing vehicleClass contentStatus to DELETED');
            vm.vehicleClass.contentStatus = "DELETED";
            VehicleClassService.update(vehicleClass,
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
