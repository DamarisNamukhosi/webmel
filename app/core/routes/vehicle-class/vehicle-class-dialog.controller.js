(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('VehicleClassDialogController', VehicleClassDialogController);

  VehicleClassDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'VehicleClassService'];

  function VehicleClassDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, VehicleClassService ) {
    var vm = this;

    vm.vehicleClass = entity;
    vm.clear = clear;
    vm.save = save;
    // vm.selectedGeneralType = selectedGeneralType;

    // vm.subCategories = GeneralVehicleClassService.query();

    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;
      if (vm.vehicleClass.id !== null) {
        VehicleClassService.update(vm.vehicleClass, onSaveSuccess, onSaveError);
      } else {
        VehicleClassService.create(vm.vehicleClass, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    // function selectedGeneralType(generalTypeId) {
    //   angular.forEach(vm.subCategories, function(subCategory) {
    //     if (subCategory.id == generalTypeId) {
    //       vm.vehicleClass.name = subCategory.name;
    //       vm.vehicleClass.abbreviation = subCategory.abbreviation;
    //       vm.vehicleClass.order = subCategory.order;
    //       vm.vehicleClass.description = subCategory.description;
    //       vm.vehicleClass.capacity = subCategory.capacity;
    //       vm.vehicleClass.extendedCapacity = subCategory.extendedCapacity;
    //
    //     }
    //   });
    // }

  }
})();
