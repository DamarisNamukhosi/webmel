(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("vehicle-class", {
        parent: "app",
        url: "/vehicle-class",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class.html",
            controller: "VehicleClassController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicleClass: [
            "$stateParams",
            "$localStorage",
            "VehicleClassService",
            function($stateParams,$localStorage, VehicleClassService) {
              return VehicleClassService.getVehicleClassByOrganizationId({id : $localStorage.current_organisation.id}).$promise;
            }
          ]
        }
      })
      .state('vehicle-class.new', {
        parent: 'vehicle-class',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/vehicle-class/vehicle-class-dialog.html',
                controller: 'VehicleClassDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function() {
                    return {
                      name: null,
                      abbreviation: null,
                      description: null,
                      capacity: null,
                      extendedCapacity: null,
                      contentStatus: "DRAFT",
                      generalTransportSubCategoryId: null,
                      generalTransportSubCategoryName: null,
                      id: null,
                      notes: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName:  $localStorage.current_organisation.name,
                      statusReason: null,
                      uuid: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go('vehicle-class', null, {
                    reload: 'vehicle-class'
                  });
                },
                function() {
                  $state.go('vehicle-class');
                }
              );
          }
        ]
      })
      .state("vehicle-class-detail", {
        parent: "vehicle-class",
        url: "/{id}/details",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle-class.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class-detail.html",
            controller: "VehicleClassDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleClassService",
            function($stateParams, VehicleClassService) {
              return VehicleClassService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "app",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-class-detail.edit", {
        parent: "vehicle-class-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-dialog.html",
                controller: "VehicleClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-detail", null, {
                    reload: "vehicle-class-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own", {
        parent: "vehicle-class",
        url: "/own",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class-own.html",
            controller: "VehicleClassOwnController",
            controllerAs: "vm"
          }
        },
        resolve: {
          vehicleClass: [
            "$stateParams",
            "$localStorage",
            "VehicleClassService",
            function($stateParams, $localStorage, VehicleClassService) {
              return VehicleClassService.getVehicleClassByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("vehicle-class-own.new", {
        parent: "vehicle-class-own",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [
            "TRANSPORTER_USER",
            "PROFESSIONAL_USER",
            "T_OPERATOR_USER",
            "MANAGE_PROFESSIONALS"
          ],
          pageTitle: "profsApp.professional.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-dialog.html",
                controller: "VehicleClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      registration: null,
                      brief: null,
                      description: null,
                      notes: null,
                      capacity: null,
                      profileStatus: "INACTIVE",
                      contentStatus: "DRAFT",
                      statusReason: "Created",
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null,
                      generalTransportSubCategoryId: null,
                      generalTransportSubCategoryName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own", null, {
                    reload: "vehicle-class-own"
                  });
                },
                function() {
                  $state.go("vehicle-class-own");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own-detail", {
        parent: "vehicle-class-own",
        url: "/{id}/vehicle",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.vehicle-class.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class-detail.html",
            controller: "VehicleClassDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleClassService",
            function($stateParams, VehicleClassService) {
              return VehicleClassService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "vehicle-class-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-class.edit", {
        parent: "vehicle-class",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-dialog.html",
                controller: "VehicleClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class", null, {
                    reload: "vehicle-class"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own.edit", {
        parent: "vehicle-class-own",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-dialog.html",
                controller: "VehicleClassDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own", null, {
                    reload: "vehicle-class-own"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class.delete", {
        parent: "vehicle-class",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-delete-dialog.html",
                controller: "VehicleClassDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class", null, {
                    reload: "vehicle-class"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own.delete", {
        parent: "vehicle-class-own-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/vehicle-class-delete-dialog.html",
                controller: "VehicleClassDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class.profile", {
        parent: "vehicle-class-own-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/vehicle-class/image-upload/image-upload-dialog.html",
                controller: "VehicleClassImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    "$stateParams",
                    "VehicleClassService",
                    function($stateParams, VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-galleries", {
        parent: "app",
        url: "/vehicle-class/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class-galleries.html",
            controller: "VehicleClassGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleClassService",
            function($stateParams, VehicleClassService) {
              return VehicleClassService.get({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-class-galleries.new", {
        parent: "vehicle-class-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-galleries", null, {
                    reload: "vehicle-class-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-galleries.delete", {
        parent: "vehicle-class-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-galleries", null, {
                    reload: "vehicle-class-galleries"
                  });
                },
                function() {
                  $state.go("vehicle-class-galleries");
                }
              );
          }
        ]
      })
      .state("vehicle-class-album-detail", {
        parent: "vehicle-class-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/vehicle-class/vehicle-class-album-detail.html",
            controller: "VehicleClassAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "VehicleClassService",
            function($stateParams, VehicleClassService) {
              return VehicleClassService.get({
                id: $stateParams.id
              }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "vehicle-class-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("vehicle-class-album-detail.edit", {
        parent: "vehicle-class-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "vehicle-class-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService", function(AlbumService) {
                    return AlbumService.get({
                      id: $stateParams.albumId
                    }).$promise;
                  }]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-album-detail", null, {
                    reload: "vehicle-class-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-class-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-class-album-detail.makeCoverImage", {
        parent: "vehicle-class-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({
                          id: $stateParams.albumId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-album-detail", null, {
                    reload: "vehicle-class-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-class-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-class-album-detail.deleteImage", {
        parent: "vehicle-class-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({
                          id: $stateParams.albumId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-album-detail", null, {
                    reload: "vehicle-class-album-detail"
                  });
                },
                function() {
                  $state.go("vehicle-class-album-detail");
                }
              );
          }
        ]
      })
      .state("vehicle-class-album-detail.upload", {
        parent: "vehicle-class-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-album-detail", null, {
                    reload: "vehicle-class-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own-detail.addLabels", {
        parent: "vehicle-class-own-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          "$localStorage",
          function($stateParams, $state, $uibModal, $localStorage) {
            $uibModal
              .open({
                templateUrl: "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                          objectType: $stateParams.objectType
                        })
                        .$promise;
                    }
                  ],
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              ); //
          }
        ]
      })
      .state("vehicle-class-own-detail.deleteLabel", {
        parent: "vehicle-class-own-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("vehicle-class-own-detail.addFeature", {
        parent: "vehicle-class-own-detail",
        url: "/{uuid}/{objectType}/add-features",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/features/object-feature-create-dialog.html",
                controller: "ObjectFeatureCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  features: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "VehicleClassService",
                    function(VehicleClassService) {
                      return VehicleClassService.get({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ],
                  savedselectedFeatures: [
                    "ObjectFeaturesService",
                    function(ObjectFeaturesService) {
                      return ObjectFeaturesService.getFeaturesByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("vehicle-class-own-detail");
                });
          }
        ]
      })
      .state("vehicle-class-own-detail.deleteFeature", {
        parent: "vehicle-class-own-detail",
        url: "/{itemToDelete}/delete-feature",

        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/features/object-feature-delete-dialog.html",
                controller: "ObjectFeatureDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("vehicle-class-own-detail", null, {
                    reload: "vehicle-class-own-detail"
                  });
                },
                function() {
                  $state.go("vehicle-class-own-detail");
                }
              );
          }
        ]
      });
  }
})();
