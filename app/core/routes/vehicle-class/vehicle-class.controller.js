(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehicleClassController', VehicleClassController);

    VehicleClassController.$inject = ['$scope', '$location', '$state', 'vehicleClass', 'URLS', 'VehicleClassService'];

  function VehicleClassController($scope, $location, $state, vehicleClass ,URLS , VehicleClassService) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.profile_url = URLS.PROFILE_IMAGE_URL;
        vm.noRecordsFound=true;
        vm.vehicleClass = vehicleClass;
        console.log('vehicleClass....');
        console.log(vm.vehicleClass);

        if (vm.vehicleClass.length === 0){
            vm.noRecordsFound = false;
        }

        vm.sortableOptions = {
          stop: function (e, ui) {
            vm.dragAlert = false;
            updateRank();
          }
        }

        function updateRank() {
          console.log("Updating Rank");
          var count = 1;

          angular.forEach(vm.vehicleClass, function (vehicleClass) {

            vehicleClass.order = count * 100;

            count = count + 1;
          });

          console.log("vm.vehicleClass");
          console.log(vm.vehicleClass);


          // call update
          VehicleClassService.updateList(vm.vehicleClass);

        }

    }





})();
