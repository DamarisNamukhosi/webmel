(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('VehiclesClassOwnController', VehiclesClassOwnController);

    VehiclesClassOwnController.$inject = ['$scope', '$location', '$state', 'vehicleClass', 'URLS'];

    function VehiclesClassOwnController ($scope, $location, $state, vehicleClass, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.profile_url = URLS.PROFILE_IMAGE_URL;

        vm.vehicleClass = vehicleClass;
        vm.noRecordsFound= false;
        if(vm.vehicleClass.length === 0){
            vm.noRecordsFound = true;
        }
    }
})();
