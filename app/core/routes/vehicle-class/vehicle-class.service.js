(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('VehicleClassService', VehicleClassService);

    VehicleClassService.$inject = ['$resource', '$localStorage', 'URLS'];

    function VehicleClassService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getVehicleClass': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes'
        },
        'getVehicleClassByOrganizationId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes/filter-by-organisation/:id?sort=order%2Casc'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          // isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes/:id'
        },
        'updateList': {
          method: 'POST',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/vehicle-classes/save-list'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        }
      });
    }
})();
