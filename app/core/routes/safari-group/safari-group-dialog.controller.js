(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SafariGroupDialogController', SafariGroupDialogController);

  SafariGroupDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$state', '$localStorage', 'SafariGroupsService', 'OrganizationsService', 'URLS', 'LocationsService', 'entity', 'safariConstants'];

  function SafariGroupDialogController($timeout, $scope, $stateParams, $uibModalInstance, $state, $localStorage, SafariGroupsService, OrganizationsService, URLS, LocationsService, entity, safariConstants) {
    var vm = this;

    vm.safariGroup = entity;
    console.log("vm.safariGroup");
    console.log(vm.safariGroup);

    //variable declarations
    vm.years = safariConstants.safariYears;
    vm.months = safariConstants.months;
    vm.packageTypes = safariConstants.packageTypes;
    vm.safariLanguages = safariConstants.supportedLanguages;
    vm.reccurentFolders = false;
    //function declaration
    vm.clear = clear;
    vm.save = save;
    vm.locationTypeChange = locationTypeChange;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.getLocs = getLocs;
    vm.onTargetSelected = onTargetSelected
    vm.toggleTypeSelect = toggleTypeSelect;
    vm.onMonthSelect = onMonthSelect;
    //vm.onLocationManagerSelected = onLocationManagerSelected;

    vm.tempParentLocations = [];
    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
    vm.newLocation = {
      id: null,
      name: null,
    };

    initConfigs();

    function initConfigs() {
      if ($stateParams.link <= 2) {
        console.log("init params");
        vm.safariFilterParams = {
          "types": $stateParams.types,
          "safariType": $stateParams.safariType,
          "month": $stateParams.month,
          "year": $stateParams.year,
          "groupId": $stateParams.groupId,
          "targetId": $stateParams.targetId,
          "targetName": $stateParams.targetName,
          "locationId": $stateParams.locationId,
          "locationName": $stateParams.locationName,
          "locationType": $stateParams.locationType
          //"locationType": $stateParams.locationType == "SET_DEPARTURE" || $stateParams.statePackageType == "EXTENSION" ? "REGION" : "C0UNTRY"
        }
        console.log(vm.safariFilterParams);
      }
      vm.isEdit = $state.includes("edit");
      vm.newGroup = angular.equals($state.current.name, "safari-group.new");;
      vm.newGroupRecurrentGroup = angular.equals($state.current.name, "safari-group-detail.new");
      //
      console.log(vm.safariGroup);
      if (vm.safariGroup.id != null) {
        vm.currentLink =$stateParams.link;
        var configs = angular.fromJson(vm.safariGroup.configs);
        vm.safariGroup.configs = angular.fromJson(vm.safariGroup.configs);

        //if(configs.types.length)
        console.log(configs);
        if(!angular.equals(configs,null)&& angular.isDefined(configs.types)){
        var types =  configs.types;
        vm.isFIT = types.includes("FIT");
      }
        if (!angular.equals(vm.safariGroup.configs, null)) {
          if (vm.safariGroup.configs.types != undefined && vm.safariGroup.configs.types.length > 1) {
            vm.safariGroup.oneType = false;
          } else {
            if (angular.equals(vm.safariGroup.safariType, null)) {
              vm.safariGroup.oneType = false;
            } else {
              vm.safariGroup.oneType = true;
            }
          }
        }
        console.log(angular.fromJson(vm.safariGroup.configs));
      } else {
        if ($stateParams.link > 0) {
          vm.currentLink = $stateParams.link;
          SafariGroupsService
            .getById({ id: $stateParams.groupId })
            .$promise.then(function (results) {
              vm.parentGroup = results;
              console.log("parent group... ");
              console.log(vm.parentGroup);
              var configs = angular.fromJson(vm.parentGroup.configs);
              var types =  configs.types;
              vm.isFIT = types.includes("FIT");
              if(angular.isDefined(vm.parentGroup.configs) && !angular.equals(vm.parentGroup.configs, null)){
                var tempConfig = {
                  "types": configs.types,
                  "year":  configs.year,
                  "month":configs.month,
                  "targetId":configs.targetId ,
                  "targetName": configs.targetName,
                  "locationName": configs.locationName,
                  "locationId": configs.slocationId,
                  "validationStatus": configs.validationStatus,
                  "defaultLanguage": configs.defaultLanguage
                  //"navlinks": initLinks()
                }
                vm.safariGroup.targetId = configs.targetId ,
                vm.safariGroup.targetName = configs.targetName ,

                vm.safariGroup.configs =  tempConfig;
                console.log("vm.safariGroup");
                console.log(vm.safariGroup);

                //vm.safariGroup.configs.types =  vm.parentGroup.configs.types;
                //vm.safariGroup.targetId =  vm.parentGroup.configs.targetId;
              }
            });
        }
        if(angular.equals(vm.currentLink, '4')){
        console.log("4444");

        }
      }
      console.log(vm.safariGroup);
      console.log(angular.equals(vm.safariGroup.safariType, null));
      //getLocalStorageFolderValues();
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      console.log(vm.safariGroup);
      vm.isSaving = true;

      if (vm.safariGroup.id !== null) {
        vm.safariGroup.configs =  JSON.stringify(vm.safariGroup.configs);
        console.log(vm.safariGroup);
        SafariGroupsService.update(vm.safariGroup, onSaveSuccess, onSaveError);
      } else {
        setConfigs();
        //updateLocalStorage();
        console.log("creating..");
        console.log(vm.safariGroup);
        SafariGroupsService.create(vm.safariGroup, onSaveSuccess, onSaveError);

      }
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
    function updateLocalStorage() {
      console.log("onMonthSelect..");
      vm.safariFilterParams = {
        "types": vm.safariGroup.types,
        "safariType": vm.safariGroup.safariType,
        "month": vm.safariGroup.month,
        "year": vm.safariGroup.year,
        "groupId": vm.safariGroup.groupId,
        "targetId": vm.safariGroup.targetId,
        "targetName": vm.safariGroup.targetName,
        "locationId": vm.safariGroup.locationId,
        "locationName": vm.safariGroup.locationName,
        "locationType": vm.safariGroup.locationType
        //"locationType": $stateParams.locationType == "SET_DEPARTURE" || $stateParams.statePackageType == "EXTENSION" ? "REGION" : "C0UNTRY"
      }
      $localStorage.safariFilterParams = vm.safariFilterParams;
    }
    function getLocalStorageFolderValues() {
      if (angular.isDefined($localStorage.safariFilterParams) && !angular.equals($localStorage.safariFilterParams, null)) {
        console.log("some items");
        if (angular.equals($stateParams.link, '5')) {
          console.log($stateParams.link);
          vm.safariGroup.targetId = $localStorage.safariFilterParams.targetId;
          vm.safariGroup.targetName = $localStorage.safariFilterParams.targetName;
          vm.safariGroup.locationName = $localStorage.safariFilterParams.locationName;
          vm.safariGroup.locationId = $localStorage.safariFilterParams.locationId;
          vm.safariGroup.types = $localStorage.safariFilterParams.types;
          vm.safariGroup.safariType = $localStorage.safariFilterParams.safariType;
          vm.safariGroup.month = $localStorage.safariFilterParams.month;
        }else if(angular.equals($stateParams.link, '4')){
          console.log($stateParams.link);
          if($localStorage.safariFilterParams.types.includes("FIT")){
            vm.safariGroup.month = $localStorage.safariFilterParams.month;
          }else{
            vm.safariGroup.targetName = $localStorage.safariFilterParams.targetName;
            vm.safariGroup.locationName = $localStorage.safariFilterParams.locationName;
          }

        }else if(angular.equals($stateParams.link, '3')){
          console.log($stateParams.link);
          vm.safariGroup.targetId = $localStorage.safariFilterParams.targetId;
          vm.safariGroup.targetName = $localStorage.safariFilterParams.targetName;

        }else if (angular.equals($stateParams.link, '2')){
          console.log($stateParams.link);

        }
      }
    }
    function setConfigs() {
      console.log("$state.current.name");
      console.log($state.current.name);
      vm.currentLink = $stateParams.link;

      if (angular.equals($state.current.name, "safari-group.new")) {
        console.log("safari-group.new");
        var tempConfig = {
          "year": vm.safariGroup.year,
          "defaultLanguage": vm.safariGroup.defaultLanguage,
          "navlinks": initLinks()
        }
      } else {
        console.log("seting configs");
        if(angular.isDefined(vm.parentGroup.configs) && !angular.equals(vm.parentGroup.configs, null)){
        console.log(angular.equals( vm.safariGroup.types , null));
        console.log(vm.parentGroup.configs);
          // vm.safariGroup.configs.targetId = angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ;
          // vm.safariGroup.configs.targetName = angular.equals(vm.safariGroup.targetName,null) ?  vm.parentGroup.targetName.targetId : vm.safariGroup.targetName ;
          // vm.safariGroup.configs.locationId = angular.equals(vm.safariGroup.configs.locationName,null) ?  vm.parentGroup.configs.locationId : vm.safariGroup.configs.locationId ;
          // vm.safariGroup.configs.locationId = angular.equals(vm.safariGroup.configs.locationName,null) ?  vm.parentGroup.configs.locationName : vm.safariGroup.configs.locationName ;
          // vm.safariGroup.configs.types = angular.equals(vm.safariGroup.configs.types,null) ?  vm.parentGroup.configs.types : vm.safariGroup.configs.types ;

          // vm.safariGroup.configs.month = angular.equals(vm.safariGroup.configs.month,null) ?  vm.parentGroup.configs.month : vm.safariGroup.configs.month ;

          // vm.safariGroup.configs.navlinks = initLinks();

          if(vm.currentLink == '2'){
            var tempConfig = {
               "types": angular.equals( vm.safariGroup.configs.types , null) ? vm.parentGroup.configs.types: vm.safariGroup.configs.types,
               "year":  angular.equals(vm.safariGroup.configs.year, null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.year ,
               "month":angular.equals(vm.safariGroup.configs.month,null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.month,
               "targetId":angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ,
               "targetName": angular.equals(vm.safariGroup.targetName,null) ? vm.parentGroup.configs.targetName :  vm.safariGroup.targetName ,
               "locationName":angular.equals(vm.safariGroup.configs.locationName,null) ? vm.parentGroup.configs.locationName : vm.safariGroup.configs.locationName,
               "locationId":  angular.equals(vm.safariGroup.configs.locationId,null)  ? vm.parentGroup.configs.locationId : vm.safariGroup.configs.locationId,
               "validationStatus": angular.equals(vm.safariGroup.configs.validationStatus,null)  ? vm.parentGroup.configs.validationStatus : "PENDING",
               "defaultLanguage": angular.equals(vm.safariGroup.configs.defaultLanguage,null)  ? vm.parentGroup.configs.defaultLanguage : vm.safariGroup.configs.defaultLanguage,
               "navlinks": initLinks()
             }
           }
       else if(vm.currentLink == '3'){
       var tempConfig = {
          "types": angular.equals( vm.safariGroup.configs.types , null) ? vm.parentGroup.configs.types: vm.safariGroup.configs.types,
          "year":  angular.equals(vm.safariGroup.configs.year, null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.year ,
          "month":angular.equals(vm.safariGroup.configs.month,null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.month,
          "targetId":angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ,
          "targetName": angular.equals(vm.safariGroup.targetName,null) ? vm.parentGroup.configs.targetName :  vm.safariGroup.targetName ,
          "locationName":angular.equals(vm.safariGroup.configs.locationName,null) ? vm.parentGroup.configs.locationName : vm.safariGroup.configs.locationName,
          "locationId":  angular.equals(vm.safariGroup.configs.locationId,null)  ? vm.parentGroup.configs.locationId : vm.safariGroup.configs.locationId,
          "validationStatus": angular.equals(vm.safariGroup.configs.validationStatus,null)  ? vm.parentGroup.configs.validationStatus : "PENDING",
          "defaultLanguage": angular.equals(vm.safariGroup.configs.defaultLanguage,null)  ? vm.parentGroup.configs.defaultLanguage : vm.safariGroup.configs.defaultLanguage,
          "navlinks": initLinks()
        }
      }else  if(vm.currentLink == '4'){
        // if(vm.isFIT){
        //   vm.isFIT = types.includes("FIT");
        //   vm.safariGroup.safariType= "FIT";
        // }
        var tempConfig = {
           "types": angular.equals( vm.safariGroup.configs.types , null) ? vm.parentGroup.configs.types: vm.safariGroup.configs.types,
           "year":  angular.equals(vm.safariGroup.configs.year, null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.year ,
           "month":angular.equals(vm.safariGroup.configs.month,null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.month,
           "targetId":angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ,
           "targetName": angular.equals(vm.safariGroup.targetName,null) ? vm.parentGroup.configs.targetName :  vm.safariGroup.targetName ,
           "locationName":angular.equals(vm.safariGroup.configs.locationName,null) ? vm.parentGroup.configs.locationName : vm.safariGroup.configs.locationName,
           "locationId":  angular.equals(vm.safariGroup.configs.locationId,null)  ? vm.parentGroup.configs.locationId : vm.safariGroup.configs.locationId,
           "validationStatus": angular.equals(vm.safariGroup.configs.validationStatus,null)  ? vm.parentGroup.configs.validationStatus : "PENDING",
           "defaultLanguage": angular.equals(vm.safariGroup.configs.defaultLanguage,null)  ? vm.parentGroup.configs.defaultLanguage : vm.safariGroup.configs.defaultLanguage,
           "navlinks": initLinks()
         }
         if(tempConfig.types == "FIT"){
          vm.safariGroup.safariType= "FIT";
         }
       }else  if(vm.currentLink == '5'){
        var tempConfig = {
           "types": angular.equals( vm.safariGroup.configs.types , null) ? vm.parentGroup.configs.types: vm.safariGroup.configs.types,
           "year":  angular.equals(vm.safariGroup.configs.year, null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.year ,
           "month":angular.equals(vm.safariGroup.configs.month,null) ?  vm.parentGroup.configs.year : vm.safariGroup.configs.month,
           "targetId":angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ,
           "targetName": angular.equals(vm.safariGroup.targetName,null) ? vm.parentGroup.configs.targetName :  vm.safariGroup.targetName ,
           "locationName":angular.equals(vm.safariGroup.configs.locationName,null) ? vm.parentGroup.configs.locationName : vm.safariGroup.configs.locationName,
           "locationId":  angular.equals(vm.safariGroup.configs.locationId,null)  ? vm.parentGroup.configs.locationId : vm.safariGroup.configs.locationId,
           "validationStatus": angular.equals(vm.safariGroup.configs.validationStatus,null)  ? vm.parentGroup.configs.validationStatus : "PENDING",
           "defaultLanguage": angular.equals(vm.safariGroup.configs.defaultLanguage,null)  ? vm.parentGroup.configs.defaultLanguage : vm.safariGroup.configs.defaultLanguage,
           "navlinks": initLinks()
         }
       }
      }
      }
      console.log(vm.safariGroup.configs);
      vm.safariGroup.configs = angular.toJson(tempConfig);
      console.log(vm.safariGroup.configs);

    }
    function initLinks(type) {
      console.log("init links ready");
      vm.currentLink = $stateParams.link;
      if (angular.equals($state.current.name, "safari-group.new")) {
        vm.links = [{
          link: 1,
          name: vm.safariGroup.name,
          value: vm.safariGroup.name,
          visible: false
        }]
      } else {
        if (vm.currentLink > 0) {

          var configs = angular.fromJson(vm.parentGroup.configs);
          vm.links =  configs.navlinks;
          //angular.fromJson(vm.parentGroup.configs).navlinks[0].value =  vm.parentGroup.id;
          // var tempConfig = {
          //   "types": angular.equals( vm.safariGroup.types , null) ? vm.parentGroup.configs.types: vm.safariGroup.types,
          //   "year":  angular.equals(vm.safariGroup.year, null) ?  vm.parentGroup.configs.year : vm.safariGroup.year ,
          //   "month":angular.equals(vm.safariGroup.month,null) ?  vm.parentGroup.configs.year : vm.safariGroup.month,
          //   "targetId":angular.equals(vm.safariGroup.targetId,null) ?  vm.parentGroup.configs.targetId : vm.safariGroup.targetId ,
          //   "targetName": angular.equals(vm.safariGroup.targetName,null) ? vm.parentGroup.configs.targetName :  vm.safariGroup.targetName ,
          //   "locationName":angular.equals(vm.safariGroup.locationName,null) ? vm.parentGroup.configs.locationName : vm.safariGroup.locationName,
          //   "locationId":  angular.equals(vm.safariGroup.locationId,null)  ? vm.parentGroup.configs.locationId : vm.safariGroup.locationId,
          //   "validationStatus": angular.equals(vm.safariGroup.validationStatus,null)  ? vm.parentGroup.configs.validationStatus : "PENDING",
          //   "defaultLanguage": angular.equals(vm.safariGroup.defaultLanguage,null)  ? vm.parentGroup.configs.defaultLanguage : vm.safariGroup.defaultLanguage,
          //   "navlinks": initLinks()
          // }
          //vm.links.push(angular.fromJson(vm.parentGroup.configs).navlinks[0]);
          console.log(vm.links);
          angular.forEach(vm.links, function (link) {
            if (link.link == vm.currentLink - 1) {
              console.log("parent link");
              link.value = vm.parentGroup.id;
            }
          });
          console.log(angular.fromJson(vm.parentGroup.configs).navlinks[0].value);
          if (vm.currentLink > 1) {
            var tempLink = {
              "link": vm.currentLink,
              "name": vm.safariGroup.name,
              "value": vm.safariGroup.name,
              visible: false
            }
          }
          vm.links.push(tempLink);
          console.log("new linkss");
          console.log(vm.links);
          vm.user = {};
        } else {
          vm.links = [{
            link: 1,
            name: vm.safariGroup.name,
            value: $stateParams.year,
            visible: false
          }]
        }
      }
      return vm.links;
    }

    function locationTypeChange(id) {
      vm.tempParentLocations = vm.locations;
      console.log('changed location Type id');

      // angular.forEach(vm.locations, function(location){
      //     if((location.typeId < id || (id ==  10 || id ==11) && (location.typeId == 12 || location.typeId == 13) ) && !((location.typeId == 10 || location.typeId == 11) && (id ==  12 || id ==13))){
      //         vm.tempParentLocations.push(location);
      //     }
      // })

    }


    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {

      vm.safariGroup.configs.locationId = $item.id;
      vm.safariGroup.configs.locationName = $item.name;
      // vm.safariFilterParams.locationId = $item.id;
      // vm.safariFilterParams.locationName = $item.name;

    }

    function getLocs(val, type) {
      return OrganizationsService
        .getOrganizationsByType({
          type: type,
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });
    }



    function onTargetSelected($item, $model) {
      vm.safariGroup.targetId = $item.id;
      vm.safariGroup.targetName = $item.name;
      // vm.safariFilterParams.targetId = $item.id;
      // vm.safariFilterParams.targetName = $item.name;
    }

    function toggleTypeSelect() {
      console.log("toggle");
      console.log(vm.safariGroup.oneType);
      if (vm.safariGroup.oneType) {
        vm.safariGroup.types = null
      } else {
        vm.safariGroup.safariType = null;
        vm.safariGroup.month = null;
      }
    }

    function onMonthSelect() {
      vm.safariFilterParams.month = vm.safariGroup.month;
    }
  }
})();
