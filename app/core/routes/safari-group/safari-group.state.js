(function() {
  "use strict";

  angular
    .module("webApp")
    .config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider.state("safari-group", {
        parent: "app",
        url: "/products",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/safari-group/safari-groups.html",
            controller: "SafariGroupsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          safariGroups: [
            "$stateParams",
            "$localStorage",
            "SafariGroupsService",
            function($stateParams, $localStorage, SafariGroupsService) {
              return SafariGroupsService
                .getByParentId({
                  orgId: $localStorage.current_organisation.id
                })
                .$promise;
            }
          ]
        }
      })

      .state("safari-group.new", {
        parent: "safari-group",
        url: "/newFolder",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.safari-group.detail.title"
        },
        // views: {
        //   "content@": {
        //     templateUrl: 'core/routes/safari-group/safari-group/safari-group-dialog.html',
        //     controller: 'SafariGroupDialogController',
        //     controllerAs: "vm"
        //   }
        // },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safari-group/safari-group-dialog.html',
                controller: 'SafariGroupDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: {
                    id: null,
                    uuid: null,
                    name: null,
                    abbreviation: null,
                    defaultLanguage: 'en',
                    description: null,
                    targetId: null,
                    targetName: null,
                    contentStatus: 'DRAFT',
                    statusReason: null,
                    parentId: $stateParams.parentId,
                    parentName: null,
                    defaultSafariId: null,
                    defaultSafariName: null,
                    safariType: null,
                    configs: null,
                    oneType: true,
                    types: [],
                    year: $stateParams.year,
                    month: null,
                    terminal: false,
                    orgId: $localStorage.current_organisation.id,
                    orgName: $localStorage.current_organisation.name
                  }
                }
              })
              .result
              .then(function() {
                $state.go("safari-group", null, {
                  reload: "safari-group"
                });
              }, function() {
                $state.go("^");
              });
          }
        ]
      })
      .state("safari-group-detail.new", {
        parent: "safari-group-detail",
        url: "/{link}/{targetId}/{locationId}/{type}/newFolder",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.safari-group.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safari-group/safari-group-dialog.html',
                controller: 'SafariGroupDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: {
                    id: null,
                    uuid: null,
                    name: null,
                    abbreviation: null,
                    defaultLanguage: 'en',
                    description: null,
                    targetId: $stateParams.targetId,
                    targetName: null,
                    contentStatus: 'DRAFT',
                    statusReason: null,
                    parentId: $stateParams.groupId,
                    parentName: null,
                    defaultSafariId: null,
                    defaultSafariName: null,
                    safariType: null,
                    configs: null,
                    oneType: true,
                    types: [],
                    year: $stateParams.year,
                    month: null,
                    terminal: false,
                    orgId: $localStorage.current_organisation.id,
                    orgName: $localStorage.current_organisation.name,
                    locationName: null,
                    locationId: $stateParams.locationId
                  }
                }
              })
              .result
              .then(function() {
                $state.go("safari-group-detail", null, {
                  reload: "safari-group-detail"
                });
              }, function() {
                $state.go("^");
              });
          }
        ]
      })

      .state("safari-group-detail", {
        parent: "safari-group",
        url: "/{groupId}/{year}/view",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/safari-group/safari-group-detail.html",
            controller: "SafarigroupDetailController",
            controllerAs: "vm"
          }
        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id',
            squash: true
          },
          search: null
        },
        resolve: {
          group: [
            "$stateParams",
            "SafariGroupsService",
            function($stateParams, SafariGroupsService) {
              return SafariGroupsService
                .getById({
                  id: $stateParams.groupId
                })
                .$promise;
            }
          ],
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "safari-group-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("safari-group-detail.edit", {
        parent: "safari-group-detail",
        url: "/{link}/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safari-group/safari-group-dialog.html',
                controller: 'SafariGroupDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "SafariGroupsService",
                    function(SafariGroupsService) {
                      return SafariGroupsService
                        .getById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go("safari-group-detail", null, {
                  reload: "safari-group-detail"
                });
              }, function() {
                $state.go("^");
              });
          }
        ]
      })
      .state('safari-group-detail.delete', {
        parent: 'safari-group-detail',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/safari-group/safari-group-delete-dialog.html',
                controller: 'SafariGroupDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'SafariGroupsService',
                    function($localStorage, SafariGroupsService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return SafariGroupsService.getById({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('safari-group-detail', null, {
                  reload: 'safari-group-detail'
                });
              }, function() {
                $state.go('^');
              });
          }
        ]
      })
      .state("safari-group.edit", {
        parent: "safari-group",
        url: "/{groupId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safari-group/safari-group-dialog.html',
                controller: 'SafariGroupDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "SafariGroupsService",
                    function(SafariGroupsService) {
                      return SafariGroupsService
                        .getById({
                          id: $stateParams.groupId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go("safari-group", null, {
                  reload: "safari-group"
                });
              }, function() {
                $state.go("^");
              });
          }
        ]
      })
      .state('safari-group.delete', {
        parent: 'safari-group',
        url: '/{groupId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/safari-group/safari-group-delete-dialog.html',
                controller: 'SafariGroupDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'SafariGroupsService',
                    function($localStorage, SafariGroupsService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return SafariGroupsService.getById({
                        id: $stateParams.groupId
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('safari-group', null, {
                  reload: 'safari-group'
                });
              }, function() {
                $state.go('^');
              });
          }
        ]
      })
      .state('safari-group-detail.deleteSafari', {
        parent: 'safari-group-detail',
        url: '/{safariId}/deleteSafari',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/safari-group/safari-group-delete-dialog.html',
                controller: 'SafariGroupDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'SafarisService',
                    function($localStorage, SafarisService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return SafarisService.getById({
                        id: $stateParams.safariId
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('safari-group-detail', null, {
                  reload: true
                });
              }, function() {
                $state.go('^');
              });
          }
        ]
      })
      .state('safari-group-detail.editSafari', {
        parent: 'safari-group-detail',
        url: '/{link}/{id}/editSafari',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/safari-group/safari-group-dialog.html',
                controller: 'SafariGroupDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'SafarisService',
                    function($localStorage, SafarisService) {
                      // get the organization id from the localStorage
                      // $localStorage.current_organisation.id
                      return SafarisService.getById({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('safari-group-detail', null, {
                  reload: true
                });
              }, function() {
                $state.go('^');
              });
          }
        ]
      });;
  }
})();
