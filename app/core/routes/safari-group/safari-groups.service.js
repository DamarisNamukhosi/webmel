(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('SafariGroupsService', SafariGroupsService);

    SafariGroupsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function SafariGroupsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getVehicles': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        },
        'getByParentId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/filter-by-parent?parent=:parentId&org=:orgId'
        },
        'getFiltered': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          //general-filter/:id?language=:language&skill=:skill
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/filter/:orgId?type=:type&year=:year&target=:target'
        },
        'getSafarisByGroupId': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          //general-filter/:id?language=:language&skill=:skill //filter/1?groupId=7
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/filter/:orgId?groupId=:groupId'
        },
        ///get-full
        'getFull': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/get-full/:id'
        },

        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        },

        'getById': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/safari-groups/:id'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        },
        'saveFull': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/safaris/save-full'
        },
        'getSafariGroups': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/safari-groups'
        }
      });
    }
})();
