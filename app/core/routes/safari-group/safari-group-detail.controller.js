(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("SafarigroupDetailController", SafarigroupDetailController);

  SafarigroupDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$state",
    "previousState",
    "group",
    "ObjectLabelsService",
    "ObjectFeaturesService",
    "URLS",
    "ProgrammesService",
    "SafariGroupsService",
    "$localStorage",
    "SafarisService",
    'ParseLinks',
    'pagingParams'
  ];

  function SafarigroupDetailController($scope, $rootScope, $stateParams, $state, previousState, group, ObjectLabelsService, ObjectFeaturesService, URLS, ProgrammesService, SafariGroupsService, $localStorage, SafarisService, ParseLinks, pagingParams) {
    var vm = this;

    vm.group = group;
    console.log(vm.group);

    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
    vm.previousState = previousState.name;
    vm.safariFilterParams = {
      "type": $stateParams.statePackageType,
      "year": "2019",
      "groupId": $stateParams.groupId,
      "country": $stateParams.country,
      "targetId": $stateParams.targetId,
      "targetName": $stateParams.targetName,
      "locationId": $stateParams.locationId,
      "locationName": $stateParams.locationName,
      "locationType": $stateParams.locationType
      //"locationType": $stateParams.locationType == "SET_DEPARTURE" || $stateParams.statePackageType == "EXTENSION" ? "REGION" : "C0UNTRY"
    }

    vm.search = search;
    vm.gotoLink = gotoLink;


    vm.foldersListView = true;
    vm.folderGridView = false;
    vm.switchDisplay = switchDisplay;

    vm.itemsPerPage = 20;
    vm.itemsPerPageStr = "20";
    vm.changeItemsPerPage = changeItemsPerPage;



    //initYears();
    initLinks();
    initChildrenFolders();
    //initItenerary();
    initSafaris();



    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 5; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function initLinks() {
      console.log(vm.group.configs);
      console.log(vm.group);
      vm.configs = angular.fromJson(vm.group.configs);
      console.log(vm.configs.navlinks)
      vm.links = [];
      if (!angular.equals(vm.configs, null)) {
        if (angular.isDefined(vm.configs.navlinks) && !angular.equals(vm.configs.navlinks, null)) {
          if (vm.configs.navlinks.length == 1) {
            console.log("1st folder..");
            //vm.links = configs.navlinks;
            vm.currentLink = 1;
          } else {
            console.log("other folder..");

            vm.currentLink = vm.configs.navlinks.length;
            console.log(vm.currentLink)
            angular.forEach(vm.configs.navlinks, function(navLink) {
              if (navLink.link != vm.currentLink) {
                vm.links.push(navLink);
              }
              if (angular.equals(navLink.link, "fit")) {
                vm.isFIT = true;
              }
            })
          }
        }
      }
    }

    function gotoLink(link) {
      console.log("going to linkk");
      console.log(link);
      $state.go("safari-group-detail", {
        groupId: link.value,
        year: $stateParams.year
      }, {
        reload: true
      });
    }

    function resolvePackageTypeName(type) {
      angular.forEach(vm.pakageTypes, function(pakageType) {
        if (type === pakageType.value) {
          vm.pakageName = pakageType.name;
        }
      });
      return vm.pakageName;
    }

    function initChildrenFolders() {
      //programmeDTOList
      if (!vm.group.terminal) {
        if (vm.group.length == undefined) {
          console.log("not yet resolved");
          SafariGroupsService
            .getByParentId({
              parentId: $stateParams.groupId,
              orgId: $localStorage.current_organisation.id
            })
            .$promise.then(function(response) {
              vm.safariGroups = response
            })
          console.log(vm.group.length);

        }
      } else {
        console.log(" terminal");
        vm.folderGridView = true;
        vm.foldersListView = false;
        // SafarisService.get().$promise.then(function(results){
        //       vm.safaris = results;
        // })

        //get safaris by group
        // SafarisService.getSafarisByGroupId({
        //   orgId: vm.group.orgId,
        //   groupId: vm.group.id
        // }).$promise.then(function(results) {
        //   vm.safaris = results;
        // });
        initSafaris();
      }
    }


    function search(name) {
      vm.searchedProgrammes = [];
      angular.forEach(vm.programmes, function(programme) {
        if (programme.reference.toLowerCase().includes(name.toLowerCase())) {
          vm.searchedProgrammes.push(programme);
        }
      });
    }

    function switchDisplay() {

      vm.folderGridView = !vm.folderGridView;
      vm.foldersListView = !vm.foldersListView;

    }

    function initSafaris(name) {
      //{id: $localStorage.current_organisation.id}


      SafarisService.getSafaris({
          orgId: vm.group.orgId,
          groupId: vm.group.id,
          name: name,
          page: pagingParams.page - 1,
          size: 1000
        },

        function(data, headers) {
          vm.linqs = ParseLinks.parse(headers('link'));
          vm.totalItems = headers('X-Total-Count');
          vm.queryCount = vm.totalItems;
          vm.safaris = data;
          vm.page = pagingParams.page;
          vm.numberOfPages = vm.queryCount / vm.itemsPerPage;
          vm.isLoading = false;
        },
        function(error) {
          console.log('error getting safaris');
          console.log(error);
          vm.isLoading = false;
        }

      );

    }

    function changeItemsPerPage(strValue) {

      console.log("converting to int: " + strValue);
      vm.itemsPerPage = parseInt(strValue, 10);
    }

  }
})();
