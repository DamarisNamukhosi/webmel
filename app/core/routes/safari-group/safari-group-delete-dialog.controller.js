(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('SafariGroupDeleteDialogController',SafariGroupDeleteDialogController);

    SafariGroupDeleteDialogController.$inject = ['$uibModalInstance', '$stateParams' ,'entity', 'SafariGroupsService','SafarisService'];

    function SafariGroupDeleteDialogController($uibModalInstance,$stateParams, entity, SafariGroupsService,SafarisService) {
        var vm = this;

        vm.group = entity;
        console.log(vm.group);
        console.log($stateParams);
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete () {
            vm.group.contentStatus = "DELETED";

            if(angular.equals($stateParams.safariId,null)|| angular.isUndefined($stateParams.safariId)){

                SafariGroupsService.update(vm.group,
                function () {
                    $uibModalInstance.close(true);
                });
            }else
            {
                console.log("deleting safari...");
                SafarisService.update(vm.group, function(){
                    $uibModalInstance.close(true);

                });
            }
        }
    }
})();
