(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('SafariGroupsController', SafariGroupsController);

  SafariGroupsController.$inject = [
    '$scope',
    '$location',
    '$state',
    '$localStorage',
    'SafariGroupsService',
    'URLS',
    'OrganizationsService',
    'safariGroups',
    '$stateParams',
    'LocationsService',
    'CurrenciesService',
    'ProgrammesService',
    '$timeout',
    'safariConstants'
  ];

  function SafariGroupsController($scope, $location, $state, $localStorage, SafariGroupsService, URLS, OrganizationsService, safariGroups, $stateParams, LocationsService, CurrenciesService, ProgrammesService, $timeout, safariConstants) {
    var vm = this;
    vm.safariGroups = safariGroups;
    console.log('safariGroups');
    console.log(vm.safariGroups);
    vm.account = null;
    vm.isAuthenticated = null;
    vm.resolvePackageTypeName = resolvePackageTypeName;
    vm.profile_url = URLS.PROFILE_IMAGE_URL;
    vm.packageName = '';
    vm.years = [];
    vm.currencies = [];
    vm.search = search;
    vm.safariFilterParams = {
      "type": $stateParams.statePackageType,
      "year": "2019",
      "groupId": $stateParams.groupId,
      "country": $stateParams.country,
      "targetId": $stateParams.targetId,
      "locationId": $stateParams.locationId,
      "locationType": $stateParams.statePackageType == "EXCURSION" || $stateParams.statePackageType == "EXTENSION" ? "REGION" : "C0UNTRY"
    }

    vm.foldersListView = true;
    vm.folderGridView = false;

    vm.type = $stateParams.statePackageType;
    vm.year = $stateParams.year;
    vm.packageTypes = safariConstants.packageTypes
    //$localStorage.safariFilterParams = vm.safariFilterParams;
    // programme stuff
    vm.programme = null;
    vm.clear = clear;
    vm.createProgramme = createProgramme;
    vm.parseTimeForSave = parseTimeForSave;

    vm.getLocation = getLocation;
    vm.onEndDestinationSelected = onEndDestinationSelected;
    vm.onstartDestinationSelected = onstartDestinationSelected;
    vm.initCurrencies = initCurrencies;
    initCurrencies();

    vm.switchDisplay = switchDisplay;

    //initialization
    initYears();


    initProgrammeObject();

    // switchDisplay();



    vm.noRecordsFound = false;
    if (vm.safariGroups.length === 0) {
      vm.noRecordsFound = true;
    }

    function resolvePackageTypeName(type) {
      angular
        .forEach(vm.packageTypes, function(packageType) {
          if (type === packageType.value) {
            vm.packageName = packageType.name;
          }
        });
      return vm.packageName;
    }

    function initYears() {
      var currentYear = new Date().getFullYear();
      var totalYears = 10; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm
          .years
          .push(year);
      }
    }

    function search(year, type) {

      $state.go($state.current, {
        statePackageType: type,
        year: year
      }, {
        reload: true
      });

    }


    // programme stuff
    function initProgrammeObject() {
      console.log(vm.safariGroups);
      if (vm.safariGroups.length == undefined) {
        SafariGroupsService
          .getFiltered({
            id: $localStorage.current_organisation.id,
            type: $stateParams.statePackageType,
            year: $stateParams.year
          })
          .$promise.then(function(response) {
            vm.safariGroups = response
          });
      }
      vm.programme = {
        contractId: null,
        contractName: null,
        contractType: "PUBLISHED",
        currencyId: null,
        currencyName: null,
        description: null,
        endLocationId: null,
        endLocationName: null,
        generalMarkUp: null,
        id: null,
        numberOfNights: null,
        numberOfDays: null,
        pakageId: null,
        pakageName: null,
        packageType: 'INDEPENDENT_TOUR',
        contentStatus: 'DRAFT',
        reference: null,
        startDay: null,
        startLocationId: null,
        startLocationName: null,
        uuid: null,
        setDimensionId: null,
        issuerId: $localStorage.current_organisation.id,
        year: null,
        targetId: null,
        serviceId: null
      };

      console.log('vm.programme');
      console.log(vm.programme);
      console.log('vm.safariGroups');
      console.log(vm.safariGroups);
    }

    vm.openFrom = function() {
      vm.openedFrom = true;
    };

    vm.openTo = function() {
      vm.openedTo = true;
    };


    function clear() {
      vm.programme = null;
    }

    function createProgramme() {
      console.log('programme');
      console.log(vm.programme);
      vm.programme.startDay = parseTimeForSave(vm.programme.startDay);
      //vm.entity.endDate = new Date(vm.entity.endDate);
      console.log("vm.d");
      console.log(vm.d);
      vm.isSaving = true;

      if (vm.programme.year === null) {
        console.log('vm.programme.startDay');
        // vm.programme.startDay
        var startDayStr = vm.programme.startDay.split("-");
        vm.programme.year = startDayStr[0];

        console.log('vm.programme.year');
        console.log(vm.programme.year);
      }

      if (vm.programme.id !== null) {
        // vm.contractParameters // we need to copy from here
        console.log('vm.programme.id');
        console.log(vm.programme.id);
        vm.programme.programmeParams = []; //reset the programmeParams

        angular.forEach(vm.contractParameters, function(parameter) {

          var param = {};
          param.id = parameter.id;
          param.type = parameter.type;
          param.parameter = parameter.parameter;
          param.value = parameter.value;
          param.exclusive = parameter.exclusive;
          param.notes = parameter.notes;
          param.uuid = parameter.uuid;
          param.contractId = parameter.contractId;

          vm.programme.contractParams.push(param);

        });

        //delete the parameters that is not needed
        delete vm.programme.rates;
        delete vm.programme.supplements;


        console.log("programme before update");
        console.log(vm.programme);
        ProgrammesService.updateFull(
          vm.programme,
          onSaveSuccess,
          onSaveError
        );
      } else {

        var year = vm.programme.year;
        // var market = resolveMarket(vm.programme.marketId);
        var service = resolveService(vm.programme.serviceId);
        var supplier = resolveSupplier(vm.programme.issuerId);
        // var target = resolveTarget(vm.programme.targetId);
        var type = vm.programme.pakageType;


        //set the default contract name
        vm.programme.name = year + " " + service + " " + type + " rates for " + supplier + " issued to " + " ";

        //prpepare contractParamDTOList
        vm.programme.contractParamDTOList = [];

        // vm.contractParameters // we need to copy from here
        angular.forEach(vm.contractParameters, function(parameter) {
          var param = {
            "id": null,
            "exclusive": parameter.exclusive,
            "contractId": null,
            "contractName": null,
            "notes": parameter.notes,
            "parameter": parameter.parameter,
            "type": "DIMENSION",
            "uuid": null,
            "value": parameter.value
          };
          vm.programme.contractParams.push(param);
        });

        // vm.contractParams // we need to copy from here
        angular.forEach(vm.contractParams, function(parameter) {
          if (parameter.value !== null) {
            var param = {
              "id": null,
              "exclusive": parameter.exclusive,
              "contractId": null,
              "contractName": null,
              "notes": parameter.notes,
              "parameter": parameter.parameter,
              "type": parameter.type,
              "uuid": null,
              "value": parameter.value
            };
            vm.programme.contractParams.push(param);
          }

        });
        console.log("programme before create");
        console.log(vm.programme);
        ProgrammesService.createFull(
          vm.programme,
          onSaveSuccess,
          onSaveError
        );
      }
    }

    function onSaveSuccess(result) {
      vm.isSaving = false;

      $("#fitModal").modal('hide');

      console.log("result");
      console.log(result);
      $state.go("programme-detail", {
        id: result.pakageId,
        progId: result.id
      }, {
        reload: true
      });

      // $state.go($state.current, {}, { reload: true});

      // $state.go("home", {}, { reload: true })
    }

    function onSaveError() {
      vm.isSaving = false;
    }


    function getLocation(val) {
      return LocationsService
        .query({
          name: val
        })
        .$promise
        .then(function(results) {
          return results.map(function(item) {
            return item;
          })
        });
    }


    function onEndDestinationSelected($item, $model) {
      vm.programme.endLocationId = $item.id;
      vm.programme.endLocationName = $item.name;
    }

    function onstartDestinationSelected($item, $model) {
      vm.programme.startLocationId = $item.id;
      vm.programme.startLocationName = $item.name;
    }

    function initCurrencies() {
      CurrenciesService.query().$promise.then(function(response) {
        angular.forEach(response, function(obj) {
          vm.currencies.push(obj);
        });
      });
    }

    function parseTimeForSave(stringTime) {
      var timed = new Date(stringTime.setDate(stringTime.getDate() + 1));
      var timed1 = new Date(timed.setDate(timed.getDate() + 1));
      var time = timed1.toISOString().split(":");
      console.log(timed);
      console.log(timed1);
      console.log(time);
      var nice = time[0].slice(0, 10).split("-");
      console.log(nice);
      vm.time = nice[0] + "-" + nice[1] + "-" + nice[2];
      console.log(vm.time);
      return vm.time;
    }

    function resolveService(id) {
      var value = '';
      if (!vm.inPropertyOrgType) {
        angular.forEach(vm.services, function(object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      } else {
        return vm.serviceName;
      }
    }

    function resolveSupplier(id) {
      var value = '';
      if (!vm.inPropertyOrgType) {
        angular.forEach(vm.suppliers, function(object) {
          if (object.id == id) {
            value = object.name;
          }
        });
        return value;
      } else {
        return $localStorage.current_organisation.name;
      }
    }


    function switchDisplay() {

      vm.folderGridView = !vm.folderGridView;
      vm.foldersListView = !vm.foldersListView;

    }

  }
})();
