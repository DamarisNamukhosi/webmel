(function () {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state("currencies", {
                parent: "app",
                url: "/currencies",
                data: {
                    authorities: []
                },
                views: {
                    "content@": {
                        templateUrl: "core/routes/currencies/currencies.html",
                        controller: "CurrenciesController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    currencies: [
                        "$stateParams",
                        "$localStorage",
                        "CurrencyService",
                        function ($stateParams, $localStorage, CurrencyService) {
                            //return CurrencyService.get().$promise;
                            return CurrencyService.getByOrganization({ id: $localStorage.current_organisation.id }).$promise;
                        }
                    ]
                }
            })
            .state('currencies.new', {
                parent: 'currencies',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: '',
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function ($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/currencies/currency-dialog.html',
                                controller: 'CurrencyDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function () {
                                        return {
                                            id: null,
                                            name: null,
                                            usage: "PRODUCTS",
                                            orgId: $localStorage.current_organisation.id,
                                            uuid: null,
                                            contentStatus: "DRAFT"
                                        };
                                    },
                                },
                            })
                            .result.then(
                                function () {
                                    $state.go('currencies', null, {
                                        reload: 'currencies',
                                    });
                                },
                                function () {
                                    $state.go('^');
                                },
                            );
                    },
                ],
            })
            .state("currencies-detail", {
                parent: "currencies",
                url: "/{groupId}/overview",
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: "webApp.markup-categories.detail.title"
                },
                views: {
                    "content@": {
                        templateUrl: "core/routes/currencies/currency-detail.html",
                        controller: "CurrencyDetailController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    entity: [
                        "$stateParams",
                        "CurrencyService",
                        function ($stateParams, CurrencyService) {
                            return CurrencyService.get({
                                id: $stateParams.groupId
                            }).$promise;
                        }
                    ],
                    conversions: [
                        "$stateParams",
                        "CurrencyService",
                        function ($stateParams, CurrencyService) {
                            return CurrencyService.getConversionByGroupId({ id: $stateParams.groupId }).$promise;
                        }
                    ],
                    previousState: [
                        "$state",
                        function ($state) {
                            var currentStateData = {
                                name: $state.current.name || "currencies",
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }
                    ]
                }
            })
            .state('currencies-detail.deleteConversion', {
                parent: 'currencies-detail',
                url: '/{conversionId}/deleteConversion',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function ($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl: 'core/routes/currencies/currency-delete-dialog.html',
                                controller: 'CurrencyDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        '$localStorage',
                                        'CurrencyService',
                                        function ($localStorage, CurrencyService) {
                                            // get the organization id from the localStorage
                                            // $localStorage.current_organisation.id
                                            return CurrencyService.getConversion({ id: $stateParams.conversionId }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result
                            .then(function () {
                                $state.go('currencies-detail', null, {
                                    reload: 'currencies-detail'
                                });
                            }, function () {
                                $state.go('^');
                            });
                    }
                ]
            })
            .state('currencies.delete', {
                parent: 'currencies',
                url: '/{groupId}/delete',
                data: {
                    requiresAuthentication: true,
                    authorities: []
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$uibModal',
                    function ($stateParams, $state, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl: 'core/routes/currencies/currency-delete-dialog.html',
                                controller: 'CurrencyDeleteDialogController',
                                controllerAs: 'vm',
                                size: 'md',
                                resolve: {
                                    entity: [
                                        '$localStorage',
                                        'CurrencyService',
                                        function ($localStorage, CurrencyService) {
                                            // get the organization id from the localStorage
                                            // $localStorage.current_organisation.id
                                            return CurrencyService.get({ id: $stateParams.groupId }).$promise;
                                        }
                                    ]
                                }
                            })
                            .result
                            .then(function () {
                                $state.go('currencies', null, {
                                    reload: 'currencies'
                                });
                            }, function () {
                                $state.go('^');
                            });
                    }
                ]
            })
            .state('currencies-detail.createConversion', {
                parent: 'currencies-detail',
                url: '/createConversion',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function ($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'core/routes/currencies/conversions/conversion-create-dialog.html',
                            controller: 'ConversionCreateDialogController',
                            controllerAs: 'vm',
                            backdrop: 'static',
                            size: 'md',
                            resolve: {
                                entity: [
                                    '$localStorage',
                                    'CurrencyService',
                                    function ($localStorage, CurrencyService) {
                                        return {
                                            contentStatus: "DRAFT",
                                            fromId: null,
                                            groupId: $stateParams.groupId,
                                            buyingRate: null,
                                            sellingRate: null,
                                            reciprocate: false,
                                            toId: null,
                                            parentId: null,
                                            parentRate: null,
                                            id: null,
                                            effectiveTime: null,
                                            startTime: null
                                        };
                                    }
                                ],
                                currencies: [
                                    'CurrenciesService',
                                    '$localStorage',
                                    function (CurrenciesService, $localStorage) {
                                        return CurrenciesService
                                            .query()
                                            .$promise;
                                    }
                                ]
                            }
                        })
                            .result
                            .then(function () {
                                $state.go('currencies-detail', null, {
                                    reload: 'currencies-detail'
                                });
                            }, function () {
                                $state.go('currencies-detail');
                            });
                    }
                ]
            })
            .state('currencies.edit', {
                parent: 'currencies',
                url: '/{groupId}/edit',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function ($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'core/routes/currencies/currency-dialog.html',
                            controller: 'CurrencyDialogController',
                            controllerAs: 'vm',
                            backdrop: 'static',
                            size: 'lg',
                            resolve: {
                                entity: [
                                    'CurrencyService',
                                    function (CurrencyService) {
                                        return CurrencyService
                                            .get({
                                                id: $stateParams.groupId
                                            })
                                            .$promise;
                                    }
                                ]
                            }
                        })
                            .result
                            .then(function () {
                                $state.go('currencies', null, {
                                    reload: 'currencies'
                                });
                            }, function () {
                                $state.go('currencies');
                            });
                    }
                ]
            })
            .state('currencies-detail.editConversion', {
                parent: 'currencies-detail',
                url: '/{conversionId}/editConversion',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: ''
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function ($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'core/routes/currencies/conversions/conversion-create-dialog.html',
                            controller: 'ConversionCreateDialogController',
                            controllerAs: 'vm',
                            backdrop: 'static',
                            size: 'lg',
                            resolve: {
                                entity: [
                                    'CurrencyService',
                                    function (CurrencyService) {
                                        return CurrencyService
                                            .getConversion({
                                                id: $stateParams.conversionId
                                            })
                                            .$promise;
                                    }
                                ],
                                currencies: [
                                    'CurrenciesService',
                                    '$localStorage',
                                    function (CurrenciesService, $localStorage) {
                                        return CurrenciesService
                                            .query()
                                            .$promise;
                                    }
                                ]
                            }
                        })
                            .result
                            .then(function () {
                                $state.go('currencies-detail', null, {
                                    reload: 'currencies-detail'
                                });
                            }, function () {
                                $state.go('currencies-detail');
                            });
                    }
                ]
            })


    }
})();
