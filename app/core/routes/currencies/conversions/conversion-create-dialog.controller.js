(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ConversionCreateDialogController', ConversionCreateDialogController);

  ConversionCreateDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$state',
    '$uibModalInstance',
    '$localStorage',
    'CurrencyService',
    'entity',
    'currencies'
  ];

  function ConversionCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $state,
    $uibModalInstance,
    $localStorage,
    CurrencyService,
    entity,
    currencies
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    console.log(vm.entity)
    vm.conversions = [];
    vm.addRules = false;
    var today = new Date();
    vm.currencies = currencies;
    vm.entity.startTime = new Date();
    vm.entity.startTime.setFullYear(vm.entity.startTime.getFullYear() + 1);
    vm.entity.effectiveTime = new Date();
    vm.entity.effectiveTime.setFullYear(vm.entity.effectiveTime.getFullYear() + 2);
    
    vm.openedFrom = false;
    vm.openedTo = false;

    //init function
    function initPolicies() {
      initOrganizationAgeLimits();
      if (vm.conversion.id !== null) {
        console.log("not new conversion..");
        // angular.forEach(vm.conversion, function(policy) {
        //     vm.conversions.push(policy);
        // });
        vm.entity.description = vm.conversion.description;
        vm.entity.id = vm.conversion.id;
        vm.entity.contentStatus = vm.conversion.contentStatus;
        vm.entity.allowed = vm.conversion.allowed;
        vm.entity.occupancyPolicyId = vm.conversion.occupancyPolicyId;
        vm.entity.occupantsPattern = vm.conversion.occupantsPattern;
        //vm.conversions.push(vm.conversion);
        if (vm.conversion.length == 1) {
          initPolicyRules(true);
        } else if (vm.conversion.length > 1) {
          initPolicyRules(true);
        }
      } else {
        console.log("new conversion..");
        vm.conversions.push({
          id: null,
          ageBracket: null,
          pattern: "[1-3]",
          baseRateId: null,
          baseRateName: null,
          rate: null,
          rateType: "PER_PERSON"
        });
      }
    }

    function addPolicyRules() {
      console.log("add policy");
      vm.addRules = true;
      vm.conversions.push(vm.entity);
      var temp = {
        contentStatus: "DRAFT",
        fromId: vm.entity.toId,
        groupId: $stateParams.groupId,
        rate: null,
        reciprocate: !vm.entity.reciprocate,
        toId: vm.entity.fromId,
        parentId:null,
        id:null     
  };

      vm.conversions.push(temp);
    };

    vm.removePolicy = function ($index) {
      vm.conversions.splice($index, 1);
    };

    function initOrganizationAgeLimits() {
      if (vm.orgAgeLimits == null) {
        $timeout(function () {
          AgeLimitsService.getAgeLimits({
            id: $localStorage.current_organisation.id
          }).$promise.then(function (response) {
            vm.orgAgeLimits = response;
            console.log("vm.orgAgeLimits");
            console.log(vm.orgAgeLimits);
          });
        }, 1000);
      }

    }

    function initPolicyRules(isEditing) {
      console.log(isEditing);
      if (vm.conversion.id == null) {
        console.log("new policies");
        if (vm.orgAgeLimits != null && vm.orgAgeLimits.length > 0) {
          angular.forEach(vm.orgAgeLimits, function (ageLimit) {
            if (ageLimit.generalAgeBracketName == "Infant") {
              vm.infantEntity = {
                "infantBaseRateId": null,
                "infantBaseRateName": "PPS",
                "infantPattern": null,
                "infantRate": null,
                "infantRateType": "PER_PERSON"
              };
            } else if (ageLimit.generalAgeBracketName == "Child") {
              vm.childEntity = {
                "childPattern": null,
                "childBaseRate": "PPS",
                "childRate": null,
                "childRateType": "PER_PERSON"
              }
            } else if (ageLimit.generalAgeBracketName == "Young Adult") {
              vm.youngAdultEntity = {
                "youngAdultBaseRateId": null,
                "youngAdultBaseRateName": "PPS",
                "youngAdultPattern": null,
                "youngAdultRate": null,
                "youngAdultRateType": "PER_PERSON"
              }
            } else if (ageLimit.generalAgeBracketName == "Adult") {
              vm.adultEntity = {
                "adultPattern": null,
                "adultBaseRate": "PPS",
                "adultRate": null,
                "adultRateType": "PER_PERSON"
              }
            }
          });
        }
      } else {
        console.log("editing a conversion");
        console.log("vm.orgAgeLimits");
        console.log(vm.orgAgeLimits);
        console.log(vm.conversion);
        if(vm.conversion.occupantsBaseRateId != null){
          console.log("init global..");
          vm.entity.global = true;
          console.log("vm.conversion");
          console.log(vm.conversion);
          //setIsGlobal();
          vm.conversions.push(vm.conversion);
          console.log(vm.conversions);

        }else{
        if (vm.orgAgeLimits.length == 0) {
          initOrganizationAgeLimits();
          console.log(vm.orgAgeLimits);

        }else if (vm.orgAgeLimits.length > 1) {
          if (vm.conversion.infantBaseRateId != null || vm.conversion.infantPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 5) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.infantPattern,
                  baseRateId: vm.conversion.infantBaseRateId,
                  rate: vm.conversion.infantRate,
                  rateType: vm.conversion.infantRateType
                };
                vm.conversions.push(temp)

              }

            });
          }
          if (vm.conversion.childBaseRateId != null || vm.conversion.childPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 1) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.childPattern,
                  baseRateId: vm.conversion.childBaseRateId,
                  baseRateName: vm.conversion.childBaseRateName,
                  rate: vm.conversion.childRate,
                  rateType: vm.conversion.childRateType
                };
                vm.conversions.push(temp)

              }

            });


          }
          if (vm.conversion.youngAdultBaseRateId != null || vm.conversion.youngAdultPattern != null) {

            console.log("here");
            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 3) {
                console.log("YA");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.youngAdultPattern,
                  baseRateId: vm.conversion.youngAdultBaseRateId,
                  baseRateName: vm.conversion.youngAdultBaseRateName,
                  rate: vm.conversion.youngAdultRate,
                  rateType: vm.conversion.youngAdultRateType
                };
                vm.conversions.push(temp);
              }
            });
          }
          if (vm.conversion.adultBaseRateId != null || vm.conversion.adultPattern != null) {
            // vm.conversions.ageBracket = 2;
            // vm.conversions.baseRate = vm.conversion.adultBaseRateId;
            // vm.conversions.pattern = vm.conversion.adultPattern;
            // vm.conversions.rate = vm.conversion.adultRate;
            // vm.conversions.rateType = vm.conversion.adultRateType;

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 2) {
                console.log("Adult");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.adultPattern,
                  baseRateId: vm.conversion.adultBaseRateId,
                  baseRateName: vm.conversion.adultBaseRateName,
                  rate: vm.conversion.adultRate,
                  rateType: vm.conversion.adultRateType
                };
                vm.conversions.push(temp);
              }
            });

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.agedPattern,
                  baseRateId: vm.conversion.agedBaseRateId,
                  baseRateName: vm.conversion.agedBaseRateName,
                  rate: vm.conversion.agedRate,
                  rateType: vm.conversion.agedRateType
                };
                vm.conversions.push(temp);
              }
            });
          }
          if (vm.conversion.agedBaseRateId != null || vm.conversion.agedPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                console.log("Adult");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.agedPattern,
                  baseRateId: vm.conversion.agedBaseRateId,
                  baseRateName: vm.conversion.agedBaseRateName,
                  rate: vm.conversion.agedRate,
                  rateType: vm.conversion.agedRateType
                };
                vm.conversions.push(temp);
              }
            });

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.conversion.agedPattern,
                  baseRateId: vm.conversion.agedBaseRateId,
                  baseRateName: vm.conversion.agedBaseRateName,
                  rate: vm.conversion.agedRate,
                  rateType: vm.conversion.agedRateType
                };
                vm.conversions.push(temp);
              }
            });
          }
        }
      }
        console.log("vm.conversions");
        console.log(vm.conversions);
      }
    }
    function setIsAllowed() {
      console.log("vm.entity.allowed");
      console.log(vm.entity.allowed);
      if (vm.entity.allowed) {
        console.log("allowed");
      } else {
        console.log("blocked");
      }
    }

    function setIsGlobal() {
      console.log("vm.entity.global");
      console.log(vm.entity.global);
      console.log(vm.conversions);
      if (vm.entity.global) {
        console.log("global");
        console.log(vm.conversions[0]);
        vm.conversions[0].occupantsBaseRateId = 7;
        vm.conversions[0].occupantsBaseRateName =  null;
        vm.conversions[0].occupantsRate= 1;
        vm.conversions[0].occupantsRateType = "PER_ROOM";
        console.log(vm.conversions[0]);
        setOccupantBaseRateName(vm.conversions[0], vm.conversions[0].occupantsBaseRateId);


      } else {
        console.log("not global");
        console.log(vm.conversions);
        vm.conversions[0].occupantsBaseRateId = null;
        vm.conversions[0].occupantsBaseRateName =  null;
        vm.conversions[0].occupantsRate= null;
        vm.conversions[0].occupantsRateType =null;
        console.log(vm.conversions[0]);


      }
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function checkPreviousState() {
      console.log($state)
      if ($state.current.parent == "settings-policies") {
        //vm.addRules();
        console.log("previous state");
        console.log($state.current.parent);
      }
    }

    function isDefaut() {
      vm.entity.default = !vm.entity.default;
      if (vm.entity.default) {
        vm.entity.name = $localStorage.current_organisation.name + " " + "Default Child Policy";
        console.log("vm.entity.name");
        console.log(vm.entity.name);
      }
    }

    function save() {
      console.log("saving...");
      console.log(vm.conversions);
      vm.isSaving = true;
      console.log(vm.entity.id != null)
      if (vm.entity.id != null) {
        console.log("ready to update");
        console.log(vm.entity);
        CurrencyService.updateConversion(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );

      } else {
        console.log("ready to save");
        console.log(vm.entity);
        CurrencyService.createConversions(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );

      }
    }
  
    function onSaveSuccess(result) {
      console.log(result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    vm.openFrom = function () {
      vm.openedFrom = true;
    };

    vm.openTo = function () {
      vm.openedTo = true;
    };

  }
})();
