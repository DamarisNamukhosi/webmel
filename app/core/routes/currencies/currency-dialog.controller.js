(function () {
    'use strict';
  
    angular
      .module('webApp')
      .controller('CurrencyDialogController', CurrencyDialogController);
  
    CurrencyDialogController.$inject = [
      '$timeout',
      '$scope',
      '$stateParams',
      '$state',
      '$uibModalInstance',
      '$localStorage',
      'CurrencyService',
      'entity'
    ];
  
    function CurrencyDialogController(
      $timeout,
      $scope,
      $stateParams,
      $state,
      $uibModalInstance,
      $localStorage,
      CurrencyService,
      entity
    ) {
      var vm = this;
  
      vm.clear = clear;
      vm.save = save;
      vm.entity  =entity;
      vm.usages = ["PRODUCTS", "OPERATIONS", "ALL"];
      //function declarion
      //initPolicies();
      //checkPreviousState();
      //initOrganizationAgeLimits();
  
      //function implementation
      function initPolicies() {
        console.log("checking context");
        if (vm.policies.id !== null) {
          vm.entity = vm.policies;
        } else {
          console.log("new");
          // vm.policyItems.push({
          //     adults: null,
          //     adultRate: null,
          //     adultRoomOccupancyId: null,
          //     children: null,
          //     childrenRoomOccupancyId: null,
          //     childrenRate: null,
          //     content_status: 'DRAFT',
          //     organisationId: $localStorage.current_organisation.id
          // });
  
          vm.entity = {
            "id": null,
            "name": null,
            "policyType": "ROOM_OCCUPANCY",
            "uuid": null,
            "isDefault": false,
            "year": "2019",
            "contentStatus": "DRAFT",
            "organisationId": $localStorage.current_organisation.id,
            "organisationName": null
          };
  
        }
      }
  
      function addPolicyRules() {
        vm.addRules = true;
        var temp = {
          adults: null,
          adultRate: null,
          adultRoomOccupancyId: null,
          children: null,
          childrenRoomOccupancyId: null,
          childrenRate: null,
          content_status: 'DRAFT',
          organisationId: $localStorage.current_organisation.id
        };
  
        vm.policyItems.push(temp);
      };
  
      vm.removePolicy = function ($index) {
        vm.policyItems.splice($index, 1);
      };
  
      function initOrganizationAgeLimits() {
        AgeLimitsService.getAgeLimits({
          id: $localStorage.current_organisation.id
        }).$promise.then(function (response) {
          vm.orgAgeLimits = response;
        });
  
      }
  
      function clear() {
        $uibModalInstance.dismiss('cancel');
      }
  
      function checkPreviousState() {
        if ($state.current.parent == "settings-policies") {
          //vm.addRules();
        }
      }
  
      function setIsDefaut() {
        if (vm.entity.isDefault) {
          vm.entity.name = $localStorage.current_organisation.name + " " + "Default Child Policy";
  
        } else {
          vm.entity.name = null;
        }
      }
  
      function save() {
        vm.isSaving = true;
        if (vm.entity.id !== null) {
          CurrencyService.update(
            vm.entity,
            onSaveSuccess,
            onSaveError
          );
        } else {
          CurrencyService.create(
            vm.entity,
            onSaveSuccess,
            onSaveError
          );
        }
      }
  
      function onSaveSuccess(result) {
        $uibModalInstance.close(result);
        vm.isSaving = false;
      }
  
      function onSaveError() {
        vm.isSaving = false;
      }
    }
  })();
  