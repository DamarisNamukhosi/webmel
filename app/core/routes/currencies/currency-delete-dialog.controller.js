(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'CurrencyDeleteDialogController',
            CurrencyDeleteDialogController
        );

    CurrencyDeleteDialogController.$inject = [
        '$stateParams',
        '$uibModalInstance',
        '$state',
        'CurrencyService',
        'entity'
    ];

    function CurrencyDeleteDialogController(
        $stateParams,
        $uibModalInstance,
        $state,
        CurrencyService,
        entity
    ) {
        var vm = this;
        vm.entity =entity;
        console.log(vm.entity)
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        //name: "settings-policies.deleteRules"
        vm.isConversionGroup = false;
        vm.isConversion=  false;

        identifyIntity();

        function identifyIntity(){
            if($state.current.name == "currencies-detail.deleteConversion"){
            
                vm.isConversion=  true;
            }else if($state.current.name == "currencies.delete"){
                vm.isConversionGroup = true;
            }
        }

        
        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        var policyId = $stateParams.id;

        function confirmDelete() {
            console.log('ID: ' + policyId);
            if(vm.isConversionGroup ){
            vm.entity.contentStatus = "DELETED";
            CurrencyService.update( vm.entity, function() {
                $uibModalInstance.close(true);
            });
        }else if(vm.isConversion){
            vm.entity.contentStatus = "DELETED";
            CurrencyService.updateConversion(vm.entity, function() {
                $uibModalInstance.close(true);
            });
        }
        }
    }
})();
