(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('CurrenciesController', CurrenciesController);

    CurrenciesController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        'CurrencyService',
        'currencies'
    ];

    function CurrenciesController(
        $scope,
        $location,
        $state,
        $localStorage,
        CurrencyService,
        currencies
        ) {
        var vm = this;

        vm.currencies = currencies;
        vm.parent= [];
        vm.rules= [];

     
        vm.editPolicy = editPolicy;
        vm.currentOrganisation =$localStorage.current_organisation;

        //init functions
        getAllRules();
        //getParent();



        function getAllRules(){
            CurrencyService.getConversions().$promise.then(function (response){
                vm.conversions= response;
            })
        }
     
        vm.addPolicyRow = function(policyId) {

            //$state.go('settings-currencies.edit', {id: vm.currencies.id}, {reload: true});
            $state.go('settings-currencies.createRules', {occupancyPolicyId: policyId}, {reload: true});

        };
        //settings-currencies.edit
         function editPolicy(policyId) {

            //$state.go('settings-currencies.edit', {id: vm.currencies.id}, {reload: true});
            $state.go('settings-currencies.edit', {id: policyId}, {reload: true});

        };
    }
})();
