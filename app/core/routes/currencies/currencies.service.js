(function() {
    'use strict';
    angular.module('webApp').factory('CurrencyService', CurrencyService);

    CurrencyService.$inject = ['$resource', '$localStorage', 'URLS'];

    function CurrencyService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(
            resourceUrl,
            {},
            {
                get: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversion-groups/:id'
                },
                getByOrganization: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversion-groups/filter-by-organisation/:id'
                },
                update: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/currency-conversion-groups'
                },
                delete: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversion-groups/:id'
                },
                create: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'costingservice/api/currency-conversion-groups'
                },
                createList: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversion-groups/create-list'
                },
                updateList: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversion-groups/update-list'
                },
                createConversions: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions'
                },
                copyConversions: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions/copy'
                },
                updateConversion: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions'
                },
                getConversions: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions'
                },

                getConversion: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions/:id'
                },
                getConversionByGroupId: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions/filter-by-group/:id'
                },
                deleteConversion: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'costingservice/api/currency-conversions/:id'
                }
                //getRules
                //POST /api/currency-conversions updateRules
                //POST /api/currency-conversion-groups/copy
            }
        );
    }
})();
