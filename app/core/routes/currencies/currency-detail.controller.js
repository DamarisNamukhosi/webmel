(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('CurrencyDetailController', CurrencyDetailController);

    CurrencyDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', '$localStorage', 'URLS', "conversions"];

    function CurrencyDetailController($scope, $rootScope, $stateParams, previousState, entity,  $localStorage, URLS, conversions) {
        var vm = this;

        vm.conversionGroup = entity;

        vm.conversions = conversions;


        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.conversionGroup.uuid;

        vm.previousState = previousState.name;

    }
})();
