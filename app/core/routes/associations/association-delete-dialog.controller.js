(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AssociationDeleteDialogController',AssociationDeleteDialogController);

        AssociationDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'AssociationsService'];

    function AssociationDeleteDialogController($uibModalInstance, entity, AssociationsService) {
        var vm = this;

        vm.association = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AssociationsService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
