(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AssociationDetailController', AssociationDetailController);

        AssociationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity'];

    function AssociationDetailController($scope, $rootScope, $stateParams,previousState, entity) {
        var vm = this;
        vm.association = entity;
        vm.previousState = previousState.name;
    }
})();
