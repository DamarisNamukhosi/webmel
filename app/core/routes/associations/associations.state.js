(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('associations', {
            parent: 'app',
            url: '/associations',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/associations/associations.html',
                    controller: 'AssociationsController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
              associations: ['$stateParams', '$localStorage','AssociationsService', function ($stateParams,$localStorage, AssociationsService) {
                return AssociationsService.getAssociations({id : $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('association-detail', {
            parent: 'associations',
            url: '/association/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'webApp.associations.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/associations/association-detail.html',
                    controller: 'AssociationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'AssociationsService', function($stateParams, AssociationsService) {
                    return AssociationsService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'associations',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('associations.new', {
            parent: 'app',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'documents-own.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/associations/association-dialog.html',
                    controller: 'AssociationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                parentId: $localStorage.current_organisation.id,
                                parentName : null,
                                childId : null,
                                childName : null,
                                typeId : null,
                                typeName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('associations', null, { reload: 'associations' });
                }, function() {
                    $state.go('associations');
                });
            }]
        })   
        .state('associations.edit', {
            parent: 'associations',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/associations/association-dialog.html',
                    controller: 'AssociationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AssociationsService', function(AssociationsService) {
                            return AssociationsService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('associations', null, { reload: 'associations' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('associations.delete', {
            parent: 'associations',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/associations/association-delete-dialog.html',
                    controller: 'AssociationDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AssociationsService', function(AssociationsService) {
                            return AssociationsService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('associations', null, { reload: 'associations' });
                }, function() {
                    $state.go('^');
                });
            }]
        });

    }
})();
