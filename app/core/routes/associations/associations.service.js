(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('AssociationsService', AssociationsService);

        AssociationsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function AssociationsService ($resource, $localStorage, URLS) {
        var resourceUrl =  '';

        return $resource(resourceUrl, {}, {
          'getAssociations': {
            method: 'GET',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user,
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/organisation-relationships/filter-by-child/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            transformResponse: function (data) {
                if (data) {
                    data = angular.fromJson(data);
                }
                return data;
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/organisation-relationships/:id'
          },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/organisation-relationships'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/organisation-relationships'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/organisation-relationships/:id'
        }
      });
    }
})();
