
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AssociationDialogController', AssociationDialogController);

    AssociationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance','$localStorage', 'entity', 'AssociationsService'];

    function AssociationDialogController ($timeout, $scope, $stateParams, $uibModalInstance,$localStorage, entity, AssociationsService) {
        var vm = this;

        vm.association = entity;
        
        console.log(vm.association);

        console.log(vm.association.childName);

        console.log(vm.association.id);

        console.log(vm.association);

        console.log(vm.association.typeName);
        
        console.log('type');
        console.log(entity.typeName);

        vm.clear = clear;
        vm.save = save;
        //vm.associationTypes = RelationshipTypesService.query();
        //vm.organizationTypes = OrganizationsService.getOrganizations();
        
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.association.id !== null) {
                AssociationsService.update(vm.association, onSaveSuccess, onSaveError);
            } else {
                AssociationsService.create(vm.association, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
