(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('MenuCategoriesDialogController', MenuCategoriesDialogController);

  MenuCategoriesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'MenuCategoriesService'];

  function MenuCategoriesDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, MenuCategoriesService) {
    var vm = this;

    vm.menuCategory = entity;
    vm.clear = clear;
    vm.save = save;
    vm.initOrgMealCategories = initOrgMealCategories;
    console.log('menu-categories....');
    console.log(vm.menuCategory);


    vm.organizationMenuCategory = [];
    //vm.menuCategories = MenuCategoriesService.getMenuCategories();
    // vm.generalMenuCategories = GeneralMenuCategoriesService.query();
    initOrgMealCategories();
    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      vm.isSaving = true;



      // if (vm.menuCategory.order == null) {
      //   console.log("nulll...");;
      //   if (vm.organizationMenuCategory.length > 0) {
      //     var i = 1;
      //     angular.forEach(vm.organizationMenuCategory, function(mealCat) {
      //         if (vm.menuCategory.id = mealCat.id) {
      //           if (mealCat.order == null) {
      //             mealCat.order = i * 100;
      //             i = i + 1;
      //           }
      //         }
      //       }
      //     });
      // } else {
      //   console.log("not null");
      //   vm.menuCategory.order = 100;
      // }
      if (vm.menuCategory.id !== null) {
        MenuCategoriesService.update(vm.menuCategory, onSaveSuccess, onSaveError);
      } else {
        MenuCategoriesService.create(vm.menuCategory, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }



    function initOrgMealCategories() {
      MenuCategoriesService.getMenuCategory().$promise.then(function(response) {
        vm.organizationMenuCategory = response;
      })
    }


  }
})();
