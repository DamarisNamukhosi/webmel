(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuCategoriesDetailController', MenuCategoriesDetailController);

        MenuCategoriesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MenuCategoriesService'];

    function MenuCategoriesDetailController($scope, $rootScope, $stateParams, previousState, entity, MenuCategoriesService) {
        var vm = this;

        vm.menuCategory = entity;
        vm.previousState = previousState.name;
    }
})();
