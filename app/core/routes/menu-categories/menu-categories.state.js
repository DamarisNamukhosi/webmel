(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('menu-categories', {
            parent: 'app',
            url: '/menu-categories',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'menu-categories'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/menu-categories/menu-categories.html',
                    controller: 'MenuCategoriesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                menuCategories: ['$stateParams', '$localStorage', 'MenuCategoriesService', function ($stateParams, $localStorage, MenuCategoriesService) {
                return MenuCategoriesService.getMenuCategories({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('menu-categories.new', {
            parent: 'menu-categories',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'profsApp.professional.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/menu-categories/menu-categories-dialog.html',
                    controller: 'MenuCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                description : null,
                                generalMenuCategoryId : null,
                                generalMenuCategoryName : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('menu-categories', null, { reload: 'menu-categories' });
                }, function() {
                    $state.go('menu-categories');
                });
            }]
        })
        .state('menu-categories-detail', {
            parent: 'app',
            url: '/menu-categories/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Menu Categories Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/menu-categories/menu-categories-detail.html',
                    controller: 'MenuCategoriesDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'MenuCategoriesService', function($stateParams, MenuCategoriesService) {
                    return MenuCategoriesService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'menu-categories',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('menu-categories.edit', {
            parent: 'menu-categories',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/menu-categories/menu-categories-dialog.html',
                    controller: 'MenuCategoriesDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MenuCategoriesService', function(MenuCategoriesService) {
                            return MenuCategoriesService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('menu-categories', null, { reload: 'menu-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('menu-categories.delete', {
            parent: 'menu-categories',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/menu-categories/menu-categories-delete-dialog.html',
                    controller: 'MenuCategoriesDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MenuCategoriesService', function(MenuCategoriesService) {
                            return MenuCategoriesService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('menu-categories', null, { reload: 'menu-categories' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
