(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuCategoriesDeleteDialogController',MenuCategoriesDeleteDialogController);

        MenuCategoriesDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'MenuCategoriesService'];

    function MenuCategoriesDeleteDialogController($uibModalInstance, entity, MenuCategoriesService) {
        var vm = this;

        vm.menuCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MenuCategoriesService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
