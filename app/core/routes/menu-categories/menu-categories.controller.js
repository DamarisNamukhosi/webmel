(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MenuCategoriesController', MenuCategoriesController);

        MenuCategoriesController.$inject = ['$scope', '$location', '$state', 'records', '$localStorage', 'URLS', 'MenuCategoriesService'];

    function MenuCategoriesController($scope, $location, $state, records, $localStorage, URLS, MenuCategoriesService) {
        var vm = this;
        vm.records = records;
        console.log("records");
        console.log(vm.records);
        vm.account = $localStorage.current_organisation;
        console.log($localStorage.current_organisation);
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        console.log(vm.profile_image_url);
        vm.isAuthenticated = null;

      vm.sortableOptions = {
        stop: function (e, ui) {
          vm.dragAlert = false;
          updateRank();
        }
      }

      function updateRank() {
        console.log("Updating Rank");
        var count = 1;

        angular.forEach(vm.records, function (record) {

          record.order = count * 100;

          count = count + 1;
        });

        console.log("vm.records");
        console.log(vm.records);


        // call update
        MenuCategoriesService.updateList(vm.records);

      }

    }
})();
