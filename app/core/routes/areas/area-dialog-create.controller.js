(function() {
  'use strict';

  angular
      .module('webApp')
      .controller('AreaDialogCreateController', AreaDialogCreateController);

      AreaDialogCreateController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'LocationGroupsService', 'LocationTypesService', 'URLS','LocationsService'];

  function AreaDialogCreateController ($timeout, $scope, $stateParams, $uibModalInstance, entity, LocationGroupsService, LocationTypesService, URLS,LocationsService) {
      var vm = this;

      vm.location = entity;
      vm.clear = clear;
      vm.save = save;
      vm.locationTypeChange = locationTypeChange;
      vm.getLocation=getLocation;
      //vm.getOrganization = getOrganization;
      vm.onLocationSelected= onLocationSelected;
      //vm.onLocationManagerSelected = onLocationManagerSelected;
      vm.tempParentLocations=[];
      vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
      vm.newLocation = {
          id: null,
          name: null,
      }

      //initDialogModels();
      // initLocCoordinates();
      function initDialogModels(){

          $timeout(function (){
             // angular.element('.form-group:eq(1)>input').focus();
             LocationGroupsService.get({id: $stateParams.areaId}).$promise.then(function(response){
              vm.location = response;
              console.log(vm.location);
           },3000);
           console.log(vm.location);

          });
    

      }
      // vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E';

   

      function clear () {
          $uibModalInstance.dismiss('cancel');
      }

      function save () {
          console.log(vm.location);
          vm.isSaving = true;
          if (vm.location.id !== null) {
              console.log(vm.location);
             
              console.log(vm.location);
            //   vm.location.locations.push(temp);
            //   console.log(vm.location);

              //console.log(temp);
              LocationGroupsService.update(vm.location, onSaveSuccess, onSaveError);
          } else {

              LocationGroupsService.create(vm.location, onSaveSuccess, onSaveError);
          }
      }

      function onSaveSuccess (result) {
          console.log("result");
          console.log(result);
          //$scope.$emit('gatewayApp:locationUpdate', result);
          $uibModalInstance.close(result);
          vm.isSaving = false;
      }

      function onSaveError () {
          vm.isSaving = false;
      }

      function locationTypeChange(id){
          vm.tempParentLocations= vm.locations;
          console.log('changed location Type id');
          
         

      }


          function getLocation(val) {
              vm.searchedProperty = false;
                return LocationsService
                  .query({
                    name: val,
                    types: [1,2,3,4,5,6,7,8,9,16,17,18,19]
                  })
                  .$promise
                  .then(function (results) {
                    vm.searchedItems = results;
                    return results.map(function (item) {
                     
                      return item;
                    })
                  });
              }

          function onLocationSelected($item, $model) {
            
              vm.newLocation.id=$item.id;
            
          }
  }
})();
