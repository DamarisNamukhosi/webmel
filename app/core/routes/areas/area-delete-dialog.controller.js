
(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AreaDeleteDialogController',AreaDeleteDialogController);

        AreaDeleteDialogController.$inject = ['$uibModalInstance', 'entity', '$localStorage',  '$stateParams', 'LocationGroupsService'];

    function AreaDeleteDialogController($uibModalInstance, entity, $localStorage, $stateParams, LocationGroupsService) {
        var vm = this;

        vm.locationGroup = entity;
        vm.location = [];
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        vm.updateGroup = false;
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        inits();


        function inits(){
            if($stateParams.locId != null){
                vm.updateGroup = true;
            console.log($stateParams.id);
            angular.forEach(vm.locationGroup.locations, function(location){
                if(location.id == $stateParams.locId){
                    vm.location = location;
                }
                console.log(vm.location);

            });
        }
        }
        function confirmDelete (location) {
            if($stateParams.locId != null){
            console.log(vm.location);
            angular.forEach(vm.locationGroup.locations, function(loc){
                if(vm.location.id == loc.id){
                    vm.locationGroup.locations.splice(vm.locationGroup.locations.indexOf(loc),1);
                }
            });
            LocationGroupsService.update(vm.locationGroup,
                function () {
                    $uibModalInstance.close(true);
                });
        }else{
          //var organizationId = $localStorage.current_organisation.id;
          console.log("Current Org ID: " + vm.locationGroup.id);
          console.log("remove loc" + location.id);
          location.contentStatus = "DELETED";
            console.log(location);
            LocationGroupsService.update(vm.locationGroup,
                function () {
                    $uibModalInstance.close(true);
                });
            }
        }
    }
})();
