(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('LocationGroupsService', LocationGroupsService);

    LocationGroupsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function LocationGroupsService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups'
            },
            'generalFilter': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups'
            },

            'getbasesPageable': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/pageable'
            },
            'getbases': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups'
            },
            'getFullbase': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/get-full/:id'
            },
            'getbasesByOrganizationId': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/filter-by-organisation/:id?sort=order%2Casc'
            },
            'getbasesByParentId': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/filter-by-parent-base/:id'
            },
            'getbasesByParentIdAndType': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/filter-by-parent-base/:id'
            },
            'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray:false,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/location-groups'
            },
            'updateList': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/location-groups/save-list'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/location-groups'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/location-groups/:id'
            },
            'deleteOwn': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/location-groups/delete/:organisationId/:baseId'
            }
        });
    }
})();
