(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('AreasController', AreasController);

  AreasController.$inject = ['$scope', '$location', '$state', '$localStorage', 'ParseLinks','pagingParams','paginationConstants','URLS', '$stateParams', "LocationGroupsService"
  ];

  function AreasController($scope, $location, $state,$localStorage,ParseLinks,pagingParams,paginationConstants,URLS, $stateParams,LocationGroupsService) {
    var vm = this;

    vm.account = null;
    vm.isAuthenticated = null;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL;
    vm.areas = [];
    vm.whenFiltering = false;
    vm.genAreas = ["BASE","AREAS"];
    vm.areas = [];
    //declare functions
    vm.loadPage = loadPage;
    vm.transition = transition;
    //pagination init
    vm.predicate = pagingParams.predicate;
    vm.reverse = pagingParams.ascending;
    vm.itemsPerPage = paginationConstants.itemsPerPage;
    vm.areaTypeChange = areaTypeChange;
    //initialization

   initAreas();



   vm.sortableOptions = {
    stop: function (e, ui) {
      vm.dragAlert = false;
      updateRank();
    }
  }



  function updateRank() {
    console.log("Updating Rank");
    var count = 1;

    angular.forEach(vm.areas, function (record) {

      record.order = count * 100;

      count = count + 1;
    });

    console.log("vm.records");
    console.log(vm.areas);


    // call update
    LocationGroupsService.updateList(vm.areas, onSaveSuccess, onSaveError);

  }

  function onSaveSuccess (result) {
    //$scope.$emit('gatewayApp:locationUpdate', result);
   console.log("update sucessful");
}

function onSaveError () {
  console.log("update not sucessful");
}
    function initAreas(){
      vm.previousState = $state.current.parent;
    
      // areas: [
      //   '$stateParams',
      //   '$localStorage'
      //   'LocationGroupsService',
      //   function ($stateParams, $localStorage, LocationGroupsService) {
      //     return LocationGroupsService.getbasesByOrganizationId({id: $localStorage.current_organisation.id}).$promise;
      //   }
      // ]

    LocationGroupsService.getbasesByOrganizationId({id: $localStorage.current_organisation.id}).$promise.then(function (response){
      vm.locations = response;
      angular.forEach(response, function(res){
        vm.areas.push(res);
      })
      console.log(vm.genAreas);
    });
  }
  function initLocs(data){
    angular.forEach(data, function(loc){
      vm.areas.push(loc);

    });

  }
    function loadPage(page) {
      vm.page = page;
      vm.transition();
    }

    function transition() {
      $state.transitionTo($state.$current, {
        countryId: $stateParams.countryId,
        page: vm.page,
        sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
        search: vm.currentSearch
      });
    }

    vm.activate = function (area) {

      if (area.contentStatus == 'PUBLISHED') {
        area.contentStatus = "UNPUBLISHED"
      } else {
        area.contentStatus = "PUBLISHED"
      }

      console.log(area);
      LocationGroupsService.update(area, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(response) {
      $state.reload();
    }

    function onSaveError(error) {
      console.log(error);
      alert(error);
    }


    function areaTypeChange(id,val)
    {
      vm.whenFiltering = true;
      vm.areas= [];
      console.log(val);
        vm.searchedProperty = false;
          return LocationGroupsService
            .query({
              name: val,
              types: [id]
            })
            .$promise
            .then(function (results) {
              angular.forEach(results, function(r){
                if(r.parentId == $stateParams.countryId){
                  vm.areas.push(r);
                }
              })

              console.log(vm.areas);;
              return results.map(function (item) {

                return item;
              })
            });
        }


  }
})();
