(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AreaDialogController', AreaDialogController);

        AreaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'setParentVisible', 'LocationGroupsService', 'LocationTypesService', 'URLS','LocationsService'];

    function AreaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, setParentVisible, LocationGroupsService, LocationTypesService, URLS,LocationsService) {
        var vm = this;

        vm.location = [];
        vm.clear = clear;
        vm.save = save;
        vm.locationTypeChange = locationTypeChange;
        vm.getLocation=getLocation;
        //vm.getOrganization = getOrganization;
        vm.onLocationSelected= onLocationSelected;
        //vm.onLocationManagerSelected = onLocationManagerSelected;
        vm.tempParentLocations=[];
        vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
        vm.newLocation = {
            id: null,
            name: null,
        }

        initDialogModels();
        // initLocCoordinates();
        function initDialogModels(){

            $timeout(function (){
               // angular.element('.form-group:eq(1)>input').focus();
               LocationGroupsService.get({id: $stateParams.areaId}).$promise.then(function(response){
                vm.location = response;
                console.log(vm.location);
             },3000);
             console.log(vm.location);

            });
      
           

            // if(vm.location.id !== null){
            //     locationTypeChange(vm.location.typeId);
            // }
            // vm.tempParentLocations= vm.locations;

        }
        // vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E';

     

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            console.log(vm.location);
            vm.isSaving = true;
            if (vm.location.id !== null) {
                console.log(vm.location);
                // var temp = {
                //     abbreviation: vm.location.abbreviation,
                //     brief : vm.location.brief,
                //     contentStatus :vm.location.contentStatus,
                //     coordinates : [{
                //         altitude: vm.location.coordinates[0].altitude,
                //         id:  vm.location.coordinates[0].id,
                //         latitude: vm.latlng[0],
                //         locationId:vm.location.coordinates[0].locationId,
                //         locationName: vm.location.coordinates[0].locationName,
                //         longitude : vm.latlng[1],
                //         name: vm.location.coordinates[0].name,
                //         type: vm.location.coordinates[0].type,
                //         uuid: vm.location.coordinates[0].uuid
                //     }],
                //     description: vm.location.description,
                //     id: vm.location.id,
                //     latitude : vm.location.latitude,
                //     longitude: vm.location.longitude,
                //     managerId : vm.location.managerId,
                //     managerName: vm.location.managerName,
                //     name : vm.location.name,
                //     parentId : vm.location.parentId,
                //     parentName : vm.location.parentName,
                //     statusReason : vm.location.statusReason,
                //     typeId: vm.location.typeId,
                //     typeName: vm.location.typeName,
                //     uuid: vm.location.uuid
                // };
                var temp = {
                    "id": vm.newLocation.id,
                    "name": vm.newLocation.name
                }
                console.log(vm.location);
                vm.location.locations.push(temp);
                console.log(vm.location);

                //console.log(temp);
                LocationGroupsService.update(vm.location, onSaveSuccess, onSaveError);
            } else {
                console.log("creating..")
               // LocationGroupsService.create(vm.location, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function locationTypeChange(id){
            vm.tempParentLocations= vm.locations;
            console.log('changed location Type id');
            
            // angular.forEach(vm.locations, function(location){
            //     if((location.typeId < id || (id ==  10 || id ==11) && (location.typeId == 12 || location.typeId == 13) ) && !((location.typeId == 10 || location.typeId == 11) && (id ==  12 || id ==13))){
            //         vm.tempParentLocations.push(location);
            //     }
            // })            

        }


            function getLocation(val) {
                vm.searchedProperty = false;
                  return LocationsService
                    .query({
                      name: val,
                      types: [1,2,3,4,5,6,7,8,9,16,17,18,19]
                    })
                    .$promise
                    .then(function (results) {
                      vm.searchedItems = results;
                      return results.map(function (item) {
                       
                        return item;
                      })
                    });
                }
  
            function onLocationSelected($item, $model) {
              
                vm.newLocation.id=$item.id;
              
            }
            // function onLocationManagerSelected($item, $model) {
            //   console.log($item);
            //     vm.newLocation.id=$item.id;
                
            //   }
    }
})();
