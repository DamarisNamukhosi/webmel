(function () {
  'use strict';

  angular.module('webApp').config(stateConfig);

  stateConfig.$inject = ['$stateProvider'];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('areas', {
        parent: 'app',
        url: '/areas',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/areas.html',
            controller: 'AreasController',
            controllerAs: 'vm'
          }
        },
        resolve: {

        },
        params: {
          page: {
            value: '1',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state('areas-areas', {
        parent: 'areas',
        url: '/{countryId}/countries',
        data: {
          requiresAuthentication: true,
          authorities: ['MANAGER_USER', 'D_AUTHORITY_USER'],
          pageTitle: 'areas'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/areas.html',
            controller: 'AreasController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          areas: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              // type id 11 is properties
              return LocationGroupsService.query({
                types: 7

              }).$promise;
            }
          ]
        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },
        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],
        }
      })
      .state('areas-areas.new', {
        parent: 'areas',
        url: '/{areaId}/newBase',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog.html',
                controller: 'AreaDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: $stateParams.parentId,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('areas', null, {
                    reload: 'areas'
                  });
                },
                function () {
                  $state.go('areas');
                }
              );
          }
        ]
      })
      .state('areas.new', {
        parent: 'areas',
        url: '/newGroup',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog-create.html',
                controller: 'AreaDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: function () {
                    return {
                      id: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      name: null,
                      order: null,
                      notes: null,
                      contentStatus: "DRAFT",
                      typeId: "19",
                      typeName: "REGION",
                      parentLocationId: null,
                      organisationId: $localStorage
                        .current_organisation.id
                    }
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('areas', null, {
                    reload: 'areas'
                  });
                },
                function () {
                  $state.go('areas');
                }
              );
          }
        ]
      })
      .state('areas-own.new', {
        parent: 'areas-own',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog-create.html',
                controller: 'AreaDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function () {
                    return {
                      abbreviation: null,
                      altitude: null,
                      brief: null,
                      description: null,
                      latitude: null,
                      areaTypeId: null,
                      longitude: null,
                      name: null,
                      organisationId: $localStorage
                        .current_organisation.id,
                      parentId: null
                    };
                  },
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('areas-own', null, {
                    reload: 'areas-own'
                  });
                },
                function () {
                  $state.go('areas-own');
                }
              );
          }
        ]
      })
      .state('areas-own', {
        parent: 'areas',
        url: '/own',
        data: {
          requiresAuthentication: true,
          authorities: ['D_AUTHORITY_USER', 'MANAGE_areaS', 'MANAGER_USER'],
          pageTitle: 'areas'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/areas-own.html',
            controller: 'areasOwnController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          areas: [
            '$stateParams',
            '$localStorage',
            'LocationGroupsService',
            function (
              $stateParams,
              $localStorage,
              LocationGroupsService
            ) {
              return LocationGroupsService.getareasByOrganizationId({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state('area-galleries', {
        parent: 'areas-own',
        url: '/{areaId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-galleries.html',
            controller: 'areaGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'areas-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('areas-areas.galleries', {
        parent: 'areas-areas',
        url: '/{areaId}/gallery',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-galleries.html',
            controller: 'areaGalleriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.get({
                id: $stateParams.areaId
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'areas-areas',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('area-galleries.new', {
        parent: 'areas-areas.galleries',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-create-dialog.html',
                controller: 'AlbumCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.areaId
                      }).$promise;
                    }
                  ],
                  album: function () {
                    return {
                      albumType: 'GENERAL',
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //area uuid
                    };
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('areas-areas.galleries', null, {
                    reload: 'areas-areas.galleries'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('area-galleries.delete', {
        parent: 'area-galleries',
        url: '/{albumId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-delete-dialog.html',
                controller: 'AlbumDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('area-galleries', null, {
                    reload: 'area-galleries'
                  });
                },
                function () {
                  $state.go('area-galleries');
                }
              );
          }
        ]
      })
      .state('area-album-detail', {
        parent: 'area-galleries',
        url: '/{albumId}/album',
        data: {
          requiresAuthentication: true,
          authorities: [
            'MASTER_USER',
            'D_AUTHORITY_USER',
            'MANAGE_areaS'
          ],
          pageTitle: 'Gallery'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-album-detail.html',
            controller: 'areaAlbumDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          album: [
            '$stateParams',
            'AlbumService',
            function ($stateParams, AlbumService) {
              return AlbumService.getAlbum({
                albumId: $stateParams.albumId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'areas-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('area-album-detail.edit', {
        parent: 'area-album-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-dialog.html',
                controller: 'AlbumDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'AlbumService',
                    function (AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('area-album-detail', null, {
                    reload: 'area-album-detail'
                  });
                },
                function () {
                  $state.go('area-album-detail');
                }
              );
          }
        ]
      })
      .state('area-album-detail.upload', {
        parent: 'area-album-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-upload-dialog.html',
                controller: 'AlbumUploadDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    '$stateParams',
                    'AlbumService',
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('area-album-detail', null, {
                    reload: 'area-album-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('area-album-detail.makeCoverImage', {
        parent: 'area-album-detail',
        url: '/{imageId}/cover-image',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-change-cover-image-dialog.html',
                controller: 'AlbumChangeCoverImageDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('area-album-detail', null, {
                    reload: 'area-album-detail'
                  });
                },
                function () {
                  $state.go('area-album-detail');
                }
              );
          }
        ]
      })
      .state('area-album-detail.deleteImage', {
        parent: 'area-album-detail',
        url: '/{imageId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'areas-own.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/albums/album-image-delete-dialog.html',
                controller: 'AlbumImageDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'AlbumService',
                    '$stateParams',
                    function (AlbumService, $stateParams) {
                      return AlbumService.get({
                        id: $stateParams.albumId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('area-album-detail', null, {
                    reload: 'area-album-detail'
                  });
                },
                function () {
                  $state.go('area-album-detail');
                }
              );
          }
        ]
      })
      .state('area-detail', {
        parent: 'areas',
        url: '/{id}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'area Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-detail.html',
            controller: 'AreaDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.getFullarea({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'areas-own',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('areas-areas.overview', {
        parent: 'areas-areas',
        url: '/{areaId}/{areaType}/overview',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'area Overview'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-detail.html',
            controller: 'areaDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.getFullarea({
                id: $stateParams.areaId
              })
                .$promise;
            }
          ],
          coordinates: [
            '$stateParams',
            'CoordinatesService',
            function ($stateParams, CoordinatesService) {
              return CoordinatesService.getCoordinatesByarea({
                id: $stateParams.areaId
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state, $) {
              var currentStateData = {
                name: $state.current.name || 'areas-areas',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('area-detail.edit', {
        parent: 'area-detail',
        url: '/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog.html',
                controller: 'AreaDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.getFullarea({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('area-detail', null, {
                    reload: 'area-detail'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('area-detail.addLabels', {
        parent: 'areas-areas.overview',
        url: '/{uuid}/{objectType}/add-labels',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-create-dialog.html',
                controller: 'ObjectLabelGroupCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  labelGroups: [
                    'LabelGroupsService',
                    function (LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.areaId
                      }).$promise;
                    }
                  ],
                  savedselectedLabels: [
                    'ObjectLabelsService',
                    function (ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('areas-areas.overview', null, {
                    reload: 'areas-areas.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('area-detail.deleteLabel', {
        parent: 'areas-areas.overview',
        url: '/{itemToDelete}/delete-label',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/labels/object-label-delete-dialog.html',
                controller: 'ObjectLabelGroupDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
              })
              .result.then(
                function () {
                  $state.go('areas-areas.overview', null, {
                    reload: 'areas-areas.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      .state('areas-areas.detail', {
        parent: 'areas-areas',
        url: '/{areaId}/{areaType}/detail',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'area Attractions'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/properties/area-properties.html',
            controller: 'areaAreasController',
            controllerAs: 'vm'
          }
        },

        resolve: {
          pagingParams: [
            '$stateParams',
            'PaginationUtil',
            function ($stateParams, PaginationUtil) {
              return {
                page: PaginationUtil.parsePage(
                  $stateParams.page
                ),
                sort: $stateParams.sort,
                predicate: PaginationUtil.parsePredicate(
                  $stateParams.sort
                ),
                ascending: PaginationUtil.parseAscending(
                  $stateParams.sort
                ),
                search: $stateParams.search
              };
            }
          ],

        },
        params: {
          page: {
            value: '0',
            squash: true
          },
          sort: {
            value: 'id,desc',
            squash: true
          },
          search: null
        },

        resolve: {

        }
      })
      .state('area-areas', {
        parent: 'areas',
        url: '/{id}/areas',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'area areas'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/areas/area-areas.html',
            controller: 'areaAreasController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.get({
                id: $stateParams.id
              })
                .$promise;
            }
          ],
          areas: [
            '$stateParams',
            'LocationGroupsService',
            function ($stateParams, LocationGroupsService) {
              return LocationGroupsService.getareasByParentId({
                id: $stateParams.id
              }).$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'areas',
                params: $state.params,
                url: $state.href(
                  $state.current.name,
                  $state.params
                )
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('areas.edit', {
        parent: 'areas',
        url: '/{areaId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog-create.html',
                controller: 'AreaDialogCreateController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.areaId
                      }).$promise;
                    }
                  ],
                  setParentVisible: function () {
                    return true;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go('areas', null, {
                    reload: 'areas'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })

      .state('areas.delete', {
        parent: 'areas',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-delete-dialog.html',
                controller: 'AreaDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('areas', null, {
                    reload: 'areas'
                  });
                },
                function () {
                  $state.go('areas');
                }
              );
          }
        ]
      })
      .state('area-detail.delete', {
        parent: 'areas-areas.overview',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-delete-dialog.html',
                controller: 'areaDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('areas-areas.overview', null, {
                    reload: 'areas-areas.overview'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })

      .state('areas-areas.delete', {
        parent: 'areas',
        url: '/{areaId}/{locId}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-delete-dialog.html',
                controller: 'AreaDeleteDialogController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                  entity: [
                    'LocationGroupsService',
                    function (LocationGroupsService) {
                      return LocationGroupsService.get({
                        id: $stateParams.areaId
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function () {
                  $state.go('areas', null, {
                    reload: 'areas'
                  });
                },
                function () {
                  $state.go('^');
                }
              );
          }
        ]
      })
      
      ;
  }
})();
