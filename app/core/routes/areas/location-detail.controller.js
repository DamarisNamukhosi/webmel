(function () {
  "use strict";

  angular
    .module("webApp")
    .controller("LocationDetailController", LocationDetailController);

  LocationDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "$state",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "LocationsService",
    "NgMap",
    "URLS"
  ]; //

  function LocationDetailController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    $state,
    previousState,
    entity,
    ObjectLabelsService,
    LocationsService,
    NgMap,
    URLS
  ) {
    var vm = this;

    vm.location = entity;
    
    
     console.log(vm.location);
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.location.uuid });
    console.log(vm.objectLabels);
    vm.coordinates = vm.location.coordinates;
    console.log(vm.coordinates[0]);
    $localStorage.current_location = vm.location;
    $localStorage.current_location_coordinates = vm.coordinates[0];
    vm.previousState = previousState.name;
    vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.location.uuid;
    var permissions = $localStorage.current_user_permissions;
    vm.childLocations = [];
    // NgMap.getMap().then(function(map) {
    //   vm.map = map;
    // });

    LocationsService.getLocationsByParentId({ id: vm.location.id }).$promise.then(function (response) {
      vm.childLocations = response;
    });

    console.log("Child Locations");
    console.log(vm.childLocations);

    vm.getpos = function (event,child) {
      console.log("event");
      console.log(event);
      console.log("id");
      console.log(child);
      map.showInfoWindow('bar', child.id);

      vm.latlng = [event.latLng.lat(), event.latLng.lng()];
      vm.lat = event.latLng.lat();
      vm.long = event.latLng.lng();
      vm.zoomLevel = 10;
    };


    /**
     * This hack checks whether the logged in user is a MASTER_USER
     * or not then allocates the appropriate sref for the
     * locations-view
     */

    vm.previousState =
      permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";
    
      function initCountry() {
        console.log("init");
        console.log($rootScope.stateObject.locationType);
        console.log(vm.location.typeId);
        console.log($rootScope.stateObject.locationType != vm.location.typeId);

        if($rootScope.stateObject.locationType != vm.location.typeId){
          //$rootScope.stateObject.locationType = vm.location.typeId;
         // $state.reload();
          $state.go($state.current, {
              locationId : vm.location.id,
              locationType: vm.location.typeId
            }, {reload: true}); 
        }
        

        console.log($stateParams.countryId > 0);
        console.log($stateParams.countryId);
        console.log($stateParams);
        console.log($stateParams.locationId);
        vm.showCountry = true;
        if ($stateParams.countryId > 0) {
          LocationsService.get({
              id: $stateParams.countryId
            })
            .$promise.then(function (results) {
              vm.country = results;
            });
        }
      }
    
    }

 
})();
