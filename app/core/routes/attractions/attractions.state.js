(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("attractions", {
        parent: "app",
        url: "/attractions",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "attractions"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/attractions/attractions.html",
            controller: "AttractionController",
            controllerAs: "vm"
          }
        },
        resolve: {
          attractions: [
            "$stateParams",
            "$localStorage",
            "AttractionsService",
            function($stateParams, $localStorage, AttractionsService) {
              return AttractionsService.getAttractions({
                id: $localStorage.current_location_id
              }).$promise;
            }
          ]
        }
      })
      .state("attractions.new", {
        parent: "attractions",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "attractions.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/attractions/attraction-dialog.html",
                controller: "AttractionDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      order: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      locationId: $localStorage.current_location_id,
                      locationName: $localStorage.current_organisation.name,
                      generalAttractionId: null,
                      generalAttractionName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("attractions", null, { reload: "attractions" });
                },
                function() {
                  $state.go("attractions");
                }
              );
          }
        ]
      })
      .state("attraction-detail", {
        parent: "app",
        url: "/attractions/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "attractions Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/attractions/attraction-detail.html",
            controller: "AttractionDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "AttractionsService",
            function($stateParams, AttractionsService) {
              return AttractionsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          location: [
            "$stateParams",
            "LocationsService",
            function($stateParams, LocationsService) {
              return LocationsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "attractions",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("attraction-detail.edit", {
        parent: "attraction-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/attractions/attraction-dialog.html",
                controller: "AttractionDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AttractionsService",
                    function(AttractionsService) {
                      return AttractionsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-detail", null, {
                    reload: "attraction-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attractions.edit", {
        parent: "attractions",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/attractions/attraction-dialog.html",
                controller: "AttractionDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AttractionsService",
                    function(AttractionsService) {
                      return AttractionsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attractions", null, { reload: "attractions" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attraction-detail.delete", {
        parent: "attraction-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/attractions/attraction-delete-dialog.html",
                controller: "AttractionDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "AttractionsService",
                    function(AttractionsService) {
                      return AttractionsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-detail", null, {
                    reload: "attraction-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attraction-galleries", {
        parent: "app",
        url: "/attractions/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS","ORGANISATION_ADMIN"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/attractions/attraction-galleries.html",
            controller: "AttractionGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "AttractionsService",
            function($stateParams, AttractionsService) {
              return AttractionsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("attraction-galleries.new", {
        parent: "attraction-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AttractionsService",
                    function(AttractionsService) {
                      return AttractionsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-galleries", null, {
                    reload: "attraction-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attraction-galleries.delete", {
        parent: "attraction-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("attraction-galleries", null, {
                    reload: "attraction-galleries"
                  });
                },
                function() {
                  $state.go("attraction-galleries");
                }
              );
          }
        ]
      })
      .state("attraction-album-detail", {
        parent: "attraction-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/attractions/attraction-album-detail.html",
            controller: "AttractionAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "AttractionsService",
            function($stateParams, AttractionsService) {
              return AttractionsService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "attraction-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("attraction-album-detail.edit", {
        parent: "attraction-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "attraction-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService",function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("attraction-album-detail", null, { reload: "attraction-album-detail" });
              },
              function () {
                $state.go("attraction-album-detail");
              }
              );
          }
        ]
      })
      .state("attraction-album-detail.makeCoverImage", {
        parent: "attraction-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-album-detail", null, {
                    reload: "attraction-album-detail"
                  });
                },
                function() {
                  $state.go("attraction-album-detail");
                }
              );
          }
        ]
      })
      .state("attraction-album-detail.deleteImage", {
        parent: "attraction-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-album-detail", null, {
                    reload: "attraction-album-detail"
                  });
                },
                function() {
                  $state.go("attraction-album-detail");
                }
              );
          }
        ]
      })
      .state("attraction-album-detail.upload", {
        parent: "attraction-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-album-detail", null, {
                    reload: "attraction-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attraction-detail.addLabels", {
        parent: "attraction-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "AttractionsService",
                    function(AttractionsService) {
                      return AttractionsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("attraction-detail", null, {
                    reload: "attraction-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("attraction-detail.deleteLabel", {
        parent: "attraction-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("attraction-detail", null, {
                    reload: "attraction-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
  }
})();
