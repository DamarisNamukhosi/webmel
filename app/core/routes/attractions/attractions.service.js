(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('AttractionsService', AttractionsService);

    AttractionsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function AttractionsService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getAttractions': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/attractions/filter-by-location/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/attractions/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/attractions'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/attractions'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/attractions/:id'
          }
        });
    }
})();
