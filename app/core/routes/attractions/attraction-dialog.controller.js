(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AttractionDialogController', AttractionDialogController);

        AttractionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AttractionsService', 'GeneralAttractionsService'];

    function AttractionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AttractionsService, GeneralAttractionsService) {
        var vm = this;

        vm.attraction = entity;
        vm.clear = clear;
        vm.save = save;

        vm.attractionTypes = GeneralAttractionsService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.attraction.id !== null) {
                AttractionsService.update(vm.attraction, onSaveSuccess, onSaveError);
            } else {
                AttractionsService.create(vm.attraction, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
