(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("AttractionDetailController", AttractionDetailController);

  AttractionDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "ActivityService",
    "location"
  ];

  function AttractionDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    ObjectLabelsService,
    ActivityService,
    location
  ) {
    var vm = this;

    vm.attraction = entity;
    console.log(vm.attraction);
    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.attraction.uuid });
    console.log(vm.objectLabels);
    vm.previousState = previousState.name;
    vm.location = location;
  }
})();
