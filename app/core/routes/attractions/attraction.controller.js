(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AttractionController', AttractionController);

        AttractionController.$inject = ['$scope', '$location', '$state', 'attractions'];

    function AttractionController ($scope, $location, $state, attractions) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.attractions = attractions;

    }
})();
