(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('AttractionDeleteDialogController',AttractionDeleteDialogController);

    AttractionDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'AttractionsService'];

    function AttractionDeleteDialogController($uibModalInstance, entity, AttractionsService) {
        var vm = this;

        vm.attraction = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function confirmDelete (attraction) {
            
            attraction.contentStatus = "DELETED";
            AttractionsService.update(attraction,
                  function () {
                      $uibModalInstance.close(true);
                  });
          }
    }
})();
