(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MarkupCategoriesDetailController', MarkupCategoriesDetailController);

    MarkupCategoriesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', '$localStorage', 'ObjectLabelsService', 'URLS', "serviceMarkups"];

    function MarkupCategoriesDetailController($scope, $rootScope, $stateParams, previousState, entity,  $localStorage, ObjectLabelsService, URLS, serviceMarkups) {
        var vm = this;

        vm.markupCategories = entity;

        vm.serviceMarkups = serviceMarkups;

        vm.previousState = previousState.name;

        vm.markupCategoriesNotFound = (vm.markupCategories === undefined || vm.markupCategories.length === 0);
        vm.markupCategoriesItemsNotFound = (vm.markupCategoriesItems === undefined || vm.markupCategoriesItems.length === 0);


        function changeItemsPerPage(strValue) {

          console.log("converting to int: " + strValue);
          vm.itemsPerPage = parseInt(strValue, 10);
        }
    }
})();
