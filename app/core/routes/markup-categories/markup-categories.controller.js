(function() {
  "use strict";

  angular.module("webApp").controller("MarkupCategoriesController", MarkupCategoriesController);

  MarkupCategoriesController.$inject = ["$scope", "entity", "$state", "$localStorage", "URLS", "MarkupCategoriesService"];

  function MarkupCategoriesController($scope, entity, $state, $localStorage, URLS, MarkupCategoriesService ) {
    var vm = this;

    vm.account = $localStorage.current_organisation;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;

    vm.entity = entity;
    console.log(vm.entity);
  }


})();
