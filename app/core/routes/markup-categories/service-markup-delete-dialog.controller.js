(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "ServiceMarkupDeleteDialogController",
      ServiceMarkupDeleteDialogController
    );

  ServiceMarkupDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "MarkupCategoriesService",
    "entity"

  ];

  function ServiceMarkupDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    MarkupCategoriesService,
    entity
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clickedItem = $stateParams.id;
    console.log("Id to delete: " + vm.clickedItem);

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    vm.save = function save() {
      vm.isSaving = true;
      vm.entity.contentStatus = 'DELETED'
      MarkupCategoriesService.updateServiceMarkUp(vm.entity, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
