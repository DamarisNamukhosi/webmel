(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('markup-categories', {
        parent: 'app',
        url: '/markup-categories/markup-categories',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'MarkupCategories Settings'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/markup-categories/markup-categories.html',
            controller: 'MarkupCategoriesController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'MarkupCategoriesService',
            '$localStorage',
            function($stateParams, MarkupCategoriesService, $localStorage) {
              return MarkupCategoriesService
                .getByOrganisation({id: $localStorage.current_organisation.id})
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function($state) {
              var currentStateData = {
                name: $state.current.name || 'markup-categories',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('markup-categories.new', {
        parent: 'markup-categories',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markup-categories/markup-categories-create-dialog.html',
                controller: 'MarkupCategoriesCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    function($localStorage) {
                      return {
                        contentStatus: 'DRAFT',
                        defaultMarkup: 10,
                        id: null,
                        isDefault: true,
                        name: null,
                        orgId: $localStorage.current_organisation.id,
                        orgName: $localStorage.current_organisation.name,
                        uuid: null
                      };
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('markup-categories', null, {
                  reload: 'markup-categories'
                });
              }, function() {
                $state.go('markup-categories');
              });
          }
        ]
      })
      .state('markup-categories.edit', {
        parent: 'markup-categories',
        url: '/{markupCategoriesId}/editMarkupCategories',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal.open({
              templateUrl: 'core/routes/markup-categories/markup-categories-create-dialog.html',
              controller: 'MarkupCategoriesCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'MarkupCategoriesService',
                    function(MarkupCategoriesService) {
                      console.log($stateParams);
                      console.log($state.params);
                      console.log($state);
                      return MarkupCategoriesService
                        .getById({
                          id: $stateParams.markupCategoriesId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('markup-categories', null, {
                  reload: 'markup-categories'
                });
              }, function() {
                $state.go('markup-categories');
              });
          }
        ]
      })
      .state('markup-categories.delete', {
        parent: 'markup-categories',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markup-categories/markup-categories-delete-dialog.html',
                controller: 'MarkupCategoriesDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'MarkupCategoriesService',
                    function($localStorage, MarkupCategoriesService) {
                      return MarkupCategoriesService
                        .getById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('markup-categories', null, {
                  reload: 'markup-categories'
                });
              }, function() {
                $state.go('^');
              });
          }
        ]
      })
      .state('markup-categories-detail.addServiceMarkup', {
        parent: 'markup-categories-detail',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/markup-categories/service-markup-dialog.html',
                controller: 'ServiceMarkupDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: {
                    contentStatus: 'DRAFT',
                    categoryId: $stateParams.catId,
                    generalServiceId: null,
                    generalServiceName: null ,
                    inverseCalculation: true,
                    markup: null
                  },
                  generalServices: [
                      '$localStorage',
                      'GeneralServicesService',
                      function ( $localStorage, GeneralServicesService) {
                        return GeneralServicesService
                          .query()
                      }
                    ]
                }
              })
              .result
              .then(function() {
                $state.go('markup-categories-detail', null, {
                  reload: 'markup-categories-detail'
                });
              }, function() {
                $state.go('markup-categories-detail');
              });
          }
        ]
      })
      .state("markup-categories-detail", {
        parent: "markup-categories",
        url: "/{catId}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.markup-categories.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/markup-categories/markup-categories-detail.html",
            controller: "MarkupCategoriesDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "MarkupCategoriesService",
            function($stateParams, MarkupCategoriesService) {
              return MarkupCategoriesService.getById({
                id: $stateParams.catId
              }).$promise;
            }
          ],
          serviceMarkups: [
            "$stateParams",
            "MarkupCategoriesService",
            function($stateParams, MarkupCategoriesService) {
              return MarkupCategoriesService.getServiceMarkups({id : $stateParams.catId}).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "markup-categories",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("markup-categories-detail.edit", {
        parent: "markup-categories-detail",
        url: "/{serviceId}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/markup-categories/service-markup-dialog.html",
                controller: "ServiceMarkupDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "MarkupCategoriesService",
                    function(MarkupCategoriesService) {
                      return MarkupCategoriesService.getByServiceMarkUpId({
                        id: $stateParams.serviceId
                      }).$promise;
                    }
                  ],
                  generalServices: [
                      '$localStorage',
                      'GeneralServicesService',
                      function ( $localStorage, GeneralServicesService) {
                        return GeneralServicesService
                          .query()
                      }
                    ],
                  editParentMarkupCategories: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("markup-categories-detail", null, {
                    reload: "markup-categories-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("markup-categories-detail.delete", {
        parent: "markup-categories-detail",
        url: "/{serciveMarkupId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/markup-categories/service-markup-delete-dialog.html",
                controller: "ServiceMarkupDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "MarkupCategoriesService",
                    function(MarkupCategoriesService) {
                      return MarkupCategoriesService.getByServiceMarkUpId({id : $stateParams.serciveMarkupId}).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("markup-categories-detail", null, {
                    reload: "markup-categories-detail"
                  });
                },
                function() {
                  $state.go("markup-categories-detail");
                }
              );
          }
        ]
      });
  }
})();
