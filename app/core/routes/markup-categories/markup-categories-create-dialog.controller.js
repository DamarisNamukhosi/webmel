(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("MarkupCategoriesCreateDialogController", MarkupCategoriesCreateDialogController);

  MarkupCategoriesCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "MarkupCategoriesService"

  ];

  function MarkupCategoriesCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    MarkupCategoriesService

  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;



      function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;

      console.log(vm.entity);
      if (vm.entity.id == null) {
        console.log("vm.entity");
        console.log(vm.entity);
        MarkupCategoriesService.createFull(vm.entity, onSaveSuccess, onSaveError);
      }
      else {
                MarkupCategoriesService.update(vm.entity, onSaveSuccess, onSaveError);

      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }


    // vm.addCountry = function () {
    //   vm.initialSet.push({
    //
    //     countryId: null,
    //     countryCode: null,
    //     countryName: null,
    //     id: null,
    //     residence: "CITIZEN"
    //   });
    // };

    // vm.removeCountry = function ($index) {
    //   // angular.forEach(vm.initialSet, function (set) {
    //   //   if (set.number === dimen.number) {
    //   //     vm.initialSet.splice(set, 1);
    //   //   }
    //   // })
    //   vm.initialSet.splice($index, 1);
    //
    // };

    vm.sortableOptions = {
      "ui-floating": true,
      update: function (e, ui) {
        //  console.log(ui);
      }
    };

    vm.changed = function (index) {

    }
  }
})();
