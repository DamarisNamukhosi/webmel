(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("ServiceMarkupDialogController", ServiceMarkupDialogController);

  ServiceMarkupDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "generalServices",
    "MarkupCategoriesService"
  ];

  function ServiceMarkupDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    generalServices,
    MarkupCategoriesService

  ) {
    var vm = this;

    vm.isSaving = false;
    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;
    vm.generalServices= generalServices;


    console.log(vm.entity);

    vm.index = 0;
    vm.serviceChange = serviceChange;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;

      console.log(vm.entity);
      if (vm.entity.id !== null) {
        console.log("upDATE...");

                MarkupCategoriesService.updateServiceMarkUp(
                  vm.entity,
                  onSaveSuccess,
                  onSaveError
                );

      }
      else {
        console.log("create...");
        MarkupCategoriesService.create(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      }
    }

    function onSaveSuccess(result) {
      console.log("done");
      vm.isSaving = false;
      $uibModalInstance.close(result);
    }

    function onSaveError() {
      vm.isSaving = false;
    }




    // vm.addServiceMarkup = function() {
    //   vm.selectedList.push({
    //
    //     countryId: null,
    //     countryCode: null,
    //     countryName: null,
    //     id: null,
    //     residence: "CITIZEN"
    //   });
    // };

    // vm.removeServiceMarkup = function($index) {
    //   // angular.forEach(vm.selectedList, function (set) {
    //   //   if (set.number === dimen.number) {
    //   //     vm.selectedList.splice(set, 1);
    //   //   }
    //   // })
    //   vm.selectedList.splice($index, 1);
    //
    // };

    vm.sortableOptions = {
      "ui-floating": true,
      update: function(e, ui) {
        //  console.log(ui);
      }
    };

    function serviceChange(serviceId){
      console.log("service change");
      console.log(serviceId);
      angular.forEach(vm.generalServices, function(genSvc){
        if(serviceId == genSvc.id){
          vm.entity.generalServiceName = genSvc.name;
        }
      })

    }
  }
})();
