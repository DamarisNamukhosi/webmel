(function() {
  "use strict";
  angular.module("webApp").factory("MarkupCategoriesService", MarkupCategoriesService);

  MarkupCategoriesService.$inject = ["$resource", "$localStorage", "URLS"];

  function MarkupCategoriesService($resource, $localStorage, URLS) {
    var resourceUrl = "";

    return $resource(
      resourceUrl, {}, {
        getServiceMarkups: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL +
            "costingservice/api/service-markups/filter-by-category/:id"
        },
        createFull: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/markup-categories/"
        },
        getByOrganisation: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          //url: URLS.BASE_URL + "costingservice/api/markup-categories/"
           url: URLS.BASE_URL + "costingservice/api/markup-categories/filter-by-organisation/:id"
        },
        getById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "costingservice/api/markup-categories/:id"
        },
        getFullById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "costingservice/api/markup-categories/get-full/:id"
        },
        get: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "costingservice/api/markup-categories/"
        },
        update: {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/markup-categories"
        },
        create: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/markup-categories"
        },
        delete: {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/markup-categories/:id"
        },

        // service-markup
        create: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/service-markups"
        },
        deleteServiceMarkup: {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/service-markups/:id"
        },

        getByServiceMarkUpId: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "costingservice/api/service-markups/:id"
        },
        updateServiceMarkUp: {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/service-markups"
        },
      }
    );
  }
})();
