(function() {
  "use strict";

  angular.module("webApp").controller("MarketsController", MarketsController);

  MarketsController.$inject = ["$scope", "entity", "$state", "$localStorage", "URLS"];

  function MarketsController($scope, entity, $state, $localStorage, URLS) {
    var vm = this;

    vm.account = $localStorage.current_organisation;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;

    vm.entity = entity;
    console.log(vm.entity);
  }


})();
