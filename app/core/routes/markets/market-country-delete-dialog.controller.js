(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "MarketCountryDeleteDialogController",
      MarketCountryDeleteDialogController
    );

  MarketCountryDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "MarketsCountriesService"
  ];

  function MarketCountryDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    MarketsCountriesService
  ) {
    var vm = this;

    vm.clickedItem = $stateParams.id;
    console.log("Id to delete: " + vm.clickedItem);

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    vm.save = function save() {
      vm.isSaving = true;
      MarketsCountriesService.delete({id: vm.clickedItem}, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
