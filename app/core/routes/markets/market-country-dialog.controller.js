(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("MarketCountryDialogController", MarketCountryDialogController);

  MarketCountryDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "MarketsCountriesService",
    "countries"
  ];

  function MarketCountryDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    MarketsCountriesService,
    countries
  ) {
    var vm = this;

    vm.isSaving = false;
    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;

    vm.countries = countries;

    vm.countries1 = [];

    vm.setCountryTypes = ["CITIZEN", "RESIDENT", "ANY"];


    vm.selectedList = [
      {
        countryId: null,
        countryCode: null,
        countryName: null,
        id: null,
        residence: "CITIZEN"
      }
    ];
    //remove countries already on the market
    vm.setMarkets = vm.entity.marketCountryDTOList; //array

    console.log(vm.entity);

    vm.index = 0;

    angular.forEach(vm.countries, function(country) {
      var countryId = country.id;
      vm.index = 0;
      angular.forEach(vm.setMarkets, function(setMarket) {
        var marketCountryId = setMarket.countryId;
        if (countryId == marketCountryId) {
          console.log("removing " + vm.index);
          console.log(country);
          vm.countries.splice(vm.index, 1);
        }
        vm.index++;
      });
    });
    //console.log(vm.countries);

    vm.countries1 = vm.countries;

    vm.marketId = $stateParams.id;

    vm.marketCountryList = [];

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      console.log("vm.selectedList");
      console.log(vm.selectedList);
      angular.forEach(vm.selectedList, function(item) {
        var temp = {
          countryCode: item.countryCode,
          countryId: item.countryId,
          countryName: item.countryName,
          marketId: vm.marketId,
          residence: item.residence
        };

        vm.marketCountryList.push(temp);
      });
      console.log(vm.marketCountryList);
      console.log(vm.entity);
      if (vm.entity.id !== null) {
        MarketsCountriesService.createFull(
          vm.marketCountryList,
          onSaveSuccess,
          onSaveError
        );
      }
    }

    function onSaveSuccess(result) {
      console.log("done");
      vm.isSaving = false;
      $uibModalInstance.close(result);
    }

    function onSaveError() {
      vm.isSaving = false;
    }




    vm.addCountry = function () {
      vm.selectedList.push({
        
        countryId: null,
        countryCode: null,
        countryName: null,
        id: null,
        residence: "CITIZEN"
      });
    }; 
    
    vm.removeCountry = function ($index) {
      // angular.forEach(vm.selectedList, function (set) {
      //   if (set.number === dimen.number) {
      //     vm.selectedList.splice(set, 1);
      //   }
      // })
      vm.selectedList.splice($index, 1);

    };

    vm.sortableOptions = {
      "ui-floating": true,
      update: function (e, ui) {
        //  console.log(ui);
      }
    };
  }
})();
