(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("MarketDialogController", MarketDialogController);

  MarketDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "MarketsService"
  ];

  function MarketDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    MarketsService
  ) {
    var vm = this;

    vm.entity = entity;
    console.log(vm.entity);
    vm.clear = clear;
    vm.save = save;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      if (vm.entity.id !== null) {
        console.log(vm.entity);
        MarketsService.update(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
