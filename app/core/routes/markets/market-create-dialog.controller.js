(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("MarketCreateDialogController", MarketCreateDialogController);

  MarketCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "MarketsService",
    "countries"
  ];

  function MarketCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    MarketsService,
    countries
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;

    vm.countries = countries;

    console.log(vm.countries);

    vm.entity.marketCountryDTOList = [];


    vm.setCountryTypes = ["CITIZEN", "RESIDENT", "ANY"];


    vm.initialSet = [
      {
        countryId: null,
        countryCode: null,
        countryName: null,
        id: null,
        residence: "CITIZEN"
      }
    ];


    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      console.log("vm.initialSet");
      console.log(vm.initialSet);
      angular.forEach(vm.initialSet, function(item) {
        var temp = {
          countryCode: item.abbreviation,
          countryName: item.name,
          countryId: item.id,
          residence: item.residence
        };

        vm.entity.marketCountryDTOList.push(temp);
      });
      console.log(vm.entity.marketCountryDTOList);
      console.log(vm.entity);
      if (vm.entity.id == null) {
        console.log("vm.entity");
        console.log(vm.entity);
        MarketsService.createFull(vm.entity, onSaveSuccess, onSaveError);
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }


    vm.addCountry = function () {
      vm.initialSet.push({
        
        countryId: null,
        countryCode: null,
        countryName: null,
        id: null,
        residence: "CITIZEN"
      });
    }; 
    
    vm.removeCountry = function ($index) {
      // angular.forEach(vm.initialSet, function (set) {
      //   if (set.number === dimen.number) {
      //     vm.initialSet.splice(set, 1);
      //   }
      // })
      vm.initialSet.splice($index, 1);

    };

    vm.sortableOptions = {
      "ui-floating": true,
      update: function (e, ui) {
        //  console.log(ui);
      }
    };

    vm.changed = function (index) {
     
    }
  }
})();
