(function() {
  "use strict";
  angular.module("webApp").factory("MarketsService", MarketsService);

  MarketsService.$inject = ["$resource", "$localStorage", "URLS"];

  function MarketsService($resource, $localStorage, URLS) {
    var resourceUrl = "";

    return $resource(
      resourceUrl,
      {},
      {
        getCountry: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url:
            URLS.BASE_URL +
            "contentservice/api/market-countries/:id"
        },
        createFull: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/markets/create-full"
        },
        getByOrganisation: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "contentservice/api/markets/filter-by-organisation/:id"
        },
        getById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "contentservice/api/markets/:id"
        },
        getFullById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "contentservice/api/markets/get-full/:id"
        },
        get: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "contentservice/api/markets/"
        },
        update: {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/markets"
        },
        create: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/markets"
        },
        delete: {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/markets/:id"
        }
      }
    );
  }
})();
