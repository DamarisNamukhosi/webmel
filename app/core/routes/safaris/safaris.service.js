(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('SafarisService', SafarisService);

  SafarisService.$inject = ['$resource', '$localStorage', 'URLS'];

  function SafarisService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'saveFull': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safaris/create-full'
      },
      'getById': {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safaris/:id'
      },
      'getFull': {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safaris/get-full/:id'
      },
      'getDaysBySafariId': {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safari-days/filter-by-safari/:id'
      },
      'getDayById': {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safari-days/:id'
      },
      'getSafarisByGroupId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safaris/filter/:orgId?groupId=:groupId'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safaris'
      }
      ,
      'updateDay': {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safari-days'
      },
      'saveFullDay': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safari-days/save-list'
      },
      'aggregate': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/util/aggregate'
      },
      'update': {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safaris'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safaris'
      },
      'getSafaris': {
        method: 'GET',
        headers : {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safaris/filter/:orgId?groupId=:groupId&name=:name&sort=order%2Casc&page=:page&size=:size'
      },
      'refreshRequirements': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/safari-days/refresh-requirements/:dayId?orgId=:orgId&locationId=:locationId'
      },
      'saveGeneralService': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safari-general-services/save-full'
      },
      'deleteGeneralService': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safari-general-services/:id'
      },
      'archiveGeneralService': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safari-general-services/archive/:serviceId?safariId=:safariId'
      },
      'deleteGeneralServiceGroup': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/safari-service-groups/:id'
      },
      'updateGenSvcGroup': {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safari-service-groups'
      },
      'updateGenSvc': {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/safari-general-services'
      }
      //safari-general-services//safari-service-groups
      //costingservice/api/util/aggregate
    });
  }
})();
