(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('SafariLocationDialogController', SafariLocationDialogController);

    SafariLocationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'setParentVisible', 'LocationGroupsService', 'LocationTypesService', 'URLS', 'LocationsService', 'SafarisService'];

    function SafariLocationDialogController($timeout, $scope, $stateParams, $uibModalInstance, setParentVisible, LocationGroupsService, LocationTypesService, URLS, LocationsService, SafarisService) {
        var vm = this;

        vm.location = [];
        console.log($stateParams);
        vm.safariDay = false;
        vm.clear = clear;
        vm.save = save;
        vm.locationTypeChange = locationTypeChange;
        vm.getLocation = getLocation;
        //vm.getOrganization = getOrganization;
        vm.onLocationSelected = onLocationSelected;
        //vm.onLocationManagerSelected = onLocationManagerSelected;
        vm.tempParentLocations = [];
        vm.googleMapsUrl = URLS.GOOGLE_MAP_URL;
        vm.newLocation = {
            id: null,
            name: null,
        }

        initDialogModels();
        // initLocCoordinates();
        function initDialogModels() {
            console.log($stateParams);
            
                //get Day & location...
                vm.safariDay = true;
                SafarisService.getDayById({
                    id: $stateParams.id
                }).$promise.then(function (response) {
                    vm.day = response;
                    if (vm.day.endLocationId > 1) {
                        LocationsService.get({ id: vm.day.endLocationId }).$promise
                            .then(function (response) {
                                vm.location = response;
                                vm.day.endLocationId = $item.id;
                                vm.day.endLocationName = $item.name;
                            });
                    }
                });
        }
        // vm.googleMapsUrl = 'https://maps.google.com/maps/api/js?key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E';



        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            console.log(vm.location);
            vm.isSaving = true;
            SafarisService.updateDay(vm.day, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function locationTypeChange(id) {
            vm.tempParentLocations = vm.locations;
            console.log('changed location Type id');

            // angular.forEach(vm.locations, function(location){
            //     if((location.typeId < id || (id ==  10 || id ==11) && (location.typeId == 12 || location.typeId == 13) ) && !((location.typeId == 10 || location.typeId == 11) && (id ==  12 || id ==13))){
            //         vm.tempParentLocations.push(location);
            //     }
            // })            

        }


        function getLocation(val) {
            vm.searchedProperty = false;
            return LocationsService
                .query({
                    name: val,
                    types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19]
                })
                .$promise
                .then(function (results) {
                    vm.searchedItems = results;
                    return results.map(function (item) {

                        return item;
                    })
                });
        }

        function onLocationSelected($item, $model) {
            if (vm.safariDay) {
                vm.day.endLocationId = $item.id;
                vm.day.endLocationName = $item.name;
            } else {
                vm.newLocation.id = $item.id;
            }
        }
        // function onLocationManagerSelected($item, $model) {
        //   console.log($item);
        //     vm.newLocation.id=$item.id;

        //   }
    }
})();
