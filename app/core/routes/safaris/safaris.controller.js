(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SafarisController', SafarisController);

  SafarisController.$inject = [
    '$scope',
    '$location',
    '$state',
    '$localStorage',
    'packages',
    'URLS',
    'OrganizationsService',
    'PackagesService',
    '$stateParams',
    'LocationsService',
    'CurrenciesService',
    'ProgrammesService',
    '$timeout',
    'safariConstants'
  ];

  function SafarisController($scope, $location, $state, $localStorage, packages, URLS, OrganizationsService, PackagesService, $stateParams, LocationsService, CurrenciesService, ProgrammesService, $timeout, safariConstants) {
    var vm = this;

  }
})();
