(function () {
  'use strict';

  angular
    .module('webApp')
    .factory('SafarisUtil', SafarisUtil);

  function SafarisUtil() {

    var service = {
      expandSafariPAXConfigs: expandSafariPAXConfigs,
      generateVehiclesInfo: generateVehiclesInfo,
      autoAllocateObjects: autoAllocateObjects,
      validConfigsObjectsTotal: validConfigsObjectsTotal,
      validGroupObjectsTotal: validGroupObjectsTotal,
      validUnitObjectsTotal: validUnitObjectsTotal,
      travellersGroupPaxTravellersMismatch: travellersGroupPaxTravellersMismatch,
      travellersPaxSafariPaxMismatch: travellersPaxSafariPaxMismatch,
      generateTravellersInfo: generateTravellersInfo,
      expandSafariServiceGroups: expandSafariServiceGroups,
      initAccommodationConfiguration: initAccommodationConfiguration,
      initTranportConfiguration: initTranportConfiguration,
      generateTravellerGroup: generateTravellerGroup,
      generateUnits: generateUnits,
      generateSlots: generateSlots,
      initMealConfiguration: initMealConfiguration,
      initIncluionConfiguration: initIncluionConfiguration,
      initMiscConfiguration:initMiscConfiguration,
      initLMSConfiguration: initLMSConfiguration,
      initOTHERConfiguration: initOTHERConfiguration
    };

    return service;


    // generate units
    function generateUnits(generalService, type) {

      console.log("expanding : " + type);
      console.log(generalService);
      //reset
      generalService.units = [];

      //auto split objects into units
      var unitsWithExtraObject = (generalService.numOfObjects % generalService.numOfUnits);
      var minPaxPerUnit = ((generalService.numOfObjects - unitsWithExtraObject) / generalService.numOfUnits);


      console.log("stat : " + type + " unitsWithExtraObject: " + unitsWithExtraObject);
      console.log("stat : " + type + " minPaxPerUnit: " + minPaxPerUnit);

      //loop
      for (var i = 0; i < generalService.numOfUnits; i++) {
        var unit = {
          "ref": generalService.ref + "-U" + (i + 1),
          "type": type,
          "name": type + " " + (i + 1),
          "numOfSlots": null, //by default do not set the number of slots...
          "notes": null,
          "configs": null,
          "numOfObjects": 0,
          "allocationMode": "AUTO",
          "objectList": null,
          "slots": []
        };

        //set the first unit to contain all people - others will be zero - manual change needed
        if (i == 0) {
          unit.numOfObjects = generalService.numOfObjects;
        }

        //distribute people into units (split evenly)
        if (i < unitsWithExtraObject) {
          unit.numOfObjects = minPaxPerUnit + 1; // plus 1
          unit.numOfSlots = 1;
        } else {
          unit.numOfObjects = minPaxPerUnit; // use minimum
          unit.numOfSlots = 1;

        }

        //add the general service
        generalService.units.push(unit);

        //expand slots
        if (type == "ROOM") {
          generateSlots(unit, "BED");
        } else if (type == "VEHICLE") {
          //unit.configs.category = "SAFARI"; //by default the vehicle is for SAFARI and not LUGGAGE
          generateSlots(unit, "SEAT");
        } else {
          //default group
          generateSlots(unit, "GROUP");
        }
      }
    }


    //generate slots in a unit
    function generateSlots(unit, type) {
      //reset
      unit.slots = [];

      if (unit.numOfSlots != null) {
        //loop
        for (var i = 0; i < unit.numOfSlots; i++) {
          var slot = {
            "ref": (i + 1),
            "type": type,
            "name": type + " " + (i + 1),
            "notes": null,
            "configs": null,
            "numOfObjects": 0,
            "objectList": null,
          };

          //add the slot
          unit.slots.push(slot);
        }
      }
    }

    //init traveller group
    function generateTravellerGroup(safari, startDateTime, endDateTime) {
      console.log("..generateTravellerGroup");
      safari.travellerGroup = {
        "name": safari.name,
        "notes": safari.description,
        "order": 0,
        "pax": safari.pax,
        "profile": safari.profile,
        "purpose": safari.purpose,
        "reference": safari.name,
        "residence": safari.residence,
        "travellers": [],
        "eta": startDateTime,
        "etd": endDateTime,
        "tel": null,
        "country": safari.country,
        "description": safari.description,
        "email": null,
        "travelDetails": {
          "arrival": {
            "pickUpLocId": safari.pickUpLocationid
          },
          "departure": {
            "dropOffLocId": safari.dropOffLocationid
          }
        }
      };

      //generate persons
      generateTravellersInfo(safari);

      //initialize the configs
      initAccommodationConfiguration(safari);
      initTranportConfiguration(safari);
      //initMealConfiguration(safari);
    }

    //initialize accommodation service configs
    function initAccommodationConfiguration(safari) {
      var configs = {
        "name": "Accommodation",
        "generalServiceType": "ACCOMMODATION",
        "generalServiceId": 1,
        "usedContractId": null,
        "objectType": "PERSON",
        "notes": null,
        "safariServiceCategory": "COVERAGE",
        "perTrip": true,
        "perObject": true,
        "perDay": true,
        "optional": false,
        "timeStrict": false,
        "global": true,
        "visible": true,
        "allocated": false,
        "allocationReason": "Not allocated",
        "numOfObjects": safari.travellerGroup.pax, //initial group size
        "objectList": null, //we need to initiate the people here...
        "numOfGroups": 1,
        "safariServiceGroups": []
      };

      //set the accommodation configs on the safari
      safari.serviceConfigsMap.ACCOMMODATION = configs;

      //expand general services
      expandSafariServiceGroups(configs);

      //auto allocate
      autoAllocateObjects(configs, safari);

    }
    //initialize meal service configs
    function initMealConfiguration(safari) {
      console.log(safari);

      var configs = {
        "name": "Meal",
        "generalServiceType": "MEAL",
        "generalServiceId": 2,
        "usedContractId": null,
        "objectType": "PERSON",
        "notes": null,
        "safariServiceCategory": "ACTIVITY",
        "perTrip": true,
        "perObject": true,
        "perDay": true,
        "optional": false,
        "timeStrict": false,
        "global": true,
        "visible": true,
        "allocated": false,
        "allocationReason": "Not allocated",
        "numOfObjects": safari.travellerGroupDTO.pax, //initial group size
        "objectList": null, //we need to initiate the people here...
        "numOfGroups": 1,
        "safariServiceGroups": []
      };

      //set the accommodation configs on the safari
      safari.serviceConfigsMap.MEAL = configs;

      //expand general services
      expandSafariServiceGroups(configs);

      //auto allocate
      autoAllocateObjects(configs, safari);

    }

    //initialize MISC service configs
    function initMiscConfiguration(safari) {
      console.log(safari);

      var configs = {
        "generalServiceType": "MISCELLANEOUS",
        "generalServiceId": 11,
        "usedContractId": null,
        "objectType": "PERSON",
        "notes": null,
        "safariServiceCategory": "ACTIVITY",
        "perTrip": true,
        "perObject": true,
        "perDay": true,
        "optional": false,
        "timeStrict": false,
        "global": true,
        "visible": true,
        "allocated": false,
        "allocationReason": "Not allocated",
        "numOfObjects": safari.travellerGroupDTO.pax, //initial group size
        "objectList": null, //we need to initiate the people here...
        "numOfGroups": 1,
        "safariServiceGroups": []
      };

      //set the accommodation configs on the safari
      safari.serviceConfigsMap.MISCELLANEOUS = configs;

      //expand general services
      expandSafariServiceGroups(configs);

      //auto allocate
      autoAllocateObjects(configs, safari);

    }
 //initialize lms service configs
 function initLMSConfiguration(safari) {
  console.log(safari);

  var configs = {
    "generalServiceType": "LOCATION_MANAGEMENT",
    "generalServiceId": 6,
    "usedContractId": null,
    "objectType": "PERSON",
    "notes": null,
    "safariServiceCategory": "ACTIVITY",
    "perTrip": true,
    "perObject": true,
    "perDay": true,
    "optional": false,
    "timeStrict": false,
    "global": true,
    "visible": true,
    "allocated": false,
    "allocationReason": "Not allocated",
    "numOfObjects": safari.travellerGroupDTO.pax, //initial group size
    "objectList": null, //we need to initiate the people here...
    "numOfGroups": 1,
    "safariServiceGroups": []
  };

  //set the accommodation configs on the safari
  safari.serviceConfigsMap.LOCATION_MANAGEMENT = configs;

  //expand general services
  expandSafariServiceGroups(configs);

  //auto allocate
  autoAllocateObjects(configs, safari);

}

//initialize other service configs
function initOTHERConfiguration(safari) {
  console.log(safari);

  var configs = {
    "generalServiceType": "OTHER",
    "generalServiceId": 12,
    "usedContractId": null,
    "objectType": "PERSON",
    "notes": null,
    "safariServiceCategory": "ACTIVITY",
    "perTrip": true,
    "perObject": true,
    "perDay": true,
    "optional": false,
    "timeStrict": false,
    "global": true,
    "visible": true,
    "allocated": false,
    "allocationReason": "Not allocated",
    "numOfObjects": safari.travellerGroupDTO.pax, //initial group size
    "objectList": null, //we need to initiate the people here...
    "numOfGroups": 1,
    "safariServiceGroups": []
  };

  //set the accommodation configs on the safari
  safari.serviceConfigsMap.OTHER = configs;

  //expand general services
  expandSafariServiceGroups(configs);

  //auto allocate
  autoAllocateObjects(configs, safari);

}

    //initialize accommodation service configs
    function initTranportConfiguration(safari) {

      var configs = {
        "name": "Transport",
        "generalServiceType": "TRANSPORT",
        "generalServiceId": 8,
        "objectType": "PERSON",
        "notes": null,
        "safariServiceCategory": "COVERAGE",
        "perTrip": true,
        "perObject": true,
        "perDay": true,
        "optional": false,
        "timeStrict": false,
        "global": true,
        "visible": true,
        "allocated": false,
        "allocationReason": "Not allocated",
        "numOfObjects": safari.travellerGroup.pax, //initial group size
        "objectList": null, //we need to initiate the people here...
        "numOfGroups": 1,
        "safariServiceGroups": []
      };

      //set the accommodation configs on the safari
      safari.serviceConfigsMap.TRANSPORT = configs;

      //expand general services
      expandSafariServiceGroups(configs);


      //auto allocate
      autoAllocateObjects(configs, safari);

    }
 //initialize meal service configs
//initialize meal service configs
 function initIncluionConfiguration(parentGenSvc, inclusion) {
  console.log(parentGenSvc);
  console.log(inclusion);
  var configs = {
    "id":inclusion.id,
    "name": inclusion.name,
    "generalServiceType": inclusion.generalServiceType,
    "generalServiceId": inclusion.generalServiceId,
    "usedContractId": parentGenSvc.usedContractId,
    "objectType": inclusion.objectType,
    "notes": inclusion.notes,
    "safariServiceCategory": inclusion.serviceCategory,
    "perTrip": inclusion.perTrip,
    "perObject": inclusion.perObject,
    "perDay": inclusion.perDay,
    "optional": inclusion.optional,
    "timeStrict": inclusion.timeStrict,
    "global": inclusion.global,
    "visible": inclusion.visible,
    "allocated": false,
    "allocationReason": "Not allocated",
    "numOfObjects": parentGenSvc.numOfObjects, //initial group size
    "numOfGroups": parentGenSvc.numOfGroups,
    "serviceId": inclusion.serviceId,
    "serviceName": inclusion.serviceName,
    "supplierId": inclusion.supplierId,
    "supplierName": inclusion.supplierName,
    "issuerId": inclusion.issuerId,
    "issuerName": inclusion.issuerName,
    "startTime": inclusion.startTime,
    "endTime": inclusion.endTime,
    "timeInHours": inclusion.timeInHours,
    "dayOffest": null,
    "currencyId": inclusion.currencyId,
    "alternativeToId": null,
    "alternativeToName": null,
    "amount": inclusion.amount,
    "happeningDuringId": parentGenSvc.serviceId,
    "happeningDuringName":  parentGenSvc.serviceName,
    "importedFromId": null,
    "importedFromName": null,
    "includedInId": parentGenSvc.serviceId,
    "includedInName": parentGenSvc.serviceName,
    "paidById": parentGenSvc.id,
    "paidByName": parentGenSvc.name,
    "partOfId": parentGenSvc.id,
    "partOfName": parentGenSvc.name,
    "safariServiceGroups": [],
    "safariObjects": null
  };
  configs.safariObjects = parentGenSvc.safariObjects;
  console.log(parentGenSvc.safariServiceGroups.length);
  angular.forEach(parentGenSvc.safariServiceGroups, function(parentGrp){
    console.log(parentGrp);
    var group = {
      "name" : parentGrp.name,
      "ref": parentGrp.ref,
      "generalServiceType" : configs.generalServiceType,
      "generalServiceId" :  configs.generalServiceId,
      "supplierId" : configs.supplierId,
      "supplierName" :  configs.supplierName,
      "issuerId" : configs.issuerId,
      "issuerName" :  configs.issuerName,
      "ref" : parentGrp.ref,
      "usedContractId" : parentGrp.usedContractId,
      "usedVersionId" : parentGrp.usedVersionId,
      "usedVersionNumber" : parentGrp.usedVersionNumber,
      "units": parentGrp.units
    };
    console.log(group);

    angular.forEach(group.units, function(unit){
      if(!angular.equals(unit.safariSlots,null) && angular.isDefined(unit.safariSlots)){
      unit.numOfSlots =  unit.safariSlots.length;
      unit.safariSlots = [];
      console.log(unit);
      console.log(unit.hasOwnProperty("rateUsedValue"))
      console.log(unit.hasOwnProperty("rateResolvedValue"));
      if(unit.hasOwnProperty("rateResolvedValue")){
        console.log(unit);

        delete unit["rateResolvedValue"];
      }
      if(unit.hasOwnProperty("supplementUsedValue")){
        console.log(unit);

        delete unit["supplementUsedValue"];
      }
      //discountUsedValue/ discountResolvedValue /safariServices
      if(unit.hasOwnProperty("safariServices")){
        console.log(unit);
        unit.safariServices = [];
             //create safariService
      angular.forEach(unit.safariObjects, function(object){
        var service = {
          "objectType": "PERSON",
          "objectId": object.id,
          "generalServiceType":configs.generalServiceType,
          "generalServiceId": configs.generalServiceId,
          "supplierId": configs.supplierId,
          "supplierName": configs.supplierName,
          "issuerId": configs.issuerId,
          "issuerName": configs.issuerName,
          "serviceId": configs.serviceId,
          "serviceName": configs.serviceName
        };
        unit.safariServices.push(service);
      });
      }else{
        unit.safariServices = [];
                  //create safariService
      angular.forEach(unit.safariObjects, function(object){
        var service = {
          "objectType": "PERSON",
          "objectId": object.id,
          "generalServiceType":configs.generalServiceType,
          "generalServiceId": configs.generalServiceId,
          "supplierId": configs.supplierId,
          "supplierName": configs.supplierName,
          "issuerId": configs.issuerId,
          "issuerName": configs.issuerName,
          "serviceId": configs.serviceId,
          "serviceName": configs.serviceName
        };
        unit.safariServices.push(service);
      });
      }

      if(unit.hasOwnProperty("rateUsedValue")){
        console.log(unit);

        delete unit["rateUsedValue"];
      }

      if(unit.hasOwnProperty("supplementResolvedValue")){
        console.log(unit);

        delete unit["supplementResolvedValue"];
      }
      if(unit.hasOwnProperty("discountUsedValue")){
        console.log(unit);

        delete unit["discountUsedValue"];
      }
      if(unit.hasOwnProperty("discountResolvedValue")){
        console.log(unit);

        delete unit["discountResolvedValue"];
      }
    }
      if (group['generalServiceType'] == "ACCOMMODATION") {
        unit.name = "Rooms";
        unit.type = "ROOM"
        //console.log("expanding : " + group['generalServiceType'] + " default: " + defaultnumOfRooms + "  rooms");

        //expand
        if (unit.numOfSlots != null) {
          //loop          generateSlots(unit, "SEAT");

          for (var i = 0; i < unit.numOfSlots; i++) {
            var slot = {
              "ref": (i + 1),
              "type": "ROOM",
              "name": "ROOM" + " " + (i + 1),
              "notes": null,
              "configs": null,
              "numOfObjects": 1,
              "objectList": null,
            };

            //add the slot
            unit.safariSlots.push(slot);
          }
        }
      } else if (group['generalServiceType'] == "TRANSPORT") {
        //generalService['unitsName'] = "Vehicles"; unit.name = "Rooms";
        unit.name = "Vehicles";
        unit.type = "VEHICLE"
        //console.log("expanding : " + group['generalServiceType'] + " default: " + defaultnumOfRooms + "  rooms");


        if (unit.numOfSlots != null) {
          //loop          generateSlots(unit, "SEAT");

          for (var i = 0; i < unit.numOfSlots; i++) {
            var slot = {
              "ref": (i + 1),
              "type": "SEAT",
              "name": "SEAT" + " " + (i + 1),
              "notes": null,
              "configs": null,
              "numOfObjects": 1,
              "objectList": null,
            };

            //add the slot
            unit.safariSlots.push(slot);
          }
        }
      }
      else if (group['generalServiceType'] == "MEAL") {
        //generalService['unitsName'] = "Table";
        unit.name = "Menu";
        unit.type = "MENU"
        //console.log("expanding : " + group['generalServiceType'] + " default: " + defaultnumOfRooms + "  rooms");

        if (unit.numOfSlots != null) {
          //loop          generateSlots(unit, "SEAT");

          for (var i = 0; i < unit.numOfSlots; i++) {
            var slot = {
              "ref": (i + 1),
              "type": "SEAT",
              "name": "SEAT" + " " + (i + 1),
              "notes": null,
              "configs": null,
              "numOfObjects": 1,
              "objectList": null,
            };

            //add the slot
            unit.safariSlots.push(slot);
          }
        }
      } else {
        //default group
        unit.name = "GROUP";
        unit.type = "GROUP"
        //console.log("expanding : " + group['generalServiceType'] + " default: " + defaultnumOfRooms + "  rooms");
      if (unit.numOfSlots != null) {

          for (var i = 0; i < unit.numOfSlots; i++) {
            var slot = {
              "ref": (i + 1),
              "type": "SLOT",
              "name": "SLOT" + " " + (i + 1),
              "notes": null,
              "configs": null,
              "numOfObjects": 1,
              "objectList": null,
            };

            //add the slot
            unit.safariSlots.push(slot);
          }
        }
      }


    });

    configs.safariServiceGroups.push(group);

  });
  console.log("configs");
  console.log(configs);
  //set the accommodation configs on the safari
  //safari.serviceConfigsMap.inclusion.generalServiceType = configs;

  //expand general services
  //expandSafariServiceGroups(configs);
  //generateUnits(generalService, "GROUP");
  //auto allocate
  //autoAllocateObjects(configs, safari);
  return configs;
}

    // expand safari travellers information
    function expandSafariServiceGroups(configs) {

      console.log("expanding : " + configs['generalServiceType'] + " " + configs.numOfGroups);

      //auto split - into groups
      var groupsWithExtraObject = (configs.numOfObjects % configs.numOfGroups);
      var minPaxPerGroup = ((configs.numOfObjects - groupsWithExtraObject) / configs.numOfGroups);

      //reset
      configs.safariServiceGroups = [];
      if (!configs.hasOwnProperty("safariObjects")){
      //loop
      for (var i = 0; i < configs.numOfGroups; i++) {

        var generalService = {
          "ref": "G" + (i + 1),
          "name": "Group " + (i + 1),
          "generalServiceId": configs.generalServiceId,
          "serviceId": null,
          "locationId": null,
          "locationName": null,
          "supplierId": null,
          "issuerId": null,
          "contractId": null,
          "contractVersionId": null,
          "numOfUnits": 1,
          "numOfObjects": 0,
          "allocationMode": "AUTO",
          "allocated": false,
          "allocationReason": "Not allocated",
          "objectList": null,
          "unitsName": "units",
          "units": []
        };

        //distribute people into groups (split evenly)
        if (i < groupsWithExtraObject) {
          generalService.numOfObjects = minPaxPerGroup + 1; // plus 1
        } else {
          generalService.numOfObjects = minPaxPerGroup; // use minimum
        }

        //add the general service
        configs.safariServiceGroups.push(generalService);

        //expand the units
        if (configs['generalServiceType'] == "ACCOMMODATION") {
          generalService['unitsName'] = "Rooms";

          //expand rooms to PPS - 2 people sharing a room
          var defaultnumOfRooms = Math.ceil(configs.numOfObjects / 2);
          generalService['numOfUnits'] = defaultnumOfRooms;

          console.log("expanding : " + configs['generalServiceType'] + " default: " + defaultnumOfRooms + "  rooms");

          //expand
          generateUnits(generalService, "ROOM");

        } else if (configs['generalServiceType'] == "TRANSPORT") {
          generalService['unitsName'] = "Vehicles";
          generalService['numOfUnits'] = 0; // do not include any vehicle by default...
          generateUnits(generalService, "VEHICLE");
        } else {
          //default group
          generateUnits(generalService, "GROUP");
        }

      }
    }else{
      console.log("has safariObjects");
      console.log(configs);
    }
    }

    // genarate travellers information
    function generateTravellersInfo(safari) {

      //reset first
      safari.travellerGroup.travellers = [];

      for (var i = 0; i < safari.travellerGroup.pax; i++) {
        var tempTraveller = {
          "age": null,
          "category": "TRAVELLER",
          "configs": null,
          "contentStatus": "DRAFT",
          "country": safari.travellerGroup.country,
          "gender": null,

          "description": null,
          "dob": null,
          "document": null,
          "email": null,
          "eta": safari.travellerGroup.eta,
          "etd": safari.travellerGroup.etd,
          "groupId": null,

          "id": null,
          "name": (safari.travellerGroup.name == null ? "Person " : safari.travellerGroup.name) + " " + (i + 1),
          "notes": null,
          "order": 0,
          "profession": null,
          "profile": safari.travellerGroup.profile,
          "purpose": safari.travellerGroup.purpose,
          "ref": "P" + (i + 1),
          "requiresCompleteness": true,
          "residence": safari.travellerGroup.residence,
          "statusReason": null,
          "tel": safari.travellerGroup.tel,
          "travelDetails": angular.copy(safari.travellerGroup.travelDetails),
          "user": null
        };
        //add the traveller
        safari.travellerGroup.travellers.push(tempTraveller);

        //hide the modal
        angular.element('#confimTravellerRefresh').modal('hide');

      }

    }


    // function to check the pax in safari
    function travellersGroupPaxTravellersMismatch(safari) {
      if (safari.travellerGroup == null) {
        return false;
      } else {
        return safari.travellerGroup.travellers.length + "" != safari.travellerGroup.pax + "";
      }
      return false;
    }

    // function to check the pax in safari
    function travellersPaxSafariPaxMismatch(safari) {
      if (safari.travellerGroup == null) {
        return false;
      } else {
        return safari.pax + "" != safari.travellerGroup.pax + "";
      }
      return false;
    }


    //validate configs total against the group
    function validConfigsObjectsTotal(configs, safari) {
      console.log("configs");
      console.log(configs);
      console.log(safari);
      if (configs == null || configs == undefined) {
        return true;
      }

      if (safari.travellerGroup == null || configs == undefined) {
        return true;
      }

      return (configs.numOfObjects + "") == (safari.travellerGroup.pax + ""); // check if safari group and total paxs ar
    }


    //validate groups total against the configs
    function validGroupObjectsTotal(configs) {

      if (configs == null || configs == undefined) {
        return true;
      }

      var totalPaxs = configs.numOfObjects;
      var accumulativeTotal = 0;

      angular.forEach(configs.safariServiceGroups, function (generalService) {
        accumulativeTotal = accumulativeTotal + generalService.numOfObjects;
      });

      return totalPaxs + "" == accumulativeTotal + ""; //return true if all is equal - ignore type
    }


    //validate units total against the group
    function validUnitObjectsTotal(generalService) {
      if (generalService == null || generalService == undefined) {
        return true;
      }

      // if the total number of units are 0 and the units list is empty or null - ALL is well (NO Vehciles needed for this)
      if ((generalService.units == null || generalService.units.length == 0) && generalService.numOfUnits == 0) {
        return true;
      }

      var totalPaxs = generalService.numOfObjects;
      var accumulativeTotal = 0;

      angular.forEach(generalService.units, function (unit) {
        accumulativeTotal = accumulativeTotal + unit.numOfObjects;
      });

      return totalPaxs + "" == accumulativeTotal + ""; //return true if all is equal - ignore type
    }




    //auto allocate objects
    function autoAllocateObjects(configs, safari) {
     console.log("autoallocation...");
     console.log(configs);
     console.log(safari);
      //check if configs allocation
      if (!validConfigsObjectsTotal(configs, safari)) {
        configs.allocated = false;
        configs.allocationReason = "PAX mismatch with client PAX.";
        return;
      }

      //create a mini list of all travellers
      console.log("angular.equals(configs.generalServiceType,'MEAL')");
      console.log(angular.equals(configs.generalServiceType,'MEAL'));
      // if(angular.equals(configs.generalServiceType,'MEAL')){

      //   var travellersMini = [];
      //   angular.forEach(safari.travellerGroup.travellers, function (traveller, key) {
      //     var travellerMini = {
      //       index: key,
      //       ref: traveller.ref,
      //       name: traveller.name,
      //       type: "PERSON",
      //       eta:traveller.eta,
      //       etd: traveller.etd,
      //       attributes :null
      //     };
      //     travellersMini.push(travellerMini);
      //   });
      // }else
      // {
      var travellersMini = [];
      if(angular.equals(safari.id, null)){
      angular.forEach(safari.travellerGroup.travellers, function (traveller, key) {
        var travellerMini = {
          index: key,
          ref: traveller.ref,
          name: traveller.name,
          type: "PERSON",
          eta:traveller.eta,
          etd: traveller.etd,
          attributes :null
        };
        travellersMini.push(travellerMini);
      });
    }else{
      angular.forEach(safari.travellerGroupDTO.travellers, function (traveller, key) {
        var travellerMini = {
          id: traveller.id,
          ref: traveller.ref,
          name: traveller.name,
          type: traveller.type,
          eta:traveller.eta,
          etd: traveller.etd,
          attributes :{
            country:traveller.country,
            purpose: traveller.purpose,
            dob: traveller.dob,
            profile: traveller.profile,
            residence: traveller.residence,
            category: traveller.category,
            age: traveller.age
          }
        };
        travellersMini.push(travellerMini);
      });
    }
//}
      configs.objectList = travellersMini;

      //check general service allocation
      if (!validGroupObjectsTotal(configs)) {
        configs.allocated = false;
        configs.allocationReason = "Service groups total not matching with client PAX";
        return;
      }

      //loop through the group
      var groupIndex = 0;
      angular.forEach(configs.safariServiceGroups, function (generalService) {
        var numOfObjects = generalService.numOfObjects;
        var max = groupIndex + numOfObjects;

        var persons = [];
        for (; groupIndex < max; groupIndex++) {
          persons.push(angular.copy(configs.objectList[groupIndex]));
        }

        generalService.objectList = persons;
        //loop through the units
        var unitIndex = 0;
        angular.forEach(generalService.units, function (unit) {

          var numOfObjectsInUnit = unit.numOfObjects;
          var innerMax = unitIndex + numOfObjectsInUnit;

          var innerPersons = [];
          for (; unitIndex < innerMax; unitIndex++) {
            innerPersons.push(angular.copy(generalService.objectList[unitIndex]));
          }
          unit.objectList = innerPersons;

        });

      });


      configs.allocated = true;
      configs.allocationReason = "Auto allocation";

      return configs;

    }

    // generate vehicles info
    function generateVehiclesInfo(configs, safari, eta, etd, safariVehicles) {

      //init or reset
      var safariVehicles = [];

      // loop through service groups and check units
      angular.forEach(configs.safariServiceGroups, function (generalService) {
        angular.forEach(generalService.units, function (unit) {

          var tempSafariVehicle = {
            "baseLocationId": safari.locationId,
            "baseLocationName": safari.locationName,
            "category": unit.configs.units, // this is equivalent to usage on the UI
            "categoryId": null,
            "categoryName": null,
            "configs": unit.configs,
            "contentStatus": "DRAFT",
            "description": null,
            "eta": eta,
            "etd": etd,
            "generalVehicleTypeId": unit.configs.generalVehicleType,
            "generalVehicleTypeName": unit.configs.generalVehicleTypeName,
            "id": null,
            "issuerId": generalService.issuerId,
            "issuerName: string": generalService.issuerId,

            "name": unit.name,
            "notes": null,
            "ref": unit.ref,
            "registration": null,
            "safariId": safari.id,
            "safariName": safari.name,
            "statusReason": null,
            "supplierId": generalService.supplierId,
            "supplierName": generalService.supplierName,
            "uuid": null,
            "vehicleId": null,
            "vehicleName": null,
            "vehicleTypeId": null,
            "vehicleTypeName": null
          }

          //push the vehicle to the safariVehicles
          safariVehicles.push(tempSafariVehicle);

        });

      });

      return safariVehicles;

    }

    // expand safari pax configs
    function expandSafariPAXConfigs(minPricingPax, maxPricingPax, pricingPAXgap) {

      var configs = [];

      var i = minPricingPax;

      for (; i < maxPricingPax; i = i + pricingPAXgap) {
        var paxConfig = {
          "contentStatus": "DRAFT",
          "maxPax": i,
          "minPax": (i + pricingPAXgap) - 1, // a gap - 1
          "name": "PAX " + i
        };
        configs.push(paxConfig);
      }

      //add the last pax
      var paxConfig = {
        "contentStatus": "DRAFT",
        "maxPax": i,
        "minPax": maxPricingPax,
        "name": "PAX " + i
      };
      configs.push(paxConfig);

      return configs;

    }

    // query params are strings, and need to be parsed
    function parsePage(page) {
      return parseInt(page);
    }

    // sort can be in the format `id,asc` or `id`
    function parsePredicate(sort) {
      var sortArray = sort.split(',');
      if (sortArray.length > 1) {
        sortArray.pop();
      }
      return sortArray.join(',');
    }
    function formatDate(startDate) {
      if (startDate !== null && startDate !== 'Invalid date') {
        var theDate = new Date(startDate.toString());
        return (
          theDate.getFullYear() +
          '-' +
          (theDate.getMonth() + 1) +
          '-' +
          theDate.getDate() +
          ' ' +
          theDate.getHours() +
          ':' +
          theDate.getMinutes() +
          ':' +
          theDate.getSeconds() +
          '.' +
          theDate.getMilliseconds()
        );
      } else {
        return '';
      }
    }
  }
})();
