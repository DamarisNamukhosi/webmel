(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('SafariDetailController', SafariDetailController);

  SafariDetailController.$inject = [
    '$scope',
    '$location',
    '$state',
    '$localStorage',
    'entity',
    'days',
    'URLS',
    'OrganizationsService',
    'PackagesService',
    '$stateParams',
    'LocationsService',
    'CurrenciesService',
    'ProgrammesService',
    '$timeout',
    'safariConstants',
    '$rootScope',
    'SafarisService'
  ];

  function SafariDetailController($scope, $location, $state, $localStorage, entity, days, URLS, OrganizationsService, PackagesService, $stateParams, LocationsService, CurrenciesService, ProgrammesService, $timeout, safariConstants, $rootScope, SafarisService) {
    var vm = this;


    vm.safari = entity;
    console.log(vm.safari);
    vm.safari.safariDays = days;
    $rootScope.$on('rootScope:emit', function(event, data) {
      console.log(data); // 'Emit!'
      $rootScope.safariDays[$rootScope.safariDays.indexOf(data)] = data;

    });
    // var ages = [21,23,12,43,52,11,42]
    // var older = ages.filter(age => age >= 21);
    // console.log(older);
    $rootScope.safariDays = vm.safari.safariDays;
    $rootScope.safari = vm.safari;
    console.log(vm.safari.safariDays);


    //initialize variables
    vm.statuses = {};

    //declare functions
    vm.toggleStatus = toggleStatus;
    vm.getStatus = getStatus;
    vm.getClasses = getClasses;
    vm.expandNames = expandNames;
    vm.countMisc = countMisc;
    vm.html2Pdf = html2Pdf;
    vm.onViewChange = onViewChange;
    vm.iteneraryMakePdf = iteneraryMakePdf;

    vm.selectService = selectService;
    vm.updateService = updateService;

    vm.simpleView = false;
    vm.manageView = true;
    vm.quotationView = false;
    vm.iteneraryView = false;
    vm.switchView = switchView;
    vm.currentView = "simple"
    //init
    initSafari();


    function initSafari() {
      angular.forEach(vm.safari.safariDays, function(day) {

        var template = angular.fromJson(day.template);
        day.template = template;

        var personsList = angular.fromJson(day.personsList);
        day.personsList = personsList;
        vm.rowData = [];
        //copy
        // day.generalServices = vm.safari.generalServices;
        angular.forEach(days, function(day) {
          day.misc = [];
          day.countMisc= 0;
          var row = {
            "day": day.day,
            "pax": day.pax,
            "locationName": day.locationName,
            "startLocationName": day.startLocationName,
            "endLocationName": day.endLocationName,
            "notes": day.notes,
            "supplierName": null,
            "roomCategory": null,
            "totalSupplierCost": day.totalSupplierCost,
            "mealPlan": null,
            "activities": [],
            "serviceName": null,
            "totalCost": vm.safari.totalSupplierCost,
            "misc":[],
            "countMisc": 0
          };
          angular.forEach(day.generalServices, function(service) {

            if (angular.equals(service.serviceCategory, "ACTIVITY")) {
              row.serviceName = service.name
            }

            if (angular.equals(service.generalServiceType, "ACCOMMODATION")) {
              row.supplierName = service.supplierName

              angular.forEach(service.usedExistences, function(ext) {
                if (angular.equals(ext.dimensionType, "ROOM_CATEGORY")) {

                  row.roomCategory = ext.name
                } else if (angular.equals(ext.dimensionType, "MEAL_PLAN")) {
                  row.mealPlan = ext.name

                }
              });
            }else {
              row.activities.push(service);
            }
            if (service.generalServiceType == 'MISCELLANEOUS') {
          row.countMisc = row.countMisc + 1;
          day.countMisc = day.countMisc + 1;
          row.misc.push(service);
          day.misc.push(service)
            }
          });

          vm.rowData.push(row);
        });

      });
      vm.manageView = false;
      vm.simpleView = true;
    }


    function expandNames(list) {
      if (angular.isDefined(list) && !angular.equals(list, null)) {
        var length = list.length;
        var names = "";

        var i = 0;

        angular.forEach(list, function(object) {
          names = names + "" + name;
          //increment
          i++;

          //names co
          if (i < length) {
            names = names + ", ";
          }
        });
      }
      return names;
    }


    function getClasses(index, length) {
      var classess = "";

      //assuming index is zero indexed...
      var remainder = (index + 1) % 2;

      //for odd index, remainder is 1
      if (remainder == 1) {
        classess = classess + "odd";
      } else {
        classess = classess + "even";
      }

      //check if inner
      if ((index + 1) + "" == length + "") {
        classess = classess + " last";
      } else {
        classess = classess + " inner";
      }

      return classess;
    }

    function getStatus(day, index) {
      var key = day + "_" + index;

      var status = vm.statuses[key];

      if (status == null || status == undefined) {
        status = false;
        vm.statuses[key] = status;
      }

      return status;
    }


    function toggleStatus(day, index) {

      var key = day + "_" + index;

      var status = vm.statuses[key];
      if (status == true) {
        status = false;
      } else {
        status = true;
      }
      vm.statuses[key] = status;
    }

    function countMisc(day) {
      vm.count = 0;
      day.miscServices = [];
      angular.forEach(day.generalServices, function(generalService) {
        if (generalService.generalServiceType == 'MISCELLANEOUS') {
          vm.count = vm.count + 1;
          day.miscServices.push(generalService);
//           var li = document.createElement("li");
// var text = document.createTextNode(generalService.supplierName);
// li.appendChild(text);
// document.getElementById("myUl-"+vm.count).appendChild(li);
        }
      });
      return day.miscServices;
    }

    function html2Pdf() {

      vm.body = [];

      var firstRow = [{
          text: 'DAY',
          style: 'tableHeader'
        },
        {
          text: 'LOCATION',
          style: 'tableHeader'
        },
        {
          text: 'SUPPLIER',
          style: 'tableHeader'
        },
        {
          text: 'ROOM CATEGORY',
          style: 'tableHeader'
        },
        {
          text: 'MEAL PLAN',
          style: 'tableHeader'
        },
        {
          text: 'ACTIVITY',
          style: 'tableHeader'
        }
      ];
      vm.body.push(firstRow);

      angular.forEach(vm.rowData, function(day) {
        var row = [{
            text: day.day,
            style: 'table'
          },
          {
            text: day.locationName,
            style: 'table'
          },
          {
            text: day.supplierName,
            style: 'table'
          },
          {
            text: day.roomCategory,
            style: 'table'
          },
          {
            text: day.mealPlan,
            style: 'table'
          },
          {
            text: day.serviceName,
            style: 'table'
          }
        ]
        vm.body.push(row);
      });

      var docDefinition = {
        info: {
          title: 'QUOTATION SUMMARY' + ' ' + vm.safari.name + ' - ' + vm.safari.year,
          author: vm.safari.orgName
        },
        header: function() {
          return [{
            margin: [62, 35, 62, 35],
            table: {
              widths: ['*'],
              headerRows: 0,
              body: [
                [{
                  text: " "

                }]
              ]
            },
            layout: 'noBorders'
          }]
        },
        footer: function(currentPage, pageCount) {
          return [{
            text: currentPage.toString() + ' of ' + pageCount,
            alignment: 'center',
            style: 'footer'
          }]
        },
        content: [
          [{
            text: "Tour Name :      " + "Quotation for" + " " + vm.safari.name + " - " + vm.safari.year,
            bold: true,
            alignment: 'left'

          }],
          [{
            text: "Client :               " + vm.safari.orgName,
            bold: true,
            alignment: 'left'
          }],
          [{
            text: "Start Location : " + vm.safari.srcLocationName,
            bold: true,
            alignment: 'left'
          }],
          [{
            text: "End Location :   " + vm.safari.dstLocationName,
            bold: true,
            alignment: 'left',
            margin: [0, 0, 0, 20]
          }],
          {
            style: 'topTable',
            table: {
              widths: ['*', '*', '*', '*', '*', '*'],
              heights: [10],
              headerRows: 1,
              body: vm.body

            },
            layout: {
              paddingLeft: function(i, node) {
                return 6;
              },
              paddingRight: function(i, node) {
                return 6;
              },
              paddingTop: function(i, node) {
                return 0;
              },
              paddingBottom: function(i, node) {
                return 0;
              },
              fillColor: function(i, node) {
                return (i % 2 === 0) ? '#F5F5F5' : null;
              }
            }

          },
          [{
            text: 'Total Safari Cost : ' + vm.safari.currencyName + ' ' + vm.safari.totalSupplierCost.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'),
            bold: true,
            alignment: 'right',
            margin: [0, 20, 0, 0]
          }]
        ],
        pageSize: 'A4',
        pageMargins: [62, 80, 62, 80],
        styles: {
          topHeader: {
            fontSize: 16,
            bold: true,
            margin: [0, 6, 0, 6],
            alignment: 'left'
          },
          tableHeader: {
            fontSize: 9,
            alignment: 'left',
            color: 'white',
            margin: [0, 5, 0, 5],
            fillColor: "#8f681b"
          },
          table: {
            fontSize: 8,
            alignment: 'left',
            color: 'black',
            margin: [0, 5, 0, 5]
          },
          header: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 15],
            alignment: 'left'
          },
          footer: {
            fontSize: 8,
            margin: [0, 25, 0, 17],
            alignment: 'center'
          }
        }
      }
      pdfMake.createPdf(docDefinition).download();
    }

    function switchView() {

      vm.manageView = !vm.manageView;
      vm.simpleView = !vm.simpleView;

    }

    function onViewChange(view) {
      console.group("view change");
      console.log("changing...");
      vm.currentView = view + '';
      if (angular.equals(view, 'simple')) {
        vm.manageView = false;
        vm.simpleView = !vm.simpleView;
        vm.quotationView = false;
        vm.iteneraryView = false;
      } else if (angular.equals(view, 'manage')) {
        vm.manageView = !vm.manageView;
        vm.simpleView = false;
        vm.quotationView = false;
        vm.iteneraryView = false;
      } else if (angular.equals(view, 'quote')) {
        vm.manageView = false;
        vm.simpleView = false;
        vm.quotationView = !vm.quotationView;
        vm.iteneraryView = false;
      } else if (angular.equals(view, 'itenerary')) {
        vm.manageView = false;
        vm.simpleView = false;
        vm.quotationView = false;
        vm.iteneraryView = !vm.iteneraryView;
      }
      console.groupEnd();

    }

    function iteneraryMakePdf() {

      vm.body = [];
      vm.paragraphs = [];

      var firstRow = [{
          text: 'DAY',
          style: 'tableHeader'
        },
        {
          text: 'LOCATION',
          style: 'tableHeader'
        },
        {
          text: 'SUPPLIER',
          style: 'tableHeader'
        },
        {
          text: 'ROOM CATEGORY',
          style: 'tableHeader'
        },
        {
          text: 'MEAL PLAN',
          style: 'tableHeader'
        },
        {
          text: 'ACTIVITY',
          style: 'tableHeader'
        }
      ];
      vm.body.push(firstRow);

      angular.forEach(vm.rowData, function(day) {
        var row = [{
            text: day.day,
            style: 'table'
          },
          {
            text: day.locationName,
            style: 'table'
          },
          {
            text: day.supplierName,
            style: 'table'
          },
          {
            text: day.roomCategory,
            style: 'table'
          },
          {
            text: day.mealPlan,
            style: 'table'
          },
          {
            text: day.serviceName,
            style: 'table'
          }
        ]
        vm.body.push(row);
      });

      angular.forEach(vm.rowData, function(day) {
        var notes = [{
            text: day.day + ' - ' + day.locationName,
            bold: true
          },
          {
            text: "On arrival in " + day.startLocationName + " you will be transfered to the Hotel and assist you at check in. The rest of your day will be spent at Leisure. Lunch and Dinner will be on own arrangement. Overnight Stay at " + day.endLocationName + " .",
            style: 'paragraphs'

          }
        ]
        vm.paragraphs.push(notes);
      });

      var docDefinition = {
        info: {
          title: 'DETAILED ITENERARY' + ' ' + vm.safari.name + ' - ' + vm.safari.year,
          alignment: 'center'

        },
        header: function() {
          return [{
            margin: [62, 35, 62, 35],
            table: {
              widths: ['*'],
              headerRows: 0,
              body: [
                [{
                  text: " "
                }]
              ]
            },
            layout: 'noBorders'
          }]
        },
        footer: function(currentPage, pageCount) {
          return [{
            text: currentPage.toString() + ' of ' + pageCount,
            alignment: 'center',
            style: 'footer'
          }]
        },
        content: [
          [{
            text: "Tour Name :      " + "Quotation for" + " " + vm.safari.name + " - " + vm.safari.year,
            bold: true,
            alignment: 'left'

          }],
          [{
            text: "Client :               " + vm.safari.orgName,
            bold: true,
            alignment: 'left'
          }],
          [{
            text: "Start Location : " + vm.safari.srcLocationName,
            bold: true,
            alignment: 'left'
          }],
          [{
            text: "End Location :   " + vm.safari.dstLocationName,
            bold: true,
            alignment: 'left'
          }],
          [{
            text: 'Itenerary at a glance',
            style: 'topHeader',
            alignment: 'center'
          }],
          {
            table: {
              widths: ['*', '*', '*', '*', '*', '*'],
              heights: [10],
              headerRows: 1,
              body: vm.body
            },
            layout: {
              paddingLeft: function(i, node) {
                return 8;
              },
              paddingRight: function(i, node) {
                return 8;
              },
              paddingTop: function(i, node) {
                return 0;
              },
              paddingBottom: function(i, node) {
                return 0;
              },
              fillColor: function(i, node) {
                return (i % 2 === 0) ? '#F5F5F5' : null;
              }
            }
          },
          [{
            text: "Full Detailed Itenerary",
            style: 'topHeader',
            alignment: 'center'
          }],
          {
            columns: [{
              stack: [
                vm.paragraphs

              ]
            }]
          }

        ],
        pageSize: 'A4',
        pageMargins: [62, 80, 62, 80],
        styles: {
          topHeader: {
            fontSize: 16,
            bold: true,
            margin: [0, 30, 0, 30]
          },
          tableHeader: {
            fontSize: 9,
            alignment: 'left',
            color: '#202124',
            margin: [0, 5, 0, 5],
            fillColor: "#fff"
          },
          table: {
            fontSize: 8,
            alignment: 'left',
            color: '#202124',
            margin: [0, 5, 0, 5]
          },
          paragraphs: {

            margin: [0, 10, 0, 15]

          },
          header: {
            fontSize: 16,
            bold: true,
            margin: [0, 10, 0, 15],
            alignment: 'left'
          },
          footer: {
            fontSize: 8,
            margin: [0, 25, 0, 17],
            alignment: 'center'
          }
        }
      }
      pdfMake.createPdf(docDefinition).download();
    }




    function selectService(record,action) {
      //set the selected row
      console.log(record);
      console.log(action);
      vm.selectedService = record;
      console.log(vm.selectedService);
      vm.generalServiceType =  record.generalServiceType;
      vm.entity = record;
      if(angular.equals(action,'update')){
       $state.go("safari-detail.newService", {day:vm.entity.dayId, type: vm.generalServiceType}, {reload: true});
      }
    }

    function updateService(service,action) {
      console.log(action);
      console.group("serive DElete");
      console.log("ready to delete " + service.id);
      SafarisService.archiveGeneralService({
        serviceId: service.id,
        safariId: vm.safari.id
      }).$promise.then(function(response) {
        console.log("deleted one group");
      });

    }

    function onSaveSuccess() {
      console.log("vm.vm.selectService");
      vm.selectedService.contentStatus = "DELETED";
      console.log(vm.selectedService);

      SafarisService.updateGenSvc(vm.selectedService).$promise.then(function(response) {
        console.log("deleted service");
        console.log(response);
        $state.reload();
      });
    }

    function onError() {
      console.length("error");
      vm.errorMessage = "Failed to delete"
    }

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;

      console.log(vm.safari.safariDays);
      if (vm.safari.safariDays.id == $stateParams.id) {
        console.log(vm.safari.safariDays);
        SafarisService.updateDay(vm.safari.safariDays, onSaveSuccess, onSaveError);

      }
    }

  }
})();
