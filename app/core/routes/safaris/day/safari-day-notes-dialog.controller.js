(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("SafariDayNotesDialogController", SafariDayNotesDialogController);

  SafariDayNotesDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "$localStorage",
    "entity",
    "SafarisService"
  ];

  function SafariDayNotesDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    $localStorage,
    entity,
    SafarisService

  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;
    vm.entity = entity;


    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;

      console.log(vm.entity);
      if (vm.entity.id == $stateParams.dayId) {

        SafarisService.updateDay(vm.entity, onSaveSuccess, onSaveError);

      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

  }
})();
