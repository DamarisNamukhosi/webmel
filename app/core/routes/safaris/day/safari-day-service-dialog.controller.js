
(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('SafariDayServiceDialogController', SafariDayServiceDialogController);

  SafariDayServiceDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$state','$injector', '$localStorage', 'SafarisService', 'OrganizationsService', 'URLS', 'LocationsService', 'entity', 'safariConstants', 'SafarisUtil', 'ContractsService', '$http', 'InclusionsService', 'DimensionSetsService', 'GeneralServicesService', 'MarketsCountriesService', 'CountriesService', 'RatesService', '$rootScope','CurrenciesService'];
  function SafariDayServiceDialogController($timeout, $scope, $stateParams, $uibModalInstance, $state,$injector, $localStorage, SafarisService, OrganizationsService, URLS, LocationsService, entity, safariConstants, SafarisUtil, ContractsService, $http, InclusionsService, DimensionSetsService, GeneralServicesService, MarketsCountriesService, CountriesService, RatesService, $rootScope,CurrenciesService) {
    var vm = this;
    console.log($scope);
    console.log($rootScope);


    //variable declarations
    vm.reccurentFolders = false;
    vm.contracts = [];
    vm.contracted = "YES"
    vm.currencyId = 204;
    vm.showUnitCosts = false;
    vm.showInclusions = false;
    vm.itemsPerPage =  5;
    vm.itemsPerPageStr = "5";
    vm.compareRates =  false;
    vm.manual =  true;
    //function declaration
    vm.clear = clear;
    vm.save = save;
    vm.getLocation = getLocation;
    vm.onLocationSelected = onLocationSelected;
    vm.getLocs = getLocs;
    vm.onTargetSelected = onTargetSelected;
    vm.initAccommodationConfiguration = initAccommodationConfiguration;
    //vm.generateTravellerGroup =  generateTravellerGroup;
    vm.generateClientInfo = generateClientInfo;
    vm.expandSafariServiceGroups = expandSafariServiceGroups;
    vm.validGroupObjectsTotal = validGroupObjectsTotal;
    vm.validUnitObjectsTotal = validUnitObjectsTotal;
    vm.getOrgs = getOrgs;
    vm.expandUnits = expandUnits;
    vm.expandSlots = expandSlots;
    vm.contractChange = contractChange;
    vm.compareWithoutType = compareWithoutType;
    vm.genSvcChange = genSvcChange;
    vm.generateInputExistences = generateInputExistences;
    vm.aggregate = aggregate;
    vm.refresh = refresh;
    vm.changeItemsPerPage = changeItemsPerPage;
   vm.selectedProperty = selectedProperty;
   vm.gatherInclusions = gatherInclusions;
   vm.recompute =  recompute;
   vm.saveGenSvc = saveGenSvc;
    //vm.onLocationManagerSelect
    initSafari();
    compareRates();
    function initSafari() {
      CurrenciesService.query().$promise.then(function (response){
        vm.currencies = response;
      })
      console.log($stateParams);
      vm.generalServiceType = $stateParams.type;
      console.log(angular.isDefined($rootScope.safariDays));
      console.log(!angular.equals($rootScope.safariDays, null));
      if (!angular.equals($rootScope.safariDays, null) && angular.isDefined($rootScope.safariDays)) {
        console.log("vm.day");
        angular.forEach($rootScope.safariDays, function (day) {
          console.log(angular.equals($stateParams.day, day.id + ''));
          if (angular.equals($stateParams.day, day.id + '')) {
            console.log("vm.day");

            vm.day = day;
            console.log(vm.day);
          }
        })
      } else {
        SafarisService.getDayById({
          id: $stateParams.day
        }).$promise.then(function (results) {
          vm.day = results
        });
      }
      if (!angular.equals($rootScope.safari, null) && angular.isDefined($rootScope.safari)) {
        vm.safari = $rootScope.safari;
        vm.day.serviceConfigsMap =vm.safari.serviceConfigsMap;
        console.log(vm.day);
        console.log(vm.safari);

      } else {
        SafarisService.getFull({ id: $stateParams.id }).$promise.then(function (response) {
          vm.safari = response;
          console.log(vm.safari);


        });

      }
      loadJson();

      // vm.entity.locationId = vm.day.locationId;
      // vm.entity.locationName = vm.day.locationName;
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function save() {
      console.log(vm.day);
      vm.isSaving = true;
      console.log(vm.safari.serviceConfigsMap['ACCOMMODATION'].safariServiceGroups);
      if (vm.entity.id !== null) {
        console.log("vm.entity");
        gatherInclusions();
        console.log(vm.entity);

        vm.day.generalServices.push(vm.entity);
        var copy = angular.copy(vm.day.generalServices.objectList);
        angular.forEach(vm.day.generalServices, function (genSvc) {
          console.log("genSvc");
          console.log(genSvc.hasOwnProperty("objectList"));
          console.log(genSvc.objectList);
          console.log(angular.equals(genSvc.objectList, "null"));
          console.log(angular.equals(genSvc.objectList, null))
          if (genSvc.hasOwnProperty("objectList")) {
            if (!angular.equals(genSvc.objectList, "null") || angular.isUndefined(genSvc.objectList)) {
              console.log(genSvc);
              console.log("safariObjects");

              genSvc["safariObjects"] = vm.day.personsList;
            } else {
              console.log(genSvc);
              genSvc["safariObjects"] = genSvc["objectList"];
              delete genSvc["objectList"];
            }
          }
          // else if (genSvc.hasOwnProperty("personsList")) {
          //   console.log("persionList");
          //   genSvc["safariObjects"] = genSvc["persionList"];
          // }
        });
        vm.day.updated = true;
        vm.dayList = [];
        console.log($rootScope.safariDays);
        angular.forEach($rootScope.safariDays, function (day, key) {
          console.log(key);
          console.log(day);
          console.log(day.id);
          console.log(day.updated);
          if (day.updated) {
            angular.forEach(day.generalServices, function(dayGenSvc){
              if(dayGenSvc.hasOwnProperty("safariObjects")){
                delete dayGenSvc.safariObjects;

              }
            });
            day.personsList = JSON.stringify(day.personsList);
            // day.template = JSON.stringify(day.template);
            console.log(day);
            console.log(key);
            delete day.template;
            console.log($rootScope.safariDays[key + 1]);
            vm.dayList.push(day);

            if (!angular.equals(day.endLocationId, null) && angular.isDefined(day.endLocationId)) {
              console.log("update next day");
              if (angular.isDefined($rootScope.safariDays[key + 1])) {
                $rootScope.safariDays[key + 1].startLocationId = day.endLocationId;
                $rootScope.safariDays[key + 1].startLocationName = day.endLocationName;
                $rootScope.safariDays[key + 1].updated = true;
              }
            }
          } else {
            console.log("not true");
            console.log(day);
          }
        });
        console.log(vm.dayList);
        console.log(vm.safari);
        SafarisService.saveFullDay(vm.dayList, onSaveSuccess, onSaveError);
      } else {
        setConfigs();
        //updateLocalStorage();
        console.log("creating..");
        console.log(vm.entity);
        //SafarisService.updateDay(vm.entity, onSaveSuccess, onSaveError);

      }
    }
    function gatherInclusions() {
      console.log("vm.entity");
      console.log(vm.entity);
      console.log(vm.day);
      console.log(vm.safari);
      console.log(vm.day.generalServices);
      console.log(vm.day.generalServices[vm.generalServiceId]);
      console.log(_.uniqBy(vm.day.inclusions, 'id'));
      // const uniqueLocations = _.uniqWith(
      //   locations,
      //   (locationA, locationB) =>
      //     locationA.latitude === locationB.latitude &&
      //     locationA.longitude === locationB.longitude
      // );
      console.log("b4 saving..");

      var uniqueInclusion = _.uniqWith(
        vm.day.inclusions,
        (inclusionA, inclusionB) =>
        inclusionA.dayOffest === inclusionB.dayOffest &&
        inclusionA.id === inclusionB.id
      );
      console.log(uniqueInclusion);
      console.group("inclusion setting");

      vm.uniqueInclusions = _.uniqBy(vm.day.inclusions, 'id');
      console.log(vm.uniqueInclusions);
      angular.forEach(vm.uniqueInclusions, function (inclusion) {
        // if (angular.equals(inclusion.dayOffest, null) || angular.equals(inclusion.dayOffest, 0)) {
        //   vm.day.generalServices.push(initIncluionConfiguration(inclusion));
        //   console.log(inclusion);

        // } else
        if (angular.equals(inclusion.dayOffest, 1)) {
          console.log(inclusion);
          console.log($rootScope.safariDays.indexOf(vm.day));
          console.log($rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day)]);
          //console.log(initIncluionConfiguration(inclusion));
          $rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day) + 1].generalServices.push(initIncluionConfiguration(inclusion));
          console.log($rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day) + 1]);
          console.groupEnd("inclusion setting end");
        }
      });

      console.log(vm.day);
      console.log($rootScope.safariDays);
    }

    function saveGenSvc(){
      if(vm.showInclusions){
        console.group("saving gen service");
        var copy =  angular.copy(vm.entity.objectList);
        vm.entity.objectList = JSON.stringify(copy);
        console.log(vm.entity);
        delete vm.entity.inclusions;
          angular.forEach(vm.entity.safariServiceGroups, function(group){
            group.copy = [];
            if(group.hasOwnProperty("safariObjects")){
            angular.forEach(group.safariObjects, function(object){
              var copy = {
                "id": object.id,
                "name": object.name,
                "type": object.type
              };
              group.copy.push(copy);
              angular.forEach(group.units, function(unit){
                console.l
                delete unit.objectList;
              })
            });
            console.log(group.safariObjects);
            group.safariObjects = group.copy;
            console.log(group.safariObjects);
          }else{
            console.group("init safariObject");
            group.safariObjects= [];
            angular.forEach(group.objectList, function(object){
              console.log(object);
              var copy = {
                "id": object.id,
                "name": object.name,
                "type": object.type
              };
              group.copy.push(copy);
              angular.forEach(group.units, function(unit){
                console.log(unit);
                if(unit.hasOwnProperty("objectList")){
                  unit.safariObjects = unit.objectList
                  delete unit.objectList;
                }
                console.log(unit);
              })
            });
            delete group.objectList;
            console.log(group.safariObjects);
            group.safariObjects = group.copy;
            console.log(group);
            console.groupEnd("init safariObject");
          }

          });
        var timeoutService = $injector.get('$timeout');


        timeoutService( function(){
          console.log("removing objectlist");
          delete vm.entity.objectList;
          console.log(vm.entity);
          console.log("removed");

        SafarisService.saveGeneralService(vm.entity).$promise.then(function (data){
          console.log("data");
          vm.entity.id = data.id;
          vm.entity.contentStatus = data.contentStatus;
          console.log(vm.entity);
          console.log(vm.day);
        });
      }, 1000 );
      }

      console.groupEnd("saving gen service");
    }
    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
      SafarisService.refreshRequirements({
        dayId: vm.day.id,
        orgId:$localStorage.current_organisation.id,
        locationId: vm.day.endLocationId
      }).$promise.then(function (response){
        console.log("requiments");
        console.log(response);
        vm.day.requirements = response;

      });
    }

    function refresh(){
      SafarisService.refreshRequirements({
        dayId: vm.day.id,
        orgId:$localStorage.current_organisation.id,
        locationId: vm.day.endLocationId
      }).$promise.then(function (response){
        console.log("requiments");
        console.log(response);
        vm.day.requirements = response;

      });
    }

    function onSaveError() {
      vm.isSaving = false;
    }
    function initIncluionConfiguration(inclusion) {
      console.log(vm.entity);
      return SafarisUtil.initIncluionConfiguration(vm.entity, inclusion);
    }
    function getLocation(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19]
        })
        .$promise
        .then(function (results) {
          vm.searchedItems = results;
          return results.map(function (item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {
      vm.day.locationId = $item.id;
      vm.day.locationName = $item.name;
      // vm.safariFilterParams.locationId = $item.id;
      // vm.safariFilterParams.locationName = $item.name;
     compareRates();

    }
    function compareRates(){
      console.log("compare rates");
      console.log(vm.day.locationId);
      console.log(vm.day.endLocationId);
      if(angular.isDefined(vm.day.locationId)){
        console.log("compare rates");

      RatesService.compressedRates({
        date: vm.day.day,
      generalServiceId: vm.entity.generalServiceId,
      locationId: vm.day.locationId,
      targetId: $localStorage.current_organisation.id
      }).$promise.then(function (results){
        vm.response = results;
        vm.organizationsRates = vm.response;
        // angular.forEach(vm.response, function (response) {
        //   if (response.contractId !== null) {
        //     vm.organizationsRates.push(response);
        //   }
        // });

        vm.rowCollection = _.orderBy(vm.organizationsRates, [function (org) {
          return org.organisationName.toLowerCase();
        }], ['asc']);
      })
    }
    }

    function getLocs(val, type) {
      return OrganizationsService
        .getOrganizationsByType({
          type: type,
          name: val
        })
        .$promise
        .then(function (results) {
          return results.map(function (item) {
            return item;
          })
        });

        // OrganizationsService
        // .filterByParentLocAndType({
        //   id: id,
        //   types: [5,6]
        // })
        // .$promise
        // .then(function (results) {
        //   return results.map(function (item) {
        //     return item;
        //   })
        // });
    }

    function generateClientInfo() {
      SafarisUtil.generateTravellerGroup(vm.safari, vm.startDateTime, vm.endDateTime);
    }

    function generatePersonsInfo() {
      //generate the travelllers information
      SafarisUtil.generateTravellersInfo(vm.safari);
    }

    function initAccommodationConfiguration() {
      initAccommodationConfiguration(vm.safari);
    }

    function initTranportConfiguration() {
      initTranportConfiguration(vm.safari);
    }

    function initMealConfiguration() {
      console.log(vm.safari);
      SafarisUtil.initMealConfiguration(vm.safari);
    }
    function initMiscConfiguration() {
      console.log(vm.safari);
      SafarisUtil.initMiscConfiguration(vm.safari);
    }
    function initLMSConfiguration() {
      console.log(vm.safari);
      SafarisUtil.initLMSConfiguration(vm.safari);
    }

    function initOTHERConfiguration() {
      console.log(vm.safari);
      SafarisUtil.initOTHERConfiguration(vm.safari);
    }
    function expandSafariServiceGroups(configs) {
      SafarisUtil.expandSafariServiceGroups(configs);
    }

    function generateVehiclesInfo(configs) {
      //init or reset
      vm.safari.safariVehicles = SafarisUtil.generateVehiclesInfo(configs, vm.safari, vm.startDateTime, vm.endDateTime, vm.safari.safariVehicles);
    }

    function expandUnits(generalService, type) {
      SafarisUtil.generateUnits(generalService, type);
      autoAllocateObjects(vm.entity, vm.safari);
    }

    function expandSlots(unit, type) {
      SafarisUtil.generateSlots(unit, type);
    }

    // validate configs total against safari pax
    function validConfigsObjectsTotal(configs) {
      return SafarisUtil.validConfigsObjectsTotal(configs, vm.safari);
    }

    // validate groups total against configs pax
    function validGroupObjectsTotal(configs) {
      return SafarisUtil.validGroupObjectsTotal(configs);
    }

    // validate units total against group pax
    function validUnitObjectsTotal(generalService) {
      return SafarisUtil.validUnitObjectsTotal(generalService);
    }

    //auto allocate people to the configs
    function autoAllocateObjects(configs) {
      //invoke safari utils
      SafarisUtil.autoAllocateObjects(configs, vm.safari);
    }


    function getOrgs(serviceId) {
      vm.contracts = [];
      return OrganizationsService
        .filterByGeneralService({
          id: serviceId
        })
        .$promise
        .then(function (results) {
          vm.organizations = results;
          vm.orgs = _.orderBy(vm.organizations, [function (org) {
            return org.name.toLowerCase();
          }], ['asc']);
          console.log("vm.orgs");
          console.log(vm.orgs);
        });
    }



    function onTargetSelected(group) {
      console.group("supplierSelect");
      console.log(group);
      console.log(vm.rowCollection);
      if(angular.equals(vm.generalServiceType, 'ACCOMMODATION')){
      angular.forEach(vm.rowCollection , function(partner){
        if(angular.isDefined(group.supplierName) && !angular.equals(group.supplierName, null)){
        if(angular.equals(partner.organisationName , group.supplierName)){

          group.supplierId = partner.organisationId;
          vm.entity.supplierId = partner.organisationId;
          vm.entity.supplierName = partner.organisationName;

        }
      }else{
        console.log("supplier Id present");
        if(angular.isDefined(group.supplierId) && !angular.equals(group.supplierId, null)){
          console.log(partner.organisationId);
        if(angular.equals(partner.organisationId , group.supplierId)){
          console.log("true");
          group.supplierName = partner.organisationName;
          vm.entity.supplierId = partner.organisationId;
          vm.entity.supplierName = partner.organisationName;

        }
      }
      }

      });
    }else{
      console.log("update entity");
      angular.forEach(vm.orgs , function(partner){
        if(angular.isDefined(group.supplierName) && !angular.equals(group.supplierName, null)){
        if(angular.equals(partner.name , group.supplierName)){

          group.supplierId = partner.id;
          vm.entity.supplierId = partner.id;
          vm.entity.supplierName = partner.name;

        }
      }else{
        if(angular.isDefined(group.supplierId) && !angular.equals(group.supplierId, null)){
        if(angular.equals(partner.name , group.supplierId)){

          group.supplierName = partner.name;
          vm.entity.supplierId = partner.id;
          vm.entity.supplierName = partner.name;

        }
      }
      }

      });
    }
      vm.day.serviceConfigsMap[vm.generalServiceType].dayId = vm.day.id;
      vm.day.serviceConfigsMap[vm.generalServiceType].dayDay = vm.day.day;

      vm.day.serviceConfigsMap[vm.generalServiceType].orgId = $localStorage.current_organisation.id;
      vm.day.serviceConfigsMap[vm.generalServiceType].orgName = $localStorage.current_organisation.name
      //filterSafariGroups();
      console.log("onTargetSelected");
      console.log(vm.entity);
      console.log(group);
      vm.dimensionSet = [];
      vm.contracts=[];
      console.log("group.supplierId");
      console.log(group.supplierId);
      console.log(angular.equals(group.supplierId, null));
      if(angular.isDefined(group.supplierId, null) && !angular.equals(group.supplierId, null)){
      ContractsService.getFiltered({ target: vm.safari.dmcId, generalService: vm.entity.generalServiceId, supplier: group.supplierId, year: vm.safari.year, showDeleted: false, showRack: true })
        .$promise.then(function (response) {
          console.log(response);
          angular.forEach(response, function (res) {
            if (angular.equals(res.status, "PUBLISHED") || angular.equals(res.status, "DRAFT")) {
              vm.contracts.push(res);
              console.log("contracts");
              console.log(vm.contracts);
            }
          })
          contractChange(group);
        });
      }
      CountriesService.get().$promise.then(function (response) {
        vm.countries = response;
        var countryId = null;
        angular.forEach(vm.countries, function (country) {
          if (angular.equals(vm.safari.country, country.code)) {
            countryId = country.id;

            MarketsCountriesService.getCountryMarketByOrgId({ id: group.supplierId, countryId: countryId }).$promise
              .then(function (response) {
                console.log(response);
                group.marketId = response.id;
                console.log(group);

              });
          }
        })
      });
      if (group.supplierId > 0) {
        OrganizationsService.getParent({
          id: group.supplierId
        }).$promise.then(function (respose) {
          if (respose.length > 0) {
            group.issuerId = respose[0].id
            group.issuerName = respose[0].name;
            vm.day.serviceConfigsMap[vm.generalServiceType].issuerId = respose[0].id;
            vm.day.serviceConfigsMap[vm.generalServiceType].issuerName = respose[0].name;
          }
        });

      }
      console.groupEnd("supplierSelect");


    }

    function initContractResolutionConfigs() {
      vm.exclusiveContractParameters = [];
      vm.myDim = [];
      DimensionSetsService.getContractResolutionConfig({
        id: vm.entity.serviceId,
        year: vm.entity.year,
        type: vm.entity.objectType
      }).
        $promise.then(function (response) {
          console.log(response);
          angular.forEach(response, function (dim) {
            if (dim.existenceDTOList.length > 0) {
              dim.selectedValue = dim.existenceDTOList[0].id;
            }
          });
          vm.exclusiveContractParameters = response;
          console.log("exclusiveContractParameters");
          console.log(vm.exclusiveContractParameters);
          console.log(vm.exclusiveContractParameters.length);
          if (vm.exclusiveContractParameters.length > 0) {
            removeDimensionType();

          } else {
            vm.myDim = vm.dimensionTypes;
          }
        });
    }

    function loadJson() {
      console.log($stateParams);
      console.log(vm.entity);
      if (angular.equals($stateParams.type, 'ACCOMMODATION') || angular.equals($stateParams.type, 'TRANSPORT')) {
        vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];


      } else if (angular.equals($stateParams.type, 'MEAL')) {
        initMealConfiguration();
        vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];
        //expandSafariServiceGroups(vm.entity);
        console.log("vm.entity");
        getOrgs(vm.entity.generalServiceId);
        console.log(vm.entity);
      }
      else if (angular.equals($stateParams.type, 'OTHER')) {
        // initOtherConfiguration();
        // vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];
        // expandSafariServiceGroups(vm.entity);
        console.log(vm.safari);
        initOTHERConfiguration();
        vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];


        console.log("vm.entity");
        console.log(vm.entity);
      }else if(angular.equals($stateParams.type, 'MISCELLANEOUS')){
        initMiscConfiguration();
        vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];
        console.log("vm.entity");
        console.log(vm.entity);

      }
      else if(angular.equals($stateParams.type, 'LOCATION_MANAGEMENT')){
        console.log("LMS...");
        initLMSConfiguration();
        vm.entity = vm.safari.serviceConfigsMap[$stateParams.type + ''];
        console.log(vm.entity);

        vm.entity.generalServiceId = 6;
        getOrgs(vm.entity.generalServiceId);

      }else if(angular.equals($stateParams.type, 'ANY')){
        console.log("load the general services...");
        GeneralServicesService.query().$promise
        .then(function (response) {
          vm.generalServices = response;
        });
      }
      //
      if (angular.isDefined(vm.entity)) {
        if (angular.equals(vm.entity.generalServiceType, "ACCOMMODATION")) {
          vm.entity.generalServiceId = 1;
          vm.compareRates =  true;
        vm.manual =  false;
        } else if (angular.equals(vm.entity.generalServiceType, "TRANSPORT")) {
          vm.entity.generalServiceId = 8;
          getOrgs(vm.entity.generalServiceId);
        } else {
          console.log(vm.entity);


        }
        GeneralServicesService.filterByGenSvcType({ type: $stateParams.type }).$promise
          .then(function (response) {
            vm.generalServices = response;
            angular.forEach(vm.generalServices, function (genSvc) {
              if (angular.equals($stateParams.type, genSvc.type)) {
                if (vm.entity.safariServiceGroups.length > 0) {
                  //vm.entity.safariServiceGroups[0].generalServiceId = genSvc.id;
                  genSvcChange(vm.entity.safariServiceGroups[0]);
                }
              }
            });
          });
      } else {
        console.log(vm.entity);

      };



    }
    function genSvcChange(group) {
      console.log(group);
      vm.entity.generalServiceId = group.generalServiceId;
      angular.forEach(vm.generalServices, function (genSvc) {
        if (angular.equals(group.generalServiceId, genSvc.id)) {
          group.generalServiceType = genSvc.type;
        }
      });
    }
    function contractChange(group) {
      console.log(group);
      console.log(group.usedContractId);
      vm.dimensionSet = [];
      group.day = vm.day.day;
      angular.forEach(vm.contracts, function (contract) {
        console.log(angular.equals(contract.id, group.usedContractId));
        if (angular.equals(contract.id, group.usedContractId)) {
          console.log(contract);
          group.serviceId = contract.serviceId;
          //contractVersionId
          group.usedVersionId = angular.equals(contract.publishedVersionId, null) ? contract.draftVersionId : contract.publishedVersionId;
          group.usedVersionNumber = angular.equals(contract.publishedVersionId, null) ? contract.draftVersionNumber : contract.publishedVersionName;
          group.dmcId = contract.targetId;
          DimensionSetsService.getSetDimensionExistenciesBySetId({
            id: contract.dimensionSetId,
            year: contract.year,
            seasonGroupId: contract.seasonGroupId
          }, function (data) {
            vm.dimensionSet = data;
            generateInputExistences(group, true);
          });

        }

      });

    }
    function generateInputExistences(group, check) {
      console.log(group);
      group.inputExistences = [];
      angular.forEach(vm.dimensionSet, function (dimension) {

        if (check) {
          console.log(dimension);
          var formValue = null;
          if (!(dimension.existenceDTOList === null || dimension.existenceDTOList === undefined) && dimension.existenceDTOList.length > 0) {
            formValue = dimension.existenceDTOList[0].id;
          }
          dimension.formValue = formValue;
        }

        if (!(dimension.dimensionType === 'ROOM_OCCUPANCY' ||
          dimension.dimensionType === 'SEASON' ||
          dimension.dimensionType === 'DAY_BAND' ||
          dimension.dimensionType === 'AGE_LIMIT' ||
          dimension.dimensionType === 'SPECIAL_DAY')) {

          // console.log("Checking: " + dimension.dimensionName + ", formValue: " + formValue);

          // loop and existence with id same as formValue
          if (!(dimension.formValue === null || dimension.formValue === undefined)) {
            // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + "formValue is set");
            angular.forEach(dimension.existenceDTOList, function (existence) {
              // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              if (existence.id == dimension.formValue) {

                group.inputExistences.push(existence);
                // console.log("MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              } else {

                // console.log("NOT MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
              }
            });
          } else {
            // console.log("Matched and added : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + " - " + existence.name + ", formValue is null");
          }

        }
      });
      ratesResolve(group);

    }
    function compareWithoutType(value1, dimension, group) {
      group.inputExistences = [];

      console.log(group);


      // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + "formValue is set");
      angular.forEach(dimension.existenceDTOList, function (existence) {
        // console.log("Checking : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
        if (existence.id == dimension.formValue) {

          group.inputExistences.push(existence);
          // console.log("MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
        } else {
          // console.log("NOT MATCHED : " + dimension.dimensionName + ", formValue: " + formValue + ", comparing id: " + existence.id + ", matched: " + existence.name);
        }
      });
      console.log(group);

      // console.log("comparing : " + value1 + " and " + value2+", "+same);
      return value1 == dimension.formValue;
    }


    function ratesResolve(group) {
      console.log("reoslving");
      console.log(group);
      //initTripRatesDTO();
      //reset the result
      //vm.tripRateDTOResp = null;
      vm.resolutionDisplayPersonDetails = false;

      //populate vm.tripRateDTO with inputExistences with formValues from dimensions
      //populate vm.tripRateDTO with age limit values

      //resolve rates
      var request = changeData(group);
      request.refresh = true;
      request.persist = false;
      group.totalSupplementValue = null;
      group.totalDiscountValue = null;
      group.totalRateValue = null;
      group.units.safariServices = [];
      request.currencyId=vm.currencyId;
      request.currencyConversionGroupId = vm.safari.currencyConversionGroup.id;
      request.markUpCategoryId = vm.safari.markupCategory.id;
      console.log("request..");
      console.log(vm.safari.currencyConversionGroup.id);
      console.log(vm.safari);
      console.log(request);
      RatesService.resolveContractRates(request,
        function (data) {
          vm.tripRateDTOResp = data;
          group.id =  data.id;
          group.totalSupplementValue = data.totalSupplementValue;
          group.totalDiscountValue = data.totalDiscountValue;
          group.totalRateValue = data.totalRateValue;
          group.totalMarkupValue = data.totalMarkupValue;
          group.units = [];
          vm.day.inclusions = [];
          console.log(vm.day);

          console.log(vm.day.inclusions);
          console.log(group);
          console.log(vm.day);
          $rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day)] =  vm.day;
          $rootScope.$emit('rootScope:emit', $rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day)]); // $rootScope.$on

          $rootScope.$on('rootScope:emit', function (event, data) {
            console.log(data); // 'Emit!'
          });

          angular.forEach(vm.tripRateDTOResp.units, function (unit) {
            unit.numOfObjects = unit.safariObjects.length;
            console.log(unit);
            group.units.push(unit);
            console.log(group.units);
            angular.forEach(group.units, function (groupUnit) {
              if (!angular.equals(groupUnit.safariServices, null) && angular.isDefined(groupUnit.safariServices)) {
                angular.forEach(groupUnit.safariServices, function (grpSfrService) {
                  console.log("grpSfrService");
                  console.log(grpSfrService);
                  angular.forEach(grpSfrService.supplierServiceInclusionList, function (supplierServiceInclusion) {
                    console.log("supplierServiceInclusion");
                    console.log(supplierServiceInclusion);
                    vm.day.inclusions.push(supplierServiceInclusion);
                  })
                })
              }
            });
          })
          // vm.safari.serviceConfigsMap['ACCOMMODATION'].safariServiceGroups =  data;
          // console.log(vm.safari.serviceConfigsMap['ACCOMMODATION'].safariServiceGroups);

        }
      );

    }

    function aggregate() {
      vm.dirty =  true;
      console.log(vm.entity);

    }
    function recompute(){
      if (vm.entity.hasOwnProperty("objectList")) {
        vm.entity["safariObjects"] = vm.entity["objectList"];
        delete vm.entity["objectList"];

        console.log(vm.entity);
      }
      SafarisService.aggregate(vm.entity, function (response) {
        //vm.entity = [];
        console.log(vm.entity);
        var copyEntity =  vm.entity;
        vm.entity = response;
        vm.entity.safariServiceGroups.numOfObjects =  vm.entity.numOfObjects;
        angular.forEach(vm.entity.safariServiceGroups, function(group){
          group.numOfObjects =  copyEntity.numOfObjects;
          group.numOfUnits =  group.units.length;
          angular.forEach(group.units, function(unit){
            unit.numOfObjects= unit.safariSlots.length;
          });

        });
            });
    }
    function changeData(group) {
      console.log("changeData");
      if (group.hasOwnProperty("objectList")) {
        group["safariObjects"] = group["objectList"];
        delete group["objectList"];

        console.log(group);
      }
      if (angular.isDefined(group.units)) {
        console.log(group);
        console.log(angular.isDefined(group.units))
        angular.forEach(group.units, function (unit) {
          console.log(group);
          console.log(angular.isDefined(group.units))
          if (unit.hasOwnProperty("objectList")) {
            unit["safariObjects"] = unit["objectList"];
            unit.rateUsedValue = null;
            unit.supplementUsedValue = null;
            unit.discountUsedValue = null;
            delete unit["objectList"];

            console.log(group);
          }else
            {
              unit.rateUsedValue = null;
              unit.supplementUsedValue = null;
              unit.discountUsedValue = null;
              unit.rateResolvedValue = null;
              console.log(unit);

              console.log(group);
            }
          if (unit.hasOwnProperty("slots")) {
            unit["safariSlots"] = unit["slots"];
            delete unit["slots"];
            angular.forEach(unit.safariSlots, function (slot) {
              if (slot.hasOwnProperty("objectList")) {
                slot["safariObjects"] = slot["objectList"];
                delete slot["objectList"];

                console.log(group);
              }
            })

          }
          if(unit.hasOwnProperty("configs")){
            delete unit.configs;
            console.log(group);

          }
        })

      }
      $rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day)] =  vm.day;
      $rootScope.$emit('rootScope:emit', $rootScope.safariDays[$rootScope.safariDays.indexOf(vm.day)]); // $rootScope.$on

      return group;
      // var title;
      // for(var i = 0; i < data.length; i++){
      //     if(data[i].hasOwnProperty("thumb")){
      //         data[i]["thumbnail"] = data[i]["thumb"];
      //         delete data[i]["thumb"];
      //     }

      //     if(data[i].hasOwnProperty("title")){ //added missing closing parenthesis
      //         title = data[i].title;
      //         data[i].title = '<span class="red">' + title + '</span>';
      //     }
      // }
    }
    function getDimension(dimensionSet, dimensionType) {
      var result = null;
      angular.forEach(dimensionSet, function (dimension) {
        if (dimension.dimensionType === dimensionType) {
          result = dimension;
        }
      });
      return result;
    }


    function initTripRatesDTO() {
      //set start date

      //populate form values at the dto level
      angular.forEach(vm.dimensionSet, function (dimension) {
        var formValue = null;
        if (!(dimension.existenceDTOList === null || dimension.existenceDTOList === undefined) && dimension.existenceDTOList.length > 0) {
          formValue = dimension.existenceDTOList[0].id;
        }
        dimension.formValue = formValue;
      });

      //populate age limits default values
      var ageLimitDimension = getDimension(vm.dimensionSet, 'AGE_LIMIT');
      if (!(ageLimitDimension === null || ageLimitDimension === undefined)) {
        angular.forEach(ageLimitDimension.existenceDTOList, function (existence) {
          console.log(existence.generalName + " - age limit");
          if (existence.generalName === 'Infant') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Child') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Young Adult') {
            existence.formValue = 0;
          } else if (existence.generalName === 'Adult') {
            existence.formValue = 1;
          } else if (existence.generalName === 'Aged') {
            existence.formValue = 0;
          }
        });
      }
    }
    function changeItemsPerPage(strValue) {

      console.log("converting to int: " + strValue);
      vm.itemsPerPage = parseInt(strValue, 10);
    }
    function selectedProperty(org,group){
      console.log("testing...");
      console.log(group);
      console.log(org);
      angular.forEach(vm.rowCollection, function(row){
        if(angular.equals(row.organisationId, org.organisationId)){
          row.selected = !row.selected ;
          //row.usedContractId = org.contractId;
          group.usedContractId = row.contractId;
          //group.usedContractId = 715;
          //group.supplierId =  row.organisationId;
          group.supplierId = row.organisationId;
          //group.supplierId = 158;
          console.log(row);
        }else{
          row.selected = false;

        }
      });
      onTargetSelected(group);
      contractChange(group);
      vm.day.endLocationId =  org.locationId;
      vm.day.endLocationName =  org.locationName;
    }
  }
})();
