(function() {
  "use strict";

  angular
    .module("webApp")
    .config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider.state("safaris", {
        parent: "app",
        url: "/safaris",
        data: {
          authorities: []
        },
        views: {
          "content@": {
            templateUrl: "core/routes/safaris/safaris.html",
            controller: "SafariGroupsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          safariGroups: [
            "$stateParams",
            "$localStorage",
            "SafariGroupsService",
            function($stateParams, $localStorage, SafariGroupsService) {
              return SafariGroupsService
                .getSafariGroups()
                .$promise;
            }
          ]
        }
      })
      .state("safaris.new", {
        parent: "app",
        url: "/{type}/{groupId}/newSafari",
        data: {
          requiresAuthentication: true,
          authorities: ['ROLE_USER'],
          pageTitle: "webApp.package.detail.title"
        },
        views: {
          "content@": {
            templateUrl: 'core/routes/safaris/safari-create-wizard.html',
            controller: 'SafariCreateWizardController',
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "$localStorage",
            function($stateParams, $localStorage) {


              console.log("$stateParams.type");
              console.log($stateParams.type);

              console.log("$stateParams.groupId");
              console.log($stateParams.groupId);

              return {
                country: null,
                countryName: null,
                contentStatus: "DRAFT",
                id: null,
                orgId: $localStorage.current_organisation.id,
                orgName: $localStorage.current_organisation.name,
                dmcId: $localStorage.current_organisation.id,
                dmcName: $localStorage.current_organisation.name,
                pax: 2,
                name: null,
                seasonGroupId: null,
                seasonGroupName: null,
                targetId: null,
                targetName: null,
                type: $stateParams.type,
                uuid: null,
                year: $stateParams.year,
                currencyConfigs: null,
                currencyConversionGroupId: null,
                currencyConversionGroupName: null,
                currencyId: null, // default is USD
                currencyName: null,
                cycle: "ONE_OFF",
                dayModel: "DATE",
                defaultLanguage: "en",
                description: null,
                dimensionSetId: null,
                dimensionSetName: null,
                dropOffPointId: null,
                dropOffPointName: null,
                dstLocationId: null,
                dstLocationName: null,
                endDay: null,
                endTime: null,
                expiryTime: null,
                groupId: $stateParams.groupId == -1 ? null : $stateParams.groupId, //use group id if group id is not 1
                groupName: null,
                locationId: null,
                locationName: null,
                markUpCategoryId: null,
                markUpCategoryName: null,
                markUpConfigs: null,
                maxBookingPax: 0,
                maxPricingPax: 0,
                minBookingPax: 1,
                minPricingPax: 0,
                numOfDays: null,
                numOfNights: null,
                operationEndDate: null,
                operationStartDate: null,
                operationStatus: "PENDING",
                order: 0,
                pickUpPointId: null,
                pickUpPointName: null,
                profile: "OTHER",
                purpose: "HOLIDAY",
                ref: null,
                requirementsPackId: null,
                requirementsPackName: null,
                residence: null,
                safariPackConfigs: null,
                safariTemplate: null,
                scope: "PRIVATE",
                serviceConfigsMap: {}, // the map as part of dto
                serviceConfigs: null, // part of model
                serviceExclusionNotes: null,
                serviceExclusions: null,
                serviceId: null,
                serviceInclusionNotes: null,
                serviceInclusions: null,
                serviceName: null,
                specialNotes: null,
                srcLocationId: null,
                srcLocationName: null,
                startDay: null,
                startTime: null,
                status: "DRAFT",
                travellerGroupConfigs: null,
                travellerGroupId: null,
                travellerGroup: null,
                travellerGroupName: null,
                vehicleGroupConfigs: null,
                virtualTravellers: true,
                virtualVehicles: true,
                safariVehicles: [],
                safariPaxConfigs: [],
              }
            }
          ],
          currencies: [
            '$stateParams',
            'CurrenciesService',
            '$localStorage',
            function(
              $stateParams,
              CurrenciesService,
              $localStorage
            ) {
              return CurrenciesService.query().$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })

      .state('safari-detail', {
        parent: 'safaris',
        url: '/{id}',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'Meal Plans Details'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/safaris/safari-detail.html',
            controller: 'SafariDetailController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: ['$stateParams', 'SafarisService', function($stateParams, SafarisService) {
            return SafarisService.getFull({
              id: $stateParams.id
            }).$promise;
          }],
          days: ['$stateParams', 'SafarisService', function($stateParams, SafarisService) {
            return SafarisService.getDaysBySafariId({
              id: $stateParams.id
            }).$promise;
          }],
          previousState: ["$state", function($state) {
            var currentStateData = {
              name: $state.current.name || 'safaris',
              params: $state.params,
              url: $state.href($state.current.name, $state.params)
            };
            return currentStateData;
          }]
        }
      })
      .state('safari-detail.newLocation', {
        parent: 'safari-detail',
        url: '/{endLocId}/location',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'safari-detail-location.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: 'core/routes/areas/area-dialog.html',
                controller: 'SafariLocationDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      abbreviation: null,
                      brief: null,
                      description: null,
                      typeId: null,
                      contentStatus: 'DRAFT',
                      statusReason: null,
                      typeName: null,
                      parentId: $stateParams.parentId,
                      parentName: null,
                      managerId: $localStorage
                        .current_organisation.id,
                      managerName: $localStorage
                        .current_organisation.name
                    };
                  },
                  setParentVisible: function() {
                    return true;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go('safari-detail', null, {
                    reload: 'safari-detail'
                  });
                },
                function() {
                  $state.go('safari-detail');
                }
              );
          }
        ]
      })

      .state("safari-detail.newService", {
        parent: "safari-detail",
        url: "/{day}/{type}/newService",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.safari-group.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safaris/day/safari-day-service-dialog.html',
                controller: 'SafariDayServiceDialogController',
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: {
                    adjustmentLog: null,
                    alternativeToId: null,
                    alternativeToName: null,
                    amount: null,
                    approvalStatus: "DRAFT",
                    brief: null,
                    configs: null,
                    contentStatus: "DRAFT",
                    createdBy: null,
                    createdOn: null,
                    currencyId: null,
                    currencyName: null,
                    dayDay: null,
                    dayId: null,
                    dayOffset: null,
                    description: null,
                    endTime: null,
                    fixed: true,
                    generalServiceId: null,
                    generalServiceName: null,
                    generalServiceType: $stateParams.type,
                    global: true,
                    happeningDuringId: null,
                    happeningDuringName: null,
                    id: null,
                    importedFromId: null,
                    importedFromName: null,
                    includedInId: null,
                    includedInName: null,
                    issuerId: null,
                    issuerName: null,
                    lastUpdatedBy: null,
                    lastUpdatedOn: null,
                    locationId: null,
                    locationName: null,
                    name: null,
                    notes: null,
                    numOfGroups: null,
                    numOfObjects: null,
                    objectList: null,
                    objectType: "PERSON",
                    optional: true,
                    order: null,
                    orgId: null,
                    orgName: null,
                    paidById: null,
                    paidByName: null,
                    partOfId: null,
                    partOfName: null,
                    perDay: true,
                    perObject: true,
                    perTrip: true,
                    ref: null,
                    requirementsPackId: null,
                    requirementsPackName: null,
                    resolutionLog: null,
                    safariId: null,
                    safariName: null,
                    serviceCategory: "COVERAGE",
                    serviceId: null,
                    serviceName: null,
                    specialNotes: null,
                    startTime: null,
                    statusReason: null,
                    supplierId: null,
                    supplierName: null,
                    timeInHours: null,
                    timeStrict: true,
                    totalAdjustmentValue: null,
                    totalDiscountValue: null,
                    totalGeneralServiceCost: null,
                    totalMarkupValue: null,
                    totalRackValue: null,
                    totalRateValue: null,
                    totalSupplementValue: null,
                    uuid: null,
                    visible: true,
                    safariServiceGroups: []
                  }

                }
              })
              .result
              .then(function() {
                $state.go("safari-detail", null, {
                  reload: "safari-detail"
                });
              }, function() {
                $state.go("^");
              })
          }
        ]
      }).state('safari-detail-notes', {
        parent: 'safari-detail',
        url: '/{dayId}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/safaris/day/safari-day-notes-dialog.html',
                controller: 'SafariDayNotesDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: ['SafarisService',
                    function(SafarisService) {
                      console.log($stateParams);
                      console.log($state.params);
                      console.log($state);
                      return SafarisService
                        .getDayById({
                          id: $stateParams.dayId
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function() {
                $state.go('safari-detail', null, {
                  reload: 'safari-detail'
                });
              }, function() {
                $state.go('safari-detail');
              });
          }
        ]
      });
  }
})();
