(function() {
  'use strict';

  angular
    .module('webApp')
    .controller('SafariCreateWizardController', SafariCreateWizardController);

  SafariCreateWizardController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$state', 'entity', 'currencies', 'SafarisService', 'OrganizationsService', 'SeasonGroupsService', 'LocationsService', 'CurrenciesService', 'safariConstants', 'CurrencyService', 'MarkupCategoriesService', 'WelcomePacksService', 'CountriesService', 'SafariGroupsService', 'DimensionSetsService', 'SafarisUtil', 'GeneralVehicleTypesService', '$log'];

  function SafariCreateWizardController($timeout, $scope, $stateParams, $localStorage, $state, entity, currencies, SafarisService, OrganizationsService, SeasonGroupsService, LocationsService, CurrenciesService, safariConstants, CurrencyService, MarkupCategoriesService, WelcomePacksService, CountriesService, SafariGroupsService, DimensionSetsService, SafarisUtil, GeneralVehicleTypesService, $log) {
    var vm = this;

    vm.safari = entity;
    // $log.log("vm.safari");
    // $log.log(vm.safari);
    $log.log("test");
    vm.minDate = new Date();
    vm.dateOptions = {
      formatYear: 'yyyy',
      startingDay: 1,
      minDate: vm.minDate
    }
    vm.save = save;
    vm.currencies = currencies;
    $localStorage.safari = vm.safari; // we need to keep saving.....

    //initialization
    vm.packageYears = [2019, 2018, 2017, 2016, 2015];
    vm.packageTypes = safariConstants.packageTypes
    vm.seasonGroups = [];
    vm.organizations = [];
    vm.currencies = [];
    vm.years = [];
    vm.supportedLanguages = safariConstants.supportedLanguages;
    vm.travellerProfiles = safariConstants.travellerProfile;
    vm.safariVehicleCategory = safariConstants.safariVehicleCategory;
    vm.purposes = safariConstants.purposeOfVisit;
    vm.safariCycles = safariConstants.safariCycle;
    vm.dayModels = safariConstants.dayModel;
    vm.safariScopes = safariConstants.safariScope;
    vm.travellerCategories = safariConstants.travellerCategory;
    vm.genders = safariConstants.genders;
    vm.allocationModes = safariConstants.allocationModes;
    vm.selectedTraveller = null;
    vm.selectedVehicle = null; // default pricing gap
    vm.safari.year = moment(new Date(vm.startDay), "YYYY-MM-DD").year();

    vm.showNext = false;

    ////declare functions
    vm.initSeasonGroups = initSeasonGroups;
    vm.onYearChange = onYearChange;
    vm.getLocation = getLocation;
    vm.onEndDestinationSelected = onEndDestinationSelected;
    vm.onstartDestinationSelected = onstartDestinationSelected;
    vm.onPackageTypeChange = onPackageTypeChange;
    vm.onLocationSelected = onLocationSelected;
    vm.onSrcLocationSelected = onSrcLocationSelected;
    vm.onDstLocationNameSelected = onDstLocationNameSelected;
    vm.getPickLocations = getPickLocations;
    vm.search = search;
    vm.onCountryChange = onCountryChange;
    vm.generateClientInfo = generateClientInfo;
    vm.onChangeGroupDetails = onChangeGroupDetails;
    vm.generatePersonsInfo = generatePersonsInfo;
    vm.selectTraveller = selectTraveller;
    vm.selectVehicle = selectVehicle;
    vm.showRefreshPersonsWarning = showRefreshPersonsWarning;
    vm.generateAccommodationConfiguration = generateAccommodationConfiguration;
    vm.generateTranportConfiguration = generateTranportConfiguration;
    vm.initAccommodationConfiguration = initAccommodationConfiguration;
    vm.initTranportConfiguration = initTranportConfiguration;
    vm.expandSafariServiceGroups = expandSafariServiceGroups; //used in service configs when split is done
    vm.expandUnits = expandUnits; //used in service configs when number of units change
    vm.expandSlots = expandSlots; //used in the service configs when the slots change
    vm.validGroupObjectsTotal = validGroupObjectsTotal;
    vm.validUnitObjectsTotal = validUnitObjectsTotal;
    vm.validConfigsObjectsTotal = validConfigsObjectsTotal;
    vm.autoAllocateObjects = autoAllocateObjects;
    vm.expandSafariPAXConfigs = expandSafariPAXConfigs;
    vm.onStartDateTimeChange = onStartDateTimeChange;
    vm.onEndDateTimeChange = onEndDateTimeChange;
    vm.getOrgs = getOrgs;
    vm.onTargetSelected = onTargetSelected;
    vm.pricingPAXgap = 1;
    vm.filterSafariGroups = filterSafariGroups;
    vm.startDayChange = startDayChange;
    vm.endDayChange = endDayChange;
    vm.travellerPickUpSelected = travellerPickUpSelected;
    vm.travellerDropOffSelected = travellerDropOffSelected;
    vm.toUTCDate = toUTCDate;
    vm.gotoLink = gotoLink;
    vm.changeWelcomePack = changeWelcomePack;
    vm.changeCurrency = changeCurrency;
    vm.changeMarkUp = changeMarkUp;
    vm.getPartnerOrgs = getPartnerOrgs;
    vm.onPartnerSelected = onPartnerSelected;
    vm.changePax = changePax;
    vm.getVehicleTypeName = getVehicleTypeName;
    vm.setSlots = setSlots;
    vm.bookingNameChange = bookingNameChange;
    vm.grpArrivalFlightNo = grpArrivalFlightNo;
    vm.grpDeprtFlightNo = grpDeprtFlightNo;
    vm.grpArrivalNotes = grpArrivalNotes;
    vm.grpArrivalTicketNo = grpArrivalTicketNo;
    vm.grpDeprtNotes = grpDeprtNotes;
    vm.grpDeprtTicketNo = grpDeprtTicketNo;
    vm.confirmConfig = confirmConfig;
    //call functions needed to initialize data
    initSteps();
    initDefaultsBySafariType();
    initSeasonGroups();
    initCurrencies();
    initYears();
    initEntities();
    initGeneralVehicleTypes();
    getPartnerOrgs(3);


    //function definition
    function onStartDateTimeChange() {
      console.log("start date and time: " + vm.startDay);
    }


    function onEndDateTimeChange() {
      console.log("end date and time: " + vm.safari.endDay + vm.safari.endTime);


    }

    function expandSafariPAXConfigs() {
      console.log("invoked expand safari pax configs");
      vm.safari.safariPAXConfigs = SafarisUtil.expandSafariPAXConfigs(vm.safari.minPricingPax, vm.safari.maxPricingPax, vm.pricingPAXgap);
    }

    function generateTranportConfiguration(configs) {

      //add the vehicles...
      generateVehiclesInfo(configs);

      //auto assign people to vehicles
      autoAllocateObjects(configs);

      //hide the modal
      angular.element('#configureTransport').modal('hide');
    }


    function generateAccommodationConfiguration(configs) {
      //auto assign people
      autoAllocateObjects(configs);

      //hide the modal
      angular.element('#configureAccommodation').modal('hide');
    }

    // check if the traveller group pax and travellers list count mismatch
    function showRefreshPersonsWarning() {
      return SafarisUtil.travellersGroupPaxTravellersMismatch(vm.safari);
    }

    function onChangeGroupDetails() {
      //we will do some work here to ensure that the safari info is upto date...
      vm.safari.name = vm.safari.travellerGroup.name;
      vm.safari.pax = vm.safari.travellerGroup.pax;
      vm.safari.country = vm.safari.travellerGroup.country;
      vm.safari.residence = vm.safari.travellerGroup.residence;
      vm.safari.profile = vm.safari.travellerGroup.profile;
      vm.safari.purpose = vm.safari.travellerGroup.purpose;
      vm.safari.description = vm.safari.travellerGroup.description;
      // angular.forEach(vm.safari.travellerGroup.travellers, function(traveller){
      //   traveller.country = vm.safari.travellerGroup.country;
      //   traveller.residence = vm.safari.travellerGroup.residence;
      // });

      angular.forEach(vm.countries, function(country) {
        if (vm.safari.travellerGroup.country == country.code) {
          vm.safari.travellerGroup.residence = country;
          vm.safari.residence = vm.safari.travellerGroup.residence;
          vm.safari.countryName = country.name;
          vm.safari.country = vm.safari.travellerGroup.country;
          console.log(vm.safari);
          angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
            traveller.country = vm.safari.travellerGroup.country;
            traveller.residence = vm.safari.travellerGroup.residence;
          });
        }
      });

      //hide the modal
      angular.element('#editGroupDetails').modal('hide');
    }


    function selectTraveller(record) {
      //set the selected row
      vm.selectedTraveller = record;
      console.log(vm.selectTraveller);
    }

    function selectVehicle(record) {
      //set the selected row
      vm.selectedVehicle = record;
    }

    function changeWelcomePack() {
      angular.forEach(vm.welcomePacks, function(welcomePack) {
        if (angular.equals(vm.safari.requirementsPackId, welcomePack.id)) {
          vm.safari.requirementsPackId = welcomePack.id;
          vm.safari.requirementsPackName = welcomePack.name;
        }
      });
    }

    function changeMarkUp() {
      angular.forEach(vm.markUpCats, function(markUpCat) {

        if (angular.equals(vm.safari.markUpCategoryId, markUpCat.id)) {
          vm.safari.markUpCategoryId = markUpCat.id;
          vm.safari.markUpCategoryName = markUpCat.name;
        }
      });
    }

    function initEntities() {
      //markup categories
      MarkupCategoriesService.getByOrganisation({
        id: $localStorage.current_organisation.id
      }).$promise.then(function response(result) {
        vm.markUpCats = result;
        angular.forEach(vm.markUpCats, function(markUpCat) {

          if (markUpCat.isDefault) {
            vm.safari.markUpCategoryId = markUpCat.id;
            vm.safari.markUpCategoryName = markUpCat.name;
          }
        });
      });

      //welcome packs
      WelcomePacksService.getByOrgId({
        id: $localStorage.current_organisation.id
      }).$promise.then(function response(result) {
        vm.welcomePacks = result;
        angular.forEach(vm.welcomePacks, function(welcomePack) {
          if (welcomePack.isDefault) {
            vm.safari.requirementsPackId = welcomePack.id;
            vm.safari.requirementsPackName = welcomePack.name;
          }
        });
      });


      //currencies
      CurrencyService.getByOrganization({
        id: $localStorage.current_organisation.id
      }).$promise.then(function response(result) {
        vm.currencyGroups = result;
        angular.forEach(vm.currencyGroups, function(group) {

          if (group.usage == "PRODUCTS") {
            vm.safari.currencyConversionGroupId = group.id;
          }
        })
      });

      //countries
      CountriesService.query().$promise.then(function response(result) {
        vm.countries = result;
      });
      //

    }

    function initDefaultsBySafariType() {
      console.log("defauls....")
      console.log(vm.safari.type === "FIT");
      console.log(vm.safari.type);
      // if ($stateParams.id != null) {
      //   vm.safari.serviceId = $stateParams.id;
      // } else {
      //   vm.safari.serviceId = 694;

      // }
      if (!angular.equals($stateParams.groupId, null)) {
        console.log("fetching group,,,");
        vm.safari.groupId = $stateParams.groupId;
        SafariGroupsService.getById({
          id: $stateParams.groupId
        }).$promise.then(function(result) {
          vm.parentGroup = result;
          vm.safari.year = vm.parentGroup.year;
          vm.safari.type = vm.parentGroup.safariType;
          console.log("fetching group,,,");
          console.log(vm.parentGroup);
          if (angular.isDefined(vm.parentGroup.configs) && !angular.equals(vm.parentGroup.configs, null)) {
            var configs = angular.fromJson(vm.parentGroup.configs);
            console.log(configs);
            console.log(configs.targetId)

            vm.safari.targetId = configs.targetId;
            vm.safari.targetName = configs.targetName;

            vm.safari.locationId = configs.locationId;
            vm.safari.locationName = configs.locationName;
            vm.safari.defaultLanguage = configs.defaultLanguage;
            filterSafariGroups();
          }

        });
        changePax();
      }
      console.log("vm.safari.type..");
      console.log(vm.safari.type);
      if (vm.safari.type === "FIT") {
        vm.fit = true;
        vm.safari.cycle = "OTHER";
        vm.safari.virtualVehicles = false;
        vm.safari.virtualTravellers = false
        vm.safari.minBookingPax = vm.safari.pax;
        vm.safari.maxBookingPax = vm.safari.pax;
      } else if (vm.safari.type === "SET_DEPARTURE") {
        console.log("SET_DEPART")
      } else if (vm.safari.type === "PRIVATE_DEPARTURE") {
        console.log("PRIVATE_DEPARTURE")
      } else if (vm.safari.type === "EXCURSION") {
        console.log("EXCURSION")
      } else if (vm.safari.type === "EXTENSION") {
        console.log("EXTENSION")
      } else if (vm.safari.type === "GIT") {
        console.log("GIT")
      }

    }

    function onCountryChange(code) {
      angular.forEach(vm.countries, function(country) {
        if (vm.safari.countryName == country.name) {
          vm.safari.residence = country.code;
          vm.safari.country = country.code;
          vm.safari.travellerGroup.country = vm.safari.country;
          vm.safari.travellerGroup.residence = vm.safari.residence;
          console.log(vm.safari);
          angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
            traveller.country = vm.safari.travellerGroup.country;
            traveller.residence = vm.safari.travellerGroup.residence;
          });
        }
      });
    }

    function changePax() {
      console.log("changing pax..");
      vm.safari.travellerGroup = [];
      generateClientInfo();
      vm.showNext = false;
    }

    function generateClientInfo() {
      SafarisUtil.generateTravellerGroup(vm.safari, vm.startDay, vm.endDay);
    }

    function generatePersonsInfo() {
      //generate the travelllers information
      SafarisUtil.generateTravellersInfo(vm.safari);
    }

    function initAccommodationConfiguration() {
      initAccommodationConfiguration(vm.safari);
    }

    function initTranportConfiguration() {
      initTranportConfiguration(vm.safari);
    }

    function expandSafariServiceGroups(configs) {
      SafarisUtil.expandSafariServiceGroups(configs);
    }

    function generateVehiclesInfo(configs) {
      //init or reset
      vm.safari.safariVehicles = SafarisUtil.generateVehiclesInfo(configs, vm.safari, vm.startDay, vm.endDay, vm.safari.safariVehicles);
    }

    function expandUnits(generalService, type) {
      if (angular.equals(type, 'VEHICLE')) {
        console.log("load gen vehicles");
        GeneralVehicleTypesService.query().$promise.then(function(response) {
          vm.generalVehicleTypes = response;
          console.log(vm.generalVehicleTypes);
        })
      }
      SafarisUtil.generateUnits(generalService, type);

    }

    function expandSlots(unit, type) {
      SafarisUtil.generateSlots(unit, type);
    }

    // validate configs total against safari pax
    function validConfigsObjectsTotal(configs) {
      return SafarisUtil.validConfigsObjectsTotal(configs, vm.safari);
    }

    // validate groups total against configs pax
    function validGroupObjectsTotal(configs) {
      return SafarisUtil.validGroupObjectsTotal(configs);
    }

    // validate units total against group pax
    function validUnitObjectsTotal(generalService) {
      return SafarisUtil.validUnitObjectsTotal(generalService);
    }

    //auto allocate people to the configs
    function autoAllocateObjects(configs) {
      //invoke safari utils
      SafarisUtil.autoAllocateObjects(configs, vm.safari);
    }


    $timeout(function() {
      angular.element('.form-group:eq(1)>input').focus();
    });

    vm.openFrom = function() {
      vm.openedFrom = true;
    };
    vm.openFromTime = function() {
      vm.openedFromTime = true;
    };

    vm.openTo = function() {
      vm.openedTo = true;
    };
    vm.openToTime = function() {
      vm.openedToTime = true;
    };

    function onPackageTypeChange(type) {
      console.log("package type change" + " " + type)
      if (type === "INDEPENDENT_TOUR") {
        vm.fit = true;
      } else {
        vm.fit = false;
      }
    }

    function getLocation(val) {
      vm.searchedProperty = false;
      if (vm.safari.type != "FIT") {
        return LocationsService
          .query({
            name: val,
            types: [1, 2, 3, 4, 5, 6, 7, 18, 19, 20, 21, 22, 23, 25]
          })
          .$promise
          .then(function(results) {
            vm.searchedItems = results;
            return results.map(function(item) {

              return item;
            })
          });
      } else {
        return LocationsService
          .query({
            name: val,
            types: [2, 3, 4, 5, 19]
          })
          .$promise
          .then(function(results) {
            vm.searchedItems = results;
            return results.map(function(item) {

              return item;
            })
          });
      }
    }

    function getPickLocations(val) {
      vm.searchedProperty = false;
      return LocationsService
        .query({
          name: val,
          types: [4, 5, 6, 7, 8, 9, 12, 13, 20, 21, 22, 23, 26]
        })
        .$promise
        .then(function(results) {
          vm.searchedItems = results;
          return results.map(function(item) {

            return item;
          })
        });
    }

    function onLocationSelected($item, $model) {

      vm.safari.locationId = $item.id;
      vm.safari.locationName = $item.name;

    }

    function travellerPickUpSelected($item, $model) {

      vm.safari.travellerGroup.travelDetails.arrival.pickUpLocId = $item.id;
      vm.safari.travellerGroup.travelDetails.arrival.pickUpLocName = $item.name;
      console.log(vm.safari.travellerGroup.travellers);
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        console.log(vm.safari.travellerGroup.travellers.travelDetails);
        traveller.travelDetails.arrival.pickUpLocId = $item.id;
        traveller.travelDetails.arrival.pickUpLocName = $item.name;
      });
      console.log(vm.safari.travellerGroup.travellers);

    }

    function travellerDropOffSelected($item, $model) {

      vm.safari.travellerGroup.travelDetails.departure.dropOffLocId = $item.id;
      vm.safari.travellerGroup.travelDetails.departure.dropOffLocName = $item.name;
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.departure.dropOffLocId = $item.id;
        traveller.travelDetails.departure.dropOffLocName = $item.name;
      });
      console.log(vm.safari.travellerGroup.travellers);


    }

    function onEndDestinationSelected($item, $model) {
      vm.safari.dropOffPointId = $item.id;
      vm.safari.dropOffPointName = $item.name;
      travellerDropOffSelected($item, $model);


    }

    function onstartDestinationSelected($item, $model) {
      vm.safari.pickUpPointId = $item.id;
      vm.safari.pickUpPointName = $item.name;
      travellerPickUpSelected($item, $model);
    }
    //onDstLocationNameSelected

    function onDstLocationNameSelected($item, $model) {
      vm.safari.dstLocationId = $item.id;
      vm.safari.dstLocationName = $item.name;
    }

    function onSrcLocationSelected($item, $model) {

      vm.safari.srcLocationId = $item.id;
      vm.safari.srcLocationName = $item.name;

    }

    function initCurrencies() {

      CurrenciesService.query().$promise.then(function(response) {
        angular.forEach(response, function(currency) {
          if (angular.equals(vm.safari.currencyId, currency.id)) {
            vm.safari.currencyName = currency.name;
          }
          vm.currencies.push(currency);
        });
      });
    }

    function changeCurrency() {
      angular.forEach(vm.currencies, function(currency) {
        if (angular.equals(vm.safari.currencyId, currency.id)) {
          vm.safari.currencyName = currency.name;
        }
      });
    }

    function save() {

      //setAttributes();

      //copy
      var toSave = angular.copy(vm.safari);


      //}

      //set endDay, time and operation end date
      if (vm.endDay != null) {
        //var endDateTimeParts = (vm.endDateTime + "").split(" ");
        vm.safari.endDay = moment(new Date(vm.endDay), "YYYY-MM-DD").format('YYYY-MM-DD');
        vm.safari.endTime = moment(new Date(vm.endTime), "HH:mm").format('HH:mm');
      }


      //convert traveller group travelDetails to string
      if (vm.safari.travellerGroup) {
        // "eta": moment(formatDate(startDay), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),
        //"etd": moment(formatDate(endDateTime), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),

        var eta = moment(new Date(vm.startDay), "YYYY-MM-DD").format('YYYY-MM-DD');
        var etd = moment(new Date(vm.endDay), "YYYY-MM-DD").format('YYYY-MM-DD');

        toSave.travellerGroup.eta = moment(formatDate(toSave.travellerGroup.eta), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),
          toSave.travellerGroup.etd = moment(formatDate(toSave.travellerGroup.etd), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),
          // "eta": moment(formatDate(startDay), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),
          // "etd": moment(formatDate(endDateTime), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),

          toSave.travellerGroup.travelDetails = angular.toString(vm.safari.travellerGroup.travelDetails);
        angular.forEach(toSave.travellerGroup.travellers, function(traveller) {
          traveller.eta = moment(formatDate(traveller.eta), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');
          traveller.etd = moment(formatDate(traveller.etd), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');

          var copy = angular.copy(traveller.travelDetails);
          console.log(copy);
          traveller.travelDetails = angular.toJson(copy);
          console.log(traveller.travelDetails);
        });
      }

      //convert vehicle configs to string

      if (toSave.safariVehicles) {
        angular.forEach(toSave.safariVehicles, function(vehicle) {
          vehicle.eta = moment(formatDate(vehicle.eta), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');
          vehicle.etd = moment(formatDate(vehicle.etd), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');

          var copy = angular.copy(vehicle.configs);
          console.log(copy);
          vehicle.configs = angular.toJson(copy);
          console.log(vehicle.configs);
        });
      }


      console.log("to save");
      console.log(toSave);

      vm.isSaving = true;
      SafarisService.saveFull(toSave, onSaveSuccess, onSaveError);

    }

    function onSaveSuccess(result) {
      vm.isSaving = false;
      console.log("result");
      console.log(result);
      $state.go("safari-detail", {
        id: result.id
      }, {
        reload: true
      })
    }

    function onSaveError() {
      vm.isSaving = false;
      console.log("failed..")

    }

    function initYears() {
      //endDay
      vm.startDay = new Date();
      // vm.startTime = new Date();

      vm.safari.startDay = moment(new Date(vm.startDay), "YYYY-MM-DD").format('YYYY-MM-DD');
      vm.safari.startTime = moment(new Date(vm.startTime), "HH:mm").format('HH:mm');

      var currentYear = new Date().getFullYear();
      var totalYears = 10; //number of years visible
      var numberOfYearsBack = 3; //how many years back
      for (var i = 1; i <= totalYears; i++) {
        var year = currentYear + i - numberOfYearsBack;
        vm.years.push(year);
      }
    }

    function search(year, type) {
      console.log("redirecting..");
      console.log(type);
      console.log(year);
      $state.go($state.current, {
        statePackageType: type,
        year: year
      }, {
        reload: true
      });
      //      url: "/packages/{statePackageType}/{year}/{locationId}/{groupId}/{countryId}/{targetId}/",

    }

    function initSteps() {

      vm.currentStep = 1;
      vm.steps = [{
          step: 1,
          name: "Basic Info",
          templateUrl: "core/routes/safaris/steps/basic-info.html",
          visible: true
        },
        {
          step: 2,
          name: "Additional Info",
          templateUrl: "core/routes/safaris/steps/additional-info.html",
          visible: true
        },
        {
          step: 3,
          name: "Client Info",
          templateUrl: "core/routes/safaris/steps/client-info.html",
          visible: true
        },
        {
          step: 4,
          name: "Service Configs",
          templateUrl: "core/routes/safaris/steps/service-info.html",
          visible: true
        },
        {
          step: 5,
          name: "Preview",
          templateUrl: "core/routes/safaris/steps/preview-info.html",
          visible: true
        },
      ];
      vm.user = {};

      //Functions

    }
    vm.gotoStep = function(newStep) {
      vm.currentStep = newStep;
    }

    vm.getStepTemplate = function() {
      for (var i = 0; i < vm.steps.length; i++) {
        if (vm.currentStep == vm.steps[i].step) {
          return vm.steps[i].templateUrl;
        }
      }
    }
    vm.goto = function(step) {
      console.log(step);
      vm.currentStep = step;
      vm.getStepTemplate();
    }

    function initSeasonGroups() {
      SeasonGroupsService.getByOranizationId({
        id: $localStorage.current_organisation.id
      }).$promise.then(function(response) {
        angular.forEach(response, function(obj) {
          vm.seasonGroups.push(obj);
        });
      });
    }

    function onYearChange() {
      vm.seasonGroups = [];
      SeasonGroupsService.getByOranizationIdAndYear({
        id: $localStorage.current_organisation.id,
        year: vm.safari.year
      }).$promise.then(function(response) {
        angular.forEach(response, function(obj) {
          vm.seasonGroups.push(obj);
        });
      });
    }

    function setAttributes() {
      // targetName : null,
      // seasonGroupName : null,
      // chainName : null
      if (vm.safari.chainId !== null || vm.safari.chainId !== "undefined") {
        angular.forEach(vm.seasonGroups, function(season) {
          if (season.id == vm.safari.seasonGroupId) {
            vm.safari.seasonGroupName = season.name;
          }
        })
      }
    }
    //search orgs

    function startDayChange() {

      vm.safari.startDay = moment(new Date(vm.startDay), "YYYY-MM-DD").format('YYYY-MM-DD');
      vm.safari.startTime = moment(new Date(vm.startTime), "HH:mm").format('HH:mm');
      vm.safari.year = moment(vm.startDay, "YYYY-MM-DD").year();
      console.log(vm.safari.startDay);
      console.log(vm.safari.startTime);
      vm.safari.operationStartDate = vm.safari.startDay;
      vm.safari.travellerGroup.eta = moment(new Date(vm.startDay), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');

      filterSafariGroups();
      onYearChange();

    }

    function endDayChange() {

      vm.safari.endDay = moment(new Date(vm.endDay), "YYYY-MM-DD").format('YYYY-MM-DD');
      vm.safari.endTime = moment(new Date(vm.endTime), 'HH:mm').format('HH:mm');

      console.log(vm.safari.endDay)
      console.log(vm.safari.endTime)

      var start = moment(vm.safari.startDay, "YYYY-MM-DD");
      var end = moment(vm.safari.endDay, "YYYY-MM-DD");

      console.log(start);
      console.log(end);



      vm.safari.operationEndDate = vm.safari.endDay;
      vm.safari.travellerGroup.etd = moment(new Date(vm.endDay), "YYYY-MM-DD").format('YYYY-MM-DD');

      //Difference in number of days
      vm.safari.numOfDays = Math.trunc(moment.duration(end.diff(start)).asDays() + 1);
      //    vm.safari.numOfDays = vm.endDateTime.diff(vm.startDay, 'days')+1;

      vm.safari.numOfNights = vm.safari.numOfDays - 1;
      console.log(vm.safari.numOfDays + " : " + vm.safari.numOfNights) // =2
      console.log(vm.safari.endTime);
      console.log(vm.safari.travellerGroup);

    }

    function formatDate(startDate) {
      if (startDate !== null && startDate !== 'Invalid date') {
        var theDate = new Date(startDate.toString());
        return (
          theDate.getFullYear() +
          '-' +
          (theDate.getMonth() + 1) +
          '-' +
          theDate.getDate() +
          ' ' +
          theDate.getHours() +
          ':' +
          theDate.getMinutes() +
          ':' +
          theDate.getSeconds() +
          '.' +
          theDate.getMilliseconds()
        );
      } else {
        return '';
      }
    }

    function filterSafariGroups() {
      vm.safariServiceGroups = [];
      SafariGroupsService.getFiltered({
        orgId: vm.safari.orgId,
        type: vm.safari.safariType,
        year: vm.safari.year,
        target: vm.safari.targetId
      }).$promise.then(function(results) {
        angular.forEach(results, function(folder) {
          if (folder.terminal && angular.equals(folder.safariType, "FIT")) {
            console.log(folder);

            //        vm.safariServiceGroups = results;
            vm.safariServiceGroups.push(folder);

          }
        })
        console.log("vm.safariServiceGroups");
        console.log(vm.safariServiceGroups);
      })
    }

    function initGeneralVehicleTypes() {
      console.log("load gen vehicles");
      GeneralVehicleTypesService.query().$promise.then(function(response) {
        vm.generalVehicleTypes = response;
        console.log(vm.generalVehicleTypes);
      });
    }

    function getVehicleTypeName(typeId) {
      console.log("load gen vehicles" + typeId);
      var typeName = null;
      angular.forEach(vm.generalVehicleTypes, function(vehicleType) {
        console.log(angular.equals(typeId, vehicleType.id));
        if (angular.equals(typeId, vehicleType.id)) {
          typeName = vehicleType.name

        }
      });
      return typeName;
    }

    function setSlots(unit, vehicleType) {
      unit.numOfSlots = vehicleType.capacity;
    }

    function toUTCDate(date) {
      var datez = new Date(date)
      // "etd": moment(formatDate(endDateTime), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm'),

      return moment(formatDate(date), "YYYY-MM-DD hh:mm").format('YYYY-MM-DD hh:mm');
    }

    function gotoLink(link) {
      console.log("going to linkk");
      console.log(link);
      $state.go("safari-group-detail", {
        groupId: $stateParams.groupId,
        year: $stateParams.year
      }, {
        reload: true
      });
    }

    function getOrgs(val, id) {
      return OrganizationsService
        .filterByGeneralService({
          name: val,
          id: id
        })
        .$promise
        .then(function(results) {
          return results.map(function(item) {
            return item;
          })
        });
    }



    function onTargetSelected($item, $model, record) {
      console.log($item);
      console.log(record);
      record.supplierId = $item.id;
      record.supplierName = $item.name;
      if (record.supplierId > 0) {
        OrganizationsService.getParent({
          id: record.supplierId
        }).$promise.then(function(respose) {
          if (respose.length > 0) {
            record.issuerId = respose[0].id
            record.issuerName = respose[0].name;
          }
        });
        console.log(record);

      }
    }

    function getPartnerOrgs(type) {
      return OrganizationsService
        .getOrganizationsByType({
          type: type
        })
        .$promise
        .then(function(results) {
          //   return results.map(function (item) {
          //     return item;
          //   })
          vm.partners = results;
        });

      console.log(item);
    }



    function onPartnerSelected(partners) {
      console.log(partners);
      angular.forEach(vm.partners, function(partner) {
        if (angular.equals(partner.name, vm.safari.targetName)) {

          vm.safari.targetId = partner.id;

        }

      });



      // vm.safari.targetName = partners.name;
      filterSafariGroups();

    }

    function bookingNameChange() {
      vm.safari.travellerGroup.name = vm.safari.name;
    }

    function grpArrivalFlightNo() {
      //travelDetails.departure.flightNo
      console.log(vm.safari.travellerGroup.travelDetails.arrival.flightNo);
      console.log("grpArrivalFlightNo");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.arrival.flightNo = vm.safari.travellerGroup.travelDetails.arrival.flightNo;
      });
      console.log(vm.safari.travellerGroup.travellers);

    }

    function grpDeprtFlightNo() {
      console.log("grpDeprtFlightNo");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.departure.flightNo = vm.safari.travellerGroup.travelDetails.arrival.flightNo;
      });
    }

    function grpArrivalTicketNo() {
      console.log("grpArrivalTicketNo");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.arrival.ticketNo = vm.safari.travellerGroup.travelDetails.arrival.ticketNo;
      });
    }

    function grpDeprtTicketNo() {
      console.log("grpDeprtTicketNo");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.departure.ticketNo = vm.safari.travellerGroup.travelDetails.arrival.ticketNo;
      });
    }

    function grpArrivalNotes() {
      console.log("grpArrivalNotes");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.arrival.notes = vm.safari.travellerGroup.travelDetails.arrival.notes;
      });
    }

    function grpDeprtNotes() {
      console.log("grpDeprtNotes");
      angular.forEach(vm.safari.travellerGroup.travellers, function(traveller) {
        traveller.travelDetails.departure.notes = vm.safari.travellerGroup.travelDetails.departure.notes;
      });
    }

    function confirmConfig() {
      console.log("confirming...");
      vm.showNext = true;
    }


  }
})();
