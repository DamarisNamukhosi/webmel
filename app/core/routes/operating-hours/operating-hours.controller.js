(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OperatingHoursController', OperatingHoursController);

        OperatingHoursController.$inject = ['$scope', '$location', '$state', 'operatingHours', '$localStorage', 'URLS'];

    function OperatingHoursController ($scope, $location, $state, operatingHours, $localStorage, URLS) {
        var vm = this;

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        vm.isAuthenticated = null;

        vm.records = operatingHours;

        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);

    }
})();
