(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OperatingHourDeleteDialogController',OperatingHourDeleteDialogController);

        OperatingHourDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'OperatingHoursService'];

    function OperatingHourDeleteDialogController($uibModalInstance, entity, OperatingHoursService) {
        var vm = this;

        vm.operatingHour = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            console.log('about to delete : '+id);
            OperatingHoursService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
