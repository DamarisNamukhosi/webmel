(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('operating-hours', {
            parent: 'app',
            url: '/operating-hours',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'operating-hours'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/operating-hours/operating-hours.html',
                    controller: 'OperatingHoursController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
              operatingHours: ['$stateParams', '$localStorage', 'OperatingHoursService', function ($stateParams, $localStorage, OperatingHoursService ) {
                return OperatingHoursService.getOperatingHoursByOrganizaitonId  ({id: $localStorage.current_organisation.id}).$promise;
              }]
            }
        })
        .state('operating-hours.new', {
            parent: 'operating-hours',
            url: '/new',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'profsApp.professional.detail.title'
            },
            onEnter: ['$stateParams', '$state', '$localStorage', '$uibModal', function($stateParams, $state, $localStorage, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/operating-hours/operating-hour-dialog.html',
                    controller: 'OperatingHourDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id : null,
                                uuid : null,
                                name : null,
                                openingTime : null,
                                operatingHoursType : null,
                                organisationId : $localStorage.current_organisation.id,
                                organisationName : null
                              };
                        }
                    }
                }).result.then(function() {
                    $state.go('operating-hours', null, { reload: 'operating-hours' });
                }, function() {
                    $state.go('operating-hours');
                });
            }]
        })
        .state('operating-hour-detail', {
            parent: 'operating-hours',
            url: '/operating-hours/{id}',
            data: {
                requiresAuthentication: true,
                authorities: [],
                pageTitle: 'Operating Hours Details'
            },
            views: {
                'content@': {
                    templateUrl: 'core/routes/operating-hours/operating-hour-detail.html',
                    controller: 'OperatingHourDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'OperatingHoursService', function($stateParams, OperatingHoursService) {
                    return OperatingHoursService.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'operating-hours',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('operating-hours.edit', {
            parent: 'operating-hours',
            url: '/{id}/edit',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/operating-hours/operating-hour-dialog.html',
                    controller: 'OperatingHourDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['OperatingHoursService', function(OperatingHoursService) {
                            return OperatingHoursService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('operating-hours', null, { reload: 'operating-hours' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('operating-hours.delete', {
            parent: 'operating-hours',
            url: '/{id}/delete',
            data: {
                requiresAuthentication: true,
                authorities: []
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'core/routes/operating-hours/operating-hour-delete-dialog.html',
                    controller: 'OperatingHourDeleteDialogController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['OperatingHoursService', function(OperatingHoursService) {
                            return OperatingHoursService.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('operating-hours', null, { reload: 'operating-hours' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }
})();
