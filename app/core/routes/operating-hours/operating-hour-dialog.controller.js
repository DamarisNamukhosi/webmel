(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OperatingHourDialogController', OperatingHourDialogController);

    OperatingHourDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', '$uibModalInstance', 'entity', 'OperatingHoursService', 'GeneralDaysService'];

    function OperatingHourDialogController ($timeout, $scope, $stateParams,$localStorage, $uibModalInstance, entity, OperatingHoursService, GeneralDaysService) {
        var vm = this;

        vm.operatingHour = entity;
        vm.clear = clear;
        vm.save = save;
        //vm.days = vm.operatingHour.days;
        //console.log(vm.days);
        vm.operatinghours = OperatingHoursService.getOperatingHoursByOrganizaitonId({id: $localStorage.current_organisation.id});

        vm.generalDays = GeneralDaysService.query();    

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.operatingHour.id !== null) {
                OperatingHoursService.update(vm.operatingHour, onSaveSuccess, onSaveError);
            } else {
                OperatingHoursService.create(vm.operatingHour, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
