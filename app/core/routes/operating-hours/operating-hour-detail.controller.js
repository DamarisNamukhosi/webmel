(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('OperatingHourDetailController', OperatingHourDetailController);

        OperatingHourDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'OperatingHoursService'];

    function OperatingHourDetailController($scope, $rootScope, $stateParams, previousState, entity, OperatingHoursService) {
        var vm = this;
        
        vm.operatingHour = entity;
        vm.previousState = previousState.name;

        console.log(vm.operatingHour.days)
    }
})();
