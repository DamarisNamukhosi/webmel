(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('PoliciesController', PoliciesController);

    PoliciesController.$inject = [
        '$scope',
        '$location',
        '$state',
        '$localStorage',
        'PoliciesService',
        'policies',
        'roomOccupancies'
    ];

    function PoliciesController(
        $scope,
        $location,
        $state,
        $localStorage,
        PoliciesService,
        policies,
        roomOccupancies
        ) {
        var vm = this;

        vm.policies = policies;
        vm.parent= [];
        vm.rules= [];

        vm.roomOccupancies = roomOccupancies;
        vm.resolveOccupancyName=resolveOccupancyName;
        vm.editPolicy = editPolicy;
        vm.currentOrganisation =$localStorage.current_organisation;

        //init functions
        getAllRules();
        getParent();



        function getAllRules(){
            PoliciesService.getRules().$promise.then(function (response){
                vm.rules= response;
            })
        }
        function getParent(){
            if($localStorage.current_immediate_parent_organisation != null){
                if($localStorage.current_immediate_parent_organisation.id != 2){
                vm.parent = $localStorage.current_immediate_parent_organisation;
            }
            }
        }
        function resolveOccupancyName(usedOccupancy){
            var  occupancyName= null;

            angular.forEach(vm.roomOccupancies, function(occupancy){
                if(usedOccupancy == occupancy.id){
                    occupancyName= occupancy.abbreviation;
                }
            });
            return occupancyName;
        }
        vm.addPolicyRow = function(policyId) {

            //$state.go('settings-policies.edit', {id: vm.policies.id}, {reload: true});
            $state.go('settings-policies.createRules', {occupancyPolicyId: policyId}, {reload: true});

        };
        //settings-policies.edit
         function editPolicy(policyId) {

            //$state.go('settings-policies.edit', {id: vm.policies.id}, {reload: true});
            $state.go('settings-policies.edit', {id: policyId}, {reload: true});

        };
    }
})();
