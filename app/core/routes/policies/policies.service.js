(function() {
    'use strict';
    angular.module('webApp').factory('PoliciesService', PoliciesService);

    PoliciesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function PoliciesService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(
            resourceUrl,
            {},
            {
                get: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/:id'
                },
                getByOrganization: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/get-full-by-organisation/:id'
                },
                update: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'contentservice/api/occupancy-policies'
                },
                delete: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/:id'
                },
                create: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url: URLS.BASE_URL + 'contentservice/api/occupancy-policies'
                },
                createList: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/create-list'
                },
                updateList: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-policies/update-list'
                },
                createRules: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules'
                },
                copyRules: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules/copy'
                },
                updateRules: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules'
                },
                getRules: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules'
                },

                getRule: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules/:id'
                },
                deleteRule: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/occupancy-rules/:id'
                }
                //getRules
                //POST /api/occupancy-rules updateRules
                //POST /api/occupancy-policies/copy
            }
        );
    }
})();
