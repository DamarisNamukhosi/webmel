(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'PolicyDeleteDialogController',
            PolicyDeleteDialogController
        );

    PolicyDeleteDialogController.$inject = [
        '$stateParams',
        '$uibModalInstance',
        '$state',
        'PoliciesService',
        'entity'
    ];

    function PolicyDeleteDialogController(
        $stateParams,
        $uibModalInstance,
        $state,
        PoliciesService,
        entity
    ) {
        var vm = this;
        vm.entity =entity;
        console.log(vm.entity)
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        //name: "settings-policies.deleteRules"
        vm.isPolicy = false;
        vm.isRule=  false;

        identifyIntity();

        function identifyIntity(){
            if($state.current.name == "settings-policies.deleteRules"){
            
                vm.isRule=  true;
            }else if($state.current.name == "settings-policies.delete"){
                vm.isPolicy = true;
            }
        }

        
        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        var policyId = $stateParams.id;

        function confirmDelete() {
            console.log('ID: ' + policyId);
            if(vm.isPolicy ){
            vm.entity.contentStatus = "DELETED";
            PoliciesService.update( vm.entity, function() {
                $uibModalInstance.close(true);
            });
        }else if(vm.isRule){
            PoliciesService.deleteRule({ id: vm.entity.id }, function() {
                $uibModalInstance.close(true);
            });
        }
        }
    }
})();
