(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('policies.new', {
                parent: 'policies',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: '',
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/policies/policy-dialog.html',
                                controller: 'PolicyDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            adults: 0,
                                            children: 0,
                                            content_status: 'DRAFT',
                                            id: 0,
                                            rate: 0,
                                            roomOccupancyId: 0,
                                            uuid: 'string',
                                        };
                                    },
                                },
                            })
                            .result.then(
                                function() {
                                    $state.go('policies', null, {
                                        reload: 'policies',
                                    });
                                },
                                function() {
                                    $state.go('^');
                                },
                            );
                    },
                ],
            })
            .state('policies-managed', {
                parent: 'app',
                url: '/my-policies',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MANAGER_USER'],
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/policies/policies-managed.html',
                        controller: 'OrganizationsManagedController',
                        controllerAs: 'vm',
                    },
                },
                resolve: {
                    policies: [
                        '$stateParams',
                        '$localStorage',
                        'OrganizationsService',
                        function(
                            $stateParams,
                            $localStorage,
                            OrganizationsService,
                        ) {
                            return $localStorage.current_managed_organisations;
                        },
                    ],
                },
            })
            .state('policies-managed.new', {
                parent: 'policies-managed',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MANAGER_USER'],
                    pageTitle: '',
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/policies/organization-register-dialog.html',
                                controller:
                                    'OrganizationRegisterDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            abbreviation: null,
                                            altitude: null,
                                            brief: null,
                                            description: null,
                                            langKey: 'en',
                                            latitude: null,
                                            longitude: null,
                                            name: null,
                                            organisationTypeId: null,
                                            parentLocationId: null,
                                            parentOrganisationId:
                                                $localStorage
                                                    .current_organisation.id,
                                            userEmailAddress: null,
                                            userFullName: null,
                                        };
                                    },
                                },
                            })
                            .result.then(
                                function() {
                                    $state.go('policies-managed', null, {
                                        reload: 'policies-managed',
                                    });
                                },
                                function() {
                                    $state.go('^');
                                },
                            );
                    },
                ],
            })
            .state('organization-detail', {
                parent: 'policies',
                url: '/{id}/overview',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MASTER_USER', 'MANAGER_USER'],
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/policies/organization-overview.html',
                        controller: 'OverviewController',
                        controllerAs: 'vm',
                    },
                },
                resolve: {
                    organization: [
                        '$stateParams',
                        'OrganizationsService',
                        function($stateParams, OrganizationsService) {
                            return OrganizationsService.get({
                                id: $stateParams.id,
                            }).$promise;
                        },
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name: $state.current.name || 'policies',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params,
                                ),
                            };
                            return currentStateData;
                        },
                    ],
                },
            })
            .state('organization-mealplans', {
                parent: 'policies',
                url: '/{id}/meal-plans',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MASTER_USER', 'MANAGER_USER'],
                },
                views: {
                    'content@': {
                        templateUrl:
                            'core/routes/policies/organization-mealplans.html',
                        controller: 'OrganizationMealPlanController',
                        controllerAs: 'vm',
                    },
                },
                resolve: {
                    organization: [
                        '$stateParams',
                        'OrganizationsService',
                        function($stateParams, OrganizationsService) {
                            return OrganizationsService.getMealPlansByOrganizations(
                                {
                                    id: $stateParams.id,
                                },
                            ).$promise;
                        },
                    ],
                    entity: [
                        '$stateParams',
                        'OrganizationsService',
                        function($stateParams, OrganizationsService) {
                            return OrganizationsService.get({
                                id: $stateParams.id,
                            }).$promise;
                        },
                    ],
                    previousState: [
                        '$state',
                        function($state) {
                            var currentStateData = {
                                name:
                                    $state.current.name ||
                                    'organization-detail',
                                params: $state.params,
                                url: $state.href(
                                    $state.current.name,
                                    $state.params,
                                ),
                            };
                            return currentStateData;
                        },
                    ],
                },
            });
    }
})();
