(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('PolicyCreateDialogController', PolicyCreateDialogController);

  PolicyCreateDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$state',
    '$uibModalInstance',
    '$localStorage',
    'PoliciesService',
    'roomOccupancies',
    'policies',
    'AgeLimitsService'
  ];

  function PolicyCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $state,
    $uibModalInstance,
    $localStorage,
    PoliciesService,
    roomOccupancies,
    policies,
    AgeLimitsService
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;

    vm.roomOccupancies = roomOccupancies;
    vm.addPolicyRules = addPolicyRules;
    vm.isDefaut = false;
    vm.policies = policies;
    vm.policyItems = [];
    vm.orgAgeLimits = [];
    vm.addRules = false;
    vm.setIsDefaut = setIsDefaut;
    // vm.entity  = {
    //     "isDefault": true,
    //     "name": null,
    //     "organisationId": $localStorage.current_organisation.id,
    //     policyType: "ROOM_OCCUPANCY",
    //     roomCategories : null
    // };

    vm.rateTypes = ["PER_PERSON", "PER_ROOM", "PER_VEHICLE"];
    vm.policyTypes = ["ROOM_OCCUPANCY", "VEHICLE_OCCUPANCY"];
    //function declarion
    initPolicies();
    checkPreviousState();
    initOrganizationAgeLimits();

    //function implementation
    function initPolicies() {
      console.log("checking context");
      if (vm.policies.id !== null) {
        vm.entity = vm.policies;
      } else {
        console.log("new");
        // vm.policyItems.push({
        //     adults: null,
        //     adultRate: null,
        //     adultRoomOccupancyId: null,
        //     children: null,
        //     childrenRoomOccupancyId: null,
        //     childrenRate: null,
        //     content_status: 'DRAFT',
        //     organisationId: $localStorage.current_organisation.id
        // });

        vm.entity = {
          "id": null,
          "name": null,
          "policyType": "ROOM_OCCUPANCY",
          "uuid": null,
          "isDefault": false,
          "year": "2019",
          "contentStatus": "DRAFT",
          "organisationId": $localStorage.current_organisation.id,
          "organisationName": null
        };

      }
    }

    function addPolicyRules() {
      vm.addRules = true;
      var temp = {
        adults: null,
        adultRate: null,
        adultRoomOccupancyId: null,
        children: null,
        childrenRoomOccupancyId: null,
        childrenRate: null,
        content_status: 'DRAFT',
        organisationId: $localStorage.current_organisation.id
      };

      vm.policyItems.push(temp);
    };

    vm.removePolicy = function ($index) {
      vm.policyItems.splice($index, 1);
    };

    function initOrganizationAgeLimits() {
      AgeLimitsService.getAgeLimits({
        id: $localStorage.current_organisation.id
      }).$promise.then(function (response) {
        vm.orgAgeLimits = response;
      });

    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function checkPreviousState() {
      if ($state.current.parent == "settings-policies") {
        //vm.addRules();
      }
    }

    function setIsDefaut() {
      if (vm.entity.isDefault) {
        vm.entity.name = $localStorage.current_organisation.name + " " + "Default Child Policy";

      } else {
        vm.entity.name = null;
      }
    }

    function save() {
      vm.isSaving = true;
      if (vm.entity.id !== null) {
        PoliciesService.update(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      } else {
        PoliciesService.create(
          vm.entity,
          onSaveSuccess,
          onSaveError
        );
      }
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
