(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "RoomCategoryWithoutOccController",
      RoomCategoryWithoutOccController
    );

  RoomCategoryWithoutOccController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "entity",
    "RoomCategoriesService"
  ];

  function RoomCategoryWithoutOccController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    entity,
    RoomCategoriesService
  ) {
    var vm = this;
    vm.entity = entity;
    vm.clickedItem = null;

    checkEntity();
    function checkEntity(){
      if(vm.entity.length > 1){
        angular.forEach(vm.entity, function(org){
          if(org.id == $stateParams.categoryToDelete){
            vm.clickedItem = org;
            console.log("org");
            console.log(org);
          }
        });
      }else if(vm.entity.length == 1){
        vm.clickedItem = vm.entity;
      }
    }

    vm.clear = function clear() {
      $uibModalInstance.dismiss("cancel");
    };

    vm.save = function save() {
      vm.clickedItem.occupancyPolicyId = null;
      vm.clickedItem.occupancyPolicyName = null;
      console.log(vm.clickedItem);
      vm.isSaving = true;

      RoomCategoriesService.update(
        vm.clickedItem,
        onSaveSuccess,
        onSaveError
      );
    };

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
