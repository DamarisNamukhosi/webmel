(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "RoomsWithOcuupancyCreateDialogController",
      RoomsWithOcuupancyCreateDialogController
    );

  RoomsWithOcuupancyCreateDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "roomCategories",
    "RoomCategoriesService"
  ];

  function RoomsWithOcuupancyCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    roomCategories,
    RoomCategoriesService
  ) {
    var vm = this;

    vm.roomCategories = roomCategories;
    console.log(vm.roomCategories);
    //vm.savedselectedRooms = savedselectedRooms;
    console.log('selected featuress');
    console.log(vm.savedselectedRooms);
    //vm.entity = entity;
    vm.clear = clear;
    vm.save = save;
    vm.selectedRooms = [];
    vm.savedList = [];
    vm.newRooms = [];

    vm.noRoomCategoriesFound = (vm.roomCategories === undefined || vm.roomCategories.length === 0);
    checkIfExists();

    function checkIfExists() {

      var found = false;
      angular.forEach(vm.roomCategories, function(room){

      //   if(!found){

      //     if (item.occupancyPolicyId == 3){

      //       found = true;

      //     }
      //   }

      
      console.log(room.name);
      if(room.occupancyPolicyId == null){
        vm.newRooms.push(room)
      }
//      console.log(found);
      console.log(vm.newRooms);
});
    }
    vm.toggleSelection = function(selectedRoom) {
      console.log("clicked");
      
      //vm.selectedRooms.push(selectedRoom);
     
      var id = vm.selectedRooms.indexOf(selectedRoom);
      if (id > -1) {
        vm.selectedRooms.splice(id, 1);
      } else {
        vm.selectedRooms.push(selectedRoom);
      }
    };

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function save() {
      vm.isSaving = true;
      var i = vm.selectedRooms.length
      angular.forEach(vm.selectedRooms, function(item) {

        // var temp = {
        //   generalFeatureId: item.id,
        //   generalFeatureName: item.name,
        //   objectId: vm.entity.uuid
        // };
        item.occupancyPolicyId = $stateParams.occupancyPolicyId;
        i = 1+1;
        if(i == vm.selectedRooms.length){
          RoomCategoriesService.update(item, onSaveSuccess, onSaveError);
      }else{
        RoomCategoriesService.update(item, onSaveSuccess, onSaveError);
      }
        //vm.savedList.push(temp);
      });
      console.log('savedlist...');
      console.log(vm.savedList);
      //ObjectFeaturesService.createList(vm.savedList, onSaveSuccess, onSaveError);
    }

    function onSaveSuccess(result) {
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
