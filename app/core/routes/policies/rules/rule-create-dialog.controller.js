(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('RuleCreateDialogController', RuleCreateDialogController);

  RuleCreateDialogController.$inject = [
    '$timeout',
    '$scope',
    '$stateParams',
    '$state',
    '$uibModalInstance',
    '$localStorage',
    'PoliciesService',
    'roomOccupancies',
    'rule',
    'AgeLimitsService',
    'organisationAgeLimits'
  ];

  function RuleCreateDialogController(
    $timeout,
    $scope,
    $stateParams,
    $state,
    $uibModalInstance,
    $localStorage,
    PoliciesService,
    roomOccupancies,
    rule,
    AgeLimitsService,
    organisationAgeLimits
  ) {
    var vm = this;

    vm.clear = clear;
    vm.save = save;

    vm.roomOccupancies = roomOccupancies;
    console.log(vm.roomOccupancies);
    vm.addPolicyRules = addPolicyRules;
    vm.setIsAllowed = setIsAllowed;
    vm.setIsGlobal = setIsGlobal;
    vm.isDefaut = isDefaut
    vm.rule = rule;
    console.log(vm.rule)
    vm.policyRules = [];
    vm.orgAgeLimits = organisationAgeLimits;
    vm.addRules = false;
    var today = new Date();
    //vm.occupancyChange = occupancyChange;
    console.log($state.current.name);
    vm.viewRules = $state.current.name == "settings-policies.viewRules"
    console.log(vm.viewRules);
    vm.entity = {
      "id": null,
      "description": null,
      "contentStatus": "DRAFT",
      "childPattern": null,
      "childBaseRateId": null,
      "childBaseRateName": null,
      "childRate": null,
      "childRateType": null,
      "adultBaseRateId": null,
      "adultPattern": null,
      "adultBaseRateName": null,
      "adultRate": null,
      "adultRateType": null,
      "occupancyPolicyId": $stateParams.occupancyPolicyId,
      "organisationId": $localStorage.current_organisation.id,
      "youngAdultBaseRateId": null,
      "youngAdultBaseRateName": null,
      "youngAdultPattern": null,
      "youngAdultRate": null,
      "youngAdultRateType": null,
      "infantBaseRateId": null,
      "infantBaseRateName": null,
      "infantPattern": null,
      "infantRate": null,
      "infantRateType": null,
      "agedBaseRateId": null,
      "agedBaseRateName": null,
      "agedPattern": null,
      "agedRate": null,
      "agedRateType": null,
      "allowed": true,
      "occupantsPattern": "[2-3]",
      "global": false
    };

    vm.rateTypes = ["PER_PERSON", "PER_ROOM"];
    //, "PER_VEHICLE"
    vm.policyTypes = ["ROOM_OCCUPANCY", "VEHICLE_OCCUPANCY"];
    //function declarion
    vm.setBaseRateName = setBaseRateName;
    vm.setOccupantBaseRateName = setOccupantBaseRateName;
    vm.createRules = createRules;
    vm.updateRules =  updateRules;

    //init function
    initOrganizationAgeLimits();
    initPolicies();
    checkPreviousState();
    initPolicyRules();
    //function implementation
    function initPolicies() {
      initOrganizationAgeLimits();
      if (vm.rule.id !== null) {
        console.log("not new rule..");
        // angular.forEach(vm.rule, function(policy) {
        //     vm.policyRules.push(policy);
        // });
        vm.entity.description = vm.rule.description;
        vm.entity.id = vm.rule.id;
        vm.entity.contentStatus = vm.rule.contentStatus;
        vm.entity.allowed = vm.rule.allowed;
        vm.entity.occupancyPolicyId = vm.rule.occupancyPolicyId;
        vm.entity.occupantsPattern = vm.rule.occupantsPattern;
        //vm.policyRules.push(vm.rule);
        if (vm.rule.length == 1) {
          initPolicyRules(true);
        } else if (vm.rule.length > 1) {
          initPolicyRules(true);
        }
      } else {
        console.log("new rule..");
        vm.policyRules.push({
          id: null,
          ageBracket: null,
          pattern: "[1-3]",
          baseRateId: null,
          baseRateName: null,
          rate: null,
          rateType: "PER_PERSON"
        });
      }
    }

    function addPolicyRules() {
      console.log("add policy");
      vm.addRules = true;
      var temp = {
        id: null,
        ageBracket: null,
        pattern: "[1-3]",
        baseRateId: null,
        baseRateName: null,
        rate: null,
        rateType: "PER_PERSON"
      };

      vm.policyRules.push(temp);
    };

    vm.removePolicy = function ($index) {
      vm.policyRules.splice($index, 1);
    };

    function initOrganizationAgeLimits() {
      if (vm.orgAgeLimits == null) {
        $timeout(function () {
          AgeLimitsService.getAgeLimits({
            id: $localStorage.current_organisation.id
          }).$promise.then(function (response) {
            vm.orgAgeLimits = response;
            console.log("vm.orgAgeLimits");
            console.log(vm.orgAgeLimits);
          });
        }, 1000);
      }

    }

    function initPolicyRules(isEditing) {
      console.log(isEditing);
      if (vm.rule.id == null) {
        console.log("new policies");
        if (vm.orgAgeLimits != null && vm.orgAgeLimits.length > 0) {
          angular.forEach(vm.orgAgeLimits, function (ageLimit) {
            if (ageLimit.generalAgeBracketName == "Infant") {
              vm.infantEntity = {
                "infantBaseRateId": null,
                "infantBaseRateName": "PPS",
                "infantPattern": null,
                "infantRate": null,
                "infantRateType": "PER_PERSON"
              };
            } else if (ageLimit.generalAgeBracketName == "Child") {
              vm.childEntity = {
                "childPattern": null,
                "childBaseRate": "PPS",
                "childRate": null,
                "childRateType": "PER_PERSON"
              }
            } else if (ageLimit.generalAgeBracketName == "Young Adult") {
              vm.youngAdultEntity = {
                "youngAdultBaseRateId": null,
                "youngAdultBaseRateName": "PPS",
                "youngAdultPattern": null,
                "youngAdultRate": null,
                "youngAdultRateType": "PER_PERSON"
              }
            } else if (ageLimit.generalAgeBracketName == "Adult") {
              vm.adultEntity = {
                "adultPattern": null,
                "adultBaseRate": "PPS",
                "adultRate": null,
                "adultRateType": "PER_PERSON"
              }
            }
          });
        }
      } else {
        console.log("editing a rule");
        console.log("vm.orgAgeLimits");
        console.log(vm.orgAgeLimits);
        console.log(vm.rule);
        if(vm.rule.occupantsBaseRateId != null){
          console.log("init global..");
          vm.entity.global = true;
          console.log("vm.rule");
          console.log(vm.rule);
          //setIsGlobal();
          vm.policyRules.push(vm.rule);
          console.log(vm.policyRules);

        }else{
        if (vm.orgAgeLimits.length == 0) {
          initOrganizationAgeLimits();
          console.log(vm.orgAgeLimits);

        }else if (vm.orgAgeLimits.length > 1) {
          if (vm.rule.infantBaseRateId != null || vm.rule.infantPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 5) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.infantPattern,
                  baseRateId: vm.rule.infantBaseRateId,
                  rate: vm.rule.infantRate,
                  rateType: vm.rule.infantRateType
                };
                vm.policyRules.push(temp)

              }

            });
          }
          if (vm.rule.childBaseRateId != null || vm.rule.childPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 1) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.childPattern,
                  baseRateId: vm.rule.childBaseRateId,
                  baseRateName: vm.rule.childBaseRateName,
                  rate: vm.rule.childRate,
                  rateType: vm.rule.childRateType
                };
                vm.policyRules.push(temp)

              }

            });


          }
          if (vm.rule.youngAdultBaseRateId != null || vm.rule.youngAdultPattern != null) {

            console.log("here");
            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 3) {
                console.log("YA");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.youngAdultPattern,
                  baseRateId: vm.rule.youngAdultBaseRateId,
                  baseRateName: vm.rule.youngAdultBaseRateName,
                  rate: vm.rule.youngAdultRate,
                  rateType: vm.rule.youngAdultRateType
                };
                vm.policyRules.push(temp);
              }
            });
          }
          if (vm.rule.adultBaseRateId != null || vm.rule.adultPattern != null) {
            // vm.policyRules.ageBracket = 2;
            // vm.policyRules.baseRate = vm.rule.adultBaseRateId;
            // vm.policyRules.pattern = vm.rule.adultPattern;
            // vm.policyRules.rate = vm.rule.adultRate;
            // vm.policyRules.rateType = vm.rule.adultRateType;

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 2) {
                console.log("Adult");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.adultPattern,
                  baseRateId: vm.rule.adultBaseRateId,
                  baseRateName: vm.rule.adultBaseRateName,
                  rate: vm.rule.adultRate,
                  rateType: vm.rule.adultRateType
                };
                vm.policyRules.push(temp);
              }
            });

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.agedPattern,
                  baseRateId: vm.rule.agedBaseRateId,
                  baseRateName: vm.rule.agedBaseRateName,
                  rate: vm.rule.agedRate,
                  rateType: vm.rule.agedRateType
                };
                vm.policyRules.push(temp);
              }
            });
          }
          if (vm.rule.agedBaseRateId != null || vm.rule.agedPattern != null) {

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                console.log("Adult");
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.agedPattern,
                  baseRateId: vm.rule.agedBaseRateId,
                  baseRateName: vm.rule.agedBaseRateName,
                  rate: vm.rule.agedRate,
                  rateType: vm.rule.agedRateType
                };
                vm.policyRules.push(temp);
              }
            });

            angular.forEach(vm.orgAgeLimits, function (limit) {
              if (limit.generalAgeBracketId == 4) {
                var temp = {
                  //description: null,
                  ageBracket: limit.id,
                  pattern: vm.rule.agedPattern,
                  baseRateId: vm.rule.agedBaseRateId,
                  baseRateName: vm.rule.agedBaseRateName,
                  rate: vm.rule.agedRate,
                  rateType: vm.rule.agedRateType
                };
                vm.policyRules.push(temp);
              }
            });
          }
        }
      }
        console.log("vm.policyRules");
        console.log(vm.policyRules);
      }
    }

    // function occupancyChange(occ, index) {
    //   console.log("occupancy change");
    //   console.log(occ);
    //   console.log(index);
    // }
    function setIsAllowed() {
      console.log("vm.entity.allowed");
      console.log(vm.entity.allowed);
      if (vm.entity.allowed) {
        console.log("allowed");
      } else {
        console.log("blocked");
      }
    }

    function setIsGlobal() {
      console.log("vm.entity.global");
      console.log(vm.entity.global);
      console.log(vm.policyRules);
      if (vm.entity.global) {
        console.log("global");
        console.log(vm.policyRules[0]);
        vm.policyRules[0].occupantsBaseRateId = 7;
        vm.policyRules[0].occupantsBaseRateName =  null;
        vm.policyRules[0].occupantsRate= 1;
        vm.policyRules[0].occupantsRateType = "PER_ROOM";
        console.log(vm.policyRules[0]);
        setOccupantBaseRateName(vm.policyRules[0], vm.policyRules[0].occupantsBaseRateId);


      } else {
        console.log("not global");
        console.log(vm.policyRules);
        vm.policyRules[0].occupantsBaseRateId = null;
        vm.policyRules[0].occupantsBaseRateName =  null;
        vm.policyRules[0].occupantsRate= null;
        vm.policyRules[0].occupantsRateType =null;
        console.log(vm.policyRules[0]);


      }
    }

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }

    function checkPreviousState() {
      console.log($state)
      if ($state.current.parent == "settings-policies") {
        //vm.addRules();
        console.log("previous state");
        console.log($state.current.parent);
      }
    }

    function isDefaut() {
      vm.entity.default = !vm.entity.default;
      if (vm.entity.default) {
        vm.entity.name = $localStorage.current_organisation.name + " " + "Default Child Policy";
        console.log("vm.entity.name");
        console.log(vm.entity.name);
      }
    }

    function save() {
      console.log("saving...");
      console.log(vm.policyRules);
      vm.isSaving = true;
      console.log(vm.rule.id != null)
      if (vm.rule.id != null) {
        console.log("ready to update");
        console.log(vm.policyRules);
        console.log(vm.orgAgeLimits);
        if (vm.entity.global) {
          console.log("global rule");
          console.log(vm.entity);
          console.log(vm.policyRules[0]);
          vm.entity.occupantsBaseRateId = vm.policyRules[0].occupantsBaseRateId;
          vm.entity.occupantsBaseRateName = vm.policyRules[0].occupantsBaseRateName;
          vm.entity.occupantsRate = vm.policyRules[0].occupantsRate;
          vm.entity.occupantsRateType = vm.policyRules[0].occupantsRateType;

          updateRules(vm.entity);
        } else {
          console.log("not global");
          angular.forEach(vm.policyRules, function (rule) {
            angular.forEach(vm.orgAgeLimits, function (orgAgeLimit) {
              if (rule.ageBracket == orgAgeLimit.id) {
                if (orgAgeLimit.generalAgeBracketId == 5) {

                  vm.entity.infantBaseRateId = rule.baseRateId;
                  vm.entity.infantBaseRateName = rule.baseRateName;
                  vm.entity.infantPattern = rule.pattern;
                  vm.entity.infantRate = rule.rate;
                  vm.entity.infantRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 1) {
                  vm.entity.childBaseRateId = rule.baseRateId;
                  vm.entity.childBaseRateName = rule.baseRateName;
                  vm.entity.childPattern = rule.pattern;
                  vm.entity.childRate = rule.rate;
                  vm.entity.childRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 3) {
                  // vm.youngAdultEntity = {
                  vm.entity.youngAdultBaseRateId = rule.baseRateId;
                  vm.entity.youngAdultBaseRateName = rule.baseRateName;
                  vm.entity.youngAdultPattern = rule.pattern;
                  vm.entity.youngAdultRate = rule.rate;
                  vm.entity.youngAdultRateType = rule.rateType;
                  // }
                } else if (orgAgeLimit.generalAgeBracketId == 2) {
                  vm.entity.adultBaseRateId = rule.baseRateId;
                  vm.entity.adultBaseRateName = rule.baseRateName;
                  vm.entity.adultPattern = rule.pattern;
                  vm.entity.adultRate = rule.rate;
                  vm.entity.adultRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 4) {
                  vm.entity.agedBaseRateId = rule.baseRateId;
                  vm.entity.agedRateName = rule.baseRateName;
                  vm.entity.agedPattern = rule.pattern;
                  vm.entity.agedRate = rule.rate;
                  vm.entity.agedRateType = rule.rateType;

                }
              }
            });
          });

          updateRules(vm.entity);
        }

      } else {
        console.log("ready to save");
        console.log(vm.policyRules);

        if (vm.entity.allowed) {
          if (vm.entity.global) {
            console.log("global");
            angular.forEach(vm.policyRules, function (rule) {
              vm.entity.occupantsBaseRateId = rule.occupantsBaseRateId;
              vm.entity.occupantsBaseRateName = rule.occupantsBaseRateName;
              vm.entity.occupantsRateType = rule.occupantsRateType;
              vm.entity.occupantsRate =  rule.occupantsRate;

            });
            createRules(vm.entity);

          }else{
            console.log("not global");

          angular.forEach(vm.policyRules, function (rule) {
            angular.forEach(vm.orgAgeLimits, function (orgAgeLimit) {
              if (rule.ageBracket == orgAgeLimit.id) {
                if (orgAgeLimit.generalAgeBracketId == 5) {

                  vm.entity.infantBaseRateId = rule.baseRateId;
                  vm.entity.infantBaseRateName = rule.baseRateName;
                  vm.entity.infantPattern = rule.pattern;
                  vm.entity.infantRate = rule.rate;
                  vm.entity.infantRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 1) {
                  vm.entity.childBaseRateId = rule.baseRateId;
                  vm.entity.childBaseRateName = rule.baseRateName;
                  vm.entity.childPattern = rule.pattern;
                  vm.entity.childRate = rule.rate;
                  vm.entity.childRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 3) {
                  // vm.youngAdultEntity = {
                  vm.entity.youngAdultBaseRateId = rule.baseRateId;
                  vm.entity.youngAdultBaseRateName = rule.baseRateName;
                  vm.entity.youngAdultPattern = rule.pattern;
                  vm.entity.youngAdultRate = rule.rate;
                  vm.entity.youngAdultRateType = rule.rateType;
                  // }
                } else if (orgAgeLimit.generalAgeBracketId == 2) {
                  vm.entity.adultBaseRateId = rule.baseRateId;
                  vm.entity.adultBaseRateName = rule.baseRateName;
                  vm.entity.adultPattern = rule.pattern;
                  vm.entity.adultRate = rule.rate;
                  vm.entity.adultRateType = rule.rateType;

                } else if (orgAgeLimit.generalAgeBracketId == 4) {
                  vm.entity.agedBaseRateId = rule.baseRateId;
                  vm.entity.agedRateName = rule.baseRateName;
                  vm.entity.agedPattern = rule.pattern;
                  vm.entity.agedRate = rule.rate;
                  vm.entity.agedRateType = rule.rateType;

                }
              }
            });
          });
          createRules(vm.entity);

        }
        } else {
          angular.forEach(vm.policyRules, function (rule) {
            angular.forEach(vm.orgAgeLimits, function (orgAgeLimit) {
              if (rule.ageBracket == orgAgeLimit.id) {
                if (orgAgeLimit.generalAgeBracketId == 5) {

                  vm.entity.infantBaseRateId = null;
                  vm.entity.infantPattern = rule.pattern;
                  vm.entity.infantRate = null;
                  vm.entity.infantRateType = null;

                } else if (orgAgeLimit.generalAgeBracketId == 1) {
                  vm.entity.childBaseRateId = null;
                  vm.entity.childPattern = rule.pattern;
                  vm.entity.childRate = null;
                  vm.entity.childRateType = null;

                } else if (orgAgeLimit.generalAgeBracketId == 3) {
                  // vm.youngAdultEntity = {
                  vm.entity.youngAdultPattern = rule.pattern;
                  vm.entity.youngAdultBaseRateId = null;
                  vm.entity.youngAdultRate = null;
                  vm.entity.youngAdultRateType = null;
                  // }
                } else if (orgAgeLimit.generalAgeBracketId == 2) {
                  vm.entity.adultBaseRateId = null;
                  vm.entity.adultPattern = rule.pattern;
                  vm.entity.adultRate = null;
                  vm.entity.adultRateType = null;

                } else if (orgAgeLimit.generalAgeBracketId == 4) {
                  vm.entity.agedPattern = rule.pattern;
                  vm.entity.agedBaseRateId = null;
                  vm.entity.agedRate = null;
                  vm.entity.agedRateType = null;

                }
              }
            });
          });
          createRules(vm.entity);

        }



      }
    }
    function createRules(object){
      console.log("vm.object");
        console.log(object);
        PoliciesService.createRules(
          object,
          onSaveSuccess,
          onSaveError
        );
    }

    function updateRules(object){
      console.log("vm.entity update");
        console.log(object);
        PoliciesService.updateRules(
          object,
          onSaveSuccess,
          onSaveError
        );
    }

    function onSaveSuccess(result) {
      console.log(result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }

    function setBaseRateName(policy, baseRateId) {
      angular.forEach(vm.roomOccupancies, function (orgOccupancy) {
        if (orgOccupancy.generalRoomOccupancyId == baseRateId) {
          policy.baseRateName = orgOccupancy.abbreviation;
        }
      });
    }

    function setOccupantBaseRateName(policy, occupantsBaseRateId) {
      angular.forEach(vm.roomOccupancies, function (orgOccupancy) {
        if (orgOccupancy.generalRoomOccupancyId == occupantsBaseRateId) {
          console.log(orgOccupancy);
          policy.occupantsBaseRateName = orgOccupancy.abbreviation;
          console.log(policy);
        }
      });
    }
  }
})();
