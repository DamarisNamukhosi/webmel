(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('PolicyDialogController', PolicyDialogController);

    PolicyDialogController.$inject = [
        '$timeout',
        '$scope',
        '$stateParams',
        '$state',
        '$uibModalInstance',
        '$localStorage',
        'PoliciesService',
        'roomOccupancies',
        'policies'
    ];

    function PolicyDialogController(
        $timeout,
        $scope,
        $stateParams,
        $state,
        $uibModalInstance,
        $localStorage,
        PoliciesService,
        roomOccupancies,
        policies
    ) {
        var vm = this;

        vm.clear = clear;
        vm.save = save;

        vm.roomOccupancies = roomOccupancies;
        console.log(vm.roomOccupancies);
        vm.addPolicyRow = addPolicyRow;

        vm.policies = policies;
        console.log(vm.policies)
        vm.policyItems = [];
        initPolicies();
        checkPreviousState();

        
        function initPolicies(){
            if (vm.policies !== null) {
                angular.forEach(vm.policies, function(policy) {
                    vm.policyItems.push(policy);
                });
            } else {
                vm.policyItems.push({
                    adults: null,
                    adultRate: null,
                    adultRoomOccupancyId: null,
                    children: null,
                    childrenRoomOccupancyId: null,
                    childrenRate: null,
                    content_status: 'DRAFT',
                    organisationId: $localStorage.current_organisation.id
                });
            }
        }

         function addPolicyRow(){
            var temp = {
                adults: null,
                adultRate: null,
                adultRoomOccupancyId: null,
                children: null,
                childrenRoomOccupancyId: null,
                childrenRate: null,
                content_status: 'DRAFT',
                organisationId: $localStorage.current_organisation.id
            };

            vm.policyItems.push(temp);
        };

        vm.removePolicy = function($index) {
            vm.policyItems.splice($index, 1);
        };

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }
        
        function checkPreviousState(){
            console.log($state)
            if($state.current.parent == "settings-policies"){
                vm.addPolicyRow();
            }
        }

        function save() {
            console.log("saving...");
            console.log(vm.policyItems);
            vm.isSaving = true;
            console.log(vm.policies !== null)
            if (vm.policies.id !== null) {
                console.log("ready to save");
                console.log(vm.policyItems);
                PoliciesService.updateList(
                    vm.policyItems,
                    onSaveSuccess,
                    onSaveError
                );
            } else {
                console.log("ready to updated");
                console.log(vm.policyItems);
                PoliciesService.createList(
                    vm.policyItems,
                    onSaveSuccess,
                    onSaveError
                );
            }
        }

        function onSaveSuccess(result) {
            console.log(result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }
    }
})();
