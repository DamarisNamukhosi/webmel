(function() {
    'use strict';

    angular
        .module('webApp')
        .controller(
            'PolicyCopyDialogController',
            PolicyCopyDialogController
        );

    PolicyCopyDialogController.$inject = [
        '$stateParams',
        '$uibModalInstance',
        '$state',
        'PoliciesService',
        'entity',
        'OrganizationsService'
    ];

    function PolicyCopyDialogController(
        $stateParams,
        $uibModalInstance,
        $state,
        PoliciesService,
        entity,
        OrganizationsService
    ) {
        var vm = this;
        vm.entity =entity;
        console.log(vm.entity)
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        vm.save =  save;
        //name: "settings-policies.deleteRules"
        vm.isPolicy = false;
        vm.isRule=  false;
        vm.selectedOrg = selectedOrg;
        initOrganizations();

        function initOrganizations(){
            OrganizationsService.getManagedOrganizations({org_id: $stateParams.parent,relationship_type: 1 })
            .$promise.then(function (response){
                vm.organizations = response;
                angular.forEach(vm.organizations, function(org){
                        if (org.id == $stateParams.currentOrg){
                        var index = vm.organizations.indexOf(org);
                        if (index > -1) {
                        vm.organizations.splice(index, 1);
                        }
                    }
                });
            });
            console.log("vm.organizations");
            console.log(vm.organizations);
        }

        
        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        var policyId = $stateParams.id;

        function save() {
            console.log('ID: ');
            console.log( vm.entity);
            // if(vm.isPolicy ){
            console.log("vm.entity");
            console.log(vm.entity);
            PoliciesService.copyRules( vm.entity, function() {
                $uibModalInstance.close(true);
            });
        
        // else if(vm.isRule){
        //     PoliciesService.deleteRule({ id: vm.entity.id }, function() {
        //         $uibModalInstance.close(true);
        //     });
        // }
        }

        function selectedOrg(orgId){
            console.log(orgId);
            angular.forEach(vm.organizations, function(org){
                if(org.id == orgId){
                    vm.entity.name = org.name + " " + "Default Child Policy";
                }
            });
        }
    }
})();
