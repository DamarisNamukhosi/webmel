(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('RatesController', RatesController);

    RatesController.$inject = ['$scope', '$location', '$state', 'rates', 'contract', 'entity', 'dimensionSet'];

    function RatesController($scope, $location, $state, rates, contract, entity, dimensionSet) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.service = entity;
        vm.contract=contract;
        vm.rates = rates;
        console.log('ratess');
        console.log(vm.rates);
        vm.add= true;
        if(vm.rates.length == 0){
            vm.add = false;
        }
        vm.value = [];
        vm.myStack = [];
        angular.forEach(vm.rates, function (rate) {
            vm.value.push(rate.value);
          
        });
        console.log('vm.value');
        console.log(vm.value);
        

        console.log('service');
        console.log(vm.service);

        console.log('contract');
        console.log(vm.contract);

        vm.dimensionSet = dimensionSet;
        console.log('dimensionSet');
        console.log(vm.dimensionSet);
        vm.firstRow = vm.dimensionSet[0].existenceDTOList;
        vm.thirdItem = {
            "type": "BASIC",
            "existenceDTOList": [
                {
                    "id": 11,
                    "uuid": "00d50256-5675-4a38-a758-cddf3b79fb0d",
                    "name": "Default",
                    "brief": null
                },
            ]
        };



        var i = 0;

        var j = 0;
        vm.y = [1, 2, 3];
        vm.exiList = {};
        vm.exitList = [];
        vm.basicDim=[];
        vm.basicDimId = [];
        angular.forEach(vm.dimensionSet, function (set) {
            console.log('set.existenceDTOList' + i);
            console.log(set.existenceDTOList);
            i++;
            if( i == 3){
                vm.thirdItem= set;
            }
            
        });
       
        console.log('vm.thirdItem');
        console.log(vm.thirdItem);

        angular.forEach(vm.dimensionSet, function (dimension) {
            if (dimension.type == 'BASIC') {
                vm.basicDim.push(dimension);

                vm.basicDimId.push(dimension.id);

                console.log('dimension.existenceDTOList' + j);
                console.log(dimension.existenceDTOList);
                vm.exist = dimension.existenceDTOList;

                vm.exiList[j] = [];
                angular.forEach(vm.exist, function (ex) {
                    vm.exiList[j].push(ex.id);
                    vm.exitList.push(ex.id);
                })
                j++;
                console.log('vm.exiList');
                console.log(vm.exiList)

                console.log('vm.exitList');
                console.log(vm.exitList)

            }
            
        });

        console.log('vm.basicDim');
        console.log(vm.basicDim);

        console.log('vm.basicDimId');
        console.log(vm.basicDimId);
        vm.dimLength = vm.basicDim.length;
        console.log('vm.dimLength..');
        console.log(vm.dimLength);
        //
        if (vm.dimLength == 3){
            vm.inner = _.chunk(vm.value, vm.dimensionSet[0].existenceDTOList.length * vm.dimensionSet[1].existenceDTOList.length);
            vm.figures = [];

            angular.forEach(vm.inner, function (obj) {
                console.log(_.chunk(obj, vm.dimensionSet[2].existenceDTOList.length));

                //
                vm.figures.push(_.chunk(obj, vm.dimensionSet[0].existenceDTOList.length));
            });
            console.log('vm.dimensionSet[2].existenceDTOList.length');
            console.log(vm.dimensionSet[2].existenceDTOList.length);

            console.log('vm.dimensionSet[0].existenceDTOList.length');
            console.log(vm.dimensionSet[0].existenceDTOList.length);

            console.log('figures');
            console.log(vm.figures);
        }
        
        else if (vm.dimLength == 2) {
            vm.inner = vm.value;
            vm.figures = [];

            
                vm.figures.push(_.chunk(vm.inner, vm.dimensionSet[0].existenceDTOList.length));
           
            console.log('vm.dimensionSet[2].existenceDTOList.length');
            console.log(vm.dimensionSet[2].existenceDTOList.length);

            console.log('vm.dimensionSet[0].existenceDTOList.length');
            console.log(vm.dimensionSet[0].existenceDTOList.length);

            console.log('figures');
            console.log(vm.figures);
        }
        else if (vm.dimLength == 1) {
            vm.inner = vm.value;
            vm.figures = [];

           
                
                vm.figures.push(vm.inner);
        
            console.log('vm.dimensionSet[0].existenceDTOList.length');
            console.log(vm.dimensionSet[0].existenceDTOList.length);

            console.log('figures');
            console.log(vm.figures);
        }


    }
})();
