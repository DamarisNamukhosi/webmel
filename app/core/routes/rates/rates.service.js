(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('RatesService', RatesService);

  RatesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function RatesService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'getRatesByContract': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/rates/filter-by-contract/:id'
      },
      'getContractedValue': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'rateservice/api/get-contracted-value'
      },
      'resolveRates': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'rateservice/api/resolve-rates'
      },
      //POST /api/rates/service-group/resolve
      'resolveContractRates': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'rateservice/api/rates/service-group/resolve'
      },
      'compareRates': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'rateservice/api/rates/compare'
      },
      'compressedRates': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'rateservice/api/rates/compare/compressed'
      },
      'ratesResolve': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'rateservice/api/rates/resolve'
      },
      'saveList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/rates/save-list'
      },
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/rates'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/rates/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/rates'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/rates'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/rates/:id'
      }
    });
  }
})();
