(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('RatesDialogController', RatesDialogController);

  RatesDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'RatesService', 'dimensionSet', 'DimensionSetsService', 'srvice'];

  function RatesDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, RatesService, dimensionSet, DimensionSetsService, srvice) {
    var vm = this;

    vm.entity = entity;
    console.log('vm.entity');
    console.log(vm.entity);
    
    vm.clear = clear;
    vm.save = save;
    vm.srvice = srvice;
    console.log('vm.srvice');
    console.log(vm.srvice);

    vm.dimensions = [];
    vm.existency = [];
    vm.basicDim=[];
    vm.basicDimId = [];
    vm.rateList=[];
    vm.rows=[];
    vm.dimensionSet = dimensionSet;
    console.log('dimensionSet');
    console.log(vm.dimensionSet);
    //vm.array3 = [[[12, 13, 14], [13, 14, 15], [14, 15, 16]], [[13, 14, 15], [14, 15, 16], [15, 16, 17]]];
    vm.array3 = [[[]], [[]]];
    vm.firstRow = vm.dimensionSet[0].existenceDTOList;
    vm.thirdItem = {
      "type": "BASIC",
      "existenceDTOList": [
        {
          "id": 11,
          "uuid": "00d50256-5675-4a38-a758-cddf3b79fb0d",
          "name": "Default",
          "brief": null
        },
      ]
    };

    
   
    var i =0;

    var j = 0;
    vm.y= [1,2,3];
    vm.exiList={};
    vm.exitList = [];
    angular.forEach(vm.dimensionSet, function (set) {
      console.log('set.existenceDTOList' + i);
      console.log(set.existenceDTOList);
      i++;
      if(i == 3){
        vm.thirdItem= set;
      }
    });

    // vm.thirdItem = (vm.dimensionSet[0].existenceDTOList) : 1;
    // angular.forEach(vm.dimensionSet, function (dimension) {
    //   if (vm.dimensionSet.length == 3) {

    //     vm.thirdItem = dimension;

    //   }
    // });
    console.log('vm.thirdItem');
    console.log(vm.thirdItem);

    // vm.thirdItem = (vm.dimensionSet[0].existenceDTOList) : 1;
    angular.forEach(vm.dimensionSet, function (dimension) {
      if (dimension.type == 'BASIC') {
        vm.basicDim.push(dimension);

        vm.basicDimId.push(dimension.id);
        
        console.log('dimension.existenceDTOList' + j);
        console.log(dimension.existenceDTOList);
        vm.exist = dimension.existenceDTOList;

        vm.exiList[j] = [];
        angular.forEach(vm.exist, function (ex) {
          vm.exiList[j].push(ex.id);
          vm.exitList.push(ex.id);
        })
        j++;
        console.log('vm.exiList');
        console.log(vm.exiList)

        console.log('vm.exitList');
        console.log(vm.exitList)

      }
    });

    console.log('vm.basicDim');
    console.log(vm.basicDim);

    console.log('vm.basicDimId');
    console.log(vm.basicDimId);
    vm.dimLength = vm.basicDim.length;
    console.log('vm.dimLength..');
    console.log(vm.dimLength);
    vm.rate1 = [];
    vm.array2 = [[], []];
    $scope.inputCounter =0;
    if (vm.dimLength === 1){
      console.log('one array');
    }
    else if (vm.dimLength === 2) {
      console.log('two array');
    }
    $timeout(function () {
      angular.element('.form-group:eq(1)>input').focus();
    });

    function clear() {
      $uibModalInstance.dismiss('cancel');
    }
    vm.rates = [];

    // Converting the JSON string with JSON.stringify()
    // then saving with localStorage in the name of rates
    //localStorage.setItem('rates', JSON.stringify(rates));

    function save() {
      vm.isSaving = true;
      if (vm.dimLength === 1) {
        console.log(' array1');
        vm.rate1 = vm.rate1;
        console.log(vm.rate1);
        angular.forEach(vm.rate1, function (value, index) {
          console.log('index');
          console.log(index); // 0, 1, 2, 3
          console.log('value');
          console.log(value); // A, B, C, D
          vm.rates.push({ 'contractId': vm.entity.contractId, 'dim1': vm.exitList[index],  'value': value });
        });
          console.log('rates...');
          console.log(vm.rates);
          RatesService.createList(vm.rates, onSaveSuccess, onSaveError);
      }
      else if (vm.dimLength === 2) {
        console.log('two array');
        vm.array2 = vm.array2;
        console.log(vm.array2);
        angular.forEach(vm.array2, function (value, index) {
          console.log('index');
          console.log(index); // 0, 1, 2, 3
          console.log('value');
          console.log(value); // A, B, C, D
          
          angular.forEach(value, function (obj, i) {
            console.log('i');
            console.log(index,  ',', i); // 0, 1, 2, 3
            console.log('obj');
            console.log(obj); // A, B, C, D
            vm.rates.push({ 'contractId': vm.entity.contractId, 'dim1': vm.exiList[0][i], 'dim2': vm.exiList[1][index], 'value': obj });
          });
        });
        
        
        console.log('rates');
        console.log(vm.rates);
        RatesService.createList(vm.rates, onSaveSuccess, onSaveError);
      }
      else if (vm.dimLength === 3) {
        console.log('three array');
        vm.array3 = vm.array3;
        console.log(vm.array3);
        angular.forEach(vm.array3, function (value, index) {
          console.log('index');
          console.log(index); // 0, 1, 2, 3
          console.log('value');
          console.log(value); // A, B, C, D
          
          angular.forEach(value, function (obj, i) {
            console.log('i');
            console.log(index, ',', i); // 0, 1, 2, 3
            console.log('obj');
            console.log(obj); // A, B, C, D
          
          angular.forEach(obj, function (obj1, j) {
            console.log('j');
            console.log(index, ',', i, ',', j); // 0, 1, 2, 3
            console.log('obj1');
            console.log(obj1); // A, B, C, D
            vm.rates.push({ 'contractId': vm.entity.contractId, 'dim1': vm.exiList[0][j], 'dim2': vm.exiList[1][i], 'dim3': vm.exiList[2][index], 'value': obj1 });
          });
        });
        });


        console.log('rates');
        console.log(vm.rates);
        RatesService.createList(vm.rates, onSaveSuccess, onSaveError);
      }
      
     
    }

    function onSaveSuccess(result) {
      //$scope.$emit('gatewayApp:locationUpdate', result);
      $uibModalInstance.close(result);
      vm.isSaving = false;
    }

    function onSaveError() {
      vm.isSaving = false;
    }
  }
})();
