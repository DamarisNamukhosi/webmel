(function() {
    'use strict';

    angular.module('webApp').config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('contract-rates', {
                parent: 'app',
                url: '/{id}rates',
                data: {
                    requiresAuthentication: true,
                    authorities: ['MASTER_USER'],
                    pageTitle: 'rates'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/rates/rates.html',
                        controller: 'RatesController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    rates: [
                        '$stateParams',
                        'RatesService',
                        function($stateParams, RatesService) {
                            return RatesService.query().$promise;
                        }
                    ]
                }
            })
            .state('contract-rates.new', {
                parent: 'rates',
                url: '/new',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'rates-own.detail.title'
                },
                onEnter: [
                    '$stateParams',
                    '$state',
                    '$localStorage',
                    '$uibModal',
                    function($stateParams, $state, $localStorage, $uibModal) {
                        $uibModal
                            .open({
                                templateUrl:
                                    'core/routes/rates/route-create-dialog.html',
                                controller: 'RatesCreateDialogController',
                                controllerAs: 'vm',
                                backdrop: 'static',
                                size: 'lg',
                                resolve: {
                                    entity: function() {
                                        return {
                                            contractId: null,
                                            contractName: null,
                                            dim1: null,
                                            dim1null: null,
                                            dim11: null,
                                            dim12: null,
                                            dim13: null,
                                            dim14: null,
                                            dim15: null,
                                            dim2: null,
                                            dim3: null,
                                            dim4: null,
                                            dim5: null,
                                            dim6: null,
                                            dim7: null,
                                            dim8: null,
                                            dim9: null,
                                            id: null,
                                            uuid: null,
                                            value: null
                                        };
                                    },
                                    setParentVisible: function() {
                                        return true;
                                    }
                                }
                            })
                            .result.then(
                                function() {
                                    $state.go('rates', null, {
                                        reload: 'rates'
                                    });
                                },
                                function() {
                                    $state.go('rates');
                                }
                            );
                    }
                ]
            })
            .state('rates-dimension', {
                parent: 'app',
                url: '/{id}/dimensions',
                data: {
                    requiresAuthentication: true,
                    authorities: [],
                    pageTitle: 'rates'
                },
                views: {
                    'content@': {
                        templateUrl: 'core/routes/rates/rates.html',
                        controller: 'RatesController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    rates: [
                        '$stateParams',
                        'DimensionsService',
                        function($stateParams, DimensionsService) {
                            return DimensionsService.getServiceDimensionSetsByServiceId(
                                { id: $stateParams.id }
                            ).$promise;
                        }
                    ]
                }
            });
    }
})();
