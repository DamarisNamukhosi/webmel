(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FullEventDialogController', FullEventDialogController);

    FullEventDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'EventsService', 'EventTypesService'];

    function FullEventDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, EventsService, EventTypesService) {
        var vm = this;

        vm.event = entity;
        vm.clear = clear;
        vm.save = save;
        vm.initDates= initDates;


        initDates();
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function initDates () {
            if(vm.event.id !== null){
                vm.event.effectiveTime = new Date(vm.event.effectiveTime);
                vm.event.expiryTime= new Date(vm.event.expiryTime);
                
            }
        }
        function save () {
            vm.isSaving = true;
            if (vm.event.id !== null) {
                console.log('This is it');
                console.log(vm.event);
                EventsService.updateFullEvent(vm.event, onSaveSuccess, onSaveError);
            } else {
                EventsService.createFullEvent(vm.event, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
