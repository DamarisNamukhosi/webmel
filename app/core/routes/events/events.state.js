(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("events", {
        parent: "app",
        url: "/events",
        data: {
          requiresAuthentication: true,
          authorities: ["RESTAURANT_USER", "PROPERTY_USER", "D_AUTHORITY_USER"],
          pageTitle: "events"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/events.html",
            controller: "EventsController",
            controllerAs: "vm"
          }
        },
        resolve: {
          events: [
            "$stateParams",
            "$localStorage",
            "EventsService",
            function($stateParams, $localStorage, EventsService) {
              return EventsService.getFullEventsByOrganization({
                id: $localStorage.current_organisation.id
              }).$promise;
            }
          ]
        }
      })
      .state("events.new", {
        parent: "events",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "events.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-dialog.html",
                controller: "FullEventDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      cycle: null,
                      effectiveTime: null,
                      expiryTime: null,
                      contentStatus: "DRAFT",
                      statusReason: null,
                      icon: null,
                      organisationId: $localStorage.current_organisation.id,
                      organisationName: null
                    };
                  },
                  locationSet: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("events", null, { reload: "events" });
                },
                function() {
                  $state.go("events");
                }
              );
          }
        ]
      })
      .state("event-detail", {
        parent: "app",
        url: "/event/{fullId}/instance/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Event Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/event-detail.html",
            controller: "EventDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          fullEvent: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              console.log("full-event details service,,,,");
              console.log($stateParams.fullId);
              console.log($stateParams.id);
              console.log(
                EventsService.getFullEvent({ id: $stateParams.fullId }).$promise
              );
              return EventsService.getFullEvent({ id: $stateParams.fullId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "events",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })

      .state("event-detail.edit", {
        parent: "event-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/event-dialog.html",
                controller: "EventDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      var data = EventsService.getFull({ id: $stateParams.id })
                        .$promise;
                      return data;
                    }
                  ],
                  locationSet: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("event-detail", null, { reload: "event-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("events.edit", {
        parent: "events",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-dialog.html",
                controller: "FullEventDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      var data = EventsService.getFullEvent({
                        id: $stateParams.id
                      }).$promise;
                      console.log(data);
                      return data;
                    }
                  ],
                  locationSet: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("events", null, { reload: "events" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-detail.delete", {
        parent: "event-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/event-delete-dialog.html",
                controller: "EventDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-detail", null, { reload: "event-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-detail.delete", {
        parent: "full-event-detail",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-delete-dialog.html",
                controller: "FullEventDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.getFull({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-detail", {
        parent: "app",
        url: "/full-event/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Event Details"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/full-event-detail.html",
            controller: "FullEventDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getFullEvent({ id: $stateParams.id })
                .$promise;
            }
          ],
          eventInstances: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getEventsByFullEvent({ id: $stateParams.id })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state) {
              var currentStateData = {
                name: $state.current.name || "events",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("full-event-detail.edit", {
        parent: "full-event-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/full-event-dialog.html",
                controller: "FullEventDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      var data = EventsService.getFullEvent({
                        id: $stateParams.id
                      }).$promise;
                      console.log(data);
                      return data;
                    }
                  ],
                  locationSet: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-detail-new-instance", {
        parent: "full-event-detail",
        url: "/event/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "events.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/events/event-dialog.html",
                controller: "EventDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      evntInstanceDTO: {
                        id: null,
                        uuid: null,
                        name: null,
                        brief: null,
                        description: null,
                        startTime: null,
                        endTime: null,
                        contentStatus: "DRAFT",
                        statusReason: null,
                        evntId: $stateParams.id,
                        evntName: null,
                        locationId: null,
                        locationName: null
                      }
                    };
                  },
                  locationSet: function() {
                    return false;
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("full-event-detail");
                }
              );
          }
        ]
      })
      .state("full-event-detail.image", {
        parent: "full-event-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/events/event-image-upload-dialog.html",
                controller: "EventImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  event: [
                    "$stateParams",
                    "EventsService",
                    function($stateParams, EventsService) {
                      return EventsService.getFull({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-detail.image", {
        parent: "event-detail",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/events/event-image-upload-dialog.html",
                controller: "EventImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  event: [
                    "$stateParams",
                    "EventsService",
                    function($stateParams, EventsService) {
                      return EventsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-detail", null, { reload: "event-detail" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-galleries", {
        parent: "app",
        url: "/event/{fullId}/instance/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS","ORGANISATION_ADMIN"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/event-galleries.html",
            controller: "EventGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          fullEvent: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getFullEvent({ id: $stateParams.fullId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "event-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("event-galleries.delete", {
        parent: "event-galleries",
        url: "/{albumId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-delete-dialog.html",
                controller: "AlbumDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("event-galleries", null, {
                    reload: "event-galleries"
                  });
                },
                function() {
                  $state.go("event-galleries");
                }
              );
          }
        ]
      })
      .state("event-galleries.new", {
        parent: "event-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("event-galleries", null, {
                    reload: "event-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-album-detail", {
        parent: "event-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/event-album-detail.html",
            controller: "EventAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          fullEvent: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getFullEvent({ id: $stateParams.fullId })
                .$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "event-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("event-album-detail.makeCoverImage", {
        parent: "event-album-detail",
        url: "/{imageId}/cover-image",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-change-cover-image-dialog.html",
                controller: "AlbumChangeCoverImageDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-album-detail", null, {
                    reload: "event-album-detail"
                  });
                },
                function() {
                  $state.go("event-album-detail");
                }
              );
          }
        ]
      })
      .state("event-album-detail.deleteImage", {
        parent: "event-album-detail",
        url: "/{imageId}/delete",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "locations-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-image-delete-dialog.html",
                controller: "AlbumImageDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "AlbumService",
                    "$stateParams",
                    function(AlbumService, $stateParams) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-album-detail", null, {
                    reload: "event-album-detail"
                  });
                },
                function() {
                  $state.go("event-album-detail");
                }
              );
          }
        ]
      })
      .state("event-album-detail.edit", {
        parent: "event-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AlbumService",
                    function(AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-album-detail", null, {
                    reload: "event-album-detail"
                  });
                },
                function() {
                  $state.go("event-album-detail");
                }
              );
          }
        ]
      })
      .state("event-album-detail.upload", {
        parent: "event-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-album-detail", null, {
                    reload: "event-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-detail.addLabels", {
        parent: "event-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("event-detail", null, {
                    reload: "event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("event-detail.deleteLabel", {
        parent: "event-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("event-detail", null, {
                    reload: "event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-galleries", {
        parent: "app",
        url: "/full-event/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS","ORGANISATION_ADMIN"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/full-event-galleries.html",
            controller: "FullEventGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getFull({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "full-event-detail",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("full-event-galleries.new", {
        parent: "full-event-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.getFull({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-galleries", null, {
                    reload: "full-event-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-album-detail", {
        parent: "full-event-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: ["MASTER_USER", "D_AUTHORITY_USER", "MANAGE_LOCATIONS"],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/events/full-event-album-detail.html",
            controller: "FullEventAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "EventsService",
            function($stateParams, EventsService) {
              return EventsService.getFull({ id: $stateParams.id }).$promise;
            }
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId })
                .$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "full-event-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("full-event-album-detail.edit", {
        parent: "full-event-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "AlbumService",
                    function(AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-album-detail", null, {
                    reload: "full-event-album-detail"
                  });
                },
                function() {
                  $state.go("event-album-detail");
                }
              );
          }
        ]
      })
      .state("full-event-album-detail.upload", {
        parent: "full-event-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-album-detail", null, {
                    reload: "full-event-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-detail.addLabels", {
        parent: "full-event-detail",
        url: "/{uuid}/{objectType}/add-labels",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-create-dialog.html",
                controller: "ObjectLabelGroupCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  labelGroups: [
                    "LabelGroupsService",
                    function(LabelGroupsService) {
                      return LabelGroupsService.getLabelGroupsByObjectType({
                        objectType: $stateParams.objectType
                      }).$promise;
                    }
                  ],
                  entity: [
                    "EventsService",
                    function(EventsService) {
                      return EventsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  savedselectedLabels: [
                    "ObjectLabelsService",
                    function(ObjectLabelsService) {
                      return ObjectLabelsService.getLabelsByObjectId({
                        uuid: $stateParams.uuid
                      }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("full-event-detail.deleteLabel", {
        parent: "full-event-detail",
        url: "/{itemToDelete}/delete-label",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/labels/object-label-delete-dialog.html",
                controller: "ObjectLabelGroupDeleteDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md"
              })
              .result.then(
                function() {
                  $state.go("full-event-detail", null, {
                    reload: "full-event-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
  }
})();
