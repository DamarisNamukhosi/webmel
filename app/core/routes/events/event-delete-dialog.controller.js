(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('EventDeleteDialogController',EventDeleteDialogController);

    EventDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'EventsService'];

    function EventDeleteDialogController($uibModalInstance, entity, EventsService) {
        var vm = this;

        vm.event = entity;
        console.log('event:');

        console.log(vm.event);
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function confirmDelete(event) {
            console.log('inside confirm delete fn');
            console.log(event);
            event.contentStatus = "DELETED";
            EventsService.update(event,
                  function () {
                      $uibModalInstance.close(true);
                  });
        }
    }
})();
