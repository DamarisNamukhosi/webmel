(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("FullEventDetailController", FullEventDetailController);

  FullEventDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "ObjectLabelsService",
    "eventInstances",
    "URLS"
  ];

  function FullEventDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    ObjectLabelsService,
    eventInstances,
    URLS
  ) {
    var vm = this;

    vm.fullEvent = entity;
    vm.eventInstances = eventInstances;

    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.fullEvent.uuid });
    console.log(vm.objectLabels);

    vm.previousState = previousState.name;
    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.fullEvent.uuid;
    vm.recordsNotFound =
      vm.eventInstances === undefined || vm.eventInstances.length === 0;
  }
})();
