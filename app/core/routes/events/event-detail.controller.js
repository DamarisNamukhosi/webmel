(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("EventDetailController", EventDetailController);

  EventDetailController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "previousState",
    "entity",
    "fullEvent",
    "ObjectLabelsService",
    "EventsService",
    "URLS"
  ];

  function EventDetailController(
    $scope,
    $rootScope,
    $stateParams,
    previousState,
    entity,
    fullEvent,
    ObjectLabelsService,
    EventsService,
    URLS
  ) {
    var vm = this;

    vm.event = entity;
    vm.fullEvent = fullEvent;
    console.log("full event id...");
    console.log(vm.fullEvent);
    vm.previousState = previousState.name;

    vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.event.uuid });
    console.log(vm.objectLabels);

    vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.event.uuid;
    console.log("inside a specific events....");
    console.log(vm.event);
  }
})();
