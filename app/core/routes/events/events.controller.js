(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('EventsController', EventsController);

    EventsController.$inject = ['$scope', '$location', '$state', 'events', 'URLS'];

        function EventsController($scope, $location, $state, events, URLS) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;

        vm.events = events;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL;

        vm.noRecordsFound = (vm.events === null || vm.events.length === 0);

    }
})();
