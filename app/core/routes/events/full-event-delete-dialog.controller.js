(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('FullEventDeleteDialogController',FullEventDeleteDialogController);

    FullEventDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'EventsService'];

    function FullEventDeleteDialogController($uibModalInstance, entity, EventsService) {
        var vm = this;

        vm.event = entity;
        console.log('event:');

        console.log(vm.event);
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
        function confirmDelete(event) {
            console.log('inside confirm delete fn');
            console.log(event);
            
            console.log('content status is');
            console.log(event.contentStatus);
            
            event.contentStatus = "DELETED";

            console.log('inside confirm delete fn');
            
            console.log('update content status is');
           
            console.log(event.contentStatus);

            console.log('updating full envents.... ');
            console.log(event);

            console.log('evntDTO for full envents.... ');
            console.log(event);

            EventsService.updateFullEvent(event,
                  function () {
                      $uibModalInstance.close(true);
                  });
        }
    }
})();
