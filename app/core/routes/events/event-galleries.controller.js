(function() {
  "use strict";

  angular
    .module("webApp")
    .controller("EventGalleriesController", EventGalleriesController);

  EventGalleriesController.$inject = [
    "$scope",
    "$rootScope",
    "$stateParams",
    "$localStorage",
    "previousState",
    "entity",
    'fullEvent',
    "URLS",
    "AlbumService"
  ]; //

  function EventGalleriesController(
    $scope,
    $rootScope,
    $stateParams,
    $localStorage,
    previousState,
    entity,
    fullEvent,
    URLS,
    AlbumService
  ) {
    var vm = this;

    vm.entity = entity;
    vm.fullEvent = fullEvent;

    vm.albums = AlbumService.getAlbums({ uuid: vm.entity.uuid });

    vm.image_url = URLS.FILE_URL;
    vm.previousState = previousState.name;

    var permissions = $localStorage.current_user_permissions;

    vm.previousState =
      permissions.indexOf("MASTER_USER") > -1 ? "locations" : "locations-own";
  }
})();
