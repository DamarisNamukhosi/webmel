(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('EventDialogController', EventDialogController);

        EventDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'locationSet', 'EventsService', 'EventTypesService', 'LocationsService'];

    function EventDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, locationSet, EventsService, EventTypesService, LocationsService) {
        var vm = this;

        vm.event = entity;
        vm.clear = clear;
        vm.save = save;

        vm.locations = [];

        if(!locationSet){
            vm.locations = LocationsService.getLocations();
        }

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.event.id !== null) {
                EventsService.update(vm.event.evntInstanceDTO, onSaveSuccess, onSaveError);
            } else {
                EventsService.create(vm.event.evntInstanceDTO, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
