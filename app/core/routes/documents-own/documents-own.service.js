(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('DocumentsOwnService', DocumentsOwnService);

        DocumentsOwnService.$inject = ['$resource', '$localStorage', 'URLS'];

    function DocumentsOwnService($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'getDocuments': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/object-documents'
          },
          'getDocumentsByOrganizationUuid': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/object-documents/filter-by-object/:uuid'
          },
          'getGeneralDocumentDetails': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/general-documents/filter-by-organisation/:id'
          },
          'get': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: false,
            url: URLS.BASE_URL + 'contentservice/api/object-documents/:id'
          },
          'update': {
            method: 'PUT',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/object-documents'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/object-documents'
          },
          'delete': {
            method: 'DELETE',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/object-documents/:id'
          }
        });
    }
})();
