(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentOwnDeleteDialogController',DocumentOwnDeleteDialogController);

        DocumentOwnDeleteDialogController.$inject = ['$uibModalInstance', 'entity', 'DocumentsOwnService', "$state"];

    function DocumentOwnDeleteDialogController($uibModalInstance, entity, DocumentsOwnService, $state) {
        var vm = this;

        vm.document = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocumentsOwnService.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
