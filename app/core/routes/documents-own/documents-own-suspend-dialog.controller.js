(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "DocumentOwnSuspendDialogController",
      DocumentOwnSuspendDialogController
    );

  DocumentOwnSuspendDialogController.$inject = [
    "$uibModalInstance",
    "entity",
    "DocumentsOwnService"
  ];

  function DocumentOwnSuspendDialogController(
    $uibModalInstance,
    entity,
    DocumentsOwnService
  ) {
    var vm = this;

    vm.document = entity;
    console.log(vm.document);
    vm.clear = clear;
    vm.confirmSuspend = confirmSuspend;

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirmSuspend(document) {
      document.documentStatus = "SUSPENDED";
      console.log(document);
      DocumentsOwnService.update(document, function() {
        $uibModalInstance.close(true);
      });
    }
  }
})();
