(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentsOwnController', DocumentsOwnController);

        DocumentsOwnController.$inject = ['$scope', '$location', '$state', 'documents', '$localStorage', 'URLS'];

    function DocumentsOwnController ($scope, $location, $state, documents, $localStorage, URLS) {
        var vm = this;

        vm.account = $localStorage.current_organisation;
        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.account.uuid;
        vm.isAuthenticated = null;
        vm.records = documents;

        console.log(vm.records);
        vm.recordsFound = (vm.records !== undefined && vm.records.length > 0);
    }
})();
