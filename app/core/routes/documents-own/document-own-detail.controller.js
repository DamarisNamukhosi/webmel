(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentOwnDetailController', DocumentOwnDetailController);

    DocumentOwnDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DocumentsOwnService'];

    function DocumentOwnDetailController($scope, $rootScope, $stateParams, previousState, entity, DocumentsOwnService) {
        var vm = this;

        vm.document = entity;

        console.log(vm.document);

        vm.previousState = previousState.name;
    }
})();
