(function() {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state("documents-own", {
        parent: "app",
        url: "/documents-own",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Documents"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents-own/documents-own.html",
            controller: "DocumentsOwnController",
            controllerAs: "vm"
          }
        },
        resolve: {
          documents: [
            "$stateParams",
            "$localStorage",
            "DocumentsOwnService",
            function($stateParams, $localStorage, DocumentsOwnService) {
              return DocumentsOwnService.getDocumentsByOrganizationUuid({
                uuid: $localStorage.current_organisation.uuid
              }).$promise;
            }
          ]
        }
      })
      // .state('documents-own-detail', {
      //     parent: 'documents-own',
      //     url: '/documents-own/{id}',
      //     data: {
      //         requiresAuthentication: true,
      //         authorities: [],
      //         pageTitle: 'Documents Details'
      //     },
      //     views: {
      //         'content@': {
      //             templateUrl: 'core/routes/documents-own/document-own-detail.html',
      //             controller: 'DocumentOwnDetailController',
      //             controllerAs: 'vm'
      //         }
      //     },
      //     resolve: {
      //         entity: ['$stateParams', 'DocumentsOwnService', function($stateParams, DocumentsOwnService) {
      //             return DocumentsOwnService.get({id : $stateParams.id}).$promise;
      //         }],
      //         previousState: ["$state", function ($state) {
      //             var currentStateData = {
      //                 name: $state.current.name || 'documents-own',
      //                 params: $state.params,
      //                 url: $state.href($state.current.name, $state.params)
      //             };
      //             return currentStateData;
      //         }]
      //     }
      // })
      .state("documents-own.new", {
        parent: "document-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "documents-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-dialog.html",
                controller: "DocumentOwnDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: function() {
                    return {
                      id: null,
                      uuid: null,
                      name: null,
                      brief: null,
                      description: null,
                      notes: null,
                      effectiveDate: null,
                      expiryDate: null,
                      documentStatus: null,
                      statusReason: null,
                      objectId: $localStorage.current_organisation.uuid,
                      generalDocumentId: null,
                      generalDocumentName: null
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("documents-own", null, { reload: "documents-own" });
                },
                function() {
                  $state.go("documents-own");
                }
              );
          }
        ]
      })
      .state("documents-own.edit", {
        parent: "documents-own",
        url: "/documents-own/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-dialog.html",
                controller: "DocumentOwnDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "DocumentsOwnService",
                    function(DocumentsOwnService) {
                      return DocumentsOwnService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("documents-own", null, { reload: "documents-own" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("documents-own.delete", {
        parent: "documents-own",
        url: "/{id}/delete",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/document-own-delete-dialog.html",
                controller: "DocumentOwnDeleteDialogController",
                controllerAs: "vm",
                size: "md",
                resolve: {
                  entity: [
                    "DocumentsOwnService",
                    function(DocumentsOwnService) {
                      return DocumentsOwnService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("documents-own", null, { reload: "documents-own" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("document-own.profile", {
        parent: "app",
        url: "/image/upload",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: ""
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/documents-own/image-upload/image-upload-dialog.html",
                controller: "DocumentOwnImageUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: function() {
                    return {
                      file: null
                    };
                  },
                  vehicle: [
                    "$stateParams",
                    "DocumentsOwnService",
                    function($stateParams, DocumentsOwnService) {
                      return DocumentsOwnService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("app", null, { reload: "app" });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("document-own-galleries", {
        parent: "app",
        url: "/documents-own/{id}/gallery",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents-own/document-own-galleries.html",
            controller: "DocumentOwnGalleriesController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "DocumentOwnsService",
            function($stateParams, DocumentOwnsService) {

              return DocumentOwnsService.get({ id: $stateParams.id }).$promise;
            }
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "locations-own",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("document-own-galleries.new", {
        parent: "document-own-galleries",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/albums/album-create-dialog.html",
                controller: "AlbumCreateDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "DocumentOwnsService",
                    function(DocumentOwnsService) {
                      return DocumentOwnsService.get({ id: $stateParams.id })
                        .$promise;
                    }
                  ],
                  album: function() {
                    return {
                      albumType: "GENERAL",
                      caption: null, //album name
                      coverName: null, //uploaded cover image file name
                      coverUuid: null, //uploaded cover image
                      isDefaultAlbum: true, //put option slider
                      name: null, //album name
                      objectUuid: null //location uuid
                    };
                  }
                }
              })
              .result.then(
                function() {
                  $state.go("document-own-galleries", null, {
                    reload: "document-own-galleries"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      })
      .state("document-own-album-detail", {
        parent: "document-own-galleries",
        url: "/{albumId}/album",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "Gallery"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/documents-own/document-own-album-detail.html",
            controller: "DocumentOwnAlbumDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "DocumentOwnsService",
            function($stateParams, DocumentOwnsService) {
              return DocumentOwnsService.get({ id: $stateParams.id }).$promise;
            },
          ],
          album: [
            "$stateParams",
            "AlbumService",
            function($stateParams, AlbumService) {
              return AlbumService.getAlbum({ albumId: $stateParams.albumId }).$promise;
            },
          ],
          previousState: [
            "$state",
            function($state, $) {
              var currentStateData = {
                name: $state.current.name || "document-own-galleries",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state("document-own-album-detail.edit", {
        parent: "document-own-album-detail",
        url: "/edit",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "document-own-own.detail.title"
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$localStorage",
          "$uibModal",
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                "core/routes/albums/album-dialog.html",
                controller: "AlbumDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: ["AlbumService",function (AlbumService) {
                    return AlbumService.get({ id: $stateParams.albumId }).$promise;
                  }
                  ]
                }
              })
              .result.then(
              function () {
                $state.go("document-own-album-detail", null, { reload: "document-own-album-detail" });
              },
              function () {
                $state.go("document-own-album-detail");
              }
              );
          }
        ]
      })
      .state("document-own-album-detail.upload", {
        parent: "document-own-album-detail",
        url: "/new",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl:
                  "core/routes/albums/album-upload-dialog.html",
                controller: "AlbumUploadDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "lg",
                resolve: {
                  entity: [
                    "$stateParams",
                    "AlbumService",
                    function ($stateParams, AlbumService) {
                      return AlbumService.get({ id: $stateParams.albumId }).$promise;
                    }
                  ]
                }
              })
              .result.then(
                function() {
                  $state.go("document-own-album-detail", null, {
                    reload: "document-own-album-detail"
                  });
                },
                function() {
                  $state.go("^");
                }
              );
          }
        ]
      });
  }
})();
