(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('DocumentOwnDialogController', DocumentOwnDialogController);

        DocumentOwnDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DocumentsOwnService', 'GeneralDocumentsService'];

    function DocumentOwnDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DocumentsOwnService,GeneralDocumentsService) {
        var vm = this;

        vm.document = entity;
        vm.effectiveDate = new Date(vm.document.effectiveDate);
        vm.expiryDate = new Date(vm.document.expiryDate);
        vm.clear = clear;
        vm.save = save;

       // vm.locationTypes = LocationTypesService.query();
       vm.generalDocumentTypes = GeneralDocumentsService.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.document.id !== null) {
                DocumentsOwnService.update(vm.document, onSaveSuccess, onSaveError);
            } else {
                DocumentsOwnService.create(vm.document, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            //$scope.$emit('gatewayApp:locationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
