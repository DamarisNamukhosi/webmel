(function () {
  "use strict";

  angular.module("webApp").config(stateConfig);

  stateConfig.$inject = ["$stateProvider"];

  function stateConfig($stateProvider) {
    $stateProvider
      .state('welcome-packs', {
        parent: 'app',
        url: '/welcome-packs/welcome-packs',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'WelcomePacks Settings'
        },
        views: {
          'content@': {
            templateUrl: 'core/routes/welcome-packs/welcome-packs.html',
            controller: 'WelcomePacksController',
            controllerAs: 'vm'
          }
        },
        resolve: {
          entity: [
            '$stateParams',
            'WelcomePacksService',
            '$localStorage',
            function ($stateParams, WelcomePacksService, $localStorage) {
              return WelcomePacksService
                .getByOrgId({id : $localStorage.current_organisation.id})
                .$promise;
            }
          ],
          previousState: [
            '$state',
            function ($state) {
              var currentStateData = {
                name: $state.current.name || 'welcome-packs',
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })
      .state('welcome-packs.new', {
        parent: 'welcome-packs',
        url: '/new',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$localStorage',
          '$uibModal',
          function ($stateParams, $state, $localStorage, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/welcome-packs/welcome-packs-create-dialog.html',
                controller: 'WelcomePacksCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    function ($localStorage) {
                      return {
                        contentStatus: 'DRAFT',
                        id: null,
                        isDefault: false,
                        name: null,
                        description: null,
                        safariType: 'SET_DEPARTURE',
                        travellerProfile: 'FAMILY',
                        orgId: $localStorage.current_organisation.id,
                        orgName: $localStorage.current_organisation.name,
                        uuid: null
                      };
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('welcome-packs', null, {
                  reload: 'welcome-packs'
                });
              }, function () {
                $state.go('welcome-packs');
              });
          }
        ]
      })
      .state('welcome-packs.edit', {
        parent: 'welcome-packs',
        url: '/{id}/edit',
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: 'profsApp.professional.detail.title'
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/welcome-packs/welcome-packs-create-dialog.html',
                controller: 'WelcomePacksCreateDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    'WelcomePacksService',
                    function (WelcomePacksService) {
                      console.log($stateParams);
                      console.log($state.params);
                      console.log($state);
                      return WelcomePacksService
                        .getById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('welcome-packs', null, {
                  reload: 'welcome-packs'
                });
              }, function () {
                $state.go('welcome-packs');
              });
          }
        ]
      })
      .state('welcome-packs.delete', {
        parent: 'welcome-packs',
        url: '/{id}/delete',
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          '$stateParams',
          '$state',
          '$uibModal',
          function ($stateParams, $state, $uibModal) {
            $uibModal.open({
                templateUrl: 'core/routes/welcome-packs/welcome-packs-delete-dialog.html',
                controller: 'WelcomePacksDeleteDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md',
                resolve: {
                  entity: [
                    '$localStorage',
                    'WelcomePacksService',
                    function ($localStorage, WelcomePacksService) {
                      return WelcomePacksService
                        .getById({
                          id: $stateParams.id
                        })
                        .$promise;
                    }
                  ]
                }
              })
              .result
              .then(function () {
                $state.go('welcome-packs', null, {
                  reload: 'welcome-packs'
                });
              }, function () {
                $state.go('^');
              });

          }
        ]
      })
      .state("welcome-packs-detail", {
        parent: "welcome-packs",
        url: "/{id}/overview",
        data: {
          requiresAuthentication: true,
          authorities: [],
          pageTitle: "webApp.welcome-packs.detail.title"
        },
        views: {
          "content@": {
            templateUrl: "core/routes/welcome-packs/welcome-packs-detail.html",
            controller: "WelcomePacksDetailController",
            controllerAs: "vm"
          }
        },
        resolve: {
          entity: [
            "$stateParams",
            "WelcomePacksService",
            function ($stateParams, WelcomePacksService) {
              return WelcomePacksService.getById({
                id: $stateParams.id
              }).$promise;
            }
          ],
          serviceMarkups: [
            "$stateParams",
            "WelcomePacksService",
            function ($stateParams, WelcomePacksService) {
              return WelcomePacksService.getServiceMarkups().$promise;
            }
          ],
          previousState: [
            "$state",
            function ($state) {
              var currentStateData = {
                name: $state.current.name || "welcome-packs",
                params: $state.params,
                url: $state.href($state.current.name, $state.params)
              };
              return currentStateData;
            }
          ]
        }
      })


      .state("welcome-packs-detail.edit", {
        parent: "welcome-packs",
        url: "/{id}/edit",
        data: {
          requiresAuthentication: true,
          authorities: []
        },
        onEnter: [
          "$stateParams",
          "$state",
          "$uibModal",
          function ($stateParams, $state, $uibModal) {
            $uibModal
              .open({
                templateUrl: "core/routes/welcome-packs/welcome-packs-dialog.html",
                controller: "WelcomePacksDialogController",
                controllerAs: "vm",
                backdrop: "static",
                size: "md",
                resolve: {
                  entity: [
                    "WelcomePacksService",
                    function (WelcomePacksService) {
                      return WelcomePacksService.get({
                        id: $stateParams.id
                      }).$promise;
                    }
                  ],
                  editParentWelcomePacks: function () {
                    return false;
                  }
                }
              })
              .result.then(
                function () {
                  $state.go("welcome-packs", null, {
                    reload: "welcome-packs"
                  });
                },
                function () {
                  $state.go("^");
                }
              );
          }
        ]
      })
      // .state("welcome-packs-detail.delete", {
      //   parent: "welcome-packs-detail",
      //   url: "/delete",
      //   data: {
      //     requiresAuthentication: true,
      //     authorities: []
      //   },
      //   onEnter: [
      //     "$stateParams",
      //     "$state",
      //     "$uibModal",
      //     function ($stateParams, $state, $uibModal) {
      //       $uibModal
      //         .open({
      //           templateUrl: "core/routes/welcome-packs/welcome-packs-delete-dialog.html",
      //           controller: "WelcomePacksDeleteDialogController",
      //           controllerAs: "vm",
      //           size: "md",
      //           resolve: {
      //             entity: [
      //               "WelcomePacksService",
      //               function (WelcomePacksService) {
      //                 return WelcomePacksService.get({
      //                   id: $stateParams.id
      //                 }).$promise;
      //               }
      //             ]
      //           }
      //         })
      //         .result.then(
      //           function () {
      //             $state.go("welcome-packs", null, {
      //               reload: "welcome-packs"
      //             });
      //           },
      //           function () {
      //             $state.go("welcome-packs");
      //           }
      //         );
      //     }
      //   ]
      // });
  }
})();
