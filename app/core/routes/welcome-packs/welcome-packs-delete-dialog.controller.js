(function() {
  "use strict";

  angular
    .module("webApp")
    .controller(
      "WelcomePacksDeleteDialogController",
      WelcomePacksDeleteDialogController
    );

  WelcomePacksDeleteDialogController.$inject = [
    "$timeout",
    "$scope",
    "$stateParams",
    "$uibModalInstance",
    "WelcomePacksService",
    "entity"
  ];

  function WelcomePacksDeleteDialogController(
    $timeout,
    $scope,
    $stateParams,
    $uibModalInstance,
    WelcomePacksService,
    entity
  ) {
    var vm = this;

    vm.entity = entity;
    vm.clear = clear;
    vm.confirmDelete = confirmDelete;

    vm.clickedItem = $stateParams.id;
    console.log("Id to delete: " + vm.clickedItem);

    function clear() {
      $uibModalInstance.dismiss("cancel");
    }

    function confirmDelete(entity) {
      console.log('changing welcome pack contentStatus to DELETED');
      vm.entity.contentStatus = "DELETED";
      WelcomePacksService.update(entity,
        function () {
          $uibModalInstance.close(true);
        });
    }
  }
})();
