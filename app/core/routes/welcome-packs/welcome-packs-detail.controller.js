(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('MarkupCategoriesDetailController', MarkupCategoriesDetailController);

    MarkupCategoriesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', '$localStorage', 'ObjectLabelsService', 'URLS', "serviceMarkups"];

    function MarkupCategoriesDetailController($scope, $rootScope, $stateParams, previousState, entity,  $localStorage, ObjectLabelsService, URLS, serviceMarkups) {
        var vm = this;

        vm.markupCategories = entity;

        vm.serviceMarkups = serviceMarkups;


        vm.profile_image_url = URLS.PROFILE_IMAGE_URL + vm.markupCategories.uuid;

        vm.previousState = previousState.name;

        vm.markupCategoriesNotFound = (vm.markupCategories === undefined || vm.markupCategories.length === 0);
        vm.markupCategoriesItemsNotFound = (vm.markupCategoriesItems === undefined || vm.markupCategoriesItems.length === 0);

        vm.objectLabels = ObjectLabelsService.getLabelsByObjectId({ uuid: vm.markupCategories.uuid });
        console.log('object labels...');
        console.log(vm.objectLabels);

    }
})();
