(function() {
  "use strict";
  angular.module("webApp").factory("WelcomePacksService", WelcomePacksService);

  WelcomePacksService.$inject = ["$resource", "$localStorage", "URLS"];

  function WelcomePacksService($resource, $localStorage, URLS) {
    var resourceUrl = "";

    return $resource(
      resourceUrl,
      {},
      {
        getWelcomePacks: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url:
            URLS.BASE_URL +
            "costingservice/api/safari-requirements-packs/"
        },
        getById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs/:id"
        },
        getByOrgId: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs/filter-by-organisation/:id"
        },
        //
        get: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs"
        },
        update: {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs"
        },
        create: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs"
        },
        delete: {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "costingservice/api/safari-requirements-packs/:id"
        }
      }
    );
  }
})();
