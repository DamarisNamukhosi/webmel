(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$state', 'AuthService', '$localStorage', 'NavbarService', '$http','safariConstants'];

  function NavbarController($state, AuthService, $localStorage, NavbarService, $http,safariConstants) {
    var vm = this;

    vm.isAuthenticated = isAuthenticated;
    vm.logout = logout;
    console.log(vm.isAuthenticated());
    vm.profile = $localStorage.account;
    vm.packageTypes =safariConstants.packageTypes

    function isAuthenticated() {
   return $localStorage.hasOwnProperty('user');
    }

    vm.isNavbarCollapsed = true;

    vm.toggleNavbar = toggleNavbar;
    vm.collapseNavbar = collapseNavbar;
    // vm.switchAccount = switchAccount;
    vm.$state = $state;

    //get managed organisations
    if ($localStorage.hasOwnProperty('account')) {
      vm.user = $localStorage.account;
      vm.currentOrganization = $localStorage.current_organisation;
      vm.organizations = $localStorage.current_managed_organisations;
      vm.parentOrganization = $localStorage.account.organisation;
      vm.immediateParentOrganization = $localStorage.current_immediate_parent_organisation;
    }


    vm.switchAccount = function (orgId) {
      console.log('id: ' + orgId);

      console.log( $localStorage.user);

      var data = NavbarService.switchOrganization({ id: orgId }).$promise
        .then(function (response) {
          console.log(response);
          //update location_id
          $localStorage.current_location_id = response.locationId;
          //update current_organisation
          $localStorage.current_organisation = response.organisation;
          $localStorage.current_managed_organisations = response.managedOrganisations;
          //update current_user_permissions
          $localStorage.current_user_permissions = response.authorities;

          $localStorage.current_immediate_parent_organisation = response.immediateParent;

          //go to home page
          if ($state.includes('home')) {
            $state.reload();
          } else {
            $state.go("home", {}, {reload: true});
          }
        }, function (error) {
          console.log(error);
        });

    }

    function toggleNavbar() {
      vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
    }

    function collapseNavbar() {
      vm.isNavbarCollapsed = true;
    }

    function logout() {
      AuthService.logout();
      $state.go('login');
    }
  }
})();
