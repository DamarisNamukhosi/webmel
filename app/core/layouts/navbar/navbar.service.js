(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('NavbarService', NavbarService);

    NavbarService.$inject = ['$resource', '$localStorage', 'URLS'];

    function NavbarService ($resource, $localStorage, URLS) {
        var resourceUrl =  'data/data.json';

        return $resource(resourceUrl, {}, {
          'switchOrganization': {
            method: 'GET',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            url: URLS.BASE_URL + 'contentservice/api/account/switch/:id'
          }
        });
    }
})();

