(function () {
  'use strict';

  angular
    .module('webApp', [
      'ngCookies',
      'ngResource',
      'ui.router',
      'ui.bootstrap',
      'ngSanitize',
      'angular-loading-bar',
      'chart.js',
      'ngStorage',
      'ngFileUpload',
      'ngMap',
      'ngMessages',
      'multipleSelect',
      'ui.sortable',
      'ngFlash',
      'uiCropper',
      'pdf',
      'ngDomToPdf',
      'angularUtils.directives.dirPagination',
      'smart-table',
      'ui.bootstrap.datetimepicker'
    ])
    .run(run)
    .config([
      'ChartJsProvider',
      function (ChartJsProvider) {
        ChartJsProvider.setOptions('global', {
          colors: [
            '#00ADF9',
            '#46BFBD',
            '#803690',
            '#DCDCDC',
            '#FDB45C',
            '#949FB1',
            '#4D5360'
          ]
        });
      }
    ])
    .config(['$httpProvider', function($httpProvider){
      $httpProvider.interceptors.push('sessionRecoverer')
    }])
    ;

  run.$inject = ['StateHandler'];

  function run(StateHandler) {
    StateHandler.initialize();
  }
})();
