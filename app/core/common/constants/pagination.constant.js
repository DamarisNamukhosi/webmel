(function () {
    'use strict';

    angular
        .module('webApp')
        .constant('paginationConstants', {
            'itemsPerPage': 12
        });
})();
