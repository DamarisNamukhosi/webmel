(function () {
  'use strict';

  angular
    .module('webApp')
    .constant('URLS', {
      BASE_URL: 'http://51.15.233.87:8080/',
      MEDIA_BASE_URL: 'http://51.15.233.87:8080/mediaservice',
      GOOGLE_MAP_URL: 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBetzn287R0D4vcFpdUpzU01YyATpB221E',
      PROFILE_IMAGE_URL: 'http://51.15.233.87:8080/mediaservice/api/albums/get-profile/',
      FILE_URL: 'http://51.15.233.87:8080/mediaservice/api/media-items/get-file/',
      MEDIA_ITEM_UPLOAD: 'http://51.15.233.87:8080/mediaservice/api/media-items/upload'
    })
    .run(function ($rootScope, URLS, $stateParams, $localStorage) {
      $rootScope.URLS = URLS;
      $rootScope.stateObject = $stateParams;
      $rootScope.localStorageObject = $localStorage;
    });
})();
