(function () {
  'use strict';

  angular
    .module('webApp')
    .constant('safariConstants', {
      travellerProfile: ["VIP", "FAMILY", "CORPORATE", "AGENT", "HONEYMOONERS", "FRIENDS", "OTHER"],
      safariType: ["SET_DEPARTURE", "PRIVATE_DEPARTURE", "FIT", "GIT", "EXTENSION", "EXCURSION"],
      safariServiceCategory: ["COVERAGE", "ACTIVITY", "UNKNOWN"],
      inclusionType : ["DIMENSION", "CUSTOMER_ATTRIBUTE", "ALL"],
      requirementType :["INCLUSION", "REQUIREMENT"],
      objectType : ["PERSON", "VEHICLE"],
      currencyUsage : ["PRODUCTS", "OPERATIONS", "ALL"],
      safariVehicleCategory : ["SAFARI", "LUGGAGE"],
      calculationMode : ["COMPUTE", "RESOLVE"],
      generalServiceType : ["ACCOMMODATION", "TRANSPORT", "MEAL", "PACKAGING" ,"MISCELLANEOUS" ,"OTHER", "LOCATION_MANAGEMENT" ],
      dayType : ["LEISURE", "CONFERENCE", "TRAVEL", "REST", "DEPARTURE", "ARRIVAL"],
      travellerCategory : ["TRAVELLER", "PROFESSIONAL"],
      inclusionType: ["DIMENSION", "CUSTOMER_ATTRIBUTE", "ALL"],
      requirementType: ["INCLUSION", "REQUIREMENT"],
      objectType: ["PERSON", "VEHICLE"],
      genders: ["MALE", "FEMALE", "OTHER"],
      currencyUsage: ["PRODUCTS", "OPERATIONS", "ALL"],
      safariVehicleCategory: ["SAFARI", "LUGGAGE"],
      calculationMode: ["COMPUTE", "RESOLVE"],
      dayType: ["LEISURE", "CONFERENCE", "TRAVEL", "REST", "DEPARTURE", "ARRIVAL"],
      otpChannel: ["SMS", "EMAIL"],
      purposeOfVisit: ["HOLIDAY", "HONEYMOON", "ANNIVERSARY", "BIRTHDAY", "PHOTOGRAPHY", "FAM_TRIP", "OTHER"],
      safariCycle: ["DAILY", "WEEKLY", "MONTHLY", "ANNUALLY", "OTHER"],
      dayModel: ["DATE", "DAY_OF_WEEK", "DAY_NUMBER"],
      safariStatus: ["NEW", "APPROVED", "OTHER"],
      safariScope: ["PRIVATE", "PUBLIC", "PROTECTED"],
      allocationModes: ["AUTO", "MANUAL"],
      contentStatus: ["DRAFT", "PUBLISHED", "UNPUBLISHED", "DELETED"],
      packageTypes: [{
        value: "SET_DEPARTURE",
        name: "Set Departure"
      }, {
        value: "PRIVATE_DEPARTURE",
        name: "Private Departure"
      }, {
        value: "FIT",
        name: "FIT"
      }, {
        value: "EXCURSION",
        name: "Excursion"
      }, {
        value: "EXTENSION",
        name: "Extension"
      }, {
        value: "GIT",
        name: "GIT"
      }],
      supportedLanguages: ["en", "sp", "fr"],
      safariYears: ["2018", "2019", "2020", "2021", "2022", "2023"],
      months: [{
        id: '01',
        abbreviation: "JAN",
        name: "JANUARY"
      }, {
        id: '02',
        abbreviation: "FEB",
        name: "FEBRUARY"
      }, {
        id: '03',
        abbreviation: "MAR",
        name: "MARCH"
      }, {
        id: '04',
        abbreviation: "APR",
        name: "APRIL"
      }, {
        id: '05',
        abbreviation: "MAY",
        name: "MAY"
      }, {
        id: '06',
        abbreviation: "JUN",
        name: "JUNE"

      }, {
        id: '07',
        abbreviation: "JUL",
        name: "JULY"

      }, {
        id: '08',
        abbreviation: "AUG",
        name: "AUGUST"

      }, {
        id: '09',
        abbreviation: "SEPT",
        name: "SEPTEMBER"

      }, {
        id: '10',
        abbreviation: "OCT",
        name: "OCTOBER"

      }, {
        id: '11',
        abbreviation: "NOV",
        name: "NOVEMBER"

      }, {
        id: '12',
        abbreviation: "DEC",
        name: "DECEMBER"

      }]
    });
})();
