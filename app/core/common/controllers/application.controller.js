
(function () {
    'use strict';

    angular
        .module('webApp')
        .controller('ApplicationController', ApplicationController);

    ApplicationController.$inject = ['$timeout', '$scope', '$stateParams', '$localStorage', 'URLS'];

    function ApplicationController($timeout, $scope, $stateParams, $localStorage, URLS) {
        var globalvm = this;
        globalvm.profile_url = URLS.PROFILE_IMAGE_URL;
        // console.log(globalvm.profile_url);
    }
})();
