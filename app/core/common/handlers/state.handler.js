(function() {
    'use strict';

    angular.module('webApp').factory('StateHandler', StateHandler);

    StateHandler.$inject = [
        '$rootScope',
        '$localStorage',
        '$location',
        'AuthService',
        '$state'
    ];

    function StateHandler(
        $rootScope,
        $localStorage,
        $location,
        AuthService,
        $state
    ) {
        return {
            initialize: initialize
        };

        function initialize() {
            var stateChangeStart = $rootScope.$on('$stateChangeStart', function(
                event,
                toState,
                toStateParams,
                fromState
            ) {
                $rootScope.toState = toState;
                $rootScope.toStateParams = toStateParams;
                $rootScope.fromState = fromState;

                // Redirect to a state with an external URL (http://stackoverflow.com/a/30221248/1098564)
                if (toState.external) {
                    event.preventDefault();
                    $window.open(toState.url, '_self');
                }

                //check whether the state requires authentication
                if (toState.data.requiresAuthentication) {
                    var permissions = toState.data.authorities;
                    //console.log(permissions);

                    //check whether the user is logged in
                    var user = $localStorage.user;

                    // console.log(user);
                    if (user === undefined) {
                        console.log('Not logged in');

                        $state.transitionTo('login');
                        event.preventDefault();
                    } else {
                        //user logged in, check state permissions
                        if (permissions.length) {
                            // user logged in ands state permission set, so we need to check the permissions before proceed to display page
                            if (!AuthService.userHasPermission(permissions)) {
                                //log some information on console
                                console.log(
                                    'user is allowed to view this state: ' +
                                        toState.name
                                );
                                console.log('State permissions: ');
                                console.log(permissions);

                                $state.transitionTo('http401');
                                event.preventDefault();
                            }
                        } else {
                            console.log(
                                "State '" +
                                    toState.name +
                                    "' does not require any permissions."
                            );
                        }
                    }
                } else {
                    //can be commented later
                    console.log(
                        "State '" +
                            toState.name +
                            "' does not require authentication."
                    );
                }
            });

            var stateChangeSuccess = $rootScope.$on(
                '$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams) {
                    //get session status
                }
            );

            $rootScope.$on('$destroy', function() {
                if (
                    angular.isDefined(stateChangeStart) &&
                    stateChangeStart !== null
                ) {
                    stateChangeStart();
                }
                if (
                    angular.isDefined(stateChangeSuccess) &&
                    stateChangeSuccess !== null
                ) {
                    stateChangeSuccess();
                }
            });

            $rootScope.$on('$stateChangeError', function(
                event,
                toState,
                toParams,
                fromState,
                fromParams,
                error
            ) {
                console.log(toState);
                console.log(error);
            });
        }
    }
})();
