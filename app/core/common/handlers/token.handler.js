(function () {
    'use strict';

    angular.module('webApp').factory('sessionRecoverer', sessionRecoverer);

    sessionRecoverer.$inject = [
        '$q',
        '$injector',
        '$localStorage'
    ];

    function sessionRecoverer(
        $q,
        $injector,
        $localStorage
    ) {
        var vm = this;
        var sessionRecoverer = {

            response: function(response){
                // console.log("response,,,");
                // console.log(response);
                if (response.status === 401) {
                    console.log("response status 401");

                }
                else if (response.status === 500) {
                    console.log("response status 500 ");

                }
    
                return response || $q.when(response);
            },
            requestError: function (request) {

                console.log("request");
                console.log(request);
            },
            
            responseError: function (response) {
                // Session has expired
                // console.log("sessionrecoveri,,,");
                // console.log(response);
                if (response.status == -1) {
                    console.log("-1");
                    var stateService = $injector.get('$state');

                    var storageService = $injector.get('AuthService');

                    var timeoutService = $injector.get('$timeout');

                    $localStorage.user = undefined;
                    //storageService.clear();
                    vm.isAuthenticated = null;

                    timeoutService( function(){
                        // stateService.go('login',{}, {reload:'login'});
                        // stateService.reload();
                        storageService.logout();

                    }, 1000 );

                    stateService.go('login',{}, {reload:'login'});
                    
                    //$state.go("login",{},{reload:true});
                    console.log("status 401");
                    // var AuthService = $injector.get('AuthService');
                    // var $http = $injector.get('$http');
                    // var deferred = $q.defer();

                    // Create a new session (recover the session)
                    // We use login method that logs the user in using the current credentials and
                    // returns a promise
                    //AuthService.login().then(deferred.resolve, deferred.reject);

                    
                    // When the session recovered, make the same backend call again and chain the request
                    // return deferred.promise.then(function () {
                    //     return $http(response.config);
                    // });
                }
                else if(response.status == 500){
                    console.log("500");
                }
                return $q.reject(response);
            }
        };
        return sessionRecoverer;
    }
})();
