(function () {
  'use strict';

  angular
    .module('webApp')
    .factory('TextUtil', TextUtil);


  function TextUtil() {


    var service = {
      shorten: shorten
    };

    return service;

    function shorten(description, limit) {
      console.log("description");
      console.log(description);

      console.log("limit");
      console.log(limit);

      var separator = '';


      if (limit === null || limit === undefined) {
        limit = 100;
      }
      if (description !== null && description !== undefined) {
        if (description.length > limit) {
          return description.substr(0, description.lastIndexOf(separator,limit)) + " ...";

        }
      }
      return description;
    }

  }
})();
