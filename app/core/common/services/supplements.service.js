(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('SupplementsService', SupplementsService);

    SupplementsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function SupplementsService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/supplements'
            },
             'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/supplements'
            },
            'getById': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'costingservice/api/supplements/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/supplements'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/supplements'
            },
            'createList': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/supplements/create-list'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/supplements/:id'
            }
        });
    }
})();