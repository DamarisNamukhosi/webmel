
(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('ObjectFeaturesService', ObjectFeaturesService);

    ObjectFeaturesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ObjectFeaturesService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
          'getFeaturesByObjectId': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/object-features/filter-by-object/:uuid'
          },
          'getFeaturesByObjectType': {
              method: 'GET',
              headers: {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/general-features/filter-by-object-type/:objectType'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/object-features/create'
        },
          'get': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              transformResponse: function (data) {
                  if (data) {
                      data = angular.fromJson(data);
                  }
                  return data;
              },
              isArray: false,
              url: URLS.BASE_URL + 'contentservice/api/object-features/:id'
          },
          'update': {
              method: 'PUT',
              headers : {
              'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-features'
          },
          'createList': {
              method: 'POST',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/object-features/create-list'
          },
          'delete': {
              method: 'DELETE',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-features/:id'
          }
      });
  }
})();
