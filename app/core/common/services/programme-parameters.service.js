(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('ProgrammeParametersService', ProgrammeParametersService);

  ProgrammeParametersService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ProgrammeParametersService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-params'
      },
      'getAll': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-params'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/programme-params/:id'
      },
      'getByProgramme': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-params//filter-by-program/:id&type=:type'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-params'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-params'
      },
      'createList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/programme-params/create-list'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/programme-params/:id'
      }
    });
  }
})();
