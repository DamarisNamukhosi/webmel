(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('SeasonGroupsService', SeasonGroupsService);

  SeasonGroupsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function SeasonGroupsService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      getByOranizationId: {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/season-groups/filter-by-organization/:id'
      },
      //
      getByOranizationIdAndYear: {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/season-groups/filter-by-organization/:id?year=:year'
      },
      getFull: {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'contentservice/api/season-groups/get-full/?year=:year& organizationId =:organizationId'
      },
      createFull: {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/create-full'
      },
      query: {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/season-groups'
      },
      get: {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/:id'
      },
      'getFull': {
        method: 'GET',
        isArray: false,
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/get-full?id=:id'
      },
      update: {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups'
      },
      updateFull: {
        method: 'PUT',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/update-full'
      },
      create: {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups'
      },
      delete: {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/:id'
      },
      deleteFull: {
        method: 'DELETE',
        headers: {
          Authorization: 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-groups/delete-full/:id'
      }
    });
  }
})();
