
(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('ObjectLabelsService', ObjectLabelsService);

ObjectLabelsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ObjectLabelsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
          'getLabelsByObjectId': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/object-labels/filter-by-object/:uuid'
          },
          'getLabelsByObjectType': {
              method: 'GET',
              headers: {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/label-groups/filter-by-object/:objectType'
          },
          'getLabelsByLabelGroupId': {
              method: 'GET',
              headers: {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/labels/filter-by-label-group/:labelGroupId'
          }, 
          'createList': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/object-labels/create-list'
        },
          'get': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              transformResponse: function (data) {
                  if (data) {
                      data = angular.fromJson(data);
                  }
                  return data;
              },
              isArray: false,
              url: URLS.BASE_URL + 'contentservice/api/object-labels/:id'
          },
          'update': {
              method: 'PUT',
              headers : {
              'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-labels'
          },
          'create': {
              method: 'POST',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-labels'
          },
          'delete': {
              method: 'DELETE',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-labels/:id'
          }
      });
  }
})();
