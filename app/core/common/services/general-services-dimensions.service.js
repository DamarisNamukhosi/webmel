(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('GeneralServicesDimensionsService', GeneralServicesDimensionsService);

  GeneralServicesDimensionsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function GeneralServicesDimensionsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'getById': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions/:id'
        },
        'getByServiceId': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions/filter-by-general-service/:id'
        },
        'getByGeneralServiceId': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions'
        },
        'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray:true,
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions'
        },
        'create': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions'
        },
        'createFull': {
          method: 'POST',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions/create-full'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'costingservice/api/general-service-dimensions/:id'
        }
      });
  }
})();
