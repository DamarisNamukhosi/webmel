(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('SeasonBandsService', SeasonBandsService);

  SeasonBandsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function SeasonBandsService($resource, $localStorage, URLS) {
    var resourceUrl = '';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/season-bands'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },

        isArray: false,
        url: URLS.BASE_URL + 'contentservice/api/season-bands/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-bands'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-bands'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/season-bands/:id'
      }
    });
  }
})();
