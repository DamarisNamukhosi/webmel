(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('CurrenciesService', CurrenciesService);

    CurrenciesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function CurrenciesService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/currencies'
            }, 'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: false,
                url: URLS.BASE_URL + 'costingservice/api/currencies/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/currencies'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/currencies'
            },
            'saveMultiple': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'costingservice/api/currencies/save-multiple'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'costingservice/api/currencies/:id'
            }
        });
    }
})();