(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('RoomCategoryService', RoomCategoryService);

  RoomCategoryService.$inject = ['$resource', '$localStorage', 'URLS'];

  function RoomCategoryService($resource, $localStorage, URLS) {
    var resourceUrl = '';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories'
      },
      'filterByOrganisation': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/room-categories/filter-by-organisation/:id'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },

        isArray: false,
        url: URLS.BASE_URL + 'contentservice/api/rooms/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/rooms'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/rooms'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/rooms/:id'
      }
    });
  }
})();
