(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('GeneralServicesService', GeneralServicesService);

  GeneralServicesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function GeneralServicesService($resource, $localStorage, URLS) {
    var resourceUrl = '';

    return $resource(resourceUrl, {}, {
      'getByOrganizationTypeId': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/general-services/filter-by-organisation-type/:id'
      },
      'filterByGenSvcType': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/general-services/filter-by-type/:type'
      },
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/general-services'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },

        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/general-services/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/general-services'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/general-services'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/general-services/:id'
      }
    });
  }
})();
