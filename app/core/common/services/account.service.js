(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('AccountService', AccountService);

    AccountService.$inject = ['$localStorage', '$http', 'URLS'];

    function AccountService($localStorage, $http, URLS) {
        var acc = {};

        acc.getAccountData = function () {

            return $http({
                method: 'GET',
                url:  URLS.BASE_URL + 'contentservice/api/account',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                }
            });
        };

        return acc;
    }
})();
