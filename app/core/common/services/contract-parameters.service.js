(function () {
  'use strict';
  angular
    .module('webApp')
    .factory('ContractParametersService', ContractParametersService);

  ContractParametersService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ContractParametersService($resource, $localStorage, URLS) {
    var resourceUrl = 'data/data.json';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/contract-params'
      },
      'get': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/contract-params'
      },
      'getById': {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: false,
        url: URLS.BASE_URL + 'costingservice/api/contract-params/:id'
      },
      'update': {
        method: 'PUT',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/contract-params'
      },
      'create': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/contract-params'
      },
      'createList': {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'costingservice/api/contract-params/create-list'
      },
      'delete': {
        method: 'DELETE',
        headers: {
          'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'costingservice/api/contract-params/:id'
      }
    });
  }
})();
