
(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('ObjectServicesService', ObjectServicesService);

    ObjectServicesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function ObjectServicesService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
          'getServicesByObjectId': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/object-services/filter-by-object/:uuid'
          },
          'getServicesByObjectType': {
              method: 'GET',
              headers: {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/general-services/filter-by-object-type/:objectType'
          },
          'create': {
            method: 'POST',
            headers : {
                'Authorization': 'Bearer ' + $localStorage.user
            },
            isArray: true,
            url: URLS.BASE_URL + 'contentservice/api/object-services/create'
        },
          'get': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              transformResponse: function (data) {
                  if (data) {
                      data = angular.fromJson(data);
                  }
                  return data;
              },
              isArray: false,
              url: URLS.BASE_URL + 'contentservice/api/object-services/:id'
          },
          'update': {
              method: 'PUT',
              headers : {
              'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-services'
          },
          'create': {
              method: 'POST',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-services'
          },
          'delete': {
              method: 'DELETE',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/object-services/:id'
          }
      });
  }
})();
