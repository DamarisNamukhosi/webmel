(function () {
    'use strict';
    angular
        .module('webApp')
        .factory('DaysService', DaysService);

    DaysService.$inject = ['$resource', '$localStorage', 'URLS'];

    function DaysService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user,
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/days'
            }, 'get': {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/days/:id'
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/days'
            },
            'create': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/days'
            },
            'saveMultiple': {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                isArray: true,
                url: URLS.BASE_URL + 'contentservice/api/days/create-list'
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + $localStorage.user
                },
                url: URLS.BASE_URL + 'contentservice/api/days/:id'
            }
        });
    }
})();