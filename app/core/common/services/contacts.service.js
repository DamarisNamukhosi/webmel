(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('ContactsService', ContactsService);

      ContactsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function ContactsService($resource, $localStorage, URLS) {
    var resourceUrl =  'data/data.json';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers : {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/contacts'
      },
      'getProfessionalContacts': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/contacts/filter-by-object/:objectId'
      },

      'getProfessionalContactAdresses': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/contact-addresses/filter-by-contact/:contactId'
      },
      //
      'get': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/contacts/:id'
      },
      'update': {
        method: 'PUT',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/contacts'
      },
      'create': {
          method: 'POST',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/contacts'
      },
      'delete': {
        method: 'DELETE',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/contacts/:id'
      }
    });
  }
})();
