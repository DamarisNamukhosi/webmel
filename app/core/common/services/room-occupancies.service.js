(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('RoomOccupanciesService', RoomOccupanciesService);

    RoomOccupanciesService.$inject = ['$resource', '$localStorage', 'URLS'];

    function RoomOccupanciesService($resource, $localStorage, URLS) {
        var resourceUrl = 'data/data.json';

        return $resource(
            resourceUrl,
            {},
            {
                query: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/general-room-occupancies',
                },
                get: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: false,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/general-room-occupancies/:id',
                },
                getByOrganization: {
                    method: 'GET',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    isArray: true,
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/room-occupancies/filter-by-organisation/:id',
                },
                update: {
                    method: 'PUT',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/general-room-occupancies',
                },
                create: {
                    method: 'POST',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/general-room-occupancies',
                },
                delete: {
                    method: 'DELETE',
                    headers: { Authorization: 'Bearer ' + $localStorage.user },
                    url:
                        URLS.BASE_URL +
                        'contentservice/api/general-room-occupancies/:id',
                },
            });
    }
})();
