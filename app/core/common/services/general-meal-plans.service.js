(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('GeneralMealPlansService', GeneralMealPlansService);

    GeneralMealPlansService.$inject = ['$resource', '$localStorage', 'URLS'];

    function GeneralMealPlansService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'query': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/general-meal-plans'
        },'get': {
          method: 'GET',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'contentservice/api/general-meal-plans/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-meal-plans'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-meal-plans'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-meal-plans/:id'
        }
      });
    }
})();
