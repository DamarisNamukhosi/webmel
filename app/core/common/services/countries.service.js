(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('CountriesService', CountriesService);

      CountriesService.$inject = ['$resource', '$localStorage', 'URLS'];

  function CountriesService($resource, $localStorage, URLS) {
    var resourceUrl =  'data/data.json';

    return $resource(resourceUrl, {}, {
      'query': {
        method: 'GET',
        headers : {
          'Authorization': 'Bearer ' + $localStorage.user,
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/countries'
      },'get': {
        method: 'GET',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        isArray: true,
        url: URLS.BASE_URL + 'contentservice/api/countries/:id'
      },
      'update': {
        method: 'PUT',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/countries'
      },
      'create': {
          method: 'POST',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/countries'
      },
      'delete': {
        method: 'DELETE',
        headers : {
            'Authorization': 'Bearer ' + $localStorage.user
        },
        url: URLS.BASE_URL + 'contentservice/api/countries/:id'
      }
    });
  }
})();
