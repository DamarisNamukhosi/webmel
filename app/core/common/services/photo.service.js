(function() {
  "use strict";
  angular.module("webApp").factory("PhotoService", PhotoService);

  PhotoService.$inject = ["$resource", "$localStorage", "URLS"];

  function PhotoService($resource, $localStorage, URLS) {
    var resourceUrl = "data/data.json";

    return $resource(
      resourceUrl,
      {},
      {
        uploadProfilePhoto: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "mediaservice/api/media-items/save-with-object"
        }
      }
    );
  }
})();
