(function() {
  "use strict";
  angular.module("webApp").factory("MediaItemsService", MediaItemsService);

  MediaItemsService.$inject = ["$resource", "$localStorage", "URLS"];

  function MediaItemsService($resource, $localStorage, URLS) {
    var resourceUrl = "data/data.json";

    return $resource(
      resourceUrl,
      {},
      {
        deleteFull: {
          method: 'DELETE',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/delete-full/:id'
        },
        'query': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'mediaservice/api/media-items/'
        }, 'get': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'mediaservice/api/media-items/:id'
        },
        'update': {
          method: 'PUT',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items'
        },
        'create': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items'
        },
        'delete': {
          method: 'DELETE',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/:id'
        },
        'saveMultiple': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-multiple'
        },
        'uploadProfilePhoto': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-with-object'
        }
        ///api/media-items/save-multiple
      }
    );
  }
})();
