(function() {
  "use strict";
  angular.module("webApp").factory("MarketsCountriesService", MarketsCountriesService);

  MarketsCountriesService.$inject = ["$resource", "$localStorage", "URLS"];

  function MarketsCountriesService($resource, $localStorage, URLS) {
    var resourceUrl = "";

    return $resource(
      resourceUrl,
      {},
      {
        getByCountries: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url:
            URLS.BASE_URL +
            "contentservice/api/market-countries/filter-by-room-category/:id"
        },
        createFull: {
          method: "POST",
          isArray: true,
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/market-countries/create-list"
        },
        getByOrganisation: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "contentservice/api/market-countries/filter-by-organisation/:id"
        },
        getById: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "contentservice/api/market-countries/:id"
        },
        get: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + "contentservice/api/market-countries/"
        },
        getCountryMarketByOrgId: {
          method: "GET",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + "contentservice/api/market-countries/resolve/:id?countryId=:countryId"
        },
        update: {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/market-countries"
        },
        create: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/market-countries"
        },
        delete: {
          method: "DELETE",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "contentservice/api/market-countries/:id"
        }
      }
    );
  }
})();
