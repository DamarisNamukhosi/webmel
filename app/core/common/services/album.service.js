(function() {
  "use strict";
  angular.module("webApp").factory("AlbumService", AlbumService);

  AlbumService.$inject = ["$resource", "$localStorage", "URLS"];

  function AlbumService($resource, $localStorage, URLS) {
    var resourceUrl = "data/data.json";

    return $resource(
      resourceUrl,
      {},
      {
        createAlbum: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "mediaservice/api/albums"
        },
        createAlbumFull: {
          method: "POST",
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "mediaservice/api/albums/create-full"
        },
        getAlbums: {
          method: "GET",
          isArray: true,
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "mediaservice/api/albums/filter-by-object/:uuid"
        },
        getProfile: {
          method: "GET",
   
          url: URLS.BASE_URL + "mediaservice/api/albums/get-profile/:uuid"
        },
        //
        getAlbum: {
          method: "GET",
          isArray: false,
          headers: {
            Authorization: "Bearer " + $localStorage.user
          },
          url: URLS.BASE_URL + "mediaservice/api/albums/get-full/:albumId"
        },
        deleteFull: {
          method: 'DELETE',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/albums/delete-full/:id'
        },
        'query': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'mediaservice/api/albums/'
        }, 'get': {
          method: 'GET',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: false,
          url: URLS.BASE_URL + 'mediaservice/api/albums/:id'
        },
        'update': {
          method: 'PUT',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/albums'
        },
        'create': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/albums'
        },
        'delete': {
          method: 'DELETE',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'mediaservice/api/albums/:id'
        },
        'saveMultiple': {
          method: 'POST',
          headers: {
            'Authorization': 'Bearer ' + $localStorage.user
          },
          isArray: true,
          url: URLS.BASE_URL + 'mediaservice/api/media-items/save-multiple'
        }
        ///api/media-items/save-multiple
      }
    );
  }
})();
