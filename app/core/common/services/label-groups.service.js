
(function() {
  'use strict';
  angular
      .module('webApp')
      .factory('LabelGroupsService', LabelGroupsService);

LabelGroupsService.$inject = ['$resource', '$localStorage', 'URLS'];

  function LabelGroupsService ($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
          'getLabelGroupsByObjectType': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user,
              },
              isArray: true,
              url: URLS.BASE_URL + 'contentservice/api/label-groups/filter-by-object/:objectType'
          },
          'get': {
              method: 'GET',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              transformResponse: function (data) {
                  if (data) {
                      data = angular.fromJson(data);
                  }
                  return data;
              },
              isArray: false,
              url: URLS.BASE_URL + 'contentservice/api/label-groups/:id'
          },
          'update': {
              method: 'PUT',
              headers : {
              'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/label-groups'
          },
          'create': {
              method: 'POST',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/label-groups'
          },
          'delete': {
              method: 'DELETE',
              headers : {
                  'Authorization': 'Bearer ' + $localStorage.user
              },
              url: URLS.BASE_URL + 'contentservice/api/label-groups/:id'
          }
      });
  }
})();
