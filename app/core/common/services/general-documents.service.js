(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('GeneralDocumentsService', GeneralDocumentsService);

        GeneralDocumentsService.$inject = ['$resource', '$localStorage', 'URLS'];

    function GeneralDocumentsService($resource, $localStorage, URLS) {
      var resourceUrl =  'data/data.json';

      return $resource(resourceUrl, {}, {
        'query': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/general-documents/:id'
        },
        'getDocumentsByOrgId': {
          method: 'GET',
          headers : {
            'Authorization': 'Bearer ' + $localStorage.user,
          },
          isArray: true,
          url: URLS.BASE_URL + 'contentservice/api/general-documents/filter-by-organisation/:id'
        },
        'update': {
          method: 'PUT',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-documents'
        },
        'create': {
            method: 'POST',
            headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-documents'
        },
        'delete': {
          method: 'DELETE',
          headers : {
              'Authorization': 'Bearer ' + $localStorage.user
          },
          url: URLS.BASE_URL + 'contentservice/api/general-documents/:id'
        }
      });
    }
})();
