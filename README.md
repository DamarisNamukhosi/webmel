# Safariway

The Angular web application.

## Setup the development environment

Run `npm install`.

Run `bower install`.

## Building for development

Run `grunt serve` for preview while in development.

## Building for production

Run `grunt build` to build the /dist folder. 

Run `cd dist`.

Run `cf push`.

## Testing

Running `grunt test` will run the unit tests with karma.
